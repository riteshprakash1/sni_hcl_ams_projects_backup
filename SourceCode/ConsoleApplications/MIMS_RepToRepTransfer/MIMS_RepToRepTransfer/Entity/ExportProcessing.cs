﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace MIMS_RepToRepTransfer.Entity
{
    /// <summary> This is the Export Processing Entity. </summary>  
    class ExportProcessing
    {

        /// <summary>This is the ProcessingID.</summary>
        private Int64 intProcessingID;

        /// <summary>This is the StartDate.</summary>
        private string strStartDate;

        /// <summary>This is the LastUpdated.</summary>
        private string strLastUpdated;

        /// <summary>This is the ProcessingArgument.</summary>
        private string strProcessingArgument;

        /// <summary>This is the ProcessingStatus.</summary>
        private string strProcessingStatus;

        /// <summary>This is the ProcessingMessage.</summary>
        private string strProcessingMessage;

        /// <summary>This is the ProcessingType.</summary>
        private string strProcessingType;

        /// <summary>This is the Duration.</summary>
        private string strDuration;

        /// <summary>Initializes a new instance of the ExportProcessing class.</summary>
        public ExportProcessing()
        {
            this.StartDate = string.Empty;
            this.LastUpdated = string.Empty;
            this.ProcessingArgument = string.Empty;
            this.ProcessingStatus = string.Empty;
            this.ProcessingMessage = string.Empty;
            this.strProcessingType = string.Empty;
            this.Duration = string.Empty;
        }

        /// <summary>Gets or sets the ProcessingID.</summary>
        /// <value>The ProcessingID.</value>
        public Int64 ProcessingID
        {
            get { return this.intProcessingID; }
            set { this.intProcessingID = value; }
        }

        /// <summary>Gets or sets the StartDate.</summary>
        /// <value>The StartDate.</value>
        public string StartDate
        {
            get { return this.strStartDate; }
            set { this.strStartDate = value; }
        }

        /// <summary>Gets or sets the LastUpdated.</summary>
        /// <value>The LastUpdated.</value>
        public string LastUpdated
        {
            get { return this.strLastUpdated; }
            set { this.strLastUpdated = value; }
        }

        /// <summary>Gets or sets the ProcessingArgument.</summary>
        /// <value>The ProcessingArgument.</value>
        public string ProcessingArgument
        {
            get { return this.strProcessingArgument; }
            set { this.strProcessingArgument = value; }
        }

        /// <summary>Gets or sets the ProcessingStatus.</summary>
        /// <value>The ProcessingStatus.</value>
        public string ProcessingStatus
        {
            get { return this.strProcessingStatus; }
            set { this.strProcessingStatus = value; }
        }

        /// <summary>Gets or sets the ProcessingMessage.</summary>
        /// <value>The ProcessingMessage.</value>
        public string ProcessingMessage
        {
            get { return this.strProcessingMessage; }
            set { this.strProcessingMessage = value; }
        }

        /// <summary>Gets or sets the ProcessingType.</summary>
        /// <value>The ProcessingType.</value>
        public string ProcessingType
        {
            get { return this.strProcessingType; }
            set { this.strProcessingType = value; }
        }

        /// <summary>Gets or sets the Duration.</summary>
        /// <value>The Duration.</value>
        public string Duration
        {
            get { return this.strDuration; }
            set { this.strDuration = value; }
        }


        /// <summary>Receives a ExportProcessing datarow and converts it to a ExportProcessing.</summary>
        /// <param name="r">The ExportProcessing DataRow.</param>
        /// <returns>ExportProcessing</returns>
        protected static ExportProcessing GetEntityFromDataRow(DataRow r)
        {
            ExportProcessing t = new ExportProcessing();

            if ((r["ProcessingID"] != null) && (r["ProcessingID"] != DBNull.Value))
                t.ProcessingID = Convert.ToInt32(r["ProcessingID"].ToString());

            t.StartDate = r["StartDate"].ToString();
            t.LastUpdated = r["LastUpdated"].ToString();
            t.ProcessingArgument = r["ProcessingArgument"].ToString();
            t.ProcessingStatus = r["ProcessingStatus"].ToString();
            t.ProcessingMessage = r["ProcessingMessage"].ToString();
            t.ProcessingType = r["ProcessingType"].ToString();
            t.Duration = r["Duration"].ToString();

            return t;
        }

        /// <summary>Gets Last Processing record.</summary>
        /// <returns>The newly populated ExportProcessing</returns>
        public static ExportProcessing GetLastExportProcessingByType(string strProcessingType)
        {
            ExportProcessing entity = null;

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@ProcessingType", strProcessingType));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetLastExportProcessingByType", parameters);

            if (dt.Rows.Count > 0)
            {
                entity = ExportProcessing.GetEntityFromDataRow(dt.Rows[0]);
            }

            return entity;
        }


        /// <summary>Gets ExportProcessing by the OrderProcessing ID .</summary>
        /// <param name="intProcessingID">the ProcessingID</param>
        /// <returns>The newly populated ProcessingEntity</returns>
        public static ExportProcessing GetExportProcessingByProcessingID(Int64 intProcessingID)
        {
            ExportProcessing entity = null;

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@ProcessingID", intProcessingID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetExportProcessingByProcessingID", parameters);

            if (dt.Rows.Count > 0)
            {
                entity = ExportProcessing.GetEntityFromDataRow(dt.Rows[0]);
            }

            return entity;
        }

        /// <summary>Insert ExportProcessing record</summary>
        /// <param name="r">the ExportProcessing object</param>
        /// <returns>the ExportProcessing with the new ID</returns>
        public static ExportProcessing InsertExportProcessing(ExportProcessing r)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            SqlParameter paramID = new SqlParameter("@ProcessingID", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            parameters.Add(paramID);

            parameters.Add(new SqlParameter("@ProcessingArgument", r.ProcessingArgument));
            parameters.Add(new SqlParameter("@ProcessingType", r.ProcessingType));

            parameters = SQLUtility.SqlExecuteNonQueryReturnParameters("sp_InsertExportProcessing", parameters);
            r.ProcessingID = Convert.ToInt64(paramID.Value.ToString());

            return r;
        }

        /// <summary>Updates the ExportProcessing record</summary>
        /// <param name="r">the ExportProcessing object</param>
        /// <returns>the number of records updated</returns>
        public static int UpdateExportProcessing(ExportProcessing r)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            parameters.Add(new SqlParameter("@ProcessingID", r.ProcessingID));
            parameters.Add(new SqlParameter("@ProcessingStatus", r.ProcessingStatus));
            parameters.Add(new SqlParameter("@ProcessingMessage", r.ProcessingMessage));
            parameters.Add(new SqlParameter("@ProcessingType", r.ProcessingType));
            parameters.Add(new SqlParameter("@Duration", r.Duration));

            int inctCnt = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateExportProcessing", parameters);

            return inctCnt;
        }

    } //// end class
}
