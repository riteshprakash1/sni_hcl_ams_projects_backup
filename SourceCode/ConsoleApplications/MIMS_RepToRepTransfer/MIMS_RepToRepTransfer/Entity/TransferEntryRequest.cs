﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace MIMS_RepToRepTransfer.Entity
{
    /// <summary>
    /// This class holdes root element required for Transfer request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    [XmlRoot("Body")]
    public class TransferEntryRequest
    {
        /// <summary>default constructor</summary>
        public TransferEntryRequest()
        { }

        #region local variables

        private Transfer _transfer;
        #endregion

        /// <summary>gets or sets transfer details as object</summary>  
        /// 
        [XmlElement("Recordset")]
        public Transfer Transfer
        {
            get { return _transfer; }
            set { _transfer = value; }
        }
    }
}
