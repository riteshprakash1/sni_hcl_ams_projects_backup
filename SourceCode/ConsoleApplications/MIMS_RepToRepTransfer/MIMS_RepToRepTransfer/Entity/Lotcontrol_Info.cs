﻿using System;
using System.Xml.Serialization;

namespace MIMS_RepToRepTransfer.Entity
{
    /// <summary>
    /// This class holds Lot Control Info required for the Transfer request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    [XmlRoot("Lotcontrol_Info")]
    public class Lotcontrol_Info
    {
        /// <summary>Default constructor</summary>
        public Lotcontrol_Info()
        { }

        /// <summary>constructor with Serial Number</summary>
        /// <param name="strSerialNumber">the SerialNumber</param>
        public Lotcontrol_Info(string strSerialNumber)
        {
            this.SerialNum = strSerialNumber;
        }

        private string _serialNum = string.Empty;

        /// <summary>Gets or sets SerialNum </summary>
        [XmlElement("SerialNum")]
        public string SerialNum
        {
            get { return _serialNum; }
            set { _serialNum = value; }
        }

    } ////end class
}
