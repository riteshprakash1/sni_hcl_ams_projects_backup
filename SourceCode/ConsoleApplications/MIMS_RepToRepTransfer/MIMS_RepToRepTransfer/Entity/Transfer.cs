﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MIMS_RepToRepTransfer.Entity
{
    /// <summary>
    ///     This class holds Transfer required for 
    ///     Transfer section in the transfer request XML.
    ///     Class is marked as Serializable so that all the public fields are Serialized
    /// </summary> 
    [Serializable]
    public class Transfer
    {
        /// <summary>Default constructor</summary>
        public Transfer()
        { }
        #region local variables
        private Int64 _transferID;
        private Int64 _transferLineID;
        private string _transferFromRepID = string.Empty;
        private string _transferToRepID = string.Empty;
        private string _materialID = string.Empty;
        private int _qty;
        private string _batch = string.Empty;
        private Lotcontrol_Info _lc_Info;
        private string _sentToSap = string.Empty;
        private string _field1 = string.Empty;
        //private string _field2 = string.Empty;
        private string _field3 = string.Empty;
        private string _status = string.Empty;
        private int _statusID;


        #endregion

        /* i.Material_Desc, i., i.,
        i.[Include], i.SentToSAP, i.ExportXML, i.CreatedOn, i.CreatedBy, i.LastUpdatedOn, i.LastUpdatedBy,
        */

        /// <summary>gets or sets TransferID</summary>        
        [XmlIgnore]
        public Int64 TransferID
        {
            get { return _transferID; }
            set { _transferID = value; }
        }

        /// <summary>gets or sets Transfer From RepID</summary>        
        [XmlElement("From_Rep")]
        public string TransferFromRepID
        {
            get { return _transferFromRepID; }
            set { _transferFromRepID = value; }
        }

        /// <summary>gets or sets Transfer To RepID</summary>        
        [XmlElement("To_Rep")]
        public string TransferToRepID
        {
            get { return _transferToRepID; }
            set { _transferToRepID = value; }
        }

        /// <summary>gets or sets MaterialID</summary>        
        [XmlElement("Material")]
        public string MaterialID
        {
            get { return _materialID; }
            set { _materialID = value; }
        }

        /// <summary>gets or sets Qty</summary>        
        [XmlElement("Qty")]
        public int Qty
        {
            get { return _qty; }
            set { _qty = value; }
        }

        /// <summary>gets or sets Batch</summary>        
        [XmlElement("Batch")]
        public string Batch
        {
            get { return _batch; }
            set { _batch = value; }
        }

        /// <summary>Gets of sets Header object</summary>
        [XmlElement("Lotcontrol_Info")]
        public Lotcontrol_Info LC_Info
        {
            get { return _lc_Info; }
            set { _lc_Info = value; }
        }

        /// <summary>gets or sets Sent to SAP</summary>        
        [XmlIgnore]
        public string SentToSAP
        {
            get { return _sentToSap; }
            set { _sentToSap = value; }
        }

        /// <summary>gets or sets Field1 (PlaceHolder)</summary>        
        [XmlElement("Field1")]
        public string Field1
        {
            get { return _field1; }
            set { _field1 = value; }
        }

        /// <summary>gets or sets Transfer Line ID</summary>        
        [XmlElement("Field2")]
        public Int64 TransferLineID
        {
            get { return _transferLineID; }
            set { _transferLineID = value; }
        }

        /// <summary>gets or sets Field3 (PlaceHolder)</summary>        
        [XmlElement("Field3")]
        public string Field3
        {
            get { return _field3; }
            set { _field3 = value; }
        }

        /// <summary>gets or sets transfer status</summary>        
        [XmlIgnore]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>gets or sets transfer status ID</summary>        
        [XmlIgnore]
        public int StatusID
        {
            get { return _statusID; }
            set { _statusID = value; }
        }

    } //// end class
}
