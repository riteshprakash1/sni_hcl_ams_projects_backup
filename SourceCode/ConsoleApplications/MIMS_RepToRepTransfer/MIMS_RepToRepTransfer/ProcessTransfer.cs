﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.Threading;
using MIMS_RepToRepTransfer.Entity;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace MIMS_RepToRepTransfer
{
    /// <summary>Class that process all the unprocessed Transfers and places request in SAP</summary>
    public class ProcessTransfer
    {
        #region Local variables
        TransferAccess _transferAccess = new TransferAccess();
        Int64 _transferLineId;
        protected string xmlstring;
        private string strExecutionLocation;
        protected Int64 intProcessingID;
        #endregion

        /// <summary>Initializes a new instance of the <see cref="ProcessTransfer"/> class.</summary>
        public ProcessTransfer(Int64 intID)
        {
            intProcessingID = intID;
        }

        /// <summary>Process the Transfer Lines by the TransferID</summary>
        /// <param name="intTransferID">the Transfer ID</param>
        /// <returns>the number of Transfers processed</returns>
        public int ProcessTransferLinesByTransferID(Int64 intTransferID)
        {
            int intProcessed = 0;
            strExecutionLocation = "Executing ProcessTransferLinesByTransferID ";
            try
            {
                List<Transfer> transfers = _transferAccess.GetApprovedUnProcessedTranferLinesByTransferID(intTransferID);
                intProcessed = this.ProcessTransferList(transfers);
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                throw new Exception("ProcessTransferLinesByTransferID Exception");
            }

            return intProcessed;

        }

        /// <summary>Process the Transfer Lines by the TransferLineID</summary>
        /// <param name="intTransferLineID">the Transfer ID</param>
        /// <returns>the number of Transfers processed</returns>
        public int ProcessTransferLinesByTransferLineID(Int64 intTransferLineID)
        {
            int intProcessed = 0;
            strExecutionLocation = "Executing ProcessTransferLinesByTransferLineID ";
            try
            {
                List<Transfer> transfers = _transferAccess.GetApprovedUnProcessedTranferLinesByTransferLineID(intTransferLineID);
                intProcessed = this.ProcessTransferList(transfers);
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                throw new Exception("ProcessTransferLinesByTransferLineID Exception");
            }

            return intProcessed;

        }


        /// <summary>Gets all the approved unprocessd Transfers Lines from database</summary>
        /// <returns>the number of transfers processed</returns>
        public int GetApprovedUnProcessedTranferLines()
        {
            int intTransfersProcessed = 0;
            strExecutionLocation = "Executing GetApprovedUnProcessedTranferLines ";
            try
            {
                List<Transfer> transfers = _transferAccess.GetApprovedUnProcessedTranferLines();
                intTransfersProcessed = this.ProcessTransferList(transfers);
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                throw new Exception("GetApprovedUnProcessedTranferLines Exception");
            }
            return intTransfersProcessed;

        }

        /// <summary>This is used to manually test the tranfers process without pulling from the database</summary>
        /// <returns>the number of transfers processed</returns>
        public int TestTransferProcess()
        {
            int intTransfersProcessed = 0;
            strExecutionLocation = "Executing TestTransferProcess";
            try
            {
                List<Transfer> _transfers = new List<Transfer>();
                Transfer rTansfer = new Transfer();

                rTansfer.TransferID = 2;
                rTansfer.TransferLineID = 2;
                rTansfer.TransferFromRepID = "0000100100-0001";
                rTansfer.TransferToRepID = "0000100015-0002";
                rTansfer.MaterialID = "014201";
                rTansfer.Qty = 4;
                rTansfer.Batch = "BatchNo";
                rTansfer.LC_Info = new Lotcontrol_Info("");

                _transfers.Add(rTansfer);
                intTransfersProcessed = this.ProcessTransferList(_transfers);
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                throw new Exception("TestTransferProcess Exception");
            }
            return intTransfersProcessed;

        }

        /// <summary>Processes the List of Transfer Lines Passed</summary>
        /// <param name="transfers">the Transfer List</param>
        /// <returns>the number of transfers processed</returns>
        public int ProcessTransferList(List<Transfer> transfers)
        {
            int intTransferLinesProcessed = 0;
            strExecutionLocation = "Starting ProcessTransferList: " + transfers.Count.ToString();
            try
            {
                foreach (Transfer t in transfers)
                {
                    _transferLineId = t.TransferLineID;
                    strExecutionLocation = "TransferLineID: " + _transferLineId.ToString();

                    TransferEntryRequest request = _transferAccess.GetTransferEntryRequest(t);
                    strExecutionLocation += "; GetTransferEntryRequest";

                    xmlstring = this.GetXMLString(request);
                    strExecutionLocation += "; GetXMLString";

                    bool blnSubmitSuccess = this.SubmitXmlWebRequest(xmlstring);
                    strExecutionLocation += "; SubmitXmlWebRequest";

                    if (blnSubmitSuccess)
                    {
                        // if saveXmlInDB is true send the XmlString to the DB otherwise make is blank
                        if (!Convert.ToBoolean(ConfigurationManager.AppSettings["saveXmlInDB"]))
                        {
                            xmlstring = string.Empty;
                        }
                        
                        _transferAccess.UpdateTransferLineSentToSap(_transferLineId, xmlstring);
                        strExecutionLocation += "; UpdateTransferLineSentToSap";

                        Program.WriteToConsole(DateTime.Now + " : Transfer Line Completed (" + _transferLineId.ToString() + ")");

                        intTransferLinesProcessed++;
                    }
                }
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error: " + strExecutionLocation + ": " + ex.Message);
            }

            return intTransferLinesProcessed;
        }

        /// <summary>Submits the XML Web Request to SAP</summary>
        /// <param name="strXml">the XML String</param>
        /// <returns>true/false</returns>
        public bool SubmitXmlWebRequest(string strXml)
        {
            string strResponseXML = string.Empty;
            bool blnSubmitSuccessful = false;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["transmitXML"]))
            {
             //   Program.WriteToConsole("In SubmitXmlWebRequest");

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["uri"].ToString());

                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml; charset=utf-8";

                webRequest.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["userName"].ToString(),
                    ConfigurationManager.AppSettings["passWord"].ToString());

                if (ConfigurationManager.AppSettings["uri"].ToString().ToLowerInvariant().Contains("https"))
                {
                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };
                }

                byte[] bytes = Encoding.ASCII.GetBytes(xmlstring);
                webRequest.ContentLength = bytes.Length;
                webRequest.PreAuthenticate = true;

                Stream stream = webRequest.GetRequestStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Close();

                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                   // Program.WriteToConsole("WebResponse Status Code: " + webResponse.StatusCode.ToString() + "; Description: " + webResponse.StatusDescription);
                    blnSubmitSuccessful = Convert.ToBoolean(webResponse.StatusCode == System.Net.HttpStatusCode.OK);
                    Program.WriteToConsole("TranserLineID: " + _transferLineId.ToString() + " XML Submission Result: " + webResponse.StatusCode.ToString());
                }
            }

            return blnSubmitSuccessful;
        }


        /// <summary>Prepares request xml</summary>
        /// <param name="TransferEntryRequest">TransferEntryRequest as object</param>
        /// <returns>request xml as string</returns>
        public string GetXMLString(TransferEntryRequest transfer)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(TransferEntryRequest));
            StringBuilder stringBuilder = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(stringBuilder))
            {
                serializer.Serialize(stringWriter, transfer);
                xmlstring = stringBuilder.ToString();
            }

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlstring);

            xmlstring = xml["Body"].InnerXml;

            xmlstring = xmlstring.Replace("MIMSGoodsMovement", "ns:MIMSGoodsMovement");

            // to cover xml message with SAP soap xml envelope
            xmlstring = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns=""http://sn.com/xi/SAPECC/GoodsMovement/1"">"
             + @"<soapenv:Header/><soapenv:Body><ns:MIMSGoodsMovement>"
             + xmlstring + "</ns:MIMSGoodsMovement></soapenv:Body></soapenv:Envelope>";

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["saveXMLFile"].ToString()))
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlstring);

                string strFileName = "TransferLine_" + _transferLineId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
                string strFolder = ConfigurationManager.AppSettings["XmlFolder"];
                xdoc.Save(strFolder + strFileName);
            }

            return xmlstring;
        }

    } //// end class
}
