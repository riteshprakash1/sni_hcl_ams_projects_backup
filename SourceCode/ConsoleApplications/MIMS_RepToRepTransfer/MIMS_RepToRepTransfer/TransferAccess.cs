﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MIMS_RepToRepTransfer.Entity;
using System.Collections.ObjectModel;

namespace MIMS_RepToRepTransfer
{
    class TransferAccess
    {

        /// <summary>Initializes a new instance of the <see cref="TransferAccess"/> class.</summary>
        public TransferAccess()
        {
            System.Diagnostics.Debug.WriteLine("TransferAccess");
        }

        /// <summary>Get Approved UnProcessed TranferLines By TransferID</summary>
        /// <param name="intTransferID">the Transfer ID</param>
        /// <returns>List of Transfer Lines number</returns>
        public List<Transfer> GetApprovedUnProcessedTranferLinesByTransferID(Int64 intTransferID)
        {
            List<Transfer> _transfers = new List<Transfer>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@TransferID", intTransferID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetApprovedUnProcessedTranferLinesByTransferID", parameters);
            foreach (DataRow r in dt.Rows)
            {
                Transfer rTansfer = this.GetTransferEntityFromDataRow(r);
                _transfers.Add(rTansfer);
            }

            return _transfers;
        }

        /// <summary>Get Approved UnProcessed TranferLines By TransferLineID</summary>
        /// <param name="intTransferLineID">the Transfer ID</param>
        /// <returns>List of Transfer Lines number</returns>
        public List<Transfer> GetApprovedUnProcessedTranferLinesByTransferLineID(Int64 intTransferLineID)
        {
            List<Transfer> _transfers = new List<Transfer>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@TransferLineID", intTransferLineID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetApprovedUnProcessedTranferLinesByTransferLineID", parameters);
            foreach (DataRow r in dt.Rows)
            {
                Transfer rTansfer = this.GetTransferEntityFromDataRow(r);
                _transfers.Add(rTansfer);
            }

            return _transfers;
        }


        protected Transfer GetTransferEntityFromDataRow (DataRow r)
        {
            Transfer rTansfer = new Transfer();

            rTansfer.TransferID = Convert.ToInt64(r["TransferId"].ToString());
            rTansfer.TransferLineID = Convert.ToInt64(r["TransferLineId"].ToString());
            rTansfer.TransferFromRepID = r["FromSalesRepID"].ToString();
            rTansfer.TransferToRepID = r["ToSalesRepID"].ToString();
            rTansfer.MaterialID = r["MaterialID"].ToString();
            rTansfer.Qty = Convert.ToInt32(r["Quantity"].ToString());
            rTansfer.Batch = r["BatchNumber"].ToString();
            rTansfer.LC_Info = new Lotcontrol_Info(r["SerialNumber"].ToString());
            rTansfer.Status = r["Status"].ToString();
            rTansfer.StatusID = Convert.ToInt32(r["StatusID"].ToString());
            rTansfer.SentToSAP = r["SentToSAP"].ToString();

            return rTansfer;
        }


        /// <summary> Get all unprocessed transfers</summary>
        /// <returns>List of Transfers</returns>
        public List<Transfer> GetApprovedUnProcessedTranferLines()
        {
            System.Diagnostics.Debug.WriteLine("GetApprovedUnProcessedTranferLines");
            List<Transfer> _transfers = new List<Transfer>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetApprovedUnProcessedTranferLines", parameters);

            foreach (DataRow r in dt.Rows)
            {
                Transfer rTansfer = this.GetTransferEntityFromDataRow(r);
                _transfers.Add(rTansfer);
            }

            return _transfers;
        }

        /// <summary>Get TransferEntryRequest from the Transfer </summary>
        /// <param name="_transfer">the Transfer object</param>
        /// <returns>Transfer Entry Request object</returns>
        public TransferEntryRequest GetTransferEntryRequest(Transfer _transfer)
        {
            TransferEntryRequest request = new TransferEntryRequest();
            request.Transfer = _transfer;
            return request;
        }

        /// <summary>Updates the SentToSap of MIMS_InventoryTransferLines table for the TransferLineID</summary>
        /// <param name="intTransferLineID">MIMS Transfer Line ID</param>
        /// <param name="strExportXml">The xml that was sent to SAP</param>
        public void UpdateTransferLineSentToSap(Int64 intTransferLineID, string strExportXml)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@TransferLineID", intTransferLineID));
            parameters.Add(new SqlParameter("@ExportXML", strExportXml));

            int intCnt = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateTransferLineSentToSap", parameters);
        }


    }  ////end class
}
