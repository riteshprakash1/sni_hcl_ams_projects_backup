﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Net.Mail;
using MIMS_RepToRepTransfer.Entity;

namespace MIMS_RepToRepTransfer
{
    class Program
    {
        static void Main(string[] args)
        {

            string strLogMsg = string.Empty;
            bool blnContinueExecution = true;
            string strExecutionLocation = "Program MAIN method: ";
            int intTransferLinesProcessed = 0;
            ExportProcessing processing = new ExportProcessing();
            string strProcessingType = ConfigurationManager.AppSettings["ProceesingType"];

            // Check for EndLast Command; to end the last processing which is generating emails and will not process the transfers until it is ended
            if ((args.Length >= 1) && (args[0].ToLower() == "endlast"))
            {
                EndLastProcessing();
                blnContinueExecution = false;
            }

            if (blnContinueExecution)
            {
                //Check to if the LastProcessing Ended and is not still in the 'Starting' status
                blnContinueExecution = Program.LastProcessingFinished();
            }

            if (blnContinueExecution)
            {
                try
                {
                    string strArguments = string.Empty;
                    foreach (string strArg in args)
                    {
                        strArguments += strArg + "; ";
                    }

                    processing.ProcessingArgument = strArguments;
                    processing.ProcessingType = strProcessingType;

                    processing = ExportProcessing.InsertExportProcessing(processing);
                    processing = ExportProcessing.GetExportProcessingByProcessingID(processing.ProcessingID);

                    DateTime startTime = DateTime.Now;

                    //Program.WriteToConsole(startTime.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": Starting ExecuteProcess");
                    Program.WriteToLogFile("Starting Process", false, true);

                    //Instantiate the ProcessTransfer and pass the ProcessingID so we can tracking each transfer submission
                    ProcessTransfer xfer = new ProcessTransfer(processing.ProcessingID);

                    // If there is one arg it should be an TransferID which will process just that Transfer by TransferID
                    if (args.Length >= 1)
                    {
                        switch (args[0].ToLower())
                        {
                            case "transferlineid":
                                try
                                {
                                    if (args.Length >= 2)
                                    {
                                        Int64 intTransferLineID = Convert.ToInt64(args[1]);
                                        intTransferLinesProcessed = xfer.ProcessTransferLinesByTransferLineID(intTransferLineID);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogException.HandleException(ex, strExecutionLocation);
                                    Program.WriteToConsole("Error: " + strExecutionLocation + ": " + ex.Message);
                                }
                                break;
                            case "transferid":
                                try
                                {
                                    if (args.Length >= 2)
                                    {
                                        Int64 intTransferID = Convert.ToInt64(args[1]);
                                        intTransferLinesProcessed = xfer.ProcessTransferLinesByTransferID(intTransferID);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogException.HandleException(ex, strExecutionLocation);
                                    Program.WriteToConsole("Error: " + strExecutionLocation + ": " + ex.Message);
                                }
                                break;
                            case "?":
                            case "help":
                                Console.WriteLine("Description".PadRight(35) + "Argument 1".PadRight(25) + "Argument 2".PadRight(30) + "Example");
                                Console.WriteLine("Transfer by TransferLineID".PadRight(35) + "TransferLineID".PadRight(25) + "2563 (ID of TransferLine)".PadRight(30) + "MIMS_RepToRepTransfer.exe TransferLineID 2563");
                                Console.WriteLine("Transfer by TransferID".PadRight(35) + "TransferID".PadRight(25) + "1942 (ID of Transfer)".PadRight(30) + "MIMS_RepToRepTransfer.exe TransferID 1942");
                                Console.WriteLine("End Last Processing".PadRight(35) + "EndLast".PadRight(25) + "".PadRight(30) + "MIMS_RepToRepTransfer.exe EndLast");
                                Console.WriteLine("All Unprocessed Transfers".PadRight(35) + "".PadRight(25) + "".PadRight(30) + "MIMS_RepToRepTransfer.exe");
                                break;
                            case "allunprocessed":
                                intTransferLinesProcessed = xfer.GetApprovedUnProcessedTranferLines();
                                break;
                            case "test":
                                xfer.TestTransferProcess();
                                intTransferLinesProcessed = 0;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        intTransferLinesProcessed = xfer.GetApprovedUnProcessedTranferLines();
                    }

                    Program.WriteToConsole(strLogMsg);
                    strExecutionLocation = "Program Class - Transfer processing completed: ";

                    DateTime endTime = DateTime.Now;
                    TimeSpan duration = endTime.Subtract(startTime);
                    string strRunTime = "Execution Time: " + duration.Minutes.ToString() + " minutes and " + duration.Seconds.ToString() + " seconds and " + duration.Milliseconds.ToString() + " milliseconds";

                    //int y = 0;
                    //int x = 5 / y;

                    Program.WriteToConsole(strRunTime);
                    Program.WriteToLogFile(" " + strLogMsg + " " + strRunTime, true, false);

                    processing.Duration = strRunTime;
                    processing.ProcessingStatus = "completed";
                    processing.ProcessingMessage = intTransferLinesProcessed.ToString() + " transfer lines processed";
                }
                catch (Exception ex)
                {
                    processing.ProcessingStatus = "failed";

                    LogException.HandleException(ex, strExecutionLocation);
                    Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                    Program.WriteToLogFile(" " + strLogMsg, true, false);
                }
                finally
                {
                    int intUpdateCnt = ExportProcessing.UpdateExportProcessing(processing);
                }
            }  ////blnLastProcessingFinished       
        }


        /// <summary>Ends the last processing by changing the status to anything but 'starting'.</summary>
        public static void EndLastProcessing()
        {
            ExportProcessing processing = ExportProcessing.GetLastExportProcessingByType(ConfigurationManager.AppSettings["strProcessingType"]);
            processing.ProcessingStatus = "Forced End";
            int cnt = ExportProcessing.UpdateExportProcessing(processing);
        }

        /// <summary>Sends an email with the body content from strBody.</summary>
        /// <param name="strBody">The email body.</param>
        /// <param name="strSubject">The email Subject.</param>
        public static void SendEmail(string strBody, string strSubject)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"]);

            string EmailToList = ConfigurationManager.AppSettings["logEmailAddresses"];
            string[] recipients = EmailToList.Split(';');

            foreach (string sTo in recipients)
            {
                Program.WriteToConsole("To: " + sTo);
                message.To.Add(new MailAddress(sTo));
            }

            message.IsBodyHtml = true;
            message.Subject = strSubject;

            message.Body = strBody;

            SmtpClient client = new SmtpClient();

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Program.WriteToConsole("SendEmail: " + excm.Message);
                LogException.HandleException(excm, "SendEmail");
            }
        }


        /// <summary>Writes a message in the named file.</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static void WriteToOutputFile(string strMsg, string strFullFileName)
        {
            if (!File.Exists(strFullFileName))
                Program.CreateNamedFile(strFullFileName);
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strFullFileName);
                using (System.IO.StreamWriter str = fileLog.AppendText())
                {
                    str.Write(strMsg);
                    str.Flush(); //// close log file
                }
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strMsg);
                Console.WriteLine("Error Writing Log: " + ex.Message);
            }
        }

        /// <summary>Writes a message in the named file.</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="blnWriteLine">true/false write line.</param>
        /// <param name="blnWriteDate">true/false write date/time.</param>
        public static void WriteToLogFile(string strMsg, bool blnWriteLine, bool blnWriteDate)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["WriteToProcessingLog"]))
            {
                string strFullFileName = ConfigurationManager.AppSettings["ProceesingLogFile"];

                if (!File.Exists(strFullFileName))
                    Program.CreateNamedFile(strFullFileName);
                try
                {
                    System.IO.FileInfo fileLog = new System.IO.FileInfo(strFullFileName);
                    using (System.IO.StreamWriter str = fileLog.AppendText())
                    {
                        string strTime = string.Empty;
                        if (blnWriteDate)
                            strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";

                        if (blnWriteLine)
                            str.WriteLine(strTime + strMsg + "\n");
                        else
                            str.Write(strTime + strMsg);

                        str.Flush(); //// close log file
                    }
                }
                catch (Exception ex)
                {
                    LogException.HandleException(ex, strMsg);
                    Console.WriteLine("Error Writing Log: " + ex.Message);
                }
            }
        }


        /// <summary>Writes a message in the named file.</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static int WriteToOutputFileCounter(string strMsg, string strFullFileName)
        {
            int intCnt = 0;
            if (!File.Exists(strFullFileName))
                Program.CreateNamedFile(strFullFileName);

            if (strMsg != string.Empty)
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strFullFileName);
                using (System.IO.StreamWriter str = fileLog.AppendText())
                {
                    str.Write(strMsg);
                    str.Flush(); //// close log file
                }

                intCnt = 1;
            }

            return intCnt;
        }

        /// <summary>Creates the file.</summary>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static void CreateNamedFile(string strFullFileName)
        {
            if (!File.Exists(strFullFileName))
            {
                FileStream fs = File.Create(strFullFileName);
                fs.Close();
                fs.Dispose();
            }
            else
            {
                File.Delete(strFullFileName);
                Program.CreateNamedFile(strFullFileName);
            }

        }

        /// <summary>Writes the Message to the Console. Turned on or off by app setting toggle</summary>
        /// <param name="strMsg">The fully qualified file name.</param>
        public static void WriteToConsole(string strMsg)
        {
            bool blnWriteToConsole = Convert.ToBoolean(ConfigurationManager.AppSettings["WriteToConsole"]);

            if (blnWriteToConsole)
                Console.WriteLine(strMsg);
        }


        /// <summary>Checks whether the last processing completed and retruns true/false</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static bool LastProcessingFinished()
        {
            bool didComplete = false;
            string strExecutionLocation = "Running LastProcessingFinished";

            try
            {
                ExportProcessing process = ExportProcessing.GetLastExportProcessingByType(ConfigurationManager.AppSettings["ProceesingType"]);
                Program.WriteToConsole("Last Processing ID: " + process.ProcessingID.ToString() + "; Status: " + process.ProcessingStatus);

                if (process.ProcessingStatus.ToLower() != "starting")
                {
                    didComplete = true;
                }
                else
                {
                    string strSubject = ConfigurationManager.AppSettings["ProgramName"] + " - Last Processing Still Running";

                    string strBody = "<table width='800px' border='1'>";
                    strBody += "<tr><td width='20%'>Processing ID: </td><td>" + process.ProcessingID.ToString() + "</td></tr>";
                    strBody += "<tr><td>Server: </td><td>" + ConfigurationManager.AppSettings["ServerName"] + "</td></tr>";
                    strBody += "<tr><td>Environment: </td><td>" + ConfigurationManager.AppSettings["Environment"] + "</td></tr>";
                    strBody += "<tr><td>Processing Status: </td><td>" + process.ProcessingStatus + "</td></tr>";
                    strBody += "<tr><td>Start Time: </td><td>" + process.StartDate + "</td></tr>";
                    strBody += "<tr><td>Processing Message: </td><td>" + process.ProcessingMessage + "</td></tr>";
                    strBody += "<tr><td>Processing Argument: </td><td>" + process.ProcessingArgument + "</td></tr>";
                    strBody += "<tr><td>Last Updated: </td><td>" + process.LastUpdated + "</td></tr>";
                    strBody += "<tr><td colspan='2'><br />The status of the last processing must be changed to anything other than 'starting' for future processings to be executed.<br />";
                    strBody += "<tr><td colspan='2'>This status update must be done in the MIMS_ExportProcessing SQL database table. <br />";
                    strBody += "<tr><td colspan='2'>Alternatively, you can update the status by manually running the Console Application by opening a command window, ";
                    strBody += "going to the application folder, type in MIMS_RepToRepTransfer.exe endlast and hit enter <br />";
                    strBody += "You may choose to wait and see if the current last processing completes executing.</td></tr>";
                    strBody += "</table>";

                    Program.SendEmail(strBody, strSubject);
                }

            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);

            }

            return didComplete;
        }


    } //// end class
}
