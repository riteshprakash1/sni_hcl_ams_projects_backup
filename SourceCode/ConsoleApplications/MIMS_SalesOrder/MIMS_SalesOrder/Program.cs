﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Net.Mail;
using MIMS_SalesOrder.Entity;

namespace MIMS_SalesOrder
{
    class Program
    {
        /// <summary>Calls the PrepareSalesOrder</summary>
        /// <param name="args">not used</param>
        static void Main(string[] args)
        {
            string strLogMsg = string.Empty;
            bool blnContinueExecution = true;
            string strExecutionLocation = "Program MAIN method: ";
            int intOrdersProcessed = 0;
            ExportProcessing processing = new ExportProcessing();
            string strProcessingType = ConfigurationManager.AppSettings["ProceesingType"];

            if ((args.Length >= 1) && (args[0].ToLower() == "endlast"))
            {
                EndLastProcessing();
                blnContinueExecution = false;
            }

            if (blnContinueExecution)
            {
                blnContinueExecution = Program.LastProcessingFinished();
//                Program.WriteToConsole("LastProcessingFinished: " + blnContinueExecution.ToString());
            }

            if (blnContinueExecution)
            {
                try
                {
                    string strArguments = string.Empty;
                    foreach (string strArg in args)
                    {
                        strArguments += strArg + "; ";
                    }

                    processing.ProcessingArgument = strArguments;
                    processing.ProcessingType = strProcessingType;

                    processing = ExportProcessing.InsertExportProcessing(processing);
                    processing = ExportProcessing.GetExportProcessingByProcessingID(processing.ProcessingID);

                    DateTime startTime = DateTime.Now;

                    //Program.WriteToConsole(startTime.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": Starting ExecuteProcess");
                    Program.WriteToLogFile("Starting Process", false, true);

                    //Instantiate the ProcessSalesOrder and pass the ProcessingID so we can tracking each order submission
                    ProcessSalesOrder so = new ProcessSalesOrder(processing.ProcessingID);

                    // If there is one arg it should be an OrderID which will process just that SalesOrder by Order ID
                    if (args.Length >= 1)
                    {
                        switch (args[0].ToLower())
                        {
                            case "orderid":
                                try
                                {
                                    if (args.Length >= 2)
                                    {
                                        Int64 intOrderID = Convert.ToInt64(args[1]);
                                        intOrdersProcessed = so.ProcessSalesOrderBySalesOrderID(intOrderID);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogException.HandleException(ex, strExecutionLocation);
                                    Program.WriteToConsole("Error: " + strExecutionLocation + ": " + ex.Message);
                                }
                                break;
                            case "?":
                                Console.WriteLine("Description".PadRight(30) + "Argument 1".PadRight(25) + "Argument 2".PadRight(25) + "Example");
                                Console.WriteLine("Individual Order".PadRight(30) + "OrderID".PadRight(25) + "2563 (ID of Order)".PadRight(25) + "MIMS_SalesOrder.exe OrderID 2563");
                                Console.WriteLine("End Last Processing".PadRight(30) + "EndLast".PadRight(25) + "".PadRight(25) + "MIMS_SalesOrder.exe EndLast");
                                Console.WriteLine("All Unprocessed Orders".PadRight(30) + "".PadRight(25) + "".PadRight(25) + "MIMS_SalesOrder.exe");
                                break;
                            case "allunprocessed":
                                intOrdersProcessed = so.GetAllUnProcessedSalesOrderNumbers();
                                break;
                            case "test":
                                so.TestWebRequest();
                                intOrdersProcessed = 0;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        intOrdersProcessed = so.GetAllUnProcessedSalesOrderNumbers();
                    }

                    Program.WriteToConsole(strLogMsg);
                    strExecutionLocation = "Program Class - Order processing completed: ";

                    DateTime endTime = DateTime.Now;
                    TimeSpan duration = endTime.Subtract(startTime);
                    string strRunTime = "Execution Time: " + duration.Minutes.ToString() + " minutes and " + duration.Seconds.ToString() + " seconds and " + duration.Milliseconds.ToString() + " milliseconds";

                    //int y = 0;
                    //int x = 5 / y;

                    Program.WriteToConsole(strRunTime);
                    Program.WriteToLogFile(" " + strLogMsg + " " + strRunTime, true, false);

                    processing.Duration = strRunTime;
                    processing.ProcessingStatus = "completed";
                    processing.ProcessingMessage = intOrdersProcessed.ToString() + " orders processed";
                }
                catch (Exception ex)
                {
                    processing.ProcessingStatus = "failed";

                    LogException.HandleException(ex, strExecutionLocation);
                    Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                    Program.WriteToLogFile(" " + strLogMsg, true, false);
                }
                finally
                {
                    int intUpdateCnt = ExportProcessing.UpdateExportProcessing(processing);
                }
            }  ////blnLastProcessingFinished
        }


        /// <summary>Ends the last processing by changing the status to anything but 'starting'.</summary>
        public static void EndLastProcessing()
        {
            ExportProcessing processing = ExportProcessing.GetLastExportProcessingByType(ConfigurationManager.AppSettings["ProceesingType"]);
            processing.ProcessingStatus = "Forced End";
            int cnt = ExportProcessing.UpdateExportProcessing(processing);

        }


        /// <summary>Sends an email with the body content from strBody.</summary>
        /// <param name="strBody">The email body.</param>
        /// <param name="strSubject">The email Subject.</param>
        public static void SendEmail(string strBody, string strSubject)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"]);

            string EmailToList = ConfigurationManager.AppSettings["logEmailAddresses"];
            string[] recipients = EmailToList.Split(';');

            foreach (string sTo in recipients)
            {
                Program.WriteToConsole("To: " + sTo);
                message.To.Add(new MailAddress(sTo));
            }

            message.IsBodyHtml = true;
            message.Subject = strSubject;

            message.Body = strBody;

            SmtpClient client = new SmtpClient();

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Program.WriteToConsole("SendEmail: " + excm.Message);
                LogException.HandleException(excm, "SendEmail");
            }
        }


        /// <summary>Writes a message in the named file.</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static void WriteToOutputFile(string strMsg, string strFullFileName)
        {
            if (!File.Exists(strFullFileName))
                Program.CreateNamedFile(strFullFileName);
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strFullFileName);
                using (System.IO.StreamWriter str = fileLog.AppendText())
                {
                    str.Write(strMsg);
                    str.Flush(); //// close log file
                }
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strMsg);
                Console.WriteLine("Error Writing Log: " + ex.Message);
            }
        }

        /// <summary>Writes a message in the named file.</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="blnWriteLine">true/false write line.</param>
        /// <param name="blnWriteDate">true/false write date/time.</param>
        public static void WriteToLogFile(string strMsg, bool blnWriteLine, bool blnWriteDate)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["WriteToProcessingLog"]))
            {
                string strFullFileName = ConfigurationManager.AppSettings["ProceesingLogFile"];

                if (!File.Exists(strFullFileName))
                    Program.CreateNamedFile(strFullFileName);
                try
                {
                    System.IO.FileInfo fileLog = new System.IO.FileInfo(strFullFileName);
                    using (System.IO.StreamWriter str = fileLog.AppendText())
                    {
                        string strTime = string.Empty;
                        if (blnWriteDate)
                            strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";

                        if (blnWriteLine)
                            str.WriteLine(strTime + strMsg + "\n");
                        else
                            str.Write(strTime + strMsg);

                        str.Flush(); //// close log file
                    }
                }
                catch (Exception ex)
                {
                    LogException.HandleException(ex, strMsg);
                    Console.WriteLine("Error Writing Log: " + ex.Message);
                }
            }
        }


        /// <summary>Writes a message in the named file.</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static int WriteToOutputFileCounter(string strMsg, string strFullFileName)
        {
            int intCnt = 0;
            if (!File.Exists(strFullFileName))
                Program.CreateNamedFile(strFullFileName);

            if (strMsg != string.Empty)
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strFullFileName);
                using (System.IO.StreamWriter str = fileLog.AppendText())
                {
                    str.Write(strMsg);
                    str.Flush(); //// close log file
                }

                intCnt = 1;
            }

            return intCnt;
        }

        /// <summary>Creates the file.</summary>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static void CreateNamedFile(string strFullFileName)
        {
            if (!File.Exists(strFullFileName))
            {
                FileStream fs = File.Create(strFullFileName);
                fs.Close();
                fs.Dispose();
            }
            else
            {
                File.Delete(strFullFileName);
                Program.CreateNamedFile(strFullFileName);
            }

        }

        /// <summary>Writes the Message to the Console. Turned on or off by app setting toggle</summary>
        /// <param name="strMsg">The fully qualified file name.</param>
        public static void WriteToConsole(string strMsg)
        {
            bool blnWriteToConsole = Convert.ToBoolean(ConfigurationManager.AppSettings["WriteToConsole"]);

            if (blnWriteToConsole)
                Console.WriteLine(strMsg);
        }


        /// <summary>Checks whether the last processing completed and retruns true/false</summary>
        /// <param name="strMsg">The message to write to the named file.</param>
        /// <param name="strFullFileName">The fully qualified file name.</param>
        public static bool LastProcessingFinished()
        {
            bool didComplete = false;
            string strExecutionLocation = "Running LastProcessingFinished";

            try
            {
                ExportProcessing process = ExportProcessing.GetLastExportProcessingByType(ConfigurationManager.AppSettings["ProceesingType"]);
                Program.WriteToConsole("Last Processing ID: " + process.ProcessingID.ToString() + "; Status: " + process.ProcessingStatus);

                if (process.ProcessingStatus.ToLower() != "starting")
                {
                    didComplete = true;
                }
                else
                {
                    string strSubject = ConfigurationManager.AppSettings["ProgramName"] + " - Last Processing Still Running";

                    string strBody = "<table width='800px' border='1'>";
                    strBody += "<tr><td width='20%'>Processing ID: </td><td>" + process.ProcessingID.ToString() + "</td></tr>";
                    strBody += "<tr><td>Server: </td><td>" + ConfigurationManager.AppSettings["ServerName"] + "</td></tr>";
                    strBody += "<tr><td>Environment: </td><td>" + ConfigurationManager.AppSettings["Environment"] + "</td></tr>";
                    strBody += "<tr><td>Processing Status: </td><td>" + process.ProcessingStatus + "</td></tr>";
                    strBody += "<tr><td>Start Time: </td><td>" + process.StartDate + "</td></tr>";
                    strBody += "<tr><td>Processing Message: </td><td>" + process.ProcessingMessage + "</td></tr>";
                    strBody += "<tr><td>Processing Argument: </td><td>" + process.ProcessingArgument + "</td></tr>";
                    strBody += "<tr><td>Last Updated: </td><td>" + process.LastUpdated + "</td></tr>";
                    strBody += "<tr><td colspan='2'><br />The status of the last processing must be changed to anything other than 'starting' for future processings to be executed.<br />";
                    strBody += "<tr><td colspan='2'>This status update must be done in the MIMS_OrderExport SQL database table. <br />";
                    strBody += "<tr><td colspan='2'>Alternatively, you can update the status by manually running the Console Application by opening a command window, going to the application folder, type in MIMS_SalesOrder.exe endlast and hit enter <br />";
                    strBody += "You may choose to wait and see if the current last processing completes executing.</td></tr>";
                    strBody += "</table>";

                    Program.SendEmail(strBody, strSubject);
                }

            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);

            }

            return didComplete;
        }


    }
}
