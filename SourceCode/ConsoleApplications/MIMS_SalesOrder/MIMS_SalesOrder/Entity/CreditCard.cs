﻿using System;
using System.Xml;
using System.Xml.Serialization;


namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holdes the data required for 
    /// CreditCard section in the SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    public class CreditCard
    {
        /// <summary>Default constructor</summary>
        public CreditCard()
        { }

        #region local variables
        private string _ccNumber = string.Empty;
        private string _holderName = string.Empty;
        private string _cardType = string.Empty;
        private string _expMonth = string.Empty;
        private string _expYear = string.Empty;
        #endregion

        /// <summary>Gets or sets credit card number</summary>
        [XmlElement("CCNUMBER")]
        public string CCNumber
        {
            get { return _ccNumber;}
            set { _ccNumber = value;}
        }

        /// <summary>Gets or sets credit card holder name</summary>
        [XmlElement("HOLDERNAME")]
        public string HolderName
        {
            get { return _holderName; }
            set { _holderName = value; }
        }

        /// <summary>Gets or sets the credit card type. Some times data is comming as none. When none comes it is set to empty.</summary>
        [XmlElement("CARDTYPE")]
        public string CardType
        {
            get { return _cardType; }
            set {_cardType = value; }
        }

        /// <summary>
        /// Gets or sets the credit card expiration date
        /// Ensuring that when there is no credit card number expiration date is set to empty.
        /// </summary>
        [XmlElement("EXPMONTH")]
        public string ExpMonth
        {
            get {return _expMonth; }
            set { _expMonth = value; }
        }

        /// <summary>
        /// Gets or sets the credit card expiration year
        /// Ensuring that when there is no credit card number expiration year is set to empty.
        /// </summary>
        [XmlElement("EXPYEAR")]
        public string ExpYear
        {
            get { return _expYear;}
            set { _expYear = value; }
        }

    }
}
