﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIMS_SalesOrder.Entity
{
    public class SalesOrder
    {

        public Int64 _orderId;
        public int intStatusID;


        public Int64 OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }

        public int StatusID
        {
            get { return intStatusID; }
            set { intStatusID = value; }
        }

    }
}
