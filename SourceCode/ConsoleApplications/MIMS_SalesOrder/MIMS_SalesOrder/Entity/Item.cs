﻿using System;
using System.Xml.Serialization;

namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holds order items required for 
    /// Items section in the SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    [XmlRoot("Item")]
    public class Item
    {

        /// <summary>Default constructor</summary>
        public Item()
        { }

        #region local variables
        private string _itemNo = string.Empty;
        private string _itemFlag = string.Empty;
        private string _material = string.Empty;
        private string _qty = string.Empty;
        private string _unit = string.Empty;
        private string _lot = string.Empty;
        private string _serialNum = string.Empty;
        private string _price = string.Empty;
        private string _keConsumed = string.Empty;
        private string _kbShipto = string.Empty;
        private string _deliveryBlock = string.Empty;
        private string _keText = string.Empty;
        private string _InvMonID = string.Empty;
        private string _Plant = string.Empty;
        private string _PricingType = string.Empty;
        private string _ReasonCode = string.Empty;
        private string _ReplenishFlag = string.Empty;
        private string _ReplenishType = string.Empty;

        #endregion

        /// <summary>Gets or sets Item Number Sequential</summary>
        [XmlElement("ITEMNO")]
        public string ItemNo
        {
            get { return _itemNo; }
            set { _itemNo = value; }
        }

        /// <summary>
        /// Gets or sets Item flag
        /// </summary>        
        [XmlElement("ITEMFLAG")]
        public string ItemFlag
        {
            get { return _itemFlag; }
            set { _itemFlag = value; }
        }

        /// <summary>Gets or sets Item number, In case of alpha numeric number, charactors are converted to upper case</summary>
        [XmlElement("MATERIAL")]
        public string Material
        {
            // mail from peter to change to Upper bcoz there is chans to get the alpha numeric data 8th Nov 2010
            get { return _material.ToUpper(); }
            set { _material = value; }
        }

        /// <summary>Gets or sets Item quantity</summary>
        [XmlElement("QTY")]
        public string QTY
        {
            get { return _qty; }
            set { _qty = value; }
        }

        /// <summary>Get or sets Item unit</summary>
        [XmlElement("UNIT")]
        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        /// <summary>Gest or sets Item lot</summary>
        [XmlElement("LOT")]
        public string Lot
        {
            get { return _lot; }
            set { _lot = value; }
        }

        /// <summary>Gest or sets Item lot</summary>
        [XmlElement("SERIALNUM")]
        public string SerialNum
        {
            get { return _serialNum; }
            set { _serialNum = value; }
        }

        /// <summary>Get or sets Item price</summary>
        [XmlElement("PRICE")]
        public string Price
        {
            get { return _price; }
            set { _price = value; }
        }

        /// <summary>Get or sets KE Consumed</summary>
        [XmlElement("KECONSUMED")]
        public string KEConsumed
        {
            get { return _keConsumed; }
            set { _keConsumed = value; }
        }

        /// <summary>Gets or sets KB Shipto</summary>
        [XmlElement("KBSHIPTO")]
        public string KBShipto
        {
            get { return _kbShipto; }
            set { _kbShipto = value; }
        }

        /// <summary>Get or sets Item delivery block</summary>
        [XmlElement("DELIVERYBLOCK")]
        public string DeliveryBlock
        {
            get { return _deliveryBlock; }
            set { _deliveryBlock = value; }
        }

        /// <summary>Get or sets Invoice Monitor ID</summary>
        [XmlElement("INVMONID")]
        public string InvMonId
        {
            get { return _InvMonID; }
            set { _InvMonID = value; }
        }

        /// <summary>Get or sets Plant</summary>
        [XmlElement("PLANT")]
        public string Plant
        {
            get { return _Plant; }
            set { _Plant = value; }
        }

        /// <summary>Get or sets Pricing Type</summary>
        [XmlElement("PRICINGTYPE")]
        public string PricingType
        {
            get { return _PricingType; }
            set { _PricingType = value; }
        }

        /// <summary>Get or sets Reason Code</summary>
        [XmlElement("REASONCODE")]
        public string ReasonCode
        {
            get { return _ReasonCode; }
            set { _ReasonCode = value; }
        }

        /// <summary>Get or sets Replenish Flag</summary>
        [XmlElement("REPLENISHFLAG")]
        public string ReplenishFlag
        {
            get { return _ReplenishFlag; }
            set { _ReplenishFlag = value; }
        }

        /// <summary>Get or sets Replenish Type</summary>
        [XmlElement("REPLENISHTYPE")]
        public string ReplenishType
        {
            get { return _ReplenishType; }
            set { _ReplenishType = value; }
        }

        /// <summary>Get or sets KE Text</summary>
        [XmlElement("KETEXT")]
        public string KEText
        {
            get { return _keText; }
            set { _keText = value; }
        }

    }
}
