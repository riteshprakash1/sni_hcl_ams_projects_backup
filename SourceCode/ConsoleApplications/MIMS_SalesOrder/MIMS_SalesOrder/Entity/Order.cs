﻿using System;
using System.Collections.Generic;

using System.Xml.Serialization;

namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holdes Order required for 
    /// Order section in the SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary> 
    [Serializable]
    public class Order
    {

        /// <summary>Default constructor</summary>
        public Order()
        { }

        #region local variables
        private Header _header;
        private List<Item> _items;
        private string _orderType = string.Empty;
        private string _sender = string.Empty;
        #endregion

        /// <summary>Gets or sets OrderType</summary>        
        [XmlElement("I_ORDERTYPE")]
        public string OrderType
        {
            get { return _orderType; }
            set { _orderType = value; }
        }

        /// <summary>Gets or sets Sender</summary>        
        [XmlElement("SENDER")]
        public string Sender
        {
            get { return _sender; }
            set { _sender = value; }
        }

        /// <summary>Gets of sets Header object</summary>
        [XmlElement("I_HEADER")]
        public Header Header
        {
            get { return _header; }
            set { _header = value; }
        }

        /// <summary>Gets or sets the Item details as object</summary>
        [XmlArray("IT_ITEM")]
        [XmlArrayItem("item")]
        public List<Item> Items
        {
            get { return _items; }
            set { _items = value; }
        }
    }
}
