﻿using System;
using System.Xml.Serialization;


namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holdes KE Partner required for 
    /// KEPartner section in the SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    public class KEPartner
    {
        /// <summary>Default constructor</summary>
        public KEPartner()
        { }

        #region local variables
        private string _soldTo = string.Empty;
        private string _shipTo = string.Empty;
        private string _orderingParty = string.Empty;
        private string _surgeonName = string.Empty;
        private string _surgeonNbr = string.Empty;
        private string _patientID = string.Empty;
        private string _surgeryDate = string.Empty;
        #endregion

        /// <summary>Gets or sets Sold to</summary>
        [XmlElement("SOLDTO")]
        public string SoldTo
        {
            get { return _soldTo; }
            set { _soldTo = value; }
        }

        /// <summary>Gets or sets Ship to</summary>
        [XmlElement("SHIPTO")]
        public string ShipTo
        {
            get { return _shipTo; }
            set { _shipTo = value; }
        }

        /// <summary>Gets or sets Ordering Party</summary>
        [XmlElement("ORDERINGPARTY")]
        public string OrderingParty
        {
            get { return _orderingParty; }
            set { _orderingParty = value; }
        }

        /// <summary>Gets or sets Surgeon Name</summary>
        [XmlElement("SURGEONNAME")]
        public string SurgeonName
        {
            get { return _surgeonName; }
            set { _surgeonName = value; }
        }

        /// <summary>Gets or sets Surgeon number</summary>
        [XmlElement("SURGEONNBR")]
        public string SurgeonNbr
        {
            get { return _surgeonNbr; }
            set { _surgeonNbr = value; }
        }

        /// <summary>Gets or sets Patient ID</summary>
        [XmlElement("PATIENTID")]
        public string PatientID
        {
            get { return _patientID; }
            set { _patientID = value; }
        }

        /// <summary>Gets or sets Surgery date</summary>
        [XmlElement("SURGERYDATE")]
        public string SurgeryDate
        {
            get { return _surgeryDate; }
            set { _surgeryDate = value; }
        }

    }
}
