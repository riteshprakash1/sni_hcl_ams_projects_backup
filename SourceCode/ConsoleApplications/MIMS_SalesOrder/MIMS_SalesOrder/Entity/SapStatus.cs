﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIMS_SalesOrder.Entity
{
    /// <summary>Class for handling the SAP status information</summary>
    public class SapStatus
    {

        #region local variables
        private string _crmNumber;
        private bool _errorStatus;
        private bool _processed;
        private string _errorStatustext;
        private string _id;
        #endregion

        /// <summary>Gets or sets the Sales order id</summary>
        public string CRMNumber
        {
            get { return _crmNumber; }
            set { _crmNumber = value; }
        }

        /// <summary>Gets or sets the id</summary>
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>Gets or sets bool value representing SAP status</summary>
        public bool ErrorStatus
        {
            get { return _errorStatus; }
            set { _errorStatus = value; }
        }

        /// <summary>Gets or sets the bool value representing SO placed on SAP</summary>
        public bool Processed
        {
            get { return _processed; }
            set { _processed = value; }
        }

        /// <summary>Gets or sets the SAP status OK for success and failure message</summary>
        public string ErrorStatusText
        {
            get { return _errorStatustext; }
            set { _errorStatustext = value; }
        }

    }
}
