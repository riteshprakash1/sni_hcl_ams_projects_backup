﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holdes root element required for SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    [XmlRoot("Body")]
    public class OrderEntryRequest
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public OrderEntryRequest()
        { }

        #region local variables

        private Order _order;
        #endregion

        /// <summary>
        /// gets or sets sales order details as object
        /// </summary>  
        /// 
        [XmlElement("Z_SALES_ORDER_TO_SAP")]
        public Order Order
        {
            get { return _order; }
            set { _order = value; }
        }
    }
}
