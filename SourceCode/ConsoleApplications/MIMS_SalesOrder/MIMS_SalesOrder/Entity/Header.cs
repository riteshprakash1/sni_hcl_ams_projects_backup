﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holdes the data required for 
    /// Header section in the SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    public class Header
    {
        /// <summary>Default constructor</summary>
        public Header()
        { }

        #region local variables
        private string _referenceNumber = string.Empty;
        private KEPartner _kePartner;
        private KBPartner _kbPartner;
        private string _orderDate = string.Empty;
        private string _shipMethod = string.Empty;
        private string _poNumber = string.Empty;
        private string _deliveryBlock = string.Empty;
        private string _billingBlock = string.Empty;
        private string _hldOrdrNte = string.Empty;
        private string _keText = string.Empty;
        private string _kbText = string.Empty;

        private string _sapNumber = string.Empty; //Release 2 item
        private string _id = string.Empty; //Release 2 item fixing update issue
        private string _salesRep = string.Empty;
        private string _territory = string.Empty;
        private string _email = string.Empty;

        //Added for Visionaire
        private string _phone = string.Empty;
        private string _caseNumber = string.Empty;

        private int _statusID;

        private CreditCard _creditCard;
        #endregion

        /// <summary>Gets or sets the ID : Release 2 item </summary>
        [XmlIgnore]
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>gets or sets Sales rep</summary>        
        [XmlElement("SALESREP")]
        public string Salesrep
        {
            get { return _salesRep; }
            set { _salesRep = value; }
        }

        /// <summary>gets or sets territory</summary>
        [XmlElement("TERRITORY")]
        public string Territory
        {
            get { return _territory; }
            set { _territory = value; }
        }

        /// <summary>gets or sets email</summary>
        [XmlElement("EMAIL")]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        /// <summary>gets or sets phone</summary>
        [XmlElement("PHONE")]
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        /// <summary>Gets or sets the SAPNumber : Release 2 item</summary>
        /// 
        [XmlIgnore]
        public string SAPNumber
        {
            get { return _sapNumber; }
            set { _sapNumber = value; }
        }

        /// <summary>Gets or sets the reference number</summary>
        [XmlElement("REFERENCENUMBER")]
        public string ReferenceNumber
        {
            get { return _referenceNumber; }
            set { _referenceNumber = value; }
        }

        /// <summary>Gets or sets the case number</summary>
        [XmlElement("CASENUMBER")]
        public string CaseNumber
        {
            get { return _caseNumber; }
            set { _caseNumber = value; }
        }


        /// <summary>holds the PO number</summary>
        [XmlElement("PONUMBER")]
        public string PONumber
        {
            get { return _poNumber; }
            set { _poNumber = value; }
        }

        /// <summary>Gets or sets the KE Partner object</summary>
        [XmlElement("KEPARTNER")]
        public KEPartner KEPartner
        {
            get { return _kePartner; }
            set { _kePartner = value; }
        }

        /// <summary>Gets or sets the KB partner object data/summary>
        [XmlElement("KBPARTNER")]
        public KBPartner KBPartner
        {
            get { return _kbPartner; }
            set { _kbPartner = value; }
        }

        /// <summary>Gets or sets the order date data</summary>
        [XmlElement("ORDERDATE")]
        public string OrderDate
        {
            get { return _orderDate; }
            set { _orderDate = value; }
        }

        /// <summary>Gets or sets ship method data</summary>
        [XmlElement("SHIPMETHOD")]
        public string ShipMethod
        {
            get { return _shipMethod; }
            set { _shipMethod = value; }
        }

        /// <summary>Gets or sets delivary block data.
        /// If delivery block is true set the default value to 30
        /// </summary>
        [XmlElement("DELIVERYBLOCK")]
        public string DeliveryBlock
        {
            get { return _deliveryBlock; }
            set { _deliveryBlock = value; }
        }

        /// <summary>Gets or sets the billing block data</summary>
        [XmlElement("BILLINGBLOCK")]
        public string BillingBlock
        {
            get { return _billingBlock; }
            set { _billingBlock = value; }
        }

        /// <summary>Gets or sets order note information</summary>
        [XmlElement("HLDORDRNTE")]
        public string HldOrdrNte
        {
            get { return _hldOrdrNte; }
            set { _hldOrdrNte = value; }
        }

        /// <summary>Gets or sets holds ketext</summary>
        [XmlElement("KETEXT")]
        public string KEText
        {
            get { return _keText; }
            set { _keText = value; }
        }

        /// <summary>Gets or sets KB Text</summary>
        [XmlElement("KBTEXT")]
        public string KBText
        {
            get { return _kbText; }
            set { _kbText = value; }
        }

        /// <summary>Gets or sets credit card object</summary>
        [XmlElement("CREDITCARD")]
        public CreditCard CreditCard
        {
            get { return _creditCard; }
            set { _creditCard = value; }
        }

        /// <summary>Gets or sets the StatusID</summary>
        [XmlIgnore]
        public int StatusID
        {
            get { return _statusID; }
            set { _statusID = value; }
        }

    }
}
