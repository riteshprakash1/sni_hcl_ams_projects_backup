﻿using System;
using System.Xml.Serialization;


namespace MIMS_SalesOrder.Entity
{
    /// <summary>
    /// This class holdes KB Partner required for 
    /// KBPartner section in the SO request XML.
    /// Class is marked as Serializable so that all the public fields are Serialized
    /// </summary>
    [Serializable]
    public class KBPartner
    {

        /// <summary>Default constructor</summary>
        public KBPartner()
        { }

        #region local variables
        private string _shipTo = string.Empty;
        private string _forwardingAgent = string.Empty;
        #endregion

        /// <summary>Gets or sets Ship to</summary>
        [XmlElement("SHIPTO")]
        public string ShipTo
        {
            get { return _shipTo; }
            set { _shipTo = value; }
        }

        /// <summary>Gets or sets ForwardingAgent, if the value is 1, set to empty</summary>
        [XmlElement("FORWARDINGAGENT")]
        public string ForwardingAgent
        {
            get { return _forwardingAgent; }
            set
            {

                if (value.ToString() == "1")
                {
                    _forwardingAgent = string.Empty;
                }
                else
                {
                    _forwardingAgent = value;
                }
            }
        }

    }
}
