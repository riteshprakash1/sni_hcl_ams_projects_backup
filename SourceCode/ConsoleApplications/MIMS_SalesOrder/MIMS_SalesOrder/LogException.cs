﻿// --------------------------------------------------------------
// <copyright file="LogException.cs" company="Smith and Nephew">
//     Copyright (c) 2017 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace MIMS_SalesOrder
{
    using System;
    using System.Data;
    using System.Configuration;
    using System.Diagnostics;
    using System.Net.Mail;

    /// <summary>This class handles and log exceptions when they are thrown.</summary>
    public class LogException
    {
        /// <summary>Initializes a new instance of the <see cref="LogException"/> class.</summary>
        public LogException()
        {
        }

        /// <summary>Handles the exception.</summary>
        /// <param name="ex">The exception object.</param>
        /// <param name="strExecutionLocation">The ExecutionLocation.</param>
        public static void HandleException(Exception ex, string strExecutionLocation)
        {
            // if it is not this exception - handle it
            if (!ex.Message.StartsWith("Thread was being aborted")
                && !ex.Message.StartsWith("Invalid length for a Base-64 char array")
                && !ex.Message.StartsWith("Unable to validate data")
                )
            {
                string strData = String.Empty;

                bool logIt = Convert.ToBoolean(ConfigurationManager.AppSettings["logErrors"]);
                if (logIt)
                {
                    string referer = String.Empty;
                    string logDateTime = DateTime.Now.ToString();
                    string strQuery = ConfigurationManager.AppSettings["ProgramName"];

                    strData = "\r\nENVIRONMENT: " + ConfigurationManager.AppSettings["Environment"];
                    strData += "\r\nExecutionLocation: " + strExecutionLocation;
                    strData += "\r\nSOURCE: " + ex.Source + "\r\nLogDateTime: " + logDateTime;
                    strData += "\r\nMESSAGE: " + ex.Message;
                    strData += "\r\nQUERYSTRING: " + strQuery + "\nTARGETSITE: " + ex.TargetSite;
                    strData += "\r\nSTACKTRACE: " + ex.StackTrace;

                    if (ex.InnerException != null)
                    {
                        strData += "\r\n0 - INNER EXCEPTION : " + LogException.GetInnerMessage(ex.InnerException, 0);
                    }
                    else
                    {
                        strData += "\r\n0 - INNER EXCEPTION : is null";
                    }

                    try
                    {
                        Debug.WriteLine(strData);
                        LogException.OpenLogFile(strData);
                    }
                    catch (Exception excp)
                    {
                        strData += "\r\nMESSAGE: " + excp.Message;
                        strData += "\r\nSTACKTRACE: " + excp.StackTrace;
                    }
                } //// END if(logIt)

                //// Send email notification
                //// Email receiptient list should be delimited by |
                bool sendLog = Convert.ToBoolean(ConfigurationManager.AppSettings["SendLog"]);

                if (sendLog)
                {
                    string strEmails = ConfigurationManager.AppSettings["logEmailAddresses"];
                    if (strEmails.Length > 0)
                    {
                        MailMessage message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"]);

                        string EmailToList = ConfigurationManager.AppSettings["logEmailAddresses"];
                        string[] recipients = EmailToList.Split(';');

                        foreach (string sTo in recipients)
                        {
                            Console.WriteLine("To: " + sTo);
                            message.To.Add(new MailAddress(sTo));
                        }

                        message.IsBodyHtml = false;
                        message.Subject = ConfigurationManager.AppSettings["ProgramName"] + " Error";

                        message.Body = strData;

                        SmtpClient client = new SmtpClient();
                        Console.WriteLine("client: " + client.Host);

                        try
                        {
                            client.Send(message);
                        }
                        catch (Exception excm)
                        {
                            Debug.WriteLine(excm.Message);
                            string strDailyLogFile = ConfigurationManager.AppSettings["DailyLogFile"];

                            LogException.OpenLogFile("SEND ERROR LOG ERROR: " + excm.Message);
                        }
                    }
                } //// end if(SendLog)
            }
        } //// END HandleException(Exception ex)

        /// <summary>Gets the inner message.</summary>
        /// <param name="innerMsg">The inner message.</param>
        /// <param name="cnt">The message count.</param>
        /// <returns>Returns a string containing the Inner Message</returns>
        private static string GetInnerMessage(Exception innerMsg, int cnt)
        {
            string strInnerMsg = "  " + cnt.ToString() + "- Inner MESSAGE: " + innerMsg.Message;
            strInnerMsg += " STACKTRACE: " + innerMsg.StackTrace;

            cnt++;
            if (innerMsg.InnerException != null && cnt < 8)
            {
                strInnerMsg += LogException.GetInnerMessage(innerMsg.InnerException, cnt);
            }
            else
            {
                strInnerMsg += " " + cnt.ToString() + "- Inner Exception is null";
            }

            return strInnerMsg;
        }

        /// <summary>Opens the log file.</summary>
        /// <param name="strMsg">The message.</param>
        /// <param name="strLogFile">The log file folder.</param>
        private static void OpenLogFile(string strMsg)
        {
            string strLogFile = ConfigurationManager.AppSettings["ErrorLogFile"];
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strLogFile);
                System.IO.StreamWriter str = fileLog.AppendText();

                string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
                str.WriteLine(strTime + strMsg + "\n");
                str.Flush(); //// close log file
                str.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Writing Log: " + ex.Message);
            }
        } //// END OpenLogFile
    } //// END class
}
