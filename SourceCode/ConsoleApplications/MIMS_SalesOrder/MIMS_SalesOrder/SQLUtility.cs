﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.Diagnostics;


namespace MIMS_SalesOrder
{
    /// <summary>This is SQL helper class.</summary>
    public sealed class SQLUtility
    {

        /// <summary>Prevents a default instance of the <see cref="SQLUtility"/> class from being created.</summary>
        private SQLUtility()
        {
        }

        /// <summary>Executes a query.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            Debug.WriteLine("I'm HERE");

            string strSqlConn = ConfigurationManager.ConnectionStrings["sqlConnectionExport"].ToString();
            SqlConnection SqlConn = new SqlConnection(strSqlConn);

            Debug.WriteLine("Conn String: " + SqlConn.ConnectionString);
            SqlConn.Open();
            Debug.WriteLine("Conn Opened ");

            using (SqlConn)
            {

                SqlCommand command = new SqlCommand();
                command.Connection = SqlConn;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = storedProcedureName;
                if (parameters.Count > 0)
                {
                    foreach (SqlParameter parameter in parameters)
                    {
                        command.Parameters.Add(parameter);
                    }
                }

                SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
                myDataAdapter.Fill(myDataTable);
                myDataAdapter.Dispose();

            }

            return myDataTable;
        }

        /// <summary>Execute non query and return the parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static Collection<SqlParameter> SqlExecuteNonQueryReturnParameters(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            string strSqlConn = ConfigurationManager.ConnectionStrings["sqlConnectionExport"].ToString();
            SqlConnection SqlConn = new SqlConnection(strSqlConn);

            SqlConn.Open();

            using (SqlConn)
            {
                int intCommandOutcome = 0;

                SqlCommand command = new SqlCommand(storedProcedureName, SqlConn);
                command.CommandType = CommandType.StoredProcedure;
                if (parameters.Count > 0)
                {
                    foreach (SqlParameter parameter in parameters)
                    {
                        command.Parameters.Add(parameter);
                    }
                }

                intCommandOutcome = command.ExecuteNonQuery();

            }

            return parameters;
        }


        /// <summary>Execute non query and return the number of updated records.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static int SqlExecuteNonQueryCount(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            int intCommandOutcome = 0;
            string strSqlConn = ConfigurationManager.ConnectionStrings["sqlConnectionExport"].ToString();
            SqlConnection SqlConn = new SqlConnection(strSqlConn);

            using (SqlConn)
            {
                SqlConn.Open();
                SqlTransaction myTransaction = SqlConn.BeginTransaction();

                SqlCommand command = new SqlCommand(storedProcedureName, SqlConn, myTransaction);
                command.CommandType = CommandType.StoredProcedure;
                if (parameters.Count > 0)
                {
                    foreach (SqlParameter parameter in parameters)
                    {
                        command.Parameters.Add(parameter);
                    }
                }

                intCommandOutcome = command.ExecuteNonQuery();

                myTransaction.Commit();

            }

            return intCommandOutcome;
        }


    }
}
