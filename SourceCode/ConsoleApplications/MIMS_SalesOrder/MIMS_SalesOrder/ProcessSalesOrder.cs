﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;
using System.Threading;
using MIMS_SalesOrder.Entity;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace MIMS_SalesOrder
{
    /// <summary>Class that process all the unprocessed SO and places request in SAP</summary>
    public class ProcessSalesOrder 
    {

        #region Local variables
        SalesOrderAccess _salesOrderAccess =  new SalesOrderAccess();
        Int64 _orderId;
        protected string xmlstring;
        private string strExecutionLocation;
        protected Int64 intProcessingID;

        #endregion

        /// <summary>Initializes a new instance of the <see cref="ProcessSalesOrder"/> class.</summary>
        public ProcessSalesOrder(Int64 intID)
        {
            intProcessingID = intID;
        }

        public void TestWebRequest ()
        {
            try
            {
 
                Console.WriteLine("Start");

                WebRequest webRequest = WebRequest.Create("http://snpidci.life.san:52000/XISOAPAdapter/MessageServlet?channel=:MIMS_DEV00:MobileSalesOrder_Out");

                //webRequest.Method = "POST";
                //webRequest.ContentType = "text/xml; charset=utf-8";

                //webRequest.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["userName"].ToString(),
                //    ConfigurationManager.AppSettings["passWord"].ToString());

                //byte[] bytes = Encoding.ASCII.GetBytes(xmlstring);
                //webRequest.ContentLength = bytes.Length;
                //webRequest.PreAuthenticate = true;

                //Stream stream = webRequest.GetRequestStream();
                //stream.Write(bytes, 0, bytes.Length);
                //stream.Close();

                //using (WebResponse webResponse = webRequest.GetResponse())
                //{
                //    StreamReader streamReader = new StreamReader(webResponse.GetResponseStream());
                //    string strResponseXML = streamReader.ReadToEnd().Trim();
                //}

                //Console.WriteLine("XMLResponse:" + strResponseXML);
                //return strResponseXML;

            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message + "; Inner Message: " + ex.InnerException.Message);
            }
        }

        /// <summary>Gets all the unprocessd SO from export database</summary>
        /// <returns>the number of orders processed</returns>
        public int GetAllUnProcessedSalesOrderNumbers()
        {
            int intOrdersProcessed = 0;
            strExecutionLocation = "Executing GetAllUnProcessedSalesOrderNumbers ";
            try
            {
                List<SalesOrder> orders = _salesOrderAccess.GetAllUnProcessedSalesOrderNumbers();
                intOrdersProcessed = this.ProcessSalesOrderList(orders);
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                throw new Exception("GetAllUnProcessedSalesOrderNumbers Exception");
            }
            return intOrdersProcessed;

        }

        /// <summary>Process the Sales Order by the OrderID</summary>
        /// <param name="intOrderID">the Order ID</param>
        /// <returns>the number of orders processed</returns>
        public int ProcessSalesOrderBySalesOrderID(Int64 intOrderID)
        {
            int intOrdersProcessed = 0;
            strExecutionLocation = "Executing ProcessSalesOrderBySalesOrderID ";
            try
            {
                List<SalesOrder> orders = _salesOrderAccess.GetSalesOrderByOrderID(intOrderID);
                intOrdersProcessed = this.ProcessSalesOrderList(orders);
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, strExecutionLocation);
                Program.WriteToConsole("Error " + strExecutionLocation + ": " + ex.Message);
                throw new Exception("ProcessSalesOrderBySalesOrderID Exception");
            }

            return intOrdersProcessed;

        }

        /// <summary>Processes the List of Sales Orders Passed</summary>
        /// <param name="orders">the Sales Order List</param>
        /// <returns>the number of orders processed</returns>
        public int ProcessSalesOrderList(List<SalesOrder> orders)
        {
            int intOrdersProcessed = 0;
            strExecutionLocation = "Starting ProcessSalesOrderList: " + orders.Count.ToString();
            foreach (SalesOrder sOrder in orders)
            {
                try
                {
                    _orderId = sOrder.OrderId;
                    strExecutionLocation = "ProcessSalesOrderList ProcessingID: " + intProcessingID.ToString() + "; OrderID: " + _orderId.ToString();

                    OrderEntryRequest orderEntryRequest = _salesOrderAccess.GetOrderEntryRequest(sOrder.OrderId);
                    strExecutionLocation += "; GetOrderEntryRequest";

                    xmlstring = this.GetXMLString(orderEntryRequest);
                    strExecutionLocation += "; GetXMLString";

                    bool blnSubmitSuccess = this.SubmitXmlWebRequest(xmlstring);
                    strExecutionLocation += "; SubmitXmlWebRequest";

                    if (blnSubmitSuccess)
                    {
                        // if saveXmlInDB is true send the XmlString to the DB otherwise make is blank
                        if (!Convert.ToBoolean(ConfigurationManager.AppSettings["saveXmlInDB"]))
                        {
                            xmlstring = string.Empty;
                        }

                        _salesOrderAccess.InsertOrderExport(_orderId, intProcessingID, xmlstring, orderEntryRequest.Order.Header.PONumber, sOrder.StatusID);
                        strExecutionLocation += "; InsertOrderExport";

                        Program.WriteToConsole(DateTime.Now + " : Order Completed (" + _orderId.ToString() + ")");

                        intOrdersProcessed++;
                    }
                }
                catch (Exception ex)
                {
                    LogException.HandleException(ex, strExecutionLocation);
                    Program.WriteToConsole("Error: " + strExecutionLocation + ": " + ex.Message);
                }
            }

            return intOrdersProcessed;
        }

        /// <summary>Submits the XML Web Request to SAP</summary>
        /// <param name="strXml">the XML String</param>
        /// <returns>Web Respnse Stream Reader</returns>
        public bool SubmitXmlWebRequest(string strXml)
        {
            string strResponseXML = string.Empty;
            bool blnSubmitSuccessful = false;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["transmitXML"]))
            {
                Console.WriteLine("In SubmitXmlWebRequest");

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["uri"].ToString());

                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml; charset=utf-8";

                webRequest.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["userName"].ToString(),
                    ConfigurationManager.AppSettings["passWord"].ToString());

                if (ConfigurationManager.AppSettings["uri"].ToString().ToLowerInvariant().Contains("https"))
                {
                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender,
                                                                                        X509Certificate certificate,
                                                                                        X509Chain chain,
                                                                                        SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };
                }

                byte[] bytes = Encoding.ASCII.GetBytes(xmlstring);
                webRequest.ContentLength = bytes.Length;
                webRequest.PreAuthenticate = true;

                Stream stream = webRequest.GetRequestStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Close();

                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    Program.WriteToConsole("WebResponse Status Code: " + webResponse.StatusCode.ToString() + "; Description: " + webResponse.StatusDescription);
                    blnSubmitSuccessful = Convert.ToBoolean(webResponse.StatusCode == System.Net.HttpStatusCode.OK); 
                    Program.WriteToConsole("OrderID: " + _orderId.ToString() + "XML Submission Result: " + webResponse.StatusCode.ToString());
                }
            }

            return blnSubmitSuccessful;
        }

        /// <summary>Prepares request xml</summary>
        /// <param name="orderEntryRequest">OrderEntryRequest as object</param>
        /// <returns>request xml as string</returns>
        public string GetXMLString(OrderEntryRequest order)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(OrderEntryRequest));
            StringBuilder stringBuilder = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(stringBuilder))
            {
                serializer.Serialize(stringWriter, order);
                xmlstring = stringBuilder.ToString();
            }

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlstring);

            xmlstring = xml["Body"].InnerXml;

            xmlstring = xmlstring.Replace("Z_SALES_ORDER_TO_SAP", "urn:Z_SALES_ORDER_TO_SAP");

            // to cover xml message with SAP soap xml envelope
            xmlstring = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" "
                + @"xmlns:urn=""urn:sap-com:document:sap:rfc:functions""><soapenv:Header/><soapenv:Body>"
                + xmlstring + "</soapenv:Body></soapenv:Envelope>";

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["saveXMLFile"].ToString()))
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlstring);

                string strFileName = "Order_" + _orderId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
                string strFolder = ConfigurationManager.AppSettings["XmlFolder"];
                xdoc.Save(strFolder + strFileName);
            }

            return xmlstring;
        }


    } //// end class
}
