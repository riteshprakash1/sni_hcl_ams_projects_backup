﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MIMS_SalesOrder.Entity;
using System.Collections.ObjectModel;

namespace MIMS_SalesOrder
{
    class SalesOrderAccess
    {

        public SalesOrderAccess()
        {
            System.Diagnostics.Debug.WriteLine("SalesOrderDataAccess");
        }


        /// <summary> Gets Sales Order by OrderID</summary>
        /// <param name="intOrderID">the Order ID</param>
        /// <returns>List of SO numbers</returns>
        public List<SalesOrder> GetSalesOrderByOrderID(Int64 intOrderID)
        {
            System.Diagnostics.Debug.WriteLine("GetSalesOrderByOrderID");
            List<SalesOrder> _orderNumbers = new List<SalesOrder>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@OrderID", intOrderID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetSalesOrderByOrderID", parameters);
            foreach(DataRow r in dt.Rows)
            {
                SalesOrder sOrder = new SalesOrder();
                sOrder.OrderId = Convert.ToInt64(r["OrderId"].ToString());
                sOrder.StatusID = Convert.ToInt32(r["StatusID"].ToString());
                _orderNumbers.Add(sOrder);
            }

            return _orderNumbers;
        }


        /// <summary> Get all SO where processed is 0 or null</summary>
        /// <returns>List of SO numbers</returns>
        public List<SalesOrder> GetAllUnProcessedSalesOrderNumbers()
        {
            System.Diagnostics.Debug.WriteLine("GetAllUnProcessedSalesOrderNumbers");
            List<SalesOrder> _orderNumbers = new List<SalesOrder>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAllUnProcessedSalesOrders", parameters);

            foreach(DataRow r in dt.Rows)
            {
                SalesOrder sOrder = new SalesOrder();
                sOrder.OrderId = Convert.ToInt64(r["OrderId"].ToString());
                sOrder.StatusID = Convert.ToInt32(r["StatusID"].ToString());
                _orderNumbers.Add(sOrder);
            }

            return _orderNumbers;
        }

        /// <summary>Get order object based on the sales order id </summary>
        /// <param name="crmOrderNumber">CRM Number as string</param>
        /// <returns>Order Entry Request object</returns>
        public OrderEntryRequest GetOrderEntryRequest(Int64 orderID)
        {
            OrderEntryRequest orderEntryRequest = new OrderEntryRequest();

            Order order = new Order();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@OrderID", orderID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetMIMsSalesOrderDetails", parameters);

            foreach (DataRow r in dt.Rows)
            {
                order.Sender = ConfigurationManager.AppSettings["Sender"];
                order.OrderType = r["OrderType"].ToString();
                Header header = new Header();
                //header.ID = r["id"].ToString();
                header.Salesrep = r["SALESREP"].ToString();
                header.Territory = r["TERRITORY"].ToString();
                header.Email = r["Email"].ToString();
                header.ReferenceNumber = r["ReferenceNumber"].ToString();
                header.PONumber = r["PONumber"].ToString();
                header.OrderDate = string.Format("{0:YYYYMMDD}", r["OrderDate"].ToString());
                header.ShipMethod = r["ShipMethod"].ToString();
                header.DeliveryBlock = r["DeliveryBlock"].ToString();
                header.BillingBlock = r["BillingBlock"].ToString();
                header.HldOrdrNte = r["HldOrdrNte"].ToString();
                header.KEText = r["KEText"].ToString();
                header.KBText = r["KBText"].ToString();
                header.Phone = r["Phone"].ToString();
                header.CaseNumber = r["CaseNumber"].ToString();

                KEPartner kePartner = new KEPartner();
                kePartner.ShipTo = r["KEPartner_ShipTo"].ToString();
                kePartner.SoldTo = r["KEPartner_SoldTo"].ToString();
                kePartner.OrderingParty = r["KEPartner_OrderingParty"].ToString();
                kePartner.SurgeonName = r["KEPartner_SurgeonName"].ToString();
                kePartner.SurgeonNbr = r["KEPartner_SurgeonNbr"].ToString();
                kePartner.PatientID = r["KEPartner_PatientID"].ToString();
                kePartner.SurgeryDate = string.Format("{0:YYYYMMDD}", r["KEPartner_SurgeryDate"].ToString());
                header.KEPartner = kePartner;

                /* This will not be used in MIMS so commented out population lines by D. Colwell   */
                KBPartner kbPartner = new KBPartner();
                kbPartner.ShipTo = r["KBPartner_ShipTo"].ToString();
                kbPartner.ForwardingAgent = r["KBPartner_ForwardingAgent"].ToString();
                header.KBPartner = kbPartner;

                /* This will not be used in MIMS so commented out population lines  by D. Colwell   */
                CreditCard creditCard = new CreditCard();
                creditCard.CCNumber = r["CCNumber"].ToString();
                creditCard.HolderName = r["HolderName"].ToString();
                creditCard.CardType = r["CardType"].ToString();
                creditCard.ExpMonth = r["ExpMonth"].ToString();
                creditCard.ExpYear = r["ExpYear"].ToString();
                header.CreditCard = creditCard;

                header.StatusID = Convert.ToInt32(r["StatusID"].ToString());

                order.Header = header;
            }

            order.Items = GetOrderItemsRequest(orderID);
            // D:\CRMCODEDEV\SOExport\Snn.Crm.SalesOrder.Common\Header.cs
            orderEntryRequest.Order = order;

            return orderEntryRequest;
        }

        /// <summary>Get Order Items details base on Sales order id</summary>
        /// <param name="orderID">MIMS OrderID</param>
        /// <returns>List of Items details</returns>
        private List<Item> GetOrderItemsRequest(Int64 orderID)
        {
            List<Item> items = new List<Item>();
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@OrderID", orderID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetMIMsSalesOrderItems", parameters);

            int lineNo = 1;

            foreach (DataRow r in dt.Rows)
            {
                Item item = new Item();
                item.ItemNo = lineNo.ToString();
                item.ItemFlag = r["ItemFlag"].ToString();
                item.Unit = r["Unit"].ToString();
                item.Lot = r["Lot"].ToString();
                item.SerialNum = r["SerialNum"].ToString();
                item.QTY = r["Qty"].ToString();
                item.Price = r["Price"].ToString();
                item.Material = r["Material"].ToString().ToUpper();
                item.KEConsumed = r["KEConsumed"].ToString();
                item.KBShipto = r["KBShipto"].ToString();
                item.DeliveryBlock = r["DeliveryBlock"].ToString();
                item.KEText = r["KEText"].ToString();

                item.InvMonId = r["InvMonId"].ToString();
                item.Plant = r["Plant"].ToString();
                item.PricingType = r["PricingType"].ToString();
                item.ReasonCode = r["ReasonCode"].ToString();
                item.ReplenishFlag = r["ReplenishFlag"].ToString();
                item.ReplenishType = r["ReplenishType"].ToString();

                items.Add(item);
                lineNo++;
            }

            return items;
        }

        /// <summary>Inserts a new record into the MIMS_OrderExport table</summary>
        /// <param name="intOrderID">MIMS Order ID</param>
        /// <param name="intProcessingID">MIMS Order ID</param>
        /// <param name="strXml">The xml that was sent to SAP</param>
        /// <param name="strPoNumber">the PO Number</param>
        /// <param name="intStatusID">MIMS Status ID</param>
        public void InsertOrderExport(Int64 intOrderID, Int64 intProcessingID, string strXml, string strPoNumber, int intStatusID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@OrderID", intOrderID));
            parameters.Add(new SqlParameter("@sentXml", strXml));
            parameters.Add(new SqlParameter("@PONumber", strPoNumber));
            parameters.Add(new SqlParameter("@StatusID", intStatusID));
            parameters.Add(new SqlParameter("@ProcessingID", intProcessingID));

            int intCnt = SQLUtility.SqlExecuteNonQueryCount("sp_InsertMIMS_OrderExport", parameters);
        }


    }  ////end class
}
