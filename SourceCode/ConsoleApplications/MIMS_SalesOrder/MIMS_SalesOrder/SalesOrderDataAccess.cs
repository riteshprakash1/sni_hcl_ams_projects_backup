﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MIMS_SalesOrderExport.Entity;


namespace MIMS_SalesOrderExport
{

    /// <summary>Data access layer for processing the SO request for SAP</summary>
    public class SalesOrderDataAccess
    {

        #region Local variables
        static string _connectionStringExport;
        static SqlConnection _sqlConnectionExport;

        static string _connectionStringImport;
        static SqlConnection _sqlConnectionImport;


        #endregion

        #region Instance

        /// <summary>
        /// private constructor, Initializes connection object
        /// </summary>
        SalesOrderDataAccess()
        {
            _connectionStringExport = ConfigurationManager.ConnectionStrings["sqlConnectionExport"].ToString();
            _sqlConnectionExport = new SqlConnection(_connectionStringExport);

            _connectionStringImport = ConfigurationManager.ConnectionStrings["sqlConnectionImport"].ToString();
            _sqlConnectionImport = new SqlConnection(_connectionStringImport);

        }

        /// <summary>
        /// Static Constructor
        /// </summary>
        static SalesOrderDataAccess()
        { }

        static readonly SalesOrderDataAccess dataAccess = new SalesOrderDataAccess();

        public static SalesOrderDataAccess GetInstance()
        {
            return dataAccess;
        }

        #endregion

        /// <summary>
        /// Get all SO where processed is 0 or null
        /// </summary>
        /// <returns>List of SO numbers</returns>
        public List<ProcessSalesOrder.SalesOrders> GetAllUnProcessedSalesOrderNumbers()
        {
            SqlDataReader reader = null;
            List<ProcessSalesOrder.SalesOrders> _orderNumbers = new List<ProcessSalesOrder.SalesOrders>();
            _sqlConnectionExport.ConnectionString = _connectionStringExport;
            _sqlConnectionExport.Open();
            try
            {
                using (_sqlConnectionExport)
                {
                    reader = SqlHelper.ExecuteReader(_sqlConnectionExport, "USP_GetAllUnProcessedSalesOrders");
                    while (reader.Read())
                    {
                        ProcessSalesOrder.SalesOrders sOrder = new ProcessSalesOrder.SalesOrders();
                        sOrder.CRMNumber = reader["CRMNumber"].ToString();
                        sOrder.ID = reader["ID"].ToString();
                        _orderNumbers.Add(sOrder);
                    }
                }
                return _orderNumbers;
            }
            finally
            {
                reader.Close();
                _sqlConnectionExport.Close();
            }
        }

        /// <summary>
        /// Get order object based on the sales order id
        /// </summary>
        /// <param name="crmOrderNumber">CRM Number as string</param>
        /// <returns>Order Entry Request object</returns>
        public OrderEntryRequest GetOrderEntryRequest(string crmOrderNumber)
        {
            OrderEntryRequest orderEntryRequest = new OrderEntryRequest();

            Order order = new Order();

            _sqlConnectionExport.ConnectionString = _connectionStringExport;
            _sqlConnectionExport.Open();

            using (_sqlConnectionExport)
            {
                SqlDataReader readr = SqlHelper.ExecuteReader(_sqlConnectionExport, CommandType.StoredProcedure, "USP_GetSalesOrderDetails",
                    new SqlParameter("@CRMNUMBER", crmOrderNumber));

                while (readr.Read())
                {
                    order.OrderType = readr["OrderType"].ToString();
                    Header header = new Header();
                    //header.ID = readr["id"].ToString();
                    header.Salesrep = readr["SALESREP"].ToString();
                    header.Territory = readr["TERRITORY"].ToString();
                    header.Email = readr["Email"].ToString();
                    header.ReferenceNumber = readr["ReferenceNumber"].ToString();
                    header.PONumber = readr["PONumber"].ToString();
                    header.OrderDate = string.Format("{0:YYYYMMDD}", readr["OrderDate"].ToString());
                    header.ShipMethod = readr["ShipMethod"].ToString();
                    header.DeliveryBlock = readr["DeliveryBlock"].ToString();
                    header.BillingBlock = readr["BillingBlock"].ToString();
                    header.HldOrdrNte = readr["HldOrdrNte"].ToString();
                    header.KEText = readr["KEText"].ToString();
                    header.KBText = readr["KBText"].ToString();
                    header.Phone = readr["SalesRepPhone"].ToString();
                    header.CaseNumber = readr["VisCaseNumber"].ToString();

                    KEPartner kePartner = new KEPartner();
                    kePartner.ShipTo = readr["KEPartner_ShipTo"].ToString();
                    kePartner.SoldTo = readr["KEPartner_SoldTo"].ToString();
                    //kePartner.OrderingParty = readr["KEPartner_OrderingParty"].ToString();
                    //kePartner.SurgeonName = readr["KEPartner_SurgeonName"].ToString();
                    //kePartner.SurgeonNbr = readr["KEPartner_SurgeonNbr"].ToString();
                    //kePartner.PatientID = readr["KEPartner_PatientID"].ToString();
                    kePartner.SurgeryDate = string.Format("{0:YYYYMMDD}", readr["KEPartner_SurgeryDate"].ToString());
                    header.KEPartner = kePartner;

                    /* This will not be used in MIMS so commented out population lines by D. Colwell   */
                    KBPartner kbPartner = new KBPartner();
                    //kbPartner.ShipTo = readr["KBPartner_ShipTo"].ToString();
                    //kbPartner.ForwardingAgent = readr["KBPartner_ForwardingAgent"].ToString();
                    header.KBPartner = kbPartner;

                    /* This will not be used in MIMS so commented out population lines  by D. Colwell   */
                    CreditCard creditCard = new CreditCard();
                    //creditCard.CCNumber = readr["CCNumber"].ToString();
                    //creditCard.HolderName = readr["HolderName"].ToString();
                    //creditCard.CardType = readr["CardType"].ToString();
                    //creditCard.ExpMonth = readr["ExpMonth"].ToString();
                    //creditCard.ExpYear = readr["ExpYear"].ToString();
                    header.CreditCard = creditCard;

                    order.Header = header;
                }
            }

            order.Items = GetOrderItemsRequest(crmOrderNumber);
            // D:\CRMCODEDEV\SOExport\Snn.Crm.SalesOrder.Common\Header.cs
            orderEntryRequest.Order = order;

            return orderEntryRequest;
        }

        /// <summary>
        /// Get Order Items details base on Sales order id
        /// </summary>
        /// <param name="crmOrderNumber">CRM Number as string</param>
        /// <returns>List of Items details</returns>
        private List<Item> GetOrderItemsRequest(string crmOrderNumber)
        {
            List<Item> items = new List<Item>();
            _sqlConnectionExport.ConnectionString = _connectionStringExport;
            _sqlConnectionExport.Open();

            using (_sqlConnectionExport)
            {
                SqlDataReader readr = SqlHelper.ExecuteReader(_sqlConnectionExport, CommandType.StoredProcedure, "USP_GetSalesOrderItems",
                    new SqlParameter("@CRMNUMBER", crmOrderNumber));

                while (readr.Read())
                {
                    Item item = new Item();
                    item.ItemFlag = readr["ItemFlag"].ToString();

                    //Mail from peter to check the condition on 03 November 2010
                    // mail from peter to replace the two decimals instead of 4 decimals.

                    // Clarity Enhancement 009714
                    //item.QTY = readr["ItemFlag"].ToString().ToLower() == "fee" ? Convert.ToString(Math.Round(Convert.ToDecimal(readr["Price"].ToString()), 2)) : readr["QTY"].ToString();

                    if (readr["ItemFlag"].ToString().ToLower() == "fee")
                    {
                        if (Convert.ToDouble(readr["Price"].ToString()) == 0.00)
                        {
                            item.QTY = "1.00";
                        }
                        else
                        {
                            item.QTY = Convert.ToString(Math.Round(Convert.ToDecimal(readr["Price"].ToString()), 0));
                        }
                    }
                    else
                    {
                        item.QTY = readr["QTY"].ToString();
                    }

                    item.Unit = readr["Unit"].ToString();
                    item.Lot = readr["Lot"].ToString();

                    //Mail from peter to check the condition on 03 November 2010
                    // mail from peter to replace the two decimals instead of 4 decimals.
                    if (readr["ItemFlag"].ToString().ToLower() == "in" || readr["ItemFlag"].ToString().ToLower() == "fee")
                    {
                        item.Price = string.Empty;
                    }
                    else
                    {
                        //todo added
                        if (!string.IsNullOrEmpty(readr["Price"].ToString().Trim()))
                            item.Price = Convert.ToString(Math.Round(Convert.ToDecimal(readr["Price"].ToString()), 2));
                        else
                            throw new Exception("Invaild Price for CRMNUMBER = " + crmOrderNumber);
                    }

                    // Mail from Peter to check the condiotion on 02 November 2010 
                    // Upper Case Implemeted on 12/01/2011 Peter excel
                    item.Number = ((string)(string.IsNullOrEmpty(readr["Number"].ToString()) == true ? readr["InValidItemId"].ToString() : readr["Number"].ToString())).ToUpper();
                    item.KEConsumed = readr["KEConsumed"].ToString();
                    item.KBShipto = readr["KBShipto"].ToString();
                    item.DeliveryBlock = readr["DeliveryBlock"].ToString();
                    item.KEText = readr["KEText"].ToString();
                    items.Add(item);
                }
            }

            return items;
        }

        /// <summary>
        /// Saves the SAP Sales Order Status in the Import database
        /// </summary>
        /// <param name="sapStatus">SAP Status as object</param>
        /// <param name="xml">The xml that was sent to SAP</param>
        public void SaveStatus(SapStatus sapStatus)
        {
            SqlParameter[] param;
            param = new SqlParameter[4];

            param[0] = new SqlParameter("@CRMNumber", sapStatus.CRMNumber);
            param[1] = new SqlParameter("@ID", sapStatus.ID);
            param[2] = new SqlParameter("@processed", sapStatus.Processed);
            //param[3] = new SqlParameter("@sentXML", xml);

            _sqlConnectionExport.ConnectionString = _connectionStringExport;
            _sqlConnectionExport.Open();

            using (_sqlConnectionExport)
            {
                SqlHelper.ExecuteNonQuery(_sqlConnectionExport, CommandType.StoredProcedure, "USP_UpdateOrderStaus", param);
            }

            SqlParameter[] param1;
            param1 = new SqlParameter[3];

            param1[0] = new SqlParameter("@CRMNumber", sapStatus.CRMNumber);
            param1[1] = new SqlParameter("@ErrorStatus", sapStatus.ErrorStatus);
            param1[2] = new SqlParameter("@ErrorStatusText", sapStatus.ErrorStatusText);

            _sqlConnectionImport.ConnectionString = _connectionStringImport;
            _sqlConnectionImport.Open();

            using (_sqlConnectionImport)
            {
                SqlHelper.ExecuteNonQuery(_sqlConnectionImport, CommandType.StoredProcedure, "USP_UpdateSAPStatusResponse", param1);
            }
        }

        /// <summary>
        /// Update XML on same row for debugging
        /// </summary>
        /// <param name="xml"></param>
        public void UpdateXML(string crmNumber, string xml)
        {
            _sqlConnectionExport.ConnectionString = _connectionStringExport;
            _sqlConnectionExport.Open();
            try
            {
                using (_sqlConnectionExport)
                {
                    //SqlHelper.ExecuteNonQuery(_sqlConnectionExport, CommandType.Text, "UPDATE new_salesorder SET new_xml= '" + xml + "' WHERE new_salesorderid='" + crmNumber + "'");
                    // removed based on kevin comment and as a bug.

                    SqlParameter[] param;
                    param = new SqlParameter[2];

                    param[0] = new SqlParameter("@CRMNumber", crmNumber);
                    param[1] = new SqlParameter("@sentXml", xml);

                    SqlHelper.ExecuteNonQuery(_sqlConnectionExport, CommandType.StoredProcedure, "UpdateSentXML", param);
                }
            }
            finally
            {
                _sqlConnectionExport.Close();
            }
        }


    }
}
