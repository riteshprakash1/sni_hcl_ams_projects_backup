using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Threading;


namespace LearnComInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.GetDomain().UnhandledException += new
                UnhandledExceptionEventHandler(Application_UnhandledException);


            LearnExtract learn  = new LearnExtract();
            learn.BuildFile();
        }

        public static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // Handle exception. 
            Console.WriteLine("I'm in Application_UnhandledException");
            LogException logger = new LogException();
            logger.HandleException(e.ExceptionObject as Exception);
            // The exception object is contained in e.ExceptionObject.
        } // END Application_UnhandledException
    }
}
