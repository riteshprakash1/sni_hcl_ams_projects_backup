// -------------------------------------------------------------
// <copyright file="LearnExtract.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------
namespace LearnComInterface
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Text;

    /// <summary>
    /// This is the class the performs the extraction for the learn.com interface.
    /// </summary>
    public class LearnExtract
    {
        /// <summary>
        /// This is our SQL connection.
        /// </summary>
        private SqlConnection sqlConn;

        /// <summary>
        /// Initializes a new instance of the <see cref="LearnExtract"/> class.
        /// </summary>
        public LearnExtract()
        {
            string strConn = ConfigurationManager.AppSettings["SqlConn"].ToString();
            this.sqlConn = new SqlConnection();
            this.sqlConn.ConnectionString = strConn;
        } //// end constructor

        /// <summary>
        /// Builds the file.
        /// </summary>
        public void BuildFile()
        {
            Console.WriteLine("Start: " + DateTime.Now.ToString());
            Console.WriteLine("ID: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name);

            ////Set a virtual folder to save the file.
            ////Make sure that you change the application name to match your folder.
            string strFtpFolder = ConfigurationManager.AppSettings["FtpFolder"].ToString();
            string strFtpFile = ConfigurationManager.AppSettings["FtpFileName"].ToString();

            ////Create a pseudo-random file name.
            string strTime = DateTime.Now.ToFileTime().ToString();
            //string fileName = strFtpFile + ".txt";

            string filePath = ConfigurationManager.AppSettings["RootFolder"].ToString();
            filePath += strFtpFolder;

            string archivePath = filePath + "\\archive";

            Console.WriteLine("filePath: " + filePath);
            string localFile = filePath + "\\" + strFtpFile + ".txt";
            string archiveFile = archivePath + "\\" + strFtpFile + strTime + ".txt";
            string returnFile = filePath + "\\" + strFtpFile + strTime + "_return.txt";
            Console.WriteLine("localFile: " + localFile);

            // if the file exists delete it
            if (File.Exists(localFile))
            {
                Console.WriteLine("Deleting Local File: " + localFile);
                File.Delete(localFile);
            }

            ////Use FileStream to create the .xls file.
            FileStream objFileStream = new FileStream(localFile, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter objStreamWriter = new StreamWriter(objFileStream);

            this.sqlConn.Open();
            DataTable dt = this.SetTable("sp_GetLearnExtractData");
            this.sqlConn.Close();
            Console.WriteLine("Start File Build: " + DateTime.Now.ToString());

            string strLine = String.Empty;
            ////Enumerate the field names and records that are used to build the file.
            foreach (DataColumn col in dt.Columns)
            {
                strLine = strLine + "\"" + col.ColumnName + "\"" + "\t";
            }

            ////Write the field name information to file.
            objStreamWriter.WriteLine(strLine);

            ////Reinitialize the string for data.
            strLine = String.Empty;

            ////Enumerate the database that is used to populate the file.
            foreach (DataRow r in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    strLine = strLine + "\"" + r[col.ColumnName].ToString().Replace("\"", "'") + "\"" + "\t";
                }

                objStreamWriter.WriteLine(strLine);
                strLine = String.Empty;
            }

            ////Clean up.
            objStreamWriter.Close();
            objFileStream.Close();

            string strFtpSite = ConfigurationManager.AppSettings["FtpSite"].ToString() + strFtpFile + ".txt";
            string strFtpUser = ConfigurationManager.AppSettings["FtpUser"].ToString();
            string strFtpPswd = ConfigurationManager.AppSettings["FtpPswd"].ToString();

            Console.WriteLine("FTPFile: " + strFtpSite);
            Console.WriteLine("LocalFile: " + localFile);
            Console.WriteLine("ReturnFile: " + returnFile);

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["UploadFile"].ToString()))
            {
                Console.WriteLine("Start Upload: " + DateTime.Now.ToString());
                //// Call the UploadFileUsingFTP to Move the file to the FTP site        
                this.UploadFileUsingFTP(strFtpSite, localFile, strFtpUser, strFtpPswd);
                Console.WriteLine("Start Download: " + DateTime.Now.ToString());
                this.FileDownloadFtp(strFtpSite, returnFile, strFtpUser, strFtpPswd);

                //// Send the file to the FTP Site
                FileInfo fileOrig = new FileInfo(localFile);

                ////Pull the file back down
                FileInfo fileReturn = new FileInfo(returnFile);

                //// if the filess are the same delete the return file and archive the original
                Console.WriteLine("Orig: " + fileOrig.Length.ToString() + "; Return: " + fileReturn.Length.ToString() + "; Same: " + Convert.ToBoolean(fileReturn.Length.ToString() == fileOrig.Length.ToString()).ToString());
                if (Convert.ToBoolean(fileReturn.Length.ToString() == fileOrig.Length.ToString()))
                {
                    fileReturn.Delete();
                    fileOrig.MoveTo(archiveFile);
                    string strLog = "File Sent Successfully; " + fileOrig.Name;
                    this.OpenLogFile(strLog);
                }
                else
                {
                    string strSubject = "Learn.com Extract File Error";
                    string strBody = "Original File: " + fileOrig.Name + "; Size: " + fileOrig.Length.ToString() + "<br>  ";
                    strBody += "Return File: " + fileReturn.Name + "; Size: " + fileReturn.Length.ToString();
                    this.SendMail(strSubject, strBody);
                    string strLog = "Error:  " + strBody;
                    this.OpenLogFile(strLog);
                }

                //// Delete Old Files
                DirectoryInfo d = new DirectoryInfo(archivePath);
                FileInfo[] files = d.GetFiles();
                for (int x = 0; x < files.Length; x++)
                {
                    DateTime fileDate = files[x].LastWriteTime;
                    DateTime today = DateTime.Now;
                    TimeSpan span = today.Subtract(fileDate);
                    int intDeleteDays = Int32.Parse(ConfigurationManager.AppSettings["DeleteDays"].ToString());

                    Console.WriteLine("Span: " + span.Days.ToString() + ": File: " + files[x].Name);
                    //// if file greater than delete days delete the file
                    if (span.Days > intDeleteDays)
                    {
                        files[x].Delete();
                    }
                }

                Console.WriteLine("Done: " + DateTime.Now.ToString());
            }
        } //// end BuildFile

        /// <summary>
        /// Sets the table.
        /// </summary>
        /// <param name="sp">The stored procedure.</param>
        /// <returns>The data table with the information.</returns>
        public DataTable SetTable(string sp)
        {
            SqlCommand cmdSetTable = new SqlCommand();
            cmdSetTable.CommandText = sp;
            cmdSetTable.CommandType = CommandType.StoredProcedure;
            cmdSetTable.Connection = this.sqlConn;

            DataTable dt = new DataTable();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                LogException logger = new LogException();
                logger.HandleException(exc);
                Console.WriteLine(sp + " SetTable ERROR" + exc.Message);
            }

            return dt;
        } //// END SetTable()

        /// <summary>
        /// Uploads the file using FTP.
        /// </summary>
        /// <param name="strCompleteFTPPath">The STR complete FTP path.</param>
        /// <param name="strCompleteLocalPath">The STR complete local path.</param>
        /// <param name="strUName">Name of the STR U.</param>
        /// <param name="strPswd">The STR PSWD.</param>
        private void UploadFileUsingFTP(string strCompleteFTPPath, string strCompleteLocalPath, string strUName, string strPswd)
        {
            //// Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(strCompleteFTPPath);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            ////If you want to access Resourse Protected You need to give UserName and PWD
            request.Credentials = new NetworkCredential(strUName, strPswd);

            //// Copy the contents of the file to the request stream.
            StreamReader sourceStream = new StreamReader(strCompleteLocalPath);
            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            request.ContentLength = fileContents.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);
            Debug.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

            response.Close();
        }

        /// <summary>
        /// Files the download FTP.
        /// </summary>
        /// <param name="strCompleteFTPPath">The STR complete FTP path.</param>
        /// <param name="strCompleteLocalPath">The STR complete local path.</param>
        /// <param name="strUName">Name of the STR U.</param>
        /// <param name="strPswd">The STR PSWD.</param>
        private void FileDownloadFtp(string strCompleteFTPPath, string strCompleteLocalPath, string strUName, string strPswd)
        {
            //// Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(strCompleteFTPPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            ////If you want to access Resourse Protected You need to give UserName and PWD
            request.Credentials = new NetworkCredential(strUName, strPswd);

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            //// Copy the contents of the file to the request stream.
            byte[] fileContents = Encoding.UTF8.GetBytes(reader.ReadToEnd());
            reader.Close();

            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            Debug.WriteLine("Download Complete, status {0}", response.StatusDescription);

            FileStream newFile = new FileStream(strCompleteLocalPath, FileMode.Create);

            //// Write data to the file
            newFile.Write(fileContents, 0, fileContents.Length);

            //// Close file
            newFile.Close();

            response.Close();
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="strSubj">The STR subj.</param>
        /// <param name="strBody">The STR body.</param>
        private void SendMail(string strSubj, string strBody)
        {
            MailAddress from = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"].ToString());
            MailAddress to = new MailAddress(ConfigurationManager.AppSettings["logEmailAddresses"].ToString());

            //// Specify the message content.
            MailMessage message = new MailMessage(from, to);
            Console.WriteLine("From: " + message.From.Address);

            message.IsBodyHtml = true;
            message.Subject = strSubj;
            message.Body = strBody;

            SmtpClient client = new SmtpClient();
            Console.WriteLine("client: " + client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Console.WriteLine(excm.Message + "\r\n" + excm.InnerException);
                Debug.WriteLine(excm.Message);
                throw;
            }
        } ////end SendMail

        /// <summary>
        /// Opens the log file.
        /// </summary>
        /// <param name="strMsg">The STR MSG.</param>
        private void OpenLogFile(string strMsg)
        {
            try
            {
                string strLogFile = ConfigurationManager.AppSettings["LogFile"].ToString();
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strLogFile);
                System.IO.StreamWriter str = fileLog.AppendText();

                string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
                str.WriteLine(strTime + strMsg + "\n");
                str.Flush(); // close log file
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Writing Log: " + ex.Message);
            }
        } //// END OpenLogFile
    }
} //// end namespace
