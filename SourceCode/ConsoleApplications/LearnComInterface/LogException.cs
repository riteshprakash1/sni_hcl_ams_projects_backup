using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Configuration;
using System.Diagnostics;
using System.Net;

namespace LearnComInterface
{
	/// <summary>
	/// Summary description for LogException.
	/// </summary>
	public class LogException
	{
		public LogException()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void HandleException(Exception ex)
		{
			//	HttpContext ctx = HttpContext.Current;	
			string strData=String.Empty;
            Console.WriteLine("HandleException");

            bool logIt = Convert.ToBoolean(ConfigurationManager.AppSettings["logErrors"]);
			if(logIt)
			{
				string referer=String.Empty;
				//				if(ctx.Request.ServerVariables["HTTP_REFERER"]!=null)
				//					referer = ctx.Request.ServerVariables["HTTP_REFERER"].ToString();

				//				string sForm = (ctx.Request.Form !=null)?ctx.Request.Form.ToString():String.Empty;
      
				string logDateTime =DateTime.Now.ToString();
				string sQuery = "Contracts Email Reminder App";

				//				string sQuery = (ctx.Request.QueryString !=null)?ctx.Request.QueryString.ToString():String.Empty;
				strData =  "\nSOURCE: " + ex.Source + "\nLogDateTime: " +logDateTime;
				strData += "\nMESSAGE: " +ex.Message;// + "\nFORM: " + sForm;
				strData += "\nQUERYSTRING: " + sQuery +	"\nTARGETSITE: " + ex.TargetSite;
				strData += "\nSTACKTRACE: " + ex.StackTrace + "\nREFERER: " +referer;

                //if(dbConnString.Length >0)
                //{                 	
                    //SqlCommand cmd = new SqlCommand();
                    //cmd.CommandType=CommandType.StoredProcedure;
                    //cmd.CommandText="usp_WebAppLogsInsert";
                    //SqlConnection cn = new SqlConnection(dbConnString);
                    //cmd.Connection=cn;
                    //cn.Open();  					
                    //try
                    //{	
                    //    cmd.Parameters.Add(new SqlParameter("@Source", ex.Source));
                    //    cmd.Parameters.Add(new SqlParameter("@Message",ex.Message));									
                    //    cmd.Parameters.Add(new SqlParameter("@Form",String.Empty));
                    //    cmd.Parameters.Add(new SqlParameter("@QueryString",sQuery));
                    //    cmd.Parameters.Add(new SqlParameter("@TargetSite",ex.TargetSite.ToString()));
                    //    cmd.Parameters.Add(new SqlParameter("@StackTrace",ex.StackTrace.ToString()));
                    //    cmd.Parameters.Add(new SqlParameter("@AppUser",String.Empty));
                    //    cmd.Parameters.Add(new SqlParameter("@Referer",referer));
                    //    SqlParameter outParm=new SqlParameter("@EventId",SqlDbType.Int);
                    //    outParm.Direction =ParameterDirection.Output;
                    //    cmd.Parameters.Add(outParm);							
                    //    cmd.ExecuteNonQuery();
                    //    evtId =Convert.ToInt32(cmd.Parameters[8].Value);
                    //    cmd.Dispose();
                    //    cn.Close();			
                    //}
                    //catch (Exception exc)
                    //{
                    //    Debug.WriteLine("usp_WebAppLogsInsert Error: " + exc.Message);	
                    //    throw;
                    //    //	EventLog.WriteEntry (ex.Source, "Database Error From Exception Log!",EventLogEntryType.Error,65535);
                    //}
                    //finally
                    //{
                    //    cmd.Dispose();
                    //    cn.Close();
                    //} // END SQL Insert


					// write error to application event log
					// Currently commented out because it will not be used by server admin
					//					try
					//					{					
					//					EventLog.WriteEntry (ex.Source, strData,EventLogEntryType.Error,evtId);
					//					}
					//					catch(Exception exl)
					//					{
					//						Debug.WriteLine("EventLog Write Error: " +exl.Message );
					//						throw;
					//
					//					}
                //}  // END dbConnString.Length >0 
			} // END if(logIt)
	
			// Send email notification
			// Email receiptient list should be delimited by |
            string strEmails = ConfigurationManager.AppSettings["logEmailAddresses"].ToString();
			if (strEmails.Length >0) 
			{
                MailAddress from = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"].ToString());

                // Set destinations for the e-mail message.
                string[] emails = strEmails.Split(Convert.ToChar("|"));
                System.Net.Mail.MailMessage msg = new MailMessage();
                MailAddress to = new MailAddress(emails[0]);

                // Specify the message content.
                MailMessage message = new MailMessage(from, to);
                Console.WriteLine("From: " + message.From.Address);

                message.IsBodyHtml = false;
                message.Subject =  "Web application error!";

                message.Body  =  strData;

                //string strSMTPHost = ConfigurationManager.AppSettings["logSmtpServer"].ToString();
                //SmtpClient client = new SmtpClient(strSMTPHost);
                //client.Credentials = new NetworkCredential("endoscopy\\colwelld", "@Beach55");
                ////    client.UseDefaultCredentials = true;
                //client.Port = 25;

                SmtpClient client = new SmtpClient();
                Console.WriteLine("client: " + client.Host);

				try
				{
                    client.Send(message);
				}
				catch (Exception excm )
				{
                    Console.WriteLine(excm.Message+"\r\n"+excm.InnerException);
                    Debug.WriteLine(excm.Message);
					throw;
				}
			}

		} // END HandleException(Exception ex)

	} // END class
}
