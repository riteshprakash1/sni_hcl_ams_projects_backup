using System;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace LdapUpdate
{
	/// <summary>
	/// Summary description for GroupMembers.
	/// </summary>
	public class GroupMembers
	{
		
		DataTable tblGrpUsers;

		public GroupMembers()
		{

		}

		public DataTable LoadGroupMembers(DataTable dtGroups, DataTable dtGrpUsers)
		{
			tblGrpUsers = dtGrpUsers;
			foreach(DataRow r in dtGroups.Rows)
			{
				this.GetMembersOfDistinguishedGroup(r["distinguishedGroup"].ToString());
			}

			DataRow[] rows = tblGrpUsers.Select("distinguishedGroup='CN=SN_GG_SUCCESSFACTORS,OU=Success Factors,OU=Cross-Domain Groups,DC=ortho,DC=san'" );
			Console.WriteLine("SUCCESSFACTORS Rows: " + rows.Length.ToString());
			
			return tblGrpUsers;

		}

		public void GetMembersOfDistinguishedGroup(string strDist)
		{
			Boolean isLastQuery = false;
			Boolean exitLoop = false;
			Int32 rangeStep = 1500;
			Int32 rangeLow = 0;
			Int32 rangeHigh = rangeLow + ( rangeStep - 1 );
			String attributeWithRange;
			
			string strLdapPath = System.Configuration.ConfigurationSettings.AppSettings["ldapServer"];
			DirectoryEntry de = new DirectoryEntry(strLdapPath + strDist);
//			DirectoryEntry de = this.GetDirectoryEntry(this.GetPath(strDist));
			DirectorySearcher srch = new DirectorySearcher(de);
			SearchResult searchResults;

			srch.Filter = "(&(objectClass=group)(distinguishedname=" + strDist + "))";
			Console.WriteLine("Filter: " + srch.Filter);
			int cnt=0;
			do
			{
				if( !isLastQuery )
					attributeWithRange = String.Format( "member;range={0}-{1}", rangeLow, rangeHigh );
				else
					attributeWithRange = String.Format( "member;range={0}-*", rangeLow );


				cnt++;
				if(cnt > 15)
				{
					Console.WriteLine(cnt.ToString() + "; strDist: " + strDist + "; " + attributeWithRange);
					exitLoop = true;
				}

				srch.PropertiesToLoad.Clear();
				srch.PropertiesToLoad.Add( attributeWithRange );
			
				searchResults = srch.FindOne();

				if( searchResults.Properties.Contains( attributeWithRange ) )
				{
					for(int x=0; x < searchResults.Properties[attributeWithRange].Count; x++)
					{
						this.LoadGroupMembers(searchResults.Properties[attributeWithRange][x].ToString(), strDist, "User");
					}
				}

				srch.Dispose();

				if( searchResults.Properties.Contains( attributeWithRange ) )
				{
					if( isLastQuery )
						exitLoop = true;
				}
				else
				{
					if( isLastQuery )
						exitLoop = true;

					isLastQuery = true;
				}

				if( !isLastQuery )
				{
					rangeLow = rangeHigh + 1;
					rangeHigh = rangeLow + ( rangeStep - 1 );
				}
			}
			while( ! ( exitLoop) );
		}

		private void LoadGroupMembers(string strMember, string strGroup, string strType)
		{
			DataRow row = this.tblGrpUsers.NewRow();
			row["distinguishedGroup"] = strGroup;
			row["distinguishedMember"] = strMember;
			row["memberType"] = strType;
			this.tblGrpUsers.Rows.Add(row);
		} //END LoadMembersToTable

	} //// end class
}