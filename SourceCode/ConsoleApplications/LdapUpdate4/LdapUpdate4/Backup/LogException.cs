using System;
using System.Web;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Configuration;

namespace LdapUpdate
{	 
	public class LogException  
	{
		public LogException( ) //ctor
		{ 
		}
	
		public void HandleException(Exception ex, string strProcessLog)
		{
			Console.WriteLine("Error: "+ ex.Message);
			int evtId = 0;
			string strData=String.Empty;
			bool logIt = Convert.ToBoolean(ConfigurationSettings.AppSettings["logErrors"]);
			if(logIt)
			{
				Console.WriteLine("In logIt");
				string dbConnString=	ConfigurationSettings.AppSettings["logDbConnString"].ToString();
				string strLogServer=	ConfigurationSettings.AppSettings["logServer"].ToString();
				
				string logDateTime =DateTime.Now.ToString();
				strData =  "\nSOURCE: " + ex.Source + "\nLogDateTime: " +logDateTime;
				strData += "\nMESSAGE: " +ex.Message;
				strData += "\nTARGETSITE: " + ex.TargetSite;
				strData += "\nSTACKTRACE: " + ex.StackTrace;
				strData += "\nAPPLICATION: LdapUpdate";
				strData += "\nUSER:  Console\n";
				strData += "\nSERVER:  " + strLogServer + "\n";
				strData += "\nPROCESS LOG: \n" + strProcessLog + "\n";

				if(dbConnString.Length >0)
				{                 	
					SqlCommand cmd = new SqlCommand();
					cmd.CommandType=CommandType.StoredProcedure;
					cmd.CommandText="usp_WebAppLogsInsert";
					SqlConnection cn = new SqlConnection(dbConnString);
					cmd.Connection=cn;
					cn.Open();  					
					try
					{	
						cmd.Parameters.Add(new SqlParameter("@Source", ex.Source));
						cmd.Parameters.Add(new SqlParameter("@Message",ex.Message));									
						cmd.Parameters.Add(new SqlParameter("@Form",String.Empty));
						cmd.Parameters.Add(new SqlParameter("@QueryString",String.Empty));
						cmd.Parameters.Add(new SqlParameter("@TargetSite",ex.TargetSite.ToString()));
						cmd.Parameters.Add(new SqlParameter("@StackTrace",ex.StackTrace.ToString()));
						cmd.Parameters.Add(new SqlParameter("@Referer",String.Empty));
						cmd.Parameters.Add(new SqlParameter("@AppUser","Console"));

						SqlParameter outParm=new SqlParameter("@EventId",SqlDbType.Int);
						outParm.Direction =ParameterDirection.Output;
						cmd.Parameters.Add(outParm);							
						cmd.ExecuteNonQuery();
						evtId =Convert.ToInt32(cmd.Parameters[8].Value);
						cmd.Dispose();
						cn.Close();			
					}
					catch (Exception exc)
					{
						Console.WriteLine("usp_WebAppLogsInsert Error: " + exc.Message);	
						throw;
					}
					finally
					{
					cmd.Dispose();
					cn.Close();
					} // END SQL Insert


				}  // END dbConnString.Length >0 
			} // END if(logIt)
	
			// Send email notification
			// Email receiptient list should be delimited by |
			string strEmails =ConfigurationSettings.AppSettings["logEmailAddresses"].ToString();
			if (strEmails.Length >0) 
			{
				string[] emails = strEmails.Split(Convert.ToChar("|"));
				MailMessage msg = new MailMessage();
				msg.BodyFormat = MailFormat.Text;
				msg.To         = emails[0];

				for (int i =1;i<emails.Length;i++)
					msg.Cc=emails[i];

				msg.From = ConfigurationSettings.AppSettings["logFromEmail"].ToString();
				msg.Subject =  "LpadUpdate Application Error";
				string detailURL=	ConfigurationSettings.AppSettings["logDetailURL"].ToString();
				msg.Body  =  strData + detailURL +"?EvtId="+ evtId.ToString(); 
				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["logSmtpServer"].ToString();
				try
				{				
					SmtpMail.Send(msg);
				}
				catch (Exception excm )
				{
					Console.WriteLine(excm.Message);
					throw;
				}
			}

		} // END HandleException(Exception ex)

	} // END class
} // END namespace
