using System;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.IO;

namespace LdapUpdate
{
	/// <summary>
	/// Summary description for Utility.
	/// </summary>
	public class Utility
	{
		protected SqlConnection sqlConnection1;
		protected DateTime dtProcessStart;
		protected DateTime dtProcessEnd;
		protected DateTime dtDomStart;
		protected DateTime dtDomEnd;
		protected string strStatus;
		protected string strLogPath;
		protected string strLogProgress;
		protected string strTempStat;
		protected string[] personArray = {"samaccountname", "givenname", "sn", "displayName", "mail", 
									"distinguishedName", "telephonenumber", "streetaddress", "l", 
									"co", "st", "postalcode","Title", "Department", 
									"physicaldeliveryofficename", "manager", "userAccountControl", "mobile", "memberof"};

		protected string[] groupArray = {"adspath",  "samaccountname", "name", "description", "distinguishedName", "cn", "member", "memberof"};
		protected DataTable dtGroupUsers;
		protected System.IO.StreamWriter str;
		protected bool killProcess = false;
		protected DataTable dtGroups;

		public Utility()
		{
			strLogProgress="";
			strStatus = "";
			AppSettingsReader configSet = new AppSettingsReader();
			string strConn = ((string)(configSet.GetValue("sqlConn", typeof(string))));
			strLogPath = ((string)(configSet.GetValue("logFilePath", typeof(string))));
			sqlConnection1 = new SqlConnection();
			sqlConnection1.ConnectionString = strConn;

			this.dtGroupUsers = this.CreateGroupUsersTable();
		} // END Constructor

		
		private SqlConnection SqlConn
		{
			get {return sqlConnection1;}
			set {sqlConnection1 = value;}
		}

		private void OpenLogFile()
		{
			string strDate = DateTime.Now.ToShortDateString().Replace("/", ""); 
			string[] strTimes = DateTime.Now.ToLongTimeString().Split(' ');
			string strNewFile = strLogPath + "Log_" + strDate + strTimes[0].Replace(":", "") + ".txt";

            System.IO.FileInfo fileLog = new System.IO.FileInfo(strNewFile);
			str = fileLog.AppendText();
		} // END OpenLogFile

        /// <summary>
        /// This process is the central command to load the LDAP database
        /// </summary>
		public void RunLoadProcess()
		{
			this.OpenLogFile();	// open log file

			this.dtProcessStart = DateTime.Now; // start timer
			string strStartLog = "Process Start: " + dtProcessStart.ToString();
			str.WriteLine(strStartLog);
			Console.WriteLine(strStartLog);
			strStatus = strStartLog + "\n";

			try
			{
				AppSettingsReader configSet = new AppSettingsReader();
				NameValueCollection DomainPaths = (NameValueCollection)ConfigurationManager.GetSection("DomainPaths");

				this.SqlConn.Open();
				int rowUpdate = this.ExecuteNonQuerySP( "sp_TruncateStageTables");  // clear user & group stage tables
				str.WriteLine("Rows Truncated: " + rowUpdate.ToString());

				if(!this.killProcess)
					this.GetAllUsers(DomainPaths);						// load user stage table
				
				if(!this.killProcess)
				{
					this.dtGroups = this.CreateGroupsTable();	// used to record oversized groups	
//					this.GetAllGroups(DomainPaths);				// load group stage table
					this.GetAllGroups();				// load group stage table
				}
				

				if(!this.killProcess)
				{
					// this step load the large groups over 1450
					Console.WriteLine("start LoadGroupMembers: " + dtGroups.Rows.Count.ToString());
					GroupMembers membersOnly = new GroupMembers();
					DataTable dtMembers = membersOnly.LoadGroupMembers(this.dtGroups, this.dtGroupUsers );
					
					string strGroupLog = "GroupMembers Load ; " + DateTime.Now.ToString();
					str.WriteLine(strGroupLog);
					Console.WriteLine(strGroupLog);

					//loads the records from the dtGroupUsers table to the database
					rowUpdate = this.LoadMembersToDB(dtMembers);	
					string strLogger = "MembersToDB: " + rowUpdate.ToString();

					rowUpdate = this.ExecuteNonQuerySP( "sp_UpdateLDAPUserLoad");  // update ldapUser table with stg info
					strLogger  += "; Rows Scrubbed: " + rowUpdate.ToString();
					str.WriteLine(strLogger);
				}

				this.SqlConn.Close();
			
				this.dtProcessEnd = DateTime.Now;
				System.TimeSpan dtSpan = this.dtProcessEnd.Subtract(dtProcessStart);
				string strEndLog =  "Process End:   " + dtProcessEnd.ToString() + "; Elapse Time: " + dtSpan.TotalMinutes.ToString().Substring(0, dtSpan.TotalMinutes.ToString().IndexOf(".") + 3);   
				str.WriteLine(strEndLog);
				Console.WriteLine(strEndLog);
				strStatus = strEndLog + "\n";

				str.Flush(); // close log file
			}
			catch(Exception except)
			{
                Console.WriteLine("RunLoadProcess Catch: " + except.Message);
                this.ErrorCatch(except);
				this.SqlConn.Close();
			}

		} // END RunLoadProcess

		/// <summary>
		/// This method loops through DomainPaths in the config file to pull 
		/// all the user records from Active directory.
		/// </summary>
		/// <param name="DomainPaths"></param>
		private void GetAllUsers(NameValueCollection DomainPaths)
		{

			for(int x=0; x<DomainPaths.Count; x++)
			{
				string[] paths = DomainPaths.GetValues(DomainPaths.GetKey(x));
				string strPath = paths[0];

				string strFilter = "(&(objectClass=user)(objectclass=person))";
				SearchResultCollection results = this.GetLdapRecords("LDAP://" + strPath, strFilter, this.personArray);

				string strUserLog = "Users:  " + strPath + ": " + results.Count.ToString() + "; " + DateTime.Now.ToString();
				str.WriteLine(strUserLog);
				Console.WriteLine(strUserLog);
				strStatus = strUserLog + "\n";

				// Loads the records to the sql stage table
				this.LoadResultsToTable(results, strPath);
				Console.WriteLine("LoadResultsToTable Completed Users:  " + strPath + ": " + results.Count.ToString() + "; " + DateTime.Now.ToString());
				results.Dispose();
			} // END for loop

		} // END GetAllUsers




		private int LoadMembersToDB(DataTable dtGrpUser)
		{
			
			foreach(DataRow aRow in dtGrpUser.Rows)
			{
				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = this.SqlConn;
				cmd.CommandText = "sp_InsertGroupMember";
				cmd.CommandTimeout=300;

				cmd.Parameters.Add(new SqlParameter("@distinguishedGroup", aRow["distinguishedGroup"].ToString()));
				cmd.Parameters.Add(new SqlParameter("@distinguishedMember", aRow["distinguishedMember"].ToString()));
				cmd.Parameters.Add(new SqlParameter("@memberType", aRow["memberType"].ToString()));

				try
				{   
					int rowsUpdated = cmd.ExecuteNonQuery();
				}
				catch(Exception ex)
				{
					this.ErrorCatch(ex);
					break;
				}
			}

			Console.WriteLine("Start sp_ScrubLdapGroupMemeber: " + DateTime.Now.ToString());

			if(!killProcess)
			{
				// Clean the table
				SqlCommand cmdScrub = new System.Data.SqlClient.SqlCommand();
				cmdScrub.CommandType = CommandType.StoredProcedure;
				cmdScrub.Connection = this.SqlConn;
				cmdScrub.CommandText = "sp_ScrubLdapGroupMemeber";
				cmdScrub.CommandTimeout=300;


				try
				{   
					int rowsUpdated = cmdScrub.ExecuteNonQuery();
				}
				catch(Exception ex)
				{
					this.ErrorCatch(ex);
				}
			}

			return dtGroupUsers.Rows.Count;

		} // END LoadMembersToDB



		private SearchResultCollection GetLdapRecords(string strPath, string strFilter, string[] strArray)
		{
			
			SearchResultCollection domainResults = null;
			try
			{
                Debug.WriteLine("GetLdapRecords: " + strPath + "; Filter: " + strFilter);
                DirectoryEntry de = new DirectoryEntry(strPath);
				DirectorySearcher srch = new DirectorySearcher(de);
				srch.Filter = strFilter;
				
				srch.PropertiesToLoad.AddRange(strArray);
				srch.PageSize =100;
				domainResults = srch.FindAll();
				srch.Dispose();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("GetLdapRecords Error: " + ex.Message + ex.StackTrace);
				this.ErrorCatch(ex);
			}

			if(domainResults == null)
			{
				Debug.WriteLine("domainResults == null for path " + strPath);
			}
			else
				Debug.WriteLine("domainResults records: " + domainResults.Count.ToString() + "; Path: " + strPath);

			return domainResults;
		} // END GetLdapRecords


		private void LoadResultsToTable(SearchResultCollection loadResults, string strDom)
		{
			foreach (SearchResult result in loadResults)
			{
				Person p = new Person();
				// Create Person instance and fill with results data

				p.UserDomain = strDom;

                //foreach (string sKey in personArray)
                //{
                //    Debug.WriteLine(sKey + "; Is Not Null: " + Convert.ToBoolean(result.Properties[sKey] != null).ToString() + "; Count: " + result.Properties[sKey].Count.ToString());
                //   // if(Convert.ToBoolean(result.Properties[sKey] != null))
                //    if (result.Properties[sKey].Count == 0)
                //        Debug.WriteLine("Zero " + sKey + "; value: " + result.Properties[sKey].ToString());
                //    else
                //        Debug.WriteLine(sKey + "; value: " + result.Properties[sKey][0].ToString());
                //}


                if (result.Properties["department"] != null && result.Properties["department"].Count > 0)	
					p.Department = result.Properties["department"][0].ToString();

                if (result.Properties["mail"] != null && result.Properties["mail"].Count > 0)	
					p.Mail = result.Properties["mail"][0].ToString();

                if (result.Properties["distinguishedName"] != null && result.Properties["distinguishedName"].Count > 0)	
					p.DistinguishedName = result.Properties["distinguishedName"][0].ToString();


                //				if(result.Properties["memberof"]!=null)
//					this.LoadMembersToLocalTable(result.Properties["memberof"], p.DistinguishedName, "User");

                if (result.Properties["displayname"] != null && result.Properties["displayname"].Count > 0)	
					p.Displayname = result.Properties["displayname"][0].ToString();

                if (result.Properties["sn"] != null && result.Properties["sn"].Count > 0)	
					p.SN = result.Properties["sn"][0].ToString();

                if (result.Properties["telephonenumber"] != null && result.Properties["telephonenumber"].Count > 0)	
					p.Telephonenumber = result.Properties["telephonenumber"][0].ToString();

                if (result.Properties["samaccountname"] != null && result.Properties["samaccountname"].Count > 0)	
					p.Samaccountname = result.Properties["samaccountname"][0].ToString();

                if (result.Properties["givenname"] != null && result.Properties["givenname"].Count > 0)	
					p.Givenname = result.Properties["givenname"][0].ToString();

                if (result.Properties["streetaddress"] != null && result.Properties["streetaddress"].Count > 0)	
					p.Streetaddress = result.Properties["streetaddress"][0].ToString();

                if (result.Properties["l"] != null && result.Properties["l"].Count > 0)	
					p.L = result.Properties["l"][0].ToString();

                if (result.Properties["co"] != null && result.Properties["co"].Count > 0)	
					p.Co = result.Properties["co"][0].ToString();

                if (result.Properties["st"] != null && result.Properties["st"].Count > 0)	
					p.St = result.Properties["st"][0].ToString();

                if (result.Properties["postalcode"] != null && result.Properties["postalcode"].Count > 0)	
					p.Postalcode = result.Properties["postalcode"][0].ToString();

                if (result.Properties["Title"] != null && result.Properties["Title"].Count > 0)	
					p.Title = result.Properties["Title"][0].ToString();

                if (result.Properties["physicaldeliveryofficename"] != null && result.Properties["physicaldeliveryofficename"].Count > 0)	
					p.Physicaldeliveryofficename = result.Properties["physicaldeliveryofficename"][0].ToString();

                if (result.Properties["manager"] != null && result.Properties["manager"].Count > 0)	
					p.Manager = result.Properties["manager"][0].ToString();

                if (result.Properties["userAccountControl"] != null && result.Properties["userAccountControl"].Count > 0)	
					p.UserAccountControl = result.Properties["userAccountControl"][0].ToString();

                if (result.Properties["mobile"] != null && result.Properties["mobile"].Count > 0)	
					p.Mobile = result.Properties["mobile"][0].ToString();
                
                SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = this.SqlConn;
				cmd.CommandText = "sp_InsertStgLdapUser";
				cmd.CommandTimeout=300;

				cmd.Parameters.Add(new SqlParameter("@userDomain", p.UserDomain)); 
				cmd.Parameters.Add(new SqlParameter("@samaccountname", p.Samaccountname));
				cmd.Parameters.Add(new SqlParameter("@givenname", p.Givenname));
				cmd.Parameters.Add(new SqlParameter("@sn", p.SN));
				cmd.Parameters.Add(new SqlParameter("@displayName", p.Displayname));
				cmd.Parameters.Add(new SqlParameter("@mail", p.Mail));
				cmd.Parameters.Add(new SqlParameter("@distinguishedName", p.DistinguishedName));
				cmd.Parameters.Add(new SqlParameter("@telephonenumber", p.Telephonenumber));
				cmd.Parameters.Add(new SqlParameter("@streetaddress", p.Streetaddress));
				cmd.Parameters.Add(new SqlParameter("@l", p.L));
				cmd.Parameters.Add(new SqlParameter("@co", p.Co));
				cmd.Parameters.Add(new SqlParameter("@st", p.St));
				cmd.Parameters.Add(new SqlParameter("@postalcode", p.Postalcode));
				cmd.Parameters.Add(new SqlParameter("@Title", p.Title));
				cmd.Parameters.Add(new SqlParameter("@Department", p.Department));
				cmd.Parameters.Add(new SqlParameter("@physicaldeliveryofficename", p.Physicaldeliveryofficename)); 
				cmd.Parameters.Add(new SqlParameter("@manager", p.Manager));
				cmd.Parameters.Add(new SqlParameter("@userAccountControl", Int32.Parse(p.UserAccountControl)));
				cmd.Parameters.Add(new SqlParameter("@mobile", p.Mobile));
			
				try
				{   
					int rowsUpdated = cmd.ExecuteNonQuery();
                }
				catch(Exception ex)
				{
					this.ErrorCatch(ex);
					break;
				}
				
			} // END foreach
		} // END LoadResultsToTable

		private void ErrorCatch(Exception ex)
		{
			this.killProcess = true;
			LogException exc = new LogException();
			exc.HandleException(ex, this.strStatus);
			Console.WriteLine(ex.Message);
			str.WriteLine(ex.Message);
			str.WriteLine(ex.StackTrace);
			str.Flush();
		}

/*******************  Group Load *******************************/
		/// <summary>
		/// This method loops through the Domain Paths from the config file 
		/// and pulls all the group record
		/// </summary>
		/// <param name="DomainPaths"></param>
		private void GetAllGroups()
		{
			string strPath = System.Configuration.ConfigurationManager.AppSettings["groupLdapPath"];

			string strFilter = "(objectClass=group)";
			SearchResultCollection results = this.GetLdapRecords(strPath, strFilter, this.groupArray);

			string strGroupLog = "Groups: " + strPath + ": " + results.Count.ToString() + "; " + DateTime.Now.ToString();
			str.WriteLine(strGroupLog);
			Console.WriteLine(strGroupLog);
			strStatus = strGroupLog + "\n";

			this.LoadGroupResultsToTable(results, strPath);
			Console.WriteLine("Groups Loaded to Table: " + strPath + ": " + results.Count.ToString() + "; " + DateTime.Now.ToString());

			results.Dispose();
			//				Console.WriteLine("done with " + strPath);
			//			} // END for loop

		} // END GetAllGroups

//  Commented out for new method with new AD project
//		private void GetAllGroups(NameValueCollection DomainPaths)
//		{
//			for(int x=0; x<DomainPaths.Count; x++)
//			{
//				string[] paths = DomainPaths.GetValues(DomainPaths.GetKey(x));
//				string strPath = paths[0];
//
//				string strFilter = "(objectClass=group)";
//				SearchResultCollection results = this.GetLdapRecords("LDAP://" + strPath, strFilter, this.groupArray);
//
//				string strGroupLog = "Groups: " + strPath + ": " + results.Count.ToString() + "; " + DateTime.Now.ToString();
//				str.WriteLine(strGroupLog);
//				Console.WriteLine(strGroupLog);
//				strStatus = strGroupLog + "\n";
//
//				this.LoadGroupResultsToTable(results, strPath);
//				Console.WriteLine("Groups Loaded to Table: " + strPath + ": " + results.Count.ToString() + "; " + DateTime.Now.ToString());
//				//				Console.WriteLine("done with " + strPath);
//			} // END for loop
//
//		} // END GetAllGroups

		
		private void LoadMembersToLocalTable(ResultPropertyValueCollection r, string strMember, string strType)
		{
			for(int x=0; x<r.Count; x++)
			{
				if(r[x].ToString()=="CN=Domain Admins,CN=Users,DC=ortho,DC=san" || r[x].ToString()=="CN=GIS-Servers,OU=Distribution Lists,OU=US,DC=ortho,DC=san")
					Console.WriteLine("LoadMembersToLocalTable Group: " + r[x].ToString() + "; Member: " + strMember);
				DataRow row = this.dtGroupUsers.NewRow();
				row["distinguishedGroup"] = r[x].ToString();
				row["distinguishedMember"] = strMember;
				row["memberType"] = strType;
				this.dtGroupUsers.Rows.Add(row);
			}
		} //END LoadMembersToTable
		

		private void LoadGroupResultsToTable(SearchResultCollection loadResults, string strDom)
		{
			foreach (SearchResult result in loadResults)
			{
				Group g = new Group();
				// Create Group instance and fill with results data

				g.UserDomain = strDom;

                if (result.Properties["Description"] != null && result.Properties["Description"].Count > 0)	
						g.Description = result.Properties["Description"][0].ToString();

                if (result.Properties["Name"] != null && result.Properties["Name"].Count > 0)	
						g.Name = result.Properties["Name"][0].ToString();

                if (result.Properties["distinguishedName"] != null && result.Properties["distinguishedName"].Count > 0)
						g.DistinguishedName = result.Properties["distinguishedName"][0].ToString();

                if (result.Properties["memberof"] != null && result.Properties["memberof"].Count > 0)
						this.LoadMembersToLocalTable(result.Properties["memberof"], g.DistinguishedName, "Group");

                if (result.Properties["adspath"] != null && result.Properties["adspath"].Count > 0)	
						g.Adspath = result.Properties["adspath"][0].ToString();

                if (result.Properties["samaccountname"] != null && result.Properties["samaccountname"].Count > 0)	
						g.Samaccountname = result.Properties["samaccountname"][0].ToString();

                if (result.Properties["CN"] != null && result.Properties["CN"].Count > 0)	
						g.CN = result.Properties["CN"][0].ToString();


//			Added 11/5/2008 because group members properties has 
//			multiple names beginning with 'member'
//				if(result.Properties["member"]!=null)
//					this.LoadGroupMembers(result.Properties["member"], g.DistinguishedName, "User");
//				if(result.Properties.Contains("member;range=1500-*"))
//				{
//					if(result.Properties["distinguishedName"]!=null)	
//					Console.WriteLine("member;range=1500-*: " + result.Properties["distinguishedName"][0].ToString());
//				}
//
//
				foreach (string strKey in result.Properties.PropertyNames)
				{
					string strLowerKey = strKey.ToLower();
					if(strLowerKey.StartsWith("member") && (strLowerKey != "memberof"))
					{
						if(result.Properties[strLowerKey]!=null)
						{
							// Check size of group members if less than 1450 load them 
							// if over 1450 process them later function membersOnly.LoadGroupMembers

							if(result.Properties[strLowerKey].Count > 1450)
							{
								// used by function LoadGroupMembers to Load the members later in program
								DataRow gRow = dtGroups.NewRow();
								gRow["distinguishedGroup"] = g.DistinguishedName;
								dtGroups.Rows.Add(gRow);
							}
							else
								this.LoadGroupMembers(result.Properties[strLowerKey], g.DistinguishedName, "User");
						}
					}
				}
//			End Add

				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = this.SqlConn;
				cmd.CommandText = "sp_InsertStgLdapGroup";
				cmd.CommandTimeout=300;

				cmd.Parameters.Add(new SqlParameter("@userDomain", g.UserDomain)); 
				cmd.Parameters.Add(new SqlParameter("@samaccountname", g.Samaccountname));
				cmd.Parameters.Add(new SqlParameter("@name", g.Name));
				cmd.Parameters.Add(new SqlParameter("@distinguishedName", g.DistinguishedName));
				cmd.Parameters.Add(new SqlParameter("@description", g.Description));
				cmd.Parameters.Add(new SqlParameter("@adspath", g.Adspath));
				cmd.Parameters.Add(new SqlParameter("@cn", g.CN));

				try
				{   
					int rowsUpdated = cmd.ExecuteNonQuery();
				}
				catch(Exception ex)
				{
					Console.WriteLine("sp_InsertStgLdapGroup error: " + g.DistinguishedName);
					this.ErrorCatch(ex);
					break;
				}
				
			} // END foreach

		} // END LoadGroupResultsToTable

		private void LoadGroupMembers(ResultPropertyValueCollection r, string strGroup, string strType)
		{
			for(int x=0; x<r.Count; x++)
			{
				DataRow row = this.dtGroupUsers.NewRow();
				row["distinguishedGroup"] = strGroup;
				row["distinguishedMember"] = r[x].ToString();
				row["memberType"] = strType;
				this.dtGroupUsers.Rows.Add(row);
			}
		} //END LoadMembersToTable
		
		private int ExecuteNonQuerySP(string strSP)
		{
			SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Connection = this.SqlConn;
			cmd.CommandText = strSP;
			cmd.CommandTimeout=300;

			int rowsUpdated = 0;
			try
			{   
				rowsUpdated = cmd.ExecuteNonQuery();
				strTempStat =  strSP + "Rows effected: " + rowsUpdated.ToString();
				Console.WriteLine(strTempStat);
				strStatus = strTempStat + "\n";
			}
			catch(Exception ex)
			{
				this.ErrorCatch(ex);
			}
		
			return rowsUpdated;
		} // END ExecuteNonQuerySP


/********************* Load Group Members ************************************/

		public DataTable CreateGroupsTable()
		{
			DataTable dt = new DataTable();
			dt = new DataTable();
			dt.Columns.Add(new DataColumn("distinguishedGroup", typeof(String)));
			dt.Columns.Add(new DataColumn("memberType", typeof(String)));
			return dt;
		}

		
		public DataTable CreateGroupUsersTable()
		{
			DataTable dt = new DataTable();
			dt = new DataTable();
			dt.Columns.Add(new DataColumn("distinguishedMember", typeof(String)));
			dt.Columns.Add(new DataColumn("distinguishedGroup", typeof(String)));
			dt.Columns.Add(new DataColumn("memberType", typeof(String)));
			return dt;
		}
		
	}// end class
}
