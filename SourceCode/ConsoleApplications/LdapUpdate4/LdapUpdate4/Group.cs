using System;

namespace LdapUpdate
{
	/// <summary>
	/// Summary description for Group.
	/// </summary>
	public class Group
	{

		protected string strDescription = "";	
		protected string strAdspath = "";	
		protected string strDistinguishedName = "";	
		protected string strName = "";
		protected string strSamaccountname = "";	
		protected string strCN = "";
		protected string strUserDomain;

		public Group()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string UserDomain
		{
			get {return strUserDomain;}
			set {strUserDomain = value;}
		}

		public string CN
		{
			get {return strCN;}
			set {strCN = value;}
		}

		public string DistinguishedName
		{
			get {return strDistinguishedName;}
			set {strDistinguishedName = value;}
		}

		public string Name
		{
			get {return strName;}
			set {strName = value;}
		}

		public string Adspath
		{
			get {return strAdspath;}
			set {strAdspath = value;}
		}

		public string Description
		{
			get {return strDescription;}
			set {strDescription = value;}
		}

		public string Samaccountname
		{
			get {return strSamaccountname;}
			set {strSamaccountname = value;}
		}

	} // END class
}
