using System;

namespace LdapUpdate
{
	/// <summary>
	/// Summary description for Person.
	/// </summary>
	public class Person
	{
		
		protected string strUserDomain = "";	
		protected string strDepartment = "";	
		protected string strMail = "";	
		protected string strDistinguishedName = "";	
		protected string strDisplayname = "";	
		protected string strSN = "";	
		protected string strTelephonenumber = "";	
		protected string strSamaccountname = "";	
		protected string strGivenname = "";	
		protected string strStreetaddress = "";	
		protected string strL = "";	
		protected string strCo = "";	
		protected string strSt = "";	
		protected string strPostalcode = "";	
		protected string strTitle = "";	
		protected string strPhysicaldeliveryofficename = "";	
		protected string strManager = "";	
		protected string strUserAccountControl = "0";
		protected string strMobile = "";	

		public Person()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/* Getters and Setters */

		public string UserDomain
		{
			get {return strUserDomain;}
			set {strUserDomain = value;}
		}

		public string Department
		{
			get {return strDepartment;}
			set {strDepartment = value;}
		}

		public string Mail
		{
			get {return strMail;}
			set {strMail = value;}
		}

		public string DistinguishedName
		{
			get {return strDistinguishedName;}
			set {strDistinguishedName = value;}
		}

		public string Displayname
		{
			get {return strDisplayname;}
			set {strDisplayname = value;}
		}
		
		public string SN
		{
			get {return strSN;}
			set {strSN = value;}
		}
		
		public string Telephonenumber
		{
			get {return strTelephonenumber;}
			set {strTelephonenumber = value;}
		}
		
		public string Samaccountname
		{
			get {return strSamaccountname;}
			set {strSamaccountname = value;}
		}
		
		public string Givenname
		{
			get {return strGivenname;}
			set {strGivenname = value;}
		}
		
		public string Streetaddress
		{
			get {return strStreetaddress;}
			set {strStreetaddress = value;}
		}

		public string L
		{
			get {return strL;}
			set {strL = value;}
		}

		public string Co
		{
			get {return strCo;}
			set {strCo = value;}
		}

		public string St
		{
			get {return strSt;}
			set {strSt = value;}
		}

		public string Postalcode
		{
			get {return strPostalcode;}
			set {strPostalcode = value;}
		}

		public string Title
		{
			get {return strTitle;}
			set {strTitle = value;}
		}


		public string Physicaldeliveryofficename
		{
			get {return strPhysicaldeliveryofficename;}
			set {strPhysicaldeliveryofficename = value;}
		}

		public string Manager
		{
			get {return strManager;}
			set {strManager = value;}
		}

		public string UserAccountControl
		{
			get {return strUserAccountControl;}
			set {strUserAccountControl = value;}
		}

		public string Mobile
		{
			get {return strMobile;}
			set {strMobile = value;}
		}


	}// END class
}
