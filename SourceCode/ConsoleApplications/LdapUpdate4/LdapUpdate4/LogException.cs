using System;
using System.Web;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
//using System.Web.Mail;
using System.Configuration;
using System.Net.Mail;

namespace LdapUpdate
{	 
	public class LogException  
	{
		public LogException( ) //ctor
		{ 
		}
	
		public void HandleException(Exception ex, string strProcessLog)
		{
			Console.WriteLine("Error: "+ ex.Message);
            //int evtId = 0;
			string strData=String.Empty;
			bool logIt = Convert.ToBoolean(ConfigurationManager.AppSettings["logErrors"]);
			if(logIt)
			{
				Console.WriteLine("In logIt");
                //string dbConnString=	ConfigurationManager.AppSettings["logDbConnString"].ToString();
                //string strLogServer=	ConfigurationManager.AppSettings["logServer"].ToString();
				
				string logDateTime =DateTime.Now.ToString();
                strData = "\nENVIRONMENT: " + ConfigurationManager.AppSettings["Environment"];
                strData += "\nSOURCE: " + ex.Source + "\nLogDateTime: " + logDateTime;
				strData += "\nMESSAGE: " +ex.Message;
				strData += "\nTARGETSITE: " + ex.TargetSite;
				strData += "\nSTACKTRACE: " + ex.StackTrace;
				strData += "\nAPPLICATION: LdapUpdate";
				strData += "\nUSER:  Console\n";
                //strData += "\nSERVER:  " + strLogServer + "\n";
				strData += "\nPROCESS LOG: \n" + strProcessLog + "\n";

                if (ex.InnerException != null)
                {
                    strData += "\n0 - INNER EXCEPTION : " + LogException.GetInnerMessage(ex.InnerException, 0);
                }
                else
                {
                    strData += "\n0 - INNER EXCEPTION : is null";
                }

                try
                {
                    Debug.WriteLine(strData);
                    LogException.OpenLogFile(strData);
                }
                catch (Exception excp)
                {
                    strData += "\nMESSAGE: " + excp.Message;
                    strData += "\nSTACKTRACE: " + excp.StackTrace;
                }

                //if(dbConnString.Length >0)
                //{                 	
                //    SqlCommand cmd = new SqlCommand();
                //    cmd.CommandType=CommandType.StoredProcedure;
                //    cmd.CommandText="usp_WebAppLogsInsert";
                //    SqlConnection cn = new SqlConnection(dbConnString);
                //    cmd.Connection=cn;
                //    cn.Open();  					
                //    try
                //    {	
                //        cmd.Parameters.Add(new SqlParameter("@Source", ex.Source));
                //        cmd.Parameters.Add(new SqlParameter("@Message",ex.Message));									
                //        cmd.Parameters.Add(new SqlParameter("@Form",String.Empty));
                //        cmd.Parameters.Add(new SqlParameter("@QueryString",String.Empty));
                //        cmd.Parameters.Add(new SqlParameter("@TargetSite",ex.TargetSite.ToString()));
                //        cmd.Parameters.Add(new SqlParameter("@StackTrace",ex.StackTrace.ToString()));
                //        cmd.Parameters.Add(new SqlParameter("@Referer",String.Empty));
                //        cmd.Parameters.Add(new SqlParameter("@AppUser","Console"));

                //        SqlParameter outParm=new SqlParameter("@EventId",SqlDbType.Int);
                //        outParm.Direction =ParameterDirection.Output;
                //        cmd.Parameters.Add(outParm);							
                //        cmd.ExecuteNonQuery();
                //        evtId =Convert.ToInt32(cmd.Parameters[8].Value);
                //        cmd.Dispose();
                //        cn.Close();			
                //    }
                //    catch (Exception exc)
                //    {
                //        Console.WriteLine("usp_WebAppLogsInsert Error: " + exc.Message);	
                //        throw;
                //    }
                //    finally
                //    {
                //    cmd.Dispose();
                //    cn.Close();
                //    } // END SQL Insert


                //}  // END dbConnString.Length >0 
			} // END if(logIt)
	
			// Send email notification
			// Email receiptient list should be delimited by |
            //string strEmails =ConfigurationManager.AppSettings["logEmailAddresses"].ToString();
            //if (strEmails.Length >0) 
            //{
            //    string[] emails = strEmails.Split(Convert.ToChar("|"));
            //    MailMessage msg = new MailMessage();
            //    msg.BodyFormat = MailFormat.Text;
            //    msg.To         = emails[0];

            //    for (int i =1;i<emails.Length;i++)
            //        msg.Cc=emails[i];

            //    msg.From = ConfigurationManager.AppSettings["logFromEmail"].ToString();
            //    msg.Subject =  "LpadUpdate Application Error";
            //    string detailURL=	ConfigurationManager.AppSettings["logDetailURL"].ToString();
            //    msg.Body  =  strData + detailURL +"?EvtId="+ evtId.ToString(); 
            //    SmtpMail.SmtpServer = ConfigurationManager.AppSettings["logSmtpServer"].ToString();
            //    try
            //    {				
            //        SmtpMail.Send(msg);
            //    }
            //    catch (Exception excm )
            //    {
            //        Console.WriteLine(excm.Message);
            //        throw;
            //    }
            //}



            //// Send email notification
            //// Email receiptient list should be delimited by |
            bool sendLog = Convert.ToBoolean(ConfigurationManager.AppSettings["SendLog"]);

            if (sendLog)
            {
                string strEmails = ConfigurationManager.AppSettings["logEmailAddresses"];
                if (strEmails.Length > 0)
                {
                    MailAddress from = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"]);

                    //// Set destinations for the e-mail message.
                    string[] emails = strEmails.Split(Convert.ToChar("|"));
                    System.Net.Mail.MailMessage msg = new MailMessage();
                    MailAddress to = new MailAddress(emails[0]);

                    //// Specify the message content.
                    MailMessage message = new MailMessage(from, to);
                    Console.WriteLine("From: " + message.From.Address);

                    message.IsBodyHtml = false;
                    message.Subject = ConfigurationManager.AppSettings["ProgramName"] + " Error";

                    message.Body = strData;

                    SmtpClient client = new SmtpClient();
                    Console.WriteLine("client: " + client.Host);

                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception excm)
                    {
                        Debug.WriteLine(excm.Message);
                     //   string strDailyLogFile = ConfigurationManager.AppSettings["DailyLogFile"];

                        LogException.OpenLogFile("SEND ERROR LOG ERROR: " + excm.Message);
                    }
                }
            } //// end if(SendLog)


		} // END HandleException(Exception ex)


        /// <summary>Gets the inner message.</summary>
        /// <param name="innerMsg">The inner message.</param>
        /// <param name="cnt">The message count.</param>
        /// <returns>Returns a string containing the Inner Message</returns>
        private static string GetInnerMessage(Exception innerMsg, int cnt)
        {
            string strInnerMsg = "  " + cnt.ToString() + "- Inner MESSAGE: " + innerMsg.Message;
            strInnerMsg += " STACKTRACE: " + innerMsg.StackTrace;

            cnt++;
            if (innerMsg.InnerException != null && cnt < 8)
            {
                strInnerMsg += LogException.GetInnerMessage(innerMsg.InnerException, cnt);
            }
            else
            {
                strInnerMsg += " " + cnt.ToString() + "- Inner Exception is null";
            }

            return strInnerMsg;
        }

        /// <summary>Opens the log file.</summary>
        /// <param name="strMsg">The message.</param>
        /// <param name="strLogFile">The log file folder.</param>
        private static void OpenLogFile(string strMsg)
        {
            string strLogFile = ConfigurationManager.AppSettings["ErrorLogFile"];
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strLogFile);
                System.IO.StreamWriter str = fileLog.AppendText();

                string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
                str.WriteLine(strTime + strMsg + "\n");
                str.Flush(); //// close log file
                str.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Writing Log: " + ex.Message);
            }
        } //// END OpenLogFile


	} // END class
} // END namespace
