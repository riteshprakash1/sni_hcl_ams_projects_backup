using System;

namespace LdapUpdate
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			AppDomain currentDomain = AppDomain.CurrentDomain;
			currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);

			Utility util = new Utility();
			util.RunLoadProcess();

     //       Console.ReadLine();

		} // END Main

		static void MyHandler(object sender, UnhandledExceptionEventArgs args) 
		{
			try
			{
				Exception e = (Exception) args.ExceptionObject;
				LogException exc = new LogException();
				exc.HandleException(e, "In Class1 Exception");
			}
			catch(Exception ex)
			{
				Console.WriteLine("ExceptionHandler Error: " + ex.Message);
			}

		} // END MyHandler



	} // END class1
}
