﻿// --------------------------------------------------------------
// <copyright file="LogException.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace MobileOrderFormExtractLoad
{
    using System;
    using System.Data;
    using System.Configuration;
    using System.Diagnostics;
    using System.Net.Mail;

    /// <summary>This class handles and log exceptions when they are thrown.</summary>
    public class LogException
    {
        /// <summary>Initializes a new instance of the <see cref="LogException"/> class.</summary>
        public LogException()
        {
        }

        /// <summary>Handles the exception.</summary>
        /// <param name="ex">The exception object.</param>
        /// <param name="req">The HttpRequest object.</param>
        public static void HandleException(Exception ex, string strExecutionLocation)
        {
            // if it is not this exception - handle it
            if (!ex.Message.StartsWith("Thread was being aborted")
                && !ex.Message.StartsWith("Invalid length for a Base-64 char array")
                && !ex.Message.StartsWith("Unable to validate data")
                )
            {
                string strData = String.Empty;

                bool logIt = Convert.ToBoolean(ConfigurationManager.AppSettings["logErrors"]);
                if (logIt)
                {
                    string referer = String.Empty;
                    string logDateTime = DateTime.Now.ToString();
                    string strQuery = ConfigurationManager.AppSettings["ProgramName"];

                    strData = "\nENVIRONMENT: " + ConfigurationManager.AppSettings["Environment"];
                    strData += "\nExecutionLocation: " + strExecutionLocation;
                    strData += "\nSOURCE: " + ex.Source + "\nLogDateTime: " + logDateTime;
                    strData += "\nMESSAGE: " + ex.Message;
                    strData += "\nQUERYSTRING: " + strQuery + "\nTARGETSITE: " + ex.TargetSite;
                    strData += "\nSTACKTRACE: " + ex.StackTrace;

                    if (ex.InnerException != null)
                    {
                        strData += "\n0 - INNER EXCEPTION : " + LogException.GetInnerMessage(ex.InnerException, 0);
                    }
                    else
                    {
                        strData += "\n0 - INNER EXCEPTION : is null";
                    }

                    try
                    {
                        Debug.WriteLine(strData);
                        LogException.OpenLogFile(strData);
                    }
                    catch (Exception excp)
                    {
                        strData += "\nMESSAGE: " + excp.Message;
                        strData += "\nSTACKTRACE: " + excp.StackTrace;
                    }
                } //// END if(logIt)

                //// Send email notification
                //// Email receiptient list should be delimited by |
                bool sendLog = Convert.ToBoolean(ConfigurationManager.AppSettings["SendLog"]);

                if (sendLog)
                {
                    string strEmails = ConfigurationManager.AppSettings["logEmailAddresses"];
                    if (strEmails.Length > 0)
                    {
                        MailAddress from = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"]);

                        //// Set destinations for the e-mail message.
                        string[] emails = strEmails.Split(Convert.ToChar("|"));
                        System.Net.Mail.MailMessage msg = new MailMessage();
                        MailAddress to = new MailAddress(emails[0]);

                        //// Specify the message content.
                        MailMessage message = new MailMessage(from, to);
                        Console.WriteLine("From: " + message.From.Address);

                        message.IsBodyHtml = false;
                        message.Subject = ConfigurationManager.AppSettings["ProgramName"] + " Error";

                        message.Body = strData;

                        SmtpClient client = new SmtpClient();
                        Console.WriteLine("client: " + client.Host);

                        try
                        {
                            client.Send(message);
                        }
                        catch (Exception excm)
                        {
                            Debug.WriteLine(excm.Message);
                            string strDailyLogFile = ConfigurationManager.AppSettings["DailyLogFile"];

                            LogException.OpenLogFile("SEND ERROR LOG ERROR: " + excm.Message);
                        }
                    }
                } //// end if(SendLog)
            }
        } //// END HandleException(Exception ex)

        /// <summary>Gets the inner message.</summary>
        /// <param name="innerMsg">The inner message.</param>
        /// <param name="cnt">The message count.</param>
        /// <returns>Returns a string containing the Inner Message</returns>
        private static string GetInnerMessage(Exception innerMsg, int cnt)
        {
            string strInnerMsg = "  " + cnt.ToString() + "- Inner MESSAGE: " + innerMsg.Message;
            strInnerMsg += " STACKTRACE: " + innerMsg.StackTrace;

            cnt++;
            if (innerMsg.InnerException != null && cnt < 8)
            {
                strInnerMsg += LogException.GetInnerMessage(innerMsg.InnerException, cnt);
            }
            else
            {
                strInnerMsg += " " + cnt.ToString() + "- Inner Exception is null";
            }

            return strInnerMsg;
        }

        /// <summary>Opens the log file.</summary>
        /// <param name="strMsg">The message.</param>
        /// <param name="strLogFile">The log file folder.</param>
        private static void OpenLogFile(string strMsg)
        {
            string strLogFile = ConfigurationManager.AppSettings["ErrorLogFile"];
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strLogFile);
                System.IO.StreamWriter str = fileLog.AppendText();

                string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
                str.WriteLine(strTime + strMsg + "\n");
                str.Flush(); //// close log file
                str.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Writing Log: " + ex.Message);
            }
        } //// END OpenLogFile
    } //// END class
}
