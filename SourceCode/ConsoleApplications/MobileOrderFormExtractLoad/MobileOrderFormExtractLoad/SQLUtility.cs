﻿//---------------------------------------------------
// <copyright file="SQLUtility.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
//---------------------------------------------------
namespace MobileOrderFormExtractLoad
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.OleDb;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Diagnostics;

    /// <summary>This is SQL helper class.</summary>
    public sealed class SQLUtility
    {

        /// <summary>Prevents a default instance of the <see cref="SQLUtility"/> class from being created.</summary>
        private SQLUtility()
        {
        }

        /// <summary>Execute non query and return the parameter collection used to get values in Output parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="SqlConn">The SqlConnection.</param>
        /// <param name="myTransaction">The SqlTransaction.</param>
        /// <returns>The parameters.</returns>
        public static SqlParameterCollection SqlExecuteNonQueryTransaction(string storedProcedureName, Collection<SqlParameter> parameters, SqlConnection SqlConn, SqlTransaction myTransaction)
        {
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn, myTransaction);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            return command.Parameters;
        }

        /// <summary>Execute non query and return the parameter collection used to get values in Output parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="SqlConn">The SqlConnection.</param>
        /// <returns>The number of records affected</returns>
        public static int SqlExecuteNonQuery(string storedProcedureName, Collection<SqlParameter> parameters, SqlConnection SqlConn)
        {
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            return intCommandOutcome;
        }

        /// <summary>Execute non query Transaction and return number of records affected.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="SqlConn">The SqlConnection.</param>
        /// <param name="myTransaction">The SqlTransaction.</param>
        /// <returns>The number of records affected</returns>
        public static int SqlExecuteNonQueryTransactionCount(string storedProcedureName, Collection<SqlParameter> parameters, SqlConnection SqlConn, SqlTransaction myTransaction)
        {
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn, myTransaction);

            int intTimeout = 120;
            if (ConfigurationManager.AppSettings["SqlTransactionTimeout"] != null)
            {
                intTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SqlTransactionTimeout"]);
            }

            command.CommandTimeout = intTimeout;
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            return intCommandOutcome;
        }

        /// <summary>Execute non query and return the parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static Collection<SqlParameter> SqlExecuteNonQueryReturnParameters(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection SqlConn = SQLUtility.OpenSqlConnection();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            SQLUtility.CloseSqlConnection(SqlConn);

            return parameters;
        }


        /// <summary>
        /// Execute non query and return the number of updated records.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static int SqlExecuteNonQueryCount(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection SqlConn = SQLUtility.OpenSqlConnection();
            SqlTransaction myTransaction = SqlConn.BeginTransaction();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn, myTransaction);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            myTransaction.Commit();

            SQLUtility.CloseSqlConnection(SqlConn);

            return intCommandOutcome;
        }

        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            SqlConnection SqlConn = SQLUtility.OpenSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = SqlConn;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            SQLUtility.CloseSqlConnection(SqlConn);

            return myDataTable;
        }

        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            SqlConnection SqlConn = SQLUtility.OpenSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = SqlConn;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            SQLUtility.CloseSqlConnection(SqlConn);

            return myDataTable;
        }


        /// <summary>SQLs the execute query.</summary>
        /// <param name="conn">The SQL conn.</param>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <returns>DataTable</returns>
        public static DataTable SqlExecuteStoredProcedure(SqlConnection conn, string storedProcedureName)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;

            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            return myDataTable;
        }


        /// <summary>Executes a query.</summary>
        /// <param name="sQuery">The text of the query to execute</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteDynamicQuery(string sQuery)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            SqlConnection SqlConn = SQLUtility.OpenSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = SqlConn;
            command.CommandType = CommandType.Text;
            command.CommandText = sQuery;

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            SQLUtility.CloseSqlConnection(SqlConn);

            return myDataTable;
        }


        /// <summary>Closes the SQL connection.</summary>
        /// <param name="connection">The SQL connection.</param>
        public static void CloseSqlConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            else if (connection.State == ConnectionState.Broken)
            {
                connection.Close();
            }
        }


        /// <summary>Opens a new SQL connection.</summary>
        /// <returns>Returns a SQL connection</returns>
        public static SqlConnection OpenSqlConnection()
        {
            SqlConnection myConnection = new SqlConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["sqlConn"].ToString();
                myConnection.Open();
            }

            return myConnection;

        }

        /// <summary>Opens a new SQL connection.</summary>
        /// <param name="strConnName">The connection string name</param>
        /// <returns>Returns a SQL connection</returns>
        public static SqlConnection OpenSqlConnection(string strConnName)
        {
            SqlConnection myConnection = new SqlConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings[strConnName].ToString();
                myConnection.Open();
            }

            return myConnection;

        }


        /// <summary>Opens a new GDW SQL connection.</summary>
        /// <returns>Returns a SQL connection</returns>
        public static SqlConnection OpenGdwSqlConnection()
        {
            SqlConnection myConnection = new SqlConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["GdwConn"].ToString();
                myConnection.Open();
            }

            return myConnection;

        }

        /// <summary>Executes a dynamic query.</summary>
        /// <param name="sQuery">The text of the query to execute</param>
        /// <param name="sConn">The OleDbConnection</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteDynamicExeclQuery(string sQuery, string strFileName)
        {
            Debug.WriteLine("strFileName: " + strFileName);
            OleDbConnection myConnection = new OleDbConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ExcelConn"].ToString();
                myConnection.ConnectionString = myConnection.ConnectionString.Replace("EXCELFILEPATH", strFileName);
                Debug.WriteLine("ExcelConn: " + myConnection.ConnectionString);
                myConnection.Open();
            }

            
            DataTable myDataTable = new DataTable();

            OleDbCommand command = new OleDbCommand();
            command.Connection = myConnection;
            command.CommandTimeout = 120;
            command.CommandType = CommandType.Text;
            command.CommandText = sQuery;

            Debug.WriteLine("Excel Query: " + sQuery);

            OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            myConnection.Close();

            return myDataTable;
        }


    }
}