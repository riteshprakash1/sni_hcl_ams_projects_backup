﻿// ------------------------------------------------------------------
// <copyright file="DataExtractEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace MobileOrderFormExtractLoad
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    class DataExtractEntity
    {
        
        /// <summary>This is the DataExtractID.</summary>
        private int intDataExtractID;

        /// <summary>This is the Stage Count.</summary>
        private int intStageCount;

        /// <summary>This is the Application Count.</summary>
        private int intApplicationCount;

        /// <summary>This is the Application Records Updated.</summary>
        private int intApplicationRecordsUpdated;

        /// <summary>This is the Extract File Name.</summary>
        private string strExtractFileName;

        /// <summary>This is the Extract Job.</summary>
        private string strExtractJob;

        /// <summary>This is the Dtsx File Name.</summary>
        private string strDtsxFileName;

        /// <summary>This is the Extract Load Results.</summary>
        private string strExtractLoadResults;

        /// <summary>This is the Extract Load Results Message.</summary>
        private string strExtractLoadResultsMessage;

        /// <summary>This is the StageTable.</summary>
        private string strStageTable;
        
        /// <summary>This is the ApplicationTable.</summary>
        private string strApplicationTable;

        /// <summary>This is the Application Update Stored Procedure.</summary>
        private string strApplicationUpdateStoredProcedure;        

        /// <summary>Initializes a new instance of the DataExtractEntity class.</summary>
        public DataExtractEntity()
        {
            this.ApplicationRecordsUpdated = 0;
            this.ApplicationCount = 0;
            this.StageCount = 0;
            this.ExtractFileName = String.Empty;
            this.ExtractJob = String.Empty;
            this.DtsxFileName = String.Empty;
            this.ExtractLoadResults = String.Empty;
            this.ExtractLoadResultsMessage = String.Empty;
            this.StageTable = String.Empty;
            this.ApplicationTable = String.Empty;
            this.ApplicationUpdateStoredProcedure = String.Empty;
        }

        /// <summary>Gets or sets the DataExtractID.</summary>
        /// <value>The DataExtractID.</value>
        public int DataExtractID
        {
            get { return this.intDataExtractID; }
            set { this.intDataExtractID = value; }
        }

        /// <summary>Gets or sets the Stage Count.</summary>
        /// <value>The Stage Count.</value>
        public int StageCount
        {
            get { return this.intStageCount; }
            set { this.intStageCount = value; }
        }

        /// <summary>Gets or sets the Application Count.</summary>
        /// <value>The Application Count.</value>
        public int ApplicationCount
        {
            get { return this.intApplicationCount; }
            set { this.intApplicationCount = value; }
        }

        /// <summary>Gets or sets the Application Records Updated.</summary>
        /// <value>The Application Records Updated.</value>
        public int ApplicationRecordsUpdated
        {
            get { return this.intApplicationRecordsUpdated; }
            set { this.intApplicationRecordsUpdated = value; }
        }

        /// <summary>Gets or sets the ExtractFileName.</summary>
        /// <value>The ExtractFileName.</value>
        public string ExtractFileName
        {
            get { return this.strExtractFileName; }
            set { this.strExtractFileName = value; }
        }

        /// <summary>Gets or sets the Extract Job.</summary>
        /// <value>The Extract Job.</value>
        public string ExtractJob
        {
            get { return this.strExtractJob; }
            set { this.strExtractJob = value; }
        }

        /// <summary>Gets or sets the Dtsx FileName.</summary>
        /// <value>The Dtsx FileName.</value>
        public string DtsxFileName
        {
            get { return this.strDtsxFileName; }
            set { this.strDtsxFileName = value; }
        }

        /// <summary>Gets or sets the Extract Load Results.</summary>
        /// <value>The Extract Load Results.</value>
        public string ExtractLoadResults
        {
            get { return this.strExtractLoadResults; }
            set { this.strExtractLoadResults = value; }
        }

        /// <summary>Gets or sets the Extract Load Results Message.</summary>
        /// <value>The Extract Load Results Message.</value>
        public string ExtractLoadResultsMessage
        {
            get { return this.strExtractLoadResultsMessage; }
            set { this.strExtractLoadResultsMessage = value; }
        }

        /// <summary>Gets or sets the StageTable.</summary>
        /// <value>The StageTable.</value>
        public string StageTable
        {
            get { return this.strStageTable; }
            set { this.strStageTable = value; }
        }

        /// <summary>Gets or sets the ApplicationTable.</summary>
        /// <value>The ApplicationTable.</value>
        public string ApplicationTable
        {
            get { return this.strApplicationTable; }
            set { this.strApplicationTable = value; }
        }

        /// <summary>Gets or sets the Application Update Stored Procedure.</summary>
        /// <value>The Application Update Stored Procedure.</value>
        public string ApplicationUpdateStoredProcedure
        {
            get { return this.strApplicationUpdateStoredProcedure; }
            set { this.strApplicationUpdateStoredProcedure = value; }
        }

        

        /// <summary>Receives a DataExtract datarow and converts it to a DataExtractEntity.</summary>
        /// <param name="r">The DataExtractEntity DataRow.</param>
        /// <returns>DataExtractEntity</returns>
        protected static DataExtractEntity GetEntityFromDataRow(DataRow r)
        {
            DataExtractEntity re = new DataExtractEntity();

            if ((r["DataExtractID"] != null) && (r["DataExtractID"] != DBNull.Value))
                re.DataExtractID = Convert.ToInt32(r["DataExtractID"].ToString());

            re.DtsxFileName = r["DtsxFileName"].ToString();
            re.ExtractFileName = r["ExtractFileName"].ToString();
            re.ExtractJob = r["ExtractJob"].ToString();
            re.ApplicationTable = r["ApplicationTable"].ToString();
            re.StageTable = r["StageTable"].ToString();
            re.ApplicationUpdateStoredProcedure = r["ApplicationUpdateStoredProcedure"].ToString();

            return re;
        }

        /// <summary>Get the Record Count by table name.</summary>
        /// <param name="strTableName">The Table name to get the record count on.</param>
        /// <returns>number of records in the table</returns>
        public static int GetRecordCountsByTableName(string strTableName)
        {
            string strQueryCount = "SELECT COUNT(*) as RecCount FROM " + strTableName;
            DataTable dt = SQLUtility.SqlExecuteDynamicQuery(strQueryCount);

            int intRecCount = 0;
            if(dt.Rows.Count > 0)
                intRecCount = Convert.ToInt32(dt.Rows[0]["RecCount"].ToString());

            return intRecCount;
        }

        /// <summary>Gets all data extracts and return a DataExtractEntity List</summary>
        /// <returns>The newly populated DataExtractEntity List</returns>
        public static List<DataExtractEntity> GetDataExtracts()
        {
            List<DataExtractEntity> extracts = new List<DataExtractEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetDataExtracts", parameters);

            foreach (DataRow r in dt.Rows)
            {
                DataExtractEntity entity = DataExtractEntity.GetEntityFromDataRow(r);
                extracts.Add(entity);
            }

            return extracts;
        }
    }////end class
}
