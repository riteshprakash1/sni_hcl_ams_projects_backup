﻿// --------------------------------------------------------------
// <copyright file="LoadRunner.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace MobileOrderFormExtractLoad
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.IO;
    using System.Data;
    using System.Data.SqlClient;
    using System.Net.Mail;
    using System.Text;
    using Microsoft.SqlServer.Dts.Runtime;

    class LoadRunner
    {
        /// <summary>Initializes a new instance of the LoadRunner class.</summary>
        public LoadRunner()
        {

        }

        /// <summary>This method executes the daily extract load process.</summary>
        public static void ExecuteDailyLoad()
        {
            try
            {
                DateTime startTime = DateTime.Now;

                Console.WriteLine(startTime.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": Starting ExecuteDailyLoad");
                LoadRunner.OpenLogFile("Starting ExecuteDailyLoad");

                //Any file in the ExtractFolder that does not have a file extension will be renamed with a .txt extension
                LoadRunner.RenameEmptyExtensionFiles();

                //Get the list of data extracts and loops thru it
                List<DataExtractEntity> extracts = DataExtractEntity.GetDataExtracts();
                foreach (DataExtractEntity e in extracts)
                {
                    DataExtractEntity extract = LoadStageFromExtractFile(e);
                    extract = LoadRunner.GetTableRecordCounts(extract);
                }

                // Executes Stored Procedures to update the application tables from the stage tables
                extracts = LoadRunner.UpdateApplicationTables(extracts);

                // Sets ExtractLoadResults to "Succeeded" when not failed
                extracts = LoadRunner.UpdateLoadResultSucceeded(extracts);
                
                // Moves the files from the Current file to a datetime stamped 'Loaded' folder
                LoadRunner.MoveExtractsToLoadedFolder();

                //This runs the Maintenance Stored Procedure(s).
                LoadRunner.RunMaintenanceProcedures("sp_UpdateSaleRepAccounts_sap");
                LoadRunner.RunMaintenanceProcedures("sp_InsertMaterialSalesOrgs");
                LoadRunner.RunMaintenanceProcedures("sp_UpdateSalesRepInventoryFromPendingOrders");

                //This deletes subfolders in the 'Loaded' folder that are old based on config file key LoadedExtractFolderDeleteDays
                LoadRunner.RemoveOldLoadedFolders();

                DateTime endTime = DateTime.Now;
                TimeSpan duration = endTime.Subtract(startTime);
                string strRunTime = "Execution Time: " + duration.Minutes.ToString() + " minutes and " + duration.Seconds.ToString() + " seconds";

                //Sends email summary based on config setting and processing success
                LoadRunner.SendEmailSummary(extracts, strRunTime);

                Console.WriteLine(endTime.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": Ending ExecuteDailyLoad");
                LoadRunner.OpenLogFile("Ending ExecuteDailyLoad");
            }
            catch (Exception exc)
            {
                Debug.WriteLine("ExecuteDailyLoad: " + exc.Message);
                Console.WriteLine("ExecuteDailyLoad: " + exc.Message);
                LogException.HandleException(exc, "ExecuteDailyLoad Error");
            }
        }

        /// <summary>Sends email summary based on config setting and processing success</summary>
        public static void SendEmailSummary(List<DataExtractEntity> extracts, string strRunTime)
        {
            bool blnProcessingSucceeded = true;
            foreach (DataExtractEntity e in extracts)
            {
                if (e.ExtractLoadResults.ToLower() == "failed")
                    blnProcessingSucceeded = false;
            }

            // the config setting should be OnError, Always, Never
            string strSendSummary = ConfigurationManager.AppSettings["SendSummary"].ToLower();

            if (strSendSummary == "always")
            {
                string strSummaryBody = EmailSummaryBody(extracts, strRunTime);
                SendEmailSummary(strSummaryBody);
            }
            else
            {
                if (strSendSummary == "onerror")
                {
                    if (blnProcessingSucceeded == false)
                    {
                        string strSummaryBody = EmailSummaryBody(extracts, strRunTime);
                        SendEmailSummary(strSummaryBody);
                    }
                }
            } //// end else
        }


        /// <summary>Sets ExtractLoadResults to "Succeeded" when not failed</summary>
        /// <param name="extracts">The List of DataExtractEntity</param>
        /// <returns>The Updated List of DataExtractEntity</returns>
        public static List<DataExtractEntity> UpdateLoadResultSucceeded(List<DataExtractEntity> extracts)
        {
            foreach (DataExtractEntity e in extracts)
            {
                if (e.ExtractLoadResults.ToLower() != "failed")
                    e.ExtractLoadResults = "Succeeded";
            }
            return extracts;
        }

        /// <summary>Checks whether the number of records in stage is close enough to number of records in application table.  Uses percent from Config file.</summary>
        /// <param name="extracts">The List of DataExtractEntity</param>
        /// <param name="strRunTime">The processing run time (duration)</param>
        /// <returns>true/false; true if close enough</returns>
        public static string EmailSummaryBody(List<DataExtractEntity> extracts, string strRunTime)
        {
            string strSummary = "<table width='800px' border='1' >";
            strSummary += "<tr>";
            strSummary += "<td colspan='6'><b>" + strRunTime + "</b></td>";
            strSummary += "</tr>";
            strSummary += "<tr>";
            strSummary += "<td colspan='6'>&nbsp;</td>";
            strSummary += "</tr>";

            strSummary += "<tr>";
            strSummary += "<td><b>" + "Extract Job" + "</b></td>";
            strSummary += "<td><b>" + "Results" + "</b></td>";
            strSummary += "<td><b>" + "Results Message" + "</b></td>";
            strSummary += "<td><b>" + "Stage" + "</b></td>";
            strSummary += "<td><b>" + "Application" + "</b></td>";
            strSummary += "<td><b>" + "Updated" + "</b></td>";

            foreach (DataExtractEntity e in extracts)
            {
                strSummary += "<tr>";
                strSummary += "<td>" + e.ExtractJob +"</td>";
                strSummary += "<td>" + e.ExtractLoadResults + "</td>";
                strSummary += "<td>" + e.ExtractLoadResultsMessage + "</td>";
                strSummary += "<td>" + e.StageCount.ToString() + "</td>";
                strSummary += "<td>" + e.ApplicationCount.ToString() + "</td>";
                strSummary += "<td>" + e.ApplicationRecordsUpdated.ToString() + "</td>";
                strSummary += "</tr>";
            }

            strSummary += "</table>";

            return strSummary;
        }
        /// <summary>Checks whether the number of records in stage is close enough to number of records in application table.  Uses percent from Config file.</summary>
        /// <param name="e">The DataExtractEntity</param>
        /// <returns>true/false; true if close enough</returns>
        public static bool RecordCountPassed(DataExtractEntity e)
        {
            bool blnRunCheckCount = Convert.ToBoolean(ConfigurationManager.AppSettings["CheckCount"]);
            
            if(blnRunCheckCount)
            {
                bool blnCntPassed = false;
                int intCheckCountPercent = Convert.ToInt32(ConfigurationManager.AppSettings["CheckCountPercent"]);

                if (e.ApplicationCount != 0)
                {
                     Console.Write("CheckCount " + e.ExtractJob + "; StageCount: " + e.StageCount.ToString() + "; ApplicationCount: " + e.ApplicationCount.ToString());
                     decimal actualCntPercent = ((decimal)e.StageCount / e.ApplicationCount)*100;
                    blnCntPassed = Convert.ToBoolean(actualCntPercent > intCheckCountPercent);
                    Console.WriteLine("; actualCntPercent: " + actualCntPercent.ToString() + "; Passed: "  + blnCntPassed.ToString());
                }
                else
                    blnCntPassed = true;

                return blnCntPassed;
            }

            return true;
        }

        /// <summary>This method loops thru the list of DataExtracts and executes the noted store procedure which 
        ///          updates the Application table from the data in the associated staging table.
        /// </summary>
        /// <param name="extracts">The List of DataExtractEntity</param>
        /// <returns>The Updated List of DataExtractEntity</returns>
        public static List<DataExtractEntity> UpdateApplicationTables(List<DataExtractEntity> extracts)
        {
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            string strExecutionLocation = "UpdateApplicationTables Error - ";

            try
            {
                foreach (DataExtractEntity e in extracts)
                {
                    // Load the Application Table if the process has failed earlier for the DataExtractEntity
                    if (e.ExtractLoadResults.ToLower() != "failed")
                    {
                        // Checks whether the number of records in stage is close enough to number of records in application table
                        if (RecordCountPassed(e))
                        {
                            strExecutionLocation = "UpdateApplicationTables Error - " + e.ExtractJob;
                            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                            //myParamters.Add(new SqlParameter("@PricingRequestID", pr.PricingRequestID));

                            e.ApplicationRecordsUpdated = SQLUtility.SqlExecuteNonQueryTransactionCount(e.ApplicationUpdateStoredProcedure, myParamters, conn, trans);
                        }
                        else
                        {
                            e.ExtractLoadResults = "failed";
                            e.ApplicationRecordsUpdated = 0;
                            e.ExtractLoadResultsMessage = "Check Record Counts Failed";
                        }

                        LoadRunner.OpenLogFile("Load " + e.ApplicationTable + ": " + e.ApplicationRecordsUpdated.ToString() + " records updated");
                    }
                 }

                trans.Commit();
                SQLUtility.CloseSqlConnection(conn);
            }
            catch (Exception ex)
            {
                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);

                LoadRunner.OpenLogFile("UpdateApplicationTables Rolledback");
                Debug.WriteLine("strExecutionLocation: " + ex.Message);
                Console.WriteLine(strExecutionLocation + ": " + ex.Message);
                LogException.HandleException(ex, strExecutionLocation);
            }

            return extracts;
        }

        /// <summary>This runs the Maintenance Stored Procedure(s).</summary>
        /// <param name="strStoredProdecure">The StoredProdecure</param>
        /// <returns>The ran successfully true/false</returns>
        public static bool RunMaintenanceProcedures(string strStoredProdecure)
        {
            bool blnWasSuccessful = true;
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            string strExecutionLocation = "RunMaintenanceProcedures " + strStoredProdecure + ": ";

            try
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryTransactionCount(strStoredProdecure, myParamters, conn, trans);

                trans.Commit();
                SQLUtility.CloseSqlConnection(conn);

                LoadRunner.OpenLogFile(strExecutionLocation + " completed - " + intRecordsUpdated.ToString() + " records updated");
            }
            catch (Exception ex)
            {
                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);
                blnWasSuccessful = false;

                LoadRunner.OpenLogFile(strExecutionLocation + " Rolledback");
                Debug.WriteLine(strExecutionLocation + " Error: " + ex.Message);
                Console.WriteLine(strExecutionLocation + ": " + ex.Message);
                LogException.HandleException(ex, strExecutionLocation);
            }

            return blnWasSuccessful;
        }

        /// <summary>Any file in the ExtractFolder that does not have a file extension will be renamed with a .txt extension</summary>
        public static void RenameEmptyExtensionFiles()
        {
            DirectoryInfo d = new DirectoryInfo(ConfigurationManager.AppSettings["ExtractFolder"]);
            foreach (FileInfo f in d.GetFiles())
            {
                if (f.Extension == string.Empty)
                {
                    Console.WriteLine("Renamed " + f.FullName + " to " + f.FullName + ".txt");
                    f.MoveTo(f.FullName + ".txt");

                    LoadRunner.OpenLogFile("Renamed " + f.FullName + " to " + f.FullName + ".txt");

                }
            }


        }

        /// <summary>Off Loads all the files in the Current folder to a date stamped loaded folder</summary>
        public static void MoveExtractsToLoadedFolder()
        {
            DateTime myTime = DateTime.Now;
            string strLoadedFolderName = myTime.Year.ToString() + myTime.Month.ToString() + myTime.Day.ToString() + myTime.Hour.ToString() + myTime.Minute.ToString() + myTime.Second.ToString();

            DirectoryInfo newOffLoadDir = Directory.CreateDirectory(ConfigurationManager.AppSettings["LoadedExtractFolder"] + strLoadedFolderName);

            DirectoryInfo extractDir = new DirectoryInfo(ConfigurationManager.AppSettings["ExtractFolder"]);
            foreach (FileInfo f in extractDir.GetFiles())
            {
                string strNewFileName = newOffLoadDir.FullName + "\\" + f.Name;
                f.MoveTo(strNewFileName);
                Console.WriteLine("Moving to: " + strNewFileName);
                LoadRunner.OpenLogFile("Moving to: " + strNewFileName);
            }
        }

        /// <summary>This deletes subfolders in the 'Loaded' folder that are old based on config file key LoadedExtractFolderDeleteDays</summary>
        public static void RemoveOldLoadedFolders()
        {
            int intDeleteDays = Convert.ToInt32(ConfigurationManager.AppSettings["LoadedExtractFolderDeleteDays"]);

            DirectoryInfo loadedDir = Directory.CreateDirectory(ConfigurationManager.AppSettings["LoadedExtractFolder"]);

            foreach (DirectoryInfo d in loadedDir.GetDirectories())
            {
                DateTime createDate = d.CreationTime;
                DateTime todayDate = DateTime.Now;

                TimeSpan span = todayDate.Subtract(createDate);
                if (span.Days > intDeleteDays)
                {
                    d.Delete(true);
                    Console.WriteLine("Deleting Folder: " + d.FullName);
                    LoadRunner.OpenLogFile("Deleting Folder: " + d.FullName);
                }
            }
        }


        /// <summary>Get the record count for the Stage and Application table of the identified extract.</summary>
        /// <param name="extract">The DataExtractEntity.</param>
        /// <returns>the updated DataExtractEntity object with the record counts</returns>
        public static DataExtractEntity GetTableRecordCounts(DataExtractEntity extract)
        {
            try
            {
                extract.StageCount = DataExtractEntity.GetRecordCountsByTableName(extract.StageTable);
                extract.ApplicationCount = DataExtractEntity.GetRecordCountsByTableName(extract.ApplicationTable);
                LoadRunner.OpenLogFile("TableRecordCounts- StageCount: " + extract.StageCount.ToString() + ";  ApplicationCount: " + extract.ApplicationCount.ToString());
            }
            catch (Exception excp)
            {
                string strExecutionLocation = "GetTableRecordCounts Error - " + extract.ExtractJob;
                Debug.WriteLine(strExecutionLocation + ": " + excp.Message);
                Console.WriteLine(strExecutionLocation + ": " + excp.Message);
                LogException.HandleException(excp, strExecutionLocation);

                extract.ExtractLoadResults = "failed";
                extract.ExtractLoadResultsMessage = "GetTableRecordCounts Error: " + excp.Message;
            }

            return extract;
        }

        /// <summary>Loads the staging table from the extract file for the extract identified in the DataExtractEntity object.</summary>
        /// <param name="extract">The DataExtractEntity.</param>
        /// <returns>the updated DataExtractEntity object with status of the load</returns>
        public static DataExtractEntity LoadStageFromExtractFile(DataExtractEntity extract)
        {
            try
            {
                string strMessage = string.Empty;
                string strExtractFile = ConfigurationManager.AppSettings["ExtractFolder"] + extract.ExtractFileName;

                if (File.Exists(strExtractFile))
                {
                    Application app = new Application();
                    Package package = null;
                    //Load the SSIS Package which will be executed
                    //   package = app.LoadPackage(@"C:\Users\colwelld\Documents\Visual Studio 2008\Projects\MobileOrderFormIntegration\MobileOrderFormIntegration\BlackListLoad.dtsx", null);

                    string strFolder = ConfigurationManager.AppSettings["SSIS_Folder"];
                    package = app.LoadPackage(strFolder + extract.DtsxFileName, null);

                    //Debug.WriteLine("Locked: " + package.Variables.Locked.ToString());
                    //foreach (Microsoft.SqlServer.Dts.Runtime.Variable v in package.Variables)
                    //{
                    //    Debug.WriteLine("QualifiedName: " + v.QualifiedName + "; Name: " + v.Name + "; ID: " + v.ID + "; ReadOnly: " + v.ReadOnly.ToString());
                    //}

                    //Pass the varibles into SSIS Package
                    //package.Variables["User::DB_DatabaseName"].Value = "SN_MobileOrderForm";        
                    package.Variables["User::DB_DatabaseName"].Value = ConfigurationManager.AppSettings["SSIS_DB"];
                    package.Variables["User::DB_ServerName"].Value = ConfigurationManager.AppSettings["SSIS_Server"];
                    package.Variables["User::DB_Password"].Value = ConfigurationManager.AppSettings["SSIS_Pwd"];
                    package.Variables["User::DB_UserName"].Value = ConfigurationManager.AppSettings["SSIS_User"];

                    package.Variables["User::ExtractFolder"].Value = ConfigurationManager.AppSettings["ExtractFolder"];
                    package.Variables["User::ExtractFileName"].Value = extract.ExtractFileName;

                    //Execute the SSIS Package and store the Execution Result
                    Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();
                    extract.ExtractLoadResults = results.ToString();

                    //Check the results for Failure and Success
                    if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure)
                    {
                        extract.ExtractLoadResults = "failed";
                        string err = "";
                        foreach (Microsoft.SqlServer.Dts.Runtime.DtsError local_DtsError in package.Errors)
                        {
                            string error = local_DtsError.Description.ToString();
                            strMessage = err + error;
                            extract.ExtractLoadResultsMessage = strMessage;
                        }
                    }
                    if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success)
                    {
                        //extract.ExtractLoadResults = "succeeded";
                        strMessage = "Executed Successfully....";
                    }

                    Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": " + extract.ExtractJob + " Package: " + strMessage);
                    LoadRunner.OpenLogFile(extract.ExtractJob + " Package: " + strMessage);
                    //You can also return the error or Execution Result
                    //return Error;
                } /// end File EXISTS
                else
                {
                    strMessage = "Extract File not found: " + strExtractFile;
                    extract.ExtractLoadResults = "failed";
                    extract.ExtractLoadResultsMessage = strMessage;
                    LoadRunner.OpenLogFile(extract.ExtractJob + " Package: " + strMessage);
                }
            }
            catch (Exception excp)
            {
                extract.ExtractLoadResults = "failed";
                extract.ExtractLoadResultsMessage = "LoadStageFromExtractFile Error: " + excp.Message;
                string strExecutionLocation = "RunPackage Error - " + extract.ExtractJob;
                Debug.WriteLine(strExecutionLocation + ": " + excp.Message);
                Console.WriteLine(strExecutionLocation + ": " + excp.Message);
                LogException.HandleException(excp, strExecutionLocation);
            }

            return extract;
        }


        /// <summary>Sends an email with the body content from strBody.</summary>
        /// <param name="strBody">The email body.</param>
        public static void SendEmailSummary(string strBody)
        {
            Console.WriteLine("From: " + ConfigurationManager.AppSettings["SummaryFromEmail"]);

            MailMessage message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["SummaryFromEmail"]);

            string EmailToList = ConfigurationManager.AppSettings["SummaryToEmail"];
            string[] recipients = EmailToList.Split(';');

            foreach (string sTo in recipients)
            {
                Console.WriteLine("To: " + sTo);
                message.To.Add(new MailAddress(sTo));
            }

            message.IsBodyHtml = true;
            message.Subject = "Mobile IMS Extract Load Summary (" + ConfigurationManager.AppSettings["SSIS_Server"] + ")";

            message.Body = strBody;

            SmtpClient client = new SmtpClient();
            Console.WriteLine("client: " + client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Debug.WriteLine(excm.Message);
                string strDailyLogFile = ConfigurationManager.AppSettings["DailyLogFile"];
                Console.WriteLine("SendEmailSummary: " + excm.Message);

                LogException.HandleException(excm, "SendEmailSummary");
            }
        }

        /// <summary>Opens the log file.</summary>
        /// <param name="strMsg">The message.</param>
        public static void OpenLogFile(string strMsg)
        {
            string strLogFile = ConfigurationManager.AppSettings["DailyLogFile"];
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strLogFile);
                System.IO.StreamWriter str = fileLog.AppendText();

                string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
                str.WriteLine(strTime + strMsg + "\n");
                str.Flush(); //// close log file
                str.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Writing Log: " + ex.Message);
            }
        } //// END OpenLogFile


    }//// end class
}
