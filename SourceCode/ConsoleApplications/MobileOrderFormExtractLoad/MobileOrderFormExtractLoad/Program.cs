﻿// --------------------------------------------------------------
// <copyright file="Program.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------


namespace MobileOrderFormExtractLoad
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>This is the program class where the program starts.</summary>
    class Program
    {
        static void Main(string[] args)
        {
            LoadRunner.ExecuteDailyLoad();
            //LoadRunner.SendEmailSummary("<table><tr><td>Testing</td></tr></table>");
        }
    }
}
