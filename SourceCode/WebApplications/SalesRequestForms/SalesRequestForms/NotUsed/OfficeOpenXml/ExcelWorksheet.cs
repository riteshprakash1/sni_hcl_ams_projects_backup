// ---------------------------------------------------------------
// <copyright file="ExcelWorksheet.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ---------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.IO;
    using System.IO.Packaging;
    using System.Xml;
    using System.Collections.Generic;


    /// <summary>
    /// Represents an Excel worksheet and provides access to its properties and methods
    /// </summary>
    public class ExcelWorksheet
    {
        /// <summary>
        /// Temporary tag for all column numbers in the worksheet XML
        /// For internal use only!
        /// </summary>
        protected internal const string TempColumnNumberTag = "colNumber";

        /// <summary>
        /// Reference to the parent package
        /// For internal use only!
        /// </summary>
        private ExcelPackage myXlsPackage;

        /// <summary>
        /// The worksheet uri.
        /// </summary>
        private Uri myWorksheetUri;

        /// <summary>
        /// THe worksheet name.
        /// </summary>
        private string myName;

        /// <summary>
        /// The worksheet ID.
        /// </summary>
        private int mySheetID;

        /// <summary>
        /// The hidden flag.
        /// </summary>
        private bool myHidden;

        /// <summary>
        /// The relationship id.
        /// </summary>
        private string myRelationshipID;

        /// <summary>
        /// The worksheet xml.
        /// </summary>
        private XmlDocument myWorksheetXml;

        /// <summary>
        /// The sheet view.
        /// </summary>
        private ExcelWorksheetView mySheetView;

        /// <summary>
        /// The header footer object.
        /// </summary>
        private ExcelHeaderFooter myHeaderFooter;

        /// <summary>
        /// The namespace manager.
        /// </summary>
        private XmlNamespaceManager myNsManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelWorksheet"/> class.
        /// </summary>
        /// <param name="parentXlPackage">The parent xl package.</param>
        /// <param name="relationshipID">The relationship ID.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <param name="uriWorksheet">The URI worksheet.</param>
        /// <param name="sheetID">The sheet ID.</param>
        /// <param name="hide">if set to <c>true</c> [hide].</param>
        protected internal ExcelWorksheet(
            ExcelPackage parentXlPackage,
            string relationshipID,
            string sheetName,
            Uri uriWorksheet,
            int sheetID,
            bool hide)
        {
            this.XlsPackage = parentXlPackage;
            this.myRelationshipID = relationshipID;
            this.myWorksheetUri = uriWorksheet;
            this.myName = sheetName;
            ////_type = Type;
            this.mySheetID = sheetID;
            this.Hidden = hide;
        }

        /// <summary>
        /// Gets access to a namespace manager instance to allow XPath searching
        /// </summary>
        public XmlNamespaceManager NameSpaceManager
        {
            get
            {
                if (this.myNsManager == null)
                {
                    NameTable nt = new NameTable();
                    this.myNsManager = new XmlNamespaceManager(nt);
                    this.myNsManager.AddNamespace("d", ExcelPackage.SchemaMain);
                }

                return this.myNsManager;
            }
        }

        /// <summary>
        /// Gets a ExcelWorksheetView object that allows you to
        /// set the view state properties of the worksheet
        /// </summary>
        public ExcelWorksheetView View
        {
            get
            {
                if (this.mySheetView == null)
                {
                    this.mySheetView = new ExcelWorksheetView(this);
                }

                return this.mySheetView;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating worksheet's name as it appears on the tab
        /// </summary>
        public string Name
        {
            get
            {
                return this.myName;
            }

            set
            {
                XmlNode sheetNode = this.XlsPackage.Workbook.WorkbookXml.SelectSingleNode(string.Format("//d:sheet[@sheetId={0}]", this.mySheetID), this.NameSpaceManager);
                if (sheetNode != null)
                {
                    XmlAttribute nameAttr = (XmlAttribute)sheetNode.Attributes.GetNamedItem("name");
                    if (nameAttr != null)
                    {
                        nameAttr.Value = value;
                    }
                }

                this.myName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the worksheet is hidden in the workbook
        /// </summary>
        public bool Hidden
        {
            get
            {
                return this.myHidden;
            }

            set
            {
                XmlNode sheetNode = this.XlsPackage.Workbook.WorkbookXml.SelectSingleNode(string.Format("//d:sheet[@sheetId={0}]", this.mySheetID), this.NameSpaceManager);
                if (sheetNode != null)
                {
                    XmlAttribute nameAttr = (XmlAttribute)sheetNode.Attributes.GetNamedItem("hidden");
                    if (nameAttr != null)
                    {
                        nameAttr.Value = value.ToString();
                    }
                }

                this.myHidden = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating default height of all rows in the worksheet
        /// </summary>
        public int DefaultRowHeight
        {
            get
            {
                int retValue = 15; //// Excel's default height
                XmlElement sheetFormat = (XmlElement)this.WorksheetXml.SelectSingleNode("//d:sheetFormatPr", this.NameSpaceManager);
                if (sheetFormat != null)
                {
                    string ret = sheetFormat.GetAttribute("defaultRowHeight");
                    if (ret != String.Empty)
                    {
                        retValue = int.Parse(ret);
                    }
                }

                return retValue;
            }

            set
            {
                XmlElement sheetFormat = (XmlElement)this.WorksheetXml.SelectSingleNode("//d:sheetFormatPr", this.NameSpaceManager);
                if (sheetFormat == null)
                {
                    //// create the node as it does not exist
                    sheetFormat = this.WorksheetXml.CreateElement("sheetFormatPr", ExcelPackage.SchemaMain);
                    //// find location to insert new element
                    XmlNode sheetViews = this.WorksheetXml.SelectSingleNode("//d:sheetViews", this.NameSpaceManager);
                    //// insert the new node
                    this.WorksheetXml.DocumentElement.InsertAfter(sheetFormat, sheetViews);
                }

                sheetFormat.SetAttribute("defaultRowHeight", value.ToString());
            }
        }

        /// <summary>
        /// Gets XML document holding all the worksheet data.
        /// </summary>
        public XmlDocument WorksheetXml
        {
            get
            {
                if (this.myWorksheetXml == null)
                {
                    this.myWorksheetXml = new XmlDocument();
                    PackagePart packPart = this.XlsPackage.Package.GetPart(this.WorksheetUri);
                    this.myWorksheetXml.Load(packPart.GetStream());
                    //// convert worksheet into the type of XML we like dealing with
                    this.AddNumericCellIDs();
                }

                return this.myWorksheetXml;
            }
        }

        /// <summary>
        /// Gets reference to the header and footer class which allows you to 
        /// set the header and footer for all odd, even and first pages of the worksheet
        /// </summary>
        public ExcelHeaderFooter HeaderFooter
        {
            get
            {
                if (this.myHeaderFooter == null)
                {
                    XmlNode headerFooterNode = this.WorksheetXml.SelectSingleNode("//d:headerFooter", this.NameSpaceManager);
                    if (headerFooterNode == null)
                    {
                        headerFooterNode = this.WorksheetXml.DocumentElement.AppendChild(this.WorksheetXml.CreateElement("headerFooter", ExcelPackage.SchemaMain));
                    }

                    this.myHeaderFooter = new ExcelHeaderFooter((XmlElement)headerFooterNode);
                }

                return this.myHeaderFooter;
            }
        }

        /// <summary>
        /// Gets or sets the XLS package.
        /// </summary>
        /// <value>The XLS package.</value>
        protected internal ExcelPackage XlsPackage
        {
            get
            {
                return this.myXlsPackage;
            }

            set
            {
                this.myXlsPackage = value;
            }
        }

        /// <summary>
        /// Gets Read-only: the Uri to the worksheet within the package
        /// </summary>
        protected internal Uri WorksheetUri
        {
            get
            {
                return this.myWorksheetUri;
            }
        }

        /// <summary>
        /// Gets Read-only: a reference to the PackagePart for the worksheet within the package
        /// </summary>
        protected internal PackagePart Part
        {
            get
            {
                return this.XlsPackage.Package.GetPart(this.WorksheetUri);
            }
        }

        /// <summary>
        /// Gets Read-only: the ID for the worksheet's relationship with the workbook in the package
        /// </summary>
        protected internal string RelationshipID
        {
            get
            {
                return this.myRelationshipID;
            }
        }

        /// <summary>
        /// Gets unique identifier for the worksheet.  Note that these can be random, so not
        /// too useful in code!
        /// </summary>
        protected internal int SheetID
        {
            get
            {
                return this.mySheetID;
            }
        }

        /// <summary>
        /// Provides access to an individual cell within the worksheet.
        /// </summary>
        /// <param name="row">The row number in the worksheet</param>
        /// <param name="col">The column number in the worksheet</param>
        /// <returns>A new cell</returns>
        public ExcelCell Cell(int row, int col)
        {
            return new ExcelCell(this, row, col);
        }

        /// <summary>
        /// Provides access to an individual row within the worksheet so you can set its properties.
        /// </summary>
        /// <param name="row">The row number in the worksheet</param>
        /// <returns>A new row.</returns>
        public ExcelRow Row(int row)
        {
            return new ExcelRow(this, row);
        }

        /// <summary>
        /// Provides access to an individual column within the worksheet so you can set its properties.
        /// </summary>
        /// <param name="col">The column number in the worksheet</param>
        /// <returns>A new column</returns>
        public ExcelColumn Column(int col)
        {
            return new ExcelColumn(this, col);
        }

        /// <summary>
        /// Creates a shared formula based on the formula already in startCell
        /// Essentially this supports the formula attributes such as t="shared" ref="B2:B4" si="0"
        /// as per Brian Jones: Open XML Formats blog. See
        /// http://blogs.msdn.com/brian_jones/archive/2006/11/15/simple-spreadsheetml-file-part-2-of-3.aspx
        /// </summary>
        /// <param name="startCell">The cell containing the formula</param>
        /// <param name="endCell">The end cell (i.e. end of the range)</param>
        public void CreateSharedFormula(ExcelCell startCell, ExcelCell endCell)
        {
            XmlElement formulaElement;
            string formula = startCell.Formula;
            if (formula == String.Empty)
            {
                throw new Exception("CreateSharedFormula Error: startCell does not contain a formula!");
            }

            //// find or create a shared formula ID
            int sharedID = -1;
            foreach (XmlNode node in this.myWorksheetXml.SelectNodes("//d:sheetData/d:row/d:c/d:f/@si", this.NameSpaceManager))
            {
                int curID = int.Parse(node.Value);
                if (curID > sharedID)
                {
                    sharedID = curID;
                }
            }

            sharedID++;  //// first value must be zero

            for (int row = startCell.Row; row <= endCell.Row; row++)
            {
                for (int col = startCell.Column; col <= endCell.Column; col++)
                {
                    ExcelCell cell = this.Cell(row, col);

                    //// to force Excel to re-calculate the formula, we must remove the value
                    cell.RemoveValue();

                    formulaElement = (XmlElement)cell.Element.SelectSingleNode("./d:f", this.NameSpaceManager);
                    if (formulaElement == null)
                    {
                        formulaElement = cell.AddFormulaElement();
                    }

                    formulaElement.SetAttribute("t", "shared");
                    formulaElement.SetAttribute("si", sharedID.ToString());
                }
            }

            //// finally add the shared cell range to the startCell
            formulaElement = (XmlElement)startCell.Element.SelectSingleNode("./d:f", this.NameSpaceManager);
            formulaElement.SetAttribute("ref", string.Format("{0}:{1}", startCell.CellAddress, endCell.CellAddress));
        }

        /// <summary>
        /// Inserts conditional formatting for the cell range.
        /// Currently only supports the dataBar style.
        /// </summary>
        /// <param name="startCell">The cell to start with.</param>
        /// <param name="endCell">The cell to end with.</param>
        /// <param name="color">What to color the cells.</param>
        public void CreateConditionalFormatting(ExcelCell startCell, ExcelCell endCell, string color)
        {
            XmlNode formatNode = this.WorksheetXml.SelectSingleNode("//d:conditionalFormatting", this.NameSpaceManager);
            if (formatNode == null)
            {
                formatNode = this.WorksheetXml.CreateElement("conditionalFormatting", ExcelPackage.SchemaMain);
                XmlNode prevNode = this.WorksheetXml.SelectSingleNode("//d:mergeCells", this.NameSpaceManager);
                if (prevNode == null)
                {
                    prevNode = this.WorksheetXml.SelectSingleNode("//d:sheetData", this.NameSpaceManager);
                }

                this.WorksheetXml.DocumentElement.InsertAfter(formatNode, prevNode);
            }

            XmlAttribute attr = formatNode.Attributes["sqref"];
            if (attr == null)
            {
                attr = this.WorksheetXml.CreateAttribute("sqref");
                formatNode.Attributes.Append(attr);
            }

            attr.Value = string.Format("{0}:{1}", startCell.CellAddress, endCell.CellAddress);

            XmlNode node = formatNode.SelectSingleNode("./d:cfRule", this.NameSpaceManager);
            if (node == null)
            {
                node = this.WorksheetXml.CreateElement("cfRule", ExcelPackage.SchemaMain);
                formatNode.AppendChild(node);
            }

            attr = node.Attributes["type"];
            if (attr == null)
            {
                attr = this.WorksheetXml.CreateAttribute("type");
                node.Attributes.Append(attr);
            }

            attr.Value = "dataBar";

            attr = node.Attributes["priority"];
            if (attr == null)
            {
                attr = this.WorksheetXml.CreateAttribute("priority");
                node.Attributes.Append(attr);
            }

            attr.Value = "1";

            //// the following is poor code, but just an example!!!
            XmlNode databar = this.WorksheetXml.CreateElement("databar", ExcelPackage.SchemaMain);
            node.AppendChild(databar);

            XmlNode child = this.WorksheetXml.CreateElement("cfvo", ExcelPackage.SchemaMain);
            databar.AppendChild(child);
            attr = this.WorksheetXml.CreateAttribute("type");
            child.Attributes.Append(attr);
            attr.Value = "min";
            attr = this.WorksheetXml.CreateAttribute("val");
            child.Attributes.Append(attr);
            attr.Value = "0";

            child = this.WorksheetXml.CreateElement("cfvo", ExcelPackage.SchemaMain);
            databar.AppendChild(child);
            attr = this.WorksheetXml.CreateAttribute("type");
            child.Attributes.Append(attr);
            attr.Value = "max";
            attr = this.WorksheetXml.CreateAttribute("val");
            child.Attributes.Append(attr);
            attr.Value = "0";

            child = this.WorksheetXml.CreateElement("color", ExcelPackage.SchemaMain);
            databar.AppendChild(child);
            attr = this.WorksheetXml.CreateAttribute("rgb");
            child.Attributes.Append(attr);
            attr.Value = color;
        }

        /// <summary>
        /// Inserts a new row into the spreadsheet.  Existing rows below the insersion position are 
        /// shifted down.  All formula are updated to take account of the new row.
        /// </summary>
        /// <param name="position">The position of the new row</param>
        public void InsertRow(int position)
        {
            XmlNode rowNode = null;
            //// create the new row element
            XmlElement rowElement = this.WorksheetXml.CreateElement("row", ExcelPackage.SchemaMain);
            rowElement.Attributes.Append(this.WorksheetXml.CreateAttribute("r"));
            rowElement.Attributes["r"].Value = position.ToString();

            XmlNode sheetDataNode = this.WorksheetXml.SelectSingleNode("//d:sheetData", this.NameSpaceManager);
            if (sheetDataNode != null)
            {
                int renumberFrom = 1;
                XmlNodeList nodes = sheetDataNode.ChildNodes;
                int nodeCount = nodes.Count;
                XmlNode insertAfterRowNode = null;
                int insertAfterRowNodeID = 0;
                for (int i = 0; i < nodeCount; i++)
                {
                    int currentRowID = int.Parse(nodes[i].Attributes["r"].Value);
                    if (currentRowID < position)
                    {
                        insertAfterRowNode = nodes[i];
                        insertAfterRowNodeID = i;
                    }

                    if (currentRowID >= position)
                    {
                        renumberFrom = currentRowID;
                        break;
                    }
                }

                //// update the existing row ids
                for (int i = insertAfterRowNodeID + 1; i < nodeCount; i++)
                {
                    int currentRowID = int.Parse(nodes[i].Attributes["r"].Value);
                    if (currentRowID >= renumberFrom)
                    {
                        nodes[i].Attributes["r"].Value = Convert.ToString(currentRowID + 1);

                        //// now update any formula that are in the row 
                        XmlNodeList formulaNodes = nodes[i].SelectNodes("./d:c/d:f", this.NameSpaceManager);
                        foreach (XmlNode formulaNode in formulaNodes)
                        {
                            formulaNode.InnerText = ExcelCell.UpdateFormulaReferences(formulaNode.InnerText, 1, 0, position, 0);
                        }
                    }
                }

                //// now insert the new row
                if (insertAfterRowNode != null)
                {
                    rowNode = sheetDataNode.InsertAfter(rowElement, insertAfterRowNode);
                }
            }
        }

        /// <summary>
        /// Deletes the specified row from the worksheet.
        /// If shiftOtherRowsUp=true then all formula are updated to take account of the deleted row.
        /// </summary>
        /// <param name="rowToDelete">The number of the row to be deleted</param>
        /// <param name="shiftOtherRowsUp">Set to true if you want the other rows renumbered so they all move up</param>
        public void DeleteRow(int rowToDelete, bool shiftOtherRowsUp)
        {
            XmlNode sheetDataNode = this.WorksheetXml.SelectSingleNode("//d:sheetData", this.NameSpaceManager);
            if (sheetDataNode != null)
            {
                XmlNodeList nodes = sheetDataNode.ChildNodes;
                int nodeCount = nodes.Count;
                int rowNodeID = 0;
                XmlNode rowNode = null;
                for (int i = 0; i < nodeCount; i++)
                {
                    int currentRowID = int.Parse(nodes[i].Attributes["r"].Value);
                    if (currentRowID == rowToDelete)
                    {
                        rowNodeID = i;
                        rowNode = nodes[i];
                    }
                }

                if (shiftOtherRowsUp)
                {
                    //// update the existing row ids
                    for (int i = rowNodeID + 1; i < nodeCount; i++)
                    {
                        int currentRowID = int.Parse(nodes[i].Attributes["r"].Value);
                        if (currentRowID > rowToDelete)
                        {
                            nodes[i].Attributes["r"].Value = Convert.ToString(currentRowID - 1);

                            //// now update any formula that are in the row 
                            XmlNodeList formulaNodes = nodes[i].SelectNodes("./d:c/d:f", this.NameSpaceManager);
                            foreach (XmlNode formulaNode in formulaNodes)
                            {
                                formulaNode.InnerText = ExcelCell.UpdateFormulaReferences(formulaNode.InnerText, -1, 0, rowToDelete, 0);
                            }
                        }
                    }
                }
                //// delete the row
                if (rowNode != null)
                {
                    sheetDataNode.RemoveChild(rowNode);
                }
            }
        }

        /// <summary>
        /// Saves the worksheet to the package.  For internal use only.
        /// </summary>
        protected internal void Save()  //// Worksheet Save
        {
            //// we also need to delete the relationship from the pageSetup tag
            XmlNode pageSetup = this.myWorksheetXml.SelectSingleNode("//d:pageSetup", this.NameSpaceManager);
            if (pageSetup != null)
            {
                XmlAttribute attr = (XmlAttribute)pageSetup.Attributes.GetNamedItem("id", ExcelPackage.SchemaRelationships);
                if (attr != null)
                {
                    string relID = attr.Value;
                    //// first delete the attribute from the XML
                    pageSetup.Attributes.Remove(attr);

                    //// get the URI
                    PackageRelationship relPrinterSettings = this.Part.GetRelationship(relID);
                    Uri printerSettingsUri = new Uri("/xl" + relPrinterSettings.TargetUri.ToString().Replace("..", String.Empty), UriKind.Relative);

                    //// now delete the relationship
                    this.Part.DeleteRelationship(relPrinterSettings.Id);

                    //// now delete the part from the package
                    this.XlsPackage.Package.DeletePart(printerSettingsUri);
                }
            }

            if (this.myWorksheetXml != null)
            {
                //// save the header & footer (if defined)
                if (this.myHeaderFooter != null)
                {
                    this.HeaderFooter.Save();
                }
                //// replace the numeric Cell IDs we inserted with AddNumericCellIDs()
                this.ReplaceNumericCellIDs();

                //// save worksheet to package
                PackagePart partPack = this.XlsPackage.Package.GetPart(this.WorksheetUri);
                this.WorksheetXml.Save(partPack.GetStream(FileMode.Create, FileAccess.Write));
                this.XlsPackage.WriteDebugFile(this.WorksheetXml, @"xl\worksheets", "sheet" + this.SheetID + ".xml");
            }
        }

        /// <summary>
        /// Returns the name of the style using its xfId
        /// </summary>
        /// <param name="styleID">The xfId of the style</param>
        /// <returns>The name of the style</returns>
        protected internal string GetStyleName(int styleID)
        {
            string retValue = null;
            XmlNode styleNode = null;
            int count = 0;
            foreach (XmlNode node in this.XlsPackage.Workbook.StylesXml.SelectNodes("//d:cellXfs/d:xf", this.NameSpaceManager))
            {
                if (count == styleID)
                {
                    styleNode = node;
                    break;
                }

                count++;
            }

            if (styleNode != null)
            {
                string searchString = string.Format("//d:cellStyle[@xfId = '{0}']", styleNode.Attributes["xfId"].Value);
                XmlNode styleNameNode = this.XlsPackage.Workbook.StylesXml.SelectSingleNode(searchString, this.NameSpaceManager);
                if (styleNameNode != null)
                {
                    retValue = styleNameNode.Attributes["name"].Value;
                }
            }

            return retValue;
        }

        /// <summary>
        /// Returns the style ID given a style name.  
        /// The style ID will be created if not found, but only if the style name exists!
        /// </summary>
        /// <param name="styleName">The name of the style.</param>
        /// <returns>The style id.</returns>
        protected internal int GetStyleID(string styleName)
        {
            int styleID = 0;
            //// find the named style in the style sheet
            string searchString = string.Format("//d:cellStyle[@name = '{0}']", styleName);
            XmlNode styleNameNode = this.XlsPackage.Workbook.StylesXml.SelectSingleNode(searchString, this.NameSpaceManager);
            if (styleNameNode != null)
            {
                string xlsfId = styleNameNode.Attributes["xfId"].Value;
                //// look up position of style in the cellXfs 
                searchString = string.Format("//d:cellXfs/d:xf[@xfId = '{0}']", xlsfId);
                XmlNode styleNode = this.XlsPackage.Workbook.StylesXml.SelectSingleNode(searchString, this.NameSpaceManager);
                if (styleNode != null)
                {
                    XmlNodeList nodes = styleNode.SelectNodes("preceding-sibling::d:xf", this.NameSpaceManager);
                    if (nodes != null)
                    {
                        styleID = nodes.Count;
                    }
                }
            }

            return styleID;
        }

        /// <summary>
        /// Adds numeric cell identifiers so that it is easier to work out position of cells
        /// Private method, for internal use only!
        /// </summary>
        private void AddNumericCellIDs()
        {
            //// process each row
            foreach (XmlNode rowNode in this.WorksheetXml.SelectNodes("//d:sheetData/d:row", this.NameSpaceManager))
            {
                //// remove the spans attribute.  Excel simply recreates it when the file is opened.
                XmlAttribute attr = (XmlAttribute)rowNode.Attributes.GetNamedItem("spans");
                if (attr != null)
                {
                    rowNode.Attributes.Remove(attr);
                }

                int row = Convert.ToInt32(rowNode.Attributes.GetNamedItem("r").Value);
                //// process each cell in current row
                foreach (XmlNode colNode in rowNode.SelectNodes("./d:c", this.NameSpaceManager))
                {
                    XmlAttribute cellAddressAttr = (XmlAttribute)colNode.Attributes.GetNamedItem("r");
                    if (cellAddressAttr != null)
                    {
                        string cellAddress = cellAddressAttr.Value;

                        int col = ExcelCell.GetColumnNumber(cellAddress);
                        attr = this.WorksheetXml.CreateAttribute(TempColumnNumberTag);
                        if (attr != null)
                        {
                            attr.Value = col.ToString();
                            colNode.Attributes.Append(attr);
                            //// remove all cell Addresses like A1, A2, A3 etc.
                            colNode.Attributes.Remove(cellAddressAttr);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Replaces the numeric cell identifiers we inserted with AddNumericCellIDs with the traditional 
        /// A1, A2 cell identifiers that Excel understands.
        /// Private method, for internal use only!
        /// </summary>
        private void ReplaceNumericCellIDs()
        {
            int maxColumn = 0;
            //// process each row
            foreach (XmlNode rowNode in this.WorksheetXml.SelectNodes("//d:sheetData/d:row", this.NameSpaceManager))
            {
                int row = Convert.ToInt32(rowNode.Attributes.GetNamedItem("r").Value);
                int count = 0;
                //// process each cell in current row
                foreach (XmlNode colNode in rowNode.SelectNodes("./d:c", this.NameSpaceManager))
                {
                    XmlAttribute colNumber = (XmlAttribute)colNode.Attributes.GetNamedItem(TempColumnNumberTag);
                    if (colNumber != null)
                    {
                        count++;
                        if (count > maxColumn)
                        {
                            maxColumn = count;
                        }

                        int col = Convert.ToInt32(colNumber.Value);
                        string cellAddress = ExcelCell.GetColumnLetter(col) + row.ToString();
                        XmlAttribute attr = this.WorksheetXml.CreateAttribute("r");
                        if (attr != null)
                        {
                            attr.Value = cellAddress;
                            //// the cellAddress needs to be the first attribute, otherwise Excel complains
                            if (colNode.Attributes.Count == 0)
                            {
                                colNode.Attributes.Append(attr);
                            }
                            else
                            {
                                colNode.Attributes.InsertBefore(attr, (XmlAttribute)colNode.Attributes.Item(0));
                            }
                        }
                        //// remove all numeric cell addresses added by AddNumericCellIDs
                        colNode.Attributes.Remove(colNumber);
                    }
                }
            }

            //// process each row and add the spans attribute
            //// TODO: Need to add proper spans handling.
            ////foreach (XmlNode rowNode in XmlDoc.SelectNodes("//d:sheetData/d:row", NameSpaceManager))
            ////{
            ////  //// we must add or update the "spans" attribute of each row
            ////  XmlAttribute spans = (XmlAttribute)rowNode.Attributes.GetNamedItem("spans");
            ////  if (spans == null)
            ////  {
            ////    spans = XmlDoc.CreateAttribute("spans");
            ////    rowNode.Attributes.Append(spans);
            ////  }
            ////  spans.Value = "1:" + maxColumn.ToString();
            ////}
        }


        #region LoadFromDataTable Extension

        #region Overloads
        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses all defaults:
        ///		Data checked to determine if string or numeric
        ///		All DataTable columns used and column name used as header label
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        public void LoadFromDataTable(DataTable loadData)
        {
            LoadFromDataTable(loadData, false);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		All DataTable columns used and column name used as header label
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="allDataAsStrings">Force all data to be treated as strings</param>
        public void LoadFromDataTable(DataTable loadData, bool allDataAsStrings)
        {
            KeyValuePair<string, string>[] columns = new KeyValuePair<string, string>[loadData.Columns.Count];
            for (int i = 0; i < loadData.Columns.Count; i++)
            {
                columns[i] = new KeyValuePair<string, string>(loadData.Columns[i].ColumnName, loadData.Columns[i].ColumnName);
            }
            LoadFromDataTable(loadData, allDataAsStrings, columns, 0, String.Empty, String.Empty);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		All DataTable columns used and column name used as header label
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="allDataAsStrings">Force all data to be treated as strings</param>
        /// <param name="strHeader">string to be used in the header row</param>
        /// <param name="strFooter">string to be used in the footer row</param>
        public void LoadFromDataTable(DataTable loadData, bool allDataAsStrings, string strHeader, string strFooter)
        {
            KeyValuePair<string, string>[] columns = new KeyValuePair<string, string>[loadData.Columns.Count];
            for (int i = 0; i < loadData.Columns.Count; i++)
            {
                columns[i] = new KeyValuePair<string, string>(loadData.Columns[i].ColumnName, loadData.Columns[i].ColumnName);
            }
            LoadFromDataTable(loadData, allDataAsStrings, columns, 0, strHeader, strFooter);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="allDataAsStrings">Force all data to be treated as strings</param>
        /// <param name="columns">The names/labels of the columns to use for data (Key is column name; Value is label that should be used in header row for this column)</param>
        public void LoadFromDataTable(DataTable loadData, bool allDataAsStrings, KeyValuePair<string, string>[] columns)
        {
            LoadFromDataTable(loadData, allDataAsStrings, columns, 0, String.Empty, String.Empty);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		Data checked to determine if string or numeric
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="columns">The names/labels of the columns to use for data (Key is column name; Value is label that should be used in header row for this column)</param>
        public void LoadFromDataTable(DataTable loadData, KeyValuePair<string, string>[] columns)
        {
            LoadFromDataTable(loadData, false, columns, 0, String.Empty, String.Empty);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		Data checked to determine if string or numeric
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="columns">The names/labels of the columns to use for data (Key is column name; Value is label that should be used in header row for this column)</param>
        /// <param name="headerStyleId">The style to add to the header row</param>
        public void LoadFromDataTable(DataTable loadData, KeyValuePair<string, string>[] columns, int headerStyleId)
        {
            LoadFromDataTable(loadData, false, columns, headerStyleId, String.Empty, String.Empty);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		Data checked to determine if string or numeric
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="columnNames">The names of the columns to use for data (column name will also be used for header row labels)</param>
        public void LoadFromDataTable(DataTable loadData, string[] columnNames)
        {
            LoadFromDataTable(loadData, false, columnNames, 0);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="allDataAsStrings">Force all data to be treated as strings</param>
        /// <param name="columnNames">The names of the columns to use for data (column name will also be used for header row labels)</param>
        public void LoadFromDataTable(DataTable loadData, bool allDataAsStrings, string[] columnNames)
        {
            LoadFromDataTable(loadData, allDataAsStrings, columnNames, 0);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		Data checked to determine if string or numeric
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="columnNames">The names of the columns to use for data (column name will also be used for header row labels)</param>
        /// <param name="headerStyleId">The style to add to the header row</param>
        public void LoadFromDataTable(DataTable loadData, string[] columnNames, int headerStyleId)
        {
            LoadFromDataTable(loadData, false, columnNames, headerStyleId);
        }

        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// Uses following defaults:
        ///		StyleID of 0 for header row
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="allDataAsStrings">Force all data to be treated as strings</param>
        /// <param name="columnNames">The names of the columns to use for data (column name will also be used for header row labels)</param>
        /// <param name="headerStyleId">The style to add to the header row</param>
        public void LoadFromDataTable(DataTable loadData, bool allDataAsStrings, string[] columnNames, int headerStyleId)
        {
            KeyValuePair<string, string>[] columns = new KeyValuePair<string, string>[columnNames.Length];
            for (int i = 0; i < columnNames.Length; i++)
            {
                columns[i] = new KeyValuePair<string, string>(columnNames[i], columnNames[i]);
            }
            LoadFromDataTable(loadData, allDataAsStrings, columns, headerStyleId, String.Empty, String.Empty);
        }
        #endregion

        #region Actual LoadFromDataTable function
        /// <summary>
        /// Quick method to "bulk load" an empty sheet with data. An exception is thrown if the sheet already contains data...
        /// </summary>
        /// <param name="loadData">The System.Data.DataTable to load the sheet object with</param>
        /// <param name="allDataAsStrings">Force all data to be treated as strings</param>
        /// <param name="columns">The names/labels of the columns to use for data (Key is column name; Value is label that should be used in header row for this column)</param>
        /// <param name="headerStyleId">The style to add to the header row</param>
        /// <param name="strHeader">The Header string for the Excel file</param>
        /// <param name="strFooter">The Footer string for the Excel file</param>
        public void LoadFromDataTable(DataTable loadData, bool allDataAsStrings, KeyValuePair<string, string>[] columns, int headerStyleId, string strHeader, string strFooter)
        {
            XmlNode sheetDataNode = WorksheetXml.SelectSingleNode("//d:sheetData", NameSpaceManager);
            if (sheetDataNode != null)
            {
                if (sheetDataNode.HasChildNodes)
                {
                    // Keep things simple by assuming the target sheet has no data
                    throw (new Exception("ExcelWorksheet Load: Target Sheet must be empty"));
                }
                else
                {
                    string cellValue;
                    XmlElement rowElement;
                    XmlElement cellElement;
                    XmlElement inlineStringElement;
                    XmlElement textElement;
                    XmlElement valueElement;
                    int rowNumber = 1;

                    //if the strHeader is not empty add the header row
                    if (strHeader != String.Empty)
                    {
                        // Added by Doug C for the Header Input
                        // Add headers to data
                        rowElement = WorksheetXml.CreateElement("row", ExcelPackage.SchemaMain);
                        rowElement.Attributes.Append(WorksheetXml.CreateAttribute("r"));
                        rowElement.Attributes["r"].Value = rowNumber.ToString();

                        cellElement = WorksheetXml.CreateElement("c", ExcelPackage.SchemaMain);
                        inlineStringElement = WorksheetXml.CreateElement("is", ExcelPackage.SchemaMain);
                        textElement = WorksheetXml.CreateElement("t", ExcelPackage.SchemaMain);
                        textElement.InnerText = strHeader;
                        inlineStringElement.AppendChild(textElement);
                        cellElement.AppendChild(inlineStringElement);
                        cellElement.Attributes.Append(WorksheetXml.CreateAttribute("t"));
                        cellElement.Attributes["t"].Value = "inlineStr";

                        // Add header cell style
                        cellElement.Attributes.Append(WorksheetXml.CreateAttribute("s"));
                        cellElement.Attributes["s"].Value = headerStyleId.ToString();

                        // Completed the cell so add to current row
                        rowElement.AppendChild(cellElement);

                        // That's the header row done, so add to the sheet
                        sheetDataNode.AppendChild(rowElement);

                        rowNumber++;
                        // end Doug C Input
                    }

                    // Add Column Label data
                    rowElement = WorksheetXml.CreateElement("row", ExcelPackage.SchemaMain);
                    rowElement.Attributes.Append(WorksheetXml.CreateAttribute("r"));
                    rowElement.Attributes["r"].Value = rowNumber.ToString();

                    for (int i = 0; i < columns.Length; i++)
                    {
                        cellElement = WorksheetXml.CreateElement("c", ExcelPackage.SchemaMain);
                        inlineStringElement = WorksheetXml.CreateElement("is", ExcelPackage.SchemaMain);
                        textElement = WorksheetXml.CreateElement("t", ExcelPackage.SchemaMain);
                        textElement.InnerText = loadData.Columns.Contains(columns[i].Key) ? columns[i].Value : "Skipped " + columns[i].Key + ": (column not found)";
                        inlineStringElement.AppendChild(textElement);
                        cellElement.AppendChild(inlineStringElement);
                        cellElement.Attributes.Append(WorksheetXml.CreateAttribute("t"));
                        cellElement.Attributes["t"].Value = "inlineStr";

                        // Add header cell style
                        cellElement.Attributes.Append(WorksheetXml.CreateAttribute("s"));
                        cellElement.Attributes["s"].Value = headerStyleId.ToString();

                        // Completed the cell so add to current row
                        rowElement.AppendChild(cellElement);
                    }
                    // That's the Column Label row done, so add to the sheet
                    sheetDataNode.AppendChild(rowElement);

                    // Now loop through the datatable adding values to the sheet
                    for (int j = 0; j < loadData.Rows.Count; j++)
                    {
                        rowNumber++;
                        rowElement = WorksheetXml.CreateElement("row", ExcelPackage.SchemaMain);
                        rowElement.Attributes.Append(WorksheetXml.CreateAttribute("r"));
                        rowElement.Attributes["r"].Value = rowNumber.ToString();

                        for (int i = 0; i < columns.Length; i++)
                        {
                            // Do we need to worry about stripping any special chars here?
                            cellValue = loadData.Columns.Contains(columns[i].Key) ? loadData.Rows[j][columns[i].Key].ToString() : "";

                            cellElement = WorksheetXml.CreateElement("c", ExcelPackage.SchemaMain);

                            if (ExcelCell.IsNumericValue(cellValue) && !allDataAsStrings)
                            {
                                // Stuff to insert a numeric value into a cell
                                valueElement = WorksheetXml.CreateElement("v", ExcelPackage.SchemaMain);
                                valueElement.InnerText = cellValue;
                                cellElement.AppendChild(valueElement);
                            }
                            else
                            {
                                // Stuff to insert a string value into a cell
                                inlineStringElement = WorksheetXml.CreateElement("is", ExcelPackage.SchemaMain);
                                textElement = WorksheetXml.CreateElement("t", ExcelPackage.SchemaMain);
                                textElement.InnerText = cellValue;
                                inlineStringElement.AppendChild(textElement);
                                cellElement.AppendChild(inlineStringElement);
                                cellElement.Attributes.Append(WorksheetXml.CreateAttribute("t"));
                                cellElement.Attributes["t"].Value = "inlineStr";
                            }

                            //  Default style is "0"
                            cellElement.Attributes.Append(WorksheetXml.CreateAttribute("s"));
                            cellElement.Attributes["s"].Value = "0";

                            // Completed the cell so add to current row
                            rowElement.AppendChild(cellElement);
                        }

                        // Complete row, so add to datasheet
                        sheetDataNode.AppendChild(rowElement);
                    }

                    //if the strFooter is not empty add the footer row
                    if (strFooter != String.Empty)
                    {
                        rowNumber++;

                        // Added by Doug C for the Footer Input
                        rowElement = WorksheetXml.CreateElement("row", ExcelPackage.SchemaMain);
                        rowElement.Attributes.Append(WorksheetXml.CreateAttribute("r"));
                        rowElement.Attributes["r"].Value = rowNumber.ToString();

                        cellElement = WorksheetXml.CreateElement("c", ExcelPackage.SchemaMain);
                        inlineStringElement = WorksheetXml.CreateElement("is", ExcelPackage.SchemaMain);
                        textElement = WorksheetXml.CreateElement("t", ExcelPackage.SchemaMain);
                        textElement.InnerText = strFooter;
                        inlineStringElement.AppendChild(textElement);
                        cellElement.AppendChild(inlineStringElement);
                        cellElement.Attributes.Append(WorksheetXml.CreateAttribute("t"));
                        cellElement.Attributes["t"].Value = "inlineStr";

                        // Add header cell style
                        cellElement.Attributes.Append(WorksheetXml.CreateAttribute("s"));
                        cellElement.Attributes["s"].Value = headerStyleId.ToString();

                        // Completed the cell so add to current row
                        rowElement.AppendChild(cellElement);

                        // That's the header row done, so add to the sheet
                        sheetDataNode.AppendChild(rowElement);
                        // end Doug C Input
                    }
                    
                    AddNumericCellIDs();
                }
            }
            else
            {
                // Can't find the sheet data node... Assume a problem!                                
                throw (new Exception("ExcelWorksheet Load: Can't find sheet data node... This is not good!"));
            }
        }
        #endregion

        #endregion




    }  //// END class Worksheet
}
