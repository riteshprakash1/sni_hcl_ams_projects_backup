// --------------------------------------------------------------
// <copyright file="ExcelCell.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.IO.Packaging;
    using System.Text.RegularExpressions;
    using System.Xml;

    /// <summary>
    /// ExcelCell represents an individual worksheet cell.
    /// </summary>
    public class ExcelCell
    {
        /// <summary>
        /// The excel worksheet
        /// </summary>
        private ExcelWorksheet myXlWorksheet;

        /// <summary>
        /// THe cell element
        /// </summary>
        private XmlElement myCellElement;

        /// <summary>
        /// The row we are on.
        /// </summary>
        private int myRow;

        /// <summary>
        /// The column we are on.
        /// </summary>
        private int myCol;

        /// <summary>
        /// The value we wish to set.
        /// </summary>
        private string myValue;

        /// <summary>
        /// The value reference we with to set.
        /// </summary>
        private string myValueRef;

        /// <summary>
        /// The formula we are editing.
        /// </summary>
        private string myFormula;

        /// <summary>
        /// The data type for the cell.
        /// </summary>
        private string myDataType;

        /// <summary>
        /// The hyperlink for the cell.
        /// </summary>
        private Uri myHyperlink;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelCell"/> class.
        /// </summary>
        /// <param name="xlsWorksheet">The excel worksheet.</param>
        /// <param name="row">The row we are located.</param>
        /// <param name="col">The column we are located.</param>
        protected internal ExcelCell(ExcelWorksheet xlsWorksheet, int row, int col)
        {
            if (row < 1 || col < 1)
            {
                throw new Exception("ExcelCell Constructor: Negative row and column numbers are not allowed");
            }

            if (xlsWorksheet == null)
            {
                throw new Exception("ExcelCell Constructor: xlWorksheet must be set to a valid reference");
            }

            this.myXlWorksheet = xlsWorksheet;
            this.myRow = row;
            this.myCol = col;

            this.myCellElement = this.GetOrCreateCellElement(xlsWorksheet, row, col);
        }

        /// <summary>
        /// Gets reference to the cell's row number
        /// </summary>
        public int Row
        {
            get
            {
                return this.myRow;
            }
        }

        /// <summary>
        /// Gets reference to the cell's column number
        /// </summary>
        public int Column
        {
            get
            {
                return this.myCol;
            }
        }

        /// <summary>
        /// Gets the current cell address in the standard Excel format (e.g. 'E5')
        /// </summary>
        public string CellAddress
        {
            get
            {
                return GetCellAddress(this.myRow, this.myCol);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is numeric.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is numeric; otherwise, <c>false</c>.
        /// </value>
        public bool IsNumeric
        {
            get
            {
                return IsNumericValue(this.Value);
            }
        }

        /// <summary>
        /// Gets or sets the value of the cell.
        /// </summary>
        public string Value
        {
            get
            {
                if (this.myValue == null)
                {
                    bool isNumeric = true;  //// default
                    XmlNode valueNode = this.myCellElement.SelectSingleNode("./d:v", this.myXlWorksheet.NameSpaceManager);
                    if (valueNode == null)
                    {
                        this.myValueRef = String.Empty;
                        this.myValue = String.Empty;
                    }
                    else
                    {
                        this.myValueRef = valueNode.InnerText;
                        //// check to see if we have a string value
                        XmlAttribute attr = this.myCellElement.Attributes["t"];
                        if (attr != null)
                        {
                            isNumeric = !(attr.Value == "s");
                        }

                        if (isNumeric)
                        {
                            this.myValue = this.myValueRef;
                        }
                        else
                        {
                            this.myValue = this.GetSharedString(Convert.ToInt32(this.myValueRef));
                        }
                    }
                }

                return this.myValue;
            }

            set
            {
                this.myValue = value;
                //// set the value of the cell
                XmlNode valueNode = this.myCellElement.SelectSingleNode("./d:v", this.myXlWorksheet.NameSpaceManager);
                if (valueNode == null)
                {
                    ////  Cell with deleted value. Add a value element now.
                    valueNode = this.myCellElement.OwnerDocument.CreateElement("v", ExcelPackage.SchemaMain);
                    this.myCellElement.AppendChild(valueNode);
                }

                if (IsNumericValue(value))
                {
                    this.myValueRef = value;
                    //// ensure we remove any existing string data type flag
                    XmlAttribute attr = this.myCellElement.Attributes["t"];
                    if (attr != null)
                    {
                        this.myCellElement.Attributes.RemoveNamedItem("t");
                    }
                }
                else
                {
                    this.myValueRef = this.SetSharedString(this.myValue).ToString();
                    XmlAttribute attr = this.myCellElement.Attributes["t"];
                    if (attr == null)
                    {
                        attr = this.myCellElement.OwnerDocument.CreateAttribute("t");
                        this.myCellElement.Attributes.Append(attr);
                    }

                    attr.Value = "s";
                }

                valueNode.InnerText = this.myValueRef;
            }
        }

        /// <summary>
        /// Gets or sets the cell's data type.  
        /// Not currently implemented correctly!
        /// </summary>
        public string DataType
        {
            //// TODO: complete DataType
            get
            {
                string retValue = null;
                XmlAttribute attr = this.myCellElement.Attributes["t"];
                if (attr != null)
                {
                    this.myDataType = String.Empty;  //// default
                }

                return retValue;
            }

            set
            {
                this.myDataType = value;
                XmlAttribute attr = this.myCellElement.Attributes["t"];
                if (attr == null)
                {
                    attr = this.myCellElement.OwnerDocument.CreateAttribute("t");
                    this.myCellElement.Attributes.Append(attr);
                }

                attr.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>The style.</value>
        public string Style
        {
            get
            {
                return this.myXlWorksheet.GetStyleName(this.StyleID);
            }

            set
            {
                this.StyleID = this.myXlWorksheet.GetStyleID(value);
            }
        }

        /// <summary>
        /// Gets or sets the cell's style using the number of the style.
        /// Useful when coping styles from one cell to another.
        /// </summary>
        public int StyleID
        {
            get
            {
                int retValue = 0;
                string sid = this.myCellElement.GetAttribute("s");
                if (sid != String.Empty)
                {
                    retValue = int.Parse(sid);
                }

                return retValue;
            }

            set
            {
                this.myCellElement.SetAttribute("s", value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the cell's Hyperlink
        /// </summary>
        public Uri Hyperlink
        {
            get
            {
                if (this.myHyperlink == null)
                {
                    string searchString = string.Format("//d:hyperlinks/d:hyperlink[@ref = '{0}']", this.CellAddress);
                    XmlNode linkNode = this.myCellElement.OwnerDocument.SelectSingleNode(searchString, this.myXlWorksheet.NameSpaceManager);
                    if (linkNode != null)
                    {
                        XmlAttribute attr = (XmlAttribute)linkNode.Attributes.GetNamedItem("id", ExcelPackage.SchemaRelationships);
                        if (attr != null)
                        {
                            string relID = attr.Value;
                            //// now use the relID to lookup the hyperlink in the relationship table
                            PackageRelationship relationship = this.myXlWorksheet.Part.GetRelationship(relID);
                            this.myHyperlink = relationship.TargetUri;
                        }
                    }
                }

                return this.myHyperlink;
            }

            set
            {
                this.myHyperlink = value;
                XmlNode linkParent = this.myCellElement.OwnerDocument.SelectSingleNode("//d:hyperlinks", this.myXlWorksheet.NameSpaceManager);
                if (linkParent == null)
                {
                    //// create the hyperlinks node
                    linkParent = this.myCellElement.OwnerDocument.CreateElement("hyperlinks", ExcelPackage.SchemaMain);
                    XmlNode prevNode = this.myCellElement.OwnerDocument.SelectSingleNode("//d:conditionalFormatting", this.myXlWorksheet.NameSpaceManager);
                    if (prevNode == null)
                    {
                        prevNode = this.myCellElement.OwnerDocument.SelectSingleNode("//d:mergeCells", this.myXlWorksheet.NameSpaceManager);
                        if (prevNode == null)
                        {
                            prevNode = this.myCellElement.OwnerDocument.SelectSingleNode("//d:sheetData", this.myXlWorksheet.NameSpaceManager);
                        }
                    }

                    this.myCellElement.OwnerDocument.DocumentElement.InsertAfter(linkParent, prevNode);
                }

                string searchString = string.Format("./d:hyperlink[@ref = '{0}']", this.CellAddress);
                XmlElement linkNode = (XmlElement)linkParent.SelectSingleNode(searchString, this.myXlWorksheet.NameSpaceManager);
                XmlAttribute attr;
                if (linkNode == null)
                {
                    linkNode = this.myCellElement.OwnerDocument.CreateElement("hyperlink", ExcelPackage.SchemaMain);
                    //// now add cell address attribute
                    linkNode.SetAttribute("ref", this.CellAddress);
                    linkParent.AppendChild(linkNode);
                }

                attr = (XmlAttribute)linkNode.Attributes.GetNamedItem("id", ExcelPackage.SchemaRelationships);
                if (attr == null)
                {
                    attr = this.myCellElement.OwnerDocument.CreateAttribute("r", "id", ExcelPackage.SchemaRelationships);
                    linkNode.Attributes.Append(attr);
                }

                PackageRelationship relationship = null;
                string relID = attr.Value;
                if (relID == String.Empty)
                {
                    relationship = this.myXlWorksheet.Part.CreateRelationship(this.myHyperlink, TargetMode.External, @"http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink");
                }
                else
                {
                    relationship = this.myXlWorksheet.Part.GetRelationship(relID);
                    if (relationship.TargetUri != this.myHyperlink)
                    {
                        relationship = this.myXlWorksheet.Part.CreateRelationship(this.myHyperlink, TargetMode.External, @"http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink");
                    }
                }

                attr.Value = relationship.Id;

                ////attr = (XmlAttribute)linkNode.Attributes.GetNamedItem("display", ExcelPackage.schemaMain);
                ////if (attr == null)
                ////{
                ////  attr = this._cellNode.OwnerDocument.CreateAttribute("display");
                ////  linkNode.Attributes.Append(attr);
                ////}
                ////attr.Value = Display;
            }
        }

        /// <summary>
        /// Gets or sets read/write access to the cell's formula.
        /// </summary>
        public string Formula
        {
            get
            {
                if (this.myFormula == null)
                {
                    XmlNode formulaNode = this.myCellElement.SelectSingleNode("./d:f", this.myXlWorksheet.NameSpaceManager);
                    if (formulaNode != null)
                    {
                        //// first check if we have a shared formula
                        XmlAttribute attr = (XmlAttribute)formulaNode.Attributes.GetNamedItem("t");
                        if (attr == null)
                        {
                            //// we have a standard formula
                            this.myFormula = formulaNode.InnerText;
                        }
                        else
                        {
                            if (attr.Value == "shared")
                            {
                                //// we must obtain the formula from the shared cell reference
                                XmlAttribute refAttr = (XmlAttribute)formulaNode.Attributes.GetNamedItem("si");
                                if (refAttr == null)
                                {
                                    throw new Exception("ExcelCell formula marked as shared but no reference ID found (i.e. si attribute)");
                                }
                                else
                                {
                                    string searchString = string.Format("//d:sheetData/d:row/d:c/d:f[@si='{0}']", refAttr.Value);
                                    XmlNode refNode = this.myCellElement.OwnerDocument.SelectSingleNode(searchString, this.myXlWorksheet.NameSpaceManager);
                                    if (refNode == null)
                                    {
                                        throw new Exception("ExcelCell formula marked as shared but no reference node found");
                                    }
                                    else
                                    {
                                        this.myFormula = refNode.InnerText;
                                    }
                                }
                            }
                            else
                            {
                                this.myFormula = formulaNode.InnerText;
                            }
                        }
                    }
                }

                return this.myFormula;
            }

            set
            {
                //// Example cell content for formulas
                //// <f>D7</f>
                //// <f>SUM(D6:D8)</f>
                //// <f>F6+F7+F8</f>
                this.myFormula = value;
                //// insert the formula into the cell
                XmlElement formulaElement = (XmlElement)this.myCellElement.SelectSingleNode("./d:f", this.myXlWorksheet.NameSpaceManager);
                if (formulaElement == null)
                {
                    formulaElement = this.AddFormulaElement();
                }
                //// we are setting the formula directly, so remove the shared attributes (if present)
                formulaElement.Attributes.RemoveNamedItem("t", ExcelPackage.SchemaMain);
                formulaElement.Attributes.RemoveNamedItem("si", ExcelPackage.SchemaMain);

                //// set the formula
                formulaElement.InnerText = value;

                //// force Excel to re-calculate the cell by removing the value
                this.RemoveValue();
            }
        }

        /// <summary>
        /// Gets the comment as a string
        /// </summary>
        public string Comment
        {
            //// TODO: implement get which will obtain the text of the comment from the comment1.xml file
            get
            {
                throw new Exception("Function not yet implemented!");
            }
            //// TODO: implement set which will add comments to the worksheet
            //// this will require you to add entries to the Drawing.vml file to get this to work! 
        }

        /// <summary>
        /// Gets reference to the cell's XmlNode (for internal use only)
        /// </summary>
        protected internal XmlElement Element
        {
            get
            {
                return this.myCellElement;
            }
        }

        /// <summary>
        /// Returns true if the string contains a numeric value
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>Whether the value is numeric.</returns>
        public static bool IsNumericValue(string value)
        {
            Regex objNotIntPattern = new Regex("[^0-9,.-]");
            Regex objIntPattern = new Regex("^-[0-9,.]+$|^[0-9,.]+$");

            return !objNotIntPattern.IsMatch(value) &&
                            objIntPattern.IsMatch(value);
        }

        /// <summary>
        /// Returns the column number from the cellAddress
        /// e.g. D5 would return 5
        /// </summary>
        /// <param name="cellAddress">An Excel format cell addresss (e.g. D5)</param>
        /// <returns>The column number</returns>
        public static int GetColumnNumber(string cellAddress)
        {
            //// find out position where characters stop and numbers begin
            int intColumnNumber = 0;
            int intPos = 0;
            bool found = false;
            foreach (char chr in cellAddress.ToCharArray())
            {
                intPos++;
                if (char.IsNumber(chr))
                {
                    found = true;
                    break;
                }
            }

            if (found)
            {
                string alphaPart = cellAddress.Substring(0, cellAddress.Length - (cellAddress.Length + 1 - intPos));

                int length = alphaPart.Length;
                int count = 0;
                foreach (char chr in alphaPart.ToCharArray())
                {
                    count++;
                    int chrValue = (int)chr - 64;
                    switch (length)
                    {
                        case 1:
                            intColumnNumber = chrValue;
                            break;
                        case 2:
                            if (count == 1)
                            {
                                intColumnNumber += chrValue * 26;
                            }
                            else
                            {
                                intColumnNumber += chrValue;
                            }

                            break;
                        case 3:
                            if (count == 1)
                            {
                                intColumnNumber += chrValue * 26 * 26;
                            }

                            if (count == 2)
                            {
                                intColumnNumber += chrValue * 26;
                            }
                            else
                            {
                                intColumnNumber += chrValue;
                            }

                            break;
                        case 4:
                            if (count == 1)
                            {
                                intColumnNumber += chrValue * 26 * 26 * 26;
                            }

                            if (count == 2)
                            {
                                intColumnNumber += chrValue * 26 * 26;
                            }

                            if (count == 3)
                            {
                                intColumnNumber += chrValue * 26;
                            }
                            else
                            {
                                intColumnNumber += chrValue;
                            }

                            break;
                    }
                }
            }

            return intColumnNumber;
        }

        /// <summary>
        /// Returns the row number from the cellAddress
        /// e.g. D5 would return 5
        /// </summary>
        /// <param name="cellAddress">An Excel format cell addresss (e.g. D5)</param>
        /// <returns>The row number</returns>
        public static int GetRowNumber(string cellAddress)
        {
            //// find out position where characters stop and numbers begin
            int intPos = 0;
            bool found = false;
            foreach (char chr in cellAddress.ToCharArray())
            {
                intPos++;
                if (char.IsNumber(chr))
                {
                    found = true;
                    break;
                }
            }

            if (found)
            {
                string numberPart = cellAddress.Substring(intPos - 1, cellAddress.Length - (intPos - 1));
                if (ExcelCell.IsNumericValue(numberPart))
                {
                    return int.Parse(numberPart);
                }
            }

            return 0;
        }

        /// <summary>
        /// Returns the AlphaNumeric representation that Excel expects for a Cell Address
        /// </summary>
        /// <param name="intRow">The number of the row</param>
        /// <param name="intColumn">The number of the column in the worksheet</param>
        /// <returns>The cell address in the format A1</returns>
        public static string GetCellAddress(int intRow, int intColumn)
        {
            return GetColumnLetter(intColumn) + intRow.ToString();
        }

        /// <summary>
        /// Checks that a cell address (e.g. A5) is valid.
        /// </summary>
        /// <param name="cellAddress">The alphanumeric cell address</param>
        /// <returns>True if the cell address is valid</returns>
        public static bool IsValidCellAddress(string cellAddress)
        {
            int row = GetRowNumber(cellAddress);
            int col = GetColumnNumber(cellAddress);

            if (GetCellAddress(row, col) == cellAddress)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Updates the formula references.
        /// </summary>
        /// <param name="formula">The formula.</param>
        /// <param name="rowIncrement">The row increment.</param>
        /// <param name="colIncrement">The col increment.</param>
        /// <param name="afterRow">The after row.</param>
        /// <param name="afterColumn">The after column.</param>
        /// <returns>The updated formula.</returns>
        public static string UpdateFormulaReferences(string formula, int rowIncrement, int colIncrement, int afterRow, int afterColumn)
        {
            string newFormula = String.Empty;

            Regex getAlphaNumeric = new Regex(@"[^a-zA-Z0-9]", RegexOptions.IgnoreCase);
            Regex getSigns = new Regex(@"[a-zA-Z0-9]", RegexOptions.IgnoreCase);

            string alphaNumeric = getAlphaNumeric.Replace(formula, " ").Replace("  ", " ");
            string signs = getSigns.Replace(formula, " ");
            char[] chrSigns = signs.ToCharArray();
            int count = 0;
            int length = 0;
            foreach (string cellAddress in alphaNumeric.Split(' '))
            {
                count++;
                length += cellAddress.Length;

                //// if the cellAddress contains an alpha part followed by a number part, then it is a valid cellAddress
                int row = GetRowNumber(cellAddress);
                int col = GetColumnNumber(cellAddress);
                string newCellAddress = String.Empty;
                //// this checks if the cellAddress is valid
                if (ExcelCell.GetCellAddress(row, col) == cellAddress)
                {
                    //// we have a valid cell address so change its value (if necessary)
                    if (row >= afterRow)
                    {
                        row += rowIncrement;
                    }

                    if (col >= afterColumn)
                    {
                        col += colIncrement;
                    }

                    newCellAddress = GetCellAddress(row, col);
                }

                if (newCellAddress == String.Empty)
                {
                    newFormula += cellAddress;
                }
                else
                {
                    newFormula += newCellAddress;
                }

                for (int i = length; i < signs.Length; i++)
                {
                    if (chrSigns[i] == ' ')
                    {
                        break;
                    }

                    if (chrSigns[i] != ' ')
                    {
                        length++;
                        newFormula += chrSigns[i].ToString();
                    }
                }
            }

            return newFormula;
        }

        /// <summary>
        /// Removes the XmlNode that holds the cell's value.
        /// Useful when the cell contains a formula as this will force Excel to re-calculate the cell's content.
        /// </summary>
        public void RemoveValue()
        {
            XmlNode valueNode = this.myCellElement.SelectSingleNode("./d:v", this.myXlWorksheet.NameSpaceManager);
            if (valueNode != null)
            {
                this.myCellElement.RemoveChild(valueNode);
            }
        }

        /// <summary>
        /// Returns the cell's value as a string.
        /// </summary>
        /// <returns>The cell's value</returns>
        public override string ToString()
        {
            return this.Value;
        }

        /// <summary>
        /// Returns the character representation of the numbered column
        /// </summary>
        /// <param name="intColumnNumber">The number of the column</param>
        /// <returns>The letter representing the column</returns>
        protected internal static string GetColumnLetter(int intColumnNumber)
        {
            int intMainLetterUnicode;
            char intMainLetterChar;

            //// TODO: we need to cater for columns larger than ZZ
            if (intColumnNumber > 26)
            {
                int intFirstLetterUnicode = 0;  //// default
                int intFirstLetter = Convert.ToInt32(intColumnNumber / 26);
                char intFirstLetterChar;
                if (Convert.ToDouble(intFirstLetter) == (Convert.ToDouble(intColumnNumber) / 26))
                {
                    intFirstLetterUnicode = intFirstLetter - 1 + 64;
                    intMainLetterChar = 'Z';
                }
                else
                {
                    intFirstLetterUnicode = intFirstLetter + 64;
                    intMainLetterUnicode = (intColumnNumber - (intFirstLetter * 26)) + 64;
                    intMainLetterChar = (char)intMainLetterUnicode;
                }

                intFirstLetterChar = (char)intFirstLetterUnicode;

                return intFirstLetterChar.ToString() + intMainLetterChar.ToString();
            }

            //// if we get here we only have a single letter to return
            intMainLetterUnicode = 64 + intColumnNumber;
            intMainLetterChar = (char)intMainLetterUnicode;
            return intMainLetterChar.ToString();
        }

        /// <summary>
        /// Adds a new formula node to the cell in the correct location
        /// </summary>
        /// <returns>The XML element</returns>
        protected internal XmlElement AddFormulaElement()
        {
            XmlElement formulaElement = this.myCellElement.OwnerDocument.CreateElement("f", ExcelPackage.SchemaMain);
            //// find the right location for insersion
            XmlNode valueNode = this.myCellElement.SelectSingleNode("./d:v", this.myXlWorksheet.NameSpaceManager);
            if (valueNode == null)
            {
                this.myCellElement.AppendChild(formulaElement);
            }
            else
            {
                this.myCellElement.InsertBefore(formulaElement, valueNode);
            }

            return formulaElement;
        }

        /// <summary>
        /// Sets the shared string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Returns a string.</returns>
        private int SetSharedString(string value)
        {
            ////  Assume the string won't be found (assign it an impossible index):
            int index = -1;

            ////  Check to see if the string already exists. If so, retrieve its index.
            ////  This search is case-sensitive, but Excel stores differently cased
            ////  strings separately within the string file.
            XmlNode stringNode = this.myXlWorksheet.XlsPackage.Workbook.SharedStringsXml.SelectSingleNode(string.Format("//d:si[d:t='{0}']", value), this.myXlWorksheet.NameSpaceManager);
            if (stringNode == null)
            {
                ////  You didn't find the string in the table, so add it now.
                stringNode = this.myXlWorksheet.XlsPackage.Workbook.SharedStringsXml.CreateElement("si", ExcelPackage.SchemaMain);
                XmlElement textNode = this.myXlWorksheet.XlsPackage.Workbook.SharedStringsXml.CreateElement("t", ExcelPackage.SchemaMain);
                textNode.InnerText = value;
                stringNode.AppendChild(textNode);
                this.myXlWorksheet.XlsPackage.Workbook.SharedStringsXml.DocumentElement.AppendChild(stringNode);
            }

            if (stringNode != null)
            {
                ////  Retrieve the index of the selected node.
                ////  To do that, count the number of preceding
                ////  nodes by retrieving a reference to those nodes.
                XmlNodeList nodes = stringNode.SelectNodes("preceding-sibling::d:si", this.myXlWorksheet.NameSpaceManager);
                index = nodes.Count;
            }

            return index;
        }

        /// <summary>
        /// Gets the shared string.
        /// </summary>
        /// <param name="stringID">The string ID.</param>
        /// <returns>Returns a string.</returns>
        private string GetSharedString(int stringID)
        {
            string retValue = null;
            XmlNodeList stringNodes = this.myXlWorksheet.XlsPackage.Workbook.SharedStringsXml.SelectNodes(string.Format("//d:si", stringID), this.myXlWorksheet.NameSpaceManager);
            XmlNode stringNode = stringNodes[stringID];
            if (stringNode != null)
            {
                retValue = stringNode.InnerText;
            }

            return retValue;
        }

        /// <summary>
        /// Gets the or create cell element.
        /// </summary>
        /// <param name="xlsWorksheet">The XLS worksheet.</param>
        /// <param name="row">The row we are on.</param>
        /// <param name="col">The column we are on.</param>
        /// <returns>A cell element.</returns>
        private XmlElement GetOrCreateCellElement(ExcelWorksheet xlsWorksheet, int row, int col)
        {
            XmlElement cellNode = null;
            //// this will create the row if it does not already exist
            XmlNode rowNode = xlsWorksheet.Row(row).Node;
            if (rowNode != null)
            {
                cellNode = (XmlElement)rowNode.SelectSingleNode(string.Format("./d:c[@" + ExcelWorksheet.TempColumnNumberTag + "='{0}']", col), this.myXlWorksheet.NameSpaceManager);
                if (cellNode == null)
                {
                    ////  Didn't find the cell so create the cell element
                    cellNode = xlsWorksheet.WorksheetXml.CreateElement("c", ExcelPackage.SchemaMain);
                    cellNode.SetAttribute(ExcelWorksheet.TempColumnNumberTag, col.ToString());

                    //// You must insert the new cell at the correct location.
                    //// Loop through the children, looking for the first cell that is
                    //// beyond the cell you're trying to insert. Insert before that cell.
                    XmlNode biggerNode = null;
                    XmlNodeList cellNodes = rowNode.SelectNodes("./d:c", this.myXlWorksheet.NameSpaceManager);
                    if (cellNodes != null)
                    {
                        foreach (XmlNode node in cellNodes)
                        {
                            XmlNode colNode = node.Attributes[ExcelWorksheet.TempColumnNumberTag];
                            if (colNode != null)
                            {
                                int colFound = Convert.ToInt32(colNode.Value);
                                if (colFound > col)
                                {
                                    biggerNode = node;
                                    break;
                                }
                            }
                        }
                    }

                    if (biggerNode == null)
                    {
                        rowNode.AppendChild(cellNode);
                    }
                    else
                    {
                        rowNode.InsertBefore(cellNode, biggerNode);
                    }
                }
            }

            return cellNode;
        }
    }
}
