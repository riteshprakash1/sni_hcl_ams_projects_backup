// ------------------------------------------------------------------
// <copyright file="ExcelHeaderFooter.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007  Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Xml;

    /// <summary>
    /// Represents the Header and Footer on an Excel Worksheet
    /// </summary>
    public class ExcelHeaderFooter
    {
        /// <summary>
        /// Use this to insert the page number into the header or footer of the worksheet
        /// </summary>
        public const string PageNumber = @"&P";

        /// <summary>
        /// Use this to insert the number of pages into the header or footer of the worksheet
        /// </summary>
        public const string NumberOfPages = @"&N";

        /// <summary>
        /// Use this to insert the name of the worksheet into the header or footer of the worksheet
        /// </summary>
        public const string SheetName = @"&A";

        /// <summary>
        /// Use this to insert the full path to the folder containing the workbook into the header or footer of the worksheet
        /// </summary>
        public const string FilePath = @"&Z";

        /// <summary>
        /// Use this to insert the name of the workbook file into the header or footer of the worksheet
        /// </summary>
        public const string FileName = @"&F";

        /// <summary>
        /// Use this to insert the current date into the header or footer of the worksheet
        /// </summary>
        public const string CurrentDate = @"&D";

        /// <summary>
        /// Use this to insert the current time into the header or footer of the worksheet
        /// </summary>
        public const string CurrentTime = @"&T";

        /// <summary>
        /// The header footer node.
        /// </summary>
        private XmlElement myHeaderFooterNode;

        /// <summary>
        /// The odd header.
        /// </summary>
        private ExcelHeaderFooterText myOddHeader;

        /// <summary>
        /// The odd footer.
        /// </summary>
        private ExcelHeaderFooterText myOddFooter;

        /// <summary>
        /// The even header.
        /// </summary>
        private ExcelHeaderFooterText myEvenHeader;

        /// <summary>
        /// The even footer.
        /// </summary>
        private ExcelHeaderFooterText myEvenFooter;

        /// <summary>
        /// The first header.
        /// </summary>
        private ExcelHeaderFooterText myFirstHeader;

        /// <summary>
        /// The first footer.
        /// </summary>
        private ExcelHeaderFooterText myFirstFooter;

        /// <summary>
        /// The align flag.
        /// </summary>
        private System.Nullable<bool> myAlignWithMargins = null;

        /// <summary>
        /// Another align flag.
        /// </summary>
        private System.Nullable<bool> myDifferentOddEven = null;

        /// <summary>
        /// The different open first.
        /// </summary>
        private System.Nullable<bool> myDifferentFirst = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelHeaderFooter"/> class.
        /// </summary>
        /// <param name="headerFooterNode">The header footer node.</param>
        protected internal ExcelHeaderFooter(XmlElement headerFooterNode)
        {
            if (headerFooterNode.Name != "headerFooter")
            {
                throw new Exception("ExcelHeaderFooter Error: Passed invalid headerFooter node");
            }
            else
            {
                this.myHeaderFooterNode = headerFooterNode;
                //// TODO: populate structure based on XML content
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the alignWithMargins attribute
        /// </summary>
        public bool AlignWithMargins
        {
            get
            {
                if (this.myAlignWithMargins == null)
                {
                    this.myAlignWithMargins = false;
                    XmlAttribute attr = (XmlAttribute)this.myHeaderFooterNode.Attributes.GetNamedItem("alignWithMargins");
                    if (attr != null)
                    {
                        if (attr.Value == "1")
                        {
                            this.myAlignWithMargins = true;
                        }
                    }
                }

                return this.myAlignWithMargins.Value;
            }

            set
            {
                this.myAlignWithMargins = value;
                XmlAttribute attr = (XmlAttribute)this.myHeaderFooterNode.Attributes.GetNamedItem("alignWithMargins");
                if (attr == null)
                {
                    attr = this.myHeaderFooterNode.Attributes.Append(this.myHeaderFooterNode.OwnerDocument.CreateAttribute("alignWithMargins"));
                }

                if (value)
                {
                    attr.Value = "1";
                }
                else
                {
                    attr.Value = "0";
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the flag that tells Excel to display different headers and footers on odd and even pages.
        /// </summary>
        public bool DifferentOddEven
        {
            get
            {
                if (this.myDifferentOddEven == null)
                {
                    this.myDifferentOddEven = false;
                    XmlAttribute attr = (XmlAttribute)this.myHeaderFooterNode.Attributes.GetNamedItem("differentOddEven");
                    if (attr != null)
                    {
                        if (attr.Value == "1")
                        {
                            this.myDifferentOddEven = true;
                        }
                    }
                }

                return this.myDifferentOddEven.Value;
            }

            set
            {
                this.myDifferentOddEven = value;
                XmlAttribute attr = (XmlAttribute)this.myHeaderFooterNode.Attributes.GetNamedItem("differentOddEven");
                if (attr == null)
                {
                    attr = this.myHeaderFooterNode.Attributes.Append(this.myHeaderFooterNode.OwnerDocument.CreateAttribute("differentOddEven"));
                }

                if (value)
                {
                    attr.Value = "1";
                }
                else
                {
                    attr.Value = "0";
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the flag that tells Excel to display different headers and footers on the first page of the worksheet.
        /// </summary>
        public bool DifferentFirst
        {
            get
            {
                if (this.myDifferentFirst == null)
                {
                    this.myDifferentFirst = false;
                    XmlAttribute attr = (XmlAttribute)this.myHeaderFooterNode.Attributes.GetNamedItem("differentFirst");
                    if (attr != null)
                    {
                        if (attr.Value == "1")
                        {
                            this.myDifferentFirst = true;
                        }
                    }
                }

                return this.myDifferentFirst.Value;
            }

            set
            {
                this.myDifferentFirst = value;
                XmlAttribute attr = (XmlAttribute)this.myHeaderFooterNode.Attributes.GetNamedItem("differentFirst");
                if (attr == null)
                {
                    attr = this.myHeaderFooterNode.Attributes.Append(this.myHeaderFooterNode.OwnerDocument.CreateAttribute("differentFirst"));
                }

                if (value)
                {
                    attr.Value = "1";
                }
                else
                {
                    attr.Value = "0";
                }
            }
        }

        /// <summary>
        /// Gets access to a ExcelHeaderFooterText class that allows you to set the values of the header on odd numbered pages of the document.
        /// If you want the same header on both odd and even pages, then only set values in this ExcelHeaderFooterText class.
        /// </summary>
        public ExcelHeaderFooterText OddHeader
        {
            get
            {
                if (this.myOddHeader == null)
                {
                    this.myOddHeader = new ExcelHeaderFooterText();
                }

                return this.myOddHeader;
            }
        }

        /// <summary>
        /// Gets access to a ExcelHeaderFooterText class that allows you to set the values of the footer on odd numbered pages of the document.
        /// If you want the same footer on both odd and even pages, then only set values in this ExcelHeaderFooterText class.
        /// </summary>
        public ExcelHeaderFooterText OddFooter
        {
            get
            {
                if (this.myOddFooter == null)
                {
                    this.myOddFooter = new ExcelHeaderFooterText();
                }

                return this.myOddFooter;
            }
        }

        /// <summary>
        /// Gets access to a ExcelHeaderFooterText class that allows you to set the values of the header on even numbered pages of the document.
        /// </summary>
        /// <value>The even header.</value>
        public ExcelHeaderFooterText EvenHeader
        {
            get
            {
                if (this.myEvenHeader == null)
                {
                    this.myEvenHeader = new ExcelHeaderFooterText();
                }

                this.DifferentOddEven = true;
                return this.myEvenHeader;
            }
        }

        /// <summary>
        /// Gets access to a ExcelHeaderFooterText class that allows you to set the values of the footer on even numbered pages of the document.
        /// </summary>
        public ExcelHeaderFooterText EvenFooter
        {
            get
            {
                if (this.myEvenFooter == null)
                {
                    this.myEvenFooter = new ExcelHeaderFooterText();
                }

                this.DifferentOddEven = true;
                return this.myEvenFooter;
            }
        }

        /// <summary>
        /// Gets access to a ExcelHeaderFooterText class that allows you to set the values of the header on the first page of the document.
        /// </summary>
        /// <value>The first header.</value>
        public ExcelHeaderFooterText FirstHeader
        {
            get
            {
                if (this.myFirstHeader == null)
                {
                    this.myFirstHeader = new ExcelHeaderFooterText();
                }

                this.DifferentFirst = true;
                return this.myFirstHeader;
            }
        }

        /// <summary>
        /// Gets access to a ExcelHeaderFooterText class that allows you to set the values of the footer on the first page of the document.
        /// </summary>
        public ExcelHeaderFooterText FirstFooter
        {
            get
            {
                if (this.myFirstFooter == null)
                {
                    this.myFirstFooter = new ExcelHeaderFooterText();
                }

                this.DifferentFirst = true;
                return this.myFirstFooter;
            }
        }

        /// <summary>
        /// Saves the header and footer information to the worksheet XML
        /// </summary>
        protected internal void Save()
        {
            ////  The header/footer elements must appear in this order, if they appear:
            ////  <oddHeader />
            ////  <oddFooter />
            ////  <evenHeader />
            ////  <evenFooter />
            ////  <firstHeader />
            ////  <firstFooter />

            XmlNode node;
            if (this.myOddHeader != null)
            {
                node = this.myHeaderFooterNode.AppendChild(this.myHeaderFooterNode.OwnerDocument.CreateElement("oddHeader", ExcelPackage.SchemaMain));
                node.InnerText = this.GetHeaderFooterText(this.OddHeader);
            }

            if (this.myOddFooter != null)
            {
                node = this.myHeaderFooterNode.AppendChild(this.myHeaderFooterNode.OwnerDocument.CreateElement("oddFooter", ExcelPackage.SchemaMain));
                node.InnerText = this.GetHeaderFooterText(this.OddFooter);
            }

            //// only set evenHeader and evenFooter 
            if (this.DifferentOddEven)
            {
                if (this.myEvenHeader != null)
                {
                    node = this.myHeaderFooterNode.AppendChild(this.myHeaderFooterNode.OwnerDocument.CreateElement("evenHeader", ExcelPackage.SchemaMain));
                    node.InnerText = this.GetHeaderFooterText(this.EvenHeader);
                }

                if (this.myEvenFooter != null)
                {
                    node = this.myHeaderFooterNode.AppendChild(this.myHeaderFooterNode.OwnerDocument.CreateElement("evenFooter", ExcelPackage.SchemaMain));
                    node.InnerText = this.GetHeaderFooterText(this.EvenFooter);
                }
            }

            //// only set firstHeader and firstFooter
            if (this.DifferentFirst)
            {
                if (this.myFirstHeader != null)
                {
                    node = this.myHeaderFooterNode.AppendChild(this.myHeaderFooterNode.OwnerDocument.CreateElement("firstHeader", ExcelPackage.SchemaMain));
                    node.InnerText = this.GetHeaderFooterText(this.FirstHeader);
                }

                if (this.myFirstFooter != null)
                {
                    node = this.myHeaderFooterNode.AppendChild(this.myHeaderFooterNode.OwnerDocument.CreateElement("firstFooter", ExcelPackage.SchemaMain));
                    node.InnerText = this.GetHeaderFooterText(this.FirstFooter);
                }
            }
        }

        /// <summary>
        /// Helper function for Save
        /// </summary>
        /// <param name="inStruct">The header footer struct.</param>
        /// <returns>The footer text.</returns>
        protected internal string GetHeaderFooterText(ExcelHeaderFooterText inStruct)
        {
            string retValue = String.Empty;

            if (inStruct.LeftAlignedText != null)
            {
                retValue += "&L" + inStruct.LeftAlignedText;
            }

            if (inStruct.CenteredText != null)
            {
                retValue += "&C" + inStruct.CenteredText;
            }

            if (inStruct.RightAlignedText != null)
            {
                retValue += "&R" + inStruct.RightAlignedText;
            }

            return retValue;
        }
    }
}