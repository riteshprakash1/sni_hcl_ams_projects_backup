// -------------------------------------------------------------
// <copyright file="ExcelColumn.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author               Change              Date
 * ******************************************************************************
 * John Tunnicliffe     Initial Release     01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Xml;

    /// <summary>
    /// Represents an individual column within the worksheet
    /// </summary>
    public class ExcelColumn
    {
        /// <summary>
        /// The excel worksheet
        /// </summary>
        private ExcelWorksheet myXlWorksheet;

        /// <summary>
        /// The column element.
        /// </summary>
        private XmlElement myColElement = null;

        /// <summary>
        /// The namespace manager.
        /// </summary>
        private XmlNamespaceManager myNsManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelColumn"/> class.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="col">The column.</param>
        protected internal ExcelColumn(ExcelWorksheet worksheet, int col)
        {
            NameTable nt = new NameTable();
            this.myNsManager = new XmlNamespaceManager(nt);
            this.myNsManager.AddNamespace("d", ExcelPackage.SchemaMain);

            this.myXlWorksheet = worksheet;
            XmlNode parent = worksheet.WorksheetXml.SelectSingleNode("//d:cols", this.myNsManager);
            if (parent == null)
            {
                parent = (XmlNode)worksheet.WorksheetXml.CreateElement("cols", ExcelPackage.SchemaMain);
                XmlNode refChild = worksheet.WorksheetXml.SelectSingleNode("//d:sheetData", this.myNsManager);
                parent = worksheet.WorksheetXml.DocumentElement.InsertBefore(parent, refChild);
            }

            XmlAttribute minAttr;
            XmlAttribute maxAttr;
            XmlNode insertBefore = null;
            //// the column definitions cover a range of columns, so find the one we want
            bool insertBeforeFound = false;
            foreach (XmlNode colNode in parent.ChildNodes)
            {
                int min = 1;
                int max = 1;
                minAttr = (XmlAttribute)colNode.Attributes.GetNamedItem("min");
                if (minAttr != null)
                {
                    min = int.Parse(minAttr.Value);
                }

                maxAttr = (XmlAttribute)colNode.Attributes.GetNamedItem("max");

                if (maxAttr != null)
                {
                    max = int.Parse(maxAttr.Value);
                }

                if (!insertBeforeFound && (col <= min || col <= max))
                {
                    insertBeforeFound = true;
                    insertBefore = colNode;
                }

                if (col >= min && col <= max)
                {
                    this.myColElement = (XmlElement)colNode;
                    break;
                }
            }

            if (this.myColElement == null)
            {
                //// create the new column definition
                this.myColElement = worksheet.WorksheetXml.CreateElement("col", ExcelPackage.SchemaMain);
                this.myColElement.SetAttribute("min", col.ToString());
                this.myColElement.SetAttribute("max", col.ToString());

                if (insertBefore != null)
                {
                    parent.InsertBefore(this.myColElement, insertBefore);
                }
                else
                {
                    parent.AppendChild(this.myColElement);
                }
            }
        }

        /// <summary>
        /// Gets or sets the first column the definition refers to.
        /// </summary>
        public int ColumnMin
        {
            get
            {
                return int.Parse(this.myColElement.GetAttribute("min"));
            }

            set
            {
                this.myColElement.SetAttribute("min", value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the last column the definition refers to.
        /// </summary>
        public int ColumnMax
        {
            get
            {
                return int.Parse(this.myColElement.GetAttribute("max"));
            }

            set
            {
                this.myColElement.SetAttribute("max", value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ExcelColumn"/> is hidden.
        /// </summary>
        /// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
        public bool Hidden
        {
            get
            {
                bool retValue = false;
                string hidden = this.myColElement.GetAttribute("hidden", "1");
                if (hidden == "1")
                {
                    retValue = true;
                }

                return retValue;
            }

            set
            {
                if (value)
                {
                    this.myColElement.SetAttribute("hidden", "1");
                }
                else
                {
                    this.myColElement.SetAttribute("hidden", "0");
                }
            }
        }

        /// <summary>
        /// Gets or sets the width of the column in the worksheet
        /// </summary>
        public double Width
        {
            get
            {
                double retValue = 10;  //// default column size
                string width = this.myColElement.GetAttribute("width");
                if (width != String.Empty)
                {
                    retValue = int.Parse(width);
                }

                return retValue;
            }

            set
            {
                this.myColElement.SetAttribute("width", value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the style for the entire column using a style name.
        /// </summary>
        public string Style
        {
            get
            {
                return this.myXlWorksheet.GetStyleName(this.StyleID);
            }

            set
            {
                //// TODO: implement correctly.  The current code causes Excel to throw a fit!
                this.StyleID = this.myXlWorksheet.GetStyleID(value);
            }
        }

        /// <summary>
        /// Gets or sets the style for the entire column using the style ID.  
        /// </summary>
        public int StyleID
        {
            get
            {
                int retValue = 0;
                string sid = this.myColElement.GetAttribute("s");
                if (sid != String.Empty)
                {
                    retValue = int.Parse(sid);
                }

                return retValue;
            }

            set
            {
                this.myColElement.SetAttribute("s", value.ToString());
            }
        }

        /// <summary>
        /// Gets a reference to the Element that represents the column.
        /// For internal use only!
        /// </summary>
        /// <value>The element.</value>
        protected internal XmlElement Element
        {
            get
            {
                return this.myColElement;
            }
        }

        /// <summary>
        /// Returns the range of columns covered by the column definition.
        /// </summary>
        /// <returns>A string describing the range of columns covered by the column definition.</returns>
        public override string ToString()
        {
            return string.Format("Column Range: {0} to {1}", this.myColElement.GetAttribute("min"), this.myColElement.GetAttribute("min"));
        }
    }
}
