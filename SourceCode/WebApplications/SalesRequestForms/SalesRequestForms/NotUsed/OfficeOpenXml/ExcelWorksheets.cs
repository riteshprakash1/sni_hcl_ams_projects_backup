// -----------------------------------------------------------------
// <copyright file="ExcelWorksheets.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -----------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Packaging;
    using System.Xml;

    /// <summary>
    /// Provides enumeration through all the worksheets in the workbook
    /// </summary>
    public class ExcelWorksheets : IEnumerable
    {
        /// <summary>
        /// These are my worksheets.
        /// </summary>
        private Dictionary<int, ExcelWorksheet> myWorksheets;

        /// <summary>
        /// This is my excel parent package.
        /// </summary>
        private ExcelPackage myXlPackage;

        /// <summary>
        /// This is my namespace manager.
        /// </summary>
        private XmlNamespaceManager myNsManager;

        /// <summary>
        /// This is my worksheets node.
        /// </summary>
        private XmlNode myWorksheetsNode;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelWorksheets"/> class.
        /// </summary>
        /// <param name="xlsPackage">The XLS package.</param>
        protected internal ExcelWorksheets(ExcelPackage xlsPackage)
        {
            this.myXlPackage = xlsPackage;
            ////  Create a NamespaceManager to handle the default namespace, 
            ////  and create a prefix for the default namespace:
            NameTable nt = new NameTable();
            this.myNsManager = new XmlNamespaceManager(nt);
            this.myNsManager.AddNamespace("d", ExcelPackage.SchemaMain);
            this.myNsManager.AddNamespace("r", ExcelPackage.SchemaRelationships);

            //// obtain container node for all worksheets
            this.myWorksheetsNode = this.myXlPackage.Workbook.WorkbookXml.SelectSingleNode("//d:sheets", this.myNsManager);
            if (this.myWorksheetsNode == null)
            {
                //// create new node as it did not exist
                this.myWorksheetsNode = this.myXlPackage.Workbook.WorkbookXml.CreateElement("sheets", ExcelPackage.SchemaMain);
                this.myXlPackage.Workbook.WorkbookXml.DocumentElement.AppendChild(this.myWorksheetsNode);
            }

            this.myWorksheets = new Dictionary<int, ExcelWorksheet>();
            int positionID = 1;
            foreach (XmlNode sheetNode in this.myWorksheetsNode.ChildNodes)
            {
                string name = sheetNode.Attributes["name"].Value;
                ////  Get the relationship id attribute:
                string relId = sheetNode.Attributes["r:id"].Value;
                int sheetID = Convert.ToInt32(sheetNode.Attributes["sheetId"].Value);
                ////if (sheetID != count)
                ////{
                ////  //// renumber the sheets as they are in an odd order
                ////  sheetID = count;
                ////  sheetNode.Attributes["sheetId"].Value = sheetID.ToString();
                ////}
                //// get hidden attribute (if present)
                bool hidden = false;
                XmlNode attr = sheetNode.Attributes["hidden"];
                if (attr != null)
                {
                    hidden = Convert.ToBoolean(attr.Value);
                }

                ////string type = "";
                ////attr = sheetNode.Attributes["type"];
                ////if (attr != null)
                ////  type = attr.Value;

                PackageRelationship sheetRelation = this.myXlPackage.Workbook.Part.GetRelationship(relId);
                Uri uriWorksheet = PackUriHelper.ResolvePartUri(this.myXlPackage.Workbook.WorkbookUri, sheetRelation.TargetUri);

                //// add worksheet to our collection
                this.myWorksheets.Add(positionID, new ExcelWorksheet(this.myXlPackage, relId, name, uriWorksheet, sheetID, hidden));
                positionID++;
            }
        }

        /// <summary>
        /// Gets the number of worksheets in the workbook
        /// </summary>
        public int Count
        {
            get
            {
                return this.myWorksheets.Count;
            }
        }

        /// <summary>
        /// Returns the worksheet at the specified position.  
        /// </summary>
        /// <param name="positionID">The position of the worksheet. 1-base</param>
        /// <returns></returns>
        public ExcelWorksheet this[int positionID]
        {
            get
            {
                return this.myWorksheets[positionID];
            }
        }

        /// <summary>
        /// Returns the worksheet matching the specified name
        /// </summary>
        /// <param name="name">The name of the worksheet</param>
        /// <returns></returns>
        public ExcelWorksheet this[string name]
        {
            get
            {
                ExcelWorksheet xlsWorksheet = null;
                foreach (ExcelWorksheet worksheet in this.myWorksheets.Values)
                {
                    if (worksheet.Name == name)
                    {
                        xlsWorksheet = worksheet;
                    }
                }

                return xlsWorksheet;
                ////throw new Exception(string.Format("ExcelWorksheets Error: Worksheet '{0}' not found!",Name));
            }
        }

        /// <summary>
        /// Returns an enumerator that allows the foreach syntax to be used to 
        /// itterate through all the worksheets
        /// </summary>
        /// <returns>An enumerator</returns>
        public IEnumerator GetEnumerator()
        {
            return this.myWorksheets.Values.GetEnumerator();
        }

        /// <summary>
        /// Adds a blank worksheet with the desired name
        /// </summary>
        /// <param name="name">The name of the worksheet.</param>
        /// <returns>The new excel worksheet.</returns>
        public ExcelWorksheet Add(string name)
        {
            //// first find maximum existing sheetID
            //// also check the name is unique - if not throw an error
            int sheetID = 0;
            foreach (XmlNode sheet in this.myWorksheetsNode.ChildNodes)
            {
                XmlAttribute attr = (XmlAttribute)sheet.Attributes.GetNamedItem("sheetId");
                if (attr != null)
                {
                    int curID = int.Parse(attr.Value);
                    if (curID > sheetID)
                    {
                        sheetID = curID;
                    }
                }

                attr = (XmlAttribute)sheet.Attributes.GetNamedItem("name");
                if (attr != null)
                {
                    if (attr.Value == name)
                    {
                        throw new Exception("Add worksheet Error: attempting to create worksheet with duplicate name");
                    }
                }
            }
            //// we now have the max existing values, so add one
            sheetID++;

            //// add the new worksheet to the package
            Uri uriWorksheet = new Uri("/xl/worksheets/sheet" + sheetID.ToString() + ".xml", UriKind.Relative);
            PackagePart worksheetPart = this.myXlPackage.Package.CreatePart(uriWorksheet, @"application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml");

            //// create the new, empty worksheet and save it to the package
            StreamWriter streamWorksheet = new StreamWriter(worksheetPart.GetStream(FileMode.Create, FileAccess.Write));
            XmlDocument worksheetXml = this.CreateNewWorksheet();
            worksheetXml.Save(streamWorksheet);
            streamWorksheet.Close();
            this.myXlPackage.Package.Flush();

            //// create the relationship between the workbook and the new worksheet
            PackageRelationship rel = this.myXlPackage.Workbook.Part.CreateRelationship(uriWorksheet, TargetMode.Internal, ExcelPackage.SchemaRelationships + "/worksheet");
            this.myXlPackage.Package.Flush();

            //// now create the new worksheet tag and set name/SheetId attributes in the workbook.xml
            XmlElement worksheetNode = this.myXlPackage.Workbook.WorkbookXml.CreateElement("sheet", ExcelPackage.SchemaMain);
            //// create the new sheet node
            worksheetNode.SetAttribute("name", name);
            worksheetNode.SetAttribute("sheetId", sheetID.ToString());
            //// set the r:id attribute
            worksheetNode.SetAttribute("id", ExcelPackage.SchemaRelationships, rel.Id);
            //// insert the sheet tag with all attributes set as above
            this.myWorksheetsNode.AppendChild(worksheetNode);

            //// create a reference to the new worksheet in our collection
            ExcelWorksheet worksheet = new ExcelWorksheet(this.myXlPackage, rel.Id, name, uriWorksheet, sheetID, false);
            int positionID = this.myWorksheets.Count + 1;
            this.myWorksheets.Add(positionID, worksheet);
            return worksheet;
        }

        /// <summary>
        /// Delete a worksheet from the workbook package
        /// </summary>
        /// <param name="positionID">The position of the worksheet in the workbook</param>
        public void Delete(int positionID)
        {
            if (this.myWorksheets.Count == 1)
            {
                throw new Exception("Error: You are attempting to delete the last worksheet in the workbook.  One worksheet MUST be present in the workbook!");
            }

            ExcelWorksheet worksheet = this.myWorksheets[positionID];

            //// delete the worksheet from the package 
            this.myXlPackage.Package.DeletePart(worksheet.WorksheetUri);

            //// delete the relationship from the package 
            this.myXlPackage.Workbook.Part.DeleteRelationship(worksheet.RelationshipID);

            //// delete worksheet from the workbook XML
            XmlNode sheetsNode = this.myXlPackage.Workbook.WorkbookXml.SelectSingleNode("//d:workbook/d:sheets", this.myNsManager);
            if (sheetsNode != null)
            {
                XmlNode sheetNode = sheetsNode.SelectSingleNode(string.Format("./d:sheet[@sheetId={0}]", worksheet.SheetID), this.myNsManager);
                if (sheetNode != null)
                {
                    sheetsNode.RemoveChild(sheetNode);
                }
            }
            //// delete worksheet from the Dictionary object
            this.myWorksheets.Remove(positionID);
        }

        /// <summary>
        /// Copies the named worksheet and creates a new worksheet in the same workbook
        /// </summary>
        /// <param name="name">The name of the existing worksheet</param>
        /// <param name="newName">The name of the new worksheet to create</param>
        /// <returns>The worksheet.</returns>
        public ExcelWorksheet Copy(string name, string newName)
        {
            //// TODO: implement copy worksheet
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Creates the XML document representing a new empty worksheet
        /// </summary>
        /// <returns>A new worksheet.</returns>
        protected internal XmlDocument CreateNewWorksheet()
        {
            //// create the new worksheet
            XmlDocument worksheetXml = new XmlDocument();
            //// XML document does not exist so create the new worksheet XML doc
            XmlElement worksheetNode = worksheetXml.CreateElement("worksheet", ExcelPackage.SchemaMain);
            worksheetNode.SetAttribute("xmlns:r", ExcelPackage.SchemaRelationships);
            worksheetXml.AppendChild(worksheetNode);
            //// create the sheetViews tag
            XmlElement tagSheetViews = worksheetXml.CreateElement("sheetViews", ExcelPackage.SchemaMain);
            worksheetNode.AppendChild(tagSheetViews);
            //// create the sheet View tag
            XmlElement tagSheetView = worksheetXml.CreateElement("sheetView", ExcelPackage.SchemaMain);
            tagSheetView.SetAttribute("workbookViewId", "0");
            tagSheetViews.AppendChild(tagSheetView);
            //// create the empty sheetData tag (must be present, but can be empty)
            XmlElement tagSheetData = worksheetXml.CreateElement("sheetData", ExcelPackage.SchemaMain);
            worksheetNode.AppendChild(tagSheetData);
            return worksheetXml;
        }
    } //// end class Worksheets
}
