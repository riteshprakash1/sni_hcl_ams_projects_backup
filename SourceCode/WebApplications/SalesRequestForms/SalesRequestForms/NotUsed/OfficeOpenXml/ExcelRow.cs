// --------------------------------------------------------
// <copyright file="ExcelRow.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Xml;

    /// <summary>
    /// Represents an individual row in the spreadsheet.
    /// </summary>
    public class ExcelRow
    {
        /// <summary>
        /// My excel worksheet.
        /// </summary>
        private ExcelWorksheet myXlWorksheet;

        /// <summary>
        /// My row element.
        /// </summary>
        private XmlElement myRowElement = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelRow"/> class.
        /// </summary>
        /// <param name="worksheet">The parent worksheet.</param>
        /// <param name="row">The row line we are on.</param>
        protected internal ExcelRow(ExcelWorksheet worksheet, int row)
        {
            this.myXlWorksheet = worksheet;

            ////  Search for the existing row
            this.myRowElement = (XmlElement)worksheet.WorksheetXml.SelectSingleNode(string.Format("//d:sheetData/d:row[@r='{0}']", row), this.myXlWorksheet.NameSpaceManager);
            if (this.myRowElement == null)
            {
                //// We didn't find the row, so add a new row element.
                //// HOWEVER we MUST insert new row in the correct position - otherwise Excel 2007 will complain!!!
                this.myRowElement = worksheet.WorksheetXml.CreateElement("row", ExcelPackage.SchemaMain);
                this.myRowElement.SetAttribute("r", row.ToString());

                //// now work out where to insert the new row
                XmlNode sheetDataNode = worksheet.WorksheetXml.SelectSingleNode("//d:sheetData", this.myXlWorksheet.NameSpaceManager);
                if (sheetDataNode != null)
                {
                    XmlNode followingRow = null;
                    foreach (XmlNode currentRow in worksheet.WorksheetXml.SelectNodes("//d:sheetData/d:row", this.myXlWorksheet.NameSpaceManager))
                    {
                        int rowFound = Convert.ToInt32(currentRow.Attributes.GetNamedItem("r").Value);
                        if (rowFound > row)
                        {
                            followingRow = currentRow;
                            break;
                        }
                    }

                    if (followingRow == null)
                    {
                        //// no data rows exist, so just add row
                        sheetDataNode.AppendChild(this.myRowElement);
                    }
                    else
                    {
                        sheetDataNode.InsertBefore(this.myRowElement, followingRow);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the row to be hidden in the worksheet
        /// </summary>
        public bool Hidden
        {
            get
            {
                bool retValue = false;
                string hidden = this.myRowElement.GetAttribute("hidden", "1");
                if (hidden == "1")
                {
                    retValue = true;
                }

                return retValue;
            }

            set
            {
                if (value)
                {
                    this.myRowElement.SetAttribute("hidden", "1");
                }
                else
                {
                    this.myRowElement.SetAttribute("hidden", "0");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the height of the row
        /// </summary>
        public double Height
        {
            get
            {
                double retValue = 10;  //// default row height
                string ht = this.myRowElement.GetAttribute("ht");
                if (ht != String.Empty)
                {
                    retValue = double.Parse(ht);
                }

                return retValue;
            }

            set
            {
                this.myRowElement.SetAttribute("ht", value.ToString());
                //// we must set customHeight="1" for the height setting to take effect
                this.myRowElement.SetAttribute("customHeight", "1");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the style name based on the StyleID
        /// </summary>
        public string Style
        {
            get
            {
                return this.myXlWorksheet.GetStyleName(this.StyleID);
            }

            set
            {
                this.StyleID = this.myXlWorksheet.GetStyleID(value);
            }
        }

        /// <summary>
        /// Gets or sets the style for the entire row using the style ID.  
        /// </summary>
        public int StyleID
        {
            get
            {
                int retValue = 0;
                string sid = this.myRowElement.GetAttribute("s");
                if (sid != String.Empty)
                {
                    retValue = int.Parse(sid);
                }

                return retValue;
            }

            set
            {
                this.myRowElement.SetAttribute("s", value.ToString());
                //// to get Excel to apply this style we need to set customFormat="1"
                this.myRowElement.SetAttribute("customFormat", "1");
            }
        }

        /// <summary>
        /// Gets access to the node representing the row.
        /// For internal use only!
        /// </summary>
        protected internal XmlNode Node
        {
            get
            {
                return this.myRowElement;
            }
        }
    }
}
