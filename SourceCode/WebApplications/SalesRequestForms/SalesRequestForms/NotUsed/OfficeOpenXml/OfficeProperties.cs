// ------------------------------------------------------------------
// <copyright file="OfficeProperties.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.IO;
    using System.IO.Packaging;
    using System.Xml;

    /// <summary>
    /// Provides access to the properties bag of any office document (i.e. Word, Excel etc.)
    /// </summary>
    public class OfficeProperties
    {
        /// <summary>
        /// The core schema.
        /// </summary>
        private const string SchemaCore = @"http://schemas.openxmlformats.org/package/2006/metadata/core-properties";

        /// <summary>
        /// The extended schema.
        /// </summary>
        private const string SchemeExtended = @"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties";

        /// <summary>
        /// The custom schema.
        /// </summary>
        private const string SchemaCustom = @"http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";

        /// <summary>
        /// The schema dc.
        /// </summary>
        private const string SchemaDc = @"http://purl.org/dc/elements/1.1/";

        /// <summary>
        /// The dc terms.
        /// </summary>
        private const string SchemaDcTerms = @"http://purl.org/dc/terms/";

        /// <summary>
        /// The dcmi schema.
        /// </summary>
        private const string SchemaDcmiType = @"http://purl.org/dc/dcmitype/";

        /// <summary>
        /// The XSI schema.
        /// </summary>
        private const string SchemaXsi = @"http://www.w3.org/2001/XMLSchema-instance";

        /// <summary>
        /// The vt schema.
        /// </summary>
        private const string SchemaVt = @"http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";

        /// <summary>
        /// The core uri properties.
        /// </summary>
        private Uri myUriPropertiesCore = new Uri("/docProps/core.xml", UriKind.Relative);

        /// <summary>
        /// The extended uri properties.
        /// </summary>
        private Uri myUriPropertiesExtended = new Uri("/docProps/app.xml", UriKind.Relative);

        /// <summary>
        /// The customer uri properties.
        /// </summary>
        private Uri myUriPropertiesCustom = new Uri("/docProps/custom.xml", UriKind.Relative);

        /// <summary>
        /// The core xml properties.
        /// </summary>
        private XmlDocument myXmlPropertiesCore;

        /// <summary>
        /// The xml core properties.
        /// </summary>
        private XmlDocument myXmlPropertiesExtended;

        /// <summary>
        /// The xml custom properties.
        /// </summary>
        private XmlDocument myXmlPropertiesCustom;

        /// <summary>
        /// The excel package.
        /// </summary>
        private ExcelPackage myXlPackage;

        /// <summary>
        /// The namespace manager.
        /// </summary>
        private XmlNamespaceManager myNsManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="OfficeProperties"/> class.
        /// </summary>
        /// <param name="xlsPackage">The XLS package.</param>
        public OfficeProperties(ExcelPackage xlsPackage)
        {
            this.myXlPackage = xlsPackage;
            ////  Create a NamespaceManager to handle the default namespace, 
            ////  and create a prefix for the default namespace:
            NameTable nt = new NameTable();
            this.myNsManager = new XmlNamespaceManager(nt);
            //// default namespace
            this.myNsManager.AddNamespace("d", ExcelPackage.SchemaMain);
            this.myNsManager.AddNamespace("vt", SchemaVt);
            //// extended properties (app.xml)
            this.myNsManager.AddNamespace("xp", SchemeExtended);
            //// custom properties
            this.myNsManager.AddNamespace("ctp", SchemaCustom);
            //// core properties
            this.myNsManager.AddNamespace("cp", SchemaCore);
            //// core property namespaces
            this.myNsManager.AddNamespace("dc", SchemaDc);
            this.myNsManager.AddNamespace("dcterms", SchemaDcTerms);
            this.myNsManager.AddNamespace("dcmitype", SchemaDcmiType);
            this.myNsManager.AddNamespace("xsi", SchemaXsi);
        }

        /// <summary>
        /// Gets access to the XML document that holds all the code 
        /// document properties.
        /// </summary>
        public XmlDocument CorePropertiesXml
        {
            get
            {
                if (this.myXmlPropertiesCore == null)
                {
                    if (this.myXlPackage.Package.PartExists(this.CorePropertiesUri))
                    {
                        this.myXmlPropertiesCore = this.myXlPackage.GetXmlFromUri(this.CorePropertiesUri);
                    }
                    else
                    {
                        //// create a new document properties part and add to the package
                        PackagePart partCore = this.myXlPackage.Package.CreatePart(this.CorePropertiesUri, @"application/vnd.openxmlformats-package.core-properties+xml");

                        //// create the document properties XML (with no entries in it)
                        this.myXmlPropertiesCore = new XmlDocument();
                        XmlElement root = this.myXmlPropertiesCore.CreateElement("cp:coreProperties", SchemaCore);
                        ExcelPackage.AddSchemaAttribute(root, SchemaCore, "cp");
                        ExcelPackage.AddSchemaAttribute(root, SchemaDc, "dc");
                        ExcelPackage.AddSchemaAttribute(root, SchemaDcTerms, "dcterms");
                        ExcelPackage.AddSchemaAttribute(root, SchemaDcmiType, "dcmitype");
                        ExcelPackage.AddSchemaAttribute(root, SchemaXsi, "xsi");
                        this.myXmlPropertiesCore.AppendChild(root);

                        //// save it to the package
                        StreamWriter streamCore = new StreamWriter(partCore.GetStream(FileMode.Create, FileAccess.Write));
                        this.myXmlPropertiesCore.Save(streamCore);
                        streamCore.Close();
                        this.myXlPackage.Package.Flush();

                        //// create the relationship between the workbook and the new shared strings part
                        this.myXlPackage.Package.CreateRelationship(this.CorePropertiesUri, TargetMode.Internal, @"http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties");
                        this.myXlPackage.Package.Flush();
                    }
                }

                return this.myXmlPropertiesCore;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating title property of the document (core property)
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetCorePropertyValue("dc", "title");
            }

            set
            {
                this.SetCorePropertyValue("dc", "title", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating subject property of the document (core property)
        /// </summary>
        public string Subject
        {
            get
            {
                return this.GetCorePropertyValue("dc", "subject");
            }

            set
            {
                this.SetCorePropertyValue("dc", "subject", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating author property of the document (core property)
        /// </summary>
        public string Author
        {
            get
            {
                return this.GetCorePropertyValue("dc", "creator");
            }

            set
            {
                this.SetCorePropertyValue("dc", "creator", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the comments property of the document (core property)
        /// </summary>
        public string Comments
        {
            get
            {
                return this.GetCorePropertyValue("dc", "description");
            }

            set
            {
                this.SetCorePropertyValue("dc", "description", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the keywords property of the document (core property)
        /// </summary>
        public string Keywords
        {
            get
            {
                return this.GetCorePropertyValue("cp", "keywords");
            }

            set
            {
                this.SetCorePropertyValue("cp", "keywords", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the lastModifiedBy property of the document (core property)
        /// </summary>
        public string LastModifiedBy
        {
            get
            {
                return this.GetCorePropertyValue("cp", "lastModifiedBy");
            }

            set
            {
                this.SetCorePropertyValue("cp", "lastModifiedBy", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the lastPrinted property of the document (core property)
        /// </summary>
        public string LastPrinted
        {
            get
            {
                return this.GetCorePropertyValue("cp", "lastPrinted");
            }

            set
            {
                this.SetCorePropertyValue("cp", "lastPrinted", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the category property of the document (core property)
        /// </summary>
        public string Category
        {
            get
            {
                return this.GetCorePropertyValue("cp", "category");
            }

            set
            {
                this.SetCorePropertyValue("cp", "category", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the status property of the document (core property)
        /// </summary>
        public string Status
        {
            get
            {
                return this.GetCorePropertyValue("cp", "contentStatus");
            }

            set
            {
                this.SetCorePropertyValue("cp", "contentStatus", value);
            }
        } 

        /// <summary>
        /// Gets access to the XML document that holds the extended properties of the document (app.xml)
        /// </summary>
        public XmlDocument ExtendedPropertiesXml
        {
            get
            {
                if (this.myXmlPropertiesExtended == null)
                {
                    if (this.myXlPackage.Package.PartExists(this.ExtendedPropertiesUri))
                    {
                        this.myXmlPropertiesExtended = this.myXlPackage.GetXmlFromUri(this.ExtendedPropertiesUri);
                    }
                    else
                    {
                        //// create a new extended properties part and add to the package
                        PackagePart partExtended = this.myXlPackage.Package.CreatePart(this.ExtendedPropertiesUri, @"application/vnd.openxmlformats-officedocument.extended-properties+xml");

                        //// create the extended properties XML (with no entries in it)
                        this.myXmlPropertiesExtended = new XmlDocument();
                        XmlElement root = this.myXmlPropertiesExtended.CreateElement("Properties", SchemeExtended);
                        ExcelPackage.AddSchemaAttribute(root, SchemaVt, "vt");
                        this.myXmlPropertiesExtended.AppendChild(root);

                        //// save it to the package
                        StreamWriter streamExtended = new StreamWriter(partExtended.GetStream(FileMode.Create, FileAccess.Write));
                        this.myXmlPropertiesExtended.Save(streamExtended);
                        streamExtended.Close();
                        this.myXlPackage.Package.Flush();

                        //// create the relationship between the workbook and the new shared strings part
                        this.myXlPackage.Package.CreateRelationship(this.ExtendedPropertiesUri, TargetMode.Internal, @"http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties");
                        this.myXlPackage.Package.Flush();
                    }
                }

                return this.myXmlPropertiesExtended;
            }
        }

        /// <summary>
        /// Gets the Application property of the document (extended property)
        /// </summary>
        public string Application
        {
            get
            {
                return this.GetExtendedPropertyValue("Application");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the HyperlinkBase property of the document (extended property)
        /// </summary>
        public Uri HyperlinkBase
        {
            get
            {
                return new Uri(this.GetExtendedPropertyValue("HyperlinkBase"));
            }

            set
            {
                this.SetExtendedPropertyValue("HyperlinkBase", value.ToString());
            }
        }

        /// <summary>
        /// Gets the AppVersion property of the document (extended property)
        /// </summary>
        public string AppVersion
        {
            get
            {
                return this.GetExtendedPropertyValue("AppVersion");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the Company property of the document (extended property)
        /// </summary>
        public string Company
        {
            get
            {
                return this.GetExtendedPropertyValue("Company");
            }

            set
            {
                this.SetExtendedPropertyValue("Company", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the Manager property of the document (extended property)
        /// </summary>
        public string Manager
        {
            get
            {
                return this.GetExtendedPropertyValue("Manager");
            }

            set
            {
                this.SetExtendedPropertyValue("Manager", value);
            }
        }

        /// <summary>
        /// Gets access to the XML document which holds the document's custom properties
        /// </summary>
        public XmlDocument CustomPropertiesXml
        {
            get
            {
                if (this.myXmlPropertiesCustom == null)
                {
                    if (this.myXlPackage.Package.PartExists(this.CustomPropertiesUri))
                    {
                        this.myXmlPropertiesCustom = this.myXlPackage.GetXmlFromUri(this.CustomPropertiesUri);
                    }
                    else
                    {
                        //// create a new extended properties part and add to the package
                        PackagePart partCustom = this.myXlPackage.Package.CreatePart(this.CustomPropertiesUri, @"application/vnd.openxmlformats-officedocument.custom-properties+xml");

                        //// create the extended properties XML (with no entries in it)
                        this.myXmlPropertiesCustom = new XmlDocument();
                        XmlElement root = this.myXmlPropertiesCustom.CreateElement("Properties", SchemaCustom);
                        ExcelPackage.AddSchemaAttribute(root, SchemaVt, "vt");
                        this.myXmlPropertiesCustom.AppendChild(root);

                        //// save it to the package
                        StreamWriter streamCustom = new StreamWriter(partCustom.GetStream(FileMode.Create, FileAccess.Write));
                        this.myXmlPropertiesCustom.Save(streamCustom);
                        streamCustom.Close();
                        this.myXlPackage.Package.Flush();

                        //// create the relationship between the workbook and the new shared strings part
                        this.myXlPackage.Package.CreateRelationship(this.CustomPropertiesUri, TargetMode.Internal, @"http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties");
                        this.myXlPackage.Package.Flush();
                    }
                }

                return this.myXmlPropertiesCustom;
            }
        }

        /// <summary>
        /// Gets The URI to the core properties component (core.xml)
        /// </summary>
        protected internal Uri CorePropertiesUri
        {
            get
            {
                return this.myUriPropertiesCore;
            }
        }

        /// <summary>
        /// Gets The URI to the extended properties component (app.xml)
        /// </summary>
        protected internal Uri ExtendedPropertiesUri
        {
            get
            {
                return this.myUriPropertiesExtended;
            }
        }

        /// <summary>
        /// Gets The URI to the custom properties component (custom.xml)
        /// </summary>
        protected internal Uri CustomPropertiesUri
        {
            get
            {
                return this.myUriPropertiesCustom;
            }
        }

        /// <summary>
        /// Gets the value of a custom property
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        /// <returns>The current value of the property</returns>
        public string GetCustomPropertyValue(string propertyName)
        {
            string retValue = null;
            string searchString = string.Format("//ctp:Properties/ctp:property/@name[.='{0}']", propertyName);
            XmlNode node = this.CustomPropertiesXml.SelectSingleNode(searchString, this.myNsManager);
            if (node != null)
            {
                retValue = node.LastChild.InnerText;
            }

            return retValue;
        }

        /// <summary>
        /// Allows you to set the value of a current custom property or create 
        /// your own custom property.  
        /// Currently only supports string values.
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="propValue">The value of the property</param>
        public void SetCustomPropertyValue(string propertyName, string propValue)
        {
            //// TODO:  provide support for other custom property data types
            string searchString = @"//ctp:Properties";
            XmlNode allProps = this.CustomPropertiesXml.SelectSingleNode(searchString, this.myNsManager);

            searchString = string.Format("//ctp:Properties/ctp:property/@ctp:name[.='{0}']", propertyName);
            XmlNode node = this.CustomPropertiesXml.SelectSingleNode(searchString, this.myNsManager);
            if (node == null)
            {
                //// the property does not exist, so first find the max PID
                int pid = 4;
                foreach (XmlNode prop in allProps.ChildNodes)
                {
                    XmlAttribute attr = (XmlAttribute)prop.Attributes.GetNamedItem("pid");
                    if (attr != null)
                    {
                        int attrValue = int.Parse(attr.Value);
                        if (attrValue > pid)
                        {
                            pid = attrValue;
                        }
                    }
                }

                pid++;
                //// the property does not exist, so create the XML node
                XmlElement element = this.CustomPropertiesXml.CreateElement("property", SchemaCustom);
                element.SetAttribute("fmtid", "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}");
                element.SetAttribute("pid", pid.ToString());  //// custom property pid
                element.SetAttribute("name", propertyName);

                XmlElement valueElement = this.CustomPropertiesXml.CreateElement("vt", "lpwstr", SchemaVt);
                valueElement.InnerText = propValue;
                element.AppendChild(valueElement);

                this.CustomPropertiesXml.DocumentElement.AppendChild(element);
            }
            else
            {
                node.LastChild.InnerText = propValue;
            }
        }

        /// <summary>
        /// Saves the office document properties back to the package (if they exist!).
        /// </summary>
        protected internal void Save()
        {
            if (this.myXmlPropertiesCore != null)
            {
                this.myXlPackage.WriteDebugFile(this.myXmlPropertiesCore, "docProps", "core.xml");
                this.myXlPackage.SavePart(this.CorePropertiesUri, this.myXmlPropertiesCore);
            }

            if (this.myXmlPropertiesExtended != null)
            {
                this.myXlPackage.WriteDebugFile(this.myXmlPropertiesExtended, "docProps", "app.xml");
                this.myXlPackage.SavePart(this.ExtendedPropertiesUri, this.myXmlPropertiesExtended);
            }

            if (this.myXmlPropertiesCustom != null)
            {
                this.myXlPackage.WriteDebugFile(this.myXmlPropertiesCustom, "docProps", "custom.xml");
                this.myXlPackage.SavePart(this.CustomPropertiesUri, this.myXmlPropertiesCustom);
            }
        }

        /// <summary>
        /// Gets the extended property value.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>Returns the extende property value string.</returns>
        private string GetExtendedPropertyValue(string propertyName)
        {
            string retValue = null;
            string searchString = string.Format("//xp:Properties/xp:{0}", propertyName);
            XmlNode node = this.ExtendedPropertiesXml.SelectSingleNode(searchString, this.myNsManager);
            if (node != null)
            {
                retValue = node.InnerText;
            }

            return retValue;
        }

        /// <summary>
        /// Sets the extended property value.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propValue">The prop value.</param>
        private void SetExtendedPropertyValue(string propertyName, string propValue)
        {
            string searchString = string.Format("//xp:Properties/xp:{0}", propertyName);
            XmlNode node = this.ExtendedPropertiesXml.SelectSingleNode(searchString, this.myNsManager);
            if (node == null)
            {
                //// the property does not exist, so create the XML node
                node = (XmlNode)this.ExtendedPropertiesXml.CreateElement(propertyName, SchemeExtended);
                this.ExtendedPropertiesXml.DocumentElement.AppendChild(node);
            }

            node.InnerText = propValue;
        }

        /// <summary>
        /// Gets the value of a core property
        /// Private method, for internal use only!
        /// </summary>
        /// <param name="nameSpace">The namespace of the property</param>
        /// <param name="propertyName">The property name</param>
        /// <returns>The current value of the property</returns>
        private string GetCorePropertyValue(string nameSpace, string propertyName)
        {
            string retValue = null;
            string searchString = string.Format("//cp:coreProperties/{0}:{1}", nameSpace, propertyName);
            XmlNode node = this.CorePropertiesXml.SelectSingleNode(searchString, this.myNsManager);
            if (node != null)
            {
                retValue = node.InnerText;
            }

            return retValue;
        }

        /// <summary>
        /// Sets a core property value.
        /// Private method, for internal use only!
        /// </summary>
        /// <param name="nameSpace">The property's namespace</param>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="propValue">The value of the property</param>
        private void SetCorePropertyValue(string nameSpace, string propertyName, string propValue)
        {
            string searchString = string.Format("//cp:coreProperties/{0}:{1}", nameSpace, propertyName);
            XmlNode node = this.CorePropertiesXml.SelectSingleNode(searchString, this.myNsManager);
            if (node == null)
            {
                //// the property does not exist, so create the XML node
                string schema = SchemaCore;
                switch (nameSpace)
                {
                    case "cp":
                        schema = SchemaCore;
                        break;
                    case "dc":
                        schema = SchemaDc;
                        break;
                    case "dcterms":
                        schema = SchemaDcTerms;
                        break;
                    case "dcmitype":
                        schema = SchemaDcmiType;
                        break;
                    case "xsi":
                        schema = SchemaXsi;
                        break;
                }

                node = (XmlNode)this.CorePropertiesXml.CreateElement(nameSpace, propertyName, schema);
                this.CorePropertiesXml.DocumentElement.AppendChild(node);
            }

            node.InnerText = propValue;
        }
    }
}