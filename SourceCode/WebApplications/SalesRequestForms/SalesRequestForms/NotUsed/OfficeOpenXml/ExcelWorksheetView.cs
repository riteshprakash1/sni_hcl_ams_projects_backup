// ------------------------------------------------------------------
// <copyright file="ExcelWorksheetView.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Xml;

    /// <summary>
    /// Represents the different view states of the worksheet
    /// </summary>
    public class ExcelWorksheetView
    {
        /// <summary>
        /// The excel worksheet.
        /// </summary>
        private ExcelWorksheet myXlWorksheet;

        /// <summary>
        /// The worksheet view.
        /// </summary>
        private XmlElement mySheetView;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelWorksheetView"/> class.
        /// </summary>
        /// <param name="xlsWorksheet">The XLS worksheet.</param>
        protected internal ExcelWorksheetView(ExcelWorksheet xlsWorksheet)
        {
            this.myXlWorksheet = xlsWorksheet;
        }

        /// <summary>
        /// Gets or sets a value indicating whether if the worksheet is selected within the workbook
        /// </summary>
        public bool TabSelected
        {
            get
            {
                bool retValue = false;
                string ret = this.SheetViewElement.GetAttribute("tabSelected");
                if (ret == "1")
                {
                    retValue = true;
                }

                return retValue;
            }

            set
            {
                //// the sheetView node should always exist, so no need to create
                if (value)
                {
                    //// ensure no other worksheet has its tabSelected attribute set to 1
                    foreach (ExcelWorksheet sheet in this.myXlWorksheet.XlsPackage.Workbook.Worksheets)
                    {
                        sheet.View.TabSelected = false;
                    }

                    this.SheetViewElement.SetAttribute("tabSelected", "1");
                }
                else
                {
                    this.SheetViewElement.SetAttribute("tabSelected", "0");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the view mode of the worksheet to pageLayout
        /// </summary>
        public bool PageLayoutView
        {
            get
            {
                bool retValue = false;
                string ret = this.SheetViewElement.GetAttribute("view");
                if (ret == "pageLayout")
                {
                    retValue = true;
                }

                return retValue;
            }

            set
            {
                if (value)
                {
                    this.SheetViewElement.SetAttribute("view", "pageLayout");
                }
                else
                {
                    this.SheetViewElement.RemoveAttribute("view");
                }
            }
        }

        /// <summary>
        /// Gets a reference to the sheetView element
        /// </summary>
        protected internal XmlElement SheetViewElement
        {
            get
            {
                if (this.mySheetView == null)
                {
                    this.mySheetView = (XmlElement)this.myXlWorksheet.WorksheetXml.SelectSingleNode("//d:sheetView", this.myXlWorksheet.NameSpaceManager);
                }

                return this.mySheetView;
            }
        }
    }
}
