﻿<%@ Page Title="Country of Origin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="SalesRequestForms.COO.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
                <td style="font-size: large;text-decoration: underline;"><strong>Country of Origin Documents</strong>
                    <asp:HiddenField ID="hdnProgramType" runat="server" Value="CountryOfOrigin" />
                </td>
            </tr>          
        <tr>
            <td>
                <asp:GridView ID="gvDocuments" runat="server" Width="900px" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no documents." ShowFooter="false" ShowHeader="false"
                     CellPadding="5" DataKeyNames="DocumentID, FolderName, FileName" GridLines="None">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" Font-Size="10pt" Font-Bold="true" />
                    <Columns>
                       <asp:TemplateField HeaderStyle-Width="25%" HeaderText="URL" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkURL" NavigateUrl='<%# Eval("DocumentURL")%>' Text='<%# Eval("PublishedName")%>' Target="_blank"  runat="server"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
    </table>

</asp:Content>
