﻿// ------------------------------------------------------------------
// <copyright file="Index.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.COO
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary> This is the Index class. </summary>    
    public partial class Index : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindDocuments();
            }

        }

        /// <summary>Binds the AppDocuments List to the GridView.</summary>
        protected void BindDocuments()
        {
            List<DocumentEntity> docs = DocumentEntity.GetAppDocumentsByProgramType(this.hdnProgramType.Value);

            string strMainURL = Page.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            foreach (DocumentEntity doc in docs)
            {
                doc.DocumentURL = strMainURL + "\\COO\\" + doc.FileName;
            }

            if (docs.Count == 0)
            {
                DocumentEntity dummy = new DocumentEntity();
                docs.Add(dummy);
                Debug.WriteLine("Making Dummy Doc: " + dummy.DocumentID.ToString());
            }

            this.gvDocuments.DataSource = docs;
            this.gvDocuments.DataBind();
        }

        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (this.gvDocuments.DataKeys[e.Row.RowIndex]["DocumentID"].ToString() == "0")
                {
                    e.Row.Visible = false;
                }
            }
        } // end gv_RowDataBound

    }//// end class
}