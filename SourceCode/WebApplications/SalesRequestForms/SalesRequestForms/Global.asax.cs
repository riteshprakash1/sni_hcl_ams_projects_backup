﻿//---------------------------------------------------
// <copyright file="Global.asax.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
//---------------------------------------------------

namespace SalesRequestForms
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.SessionState;
    using SalesRequestForms.Classes;

    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        /// <summary>Handles the Error event of the Application control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = null;
            string authUser = Request.LogonUserIdentity.Name.ToString();
            string strErrorMsg = string.Empty;
            string strHtmlError = string.Empty;

            try
            {
                ex = Server.GetLastError().GetBaseException();
                //System.Diagnostics.Debug.WriteLine("Application Error: " + ex.Message + "; Inner: " + ex.ToString());
                Debug.WriteLine("Application Error: " + ex.Message);
                strErrorMsg = ex.Message;


                string strPotentiallyDangerous = "A potentially dangerous Request.Form value";
                if (strErrorMsg.ToLower().StartsWith(strPotentiallyDangerous.ToLower()))
                {
                    strHtmlError = strPotentiallyDangerous.ToLower();
                }
                else

                    strHtmlError = ex.Message.Replace("<", "&lt>;").Replace(">", "&gt;");

                LogException.HandleException(ex, Request);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine("ExceptionHandler Error: " + exc.Message);
            }

            Server.ClearError();

            //string strErrorPage = System.Configuration.ConfigurationManager.AppSettings["ErrorPage"];
            //Response.Redirect(strErrorPage + "?ErrorMsg=" + strHtmlError, false);


            string strReferringPage = Request.Url.ToString().ToLower();
            Debug.WriteLine("Global.ascx strErrorReferringPage: " + strReferringPage);

            if (!strReferringPage.Contains("errorpage"))
            {
                string strErrorPage = System.Configuration.ConfigurationManager.AppSettings["ErrorPage"];
                Response.Redirect(strErrorPage + "?ErrorMsg=" + strHtmlError, false);
            }

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
