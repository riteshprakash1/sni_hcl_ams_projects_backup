USE [SalesRequestForm]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Queries]') AND type in (N'U'))
 	DROP TABLE Queries 
GO

CREATE TABLE [dbo].Queries(
	QueryID [int] IDENTITY(1,1) NOT NULL,
	QueryName [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Query [nvarchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

DECLARE @strQuery VARCHAR(MAX);
SET @strQuery = 'SELECT cp.Customer_ID, c.Customer_Name, c.Street, City, c.State_Province_Code, c.Postal_Code, c.Gpo, c.Gpo_ID, cp.Partner_Function, '
SET @strQuery = @strQuery + 'cp.Partner_Function_Desc, cp.Sales_Org_ID FROM dim_CustomerPartner cp JOIN dim_Customer c ON cp.Customer_ID=c.Customer_ID '
SET @strQuery = @strQuery + 'AND cp.Sales_Org_ID = c.Sales_Org_ID AND c.Current_Row = ''Y'' WHERE cp.Sales_Org_ID = ''3000'' AND cp.Partner_Function = ''AG'' '
SET @strQuery = @strQuery + 'AND cp.customer_ID = '''

INSERT INTO Queries (QueryName, Query) VALUES ('SoldToCustomerByCustomerID', @strQuery)


SET @strQuery = 'SELECT cp.Customer_ID, c.Customer_Name, c.Street, City, c.State_Province_Code, c.Postal_Code, c.Gpo, '
SET @strQuery = @strQuery + 'c.Gpo_ID, cp.Partner_Function, cp.Partner_Function_Desc, cp.Sales_Org_ID '
SET @strQuery = @strQuery + 'FROM dim_CustomerPartner cp JOIN dim_Customer c ON cp.Customer_ID=c.Customer_ID AND cp.Sales_Org_ID = c.Sales_Org_ID AND c.Current_Row = ''Y'' '
SET @strQuery = @strQuery + 'WHERE cp.Sales_Org_ID = ''3000'' AND cp.Partner_Function IN (''AG'', ''WE'') AND cp.customer_ID = ''XXXXXXXXXX'' '
SET @strQuery = @strQuery + 'ORDER BY cp.Partner_Function DESC'

INSERT INTO Queries (QueryName, Query) VALUES ('ShipToCustomerByCustomerID', @strQuery)

--SELECT * FROM Queries
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdditionalCustomers]') AND type in (N'U'))
 	DROP TABLE AdditionalCustomers 

CREATE TABLE [dbo].AdditionalCustomers(
	[PlacedCapitalRequestID] [int] NULL,
	[CustomerID] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CustomerName] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Street] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StateCode] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PostalCode] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GpoDesc] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlacedCapitalMaterialRequest]') AND type in (N'U'))
 	DROP TABLE PlacedCapitalMaterialRequest 
GO
CREATE TABLE [dbo].PlacedCapitalMaterialRequest(
	[PlacedCapitalMaterialRequestID] [int] IDENTITY(1,1) NOT NULL,
	[PlacedCapitalRequestID] [int] NULL,
	[CapitalGroup] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MaterialID] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MaterialDesc] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ListPrice] [decimal](29, 3) NULL,
	[AnnualValue] [money] NULL,
	[ContractValue] [money] NULL,
	[AnnualRequiredPurchases] [money] NULL,
	[Qty] [int] NULL,
     ServiceUpchargeRate [float] NULL,
     MarginUpchargeRate [float] NULL,
     OverheadChargeRate [float] NULL,
     Depreciation [float] NULL,
     CommercialMarginRate [float] NULL
) ON [PRIMARY]


GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlacedCapitalRequest]') AND type in (N'U'))
 	DROP TABLE PlacedCapitalRequest 
GO

CREATE TABLE [dbo].[PlacedCapitalRequest](
	[PlacedCapitalRequestID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramID] [int] NOT NULL,
	[SoldToCustomerID] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SoldToCustomerName] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SoldToStreet] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SoldToCity] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SoldToStateCode] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SoldToPostalCode] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RvpName] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistrictName] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SalesRepName] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SalesRepID] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GpoDesc] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipToCustomerID] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipToCustomerName] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipToStreet] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipToCity] [nvarchar](75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipToStateCode] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShipToPostalCode] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailFax] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FacilityContact] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContractTerm] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	FormStamp [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubmittedByName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Factors]') AND type in (N'U'))
 	DROP TABLE Factors 
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProgramRates]') AND type in (N'U'))
 	DROP TABLE ProgramRates 
GO
CREATE TABLE [dbo].ProgramRates(
	ProgramRateID [int] IDENTITY(1,1) NOT NULL,
	ProgramID [int] NOT NULL,
	[CapitalGroup] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RateDescription [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Rate [float] NULL,
	RateText [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreatedBy [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreateDate [datetime] NULL,
	ModifiedBy [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ModifiedDate [datetime] NULL
) ON [PRIMARY]
GO
--Load Factor Table
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) 
VALUES (1, 'Shavers', 'Service Upcharge', 0.87, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, 'Pumps', 'Service Upcharge', 0.26, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, 'Generators', 'Service Upcharge', 0.1, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, '', 'Overhead Upcharge', 0.15, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, 'Shavers', 'Margin Upcharge', 1, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, 'Pumps', 'Margin Upcharge', 1, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, 'Generators', 'Margin Upcharge', 1, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, '', 'Commercially Reasonable Margin', 0.3, '', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, '', 'Contract Term', 1.0, 'Evergreen', 'colwelld', GetDate(), 'colwelld', GetDate())
INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) VALUES (1, '', 'Depreciation', 3.0, '', 'colwelld', GetDate(), 'colwelld', GetDate())
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RateDescriptions]') AND type in (N'U'))
 	DROP TABLE RateDescriptions 
GO
CREATE TABLE [dbo].RateDescriptions(
	RateDescriptionID [int] IDENTITY(1,1) NOT NULL,
	RateDescription [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

INSERT INTO RateDescriptions (RateDescription) VALUES ('Service Upcharge')
INSERT INTO RateDescriptions (RateDescription) VALUES ('Commercially Reasonable Margin')
INSERT INTO RateDescriptions (RateDescription) VALUES ('Overhead Upcharge')
INSERT INTO RateDescriptions (RateDescription) VALUES ('Margin Upcharge')
INSERT INTO RateDescriptions (RateDescription) VALUES ('Contract Term')
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlacedCapitalMaterial]') AND type in (N'U'))
 	DROP TABLE PlacedCapitalMaterial 
GO
CREATE TABLE [dbo].[PlacedCapitalMaterial](
	[CapitalGroup] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MaterialID] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MaterialDesc] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ListPrice] [decimal](29, 3) NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModifiedDate] [datetime] NULL

) ON [PRIMARY]

GO

INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72200872', 'MDU, HAND CNTRL, PWRMAX ELITE II (OPPOSITE SIDE SU', 1448)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72200873', 'DYONICS POWER II CONTROL UNIT', 1301)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72201092', 'DYONICS POWER II FOOTSWITCH', 678)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72201029', 'KIT, DYONICS POWER II', 27)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205841', 'REFURB, CONTROL UNIT ASSEMBLY', 1154)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205886', 'DYONICS POWER ENGLISH KT,12', 19)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205355F', 'RFB, MOTOR DRIVE UNIT, POWER', 1138)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205971', 'MOTOR DRIVE UNIT,ULTRALIGHT W/', 1200)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72200616', 'MDU, HAND CNTRL, PWRMAX ELITE I', 1225)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72200617', 'MOTOR DRIVE UNIT,NON-HAND CNT', 1199)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205396', 'FOOTSWITCH,EP-1,PEDAL STYLE,', 379)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205397', 'FOOTSWITCH,EP-1,ACLR,LO PRO', 475)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205399', 'FOOTSWITCH,EP-1,LOW PROFILE,W/', 469)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205357', 'MINI MOTOR DRIVE UNIT, EP-1,', 967)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72201500', 'DYONICS POWERMINI, WITH HAND CONTROLS', 1276)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '72201503', 'DYONICS POWERMINI, WITHOUT HAND CONTROLS', 1363)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '4323', 'STERI-TRAY,MDU', 181)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '4324', 'STERI-TRAY INSERT,MDU,ACCESSRY', 96)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('SHAVERS', '7205683', 'TRAY,X-LARGE MULTI-PURPOSE', 261)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('GENERATORS', '7210812F', 'RFB, VULCAN GENERATOR, CE MARK', 2796)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('GENERATORS', '7210873', 'KIT, VULCAN, DOMESTIC', 21)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('GENERATORS', '7209692', 'FOOTSWITCH,VULCAN', 181)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('GENERATORS', '72202149', 'DYONICS RF GENERATOR (includes Kit & Footswitch)', 2166)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('GENERATORS', '72202151', 'FOOT PEDAL, DYONICS RF GENERATOR', 162)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '72200208', 'KIT, U.S., DYONICS 25', 43)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '7211010F', 'CONTROL UNIT, DYONICS 25', 3139)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '72200891', 'WIRELESS REMOTE', 128)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '7211011', 'LEVELERT II,  FLUID LEVEL SENSOR', 210)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '72200747', 'CART, W. ISOL. TRANSFORMER', 1535)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '72201093', 'CANNULA, INTEL SPRING LOADED W/STORZ', 279)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '72200829', 'CANN, H.F.,DGNST, 6MM, RTBL, DBL VLV', 155)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '72200830', 'CANN, H.F.,DGNST, 6MM, RTBL, SGL VLV', 130)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '7210062', 'INTELIJET HERMES RDY PUMP,US', 0)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '3982', 'LEVELERT SENSOR,W/CORD', 0)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '4429', 'CABLE, COMMUNICATION 2 FT, INTELIJET', 0)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '7205392', 'REMOTE CONTROL,ASSY,INTL', 0)
INSERT INTO PlacedCapitalMaterial (CapitalGroup, MaterialID, MaterialDesc, ListPrice) VALUES ('PUMPS', '7205697F', 'MTO,RFB,C/U,ACCESS 15 PUMP,150 (INCLUDED REMOTE/LI', 1822)

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdminUsers]') AND type in (N'U'))
 	DROP TABLE AdminUsers 
GO

CREATE TABLE [dbo].AdminUsers(
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[lastname] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[firstname] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createDate] [datetime] NULL,
	[modifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[modifiedDate] [datetime] NULL
) ON [PRIMARY]

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Programs]') AND type in (N'U'))
 	DROP TABLE Programs 
GO

CREATE TABLE [dbo].[Programs](
	[programID] [int] IDENTITY(1,1) NOT NULL,
	[programName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[programStatus] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[overview] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[guidelines] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[programdescription] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[featuresBenefits] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[whenToUse] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[programFormName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createDate] [datetime] NULL,
	[modifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[modifiedDate] [datetime] NULL
) ON [PRIMARY]

GO

DECLARE @strOverView VARCHAR(MAx);
SET @strOverView = 'Provides your customers with the use of reusable medical devices in connection with sale of related disposables manufactured and sold by S&N.';

DECLARE @strDescription VARCHAR(MAx);
SET @strDescription = ' Equipment is placed at customer site and Customer purchases related disposables in exchange for the use of equipment.' + CHAR(13)+ CHAR(10)
SET @strDescription = @strDescription + ' Customer is not invoiced a rental or other charge for use of the equipment.' + CHAR(13)+ CHAR(10)
SET @strDescription = @strDescription + ' The fair market value of the annual use of the equipment and the provision of service represents an in-kind discount that reduces the invoice prices paid for the related disposables.' + CHAR(13)+ CHAR(10)
SET @strDescription = @strDescription + ' A reconciliation statement is sent to Customer documenting the additional discount provided and its application to the purchase prices of related disposables purchased during the contract year, with the allocation of the additional discount representing Customers cost of Equipment usage and service coverage.'

DECLARE @strGuidelines VARCHAR(MAx);
SET @strGuidelines = 'Customer requests shaver, pump and/or generator equipment to be placed on-site in connection with the purchase of related disposables in order to support the placement of equipment.'

DECLARE @strFeatures VARCHAR(MAx);
SET @strFeatures = ' No cash outlay upfront.' + CHAR(13)+ CHAR(10)
SET @strFeatures = @strFeatures + ' Includes replacement service exchanges.' + CHAR(13)+ CHAR(10)
SET @strFeatures = @strFeatures + ' Evergreen terms (perpetual agreement).'

DECLARE @strWhen VARCHAR(MAx);
SET @strWhen = ' Funds are not budgeted for the purchase.' + CHAR(13)+ CHAR(10)
SET @strWhen = @strWhen + ' Equipment ownership is not desired.' + CHAR(13)+ CHAR(10)
SET @strWhen = @strWhen + ' Customer is currently purchasing or has plans to purchase an adequate volume of related disposables.'

INSERT INTO Programs
	(programName, programStatus, overview, guidelines, programdescription, featuresBenefits, whenToUse, programFormName, createdBy, createDate, modifiedBy, modifiedDate)
VALUES
	('Placed Capital', 'Enabled', @strOverView, @strGuidelines, @strDescription, @strFeatures, @strWhen, 'PlacedCapitalRequestForm', 'colwelld', GetDate(), 'colwelld', GetDate())


/*  *********************************** Stored Procedures *******************************************/

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProgramByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetProgramByID
GO
-- =============================================
-- Author:		Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is the select statement that gets the program by the program ID.
-- =============================================
CREATE PROCEDURE dbo.sp_GetProgramByID
(
	@programID int
)

AS
	SELECT programID, programName, overview, guidelines, programdescription, featuresBenefits, whenToUse, 
	programStatus, programFormName, createdBy, createDate, modifiedBy, modifiedDate 
	FROM programs
	WHERE programID = @programID
	
	/* SET NOCOUNT ON */
	RETURN

GO

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPrograms]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetPrograms
GO
-- =============================================
-- Author:		Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is the select statement that gets all the programs.
-- =============================================
CREATE PROCEDURE dbo.sp_GetPrograms

AS

	SELECT programID, programName, overview, guidelines, programdescription, featuresBenefits, whenToUse, 
	programStatus, programFormName, createdBy, createDate, modifiedBy, modifiedDate 
	FROM programs
	ORDER BY programName

RETURN

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertProgram
GO
-- =============================================
-- Author:		Doug Colwell
-- Create date: 09-07-2012
-- Description:	This inserts a new program record.
-- =============================================
CREATE PROCEDURE dbo.sp_InsertProgram
(
	@programID int OUTPUT,
	@programName nvarchar(50),
	@programStatus nvarchar(20),
	@overview nvarchar(max),
	@guidelines nvarchar(max),
	@programdescription nvarchar(max),
	@featuresBenefits nvarchar(max),
	@whenToUse nvarchar(max),
	@programFormName nvarchar(50),	
	@modifiedBy	 nvarchar(50)
)

AS
	
INSERT INTO Programs
	(programName, programStatus, overview, guidelines, programdescription, featuresBenefits, whenToUse, programFormName, createdBy, createDate, modifiedBy, modifiedDate)
VALUES
	(@programName, @programStatus, @overview, @guidelines, @programdescription, @featuresBenefits, @whenToUse, @programFormName, @modifiedBy, GetDate(), @modifiedBy, GetDate())
	

SET @programID = (SELECT @@IDENTITY AS 'Identity') 

	/* SET NOCOUNT ON */
RETURN


GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_UpdateProgram
GO
-- =============================================
-- Author:		Doug Colwell
-- Create date: 09-07-2012
-- Description:	This updates a program record by the programID.
-- =============================================
CREATE PROCEDURE dbo.sp_UpdateProgram
(
	@programID int,
	@programName nvarchar(50),
	@programStatus nvarchar(20),
	@overview nvarchar(max),
	@guidelines nvarchar(max),
	@programdescription nvarchar(max),
	@featuresBenefits nvarchar(max),
	@whenToUse nvarchar(max),
	@programFormName nvarchar(50),	
	@modifiedBy	 nvarchar(50)	
)

AS
	
UPDATE Programs
	SET programName = @programName,
	    programStatus = @programStatus, 
	    overview = @overview, 
		guidelines = @guidelines, 
		programdescription = @programdescription, 
		featuresBenefits = @featuresBenefits, 
		whenToUse = @whenToUse,
		programFormName = @programFormName,
		modifiedBy = @modifiedBy,
		modifiedDate = GetDate()
WHERE programID = @programID	


RETURN
GO

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdminUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetAdminUsers
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get all the admin users.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetAdminUsers

AS

SELECT userID, username, lastName, firstName, createdBy, CreateDate, modifiedBy, modifiedDate
FROM AdminUsers a
ORDER BY lastName, firstName 

RETURN

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAdminUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertAdminUser
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to insert a new admin user.
-- =============================================
CREATE PROCEDURE [dbo].sp_InsertAdminUser
(
	@username nvarchar(50),
	@lastName nvarchar(50),
	@firstName nvarchar(50),
	@ModifiedBy nvarchar(50),
	@userID int OUTPUT
)
AS

INSERT INTO AdminUsers
 (username, lastName, firstName, createdBy, CreateDate, modifiedBy, modifiedDate)
VALUES
 (@username, @lastName, @firstName, @ModifiedBy, GetDate(), @ModifiedBy, GetDate())

SET @userID = (SELECT @@IDENTITY AS 'Identity')

RETURN
GO 
EXEC sp_InsertAdminUser 'colwelld', 'Colwell', 'Doug', 'colwelld', 0


GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteAdminUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_DeleteAdminUser
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 03/30/2010
-- Description:	This is going to delete the admin user based on the ID.
-- =============================================
CREATE PROCEDURE [dbo].sp_DeleteAdminUser
(
	@userID int
)
AS

DELETE AdminUsers WHERE userID = @userID

RETURN
GO

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdminUserByUserName]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetAdminUserByUserName
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get the admin user by the username.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetAdminUserByUserName
(
	@username nvarchar(50)
)
AS

SELECT userID, username, lastName, firstName, createdBy, CreateDate, modifiedBy, modifiedDate
FROM AdminUsers a
WHERE username = @username

RETURN

GO



GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllPlacedCapitalMaterial]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetAllPlacedCapitalMaterial
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get all PlacedCapitalMaterial records.
-- =============================================

CREATE PROCEDURE dbo.sp_GetAllPlacedCapitalMaterial
AS

	SELECT 
		a.CapitalGroup, MaterialID, MaterialDesc, ListPrice, null as AnnualRequiredPurchases, 
		null as AnnualValue, null as Qty, srvc.rate as ServiceUpchargeRate, marg.rate as MarginUpchargeRate,
		overhead.rate as OverheadChargeRate, comm.rate as CommercialMarginRate, Depr.rate as Depreciation
	 FROM PlacedCapitalMaterial a
	LEFT JOIN ProgramRates srvc ON (a.CapitalGroup = srvc.CapitalGroup AND srvc.RateDescription = 'Service Upcharge')
	LEFT JOIN ProgramRates marg ON (a.CapitalGroup = marg.CapitalGroup AND marg.RateDescription = 'Margin Upcharge')
	LEFT JOIN ProgramRates overhead ON (overhead.RateDescription = 'Overhead Upcharge')
	LEFT JOIN ProgramRates comm ON (comm.RateDescription = 'Commercially Reasonable Margin')
	LEFT JOIN ProgramRates Depr ON (a.CapitalGroup = marg.CapitalGroup AND Depr.RateDescription = 'Depreciation')

	RETURN
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPlacedCapitalMaterial]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertPlacedCapitalMaterial
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to insert a PlacedCapitalMaterial record.
-- =============================================
CREATE PROCEDURE [dbo].sp_InsertPlacedCapitalMaterial
(
	@CapitalGroup nvarchar(50),
	@MaterialID nvarchar(20),
	@MaterialDesc nvarchar(50),
	@ListPrice decimal(29,3),
	@modifiedBy	 nvarchar(50)
)
AS

INSERT INTO PlacedCapitalMaterial
	(CapitalGroup, MaterialID, MaterialDesc, ListPrice, createdBy, createDate, modifiedBy, modifiedDate)
VALUES
	(@CapitalGroup, @MaterialID, @MaterialDesc, @ListPrice, @modifiedBy, GetDate(), @modifiedBy, GetDate()) 


RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePlacedCapitalMaterial]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_UpdatePlacedCapitalMaterial
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to update a PlacedCapitalMaterial record.
-- =============================================
CREATE PROCEDURE [dbo].sp_UpdatePlacedCapitalMaterial
(
	@CapitalGroup nvarchar(50),
	@MaterialID nvarchar(20),
	@MaterialDesc nvarchar(50),
	@ListPrice decimal(29,3),
	@modifiedBy	 nvarchar(50)
)
AS

UPDATE PlacedCapitalMaterial
	SET CapitalGroup = @CapitalGroup, 
		MaterialDesc = @MaterialDesc, 
		ListPrice = @ListPrice, 
		modifiedBy = @modifiedBy, 
		modifiedDate = GetDate()
WHERE MaterialID = @MaterialID

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPlacedCapitalMaterialByMaterialID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetPlacedCapitalMaterialByMaterialID
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get a PlacedCapitalMaterial record BY the MaterialID.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetPlacedCapitalMaterialByMaterialID
(
	@MaterialID nvarchar(20)
)
AS

SELECT CapitalGroup, MaterialID, MaterialDesc, ListPrice, createdBy, createDate, modifiedBy, modifiedDate 
FROM PlacedCapitalMaterial
WHERE MaterialID = @MaterialID

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeletePlacedCapitalMaterialByMaterialID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_DeletePlacedCapitalMaterialByMaterialID
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to delete a PlacedCapitalMaterial record BY the MaterialID.
-- =============================================
CREATE PROCEDURE [dbo].sp_DeletePlacedCapitalMaterialByMaterialID
(
	@MaterialID nvarchar(20)
)
AS

DELETE PlacedCapitalMaterial WHERE MaterialID = @MaterialID

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPlacedCapitalMaterialRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertPlacedCapitalMaterialRequest
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to insert a PlacedCapitalMaterialRequest record.
-- =============================================
CREATE PROCEDURE [dbo].sp_InsertPlacedCapitalMaterialRequest
(
	@PlacedCapitalRequestID int, 
	@CapitalGroup nvarchar(50),
	@MaterialID nvarchar(20),
	@MaterialDesc nvarchar(50),
	@ListPrice decimal(29,3),
	@AnnualValue money,
	@ContractValue money,
	@AnnualRequiredPurchases money,
	@Qty int,
    @ServiceUpchargeRate float,
    @MarginUpchargeRate float,
    @OverheadChargeRate float,
    @CommercialMarginRate float,
	@Depreciation float,
	@PlacedCapitalMaterialRequestID int OUTPUT
)
AS

INSERT INTO PlacedCapitalMaterialRequest
	(PlacedCapitalRequestID, CapitalGroup, MaterialID, MaterialDesc, ListPrice, AnnualValue,
     ContractValue, AnnualRequiredPurchases, Qty, ServiceUpchargeRate, 
	 MarginUpchargeRate, OverheadChargeRate, CommercialMarginRate, Depreciation)
VALUES
	(@PlacedCapitalRequestID, @CapitalGroup, @MaterialID, @MaterialDesc, 
	@ListPrice, @AnnualValue, @ContractValue, @AnnualRequiredPurchases, @Qty, @ServiceUpchargeRate, 
	@MarginUpchargeRate, @OverheadChargeRate, @CommercialMarginRate, @Depreciation)

SET @PlacedCapitalMaterialRequestID = (SELECT @@IDENTITY AS 'Identity')



RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePlacedCapitalMaterialRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_UpdatePlacedCapitalMaterialRequest
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to update a PlacedCapitalMaterialRequest record.
-- =============================================
CREATE PROCEDURE [dbo].sp_UpdatePlacedCapitalMaterialRequest
(
	@PlacedCapitalRequestID int, 
	@CapitalGroup nvarchar(50),
	@MaterialID nvarchar(20),
	@MaterialDesc nvarchar(50),
	@ListPrice decimal(29,3),
	@AnnualValue money,
	@ContractValue money,
	@AnnualRequiredPurchases money,
	@Qty int,
    @ServiceUpchargeRate float,
    @MarginUpchargeRate float,
    @OverheadChargeRate float,
    @CommercialMarginRate float,
	@Depreciation float,
	@PlacedCapitalMaterialRequestID int
)
AS

UPDATE PlacedCapitalMaterialRequest
	SET PlacedCapitalRequestID = @PlacedCapitalRequestID, 
		CapitalGroup = @CapitalGroup, 
		MaterialID = @MaterialID, MaterialDesc = @MaterialDesc, 
		ListPrice = @ListPrice, AnnualValue = @AnnualValue,
		ContractValue = @ContractValue, 
		AnnualRequiredPurchases = @AnnualRequiredPurchases, 
		Qty = @Qty,
		ServiceUpchargeRate = @ServiceUpchargeRate,
		MarginUpchargeRate = @MarginUpchargeRate,
		OverheadChargeRate = @OverheadChargeRate,
		CommercialMarginRate = @CommercialMarginRate,
		Depreciation = @Depreciation
WHERE PlacedCapitalMaterialRequestID = @PlacedCapitalMaterialRequestID

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPlacedCapitalMaterialRequestsByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetPlacedCapitalMaterialRequestsByID
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get a PlacedCapitalMaterialRequests records by PlacedCapitalRequestID .
-- =============================================
CREATE PROCEDURE [dbo].sp_GetPlacedCapitalMaterialRequestsByID
(
	@PlacedCapitalRequestID int
)
AS
	SELECT PlacedCapitalMaterialRequestID, PlacedCapitalRequestID, CapitalGroup, MaterialID, MaterialDesc, ListPrice, AnnualValue,
     ContractValue, AnnualRequiredPurchases, Qty, ServiceUpchargeRate, 
	 MarginUpchargeRate, OverheadChargeRate, CommercialMarginRate, Depreciation
	FROM PlacedCapitalMaterialRequest
	WHERE PlacedCapitalRequestID = @PlacedCapitalRequestID

	RETURN


GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPlacedCapitalRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertPlacedCapitalRequest
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to insert a PlacedCapitalRequest record.
-- =============================================
CREATE PROCEDURE [dbo].sp_InsertPlacedCapitalRequest
(
	@PlacedCapitalRequestID int OUTPUT,
	@ProgramID int,
	@SoldToCustomerID nvarchar(10),
	@SoldToCustomerName nvarchar(75),
	@SoldToStreet nvarchar(75),
	@SoldToCity nvarchar(75),
	@SoldToStateCode nvarchar(75),
	@SoldToPostalCode nvarchar(20),
	@RvpName nvarchar(75),
	@DistrictName nvarchar(75),
	@SalesRepName nvarchar(75),
	@SalesRepID nvarchar(10),
	@GpoDesc nvarchar(75),
	@ShipToCustomerID nvarchar(10),
	@ShipToCustomerName nvarchar(75),
	@ShipToStreet nvarchar(75),
	@ShipToCity nvarchar(75),
	@ShipToStateCode nvarchar(20),
	@ShipToPostalCode nvarchar(20),
	@EmailFax nvarchar(50),
	@FacilityContact nvarchar(50),
	@ContractTerm nvarchar(50),
	@FormStamp nvarchar(50),
	@ModifiedBy nvarchar(50),
	@SubmittedByName nvarchar(50)
)
AS
		INSERT INTO PlacedCapitalRequest
			(ProgramID, SoldToCustomerID, SoldToCustomerName, SoldToStreet, SoldToCity, SoldToStateCode, 
			 SoldToPostalCode, RvpName, DistrictName, SalesRepName, SalesRepID, GpoDesc, ShipToCustomerID, ShipToCustomerName, 
			 ShipToStreet, ShipToCity, ShipToStateCode, ShipToPostalCode, 
			 EmailFax, FacilityContact, ContractTerm, FormStamp, CreatedBy, CreateDate, ModifiedBy, ModifiedDate, SubmittedByName)
		VALUES
			(@ProgramID, @SoldToCustomerID, @SoldToCustomerName, @SoldToStreet, @SoldToCity, 
			 @SoldToStateCode, @SoldToPostalCode, @RvpName, @DistrictName, @SalesRepName, @SalesRepID, @GpoDesc, @ShipToCustomerID, @ShipToCustomerName, @ShipToStreet, @ShipToCity, 
			 @ShipToStateCode, @ShipToPostalCode, @EmailFax, @FacilityContact, @ContractTerm, @FormStamp,
			 @ModifiedBy, GetDate(), @ModifiedBy, GetDate(), @SubmittedByName)

		SET @PlacedCapitalRequestID = (SELECT @@IDENTITY AS 'Identity')

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePlacedCapitalRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_UpdatePlacedCapitalRequest
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to Update a PlacedCapitalRequest record.
-- =============================================
CREATE PROCEDURE [dbo].sp_UpdatePlacedCapitalRequest
(
	@PlacedCapitalRequestID int,
	@ProgramID int,
	@SoldToCustomerID nvarchar(10),
	@SoldToCustomerName nvarchar(75),
	@SoldToStreet nvarchar(75),
	@SoldToCity nvarchar(75),
	@SoldToStateCode nvarchar(75),
	@SoldToPostalCode nvarchar(20),
	@RvpName nvarchar(75),
	@DistrictName nvarchar(75),
	@SalesRepName nvarchar(75),
	@SalesRepID nvarchar(10),
	@GpoDesc nvarchar(75),
	@ShipToCustomerID nvarchar(10),
	@ShipToCustomerName nvarchar(75),
	@ShipToStreet nvarchar(75),
	@ShipToCity nvarchar(75),
	@ShipToStateCode nvarchar(20),
	@ShipToPostalCode nvarchar(20),
	@EmailFax nvarchar(50),
	@FacilityContact nvarchar(50),
	@ContractTerm nvarchar(50),
	@FormStamp nvarchar(50),
	@ModifiedBy nvarchar(50),
	@SubmittedByName nvarchar(50)
)
AS

UPDATE PlacedCapitalRequest
	SET ProgramID = @ProgramID,
		SoldToCustomerID = @SoldToCustomerID,
		SoldToCustomerName = @SoldToCustomerName,
		SoldToStreet = @SoldToStreet,
		SoldToCity = @SoldToCity,
		SoldToStateCode = @SoldToStateCode,
		SoldToPostalCode = @SoldToPostalCode,
		RvpName = @RvpName,
		DistrictName = @DistrictName,
		SalesRepName = @SalesRepName,
		SalesRepID = @SalesRepID,
		GpoDesc = @GpoDesc,
		ShipToCustomerID = @ShipToCustomerID,
		ShipToCustomerName = @ShipToCustomerName,
		ShipToStreet = @ShipToStreet,
		ShipToCity = @ShipToCity,
		ShipToStateCode = @ShipToStateCode,
		ShipToPostalCode = @ShipToPostalCode,
		EmailFax = @EmailFax,
		FacilityContact = @FacilityContact,
		ContractTerm = @ContractTerm,
		FormStamp = @FormStamp,
		ModifiedBy = @ModifiedBy,
		SubmittedByName = @SubmittedByName,
		ModifiedDate = GetDate()
	WHERE PlacedCapitalRequestID = @PlacedCapitalRequestID

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPlacedCapitalRequestByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetPlacedCapitalRequestByID
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get the PlacedCapitalRequest by the PlacedCapitalRequestID.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetPlacedCapitalRequestByID
(
	@PlacedCapitalRequestID int
)
AS

	SELECT PlacedCapitalRequestID, ProgramID, SoldToCustomerID, SoldToCustomerName, SoldToStreet, SoldToCity, SoldToStateCode, 
	SoldToPostalCode, RvpName, DistrictName, SalesRepName, SalesRepID, GpoDesc, ShipToCustomerID, ShipToCustomerName, 
	ShipToStreet, ShipToCity, ShipToStateCode, ShipToPostalCode, EmailFax, FacilityContact, ContractTerm, FormStamp,
	CreatedBy, CreateDate, ModifiedBy, ModifiedDate, SubmittedByName
	FROM PlacedCapitalRequest
	WHERE PlacedCapitalRequestID = @PlacedCapitalRequestID

RETURN

GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPlacedCapitalRequestByFormStamp]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetPlacedCapitalRequestByFormStamp
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get the PlacedCapitalRequest by the PlacedCapitalRequestID.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetPlacedCapitalRequestByFormStamp
(
	@FormStamp nvarchar(50)
)
AS

	SELECT PlacedCapitalRequestID, ProgramID, SoldToCustomerID, SoldToCustomerName, SoldToStreet, SoldToCity, SoldToStateCode, 
	SoldToPostalCode, RvpName, DistrictName, SalesRepName, SalesRepID, GpoDesc, ShipToCustomerID, ShipToCustomerName, 
	ShipToStreet, ShipToCity, ShipToStateCode, ShipToPostalCode, EmailFax, FacilityContact, ContractTerm, FormStamp,
	CreatedBy, CreateDate, ModifiedBy, ModifiedDate, SubmittedByName
	FROM PlacedCapitalRequest
	WHERE FormStamp = @FormStamp

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAdditionalCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertAdditionalCustomers
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to insert a AdditionalCustomers record.
-- =============================================
CREATE PROCEDURE [dbo].sp_InsertAdditionalCustomers
(
	@PlacedCapitalRequestID int,
	@CustomerID nvarchar(20),
	@CustomerName nvarchar(75),
	@Street nvarchar(75),
	@City nvarchar(75),
	@StateCode nvarchar(75),
	@PostalCode nvarchar(20),
	@GpoDesc nvarchar(75)
)
AS

INSERT INTO AdditionalCustomers
	(PlacedCapitalRequestID, CustomerID, CustomerName, Street, City, StateCode, PostalCode, GpoDesc)
VALUES
	(@PlacedCapitalRequestID, @CustomerID, @CustomerName, @Street, @City, @StateCode, @PostalCode, @GpoDesc)

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateAdditionalCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_UpdateAdditionalCustomers
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to Update a AdditionalCustomers record.
-- =============================================
CREATE PROCEDURE [dbo].sp_UpdateAdditionalCustomers
(
	@PlacedCapitalRequestID int,
	@CustomerID nvarchar(20),
	@CustomerName nvarchar(75),
	@Street nvarchar(75),
	@City nvarchar(75),
	@StateCode nvarchar(75),
	@PostalCode nvarchar(20),
	@GpoDesc nvarchar(75)
)
AS

UPDATE AdditionalCustomers
	SET CustomerName = @CustomerName,
		Street = @Street,
		City = @City,
		StateCode = @StateCode,
		PostalCode = @PostalCode,
		GpoDesc = @GpoDesc
	WHERE PlacedCapitalRequestID = @PlacedCapitalRequestID AND CustomerID = @CustomerID

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalCustomersByPlacedCapitalRequestID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetAdditionalCustomersByPlacedCapitalRequestID
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get a AdditionalCustomers records by PlacedCapitalRequestID.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetAdditionalCustomersByPlacedCapitalRequestID
(
	@PlacedCapitalRequestID int
)
AS

SELECT PlacedCapitalRequestID, CustomerID, CustomerName, Street, City, StateCode, PostalCode, GpoDesc 
FROM AdditionalCustomers
WHERE PlacedCapitalRequestID = @PlacedCapitalRequestID

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProgramRates]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetProgramRates
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get all the program rates.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetProgramRates
AS

SELECT ProgramRateID, a.ProgramID, b.programName, CapitalGroup, RateDescription, Rate, RateText, a.CreatedBy, a.CreateDate, a.ModifiedBy, a.ModifiedDate 
FROM ProgramRates a
LEFT JOIN programs b ON a.ProgramID=b.ProgramID
ORDER BY b.programName, CapitalGroup, RateDescription

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProgramRatesByProgramID]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetProgramRatesByProgramID
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get all the program rates by programID.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetProgramRatesByProgramID
(
	@ProgramID int
)
AS

SELECT ProgramRateID, a.ProgramID, b.programName, CapitalGroup, RateDescription, Rate, RateText, a.CreatedBy, a.CreateDate, a.ModifiedBy, a.ModifiedDate 
FROM ProgramRates a
LEFT JOIN programs b ON a.ProgramID=b.ProgramID
WHERE a.ProgramID = @ProgramID
ORDER BY CapitalGroup, RateDescription

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertProgramRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_InsertProgramRate
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to insert a program rate record.
-- =============================================
CREATE PROCEDURE [dbo].sp_InsertProgramRate
(
	@ProgramRateID int OUTPUT,
	@ProgramID int,
	@CapitalGroup nvarchar(25),
	@RateDescription nvarchar(50),
	@Rate float,
	@RateText nvarchar(50),
	@ModifiedBy nvarchar(50)
)
AS

INSERT INTO ProgramRates (ProgramID, CapitalGroup, RateDescription, Rate, RateText, CreatedBy, CreateDate, ModifiedBy, ModifiedDate) 
	VALUES (@ProgramID, @CapitalGroup, @RateDescription, @Rate, @RateText, @ModifiedBy, GetDate(), @ModifiedBy, GetDate())

SET @ProgramRateID = (SELECT @@IDENTITY AS 'Identity')

RETURN

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateProgramRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_UpdateProgramRate
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to update a program rate record.
-- =============================================
CREATE PROCEDURE [dbo].sp_UpdateProgramRate
(
	@ProgramRateID int,
	@ProgramID int,
	@CapitalGroup nvarchar(25),
	@RateDescription nvarchar(50),
	@Rate float,
	@RateText nvarchar(50),
	@ModifiedBy nvarchar(50)
)
AS

UPDATE ProgramRates
	SET ProgramID = @ProgramID, 
		CapitalGroup = @CapitalGroup, 
		RateDescription = @RateDescription, 
		Rate = @Rate,
		RateText = @RateText, 
		ModifiedBy = @ModifiedBy,
		ModifiedDate = GetDate() 
	WHERE ProgramRateID = @ProgramRateID

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteProgramRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_DeleteProgramRate
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to delete a program rate record.
-- =============================================
CREATE PROCEDURE [dbo].sp_DeleteProgramRate
(
	@ProgramRateID int
)
AS

DELETE ProgramRates WHERE ProgramRateID = @ProgramRateID

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetQueryByQueryName]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetQueryByQueryName
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to a queries record by the queryname.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetQueryByQueryName
(
	@QueryName nvarchar(100)
)
AS

SELECT QueryID, QueryName, Query 
FROM Queries 
WHERE QueryName = @QueryName

RETURN

GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRateDescriptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetRateDescriptions
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get all the Rate Descriptions.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetRateDescriptions
AS

SELECT RateDescriptionID, RateDescription 
FROM RateDescriptions
ORDER BY RateDescription

RETURN

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProgramFormPages]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.sp_GetProgramFormPages
GO
-- =============================================
-- Author:	Doug Colwell
-- Create date: 09-07-2012
-- Description:	This is going to get all the Program Form Pages.
-- =============================================
CREATE PROCEDURE [dbo].sp_GetProgramFormPages
AS

SELECT ProgramFormPageNameID, ProgramFormPageName, ProgramFormPageTitle 
FROM ProgramFormPages
ORDER BY ProgramFormPageTitle

RETURN

GO
