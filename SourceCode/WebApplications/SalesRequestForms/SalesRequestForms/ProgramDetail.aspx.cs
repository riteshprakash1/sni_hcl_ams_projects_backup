﻿// --------------------------------------------------------------
// <copyright file="ProgramDetail.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Program Detail page.</summary>   
    public partial class ProgramDetail : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["programId"] != null)
                {
                    Debug.WriteLine("programId: " + Request.QueryString["programId"]);
                    this.LoadProgramData(Request.QueryString["programId"]);
                }
            } //// end IsPostBack
        }

        protected void LoadProgramData(string strProgramID)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            Debug.WriteLine("LoadProgramData");
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@programID", strProgramID));

            this.lblProgramName.Text = "Program not found";
            this.btnRequestForm.Visible = false;

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetProgramByID", myParamters);
            if (dt.Rows.Count > 0)
            {
                DataRow r = dt.Rows[0];
                // if program is enabled or user is admin load the data
                if (r["programstatus"].ToString().ToLower() == "enabled" || (AppUsersEntity.IsAdmin(myUsers)))
                {
                    this.lblFeaturesBenefits.Text = r["featuresBenefits"].ToString().Replace("\n", "<br/>");
                    this.lblGuidelines.Text = r["guidelines"].ToString().Replace("\n", "<br/>");
                    this.lblOverview.Text = r["overview"].ToString().Replace("\n", "<br/>");
                    this.lblProgramDescription.Text = r["programdescription"].ToString().Replace("\n", "<br/>");
                    this.lblProgramName.Text = r["programName"].ToString().Replace("\n", "<br/>");
                    this.lblWhenToUse.Text = r["whenToUse"].ToString().Replace("\n", "<br/>");

                    this.btnRequestForm.CommandName = r["programFolder"].ToString() + "/" + r["programFormName"].ToString();
                    this.btnRequestForm.Visible = true;
                }
            }
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }


        protected void btnRequestForm_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.btnRequestForm.CommandName + ".aspx", true);
        } //// end btnRequestForm_Click

    }
}