﻿// -------------------------------------------------------------
// <copyright file="PriceRequestFormMaterials.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PricingRequest.Classes;
    
    /// <summary>This is the Price Request Form Materials page.</summary> 
    public partial class PriceRequestFormMaterials : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("PriceRequestFormMaterials Page_Load");
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["PricingRequestID"] == null)
                    //Response.Redirect("PriceRequestFormMaterials.aspx?PricingRequestID=1", false);
                    Response.Redirect("PriceRequestFormCustomer.aspx", false);
                else
                {
                    this.lblPriceRequestID.Text = Request.QueryString["PricingRequestID"];
                    int intPricingRequestID = Convert.ToInt32(this.lblPriceRequestID.Text);

                    PricingRequestEntity pc = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);

                    this.LoadPriceRequestDataToPage(pc);
                    this.LoadHierarchyCheckBoxLists();

                    List<PricingRequestMaterialEntity> materials = PricingRequestMaterialEntity.GetPricingRequestMaterialByPricingRequestID(intPricingRequestID);

                    this.BindMaterials(materials);

                }

                EnterButton.TieButton(txtSetRequestedPrice, btnRequestedPrice);
            }

            this.SetControls();
        }//// end Page_Load

        protected void LoadPriceRequestDataToPage(PricingRequestEntity pc)
        {
            CustomerMasterHorizontalSummary1.CustomerName = pc.SoldToCustomerName;
            CustomerMasterHorizontalSummary1.CustomerID = pc.SoldToCustomerID;
            CustomerMasterHorizontalSummary1.CustomerAddress = pc.SoldToStreet;
            CustomerMasterHorizontalSummary1.CustomerCity = pc.SoldToCity;
            CustomerMasterHorizontalSummary1.CustomerState = pc.SoldToStateCode;
            CustomerMasterHorizontalSummary1.CustomerZip = pc.SoldToPostalCode;
            CustomerMasterHorizontalSummary1.GPO = pc.GpoDesc;
            CustomerMasterHorizontalSummary1.CustGroup1 = pc.CustGroup1;
            CustomerMasterHorizontalSummary1.CustGroup1ID = pc.CustGroup1ID;

            if (pc.CustGroup1ID != string.Empty)
                CustomerMasterHorizontalSummary1.CustGroup1Filler = " - ";
            else
                CustomerMasterHorizontalSummary1.CustGroup1Filler = string.Empty;

            CustomerMasterHorizontalSummary1.DeName = pc.DistrictName;
            CustomerMasterHorizontalSummary1.RepName = pc.SalesRepName;
            CustomerMasterHorizontalSummary1.RvpName = pc.RvpName;

            this.lblPriceRequestID.Text = pc.PricingRequestID.ToString();
            this.lblPriceRequestStatus.Text = pc.PricingRequestStatus;
        }

        protected DataTable GetCustomerMaterialTable()
        {
            DataTable dtMaterials = null;
            string strMaterialListSessionName = CustomerMasterHorizontalSummary1.CustomerID + "MaterialList";
            if (Session[strMaterialListSessionName] == null)
            {
                dtMaterials = PricingRequestMaterialEntity.GetPriceRequestByCustomerTable(CustomerMasterHorizontalSummary1.CustomerID, CustomerMasterHorizontalSummary1.CustomerState);
                Session[strMaterialListSessionName] = dtMaterials;
            }
            else
            {
                dtMaterials = Session[strMaterialListSessionName] as DataTable;
            }

            return dtMaterials;
        }

        /// <summary>This populates tyhe items in the Level 4, 5, 6  CheckBoxLists control .</summary>
        protected void LoadHierarchyCheckBoxLists()
        {
            List<PricingRequestMaterialEntity> level4List = PricingRequestMaterialEntity.GetPriceRequestLevel4();
            List<PricingRequestMaterialEntity> level5List = PricingRequestMaterialEntity.GetPriceRequestLevel5();
            List<PricingRequestMaterialEntity> level6List = PricingRequestMaterialEntity.GetPriceRequestLevel6();

            this.ckbListLevel4.Items.Clear();
            this.ckbListLevel5.Items.Clear();
            this.ckbListLevel6.Items.Clear();

            this.ckbListLevel4.DataSource = level4List;
            this.ckbListLevel4.DataBind();

            this.ckbListLevel5.DataSource = level5List;
            this.ckbListLevel5.DataBind();

            this.ckbListLevel6.DataSource = level6List;
            this.ckbListLevel6.DataBind();

        }

        protected void btnMaterialList_Click(object sender, EventArgs e)
        {
            string strMaterialListSessionName = CustomerMasterHorizontalSummary1.CustomerID.PadLeft(10, '0') + "MaterialList";
            //   List<PricingRequestMaterialEntity> materials = Session[strMaterialListSessionName] as List<PricingRequestMaterialEntity>;

            string strLevel4 = this.GetCheckBoxSelectedList(this.ckbListLevel4);
            if (strLevel4 != string.Empty)
                strLevel4 = "Level_4_Desc IN (" + strLevel4 + ") ";

            string strLevel5 = this.GetCheckBoxSelectedList(this.ckbListLevel5);
            if (strLevel5 != string.Empty)
                strLevel5 = "Level_5_Desc IN (" + strLevel5 + ") ";

            string strLevel6 = this.GetCheckBoxSelectedList(this.ckbListLevel6);
            if (strLevel6 != string.Empty)
                strLevel6 = "Level_6_Desc IN (" + strLevel6 + ") ";

            string strCondition = string.Empty;

            if (strLevel4 != string.Empty)
                strCondition = strLevel4;

            if (strLevel5 != string.Empty)
            {
                if (strCondition != string.Empty)
                    strCondition += "OR " + strLevel5;
                else
                    strCondition = strLevel5;
            }

            if (strLevel6 != string.Empty)
            {
                if (strCondition != string.Empty)
                    strCondition += "OR " + strLevel6;
                else
                    strCondition = strLevel6;
            }

            Debug.WriteLine("strCondition: " + strCondition);


            if (strCondition != string.Empty)
            {
                DataTable dtCustMaterials = this.GetCustomerMaterialTable();
                DataRow[] rows = dtCustMaterials.Select(strCondition);

                List<PricingRequestMaterialEntity> materials = this.LoadPricingRequestMaterialEntityListFromGridView();

                foreach (DataRow r in rows)
                {
                    PricingRequestMaterialEntity pc = PricingRequestMaterialEntity.GetEntityFromDataRow(r);

                    int materialFindIndex = materials.FindIndex(item => item.Material_Id == pc.Material_Id);
                    //Debug.WriteLine("Material Find Index: " + materialFindIndex.ToString());

                    if(materialFindIndex < 0)
                        materials.Add(pc);
                }

                this.BindMaterials(materials);
            }
            else
            {
                Page.Validate("GetMaterialList");
                this.vldMaterialList.IsValid = false;
            }

        }

        protected void BindMaterials(List<PricingRequestMaterialEntity> lstMaterials)
        {
            if (lstMaterials.Count == 0)
                lstMaterials.Add(new PricingRequestMaterialEntity());

            this.gvMaterial.DataSource = lstMaterials;
            this.gvMaterial.DataBind();

            PricingRequestMaterialEntity.HighLightGridViewColumns(this.gvMaterial);
            PricingRequestMaterialEntity.HideGridViewColumns(this.gvMaterial, Request);

            this.SetControls();
        }

        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            TextBox txtMaterialID = (TextBox)this.gvMaterial.FooterRow.FindControl("txtMaterialID");
            CustomValidator vldMaterialIDNotFound = (CustomValidator)this.gvMaterial.FooterRow.FindControl("vldMaterialIDNotFound");
            CustomValidator vldMaterialAlreadyInList = (CustomValidator)this.gvMaterial.FooterRow.FindControl("vldMaterialAlreadyInList");
            
            Debug.WriteLine("GV_Add");
            Page.Validate("AddMaterial");

            List<PricingRequestMaterialEntity> materials = this.LoadPricingRequestMaterialEntityListFromGridView();

            int materialFindIndex = materials.FindIndex(item => item.Material_Id == txtMaterialID.Text.Trim());
            //Debug.WriteLine("Material Find Index: " + materialFindIndex.ToString());

            DataTable dtCustMaterials = this.GetCustomerMaterialTable();
            string strCondition = "Material_Id = '" + txtMaterialID.Text.Trim() + "'";
            Debug.WriteLine("strCondition : " + strCondition);
            DataRow[] rows = dtCustMaterials.Select(strCondition);

            vldMaterialIDNotFound.IsValid = Convert.ToBoolean(rows.Length > 0);
            vldMaterialAlreadyInList.IsValid = Convert.ToBoolean(materialFindIndex < 0);

            if (Page.IsValid)
            {
                PricingRequestMaterialEntity pc = PricingRequestMaterialEntity.GetEntityFromDataRow(rows[0]);
                materials.Add(pc);
                this.BindMaterials(materials);
            }


        }  ////end GV_Add

        /// <summary>Handles the RowDataBound event of the gvMaterial GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtRequestedPrice = (TextBox)e.Row.FindControl("txtRequestedPrice");
                TextBox txtCompetitorPrice = (TextBox)e.Row.FindControl("txtCompetitorPrice");
                TextBox txtExpectedVolume = (TextBox)e.Row.FindControl("txtExpectedVolume");
                Label lblMaterialID = (Label)e.Row.FindControl("lblMaterialID");

                if (lblMaterialID.Text == string.Empty)
                    e.Row.Visible = false;

                txtRequestedPrice.Attributes.Add("onkeydown", "return (event.keyCode!=13);");
                txtRequestedPrice.Attributes.Add("onFocus", "clearTextOnFocus(this)");

                txtCompetitorPrice.Attributes.Add("onkeydown", "return (event.keyCode!=13);");
                txtExpectedVolume.Attributes.Add("onkeydown", "return (event.keyCode!=13);");
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtMaterialID = (TextBox)e.Row.FindControl("txtMaterialID");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtMaterialID, btnAdd);
            }
        } // end gv_RowDataBound

        protected void SetControls()
        {
            this.btnRemoveItems.Visible = Convert.ToBoolean(this.gvMaterial.Rows.Count > 0);
            this.btnUpdateMaterial.Visible = Convert.ToBoolean(this.gvMaterial.Rows.Count > 0);
            this.btnStep3.Visible = Convert.ToBoolean(this.gvMaterial.Rows.Count > 0);
        }


        protected string GetCheckBoxSelectedList(CheckBoxList chkList)
        {
            string strSelectedItems = string.Empty;
            int intSelectedItems = 0;
            foreach (ListItem i in chkList.Items)
            {
                if (i.Selected)
                {
                    if (strSelectedItems == string.Empty)
                        strSelectedItems += "'" + i.Value + "'";
                    else
                        strSelectedItems += ", '" + i.Value + "'";


                    intSelectedItems++;
                }
            }

            return strSelectedItems;
        }

        protected void btnRemoveItems_Click(object sender, EventArgs e)
        {
            DataTable dtCustMaterials = this.GetCustomerMaterialTable();
            List<PricingRequestMaterialEntity> originalMaterial = this.LoadPricingRequestMaterialEntityListFromGridView();
            List<PricingRequestMaterialEntity> materials = new List<PricingRequestMaterialEntity>();

            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                CheckBox ckbRemoveMaterial = (CheckBox)this.gvMaterial.Rows[r.RowIndex].FindControl("ckbRemoveMaterial");
                if (!ckbRemoveMaterial.Checked)
                {
                    string strMaterialID = this.gvMaterial.DataKeys[r.RowIndex]["Material_ID"].ToString();

                    PricingRequestMaterialEntity m = originalMaterial.Find(delegate(PricingRequestMaterialEntity mat) { return mat.Material_Id == strMaterialID; });
                    materials.Add(m);

                    //DataRow updateRow = dtCustMaterials.Rows.Find(strMaterialID);
                    //materials.Add(PricingRequestMaterialEntity.GetEntityFromDataRow(updateRow));
                }
            }

            this.BindMaterials(materials);
        }//// end

        protected void ValidationFailed()
        {
            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                string strMaterialID = this.gvMaterial.DataKeys[r.RowIndex]["Material_ID"].ToString();
                RequiredFieldValidator vldRequiredRequestedPrice = (RequiredFieldValidator)this.gvMaterial.Rows[r.RowIndex].FindControl("vldRequiredRequestedPrice");
                CompareValidator vldCompareRequestedPrice = (CompareValidator)this.gvMaterial.Rows[r.RowIndex].FindControl("vldCompareRequestedPrice");
                CompareValidator vldCompareCompetitorPrice = (CompareValidator)this.gvMaterial.Rows[r.RowIndex].FindControl("vldCompareCompetitorPrice");
                RequiredFieldValidator vldRequiredCompetitorPrice = (RequiredFieldValidator)this.gvMaterial.Rows[r.RowIndex].FindControl("vldRequiredCompetitorPrice");
                CompareValidator vldCompareExpectedVolume = (CompareValidator)this.gvMaterial.Rows[r.RowIndex].FindControl("vldCompareExpectedVolume");
                RequiredFieldValidator vldRequiredExpectedVolume = (RequiredFieldValidator)this.gvMaterial.Rows[r.RowIndex].FindControl("vldRequiredExpectedVolume");

                int colIndex = PricingRequestMaterialEntity.GetColumnIndexByHeaderText(this.gvMaterial, "Request Amt($)");
                if (!vldRequiredRequestedPrice.IsValid || !vldCompareRequestedPrice.IsValid)
                {
                    r.Cells[colIndex].BackColor = System.Drawing.Color.Red;

                    if (!vldRequiredRequestedPrice.IsValid)
                        vldRequiredRequestedPrice.ErrorMessage = strMaterialID + " - Requested $ is required.";

                    if (!vldCompareRequestedPrice.IsValid)
                        vldCompareRequestedPrice.ErrorMessage = strMaterialID + " - Requested $ must be a number greater than zero.";
                }
                else
                    r.Cells[colIndex].BackColor = System.Drawing.Color.FromArgb(255, 255, 0);

                colIndex = PricingRequestMaterialEntity.GetColumnIndexByHeaderText(this.gvMaterial, "Competitor Price");
                if (!vldCompareCompetitorPrice.IsValid || !vldRequiredCompetitorPrice.IsValid)
                {
                    r.Cells[colIndex].BackColor = System.Drawing.Color.Red;

                    if (!vldRequiredCompetitorPrice.IsValid)
                        vldRequiredCompetitorPrice.ErrorMessage = strMaterialID + " - Competitor Price is required.";

                    if (!vldCompareCompetitorPrice.IsValid)
                        vldCompareCompetitorPrice.ErrorMessage = strMaterialID + " - Competitor Price must be a number greater than or equal to zero.";
                }
                else
                    r.Cells[colIndex].BackColor = System.Drawing.Color.FromArgb(255, 255, 0);

                colIndex = PricingRequestMaterialEntity.GetColumnIndexByHeaderText(this.gvMaterial, "Expected $ volume (per item)");
                if (!vldCompareExpectedVolume.IsValid || !vldRequiredExpectedVolume.IsValid)
                {
                    r.Cells[colIndex].BackColor = System.Drawing.Color.Red;

                    if (!vldCompareExpectedVolume.IsValid)
                        vldCompareExpectedVolume.ErrorMessage = strMaterialID + " - Expected Volume must be a number greater than or equal to zero.";

                    if (!vldRequiredExpectedVolume.IsValid)
                        vldRequiredExpectedVolume.ErrorMessage = strMaterialID + " - Expected Volume is required.";
                }
                else
                    r.Cells[colIndex].BackColor = System.Drawing.Color.FromArgb(255, 255, 0);
            }

        }

        /// <summary>Handles the UpdateMaterial button click event. It loads the data from the GridView and 
        ///         recalculates based on the user entries and binds the data to the GridView.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnUpdateMaterial_Click(object sender, EventArgs e)
        {
            Page.Validate("UpdateMaterial");

            if (Page.IsValid)
            {
                List<PricingRequestMaterialEntity> updatedMaterials = this.LoadPricingRequestMaterialEntityListFromGridView();
                this.BindMaterials(updatedMaterials);
            }
            else
            {
                this.ValidationFailed();
            }
        } //// end

        /// <summary>This loads the data from the GridView into PricingRequestMaterialEntity List.</summary>
        /// <returns>The PricingRequestMaterialEntity List</returns>
        protected List<PricingRequestMaterialEntity> LoadPricingRequestMaterialEntityListFromGridView()
        {
            DataTable dtCustMaterials = this.GetCustomerMaterialTable();
            List<PricingRequestMaterialEntity> materials = new List<PricingRequestMaterialEntity>();

            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                Label lblMaterialID = (Label)this.gvMaterial.Rows[r.RowIndex].FindControl("lblMaterialID");

                if (lblMaterialID.Text != string.Empty)
                {

                    TextBox txtRequestedPrice = (TextBox)this.gvMaterial.Rows[r.RowIndex].FindControl("txtRequestedPrice");
                    TextBox txtCompetitorPrice = (TextBox)this.gvMaterial.Rows[r.RowIndex].FindControl("txtCompetitorPrice");
                    TextBox txtExpectedVolume = (TextBox)this.gvMaterial.Rows[r.RowIndex].FindControl("txtExpectedVolume");

                    string strMaterialID = this.gvMaterial.DataKeys[r.RowIndex]["Material_ID"].ToString();
                    DataRow updateRow = dtCustMaterials.Rows.Find(strMaterialID);

                    PricingRequestMaterialEntity pc = PricingRequestMaterialEntity.GetEntityFromDataRow(updateRow);
                    try
                    {
                        pc.Requested_Price = Convert.ToDouble(txtRequestedPrice.Text);
                    }
                    catch
                    {
                        pc.Requested_Price = 0.00;
                    }

                    try
                    {
                        pc.Competitor_Price = Convert.ToDouble(txtCompetitorPrice.Text);
                    }
                    catch
                    {
                        pc.Competitor_Price = 0.00;
                    }

                    try
                    {
                        pc.Expected_Volume = Convert.ToDouble(txtExpectedVolume.Text);
                    }
                    catch
                    {
                        pc.Expected_Volume = 0.00;
                    }

                    //Requested % off list = (2013 List $ - Requested $) / 2013 List $
                    pc.Requested_Discount = (pc.List_Price - pc.Requested_Price) / pc.List_Price;

                    //Requested GM% = (Requested $ - COGs) / Requested $
                    pc.Requested_GM = (pc.Requested_Price - pc.COG) / pc.Requested_Price;

                    // '=If (Requested $<RVP Price,"Deal Desk",(If(Requested $<DOS/DE Price,"RVP","Within Rep/Mgr Limits"))
                    if (pc.Requested_Price < pc.RVP_Price)
                    {
                        pc.Approval_Required_From = "Deal Desk";
                    }
                    else
                    {
                        if(pc.Requested_Price < pc.DE_Price)
                            pc.Approval_Required_From = "RVP";
                        else
                            pc.Approval_Required_From = "Within Rep/Mgr Limits";
                    }

                    materials.Add(pc);

                } ////lblMaterialID.Text != string.Empty

            } //// foreach

            return materials;
        }

        /// <summary>Saves the data from the material list and redirects to the strPageName</summary>
        /// <param name="strPageName">The page to redirect to</param>
        protected void SaveAndRedirect(string strPageName)
        {
            PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(Convert.ToInt32(this.lblPriceRequestID.Text));
            if ((pr.PricingRequestStatus != "Submitted") && pr.PricingRequestStatus != "Expired")
            {
                Page.Validate("UpdateMaterial");

                if (Page.IsValid)
                {
                    List<PricingRequestMaterialEntity> updatedMaterials = this.LoadPricingRequestMaterialEntityListFromGridView();
                    int intInsertedMaterials = PricingRequestMaterialEntity.SavePricingRequestMaterialsInDatabase(Convert.ToInt32(this.lblPriceRequestID.Text), updatedMaterials);

                    Response.Redirect(strPageName + "?PricingRequestID=" + this.lblPriceRequestID.Text, false);
                }
                else
                {
                    this.ValidationFailed();
                }
            }
            else
            {
                this.lblMessage.Text = "The Price Request Status is " + pr.PricingRequestStatus + " which means the Price Request Form cannot be changed."; 
            }
        
        }

        /// <summary>Handles the Step1 button click event.  It saves the data from the material list and redirects to the PriceRequestFormCustomer page</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnStep1_Click(object sender, EventArgs e)
        {
            this.SaveAndRedirect("PriceRequestFormCustomer.aspx");
        }

        /// <summary>Handles the Step3 button click event.  It saves the data from the material list and redirects to the PriceRequestFormReview page</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnStep3_Click(object sender, EventArgs e)
        {
            this.SaveAndRedirect("PriceRequestFormReview.aspx");
        }

        protected void btnRequestedPrice_Click(object sender, EventArgs e)
        {
            Page.Validate("SetRequestedPrice");

            int checkedItemsCount = 0;
            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                CheckBox ckbRemoveMaterial = (CheckBox)this.gvMaterial.Rows[r.RowIndex].FindControl("ckbRemoveMaterial");
                if (ckbRemoveMaterial.Checked)
                    checkedItemsCount++;
            }

            this.vldRequestedPriceUpdateItems.IsValid = Convert.ToBoolean(checkedItemsCount > 0);

            if (Page.IsValid)
            {
                foreach (GridViewRow r in this.gvMaterial.Rows)
                {
                    CheckBox ckbRemoveMaterial = (CheckBox)this.gvMaterial.Rows[r.RowIndex].FindControl("ckbRemoveMaterial");
                    if (ckbRemoveMaterial.Checked)
                    {
                        TextBox txtRequestedPrice = (TextBox)this.gvMaterial.Rows[r.RowIndex].FindControl("txtRequestedPrice");
                        txtRequestedPrice.Text = this.txtSetRequestedPrice.Text;
                    }
                }

                List<PricingRequestMaterialEntity> updatedMaterials = this.LoadPricingRequestMaterialEntityListFromGridView();
                this.BindMaterials(updatedMaterials);

                this.txtSetRequestedPrice.Text = string.Empty;
            }

        }

        protected void ckbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ckbSelectAll = (CheckBox)this.gvMaterial.HeaderRow.FindControl("ckbSelectAll");
            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                CheckBox ckbRemoveMaterial = (CheckBox)this.gvMaterial.Rows[r.RowIndex].FindControl("ckbRemoveMaterial");
                ckbRemoveMaterial.Checked = ckbSelectAll.Checked;
            }

        }


    }//// end class
}