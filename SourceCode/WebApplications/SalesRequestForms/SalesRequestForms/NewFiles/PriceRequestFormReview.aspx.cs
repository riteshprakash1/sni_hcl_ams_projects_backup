﻿// -------------------------------------------------------------
// <copyright file="PriceRequestFormReview.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PricingRequest.Classes;

    /// <summary>This is the Price Request Form Review page.</summary> 
    public partial class PriceRequestFormReview : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("PriceRequestFormReview.aspx Page_Load");
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["PricingRequestID"] == null)
//                    Response.Redirect("PriceRequestFormCustomer.aspx", false);
                    Response.Redirect("PriceRequestFormReview.aspx?PricingRequestID=23", false);
                else
                {
                    this.hdnPricingRequestID.Value = Request.QueryString["PricingRequestID"];
                    int intPricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);

                    PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);
                    PriceRequestHeaderView1.PricingRequestDataLoad(pr);
                    PriceRequestHeaderView1.HeaderTitle = "PRICE REQUEST FORM REVIEW";

                    this.SetControls(pr);
                    PriceRequestMaterialView1.BindMaterials(pr.PricingRequestID);
                }
            }
        }//// end Page_Load

        /// <summary>Sets the Visible attribute of some web controls</summary>
        public void SetControls(PricingRequestEntity pr)
        {
            bool isVisible = Convert.ToBoolean((pr.PricingRequestStatus != "Submitted") && (pr.PricingRequestStatus != "Expired"));
            this.btnStep2.Visible = isVisible;
            this.btnSubmit.Visible = isVisible;

            PriceRequestHeaderView1.SetHeaderControls(pr);
        }

        /// <summary>Handles the btnStep2 button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnStep2_Click(object sender, EventArgs e)
        {
            Response.Redirect("PriceRequestFormMaterials.aspx?PricingRequestID=" + this.hdnPricingRequestID.Value, false);
        }

        /// <summary>Handles the Submit button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ADUserEntity myUser = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
            int intPricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);

            PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);
            if ((pr.PricingRequestStatus != "Submitted") && pr.PricingRequestStatus != "Expired")
            {
                pr.PricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);
                pr.SubmittedByName = myUser.DisplayName;
                pr.ModifiedBy = myUser.UserName;
                pr.PricingRequestStatus = "Submitted";

                int intRecordsUpdated = PricingRequestEntity.UpdatePricingRequestStatus(pr);

                if (intRecordsUpdated == 1)
                {
                    this.lblMessage.Text = "The Price Request Form was successfully submitted.";
                }
                else
                {
                    this.lblMessage.Text = "The Price Request Form was NOT successfully submitted.";
                    Debug.WriteLine("UPDATED HAD A PROBLEM");
                }
            }
            else
            {
                this.lblMessage.Text = "The Price Request Status is " + pr.PricingRequestStatus + " which means the Price Request Form cannot be changed.";
            }

            pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);
            PriceRequestHeaderView1.PricingRequestDataLoad(pr);
            this.SetControls(pr);

            ADUserEntity user = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
            string strSubject = System.Configuration.ConfigurationManager.AppSettings["PricingRequestEmailSubject"] + pr.PricingRequestID.ToString();
            strSubject += " for " + pr.SoldToCustomerName + " (" + pr.SoldToCustomerID + ")";

            string strFromEmail = System.Configuration.ConfigurationManager.AppSettings["PricingRequestFromEmail"];
            string strToEmail = System.Configuration.ConfigurationManager.AppSettings["PricingRequestToEmail"];

            Utility.SendEmail(strToEmail, strFromEmail, user.Email, strSubject, this.GetEmailBody(pr), true);
        }

        /// <summary>This writes a string that with the data for the body of the email.</summary>
        /// <param name="pc">The PricingRequestEntity object.</param>
        /// <returns>The email body string</returns>
        protected string GetEmailBody(PricingRequestEntity pr)
        {
            string strEmailBody = string.Empty;
            string strSiteURL = System.Configuration.ConfigurationManager.AppSettings["SiteURL"];
            string strRequestURL = strSiteURL + "PricingRequest/PriceRequestView.aspx?PricingRequestID=" + this.hdnPricingRequestID.Value;
            Debug.WriteLine("strSiteURL: " + strSiteURL + "; strRequestURL: " + strRequestURL);
            strEmailBody += PriceRequestHeaderView1.GetHeaderEmailBody(pr);

            string tablewidth = "980px";
            strEmailBody += "<table width=\"" + tablewidth + "\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td>&nbsp;</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"font-family:Arial, Helvetica, sans-serif; font-size:10pt\">To view the entire request, please click this link <a href=\"" + strRequestURL + "\">Pricing Request</a></td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"color: #FF7300;font-family:Arial, Helvetica, sans-serif; font-size:10pt\">Note - You must be connected to the Smith &amp; Network to view the request online.</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            
            return strEmailBody;
        }

    }//// end class
}