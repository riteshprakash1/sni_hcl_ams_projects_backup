﻿<%@ Page Title="Price Request View" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="PriceRequestView.aspx.cs" Inherits="SalesRequestForms.PricingRequest.PriceRequestView" %>
<%@ Register src="UserControls/PriceRequestHeaderView.ascx" tagname="PriceRequestHeaderView" tagprefix="uc1" %>
<%@ Register src="UserControls/PriceRequestMaterialView.ascx" tagname="PriceRequestMaterialView" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PriceRequestHeaderView ID="PriceRequestHeaderView1" runat="server" />
    
    <table>
        <tr>
            <td><asp:HiddenField ID="hdnPricingRequestID" runat="server" /></td>
        </tr>
        <tr>
            <td>&nbsp; &nbsp; &nbsp; &nbsp;    
                    <asp:Button ID="btnExcelExport" runat="server" Text="Export to Excel" Width="140px" 
                        CssClass="buttonsSubmitBorder" onclick="btnExcelExport_Click"/>
                    <br /><asp:Label ID="lblMessage" runat="server"></asp:Label></td>
        </tr>
    </table>
    <uc2:PriceRequestMaterialView ID="PriceRequestMaterialView1" runat="server" />
</asp:Content>

