﻿// --------------------------------------------------------------
// <copyright file="Site.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Master page for the Web Application.</summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        /// <summary>Handles the OnInit event of the Page control.  This fixes Menu rendering in Chrome and Safari.</summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {    // For Chrome and Safari    
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                if (Request.Browser.Adapters.Count > 0)
                {
                    Request.Browser.Adapters.Clear();
                    Response.Redirect(Page.Request.Url.AbsoluteUri);
                }
            }
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl ("~/Scripts/ShowHint.js")); 

            if (!Page.IsPostBack)
            {
 //               Response.Write("User Agent: " + Request.Headers["User-Agent"].ToLower());
                this.NavMenu.Items.Add(new MenuItem("Home", "Default.aspx"));

                List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

                if (AppUsersEntity.IsAdmin(myUsers))
                    this.NavMenu.Items.Add(new MenuItem("Administration", "", "", "~/Admin/Default.aspx"));

                //try
                //{
                //    List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));
                //    MenuItem adminMenu = new MenuItem("Administration", "", "", "~/Admin/UserList.aspx");
                //    adminMenu.ChildItems.Add(new MenuItem("Programs", "", "", "~/Admin/Programs.aspx"));
                //    adminMenu.ChildItems.Add(new MenuItem("User List", "", "", "~/Admin/UserList.aspx"));

                //    // Placed Capital Admin Pages
                //    if (AppUsersEntity.IsProgramAdmin(myUsers, 1))
                //    {
                //        adminMenu.ChildItems.Add(new MenuItem("Program Rates & Variables", "", "", "~/Admin/ProgramRates.aspx"));
                //        adminMenu.ChildItems.Add(new MenuItem("Placed Capital Materials", "", "", "~/Admin/PlacedCapitalMaterials.aspx"));
                //    }

                //    //Pricing Request Admin Page
                //    if (AppUsersEntity.IsProgramAdmin(myUsers, 2))
                //    {
                //        adminMenu.ChildItems.Add(new MenuItem("Pricing Eligible Materials", "", "", "~/Admin/PricingEligibleMaterials.aspx"));
                //        adminMenu.ChildItems.Add(new MenuItem("Import Pricing Material Matrix", "", "", "~/Admin/ImportPricingMaterials.aspx"));
                //    }

                //    if (AppUsersEntity.IsAdmin(myUsers))
                //    {
                //        this.NavMenu.Items.Add(adminMenu);
                //    }

                //}
                //catch (Exception ex)
                //{
                //    LogException.HandleException(ex, Request);
                //}
            }
        }

        /// <summary>Called when MenuItem is clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.MenuEventArgs"/> instance containing the event data.</param>
        public void OnClick(Object sender, MenuEventArgs e)
        {
            Debug.WriteLine("In OnClick");
            //MessageLabel.Text = "You selected " + e.Item.Text + ".";
            Debug.WriteLine("Value: " + e.Item.Value + "; Text: " + e.Item.Text);
            e.Item.Selected = true;
            string strPage = "~/" + e.Item.Value.Replace("Admin", "Admin/");
            Response.Redirect(strPage, true);
        }

        /// <summary>Handles the Click event of the btnAppName control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAppName_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx", true);
        }
    } //// end class
}