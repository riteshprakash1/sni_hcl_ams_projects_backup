﻿<%@ Page Title="Price Request Form Review" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="PriceRequestFormReview.aspx.cs" Inherits="SalesRequestForms.PricingRequest.PriceRequestFormReview" %>
<%@ Register src="UserControls/PriceRequestMaterialView.ascx" tagname="PriceRequestMaterialView" tagprefix="uc2" %>
<%@ Register src="UserControls/PriceRequestHeaderView.ascx" tagname="PriceRequestHeaderView" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PriceRequestHeaderView ID="PriceRequestHeaderView1" runat="server" />    
    <div style="width:980px; text-align:left">
        <table width="100%">
             <tr>
                <td colspan="2">&nbsp;
                    <asp:HiddenField ID="hdnPricingRequestID" runat="server" />
                </td>
            </tr>
           <tr>
                <td style="text-align:center; width:50%">
                    <asp:Button ID="btnSubmit" runat="server" CausesValidation="False" CssClass="buttonsSubmitBorder" 
                        Text="Submit Pricing Request" Width="180px" onclick="btnSubmit_Click" />
                </td>
                <td style="text-align:center; width:50%">
                    <asp:Button ID="btnStep2" runat="server" CausesValidation="False" CssClass="buttonsSubmitBorder" 
                        Text="Return to Step 2 - Material" Width="180px" onclick="btnStep2_Click"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;"><asp:Label ID="lblMessage" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label></td>
            </tr>
        
             <tr>
                <td colspan="2" style="text-align:center"><asp:ValidationSummary ID="ValidationSummary3" runat="server" ForeColor="#FF7300" Font-Size="10pt"/></td>
            </tr> 
        </table>
    </div>

    <uc2:PriceRequestMaterialView ID="PriceRequestMaterialView1" runat="server" />
</asp:Content>
