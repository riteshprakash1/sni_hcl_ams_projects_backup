﻿// -------------------------------------------------------------
// <copyright file="PriceRequestFormReview.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using System.IO;
    using System.Security.Principal;
    using SalesRequestForms.PricingRequest.Classes;
//    using Microsoft.Office.Interop.Excel;

    /// <summary>This is the Price Request View page.</summary> 
    public partial class PriceRequestView : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("PriceRequestView.aspx Page_Load");
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["PricingRequestID"] == null)
                //    Response.Redirect("PriceRequestFormCustomer.aspx", false);
                    Response.Redirect("PriceRequestView.aspx?PricingRequestID=23", false);
                else
                {
                    this.hdnPricingRequestID.Value = Request.QueryString["PricingRequestID"];

                    int intPricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);
 
                    PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);
                    PriceRequestHeaderView1.PricingRequestDataLoad(pr);
                    PriceRequestHeaderView1.HeaderTitle = "PRICE REQUEST VIEW";

                    PriceRequestMaterialView1.BindMaterials(pr.PricingRequestID);
                }

                int intUserRoleID = AppUsersEntity.GetAppUserRoleIDByProgramIDByUsername(2, Utility.CurrentUser(Request));
                bool isApple = Convert.ToBoolean(Request.UserAgent.IndexOf("AppleWebKit") > 0);
                bool showExport =  Convert.ToBoolean(((intUserRoleID == 1) || (intUserRoleID == 3) || (intUserRoleID == 4) || (intUserRoleID == 5)) && !isApple);

                this.btnExcelExport.Visible = showExport;

            }
        }//// end Page_Load

        /// <summary>Handles the btnExcelExport click event.</summary>
        /// <param name="strExportFolder">The fully qualified export directory address.</param>
        protected void RemoveOldExportFiles(string strExportFolder)
        {
            try
            {
                DateTime yesterday = DateTime.Now.AddDays(-1);
                Debug.WriteLine("yesterday: " + yesterday.ToString());

                DirectoryInfo exportDirectory = new DirectoryInfo(strExportFolder);
                FileInfo[] exportFiles = exportDirectory.GetFiles();

                foreach (FileInfo f in exportFiles)
                {
                    if (f.CreationTime < yesterday)
                        f.Delete();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("RemoveOldExportFiles Error: " + ex.Message);
                LogException.HandleException(ex, Request);
            }          

        }

        /// <summary>Handles the btnExcelExport click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnExcelExport_Click(object sender, EventArgs e)
        {
            int intPricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);
            string strFileName = "PricingRequest_" + intPricingRequestID.ToString() + "_" + DateTime.Now.ToBinary().ToString() + ".xlsx";
            string strExportFolder = ConfigurationManager.AppSettings["ExportFolder"];
            string strQualifiedFileName = strExportFolder + strFileName;

            Utility.DeleteDirectoryFilesAsync(strExportFolder, Request);

            string strUserStoreProcedure = string.Empty;
            string strTemplateFile = string.Empty;

            Debug.WriteLine("Role ID: " + AppUsersEntity.GetAppUserRoleIDByProgramIDByUsername(2, Utility.CurrentUser(Request)).ToString());

            switch (AppUsersEntity.GetAppUserRoleIDByProgramIDByUsername(2, Utility.CurrentUser(Request)))
            {
                 case 1:
                 case 3:
                 case 4:
                     strUserStoreProcedure = "sp_GetDealDeskPricingRequestMaterialForExcelByPricingRequestID";
                     strTemplateFile = "PricingRequestDealDeskTemplate.xlsx";
                      break;
                 case 5:
                      strUserStoreProcedure = "sp_GetCustSrvcPricingRequestMaterialForExcelByPricingRequestID";
                      strTemplateFile = "PricingRequestCustSrvcTemplate.xlsx";
                     break;
                 default:
                      strUserStoreProcedure = string.Empty;
                      break;
             }

            if (strUserStoreProcedure != string.Empty)
            {
                PriceRequestExcelExport.BuildFile(intPricingRequestID, strQualifiedFileName, strUserStoreProcedure, strTemplateFile);

                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
                Response.TransmitFile(strQualifiedFileName);
                Response.End();
            }


     //       this.StreamTheDownload(strQualifiedFileName);

        }

        protected void StreamTheDownload(string strFileName)
        {
            // Write the file to the Response
            const int bufferLength = 10000;
            byte[] buffer = new Byte[bufferLength];
            int length = 0;
            Stream download = null;

            try
            {
                download = new FileStream(strFileName, FileMode.Open, FileAccess.Read);
                do
                {
                    if (Response.IsClientConnected)
                    {
                        length = download.Read(buffer, 0, bufferLength);
                        Response.OutputStream.Write(buffer, 0, length);
                        buffer = new Byte[bufferLength];
                    }
                    else
                    {
                        length = -1;
                    }
                }
                while (length > 0);
                
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Export Error: " + ex.Message);
                LogException.HandleException(ex, Request);
            }
            finally
            {
                if (download != null)
                    download.Close();

                if (File.Exists(strFileName))
                    File.Delete(strFileName);

            }
        }

    }//// end class
}