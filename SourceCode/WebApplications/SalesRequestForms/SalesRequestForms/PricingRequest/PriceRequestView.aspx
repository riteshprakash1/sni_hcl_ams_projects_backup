﻿<%@ Page Title="Price Request View" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="PriceRequestView.aspx.cs" Inherits="SalesRequestForms.PricingRequest.PriceRequestView" %>
<%@ Register src="UserControls/PriceRequestHeaderView.ascx" tagname="PriceRequestHeaderView" tagprefix="uc1" %>
<%@ Register src="UserControls/PriceRequestMaterialView.ascx" tagname="PriceRequestMaterialView" tagprefix="uc2" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PriceRequestHeaderView ID="PriceRequestHeaderView1" runat="server" />
    
    <table>
        <tr>
            <td><asp:HiddenField ID="hdnPricingRequestID" runat="server" /><asp:HiddenField ID="hdnProgramID" Value="2" runat="server" /></td>
        </tr>
        <tr>
            <td>&nbsp; &nbsp; &nbsp; &nbsp;    
                    <asp:Button ID="btnExcelExport" runat="server" Text="Export to Excel" Width="140px" 
                        CssClass="buttonsSubmitBorder" onclick="btnExcelExport_Click"/>
            &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btnUpdateToDate" runat="server" Text="Update Sales Data" Width="140px" 
                        CssClass="buttonsSubmitBorder" onclick="btnUpdateToDate_Click"/>
            &nbsp; &nbsp; &nbsp; &nbsp;
                    <uc3:waitbutton id="btnCopy" Width="140px" CssClass="buttonsSubmitBorder" 
                    runat="server" Text="Copy Request"
				        WaitText="Wait" CausesValidation="false" onclick="btnCopy_Click"></uc3:waitbutton>                                 
                    <br /><asp:Label ID="lblMessage" runat="server"></asp:Label></td>
        </tr>
    </table>
    <uc2:PriceRequestMaterialView ID="PriceRequestMaterialView1" runat="server" />
</asp:Content>

