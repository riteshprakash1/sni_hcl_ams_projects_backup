﻿<%@ Page Title="Price Request Form Customer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PriceRequestFormCustomer.aspx.cs" Inherits="SalesRequestForms.PricingRequest.PriceRequestFormCustomer" %>
<%@ Register src="../UserControls/CustomerMaster.ascx" tagname="CustomerMaster" tagprefix="uc1" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="PageTable">
    <table width="100%">
        <tr>
            <td style="text-align:center;font-size: medium;"><strong>PRICE REQUEST FORM</strong></td>
        </tr>
    </table>
    <table width="100%" style="background-color: #FFFF00">
        <tr>
            <td style="font-weight:bold; font-size:10pt"><asp:Label ID="lblCompletionTime" runat="server" Text="ONCE REQUEST IS SUBMITTED, PLEASE ALLOW UP TO 3-5 DAYS FOR COMPLETION."></asp:Label></td>
        </tr>
    </table>
    <table  width="100%">
        <tr>
            <td><asp:HyperLink ID="lnkInstructionsLabel" runat="server" CssClass="hintanchor" NavigateUrl="#">Form Instructions</asp:HyperLink></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td style="width:50%; vertical-align:top">
            <uc1:CustomerMaster ID="CustomerMaster1" runat="server" />
                <asp:HiddenField ID="hdnProgramID" Value="2" runat="server" />
                <asp:HiddenField ID="hdnSubmittedBy" runat="server" /><asp:HiddenField ID="hdnFormStamp" runat="server" /><asp:HiddenField ID="hdnPricingRequestID" runat="server" />
                    <table>
                       <tr>
                            <td style="width:31%">Agreement Term: *</td>
                            <td><asp:TextBox ID="txtAgreementTerm" MaxLength="50" runat="server" Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredAgreementTerm" ControlToValidate="txtAgreementTerm" runat="server" ForeColor="#FF7300" 
                                    ErrorMessage="Requested Agreement Term is required.">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>        
                        <tr>
                            <td>Competitor: </td>
                            <td><asp:TextBox ID="txtCompetitor" MaxLength="50" runat="server" Width="250px"></asp:TextBox></td>
                        </tr>
                    </table>           
                </td>
            <td style="width:50%; vertical-align:top">
                <table width="100%">
                    <tr>
                        <td valign="top" style="width:160; font-weight:bold">Additional Acct# 's that are part of this deal:</td>
                    </tr>
                    <tr>
                         <td>
                             <asp:GridView ID="gvAdditionalAcct" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GV_AdditionalAcctRowDataBound"
                                 OnRowDeleting="GV_AdditionalAcctDelete"  ShowFooter="true" DataKeyNames="CustomerID, PricingCustomerID, PricingCustomerTypeID" 
                                 CellPadding="2" GridLines="Both" OnRowCancelingEdit="GV_AdditionalAcctAdd">
                                <RowStyle HorizontalAlign="left" />
                                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                                <Columns>
                                      <asp:TemplateField HeaderStyle-Width="12%" ItemStyle-HorizontalAlign="Left" FooterStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							                    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								                ></asp:Button>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							                    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>                                
					                    </FooterTemplate>
                                    </asp:TemplateField> 
                                   <asp:TemplateField HeaderStyle-Width="24%" HeaderText="Customer ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerID" Font-Size="8pt" runat="server"  Text='<%# Eval("CustomerID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtCustomerID" MaxLength="10" width="80px" Font-Size="8pt" runat="server"></asp:TextBox>
                                            <asp:CustomValidator ID="vldCustomerIDExistsSubmitted" runat="server" ForeColor="#FF7300" 
                                                ErrorMessage="The Account Number was not found.">*</asp:CustomValidator>
                                            <asp:CustomValidator ID="vldCustomerIDExists" runat="server" ForeColor="#FF7300" ValidationGroup="AdditionalAccount" 
                                                ErrorMessage="The Account Number was not found.">*</asp:CustomValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>                            
                                   <asp:TemplateField HeaderStyle-Width="64%" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerName" Font-Size="8pt" runat="server"  Text='<%# Eval("CustomerName")%>'></asp:Label>
                                        </ItemTemplate>
                                   </asp:TemplateField>                            
                           
                               </Columns>
                            </asp:GridView>                
                            <asp:Label ID="lblAddCustomerMsg" Font-Size="8pt" runat="server"  Text="Click 'Add' after each entry if needing multiple selections."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>Business Opportunities, Justification, and Strategic Rationale for Price: *</td>
            <td><asp:TextBox ID="txtJustification" TextMode="MultiLine" runat="server" Width="430px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="vldRequiredJustification" ControlToValidate="txtJustification" runat="server" ForeColor="#FF7300" 
                    ErrorMessage="Business Opportunities, Justification, and Strategic Rationale for Price is required.">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>What is estimated 12 month incremental revenue for products on request? *</td>
            <td><asp:TextBox ID="txtIncrementalRevenue" runat="server" MaxLength="50" Width="247px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="vldRequired12MonthIncrementalRevenue" ControlToValidate="txtIncrementalRevenue" runat="server" 
                 ForeColor="#FF7300" ErrorMessage="Estimated 12 month incremental revenue is a required.">*</asp:RequiredFieldValidator>
                <asp:RangeValidator ID="vldRangeIncrementalRevenue" runat="server" 
                    ControlToValidate="txtIncrementalRevenue" MinimumValue="0" MaximumValue="99999999999"
                   ForeColor="#FF7300" 
                    ErrorMessage="Estimated 12 month incremental revenue must be a number greater than or equeal to zero. No commas. (e.g. 12345.50)" 
                    Type="Double">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>What is the probability of incremental revenue for  products on request (%)? *</td>
            <td><asp:DropDownList ID="ddlRevenueProbability" runat="server" Width="250px">
                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                <asp:ListItem Text="0-25%: Not very likely" Value="0-25%: Not very likely"></asp:ListItem>
                <asp:ListItem Text="26-50%: Somewhat likely" Value="26-50%: Somewhat likely"></asp:ListItem>
                <asp:ListItem Text="51-75%: Likely" Value="51-75%: Likely"></asp:ListItem>
                <asp:ListItem Text="76-90%: Very Likely" Value="76-90%: Very Likely"></asp:ListItem>
                <asp:ListItem Text="91-95%: Extremely Likely" Value="91-95%: Extremely Likely"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="vldRequiredRevenueProbability" ControlToValidate="ddlRevenueProbability" runat="server" 
                     ForeColor="#FF7300" ErrorMessage="Probability of incremental revenue is a required.">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table width="100%">
         <tr>
            <td colspan="2">* - indicates required fields</td>
        </tr>
       <tr>
            <td colspan="2" style="text-align:center">
                <asp:Button ID="btnSubmit" runat="server" CausesValidation="False" CssClass="buttonsSubmitBorder" 
                    Text="Move to Step 2 - Materials" onclick="btnSubmit_Click" Width="180px" />
            </td>
        </tr>
        <tr>
            <td><asp:Label ID="lblMessage" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label></td>
        </tr>
        
         <tr>
            <td><asp:ValidationSummary ID="ValidationSummary3" runat="server" ForeColor="#FF7300" Font-Size="10pt"/>
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ValidationGroup="AdditionalAccount" ForeColor="#FF7300" Font-Size="10pt"/>
            </td>
        </tr> 
    </table>
    </div>
</asp:Content>
