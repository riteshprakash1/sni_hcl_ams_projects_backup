﻿// -------------------------------------------------------------
// <copyright file="PriceRequestSearch.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using System.IO;
    using System.Security.Principal;
    using System.Linq;
    using SalesRequestForms.PricingRequest.Classes;

    /// <summary>This is the Price Request Search page.</summary> 
    public partial class PriceRequestSearch : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["PriceSearchList"] = null;
                
                EnterButton.TieButton(this.txtCustomerID, this.btnSearch);
                EnterButton.TieButton(this.txtCustomerName, this.btnSearch);
                EnterButton.TieButton(this.txtMaterialID, this.btnSearch);
                EnterButton.TieButton(this.txtPriceRequestID, this.btnSearch);
                EnterButton.TieButton(this.txtSalesDistrictName, this.btnSearch);
                EnterButton.TieButton(this.txtSalesRepID, this.btnSearch);
                EnterButton.TieButton(this.txtSalesRepName, this.btnSearch);
                EnterButton.TieButton(this.txtDateFrom, this.btnSearch);
                EnterButton.TieButton(this.txtDateTo, this.btnSearch);
                EnterButton.TieButton(this.txtSalesDistrictID, this.btnSearch);
            }
        }

        /// <summary>Handles the Search button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Session["PriceSearchList"] = null;
            List<PricingRequestEntity> results = GetSearchResults();
            this.BindDT(results);
        }

        /// <summary>This creates the dynamic query for the search and returns the result set</summary>
        /// <returns>the PricingRequestEntity list</returns>
        protected List<PricingRequestEntity> GetSearchResults()
        {

            string strUserName = Utility.CurrentUser(Request);
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(strUserName);
            int intProgramID = Convert.ToInt32(this.hdnProgramID.Value);
            int intRoleID = AppUsersEntity.GetAppUserRoleIDByProgramIDByUsername(intProgramID, strUserName);


       //     if (!AppUsersEntity.IsProgramAdmin(myUsers, intProgramID))
            
            if (Session["PriceSearchList"] == null)
            {
                string strQuery = "SELECT PricingRequestID, ProgramID, SoldToCustomerID, SoldToCustomerName, SoldToStreet, SoldToCity, SoldToStateCode, SoldToPostalCode, RvpName, ";
                strQuery += "DistrictID, DistrictName, SalesRepName, SalesRepID, GpoDesc, CustGroup1, CustGroup1ID, AgreementTerms, BusinessJustification, Competitor, IncrementalRevenueEstimate, ";
                strQuery += "IncrementalRevenueProbability, FormStamp, CreatedBy, CreateDate, ModifiedBy, SubmittedByName, ";
                strQuery += "replace(convert(varchar(10),ModifiedDate, 111), '/', '-') as ModifiedDate, PricingRequestStatus, ";
                strQuery += "Request12RollingRev, RequestToDateRev, convert(varchar(10),ToDateUpdatedOn, 101) as ToDateUpdatedOn ";
                strQuery += "FROM PricingRequest pr ";

                if (this.txtPriceRequestID.Text != string.Empty)
                {
                    strQuery += "WHERE PricingRequestID = " + this.txtPriceRequestID.Text;
                }
                else
                {
                    string strWhere = string.Empty;

                    if (this.txtCustomerID.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " SoldToCustomerID = '" + this.txtCustomerID.Text.PadLeft(10, '0') + "' ");

                    if (this.txtCustomerName.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " SoldToCustomerName LIKE '%" + this.txtCustomerName.Text.Replace("'", "''") + "%' ");

                    //if (this.txtSalesDistrictID.Text != string.Empty)
                    //    this.GetConnect(strWhere, " SoldToCustomerID = '" + this.txtSalesDistrictID.Text + "' ");

                    if (this.txtSalesRepID.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " SalesRepID = '" + this.txtSalesRepID.Text.PadLeft(10,'0') + "' ");

                    if (this.txtSalesRepName.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " SalesRepName LIKE '%" + this.txtSalesRepName.Text.Replace("'", "''") + "%' ");

                    if (this.txtSalesDistrictName.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " DistrictName LIKE '%" + this.txtSalesDistrictName.Text.Replace("'", "''") + "%' ");

                    if (this.txtSalesDistrictID.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " DistrictID = '" + this.txtSalesDistrictID.Text + "' ");

                    if (this.ddlStatus.SelectedIndex > 0)
                        strWhere = this.GetConnect(strWhere, " PricingRequestStatus = '" + this.ddlStatus.SelectedValue + "' ");

                    if (this.txtMaterialID.Text != string.Empty)
                    {
                        string strExists = " EXISTS (SELECT * FROM PricingRequestMaterials prm WHERE prm.PricingRequestID=pr.PricingRequestID ";
                        strExists += "AND prm.Material_Id LIKE '%" + this.txtMaterialID.Text + "%') ";
                        strWhere = this.GetConnect(strWhere, strExists);
                    }

                    if (this.txtDateFrom.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " ModifiedDate >= '" + this.txtDateFrom.Text + "' ");

                    if (this.txtDateTo.Text != string.Empty)
                        strWhere = this.GetConnect(strWhere, " ModifiedDate <= '" + this.txtDateTo.Text + "' ");

                    Debug.WriteLine("strWhere: " + strWhere);

                    if(intRoleID == 0) // If SalesRep
                        strWhere = this.GetConnect(strWhere, " CreatedBy = '" + strUserName + "' ");

                    //DE can search on all requests they are assigned to or that they authored
                    if (intRoleID == 6)
                    {
                        List<UserTerritoryEntity> territories = UserTerritoryEntity.GetUserTerritoriesByUserName(strUserName);
                        string strTerritoryWhere = "(CreatedBy = '" + strUserName + "' ";
                        foreach (UserTerritoryEntity t in territories)
                        {
                            strTerritoryWhere += "OR DistrictID = '" + t.Territory + "' ";
                        }

                        strWhere = this.GetConnect(strWhere, strTerritoryWhere + ") ");
                    }

                    if (strWhere == string.Empty)
                        strWhere += "WHERE PricingRequestID = 0";

                    strQuery += strWhere;
                }

                Debug.WriteLine("strQuery: " + strQuery);

                List<PricingRequestEntity> results = PricingRequestEntity.GetPricingRequestSearch(strQuery);
                Session["PriceSearchList"] = results;
                return results;
            }
            else
            {
                return Session["PriceSearchList"] as List<PricingRequestEntity>;
            }
        }


        /// <summary>Adds the Clause to the Where String and handles the Where/And condition</summary>
        /// <param name="strWhere">The existing Where string.</param>
        /// <param name="strClause">The clause to add to the Where string.</param>
        /// <returns>the updated where string</returns>
        protected string GetConnect(string strWhere, string strClause)
        {
            Debug.WriteLine("GetConnect strWhere: " + strWhere);
        
            
            if(strWhere == string.Empty)
                strWhere = " WHERE " + strClause;
            else
                strWhere += " AND " + strClause;

            return strWhere;
        }

        /// <summary>Handles the PageIndexChanging event of the grid view control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.gvSearch.DataSource = this.GetSearchResults();
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.DataBind();
        }

        /// <summary>Binds the PriceRequest table to the gvSearch GridView.</summary>
        /// <param name="results">The PricingRequestEntity List containing the Search Results.</param>
        protected void BindDT(List<PricingRequestEntity> results)
        {
            //List<PricingRequestEntity> results = AppUsersEntity.GetAppUserByProgramID(Convert.ToInt32(this.hdnProgramID.Value));

            this.gvSearch.DataSource = results;
            this.gvSearch.DataBind();
        }


        protected List<PricingRequestEntity> SortGridView(List<PricingRequestEntity> priceRequestList)
        {
            Debug.WriteLine("hdnSortField: " + this.hdnSortField.Value);
            Func<PricingRequestEntity, object> prop = null;
            switch (this.hdnSortField.Value.ToLower())
            {
                case "soldtocustomername":
                    {
                        prop = p => p.SoldToCustomerName;
                        break;
                    }
                case "soldtocustomerid":
                    {
                        prop = p => p.SoldToCustomerID;
                        break;
                    }
                case "salesrepid":
                    {
                        prop = p => p.SalesRepID;
                        break;
                    }
                case "salesrepname":
                    {
                        prop = p => p.SalesRepName;
                        break;
                    }
                case "modifieddate":
                    {
                        prop = p => p.ModifiedDate;
                        break;
                    }
                case "districtname":
                    {
                        prop = p => p.DistrictName;
                        break;
                    }
                case "pricingrequestid":
                    {
                        prop = p => p.PricingRequestID;
                        break;
                    }
                case "pricingrequeststatus":
                    {
                        prop = p => p.PricingRequestStatus;
                        break;
                    }
                default:
                    {
                        prop = p => p.PricingRequestID;
                        break;
                    }
            }


            Func<IEnumerable<PricingRequestEntity>, Func<PricingRequestEntity, object>, IEnumerable<PricingRequestEntity>> func = null;
            Debug.WriteLine("SortDirection: " + this.hdnSortDirection.Value);

            switch (this.hdnSortDirection.Value)
            {
                case "ascending":
                    {
                        func = (c, p) => c.OrderBy(p);
                        break;
                    }
                case "descending":
                    {
                        func = (c, p) => c.OrderByDescending(p);
                        break;
                    }
            }

            IEnumerable<PricingRequestEntity> priceRequests = priceRequestList as IEnumerable<PricingRequestEntity>;

            priceRequests = func(priceRequests, prop);

            List<PricingRequestEntity> sortedRequests = priceRequests.ToList<PricingRequestEntity>();

            return sortedRequests;
        }

        ///// <summary>Handles the Sorting event of the grid view control.</summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void GV_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {

            if (e.SortExpression.ToLower() == this.hdnSortField.Value.ToLower())
            {
                if (this.hdnSortDirection.Value == "ascending")
                    this.hdnSortDirection.Value = "descending";
                else
                    this.hdnSortDirection.Value = "ascending";
            }
            else
            {
                this.hdnSortField.Value = e.SortExpression.ToLower();
                this.hdnSortDirection.Value = "ascending";
            }

            List<PricingRequestEntity> priceRequests = null;
            if (Session["PriceSearchList"] != null)
            {
                priceRequests = Session["PriceSearchList"] as List<PricingRequestEntity>;
            }
            else
            {
                priceRequests = this.GetSearchResults();
            }

            priceRequests = this.SortGridView(priceRequests);
            Session["PriceSearchList"] = priceRequests;
            this.BindDT(priceRequests);
        }

        /// <summary>Handles the Delete event of the gvSearch GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_View(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intID = Int32.Parse(this.gvSearch.DataKeys[e.RowIndex].Value.ToString());
            string strStatus = this.gvSearch.DataKeys[e.RowIndex]["PricingRequestStatus"].ToString().ToLower();

            string strCreatedBy = this.gvSearch.DataKeys[e.RowIndex]["CreatedBy"].ToString().ToLower();
            
            if((strStatus != "submitted") && (strCreatedBy == ADUtility.GetUserNameOfAppUser(Request).ToLower()))
                Response.Redirect("PriceRequestFormCustomer.aspx?PricingRequestID=" + intID.ToString(), false);
            else
                Response.Redirect("PriceRequestView.aspx?PricingRequestID=" + intID.ToString(), false);

        }
        
        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Copy(object sender, GridViewCancelEditEventArgs e)
        {
            int intID = Int32.Parse(this.gvSearch.DataKeys[e.RowIndex].Value.ToString());

            ADUserEntity myUser = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

            PricingRequestEntity pr = PricingRequestEntity.CopyPricingRequest(intID, myUser.UserName);
            Response.Redirect("PriceRequestFormCustomer.aspx?PricingRequestID=" + pr.PricingRequestID.ToString(), false);

        }  ////end GV_Copy

        /// <summary>Handles the RowDataBound event of the gvSearch GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnCopy");
                btn.Attributes.Add("onclick", "return confirm_copy();");

            //    Label lblUserName = (Label)e.Row.FindControl("lblUserName");
            //    if (lblUserName.Text == string.Empty)
            //        e.Row.Visible = false;
            }

            //if (e.Row.RowType == DataControlRowType.Footer)
            //{
            //    TextBox txtAddUserName = (TextBox)e.Row.FindControl("txtAddUserName");
            //    Button btnAdd = (Button)e.Row.FindControl("btnAdd");
            //    EnterButton.TieButton(txtAddUserName, btnAdd);
            //}

        }

        /// <summary>Handles the Reset button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.txtCustomerID.Text = string.Empty;
            this.txtCustomerName.Text = string.Empty;
            this.txtDateFrom.Text = string.Empty;
            this.txtDateTo.Text = string.Empty;
            this.txtMaterialID.Text = string.Empty;
            this.txtPriceRequestID.Text = string.Empty;
            this.txtSalesDistrictID.Text = string.Empty;
            this.txtSalesDistrictName.Text = string.Empty;
            this.txtSalesRepID.Text = string.Empty;
            this.txtSalesRepName.Text = string.Empty;

            this.ddlStatus.ClearSelection();

            Session["PriceSearchList"] = null;
            List<PricingRequestEntity> results = new List<PricingRequestEntity>();
            this.BindDT(results);

        } // end gv_RowDataBound

    }
}