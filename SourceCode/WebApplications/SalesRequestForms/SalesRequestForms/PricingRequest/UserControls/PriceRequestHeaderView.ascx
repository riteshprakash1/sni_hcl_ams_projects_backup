﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceRequestHeaderView.ascx.cs" Inherits="SalesRequestForms.PricingRequest.UserControls.PriceRequestHeaderView" %>
   <div style="width:980px; text-align:left">
        <table width="100%">
            <tr>
                <td style="text-align:center;font-size: medium;"><strong><asp:Label ID="lblHeaderTitle" runat="server"></asp:Label></strong></td>
            </tr>
        </table>
        <table width="100%" style="background-color: #FFFF00">
            <tr>
                <td style="font-weight:bold; font-size:10pt"><asp:Label ID="lblCompletionTime" runat="server" Text="ONCE REQUEST IS SUBMITTED, PLEASE ALLOW UP TO 3-5 DAYS FOR COMPLETION."></asp:Label></td>
            </tr>
        </table>
    </div>
    <div style="width:980px; text-align:left">
            <table width="100%">
            <tr>
                <td style="width:50%; vertical-align:top">
                    <table>
                        <tr>
                            <td style="width: 32%"><strong>Price Request ID:</strong></td>
                            <td><asp:Label ID="lblPricingRequestID" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Status:</td>
                            <td><asp:Label ID="lblPricingRequestStatus" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="LabelSubmittedBy" runat="server" Text="Submitted By:"></asp:Label></td>
                            <td><asp:Label ID="lblSubmittedBy" runat="server"></asp:Label></td>
                        </tr>    
                        <tr>
                            <td><asp:Label ID="LabelSubmittedDate" runat="server" Text="Submitted Date:"></asp:Label></td>
                            <td><asp:Label ID="lblSubmittedDate" runat="server"></asp:Label></td>
                        </tr>       
                        <tr>
                            <td>Customer Sold To Number:</td>
                            <td><asp:Label ID="lblCustomerID" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Sales Rep:</td>
                            <td><asp:Label ID="lblSalesRepName" runat="server"></asp:Label><asp:Label ID="lblSalesRepID" runat="server" Visible="false"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>RVP:</td>
                            <td><asp:Label ID="lblRvpName" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>DE:</td>
                            <td><asp:Label ID="lblDeName" runat="server"></asp:Label><asp:Label ID="lblDistrictID" runat="server" Visible="false"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Customer Name:</td>
                            <td><asp:Label ID="lblCustomerName" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Customer Address:</td>
                            <td><asp:Label ID="lblCustomerAddress" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Customer City, State, Zip:</td>
                            <td><asp:Label ID="lblCustomerCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblCustomerState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblCustomerZip" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>GPO:</td>
                            <td><asp:Label ID="lblGPO" runat="server"></asp:Label></td>
                        </tr>
                         <tr>
                            <td>Customer Group1:</td>
                            <td><asp:Label ID="lblCustGroup1" runat="server"></asp:Label><asp:Label ID="LabelCustGroup1Filler" runat="server"></asp:Label><asp:Label ID="lblCustGroup1ID" runat="server"></asp:Label></td>
                        </tr>                       
                        <tr>
                            <td>Agreement Term: </td>
                            <td><asp:Label ID="lblAgreementTerm" runat="server"></asp:Label></td>
                        </tr>        
                        <tr>
                            <td>Competitor: </td>
                            <td><asp:Label ID="lblCompetitor" runat="server"></asp:Label></td>
                        </tr>
                    </table>           
                </td>
                <td style="width:50%; vertical-align:top">
                    <table width="100%">
                        <tr>
                            <td valign="top" style="width:160; font-weight:bold">Additional Acct# 's that are part of this deal:</td>
                        </tr>
                        <tr>
                             <td>
                                <asp:GridView ID="gvAdditionalAcct" runat="server" Width="100%" AutoGenerateColumns="False" ShowFooter="false" DataKeyNames="CustomerID" 
                                     CellPadding="2" GridLines="Both">
                                    <RowStyle HorizontalAlign="left" />
                                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                                    <Columns>
                                        <asp:BoundField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer ID" DataField="CustomerID" />
                                        <asp:BoundField ItemStyle-Width="75%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer Name" DataField="CustomerName" />
                                    </Columns>
                                </asp:GridView>                
                            </td>
                        </tr>
                    </table>
                    <table style="width:100%">
                        <tr>
                            <td colspan="2"><br /><br /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong><u>To-Date Sales for Order Materials</u></strong></td>
                        </tr>            
                        <tr>
                            <td style="width:28%">Rolling 12 Month:</td>
                            <td><asp:Label ID="lblRequest12RollingRev" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Request Date To Current:</td>
                            <td><asp:Label ID="lblRequestToDateRev" runat="server"></asp:Label></td>
                        </tr>        
                        <tr>
                            <td>Last Updated:</td>
                            <td><asp:Label ID="lblToDateUpdatedOn" runat="server"></asp:Label></td>
                        </tr>        
                    </table>
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td style="width: 375px; vertical-align:top">Business Opportunities, Justification, and Strategic Rationale for Price: </td>
                <td><asp:Label ID="lblJustification" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>What is estimated 12 month incremental revenue for products on request? </td>
                <td><asp:Label ID="lblIncrementalRevenue" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>What is the probability of incremental revenue for  products on request (%)? </td>
                <td><asp:Label ID="lblRevenueProbability" runat="server"></asp:Label></td>
            </tr>
        </table>
    </div>