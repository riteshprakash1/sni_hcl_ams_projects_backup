﻿// -------------------------------------------------------------
// <copyright file="PriceRequestMaterialView.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.UserControls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PricingRequest.Classes;

    /// <summary>This is the Price Request Material View User Control page.</summary> 
    public partial class PriceRequestHeaderView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>Gets or sets the Header Title.</summary>
        /// <value>The Header Title.</value>
        public string HeaderTitle
        {
            get { return this.lblHeaderTitle.Text; }

            set { this.lblHeaderTitle.Text = value; }
        }


        /// <summary>Load the PricingRequestEntity from the data.</summary>
        /// <param name="p">The PricingRequestEntity.</param>
        public void PricingRequestDataLoad(PricingRequestEntity p)
        {
            this.lblPricingRequestID.Text = p.PricingRequestID.ToString();
            this.lblPricingRequestStatus.Text = p.PricingRequestStatus;
            this.lblCustomerID.Text = p.SoldToCustomerID;
            this.lblCustomerName.Text = p.SoldToCustomerName;
            this.lblCustomerAddress.Text = p.SoldToStreet;
            this.lblCustomerCity.Text = p.SoldToCity;
            this.lblCustomerState.Text = p.SoldToStateCode;
            this.lblCustomerZip.Text = p.SoldToPostalCode;
            this.lblSalesRepID.Text = p.SalesRepID;
            this.lblSalesRepName.Text = p.SalesRepName;
            this.lblRvpName.Text = p.RvpName;
            this.lblDeName.Text = p.DistrictName;
            this.lblDistrictID.Text = p.DistrictID;
            this.lblGPO.Text = p.GpoDesc;
            this.lblCustGroup1.Text = p.CustGroup1;

            if (p.CustGroup1ID != string.Empty)
                this.LabelCustGroup1Filler.Text = " - ";
            else
                this.LabelCustGroup1Filler.Text = string.Empty;

            this.lblCustGroup1ID.Text = p.CustGroup1ID;

            this.lblAgreementTerm.Text = p.AgreementTerms;
            this.lblJustification.Text = p.BusinessJustification.Replace("\n", "<br/>"); ;
            this.lblCompetitor.Text = p.Competitor;
            this.lblIncrementalRevenue.Text = p.IncrementalRevenueEstimate;
            this.lblRevenueProbability.Text = p.IncrementalRevenueProbability;

            this.lblRequestToDateRev.Text = String.Format("{0:C}", p.RequestToDateRev);
            this.lblRequest12RollingRev.Text = String.Format("{0:C}", p.Request12RollingRev);
            this.lblToDateUpdatedOn.Text = p.ToDateUpdatedOn;

            this.lblSubmittedBy.Text = p.SubmittedByName;

            /********* Need a submit date *******************/
            this.lblSubmittedDate.Text = p.ModifiedDate;

            this.BindAdditionalAcct(p.PricingRequestID);
        }

        /// <summary>Sets the Visible attribute of some web controls</summary>
        public void SetHeaderControls(PricingRequestEntity pr)
        {
            bool isVisible = Convert.ToBoolean((pr.PricingRequestStatus != "Submitted") && (pr.PricingRequestStatus != "Expired"));
            this.LabelSubmittedBy.Visible = !isVisible;
            this.LabelSubmittedDate.Visible = !isVisible;
            this.lblSubmittedBy.Visible = !isVisible;
            this.lblSubmittedDate.Visible = !isVisible;
        }

        /// <summary>Binds the Additional Acct table to the gvAdditionalAcct GridView.</summary>
        /// <param name="intPricingRequestID">The PricingRequestID.</param>
        protected void BindAdditionalAcct(int intPricingRequestID)
        {
            Collection<PricingCustomerEntity> myCustomers = PricingCustomerEntity.GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID(intPricingRequestID, 1);

            this.gvAdditionalAcct.DataSource = myCustomers;
            this.gvAdditionalAcct.DataBind();
        }

        /// <summary>This writes a string that with the data for the body of the email.</summary>
        /// <param name="pc">The PricingRequestEntity object.</param>
        /// <returns>The email body string</returns>
        public string GetHeaderEmailBody(PricingRequestEntity pr)
        {
            Collection<PricingCustomerEntity> additionalCustomers = PricingCustomerEntity.GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID(pr.PricingRequestID, 1);
            
            string tablewidth = "980px";

            string strEmailBody = string.Empty;
            strEmailBody += "<style type=\"text/css\">";
            strEmailBody += ".style1{ text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:8pt}";
            strEmailBody += ".style10{ text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:10pt}";
            strEmailBody += "</style>";

            //          strEmailBody += "<div style=\"width:" + tablewidth + "; text-align:left;font-family: Arial, Helvetica, sans-serif;font-size: 8pt;\">";
            strEmailBody += "<table width=\"" + tablewidth + "\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"text-align:center;font-size: medium;\"><strong>PRICE REQUEST FORM SUBMITTED</strong></td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "<table width=\"" + tablewidth + "\" style=\"background-color: #FFFF00\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"font-weight:bold; font-size:10pt\">" + this.lblCompletionTime.Text + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            //strEmailBody += "</div>";
            //strEmailBody += "<div style=\"width:" + tablewidth + "; text-align:left;font-family: Arial, Helvetica, sans-serif;font-size: 8pt;\">";
            strEmailBody += "<table width=\"" + tablewidth + "\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"width:50%; vertical-align:top\">";
            strEmailBody += "<table>";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"width: 32%;font-size:10pt;\"><strong>Price Request ID:</strong></td>";
            strEmailBody += "<td  class=\"style1\">" + pr.PricingRequestID.ToString() + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Status:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.PricingRequestStatus + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Submitted By:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.SubmittedByName + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Submitted Date:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.ModifiedDate + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Customer Sold To Number:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.SoldToCustomerID + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Sales Rep:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.SalesRepName + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">RVP:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.RvpName + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">DE:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.DistrictName + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Customer Name:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.SoldToCustomerName + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Customer Address:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.SoldToStreet + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Customer City, State, Zip:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.SoldToCity + ",&nbsp;" + pr.SoldToStateCode + "&nbsp;&nbsp;" + pr.SoldToPostalCode + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">GPO:</td>";
            strEmailBody += "<td  class=\"style1\">" + pr.GpoDesc + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Customer Group1:</td>";

            string strCustGroup1 = string.Empty;
            if (pr.CustGroup1ID != string.Empty)
                strCustGroup1 = pr.CustGroup1 + " - " + pr.CustGroup1ID;

            strEmailBody += "<td  class=\"style1\">" + strCustGroup1 + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Agreement Term: </td>";
            strEmailBody += "<td  class=\"style1\">" + pr.AgreementTerms + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td  class=\"style1\">Competitor: </td>";
            strEmailBody += "<td  class=\"style1\">" + pr.Competitor + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "</td>";
            strEmailBody += "<td style=\"width:50%; vertical-align:top\">";

            strEmailBody += "<table width=\"487px\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td valign=\"top\" style=\"font-size:10pt;font-weight:bold;font-family: Arial, Helvetica, sans-serif;\">Additional Acct# 's that are part of this deal:</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td>";
            strEmailBody += "<table cellspacing=\"0\" cellpadding=\"2\" rules=\"all\" border=\"1\" style=\"width:100%;border-collapse:collapse;\">";
            strEmailBody += "<tr align=\"left\" style=\"background-color:Gainsboro;\">";
            strEmailBody += "<td align=\"left\" style=\"font-family: Arial, Helvetica, sans-serif;font-size:10pt;\">Customer ID</td>";
            strEmailBody += "<td align=\"left\" style=\"font-family: Arial, Helvetica, sans-serif;font-size:10pt;\">Customer Name</td>";
            strEmailBody += "</tr>";
            foreach (PricingCustomerEntity ad in additionalCustomers)
            {
                strEmailBody += "<tr align=\"left\">";
                strEmailBody += "<td class=\"style1\" align=\"left\" style=\"width:25%;\">" + ad.CustomerID + "</td><td class=\"style1\" align=\"left\" style=\"width:75%;\">" + ad.CustomerName + "</td>";
                strEmailBody += "</tr>";
            }
            strEmailBody += "</table>";
            strEmailBody += "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "<table width=\"" + tablewidth + "\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td class=\"style1\" style=\"width: 375px; vertical-align:top\">Business Opportunities, Justification, and Strategic Rationale for Price: </td>";
            strEmailBody += "<td class=\"style1\">" + pr.BusinessJustification.Replace("\n", "<br/>") + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td class=\"style1\">What is estimated 12 month incremental revenue for products on request? </td>";
            strEmailBody += "<td class=\"style1\">" + pr.IncrementalRevenueEstimate + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td class=\"style1\">What is the probability of incremental revenue for  products on request (%)? </td>";
            strEmailBody += "<td class=\"style1\">" + pr.IncrementalRevenueProbability + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            //           strEmailBody += "</div>";

            return strEmailBody;
        }



    } //// end class
}