﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceRequestMaterialView.ascx.cs" Inherits="SalesRequestForms.PricingRequest.UserControls.PriceRequestMaterialView" %>
    <table width="100%">
        <tr>
            <td>
                <div id="gridContainer">
                    <asp:GridView ID="gvMaterial" runat="server" Width="2730px" AutoGenerateColumns="False" 
                        ShowFooter="false" DataKeyNames="Material_ID" CellPadding="2" GridLines="Both">
                        <HeaderStyle BackColor="#dcdcdc" VerticalAlign="Bottom" Height="45px"/>
                        <AlternatingRowStyle BackColor="#F2F2F2" />
                        <RowStyle HorizontalAlign="Right" Height="60px" />
                        <Columns>
                           <asp:BoundField  ItemStyle-Width="72px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material ID" DataField="Material_ID" />
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material Desc" DataField="Material_Desc" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="List Price" DataField="List_Price" />
                           <asp:BoundField ItemStyle-Width="32px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="UOM" DataField="Sellable_UOM" />
                           <asp:BoundField ItemStyle-Width="32px"  ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="UOM Qty" DataField="Qty_Per_Sellable_UOM" />
                           <asp:BoundField ItemStyle-Width="71px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request Amt($)" DataField="Requested_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request % Off" DataField="Requested_Discount" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="GPO Price" DataField="GPO_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="GPO Sales Deal" DataField="GPO_SalesDeal" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Cust Grp1 Price" DataField="CG1_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="CG1 Sales Deal" DataField="CG1_SalesDeal" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="CG1 Valid From" DataField="CG1_ValidFrom" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="CG1 Valid To" DataField="CG1_ValidTo" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Local Price" DataField="Local_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Local Sales Deal" DataField="Local_SalesDeal" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Local Valid From" DataField="Local_ValidFrom" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Local Valid To" DataField="Local_ValidTo" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Current Price" DataField="Net_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rolling 12 Mth Sales" DataField="Rolling_Revenue" />
                           <asp:BoundField ItemStyle-Width="71px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Competitor Price" DataField="Competitor_Price" />
                           <asp:BoundField ItemStyle-Width="71px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Expected $ volume (per item)" DataField="Expected_Volume" />
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 4" DataField="Level_4_Desc" />
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 5" DataField="Level_5_Desc" />
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 6" DataField="Level_6_Desc" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="COG" DataField="COG" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request GM%" DataField="Requested_GM" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Internal Floor" DataField="Internal_Floor_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="State ASP" DataField="State_ASP" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="US ASP" DataField="US_ASP" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="HPG" DataField="HPG_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="MedAssets T1" DataField="MedAssets_T1_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="MedAssets T2" DataField="MedAssets_T2_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Premier" DataField="Premier_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Novation Part" DataField="Novation_Participant_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Novation Comm" DataField="Novation_Committed_Price" />
                           <asp:BoundField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Market Manager" DataField="Marketing_Manager" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rep Discount" DataField="Rep_Discount" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rep Price" DataField="Rep_Price" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="DOS/DE Discount" DataField="DE_Discount" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="DOS/DE Price" DataField="DE_Price" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="RVP Discount" DataField="RVP_Discount" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="RVP Price" DataField="RVP_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Approval Required From" DataField="Approval_Required_From" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Recommendation Approval" DataField="Recommendation_Approval" />
                        </Columns>
                    </asp:GridView>  
                </div>
            </td>
        </tr>
    </table>

                    


                    <script type="text/javascript" src="../../Scripts/jquery-1.4.1.js"></script> 

                    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script> 
                    <script type="text/javascript" >

                        $(document).ready(function () {

                            var n = $("#wideShell div").width;

                            // here clone our gridview first         
                            var tab = $("#<%=gvMaterial.ClientID%>").clone(true);
                            // clone again for freeze         
                            var tabFreeze = $("#<%=gvMaterial.ClientID%>").clone(true);

                            // set width (for scroll)         
                            var totalWidth = $("#<%=gvMaterial.ClientID%>").outerWidth();
                            var firstColWidth = $("#<%=gvMaterial.ClientID%> th:first-child").outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().next().next().outerWidth();

                            tabFreeze.width(firstColWidth);
                            tab.width(totalWidth - firstColWidth);

                            var totalHeight = $("#<%=gvMaterial.ClientID%>").outerHeight() + 17;

                            // here make 2 table 1 for freeze column 2 for all remain column          
                            tabFreeze.find("th:gt(5)").remove();
                            tabFreeze.find("td:not(:nth-child(-n+6))").remove();
                            tab.find("th:lt(6)").remove();
                            tab.find("td::nth-child(-n+6)").remove();

                            tab.find("th:first-child").height(45);
                            tabFreeze.find("th:first-child").height(45);
                            tab.find("td:first-child").height(60);
                            tabFreeze.find("td:first-child").height(60);

                            // create a container for these 2 table and make 2nd table scrollable           
                            var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:1000px; overflow-x:auto; height:' + totalHeight.toString() + '; overflow-y:hidden"></div></td></tr></table)');
                            $("#FCol", container).html($(tabFreeze));
                            $("#Col", container).html($(tab));
                            // clear all html         
                            $("#gridContainer").html('');
                            $("#gridContainer").append(container);
                        }); 
                        </script>