﻿// -------------------------------------------------------------
// <copyright file="PriceRequestMaterialView.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.UserControls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Text;
    using System.IO;
    using SalesRequestForms.PricingRequest.Classes;
    using SalesRequestForms.Classes;


    /// <summary>This is the Price Request Material View User Control page.</summary> 
    public partial class PriceRequestMaterialView : System.Web.UI.UserControl
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("PriceRequestFormMaterialView Page_Load");

        }//// end Page_Load

        /// <summary>Binds the Materials table to the gvMaterial GridView.</summary>
        /// <param name="materials">The List of PricingRequestMaterialEntities.</param>
        public void BindMaterials(List<PricingRequestMaterialEntity> materials)
        {

            this.gvMaterial.DataSource = materials;
            this.gvMaterial.DataBind();

            PricingRequestMaterialEntity.HighLightGridViewColumns(this.gvMaterial);
            PricingRequestMaterialEntity.HideGridViewColumns(this.gvMaterial, Request);
        }

    } //// end class
}