﻿// -------------------------------------------------------------
// <copyright file="PriceRequestFormCustomer.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PricingRequest
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PricingRequest.Classes;

    /// <summary>This is the Price Request Form Customer page.</summary> 
    public partial class PriceRequestFormCustomer : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("PriceRequestFormCustomer Page_Load");
            ADUserEntity myUser = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
            this.hdnSubmittedBy.Value = myUser.DisplayName;
            this.hdnFormStamp.Value = myUser.UserName + DateTime.Now.ToUniversalTime().ToString().Replace(" ", string.Empty);

            this.lblMessage.Text = string.Empty;

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["PricingRequestID"] == null)
                {
                    this.hdnPricingRequestID.Value = "0";
                    Collection<PricingCustomerEntity> collCustomers = new Collection<PricingCustomerEntity>();
                    collCustomers.Add(new PricingCustomerEntity());
                    this.BindAdditionalAcct(collCustomers);
                }
                else
                {
                    this.hdnPricingRequestID.Value = Request.QueryString["PricingRequestID"];
                    int intPricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);

                    PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);
                    this.PricingRequestDataLoad(pr);

                    Collection<PricingCustomerEntity> additionalCustomers = PricingCustomerEntity.GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID(intPricingRequestID, 1);
                    this.BindAdditionalAcct(additionalCustomers);
                }


                string strInstructions = "1. Enter the Sold To Customer ID and click \"Find\" to populate the customer information and Sales Rep drop down list<br> ";
                strInstructions += "2. Select the Sales Rep<br>";
                strInstructions += "3. Fill out remaining customer information and material information<br>";
                strInstructions += "4. After you have completed the form, click the \"Submit Request\" button";

                this.lnkInstructionsLabel.Attributes.Add("onmouseover", "showhint('" + strInstructions + " ', this, event, '700px', 0, 90);");

                EnterButton.TieButton(txtAgreementTerm, btnSubmit);
                EnterButton.TieButton(txtCompetitor, btnSubmit);
                EnterButton.TieButton(txtIncrementalRevenue, btnSubmit);
              //  EnterButton.TieButton(txtRevenueProbability, btnSubmit);
            }
            else
            {
                Debug.WriteLine("Page.IsPostBack");
            } 
            
        }//// end Page_Load

        /************************************ ADDITIONAL ACCTS ****************************************************************/
        /// <summary>Load the CustomerEntity Collection with the Additional Account Customers from the AdditionalAcct Gridview.</summary>
        /// <returns>The Collection PricingCustomerEntity</returns>
        protected Collection<PricingCustomerEntity> LoadAdditionalAcctCustomersFromPage()
        {
            Collection<PricingCustomerEntity> additionalAccts = new Collection<PricingCustomerEntity>();

            foreach (GridViewRow r in this.gvAdditionalAcct.Rows)
            {
                if ((r.RowType == DataControlRowType.DataRow) && r.Visible)
                {
                    Label lblCustomerID = (Label)r.FindControl("lblCustomerID");
                    Label lblCustomerName = (Label)r.FindControl("lblCustomerName");

                    PricingCustomerEntity cust = new PricingCustomerEntity();
                    cust.CustomerID = lblCustomerID.Text;
                    cust.CustomerName = lblCustomerName.Text;
                    cust.PricingCustomerTypeID = 1;

                    Debug.WriteLine("r.RowType: " + r.RowType.ToString() + "; ID: " + cust.CustomerID + "; Name: " + cust.CustomerName);
                    additionalAccts.Add(cust);
                }
            }

            return additionalAccts;
        }

        /// <summary>Handles the Delete event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_AdditionalAcctDelete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            Collection<PricingCustomerEntity> myCustomers = this.LoadAdditionalAcctCustomersFromPage();
            myCustomers.RemoveAt(e.RowIndex);
            this.BindAdditionalAcct(myCustomers);
        }

        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_AdditionalAcctAdd(object sender, GridViewCancelEditEventArgs e)
        {
            TextBox txtCustomerID = (TextBox)this.gvAdditionalAcct.FooterRow.FindControl("txtCustomerID");
            Collection<PricingCustomerEntity> newCustomers = PricingCustomerEntity.GetSoldToByCustomerID(txtCustomerID.Text);

            if (newCustomers.Count > 0)
            {
                Collection<PricingCustomerEntity> myCustomers = this.LoadAdditionalAcctCustomersFromPage();
                myCustomers.Add(newCustomers[0]);

                this.BindAdditionalAcct(myCustomers);
            }
            else
            {
                Page.Validate("AdditionalAccount");
                CustomValidator vldCustomerIDExists = (CustomValidator)this.gvAdditionalAcct.FooterRow.FindControl("vldCustomerIDExists");
                vldCustomerIDExists.IsValid = false;
            }

        } ////end GV_Add

        /// <summary>Adds the Sold To Customer to the GridView.</summary>
        protected void AdditionalAcctAdd()
        {
            TextBox txtCustomerID = (TextBox)this.gvAdditionalAcct.FooterRow.FindControl("txtCustomerID");
            Collection<PricingCustomerEntity> newCustomers = PricingCustomerEntity.GetSoldToByCustomerID(txtCustomerID.Text);

            if (newCustomers.Count > 0)
            {
                Collection<PricingCustomerEntity> myCustomers = this.LoadAdditionalAcctCustomersFromPage();
                myCustomers.Add(newCustomers[0]);

                this.BindAdditionalAcct(myCustomers);
            }
            else
            {
                Page.Validate("AdditionalAccount");
                CustomValidator vldCustomerIDExists = (CustomValidator)this.gvAdditionalAcct.FooterRow.FindControl("vldCustomerIDExists");
                vldCustomerIDExists.IsValid = false;
            }

        } ////end SoldToAdd

        /// <summary>Binds the Additional Acct table to the gvAdditionalAcct GridView.</summary>
        /// <param name="myReps">AccountChangeReps collection</param>
        protected void BindAdditionalAcct(Collection<PricingCustomerEntity> myCustomers)
        {
            if (myCustomers.Count == 0)
            {
                PricingCustomerEntity customer = new PricingCustomerEntity();
                myCustomers.Add(customer);
            }

            this.gvAdditionalAcct.DataSource = myCustomers;
            this.gvAdditionalAcct.DataBind();

            TextBox txtCustomerID = (TextBox)this.gvAdditionalAcct.FooterRow.FindControl("txtCustomerID");
            Button btnAdd = (Button)this.gvAdditionalAcct.FooterRow.FindControl("btnAdd");
            EnterButton.TieButton(txtCustomerID, btnAdd);
        }

        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_AdditionalAcctRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCustomerID = (Label)e.Row.FindControl("lblCustomerID");
                Label lblCustomerName = (Label)e.Row.FindControl("lblCustomerName");

                //// if the Sold To are blank hide the row
                e.Row.Visible = Convert.ToBoolean((lblCustomerID.Text != String.Empty) || (lblCustomerName.Text != String.Empty));
            }
        } // end 

       /// <summary>Handles the Submit button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(Convert.ToInt32(this.hdnPricingRequestID.Value));
            if(pr == null)
                pr = new PricingRequestEntity();

            Debug.WriteLine("btnSubmit_Click: " + pr.PricingRequestStatus);

            if ((pr.PricingRequestStatus != "Submitted") && (pr.PricingRequestStatus != "Expired") || (this.hdnPricingRequestID.Value == "0"))
            {

                if ((pr.CreatedBy.ToLower() == ADUtility.GetUserNameOfAppUser(Request).ToLower()) || (this.hdnPricingRequestID.Value == "0"))
                {
                    Page.Validate();
                    Debug.WriteLine("Validated");

                    if (Page.IsValid)
                    {
                        try
                        {
                            PricingRequestEntity req = LoadPricingRequestEntityFromPage();

                            Collection<PricingCustomerEntity> addCustomers = this.LoadAdditionalAcctCustomersFromPage();

                            if (this.hdnPricingRequestID.Value == "0")
                            {

                                req = PricingRequestEntity.InsertPricingRequest(req, addCustomers);
                                this.hdnPricingRequestID.Value = req.PricingRequestID.ToString();
                                this.hdnFormStamp.Value = req.FormStamp;
                            }
                            else
                            {
                                req = PricingRequestEntity.UpdatePricingRequest(req, addCustomers);
                            }

                            Response.Redirect("PriceRequestFormMaterials.aspx?PricingRequestID=" + req.PricingRequestID.ToString(), false);
                        }
                        catch (Exception ex)
                        {
                            LogException.HandleException(ex, Request);
                            this.lblMessage.Text = "Error on Submit: " + ex.Message;
                        }
                    }
                }
                else
                {
                    this.lblMessage.Text = "Only the original author of the Price Request can make changes.";
                }
            }
            else
            {
                this.lblMessage.Text = "The Price Request Status is " + pr.PricingRequestStatus + " which means the Price Request Form cannot be changed.";
            }

        }

        /// <summary>Load the PricingRequestEntity from the data.</summary>
        /// <returns>The PricingRequestEntity</returns>
        protected PricingRequestEntity LoadPricingRequestEntityFromPage()
        {
            PricingRequestEntity p = new PricingRequestEntity();

            p.PricingRequestID = Convert.ToInt32(this.hdnPricingRequestID.Value);
            p.ProgramID = Convert.ToInt32(this.hdnProgramID.Value);
            p.SoldToCustomerID = CustomerMaster1.CustomerID.PadLeft(10, '0');
            p.SoldToCustomerName = CustomerMaster1.CustomerName;
            p.SoldToStreet = CustomerMaster1.CustomerAddress;
            p.SoldToCity = CustomerMaster1.CustomerCity;
            p.SoldToStateCode = CustomerMaster1.CustomerState;
            p.SoldToPostalCode = CustomerMaster1.CustomerZip;

            p.GpoDesc = CustomerMaster1.GPO;
            p.CustGroup1 = CustomerMaster1.CustGroup1;
            p.CustGroup1ID = CustomerMaster1.CustGroup1ID;
            p.AgreementTerms = this.txtAgreementTerm.Text;
            p.BusinessJustification = this.txtJustification.Text;
            p.Competitor = this.txtCompetitor.Text;
            p.IncrementalRevenueEstimate = this.txtIncrementalRevenue.Text;
            p.IncrementalRevenueProbability = this.ddlRevenueProbability.SelectedValue;
            p.PricingRequestStatus = "In Progress";

            p.FormStamp = this.hdnFormStamp.Value;
            p.ModifiedBy = ADUtility.GetUserNameOfAppUser(Request);
            p.SubmittedByName = this.hdnSubmittedBy.Value;

            p.SalesRepID = CustomerMaster1.RepID;
            p.SalesRepName = CustomerMaster1.RepName;
            p.RvpName = CustomerMaster1.RvpName;
            p.DistrictName = CustomerMaster1.DeName;
            p.DistrictID = CustomerMaster1.DistrictID;

            return p;
        }

        /// <summary>Load the PricingRequestEntity from the data.</summary>
        /// <param name="p">The PricingRequestEntity.</param>
        protected void PricingRequestDataLoad(PricingRequestEntity p)
        {
            this.hdnPricingRequestID.Value =  p.PricingRequestID.ToString();
            this.hdnProgramID.Value = p.ProgramID.ToString();
            CustomerMaster1.CustomerID = p.SoldToCustomerID;

            Collection<SalesRepEntity> myReps = SalesRepEntity.GetSalesRepsByCustomerID(p.SoldToCustomerID.PadLeft(10, '0'));
            CustomerMaster1.LoadSalesRepDropDown(myReps);

            CustomerMaster1.CustomerName = p.SoldToCustomerName;
            CustomerMaster1.CustomerAddress = p.SoldToStreet;
            CustomerMaster1.CustomerCity = p.SoldToCity;
            CustomerMaster1.CustomerState = p.SoldToStateCode;
            CustomerMaster1.CustomerZip = p.SoldToPostalCode;
            CustomerMaster1.RepID = p.SalesRepID;
            CustomerMaster1.RepName = p.SalesRepName;
            CustomerMaster1.RvpName = p.RvpName;
            CustomerMaster1.DeName = p.DistrictName;
            CustomerMaster1.DistrictID = p.DistrictID;
            CustomerMaster1.GPO = p.GpoDesc;
            CustomerMaster1.CustGroup1 = p.CustGroup1;
            CustomerMaster1.CustGroup1ID = p.CustGroup1ID;

            this.txtAgreementTerm.Text = p.AgreementTerms;
            this.txtJustification.Text = p.BusinessJustification;
            this.txtCompetitor.Text = p.Competitor;
            this.txtIncrementalRevenue.Text =  p.IncrementalRevenueEstimate;

            this.ddlRevenueProbability.ClearSelection();
            if (ddlRevenueProbability.Items.FindByValue(p.IncrementalRevenueProbability) != null)
                ddlRevenueProbability.Items.FindByValue(p.IncrementalRevenueProbability).Selected = true;

            this.hdnFormStamp.Value = p.FormStamp;
        }




    } //// end class
}