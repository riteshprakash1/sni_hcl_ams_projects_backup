﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="TestFreeze.aspx.cs" Inherits="SalesRequestForms.PricingRequest.TestFreeze" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table>
    <tr>
        <td>
              <div id="gridContainer">         
                 <asp:GridView ID="DataGrid1" runat="server" Width="2764px" AutoGenerateColumns="False" OnRowDataBound="GV_RowDataBound" 
                    ShowFooter="true" DataKeyNames="Material_ID" CellPadding="2" GridLines="Both" CssClass="Grid" UseAccessibleHeader="True">
                <HeaderStyle BackColor="#dcdcdc" VerticalAlign="Bottom"/>
                <AlternatingRowStyle BackColor="#F2F2F2" />
                <RowStyle HorizontalAlign="Right" />
                     <Columns>
                       <asp:BoundField  ItemStyle-Width="72px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material ID" DataField="Material_ID" />
                       <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material Desc" DataField="Material_Desc" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="List Price" DataField="List_Price" />
                       <asp:BoundField ItemStyle-Width="32px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="UOM" DataField="Sellable_UOM" />
                       <asp:BoundField ItemStyle-Width="32px"  ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="UOM Qty" DataField="Qty_Per_Sellable_UOM" />
                       <asp:BoundField ItemStyle-Width="71px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request Amt($)" DataField="Requested_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request % Off" DataField="Requested_Discount" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="GPO Price" DataField="GPO_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Cust Grp1 Price" DataField="CG1_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Local Price" DataField="Local_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Current Price" DataField="Net_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rolling 12 Mth Sales" DataField="Rolling_Revenue" />
                       <asp:BoundField ItemStyle-Width="71px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Competitor Price" DataField="Competitor_Price" />
                       <asp:BoundField ItemStyle-Width="71px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Expected $ volume (per item)" DataField="Expected_Volume" />
                       <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 4" DataField="Level_4_Desc" />
                       <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 5" DataField="Level_5_Desc" />
                       <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 6" DataField="Level_6_Desc" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="COG" DataField="COG" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request GM%" DataField="Requested_GM" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Internal Floor" DataField="Internal_Floor_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="State ASP" DataField="State_ASP" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="US ASP" DataField="US_ASP" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="HPG" DataField="HPG_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="MedAssets T1" DataField="MedAssets_T1_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="MedAssets T2" DataField="MedAssets_T2_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Premier" DataField="Premier_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Novation Part" DataField="Novation_Participant_Price" />
                       <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Novation Comm" DataField="Novation_Committed_Price" />
                       <asp:BoundField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Market Manager" DataField="Marketing_Manager" />
                       <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rep Discount" DataField="Rep_Discount" />
                       <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rep Price" DataField="Rep_Price" />
                       <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="DOS/DE Discount" DataField="DE_Discount" />
                       <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="DOS/DE Price" DataField="DE_Price" />
                       <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="RVP Discount" DataField="RVP_Discount" />
                       <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="RVP Price" DataField="RVP_Price" />
                       <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Approval Required From" DataField="Approval_Required_From" />
                       <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Recommendation Approval" DataField="Recommendation_Approval" />
                            </Columns>
                        </asp:GridView>
                    </div>
       
        </td>
    </tr>
</table>





</asp:Content>
