﻿<%@ Page Title="Price Request Form - Materials" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="PriceRequestFormMaterials.aspx.cs" Inherits="SalesRequestForms.PricingRequest.PriceRequestFormMaterials" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc2" %>
<%@ Register src="../UserControls/CustomerMasterHorizontalSummary.ascx" tagname="CustomerMasterHorizontalSummary" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="960px">
        <tr>
            <td style="width:14%"><strong>Price Request ID:</strong></td>
            <td style="width:36%"><asp:Label ID="lblPriceRequestID" runat="server"></asp:Label></td>
            <td style="width:15%"><strong>Status:</strong></td>
            <td style="width:35%"><asp:Label ID="lblPriceRequestStatus" runat="server"></asp:Label></td>
        </tr>
    </table>
     <uc1:CustomerMasterHorizontalSummary ID="CustomerMasterHorizontalSummary1" runat="server" />    
    <table width="970px">
        <tr>
            <td style="text-align:left">
                <uc2:waitbutton id="btnRemoveItems" Width="230px"  runat="server" CausesValidation="False" 
                     CssClass="buttonsSubmitBorder" Text="Remove Checked Items"
				        WaitText="Processing..." onclick="btnRemoveItems_Click" ></uc2:waitbutton> 
                 &nbsp;&nbsp;&nbsp;&nbsp;
                
            </td>
            <td style="text-align:left">&nbsp;</td>
            <td style="text-align:right">
                <asp:Button ID="btnStep1" runat="server" CausesValidation="False" 
                     CssClass="buttonsSubmitBorder" Text="Return to Step 1 - Customer" Width="200px" 
                    onclick="btnStep1_Click" />
            </td>
        </tr>
        <tr>
            <td style="width:33%"><uc2:waitbutton id="btnRequestedPrice"  runat="server" CausesValidation="False"  CssClass="buttonsSubmitBorder" 
                            Text="Requested Price for Checked Items" WaitText="Processing..." Width="230px" onclick="btnRequestedPrice_Click" ></uc2:waitbutton>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtSetRequestedPrice" MaxLength="12" style="text-align:right" width="60px" Font-Size="8pt" runat="server" ></asp:TextBox>
                <asp:CompareValidator ID="vldCompareRequestedPrice" runat="server" ControlToValidate="txtSetRequestedPrice" ForeColor="#FF7300" ValueToCompare="0"
                            ErrorMessage="Requested $ must be a number greater than zero." ValidationGroup="SetRequestedPrice"  
                            Operator="GreaterThan" Type="Double">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="vldRequiredSetRequestedPrice" runat="server" ForeColor="#FF7300" 
                ErrorMessage="Requested Price is required." ControlToValidate="txtSetRequestedPrice" ValidationGroup="SetRequestedPrice">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="vldRequestedPriceUpdateItems" runat="server" ValidationGroup="SetRequestedPrice" ForeColor="#FF7300" ErrorMessage="You must select at least one item.">*</asp:CustomValidator>
            </td>
            <td style="text-align:center; width:33%"><uc2:waitbutton id="btnUpdateMaterial"  runat="server" CausesValidation="False" 
                     Text="Update Material" WaitText="Processing..."  CssClass="buttonsSubmitBorder" 
                    onclick="btnUpdateMaterial_Click" ></uc2:waitbutton>
            </td>
            <td style="text-align:right; width:33%">
                <asp:Button ID="btnStep3" runat="server" CausesValidation="False" 
                CssClass="buttonsSubmitBorder" Text="Move to Step 3 - Review" Width="200px" 
                    onclick="btnStep3_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" Font-Size="10pt"/>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AddMaterial" ForeColor="#FF7300" Font-Size="10pt"/>
                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="SetRequestedPrice" ForeColor="#FF7300" Font-Size="10pt"/></td>
        </tr> 
        <tr>
            <td colspan="3" style="text-align:center"><asp:Label ID="lblMessage" runat="server" ForeColor="#FF7300"></asp:Label><asp:HiddenField
                    ID="hdnIsAuthor" runat="server" />
            
            </td>
        </tr>
    </table>
    <table width="100%">
       <tr>
            <td colspan="2">
                <div id="gridContainer">
                    <asp:GridView ID="gvMaterial" runat="server" Width="2764px" AutoGenerateColumns="False" OnRowDataBound="GV_RowDataBound" 
                        ShowFooter="true" DataKeyNames="Material_ID" CellPadding="2" GridLines="Both" OnRowCancelingEdit="GV_Add">
                        <HeaderStyle BackColor="#dcdcdc" VerticalAlign="Bottom" Height="45px"/>
                        <AlternatingRowStyle BackColor="#F2F2F2" />
                        <RowStyle HorizontalAlign="Right" Height="60px" />
                        <FooterStyle Height="60px" />
                        <Columns>
                            <asp:TemplateField HeaderStyle-Width="37px" HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ckbSelectAll" runat="server" AutoPostBack="True" oncheckedchanged="ckbSelectAll_CheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ckbRemoveMaterial" runat="server" />
                                 </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" ValidationGroup="Add" Width="35px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					            </FooterTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Material ID" HeaderStyle-Width="72px" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblMaterialID" Font-Size="8pt" runat="server"  Text='<%# Eval("Material_ID")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMaterialID" MaxLength="20" width="60px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:CustomValidator ID="vldMaterialIDNotFound" runat="server" ValidationGroup="AddMaterial" ErrorMessage="Material ID not found.">*</asp:CustomValidator>
                                    <asp:CustomValidator ID="vldMaterialAlreadyInList" runat="server" ValidationGroup="AddMaterial" ErrorMessage="Material ID is already in the list.">*</asp:CustomValidator>
                                </FooterTemplate>
                           </asp:TemplateField>
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material Desc" DataField="Material_Desc" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="List Price" DataField="List_Price" />
                           <asp:BoundField ItemStyle-Width="32px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="UOM" DataField="Sellable_UOM" />
                           <asp:BoundField ItemStyle-Width="32px"  ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="UOM Qty" DataField="Qty_Per_Sellable_UOM" />
                           <asp:TemplateField HeaderStyle-Width="71px" HeaderText="Request Amt($)" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="TARight">
                                <ItemTemplate>
                                       <asp:CompareValidator ID="vldCompareRequestedPrice" runat="server" ControlToValidate="txtRequestedPrice" ForeColor="#FF7300" ValueToCompare="0"
                                    ErrorMessage="Requested $ must be a number greater than zero." ValidationGroup="UpdateMaterial"  
                                    Operator="GreaterThan" Type="Double">*</asp:CompareValidator>
                                    <asp:TextBox ID="txtRequestedPrice" MaxLength="12" style="text-align:right" width="60px" Font-Size="8pt" runat="server" Text='<%#String.Format("{0:0.00}",Eval("Requested_Price")) %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredRequestedPrice" ControlToValidate="txtRequestedPrice" runat="server" ErrorMessage="Requested $ is a required." 
                                        ValidationGroup="UpdateMaterial" ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request % Off" DataField="Requested_Discount" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="GPO Price" DataField="GPO_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="GPO Sales Deal" DataField="GPO_SalesDeal" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Cust Grp1 Price" DataField="CG1_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="CG1 Sales Deal" DataField="CG1_SalesDeal" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="CG1 Valid From" DataField="CG1_ValidFrom" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="CG1 Valid To" DataField="CG1_ValidTo" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Local Price" DataField="Local_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Local Sales Deal" DataField="Local_SalesDeal" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Local Valid From" DataField="Local_ValidFrom" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Local Valid To" DataField="Local_ValidTo" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Current Price" DataField="Net_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rolling 12 Mth Sales" DataField="Rolling_Revenue" />
                           <asp:TemplateField HeaderStyle-Width="71px" HeaderText="Competitor Price" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="TARight">
                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="vldRequiredCompetitorPrice" ControlToValidate="txtCompetitorPrice" runat="server" ErrorMessage="Competitor Price is a required." 
                                        ValidationGroup="UpdateMaterial" ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtCompetitorPrice" style="text-align:right;" MaxLength="12" width="60px" Font-Size="8pt" runat="server" Text='<%#String.Format("{0:0.00}",Eval("Competitor_Price")) %>'></asp:TextBox>
                                    <asp:CompareValidator ID="vldCompareCompetitorPrice" runat="server" ControlToValidate="txtCompetitorPrice" ForeColor="#FF7300" ValueToCompare="0"
                                    ErrorMessage="Competitor Price must be a number greater than or equal to zero." ValidationGroup="UpdateMaterial"  
                                    Operator="GreaterThanEqual" Type="Double">*</asp:CompareValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField HeaderStyle-Width="71px" HeaderText="Expected $ volume (per item)" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="TARight">
                                <ItemTemplate>
                                    <asp:CompareValidator ID="vldCompareExpectedVolume" runat="server" ControlToValidate="txtExpectedVolume" ForeColor="#FF7300" ValueToCompare="0"
                                    ErrorMessage="Expected Volume must be a number greater than or equal to zero." ValidationGroup="UpdateMaterial"  
                                    Operator="GreaterThanEqual" Type="Double">*</asp:CompareValidator>
                                    <asp:TextBox ID="txtExpectedVolume" style="text-align:right" MaxLength="12" width="60px" Font-Size="8pt" runat="server" Text='<%#String.Format("{0:0.00}",Eval("Expected_Volume")) %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredExpectedVolume" ControlToValidate="txtExpectedVolume" runat="server" ErrorMessage="Expected Volume is a required." 
                                        ValidationGroup="UpdateMaterial" ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 4" DataField="Level_4_Desc" />
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 5" DataField="Level_5_Desc" />
                           <asp:BoundField ItemStyle-Width="107px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Level 6" DataField="Level_6_Desc" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="COG" DataField="COG" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Request GM%" DataField="Requested_GM" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Internal Floor" DataField="Internal_Floor_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="State ASP" DataField="State_ASP" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="US ASP" DataField="US_ASP" />
                
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="HPG" DataField="HPG_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="MedAssets T1" DataField="MedAssets_T1_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="MedAssets T2" DataField="MedAssets_T2_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Premier" DataField="Premier_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Novation Part" DataField="Novation_Participant_Price" />
                           <asp:BoundField ItemStyle-Width="66px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Novation Comm" DataField="Novation_Committed_Price" />
                           <asp:BoundField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Market Manager" DataField="Marketing_Manager" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rep Discount" DataField="Rep_Discount" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Rep Price" DataField="Rep_Price" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="DOS/DE Discount" DataField="DE_Discount" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="DOS/DE Price" DataField="DE_Price" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:P2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="RVP Discount" DataField="RVP_Discount" />
                           <asp:BoundField ItemStyle-Width="50px" DataFormatString="{0:c2}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="RVP Price" DataField="RVP_Price" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Approval Required From" DataField="Approval_Required_From" />
                           <asp:BoundField ItemStyle-Width="66px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Recommendation Approval" DataField="Recommendation_Approval" />
                        </Columns>
                    </asp:GridView>
                </div>  
            </td>
        </tr>
    </table>
    <table width="970px">
        <tr>
            <td colspan="3" style="text-align:center">
                <uc2:waitbutton id="btnMaterialList"  runat="server"  CssClass="buttonsSubmitBorder" CausesValidation="False" 
                    Text="Get Material List" onclick="btnMaterialList_Click"
				    WaitText="Processing..." ></uc2:waitbutton>
                 <asp:CustomValidator ID="vldMaterialList" ForeColor="#FF7300" ValidationGroup="GetMaterialList" runat="server" ErrorMessage="You must check at least one checkbox below.">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center"><asp:ValidationSummary ID="vldSummaryMaterialList" runat="server" ValidationGroup="GetMaterialList" ForeColor="#FF7300" Font-Size="10pt"/></td>
        </tr>
        <tr>
           <td colspan="3" style="text-align:left; font-size:10pt; color:#9C6500; background-color:#FFEB9C; border: 1px solid #9C6500; padding:5px 5px 5px 5px;">
            You may choose one or a combination of both of the following options to add products and pricing to the request as follows: <br /><br />
            1. Enter individual Material IDs 
               above, click Add, enter Request Amt ($), then click Update Material button, OR <br /><br />
            2. Check desired Level 5 or Level 6 hierarchy and click Get Material List button.  Then enter Request Amt ($) for each item and click Update Material button
           </td> 
        </tr> 
        <tr>
            <td style="font-size:12pt" colspan="3"><b>PICK GROUPS OF PRODUCTS BY HIERARCHY (check off products to populate below):</b></td>
        </tr>
        <tr>
            <td style="width:300px; font-size:10pt; text-decoration: underline;"><strong>Level 5</strong></td>
            <td style="width:300px; font-size:10pt; text-decoration: underline;"><strong>Level 6</strong></td>
            <td style="width:300px; font-size:10pt; ">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align:top; border-right-style: solid;"><div style="max-height:285px; width:270px; overflow:scroll"><asp:CheckBoxList ID="ckbListLevel5" BackColor="White" DataValueField="Level_5_Desc" DataTextField="Level_5_Desc" runat="server" Width="230px"></asp:CheckBoxList></div></td>
            <td style="vertical-align:top"><div style="max-height:285px; width:270px; overflow:scroll"><asp:CheckBoxList ID="ckbListLevel6" BackColor="White" DataValueField="Level_6_Desc" DataTextField="Level_6_Desc" runat="server" Width="230px"></asp:CheckBoxList></div></td>
            <td style="vertical-align:top; border-right-style: solid;"><div style="max-height:285px; height:285px; width:270px;"><asp:CheckBoxList ID="ckbListLevel4" Visible="false" BackColor="White" DataValueField="Level_4_Desc" DataTextField="Level_4_Desc" runat="server" Width="230px"></asp:CheckBoxList></div></td>
        </tr>
    </table>

    
                <!--style type="text/css"> 
                    .myGrid 
                    {
                    	background-color: #fff;              
                    	margin: 5px 0 10px 0;              
                    	border: solid 1px #525252;              
                    	border-collapse:collapse; 
                    	         
                    }         
                    
                    .myGrid td               
                    {
                    	padding: 2px;              
                    	border: solid 1px #c1c1c1;              
                    	color: #717171;
                    	height:60px; 
                    	font-size:8pt;         
                    }         
                    
                    .myGrid th 
                    {              
                    	padding: 4px 2px;              
                    	color: #fff;              
                    	background-color: #424242;              
                    	border-left: solid 1px #525252;              
                    	font-size:9pt;         
                    	width:66px;
                    	height:75px; 
                    	         
                    }         
                    
                    .myGrid .alt 
                    {             
                    	background-color: #EFEFEF;         
                    } 
                    
                </style-->
                
                <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script> 

                                   
                    <script type="text/javascript">

                        $(function () {
                            //declare function 
                            $.fn.scrollfunction = function () {

                                //                            alert('In scrollfunction');

/* Scroll Off*/ //                               var tabOrig = $("#<%=gvMaterial.ClientID%>").clone(true);

                                // here clone our gridview first         
                                var tab = $("#<%=gvMaterial.ClientID%>").clone(true);
                                // clone again for freeze         
                                var tabFreeze = $("#<%=gvMaterial.ClientID%>").clone(true);

                                // set width (for scroll)         
                                var totalWidth = $("#<%=gvMaterial.ClientID%>").outerWidth();
                                //var firstColWidth = $("#<%=gvMaterial.ClientID%> th:first-child").outerWidth();
                                
//                                var firstColWidth = $("#<%=gvMaterial.ClientID%> th:lt(4)").outerWidth()
//                                firstColWidth += $("#<%=gvMaterial.ClientID%> th:first-child").next().outerWidth()
//                                firstColWidth += $("#<%=gvMaterial.ClientID%> th:first-child").next().next().outerWidth()
//                                firstColWidth += $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().outerWidth()
//                                firstColWidth += $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().next().outerWidth();
//                                firstColWidth += $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().next().next().outerWidth();
//                                firstColWidth += $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().next().next().outerWidth();
//                                //                            var firstColWidth = $("#<%=gvMaterial.ClientID%> td:lt(2)").outerWidth();
                                var firstColWidth = 417;
                                //alert(firstColWidth.toString())

/* Scroll Off*/ //                               tabOrig.width(1);
                                tabFreeze.width(firstColWidth);
                                tab.width(totalWidth - firstColWidth);

                                var totalHeight = $("#<%=gvMaterial.ClientID%>").outerHeight() + 17;
                                //  alert('firstColWidth: ' + firstColWidth.toString())

                                // here make 2 table 1 for freeze column 2 for all remain column          
                                tabFreeze.find("th:gt(6)").remove();
                                tabFreeze.find("td:not(:nth-child(-n+7))").remove();
                                //                            tabFreeze.find("td:not(:nth-child(1))").remove();
                                tab.find("th:lt(7)").remove();
                                tab.find("td::nth-child(-n+7)").remove();

                                tab.find("th:first-child").height(45);
                                tabFreeze.find("th:first-child").height(45);
                                tab.find("td:first-child").height(60);
                                tabFreeze.find("td:first-child").height(60);

                                // create a container for these 2 table and make 2nd table scrollable           

                                var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:1000px; height: ' + totalHeight.toString() + '; overflow-x:auto; overflow-y:hidden"></div></td></tr></table)');
/* Scroll Off*/  //                              var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" colspan="2"><div id="OrigCol" style="display:none"></div></td></tr><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:1000px; overflow:auto"></div></td></tr></table)');
/* Scroll Off*/  //                              $("#OrigCol", container).html($(tabOrig));
                                $("#FCol", container).html($(tabFreeze));
                                $("#Col", container).html($(tab));
                                // clear all html         
                                $("#gridContainer").html('');
                                $("#gridContainer").append(container);


                                $("#wideShell").css("width", "1600px");

                                return true;
                            };


                            $.fn.scrollOffFunction = function () {
                                //                    alert('In scrollOfffunction');

                                // here clone our gridview first         
                                var tab = $("#<%=gvMaterial.ClientID%>").clone(true);
                                tab.width(2764);
                                $("#wideShell").css("width", "2765px");

                                //      var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:1000px; overflow:auto"></div></td></tr></table)');
                                var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="Col" style="width:2700px; overflow:off"></div></td></tr></table)');
                                //   $("#FCol", container).html($(tabFreeze));
                                $("#Col", container).html($(tab));
                                // clear all html         
                                $("#gridContainer").html('');
                                $("#gridContainer").append(container);

                                return true;
                            };


                        });

                        $(document).ready(function () {

                         //   alert('In Ready Function');
                            $("#wideShell").scrollfunction();
/*
                            // here clone our gridview first         
                            var tab = $("#<%=gvMaterial.ClientID%>").clone(true);
                            // clone again for freeze         
                            var tabFreeze = $("#<%=gvMaterial.ClientID%>").clone(true);

                            // set width (for scroll)         
                            var totalWidth = $("#<%=gvMaterial.ClientID%>").outerWidth();
                            //var firstColWidth = $("#<%=gvMaterial.ClientID%> th:first-child").outerWidth();
                            var firstColWidth = $("#<%=gvMaterial.ClientID%> th:lt(4)").outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().outerWidth() + $("#<%=gvMaterial.ClientID%> th:first-child").next().next().next().next().outerWidth();
                            //                            var firstColWidth = $("#<%=gvMaterial.ClientID%> td:lt(2)").outerWidth();

                            tabFreeze.width(firstColWidth);
                            tab.width(totalWidth - firstColWidth);

                            var totalHeight = $("#<%=gvMaterial.ClientID%>").outerHeight() + 35;
                            //  alert('firstColWidth: ' + firstColWidth.toString())

                            // here make 2 table 1 for freeze column 2 for all remain column          
                            tabFreeze.find("th:gt(5)").remove();
                            tabFreeze.find("td:not(:nth-child(-n+6))").remove();
                            //                            tabFreeze.find("td:not(:nth-child(1))").remove();
                            tab.find("th:lt(6)").remove();
                            tab.find("td::nth-child(-n+6)").remove();

                            //                            tabFreeze.find("th:gt(0)").remove();

                            //                            tabFreeze.find("td:not(:first-child)").remove();
                            //                            tab.find("th:first-child").remove();
                            //                            tab.find("td:first-child").remove();          

                            tab.find("th:first-child").height(45);
                            tabFreeze.find("th:first-child").height(45);
                            tab.find("td:first-child").height(60);
                            tabFreeze.find("td:first-child").height(60);
                            //                            tab.find("td:first-child").remove();          

                            // create a container for these 2 table and make 2nd table scrollable           

                            //      var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:1000px; height: ' + totalHeight.toString() + '; overflow:auto"></div></td></tr></table)');
                            var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:1000px; overflow:auto"></div></td></tr></table)');
                            $("#FCol", container).html($(tabFreeze));
                            $("#Col", container).html($(tab));
                            // clear all html         
                            $("#gridContainer").html('');
                            $("#gridContainer").append(container);
*/
                        });

                        </script>

                      <!--

                        $("#<XX=btnScroll.ClientID%>").toggle(function () {
                       //     alert('No Bunck');
                            $(this).val('Scroll On');
                            $("#wideShell").scrollOffFunction();
                        }, function () {
                        //    alert('Bunck');
                            $(this).val('Scroll Off');
                            $("#wideShell").scrollfunction();
                        });


                        $("#<XX=btnScroll.ClientID%>").click(function () {
                            alert('btnScroll Function');


                            $("#wideShell").scrollOffFunction();

                            //  $("#success").slideToggle("fast");
                        });
-->

</asp:Content>
