﻿<%@ Page Title="Price Request Search" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PriceRequestSearch.aspx.cs" Inherits="SalesRequestForms.PricingRequest.PriceRequestSearch" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width:100%">
        <tr><asp:HiddenField ID="hdnProgramID" Value="2" runat="server" />
            <td style="width:12%">Price Request ID:</td>
            <td style="width:25%"><asp:TextBox ID="txtPriceRequestID" runat="server"></asp:TextBox></td>
            <td style="width:12%">Customer Name:</td>
            <td><asp:TextBox ID="txtCustomerName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Material ID:</td>
            <td><asp:TextBox ID="txtMaterialID" runat="server" MaxLength="18"></asp:TextBox></td>
            <td>Customer ID:</td>
            <td><asp:TextBox ID="txtCustomerID" runat="server" MaxLength="10"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Sales Rep Name:</td>
            <td><asp:TextBox ID="txtSalesRepName" runat="server"></asp:TextBox></td>
            <td>Sales Rep ID:</td>
            <td><asp:TextBox ID="txtSalesRepID" runat="server" MaxLength="10"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Sales District Name:</td>
            <td><asp:TextBox ID="txtSalesDistrictName" runat="server"></asp:TextBox></td>
            <td>Sales District ID:</td>
            <td><asp:TextBox ID="txtSalesDistrictID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Status:</td>
            <td><asp:DropDownList ID="ddlStatus" runat="server">
                <asp:ListItem>All</asp:ListItem>
                <asp:ListItem>Submitted</asp:ListItem>
                <asp:ListItem>In Progress</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>Submit Date:</td>
            <td><asp:TextBox ID="txtDateFrom" runat="server"></asp:TextBox> to <asp:TextBox ID="txtDateTo" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <uc2:waitbutton id="btnSearch" Width="125px"  runat="server" CausesValidation="False" 
                     CssClass="buttonsSubmitBorder" Text="Search"
				        WaitText="Processing..." onclick="btnSearch_Click"></uc2:waitbutton>
                <asp:HiddenField ID="hdnSortDirection" runat="server" Value="descending" />
                <asp:HiddenField ID="hdnSortField" runat="server" Value="PricingRequestID" />
            </td>
            <td colspan="2" style="text-align:center">
                <uc2:waitbutton id="btnReset" Width="125px"  runat="server" CausesValidation="False" 
                     CssClass="buttonsSubmitBorder" Text="Reset"
				        WaitText="Processing..." onclick="btnReset_Click"></uc2:waitbutton>
            </td>
        </tr>
    </table>
    <table style="width:100%">
        <tr>
            <td>
                <asp:GridView ID="gvSearch" runat="server" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="No matching records found" OnRowDeleting="GV_View"  ShowFooter="false" 
                     CellPadding="2" DataKeyNames="PricingRequestID, PricingRequestStatus, CreatedBy" GridLines="Both" OnRowCancelingEdit="GV_Copy"
                     OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" AllowPaging="true" PageSize="20"
                      AllowSorting="true" OnSorting="GV_Sorting">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnView" Width="40px" Height="18" runat="server" Text="View" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <uc2:waitbutton id="btnCopy" Width="45px"  Height="18"  runat="server" Text="Copy" CommandName="Cancel"
				                    WaitText="Wait" Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></uc2:waitbutton>                                 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:BoundField DataField="PricingRequestID" SortExpression="PricingRequestID" HeaderStyle-Width="5%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="ID" />                       
                        <asp:BoundField DataField="SoldToCustomerName" SortExpression="SoldToCustomerName" HeaderStyle-Width="24%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer Name" />                       
                        <asp:BoundField DataField="SalesRepID" SortExpression="SalesRepID" HeaderStyle-Width="8%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Sales Rep ID" />                       
                        <asp:BoundField DataField="SalesRepName" SortExpression="SalesRepName" HeaderStyle-Width="24%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Sales Rep Name" />                       
                        <asp:BoundField DataField="DistrictName" SortExpression="DistrictName" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="District Name" />                       
                        <asp:BoundField DataField="ModifiedDate" SortExpression="ModifiedDate" HeaderStyle-Width="9%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Modified Date" />                       
                        <asp:BoundField DataField="PricingRequestStatus" SortExpression="PricingRequestStatus" HeaderStyle-Width="9%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Status" />                       
                   </Columns>
                </asp:GridView> 
            </td>
        </tr>
    </table>
</asp:Content>
