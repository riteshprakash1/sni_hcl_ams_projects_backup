﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SalesRequestForms.Classes;
using SalesRequestForms.PricingRequest.Classes;

namespace SalesRequestForms.PricingRequest
{
    public partial class TestFreeze : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindMaterials(8911);
        }


        /// <summary>Binds the Materials table to the gvMaterial GridView.</summary>
        /// <param name="intPricingRequestID">The PricingRequestID.</param>
        public void BindMaterials(int intPricingRequestID)
        {
            List<PricingRequestMaterialEntity> materials = PricingRequestMaterialEntity.GetPricingRequestMaterialByPricingRequestID(intPricingRequestID);

            Debug.WriteLine("materials count: " + materials.Count.ToString());
            this.DataGrid1.DataSource = materials;
            this.DataGrid1.DataBind();

            Debug.WriteLine("BindMaterials");
            //this.DataGrid1.DataSource = materials;
            //this.DataGrid1.DataBind();

            //PricingRequestMaterialEntity.HighLightGridViewColumns(this.gvMaterial);
            //PricingRequestMaterialEntity.HideGridViewColumns(this.gvMaterial, Request);
        }


        /// <summary> Handles the RowDataBound event of the gvCosts GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Debug.WriteLine("GV_RowDataBound");
            e.Row.Cells[0].CssClass = "locked";
            e.Row.Cells[1].CssClass = "locked";
        }

        /// <summary> Handles the RowDataBound event of the gvCosts GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void DG_RowDataBound(object sender, DataGridItemEventArgs e)
        {
            Debug.WriteLine("DG_RowDataBound");
            e.Item.Cells[0].CssClass = "locked";
            e.Item.Cells[1].CssClass = "locked";
        }
    }
}