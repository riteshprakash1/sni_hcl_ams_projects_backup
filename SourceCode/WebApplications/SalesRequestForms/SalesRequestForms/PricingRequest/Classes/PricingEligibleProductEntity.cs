﻿// ------------------------------------------------------------------
// <copyright file="PricingEligibleProductEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using SalesRequestForms.Classes;
    
    /// <summary>This is the PricingEligibleProduct Entity.</summary>
    public class PricingEligibleProductEntity
    {
        /// <summary>This is the ProductID.</summary>
        private string strProductID;

        /// <summary>This is the ProductLevel.</summary>
        private string strProductLevel;

        /// <summary>This is the Product Desc.</summary>
        private string strProductDesc;

        /// <summary>Initializes a new instance of the <see cref="PricingEligibleProductEntity"/> class.</summary>
        public PricingEligibleProductEntity()
        {
            this.ProductID = String.Empty;
            this.ProductLevel = String.Empty;
            this.ProductDesc = String.Empty;
        }

        /// <summary>Gets or sets the Product ID.</summary>
        /// <value>The Product ID.</value>
        public string ProductID
        {
            get { return this.strProductID; }

            set { this.strProductID = value; }
        }

        /// <summary>Gets or sets the Product Level.</summary>
        /// <value>The Product Level.</value>
        public string ProductLevel
        {
            get { return this.strProductLevel; }

            set { this.strProductLevel = value; }
        }

        /// <summary>Gets or sets the Product Desc.</summary>
        /// <value>The Product Desc.</value>
        public string ProductDesc
        {
            get { return this.strProductDesc; }

            set { this.strProductDesc = value; }
        }

        /// <summary>Inserts a new record into the PricingEligibleProduct table with the information in the PricingEligibleProductEntity.</summary>
        /// <param name="mat">The PricingEligibleProductEntity instance</param>
        /// <returns>int number of records created</returns>
        public static int InsertPricingEligibleProduct(PricingEligibleProductEntity prod)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@ProductID", prod.ProductID));
            myParamters.Add(new SqlParameter("@ProductDesc", prod.strProductDesc));
            myParamters.Add(new SqlParameter("@ProductLevel", prod.ProductLevel));

            int recordsCreated = SQLUtility.SqlExecuteNonQueryCount("sp_InsertPricingEligibleProducts", myParamters);

            return recordsCreated;
        }

        /// <summary>Deletes a PricingEligibleProduct record by the productID.</summary>
        /// <param name="strProductID">The Product ID</param>
        /// <returns>int number of records deleted</returns>
        public static int DeletePricingEligibleProduct(string strProductID)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@ProductID", strProductID));

            int recordsDeleted = SQLUtility.SqlExecuteNonQueryCount("sp_DeletePricingEligibleProductsByProductID", myParamters);

            return recordsDeleted;
        }

        /// <summary>Sets the Entity object from a datarow</summary>
        /// <param name="r">The Data Row</param>
        /// <returns>The newly populated Entity</returns>
        protected static PricingEligibleProductEntity GetEntityFromDataRow(DataRow r)
        {
            PricingEligibleProductEntity prod = new PricingEligibleProductEntity();

            prod.ProductID = r["ProductID"].ToString();
            prod.ProductLevel = r["ProductLevel"].ToString();
            prod.ProductDesc = r["ProductDesc"].ToString();

            return prod;

        }

        /// <summary>Gets the PricingEligibleProductEntity Collection.</summary>
        /// <returns>The newly populated PricingEligibleProductEntity Collection</returns>
        public static Collection<PricingEligibleProductEntity> GetPricingEligibleProducts()
        {
            Collection<PricingEligibleProductEntity> products = new Collection<PricingEligibleProductEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingEligibleProducts", parameters);

            foreach (DataRow r in dt.Rows)
            {
                PricingEligibleProductEntity myProduct = PricingEligibleProductEntity.GetEntityFromDataRow(r);
                products.Add(myProduct);
            }

            return products;
        }

        /// <summary>Gets the PricingEligibleProductEntity by the product ID.</summary>
        /// <param name="strProductID">The product ID</param>
        /// <returns>The newly populated PricingEligibleProductEntity </returns>
        public static PricingEligibleProductEntity GetPricingEligibleProductsByProductID(string strProductID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@ProductID", strProductID));
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingEligibleProductsByProductID", parameters);

            PricingEligibleProductEntity myProduct = null;
            if (dt.Rows.Count > 0)
            {
                myProduct = PricingEligibleProductEntity.GetEntityFromDataRow(dt.Rows[0]);
            }

            return myProduct;
        }

        /// <summary>Gets the Valid PricingEligibleProduct Entity for the Product Level and Product ID.</summary>
        /// <param name="strProductID">The Product ID</param>
        /// <param name="strProductLevel">The Product Level</param>
        /// <returns>The newly populated Entity</returns>
        public static PricingEligibleProductEntity GetValidPricingEligibleProductByProductID(string strProductID, string strProductLevel)
        {
            string sQuery = string.Empty;
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@QueryName", "PricingEligibleMaterialByID"));

            // retrieve the GDW query to execute
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetQueryByQueryName", parameters);
            //            if (dt.Rows.Count > 0)
            //                sQuery = dt.Rows[0]["Query"].ToString() + " AND sd." + strProductLevel + "_ID = '" + strProductID + "'";

            if (dt.Rows.Count > 0)
            {
                if (strProductLevel.ToLower() == "material")
                    sQuery = dt.Rows[0]["Query"].ToString() + " AND MATERL_NO = '" + strProductID + "'";
                else
                    sQuery = dt.Rows[0]["Query"].ToString() + " AND PRODUCT_HIERRCHY_" + strProductLevel.ToUpper() + "_CD = '" + strProductID + "'";
            }


            Debug.WriteLine("Product Query: " + sQuery);

            // execute the GDW query
//            DataTable dtGDW = SQLUtility.SqlExecuteDynamicQuery(sQuery, SQLUtility.OpenGdwSqlConnection());
            DataTable dtGDW = SQLUtility.OracleExecuteDynamicQuery(sQuery);

            if (dtGDW.Rows.Count > 0)
            {
                PricingEligibleProductEntity prod = new PricingEligibleProductEntity();

                prod.ProductID = strProductID;
                prod.ProductLevel = strProductLevel;
                prod.ProductDesc = dtGDW.Rows[0][strProductLevel + "_Desc"].ToString();

                return prod;
            }
            else
                return null;

        }


    } //// end class
}