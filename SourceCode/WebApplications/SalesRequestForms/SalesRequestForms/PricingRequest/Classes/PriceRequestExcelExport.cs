﻿// ------------------------------------------------------------------
// <copyright file="PriceRequestExcelExport.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Web;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using DocumentFormat.OpenXml.Extensions;
    using SalesRequestForms.Classes;
    
    public class PriceRequestExcelExport
    {

        /// <summary>Initializes a new instance of the PriceRequestExcelExport class.</summary>
        public PriceRequestExcelExport()
        {
            
        }

        /// <summary>Copies the Template File to a new File and loads the data returning the new file name</summary>
        /// <param name="intPricingRequestID">The PricingRequest ID.</param>
        /// <param name="strQualifiedFileName">The fully Qualified FileName.</param>
        /// <param name="strUserMaterialStoredProcedure">the stored procedure to use based on the user role</param>
        /// <param name="strTemplateFile">the name of the Excel Template  File</param>
        public static void BuildFile(int intPricingRequestID, string strQualifiedFileName, string strUserMaterialStoredProcedure, string strTemplateFile)
        {
            Debug.WriteLine("strUserMaterialStoredProcedure: " + strUserMaterialStoredProcedure);
            Debug.WriteLine("strTemplateFile: " + strTemplateFile);
            Debug.WriteLine("strQualifiedFileName: " + strQualifiedFileName);
            
            strTemplateFile = ConfigurationManager.AppSettings["TemplateFolder"] + strTemplateFile;

            File.Copy(strTemplateFile, strQualifiedFileName, true);

            //populate the data into the spreadsheet
            using (SpreadsheetDocument package = SpreadsheetDocument.Open(strQualifiedFileName, true))
            {
                LoadMaterials(package, intPricingRequestID, strUserMaterialStoredProcedure);
                LoadPriceRequestTab(package, intPricingRequestID);

                package.Close();
            }
            
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
            //Response.TransmitFile(strQualifiedFileName);
            //Response.End();

        }


        /// <summary>Loads the data to the Pricing Request Header List tab</summary>
        /// <param name="intPricingRequestID">The PricingRequest ID.</param>
        /// <param name="package">the SpreadsheetDocument called the package</param>
        protected static void LoadPriceRequestTab(SpreadsheetDocument package, int intPricingRequestID)
        {
            WorkbookPart workbookpart = package.WorkbookPart;
            //create a reference to Sheet1

            PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);

            WorksheetPart headerWorksheetPart = SpreadsheetReader.GetWorksheetPartByName(package, "Pricing Request Header List");

            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(1), pr.PricingRequestID.ToString(), "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(2), pr.PricingRequestStatus, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(3), pr.SubmittedByName, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(4), pr.ModifiedDate, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(5), pr.SoldToCustomerID, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(6), pr.SalesRepName, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(7), pr.RvpName, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(8), pr.DistrictName, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(9), pr.SoldToCustomerName, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(10), pr.SoldToStreet, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(11), pr.SoldToCity + ", " + pr.SoldToStateCode + " " + pr.SoldToPostalCode, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(12), pr.GpoDesc, "System.String");

            string strCustGroup1 = string.Empty;
            if (pr.CustGroup1ID != string.Empty)
            {
                strCustGroup1 = pr.CustGroup1 + " - " + pr.CustGroup1ID;
            }

            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(13), strCustGroup1, "System.String");
            
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(14), pr.AgreementTerms, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(15), pr.Competitor, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(16), pr.BusinessJustification, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(17), pr.IncrementalRevenueEstimate, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(18), pr.IncrementalRevenueProbability, "System.String");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(21), pr.Request12RollingRev.ToString(), "System.Double");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(22), pr.RequestToDateRev.ToString(), "System.Double");
            PasteValue(package, headerWorksheetPart, "B", Convert.ToUInt32(23), pr.ToDateUpdatedOn, "System.String");

            // Load Additional Customers
            Collection<PricingCustomerEntity> myCustomers = PricingCustomerEntity.GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID(intPricingRequestID, 1);

            int currentCustRow = 3;
            foreach (PricingCustomerEntity cust in myCustomers)
            {
                PasteValue(package, headerWorksheetPart, "D", Convert.ToUInt32(currentCustRow), cust.CustomerID, "System.String");
                PasteValue(package, headerWorksheetPart, "E", Convert.ToUInt32(currentCustRow), cust.CustomerName, "System.String");
                currentCustRow++;
            }

            headerWorksheetPart.Worksheet.Save();
            workbookpart.Workbook.Save();

        }

        /// <summary>Loads the data to the Material List tab</summary>
        /// <param name="intPricingRequestID">The PricingRequest ID.</param>
        /// <param name="package">the SpreadsheetDocument called the package</param>
        /// <param name="strUserStoredProcedure">the stored procedure to use based on the user role</param>
        protected static void LoadMaterials(SpreadsheetDocument package, int intPricingRequestID, string strUserStoredProcedure)
        {
            WorkbookPart workbookpart = package.WorkbookPart;
            //create a reference to Sheet1

            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));
            System.Data.DataTable dtMaterials = SQLUtility.SqlExecuteQuery(strUserStoredProcedure, myParameters);

            WorksheetPart materialPart = SpreadsheetReader.GetWorksheetPartByName(package, "Material List");

            int currentRow = 0;

            for (int i = 0; i < dtMaterials.Rows.Count; i++)
            {
                currentRow = i + 2;

                for (int col = 0; col < dtMaterials.Columns.Count; col++)
                {
                    int currentColumn = col + 1;
                    string strColName = GetColumnAlphaChar(currentColumn);

                    string strTableValue = dtMaterials.Rows[i][col].ToString();
                    string strType = dtMaterials.Rows[i][col].GetType().ToString();
                    PasteValue(package, materialPart, strColName, Convert.ToUInt32(currentRow), strTableValue, strType);
                }
            }

            int intEndIndex = 2500;

            for (int intStartIndex = currentRow + 1; intStartIndex <= intEndIndex; intStartIndex++)
            {
                Row refRow1 = materialPart.Worksheet.Descendants<Row>().Where(r => r.RowIndex == intStartIndex).First();
                refRow1.Remove();
            }

            materialPart.Worksheet.Save();
            workbookpart.Workbook.Save();

        }

        /// <summary>
        /// Given a column name, a row index, and a WorksheetPart, inserts a cell into the worksheet. 
        /// If the cell already exists, returns it.
        ///</summary>
        /// <param name="columnName">The column Name</param>
        /// <param name="rowIndex">The row Index.</param>
        /// <param name="worksheetPart">The WorksheetPart.</param>
        /// <returns>the cell</returns>
        protected static Cell FindCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }

        ///<summary>Given a spreadsheet reference and text, writes any value to the row and column specified.</summary>
        /// <param name="spreadsheet">The SpreadsheetDocument</param>
        /// <param name="worksheetPart">The WorksheetPart.</param>
        /// <param name="column">The column Name</param>
        /// <param name="row">The row Index.</param>
        /// <param name="value">The value to put in the cell</param>
        /// <param name="strValueType">The Value Type</param>
        /// <returns>the cell</returns>
        protected static Cell PasteValue(SpreadsheetDocument spreadsheet, WorksheetPart worksheetPart, string column, uint row, string value, string strValueType)
        {
            //Get the cell, or insert a new one
            Cell cell = FindCellInWorksheet(column, row, worksheetPart);

            CellValues type = CellValues.String;

            if (strValueType != "System.String")
                type = CellValues.Number;

            //If shared text then get the SharedStringTablePart. 
            //Create one in Excel by adding text, saving, then removing the text again.
            if (type == CellValues.SharedString)
            {
                SharedStringTablePart shareStringPart = spreadsheet.WorkbookPart.SharedStringTablePart;
                if (shareStringPart == null) throw new ApplicationException("Template does not contain a shared string table.");

                // Insert the text into the SharedStringTablePart.
                uint index = SpreadsheetWriter.GetSharedStringItem(value, shareStringPart);

                // Set the value of cell.
                cell.CellValue = new CellValue(index.ToString());
                cell.DataType = new DocumentFormat.OpenXml.EnumValue<CellValues>(CellValues.SharedString);
            }
            else
            {
                cell.CellValue = new CellValue(value);
                cell.DataType = new DocumentFormat.OpenXml.EnumValue<CellValues>(type);
                //Debug.WriteLine("NOT CellValues: " + CellValues.SharedString.ToString());
            }

            return cell;
        }

        ///<summary>Gets the alpha representation of the column index number</summary>
        /// <param name="columnIndex">The column Index</param>
        /// <returns>the column alpha representation</returns>
        protected static string GetColumnAlphaChar(int columnIndex)
        {
            int dividend = columnIndex;
            string columnName = String.Empty;
            int modifier;

            while (dividend > 0)
            {
                modifier = (dividend - 1) % 26;
                columnName =
                    Convert.ToChar(65 + modifier).ToString() + columnName;
                dividend = (int)((dividend - modifier) / 26);
            }

            return columnName;
        }
    } //// end class
}