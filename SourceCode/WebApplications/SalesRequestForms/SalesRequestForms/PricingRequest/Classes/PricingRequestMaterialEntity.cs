﻿// ------------------------------------------------------------------
// <copyright file="PricingRequestMaterialEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using SalesRequestForms.Classes;

    /// <summary>This is the Price Request Material Entity.</summary>
    public class PricingRequestMaterialEntity
    {
        /// <summary> This is the PricingRequestMaterialID</summary> 
        private int intPricingRequestMaterialID;

        /// <summary> This is the PricingRequestID</summary> 
        private int intPricingRequestID;

        /// <summary> This is the Material_Id</summary> 
        private string strMaterial_Id;

        /// <summary> This is the Material_Desc</summary> 
        private string strMaterial_Desc;

        /// <summary> This is the Marketing_Manager</summary> 
        private string strMarketing_Manager;

        /// <summary> This is the Level_4_Desc</summary> 
        private string strLevel_4_Desc;

        /// <summary> This is the Level_5_Desc</summary> 
        private string strLevel_5_Desc;

        /// <summary> This is the Level_6_Desc</summary> 
        private string strLevel_6_Desc;

        /// <summary> This is the Requested_Price</summary> 
        private double dblRequested_Price;

        /// <summary> This is the Expected_Volume</summary> 
        private double dblExpected_Volume;

        /// <summary> This is the Requested_Discount</summary> 
        private double dblRequested_Discount;

        /// <summary> This is the Requested_GM</summary> 
        private double dblRequested_GM;

        /// <summary> This is the Competitor_Price</summary> 
        private double dblCompetitor_Price;

        /// <summary> This is the Rolling_Revenue</summary> 
        private double dblRolling_Revenue;

        /// <summary> This is the List_Price</summary> 
        private double dblList_Price;

        /// <summary> This is the Local_Price</summary> 
        private double dblLocal_Price;

        /// <summary> This is the GPO_Price</summary> 
        private double dblGPO_Price;

        /// <summary> This is the CG1_Price</summary> 
        private double dblCG1_Price;

        /// <summary> This is the Net_Price</summary> 
        private double dblNet_Price;

        /// <summary> This is the State_ASP</summary> 
        private double dblState_ASP;

        /// <summary> This is the US_ASP</summary> 
        private double dblUS_ASP;

        /// <summary> This is the Standard_Cost</summary> 
        private double dblStandard_Cost;

        /// <summary> This is the Cost of Good Sold</summary> 
        private double dblCOG;

        /// <summary> This is the Sellable_UOM</summary> 
        private string strSellable_UOM;

        /// <summary> This is the Qty_Per_Sellable_UOM</summary> 
        private double dblQty_Per_Sellable_UOM;

        /// <summary> This is the Internal_Floor_Price</summary> 
        private double dblInternal_Floor_Price;

        /// <summary> This is the HPG_Price</summary> 
        private double dblHPG_Price;

        /// <summary> This is the MedAssets_T1_Price</summary> 
        private double dblMedAssets_T1_Price;

        /// <summary> This is the MedAssets_T2_Price</summary> 
        private double dblMedAssets_T2_Price;

        /// <summary> This is the Premier_Price</summary> 
        private double dblPremier_Price;

        /// <summary> This is the Novation_Participant_Price</summary> 
        private double dblNovation_Participant_Price;

        /// <summary> This is the Novation_Committed_Price</summary> 
        private double dblNovation_Committed_Price;

        /// <summary> This is the Rep_Discount</summary> 
        private double dblRep_Discount;

        /// <summary> This is the Rep_Price</summary> 
        private double dblRep_Price;

        /// <summary> This is the DE_Discount</summary> 
        private double dblDE_Discount;

        /// <summary> This is the DE_Price</summary> 
        private double dblDE_Price;

        /// <summary> This is the RVP_Discount</summary> 
        private double dblRVP_Discount;

        /// <summary> This is the RVP_Price</summary> 
        private double dblRVP_Price;

        /// <summary> This is the Approval_Required_From</summary> 
        private string strApproval_Required_From;

        /// <summary> This is the Recommendation_Approval</summary> 
        private string strRecommendation_Approval;

        /// <summary> This is the CG1_ValidFrom</summary> 
        private string strCG1_ValidFrom;

        /// <summary> This is the CG1_ValidTo</summary> 
        private string strCG1_ValidTo;

        /// <summary> This is the Local_ValidFrom</summary> 
        private string strLocal_ValidFrom;

        /// <summary> This is the Local_ValidTo</summary> 
        private string strLocal_ValidTo;

        /// <summary> This is the GPO_SalesDeal</summary> 
        private string strGPO_SalesDeal;

        /// <summary> This is the CG1_SalesDeal</summary> 
        private string strCG1_SalesDeal;

        /// <summary> This is the Local_SalesDeal</summary> 
        private string strLocal_SalesDeal;

        /// <summary>Initializes a new instance of the <see cref="PricingRequestMaterialEntity"/> class.</summary>
        public PricingRequestMaterialEntity()
        {
            this.Material_Id = string.Empty;
            this.Material_Desc = string.Empty;
            this.Level_4_Desc = string.Empty;
            this.Level_5_Desc = string.Empty;
            this.Level_6_Desc = string.Empty;
            this.Sellable_UOM = string.Empty;
            this.Marketing_Manager = string.Empty;
            this.Approval_Required_From = string.Empty;
            this.Recommendation_Approval = string.Empty;
        }

        /// <summary>Initializes a new instance of the <see cref="PricingRequestMaterialEntity"/> class.</summary>
        public PricingRequestMaterialEntity(string strMaterialID)
        {
            this.Material_Id = strMaterialID;
            this.Material_Desc = string.Empty;
            this.Level_4_Desc = string.Empty;
            this.Level_5_Desc = string.Empty;
            this.Level_6_Desc = string.Empty;
            this.Sellable_UOM = string.Empty;
            this.Marketing_Manager = string.Empty;
            this.Approval_Required_From = string.Empty;
            this.Recommendation_Approval = string.Empty;
            this.Local_ValidTo = string.Empty;
            this.Local_ValidFrom = string.Empty;
            this.CG1_ValidFrom = string.Empty;
            this.CG1_ValidTo = string.Empty;
            this.Local_SalesDeal = string.Empty;
            this.GPO_SalesDeal = string.Empty;
            this.CG1_SalesDeal = string.Empty;
        }

        /// <summary>Gets or sets the PricingRequestMaterialID.</summary>
        /// <value>The PricingRequestMaterialID.<value> 
        public int PricingRequestMaterialID { get { return this.intPricingRequestMaterialID; } set { this.intPricingRequestMaterialID = value; } }

        /// <summary>Gets or sets the PricingRequestID.</summary>
        /// <value>The PricingRequestID.<value> 
        public int PricingRequestID { get { return this.intPricingRequestID; } set { this.intPricingRequestID = value; } }
        
        /// <summary>Gets or sets the Material_Id.</summary>
        /// <value>The Material_Id.<value> 
        public string Material_Id { get { return this.strMaterial_Id; } set { this.strMaterial_Id = value; } }

        /// <summary>Gets or sets the Material_Desc.</summary>
        /// <value>The Material_Desc.<value> 
        public string Material_Desc { get { return this.strMaterial_Desc; } set { this.strMaterial_Desc = value; } }

        /// <summary>Gets or sets the Marketing_Manager.</summary>
        /// <value>TheMarketing_Manager.<value> 
        public string Marketing_Manager { get { return this.strMarketing_Manager; } set { this.strMarketing_Manager = value; } }

        /// <summary>Gets or sets the Level_4_Desc.</summary>
        /// <value>The Level_4_Desc.<value> 
        public string Level_4_Desc { get { return this.strLevel_4_Desc; } set { this.strLevel_4_Desc = value; } }

        /// <summary>Gets or sets the Level_5_Desc.</summary>
        /// <value>The Level_5_Desc.<value> 
        public string Level_5_Desc { get { return this.strLevel_5_Desc; } set { this.strLevel_5_Desc = value; } }

        /// <summary>Gets or sets the Level_6_Desc.</summary>
        /// <value>The Level_6_Desc.<value> 
        public string Level_6_Desc { get { return this.strLevel_6_Desc; } set { this.strLevel_6_Desc = value; } }

        /// <summary>Gets or sets the Requested_Price.</summary>
        /// <value>The Requested_Price.<value> 
        public double Requested_Price { get { return this.dblRequested_Price; } set { this.dblRequested_Price = value; } }

        /// <summary>Gets or sets the Expected_Volume.</summary>
        /// <value>The Expected_Volume.<value> 
        public double Expected_Volume { get { return this.dblExpected_Volume; } set { this.dblExpected_Volume = value; } }

        /// <summary>Gets or sets the Requested_Discount.</summary>
        /// <value>The Requested_Discount.<value> 
        public double Requested_Discount { get { return this.dblRequested_Discount; } set { this.dblRequested_Discount = value; } }

        /// <summary>Gets or sets the Requested_GM.</summary>
        /// <value>The Requested_GM.<value> 
        public double Requested_GM { get { return this.dblRequested_GM; } set { this.dblRequested_GM = value; } }

        /// <summary>Gets or sets the Competitor_Price.</summary>
        /// <value>The Competitor_Price.<value> 
        public double Competitor_Price { get { return this.dblCompetitor_Price; } set { this.dblCompetitor_Price = value; } }

        /// <summary>Gets or sets the Rolling_Revenue.</summary>
        /// <value>The Rolling_Revenue.<value> 
        public double Rolling_Revenue { get { return this.dblRolling_Revenue; } set { this.dblRolling_Revenue = value; } }

        /// <summary>Gets or sets the List_Price.</summary>
        /// <value>The List_Price.<value> 
        public double List_Price { get { return this.dblList_Price; } set { this.dblList_Price = value; } }

        /// <summary>Gets or sets the Local_Price.</summary>
        /// <value>The Local_Price.<value> 
        public double Local_Price { get { return this.dblLocal_Price; } set { this.dblLocal_Price = value; } }

        /// <summary>Gets or sets the GPO_Price.</summary>
        /// <value>The GPO_Price.<value> 
        public double GPO_Price { get { return this.dblGPO_Price; } set { this.dblGPO_Price = value; } }

        /// <summary>Gets or sets the CG1_Price.</summary>
        /// <value>The CG1_Price.<value> 
        public double CG1_Price { get { return this.dblCG1_Price; } set { this.dblCG1_Price = value; } }

        /// <summary>Gets or sets the Net_Price.</summary>
        /// <value>The Net_Price.<value> 
        public double Net_Price { get { return this.dblNet_Price; } set { this.dblNet_Price = value; } }

        /// <summary>Gets or sets the State_ASP.</summary>
        /// <value>The State_ASP.<value> 
        public double State_ASP { get { return this.dblState_ASP; } set { this.dblState_ASP = value; } }

        /// <summary>Gets or sets the US_ASP.</summary>
        /// <value>The US_ASP.<value> 
        public double US_ASP { get { return this.dblUS_ASP; } set { this.dblUS_ASP = value; } }

        /// <summary>Gets or sets the Cost of Goods Sold.</summary>
        /// <value>The COG.<value> 
        public double COG { get { return this.dblCOG; } set { this.dblCOG = value; } }

        /// <summary>Gets or sets the Standard_Cost.</summary>
        /// <value>The Standard_Cost.<value> 
        public double Standard_Cost { get { return this.dblStandard_Cost; } set { this.dblStandard_Cost = value; } }

        /// <summary>Gets or sets the Sellable_UOM.</summary>
        /// <value>The Sellable_UOM.<value> 
        public string Sellable_UOM { get { return this.strSellable_UOM; } set { this.strSellable_UOM = value; } }

        /// <summary>Gets or sets the Qty_Per_Sellable_UOM.</summary>
        /// <value>The Qty_Per_Sellable_UOM.<value> 
        public double Qty_Per_Sellable_UOM { get { return this.dblQty_Per_Sellable_UOM; } set { this.dblQty_Per_Sellable_UOM = value; } }

        /// <summary>Gets or sets the Internal_Floor_Price.</summary
        /// >/// <value>The Internal_Floor_Price.<value> 
        public double Internal_Floor_Price { get { return this.dblInternal_Floor_Price; } set { this.dblInternal_Floor_Price = value; } }

        /// <summary>Gets or sets the HPG_Price.</summary>
        /// <value>The HPG_Price.<value> 
        public double HPG_Price { get { return this.dblHPG_Price; } set { this.dblHPG_Price = value; } }

        /// <summary>Gets or sets the MedAssets_T1_Price.</summary>
        /// <value>The MedAssets_T1_Price.<value> 
        public double MedAssets_T1_Price { get { return this.dblMedAssets_T1_Price; } set { this.dblMedAssets_T1_Price = value; } }

        /// <summary>Gets or sets the MedAssets_T2_Price.</summary>
        /// <value>The MedAssets_T2_Price.<value> 
        public double MedAssets_T2_Price { get { return this.dblMedAssets_T2_Price; } set { this.dblMedAssets_T2_Price = value; } }

        /// <summary>Gets or sets the Premier_Price.</summary>
        /// <value>The Premier_Price.<value> 
        public double Premier_Price { get { return this.dblPremier_Price; } set { this.dblPremier_Price = value; } }

        /// <summary>Gets or sets the Novation_Participant_Price.</summary>
        /// <value>The Novation_Participant_Price.<value> 
        public double Novation_Participant_Price { get { return this.dblNovation_Participant_Price; } set { this.dblNovation_Participant_Price = value; } }

        /// <summary>Gets or sets the Novation_Committed_Price.</summary>
        /// <value>The Novation_Committed_Price.<value> 
        public double Novation_Committed_Price { get { return this.dblNovation_Committed_Price; } set { this.dblNovation_Committed_Price = value; } }

        /// <summary>Gets or sets the Rep_Discount.</summary>
        /// <value>The Rep_Discount.<value> 
        public double Rep_Discount { get { return this.dblRep_Discount; } set { this.dblRep_Discount = value; } }

        /// <summary>Gets or sets the Rep_Price.</summary>
        /// <value>The Rep_Price.<value> 
        public double Rep_Price { get { return this.dblRep_Price; } set { this.dblRep_Price = value; } }

        /// <summary>Gets or sets the DE_Discount.</summary>
        /// <value>The DE_Discount.<value> 
        public double DE_Discount { get { return this.dblDE_Discount; } set { this.dblDE_Discount = value; } }

        /// <summary>Gets or sets the DE_Price.</summary>
        /// <value>The DE_Price.<value> 
        public double DE_Price { get { return this.dblDE_Price; } set { this.dblDE_Price = value; } }

        /// <summary>Gets or sets the RVP_Discount.</summary>
        /// <value>The RVP_Discount.<value> 
        public double RVP_Discount { get { return this.dblRVP_Discount; } set { this.dblRVP_Discount = value; } }

        /// <summary>Gets or sets the RVP_Price.</summary>
        /// <value>The RVP_Price.<value> 
        public double RVP_Price { get { return this.dblRVP_Price; } set { this.dblRVP_Price = value; } }

        /// <summary>Gets or sets the Approval_Required_From.</summary>
        /// <value>The Approval_Required_From.<value> 
        public string Approval_Required_From { get { return this.strApproval_Required_From; } set { this.strApproval_Required_From = value; } }

        /// <summary>Gets or sets the Recommendation_Approval.</summary>
        /// <value>The Recommendation_Approval.<value> 
        public string Recommendation_Approval { get { return this.strRecommendation_Approval; } set { this.strRecommendation_Approval = value; } }

        /// <summary>Gets or sets the CG1_ValidFrom.</summary>
        /// <value>The CG1_ValidFrom.<value> 
        public string CG1_ValidFrom { get { return this.strCG1_ValidFrom; } set { this.strCG1_ValidFrom = value; } }

        /// <summary>Gets or sets the CG1_ValidTo.</summary>
        /// <value>The CG1_ValidTo.<value> 
        public string CG1_ValidTo { get { return this.strCG1_ValidTo; } set { this.strCG1_ValidTo = value; } }

        /// <summary>Gets or sets the Local_ValidFrom.</summary>
        /// <value>The Local_ValidFrom.<value> 
        public string Local_ValidFrom { get { return this.strLocal_ValidFrom; } set { this.strLocal_ValidFrom = value; } }

        /// <summary>Gets or sets the Local_ValidTo.</summary>
        /// <value>The Local_ValidTo.<value> 
        public string Local_ValidTo { get { return this.strLocal_ValidTo; } set { this.strLocal_ValidTo = value; } }

        /// <summary>Gets or sets the Local_SalesDeal.</summary>
        /// <value>The Local_SalesDeal.<value> 
        public string Local_SalesDeal { get { return this.strLocal_SalesDeal; } set { this.strLocal_SalesDeal = value; } }

        /// <summary>Gets or sets the GPO_SalesDeal.</summary>
        /// <value>The GPO_SalesDeal.<value> 
        public string GPO_SalesDeal { get { return this.strGPO_SalesDeal; } set { this.strGPO_SalesDeal = value; } }

        /// <summary>Gets or sets the CG1_SalesDeal.</summary>
        /// <value>The CG1_SalesDeal.<value> 
        public string CG1_SalesDeal { get { return this.strCG1_SalesDeal; } set { this.strCG1_SalesDeal = value; } }

        /// <summary>Gets The PriceRequestMaterial from the database and loads to a datatable</summary>
        /// <returns>a datatable with the PriceRequestMaterial</returns>
        protected static DataTable GetAllPriceRequestMaterialsTable()
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingMaterial", myParamters);

            return dt;
        }

        /// <summary>Gets The PricingRequestMaterialEntity Collection from a DataTable</summary>
        /// <param name="dt">The PriceRequest DataTable.</param>
        /// <returns>a Collection of PricingRequestMaterialEntitys</returns>
        public static Collection<PricingRequestMaterialEntity> GetPricingRequestMaterialEntityCollection(DataTable dt)
        {
            Collection<PricingRequestMaterialEntity> myPriceRequests = new Collection<PricingRequestMaterialEntity>();

            foreach (DataRow row in dt.Rows)
            {
                myPriceRequests.Add(PricingRequestMaterialEntity.GetEntityFromDataRow(row));
            }

            return myPriceRequests;
        }

        /// <summary>Gets The PricingRequestMaterialEntity List from a DataTable</summary>
        /// <param name="dt">The PriceRequest DataTable.</param>
        /// <returns>a List with the PricingRequestMaterialEntity</returns>
        public static List<PricingRequestMaterialEntity> GetPricingRequestMaterialEntityList(DataTable dt)
        {
            List<PricingRequestMaterialEntity> myPriceRequests = new List<PricingRequestMaterialEntity>();

            foreach (DataRow row in dt.Rows)
            {
                myPriceRequests.Add(PricingRequestMaterialEntity.GetEntityFromDataRow(row));
            }

            return myPriceRequests;
        }

        /// <summary>Uses the selected Prodcut Hierarchy items to generate the where clause used in the Dynamic query</summary>
        /// <returns>the Prodcut Hierarchy WhereClause</returns>
        protected static string GetMaterialWhereClause()
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingEligibleProducts", parameters);

            // creates the where clause to be added to the end of the GDW queries
            // this is done to only get the eligible materials from GDW
            string strCurrentLevel = string.Empty;
            string strWhere = string.Empty;
            string strLevel = string.Empty;

            foreach (DataRow r in dt.Rows)
            {
                if (r["ProductLevel"].ToString() != strCurrentLevel)
                {
                    strCurrentLevel = r["ProductLevel"].ToString();

                    if (strCurrentLevel.ToLower() == "material")
                        strLevel = " MATERL_NO ";
                    else
                        strLevel = " PRODUCT_HIERRCHY_" + strCurrentLevel.ToUpper() + "_CD ";

                    if (strWhere == string.Empty)
                        strWhere += " (" + strLevel + " IN (";
                    else
                        strWhere += ") OR " + strLevel + " IN (";
                }
                strWhere += "'" + r["ProductID"].ToString() + "', ";

            }

            strWhere += "))";

            strWhere = strWhere.Replace("', )", "') ");

            Debug.WriteLine("strWhere: " + strWhere);
            Debug.WriteLine("strWhere replace: " + strWhere.Replace("', )", "') "));

            return strWhere;
        }

        /// <summary>Gets The PricingRequestMaterialEntity List populated with MaterialLevel4 items</summary>
        /// <returns>a List with the PricingRequestMaterialEntity</returns>
        public static List<PricingRequestMaterialEntity> GetPriceRequestLevel4()
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingMaterialLevel4", myParamters);

            List<PricingRequestMaterialEntity> myPriceRequests = new List<PricingRequestMaterialEntity>();

            foreach (DataRow r in dt.Rows)
            {
                PricingRequestMaterialEntity pr = new PricingRequestMaterialEntity();
                pr.Level_4_Desc = r["Level_4_Desc"].ToString();
                myPriceRequests.Add(pr);
            }

            return myPriceRequests;
        }

        /// <summary>Gets The PricingRequestMaterialEntity List populated with MaterialLevel5 items</summary>
        /// <returns>a List with the PricingRequestMaterialEntity</returns>
        public static List<PricingRequestMaterialEntity> GetPriceRequestLevel5()
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingMaterialLevel5", myParamters);

            List<PricingRequestMaterialEntity> myPriceRequests = new List<PricingRequestMaterialEntity>();

            foreach (DataRow r in dt.Rows)
            {
                PricingRequestMaterialEntity pr = new PricingRequestMaterialEntity();
                pr.Level_5_Desc = r["Level_5_Desc"].ToString();
                myPriceRequests.Add(pr);
            }

            return myPriceRequests;
        }

        /// <summary>Gets The PricingRequestMaterialEntity List populated with MaterialLevel6 items</summary>
        /// <returns>a List with the PricingRequestMaterialEntity</returns>
        public static List<PricingRequestMaterialEntity> GetPriceRequestLevel6()
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingMaterialLevel6", myParamters);

            List<PricingRequestMaterialEntity> myPriceRequests = new List<PricingRequestMaterialEntity>();

            foreach (DataRow r in dt.Rows)
            {
                PricingRequestMaterialEntity pr = new PricingRequestMaterialEntity();
                pr.Level_6_Desc = r["Level_6_Desc"].ToString();
                myPriceRequests.Add(pr);
            }

            return myPriceRequests;
        }

        /// <summary>Gets The PricingRequestMaterialEntity List for the based on the CustomerID and Customer State (used for State ASP Calc)</summary>
        /// <param name="strCustomerID">The CustomerID.</param>
        /// <param name="strCustomerState">The CustomerState.</param>
        /// <returns>a List with the PricingRequestMaterialEntity</returns>
        public static DataTable GetPriceRequestByCustomerTable(string strCustomerID, string strCustomerState)
        {
            DataTable dtAllMaterials = PricingRequestMaterialEntity.GetAllPriceRequestMaterialsTable();
            //List<PricingRequestMaterialEntity> allPriceRequestsMaterials = PricingRequestMaterialEntity.GetPricingRequestMaterialEntityList(dtAllMaterials);

            DataColumn[] primaryCols = new DataColumn[1];  //set primary key
            primaryCols[0] = dtAllMaterials.Columns["Material_ID"];
            dtAllMaterials.PrimaryKey = primaryCols;


            string strMaterialWhereClause = PricingRequestMaterialEntity.GetMaterialWhereClause();

            Debug.WriteLine("strMaterialWhereClause: " + strMaterialWhereClause);

            DataTable dtCustPrices = PricingRequestMaterialEntity.GetQueryByCustomer(strCustomerID, strMaterialWhereClause, "PriceRequestByCustomer");

            // Updates the customer prices in the allPriceRequestsMaterials List
            // Loops thru the dtCustPrices table and finds the matching materialIDs in allPriceRequestsMaterials List
            // then updates the pricing for the List item
            foreach (DataRow pRow in dtCustPrices.Rows)
            {
                DataRow updateRow = dtAllMaterials.Rows.Find(pRow["Material_ID"].ToString());
                if (updateRow != null)
                {
                    if ((pRow["List_Price"] != null) && (pRow["List_Price"] != DBNull.Value))
                        updateRow["List_Price"] = Convert.ToDouble(pRow["List_Price"].ToString());

                    if ((pRow["Local_Price"] != null) && (pRow["Local_Price"] != DBNull.Value))
                        updateRow["Local_Price"] = Convert.ToDouble(pRow["Local_Price"].ToString());

                    if ((pRow["GPO_Price"] != null) && (pRow["GPO_Price"] != DBNull.Value))
                        updateRow["GPO_Price"] = Convert.ToDouble(pRow["GPO_Price"].ToString());

                    if ((pRow["CG1_Price"] != null) && (pRow["CG1_Price"] != DBNull.Value))
                        updateRow["CG1_Price"] = Convert.ToDouble(pRow["CG1_Price"].ToString());

                    if ((pRow["Net_Price"] != null) && (pRow["Net_Price"] != DBNull.Value))
                        updateRow["Net_Price"] = Convert.ToDouble(pRow["Net_Price"].ToString());

                    if(pRow.Table.Columns.Contains("CG1_ValidFrom"))
                        updateRow["CG1_ValidFrom"] = pRow["CG1_ValidFrom"].ToString();

                    if(pRow.Table.Columns.Contains("CG1_ValidTo"))
                        updateRow["CG1_ValidTo"] = pRow["CG1_ValidTo"].ToString();

                    if(pRow.Table.Columns.Contains("Local_ValidFrom"))
                        updateRow["Local_ValidFrom"] = pRow["Local_ValidFrom"].ToString();

                    if(pRow.Table.Columns.Contains("Local_ValidTo"))
                        updateRow["Local_ValidTo"] = pRow["Local_ValidTo"].ToString();

                    if (pRow.Table.Columns.Contains("Local_SalesDeal"))
                        updateRow["Local_SalesDeal"] = pRow["Local_SalesDeal"].ToString();

                    if (pRow.Table.Columns.Contains("GPO_SalesDeal"))
                        updateRow["GPO_SalesDeal"] = pRow["GPO_SalesDeal"].ToString();

                    if (pRow.Table.Columns.Contains("CG1_SalesDeal"))
                        updateRow["CG1_SalesDeal"] = pRow["CG1_SalesDeal"].ToString();
                }
                else
                {
                    Debug.WriteLine("Material_ID: " + pRow["Material_ID"].ToString() + " NOT FOUND!");
                }

            }

            DataTable dtRolling = PricingRequestMaterialEntity.GetQueryByCustomer(strCustomerID, strMaterialWhereClause, "PriceRequestRolling12MonthsByCustomer");

            // Updates the customer rolling revenue  in the allPriceRequestsMaterials List
            // Loops thru the dtRolling table and finds the matching materialIDs in allPriceRequestsMaterials List
            // then updates the Rolling Revenue for the List item
            foreach (DataRow pRow in dtRolling.Rows)
            {
                DataRow updateRow = dtAllMaterials.Rows.Find(pRow["Material_ID"].ToString());

                if (updateRow != null)
                {
                    if ((pRow["Rolling_Revenue"] != null) && (pRow["Rolling_Revenue"] != DBNull.Value))
                        updateRow["Rolling_Revenue"] = Convert.ToDouble(pRow["Rolling_Revenue"].ToString());
                }
            }

            DataTable dtStateASP = PricingRequestMaterialEntity.GetASPbyState(strCustomerState);

            // Updates the state ASP in the allPriceRequestsMaterials List
            // Loops thru the dtStateASP table and finds the matching materialIDs in allPriceRequestsMaterials List
            // then updates the State ASP for the List item
            foreach (DataRow aRow in dtStateASP.Rows)
            {
                DataRow updateRow = dtAllMaterials.Rows.Find(aRow["Material_ID"].ToString());
                if (updateRow != null)
                {
                    if ((aRow["State_ASP"] != null) && (aRow["State_ASP"] != DBNull.Value))
                        updateRow["State_ASP"] = Convert.ToDouble(aRow["State_ASP"].ToString());
                }

            }

            return dtAllMaterials;
        }

        /// <summary>Receives a PriceRequest datarow and converts it to a PricingRequestMaterialEntity.</summary>
        /// <param name="r">The PriceRequest DataRow.</param>
        /// <returns>PricingRequestMaterialEntity</returns>
        public static PricingRequestMaterialEntity GetEntityFromDataRow(DataRow r)
        {
            PricingRequestMaterialEntity pr = new PricingRequestMaterialEntity();

            pr.Material_Id = r["Material_Id"].ToString();
            pr.Material_Desc = r["Material_Desc"].ToString();
            pr.Marketing_Manager = r["Marketing_Manager"].ToString();
            pr.Level_4_Desc = r["Level_4_Desc"].ToString();
            pr.Level_5_Desc = r["Level_5_Desc"].ToString();
            pr.Level_6_Desc = r["Level_6_Desc"].ToString();
            pr.Sellable_UOM = r["Sellable_UOM"].ToString();
            pr.Approval_Required_From = r["Approval_Required_From"].ToString();
            pr.Recommendation_Approval = r["Recommendation_Approval"].ToString();

            if ((r["Requested_Price"] != null) && (r["Requested_Price"] != DBNull.Value))
                pr.Requested_Price = Convert.ToDouble(r["Requested_Price"].ToString());

            if ((r["Expected_Volume"] != null) && (r["Expected_Volume"] != DBNull.Value))
                pr.Expected_Volume = Convert.ToDouble(r["Expected_Volume"].ToString());

            if ((r["Requested_Discount"] != null) && (r["Requested_Discount"] != DBNull.Value))
                pr.Requested_Discount = Convert.ToDouble(r["Requested_Discount"].ToString());

            if ((r["Requested_GM"] != null) && (r["Requested_GM"] != DBNull.Value))
                pr.Requested_GM = Convert.ToDouble(r["Requested_GM"].ToString());

            if ((r["Competitor_Price"] != null) && (r["Competitor_Price"] != DBNull.Value))
                pr.Competitor_Price = Convert.ToDouble(r["Competitor_Price"].ToString());

            if ((r["Rolling_Revenue"] != null) && (r["Rolling_Revenue"] != DBNull.Value))
                pr.Rolling_Revenue = Convert.ToDouble(r["Rolling_Revenue"].ToString());

            if ((r["List_Price"] != null) && (r["List_Price"] != DBNull.Value))
                pr.List_Price = Convert.ToDouble(r["List_Price"].ToString());

            if ((r["Local_Price"] != null) && (r["Local_Price"] != DBNull.Value))
                pr.Local_Price = Convert.ToDouble(r["Local_Price"].ToString());

            if ((r["GPO_Price"] != null) && (r["GPO_Price"] != DBNull.Value))
                pr.GPO_Price = Convert.ToDouble(r["GPO_Price"].ToString());

            if ((r["CG1_Price"] != null) && (r["CG1_Price"] != DBNull.Value))
                pr.CG1_Price = Convert.ToDouble(r["CG1_Price"].ToString());

            if ((r["Net_Price"] != null) && (r["Net_Price"] != DBNull.Value))
                pr.Net_Price = Convert.ToDouble(r["Net_Price"].ToString());

            if ((r["State_ASP"] != null) && (r["State_ASP"] != DBNull.Value))
                pr.State_ASP = Convert.ToDouble(r["State_ASP"].ToString());

            if ((r["US_ASP"] != null) && (r["US_ASP"] != DBNull.Value))
                pr.US_ASP = Convert.ToDouble(r["US_ASP"].ToString());

            if ((r["Standard_Cost"] != null) && (r["Standard_Cost"] != DBNull.Value))
                pr.Standard_Cost = Convert.ToDouble(r["Standard_Cost"].ToString());

            if ((r["COG"] != null) && (r["COG"] != DBNull.Value))
                pr.COG = Convert.ToDouble(r["COG"].ToString());

            if ((r["Qty_Per_Sellable_UOM"] != null) && (r["Qty_Per_Sellable_UOM"] != DBNull.Value))
                pr.Qty_Per_Sellable_UOM = Convert.ToDouble(r["Qty_Per_Sellable_UOM"].ToString());

            if ((r["Internal_Floor_Price"] != null) && (r["Internal_Floor_Price"] != DBNull.Value))
                pr.Internal_Floor_Price = Convert.ToDouble(r["Internal_Floor_Price"].ToString());

            if ((r["HPG_Price"] != null) && (r["HPG_Price"] != DBNull.Value))
                pr.HPG_Price = Convert.ToDouble(r["HPG_Price"].ToString());

            if ((r["MedAssets_T1_Price"] != null) && (r["MedAssets_T1_Price"] != DBNull.Value))
                pr.MedAssets_T1_Price = Convert.ToDouble(r["MedAssets_T1_Price"].ToString());

            if ((r["MedAssets_T2_Price"] != null) && (r["MedAssets_T2_Price"] != DBNull.Value))
                pr.MedAssets_T2_Price = Convert.ToDouble(r["MedAssets_T2_Price"].ToString());

            if ((r["Premier_Price"] != null) && (r["Premier_Price"] != DBNull.Value))
                pr.Premier_Price = Convert.ToDouble(r["Premier_Price"].ToString());

            if ((r["Novation_Participant_Price"] != null) && (r["Novation_Participant_Price"] != DBNull.Value))
                pr.Novation_Participant_Price = Convert.ToDouble(r["Novation_Participant_Price"].ToString());

            if ((r["Novation_Committed_Price"] != null) && (r["Novation_Committed_Price"] != DBNull.Value))
                pr.Novation_Committed_Price = Convert.ToDouble(r["Novation_Committed_Price"].ToString());

            if ((r["Rep_Discount"] != null) && (r["Rep_Discount"] != DBNull.Value))
                pr.Rep_Discount = Convert.ToDouble(r["Rep_Discount"].ToString());

            if ((r["Rep_Price"] != null) && (r["Rep_Price"] != DBNull.Value))
                pr.Rep_Price = Convert.ToDouble(r["Rep_Price"].ToString());

            if ((r["DE_Discount"] != null) && (r["DE_Discount"] != DBNull.Value))
                pr.DE_Discount = Convert.ToDouble(r["DE_Discount"].ToString());

            if ((r["DE_Price"] != null) && (r["DE_Price"] != DBNull.Value))
                pr.DE_Price = Convert.ToDouble(r["DE_Price"].ToString());

            if ((r["RVP_Discount"] != null) && (r["RVP_Discount"] != DBNull.Value))
                pr.RVP_Discount = Convert.ToDouble(r["RVP_Discount"].ToString());

            if ((r["RVP_Price"] != null) && (r["RVP_Price"] != DBNull.Value))
                pr.RVP_Price = Convert.ToDouble(r["RVP_Price"].ToString());

            if (r.Table.Columns.Contains("Local_ValidFrom"))
                pr.Local_ValidFrom = r["Local_ValidFrom"].ToString();

            if (r.Table.Columns.Contains("Local_ValidTo"))
                pr.Local_ValidTo = r["Local_ValidTo"].ToString();

            if (r.Table.Columns.Contains("CG1_ValidTo"))
                pr.CG1_ValidTo = r["CG1_ValidTo"].ToString();

            if (r.Table.Columns.Contains("CG1_ValidFrom"))
                pr.CG1_ValidFrom = r["CG1_ValidFrom"].ToString();

            if (r.Table.Columns.Contains("CG1_SalesDeal"))
                pr.CG1_SalesDeal = r["CG1_SalesDeal"].ToString();

            if (r.Table.Columns.Contains("GPO_SalesDeal"))
                pr.GPO_SalesDeal = r["GPO_SalesDeal"].ToString();

            if (r.Table.Columns.Contains("Local_SalesDeal"))
                pr.Local_SalesDeal = r["Local_SalesDeal"].ToString();

            return pr;
        }

        /// <summary>Gets the PricingMaterialASP by State</summary>
        /// <param name="strState">The Customer State</param>
        /// <returns>The newly populated datatable</returns>
        public static DataTable GetASPbyState(string strState)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@State_Province_Code", strState));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingMaterialASPbyState", myParamters);

            return dt;
        }

        /// <summary>Gets the datatable for the specified query for the specified customer</summary>
        /// <param name="strCustomerID">The Customer ID</param>
        /// <param name="strMaterialClause">The Material Where Clause string</param>
        /// <param name="strQueryName">The name of the Query</param>
        /// <returns>The newly populated Price Request datatable</returns>
        public static DataTable GetQueryByCustomer(string strCustomerID, string strMaterialClause, string strQueryName)
        {
            string sQuery = string.Empty;
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@QueryName", strQueryName));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetQueryByQueryName", parameters);
            if (dt.Rows.Count > 0)
            {
                sQuery = dt.Rows[0]["Query"].ToString();
                sQuery = sQuery.Replace("MaterialWhereClause", strMaterialClause);
                sQuery = sQuery.Replace("xxCUSTOMERIDxx", strCustomerID);
            }

            Debug.WriteLine("sQuery: " + sQuery);

            DataTable dtMaterial = PricingRequestMaterialEntity.GetDynamicQueryTable(sQuery);

            return dtMaterial;
        }

        /// <summary>Gets the datatable for the GDW query provided.</summary>
        /// <param name="sQuery">The Query to use</param>
        /// <returns>The newly populated DataTable<CustomerEntity></returns>
        protected static DataTable GetDynamicQueryTable(string sQuery)
        {
//            DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery, SQLUtility.OpenGdwSqlConnection());
            DataTable dt = SQLUtility.OracleExecuteDynamicQuery(sQuery);
            return dt;
        }

        /// <summary>Gets Material List string</summary>
        /// <returns>the Material List string</returns>
        public static string GetPriceRequestMaterialList()
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            //  myParamters.Add(new SqlParameter("@PlacedCapitalRequestID", intPlacedCapitalRequestID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingMaterial", myParamters);
            string strMaterialList = string.Empty;
            foreach (DataRow r in dt.Rows)
            {
                strMaterialList += ",'" + r["Material_ID"].ToString() + "'";
            }

            strMaterialList = "(" + strMaterialList.Trim().Trim(',') + ")";
            Debug.WriteLine("strMaterialList: " + strMaterialList);

            return strMaterialList;
        }

        /// <summary>Saves the PricingRequest Materials in the database and returns the number of records updated.</summary>
        /// <param name="intPricingRequestID">The PricingRequestID</param>
        /// <param name="materials">The PricingRequestMaterialEntity List</param>
        /// <returns>the number of records updated</returns>
        public static int SavePricingRequestMaterialsInDatabase(int intPricingRequestID, List<PricingRequestMaterialEntity> materials)
        {
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            int insertCnt = 0;

            try
            {
                Collection<SqlParameter> deleteParameters = new Collection<SqlParameter>();
                deleteParameters.Add(new SqlParameter("PricingRequestID", intPricingRequestID));
                int deletedCustomerCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_DeletePricingRequestMaterialByPricingRequestID", deleteParameters, conn, trans);

                foreach (PricingRequestMaterialEntity m in materials)
                {
                    Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

                    SqlParameter SQLID = new SqlParameter("@PricingRequestMaterialID", m.PricingRequestMaterialID);
                    SQLID.Direction = ParameterDirection.Output;
                    myParameters.Add(SQLID);

                    myParameters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));
                    myParameters.Add(new SqlParameter("@Material_Id", m.Material_Id));
                    myParameters.Add(new SqlParameter("@Material_Desc", m.Material_Desc));
                    myParameters.Add(new SqlParameter("@Marketing_Manager", m.Marketing_Manager));
                    myParameters.Add(new SqlParameter("@Level_4_Desc", m.Level_4_Desc));
                    myParameters.Add(new SqlParameter("@Level_5_Desc", m.Level_5_Desc));
                    myParameters.Add(new SqlParameter("@Level_6_Desc", m.Level_6_Desc));
                    myParameters.Add(new SqlParameter("@List_Price", m.List_Price));
                    myParameters.Add(new SqlParameter("@Local_Price", m.Local_Price));
                    myParameters.Add(new SqlParameter("@Requested_Price", m.Requested_Price));
                    myParameters.Add(new SqlParameter("@Expected_Volume", m.Expected_Volume));
                    myParameters.Add(new SqlParameter("@Requested_Discount", m.Requested_Discount));
                    myParameters.Add(new SqlParameter("@Requested_GM", m.Requested_GM));
                    myParameters.Add(new SqlParameter("@Competitor_Price", m.Competitor_Price));
                    myParameters.Add(new SqlParameter("@Rolling_Revenue", m.Rolling_Revenue));
                    myParameters.Add(new SqlParameter("@GPO_Price", m.GPO_Price));
                    myParameters.Add(new SqlParameter("@CG1_Price", m.CG1_Price));
                    myParameters.Add(new SqlParameter("@Net_Price", m.Net_Price));
                    myParameters.Add(new SqlParameter("@State_ASP", m.State_ASP));
                    myParameters.Add(new SqlParameter("@US_ASP", m.US_ASP));
                    myParameters.Add(new SqlParameter("@Standard_Cost", m.Standard_Cost));
                    myParameters.Add(new SqlParameter("@COG", m.COG));
                    myParameters.Add(new SqlParameter("@Sellable_UOM", m.Sellable_UOM));
                    myParameters.Add(new SqlParameter("@Qty_Per_Sellable_UOM", m.Qty_Per_Sellable_UOM));
                    myParameters.Add(new SqlParameter("@Internal_Floor_Price", m.Internal_Floor_Price));
                    myParameters.Add(new SqlParameter("@HPG_Price", m.HPG_Price));
                    myParameters.Add(new SqlParameter("@MedAssets_T1_Price", m.MedAssets_T1_Price));
                    myParameters.Add(new SqlParameter("@MedAssets_T2_Price", m.MedAssets_T2_Price));
                    myParameters.Add(new SqlParameter("@Premier_Price", m.Premier_Price));
                    myParameters.Add(new SqlParameter("@Novation_Participant_Price", m.Novation_Participant_Price));
                    myParameters.Add(new SqlParameter("@Novation_Committed_Price", m.Novation_Committed_Price));
                    myParameters.Add(new SqlParameter("@Rep_Discount", m.Rep_Discount));
                    myParameters.Add(new SqlParameter("@Rep_Price", m.Rep_Price));
                    myParameters.Add(new SqlParameter("@DE_Discount", m.DE_Discount));
                    myParameters.Add(new SqlParameter("@DE_Price", m.DE_Price));
                    myParameters.Add(new SqlParameter("@RVP_Discount", m.RVP_Discount));
                    myParameters.Add(new SqlParameter("@RVP_Price", m.RVP_Price));
                    myParameters.Add(new SqlParameter("@Approval_Required_From", m.Approval_Required_From));
                    myParameters.Add(new SqlParameter("@Recommendation_Approval", m.Recommendation_Approval));
                    myParameters.Add(new SqlParameter("@Local_ValidFrom", m.Local_ValidFrom));
                    myParameters.Add(new SqlParameter("@Local_ValidTo", m.Local_ValidTo));
                    myParameters.Add(new SqlParameter("@CG1_ValidFrom", m.CG1_ValidFrom));
                    myParameters.Add(new SqlParameter("@CG1_ValidTo", m.CG1_ValidTo));
                    myParameters.Add(new SqlParameter("@GPO_SalesDeal", m.GPO_SalesDeal));
                    myParameters.Add(new SqlParameter("@CG1_SalesDeal", m.CG1_SalesDeal));
                    myParameters.Add(new SqlParameter("@Local_SalesDeal", m.Local_SalesDeal));

                    Debug.WriteLine("material ID: " + m.Material_Id + "; Requested_Price: " + m.Requested_Price.ToString() + "; Requested_GM: " + m.Requested_GM.ToString());

                    foreach (SqlParameter p in myParameters)
                    {
                        Debug.WriteLine("Name: " + p.ParameterName + "; Value: " + p.Value.ToString());
                    }

                    int intInsertCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_InsertPricingRequestMaterial", myParameters, conn, trans);

                    insertCnt++;
                }
                 trans.Commit();

                SQLUtility.CloseSqlConnection(conn);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("SavePricingRequestMaterialsInDatabase Error: " + ex.Message);

                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);
                throw (ex);

            }

            return insertCnt;
        }

        /// <summary>Gets the PricingRequestMaterialEntity List by the PricingRequestID.</summary>
        /// <param name="intPricingRequestID">The PricingRequestID</param>
        /// <returns>the PricingRequestMaterialEntity List</returns>
        public static List<PricingRequestMaterialEntity> GetPricingRequestMaterialByPricingRequestID(int intPricingRequestID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingRequestMaterialByPricingRequestID", myParameters);

            List<PricingRequestMaterialEntity> myPriceRequests = new List<PricingRequestMaterialEntity>();

            foreach (DataRow r in dt.Rows)
            {
                PricingRequestMaterialEntity pr = PricingRequestMaterialEntity.GetEntityFromDataRow(r);
                myPriceRequests.Add(pr);
            }

            return myPriceRequests;
        }

        /// <summary>Gets the index of the Gridview column with the matching Header Text.</summary>
        /// <param name="gv">The GridView control</param>
        /// <param name="strHeaderText">The HeaderText</param>
        /// <returns>the column index</returns>
        protected static int GetColumnIndexByHeaderText(System.Web.UI.WebControls.GridView gv, string strHeaderText)
        {
            int intIndex = 0;

            foreach (System.Web.UI.WebControls.DataControlField c in gv.Columns)
            {
                if (c.HeaderText.ToLower() == strHeaderText.ToLower())
                {
//                    Debug.WriteLine("HeaderText: " + strHeaderText + "; Index: " + intIndex.ToString());
                    return intIndex;
                }
                intIndex++;
            }

            return -1;
        }

        /// <summary>Hides specific columns in the Gridview control based on the user role and column name.</summary>
        /// <param name="gv">The GridView control</param>
        /// <param name="req">The HttpRequest used to get the username then check the userRole</param>
        public static void HideGridViewColumns(System.Web.UI.WebControls.GridView gv, System.Web.HttpRequest req)
        {
            //0-No Role Found (SalesRep) 1-Super Admin, 2-Placed Capital Admin, 3-Pricing Request Admin, 4-Deal Desk, 5-Customer Service, 6-DOS/DE, 7-RVP
            int intUserRoleID = AppUsersEntity.GetAppUserRoleIDByProgramIDByUsername(2, Utility.CurrentUser(req));

            // if SuperAdmin or Pricing Request Admin or Deal Desk or RVP
            bool viewCOGs = Convert.ToBoolean((intUserRoleID == 1) || (intUserRoleID == 3) || (intUserRoleID == 4) || (intUserRoleID == 7));
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "COG")].Visible = viewCOGs;

            // if SuperAdmin or Pricing Request Admin or Deal Desk or RVP
            bool viewRequestGM = Convert.ToBoolean((intUserRoleID == 1) || (intUserRoleID == 3) || (intUserRoleID == 4) || (intUserRoleID == 7));
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Request GM%")].Visible = viewRequestGM;

            // if SuperAdmin or Pricing Request Admin or Deal Desk or RVP 
            bool viewInternalFloor = Convert.ToBoolean((intUserRoleID == 1) || (intUserRoleID == 3) || (intUserRoleID == 4) || (intUserRoleID == 7));
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Internal Floor")].Visible = viewInternalFloor;

            // if SuperAdmin or Pricing Request Admin or Deal Desk or DE OR RVP 
            bool viewUSASP = Convert.ToBoolean((intUserRoleID == 1) || (intUserRoleID == 3) || (intUserRoleID == 4) || (intUserRoleID == 6) || (intUserRoleID == 7));
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "US ASP")].Visible = viewUSASP;

            // if SuperAdmin or Pricing Request Admin or Deal Desk or DE OR RVP 
            bool viewStateASP = Convert.ToBoolean((intUserRoleID == 1) || (intUserRoleID == 3) || (intUserRoleID == 4) || (intUserRoleID == 6) || (intUserRoleID == 7));
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "State ASP")].Visible = viewStateASP;

            // if NOT Sales Rep 
            bool viewNotRep = Convert.ToBoolean((intUserRoleID != 0));
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "RVP Discount")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "RVP Price")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Approval Required From")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Recommendation Approval")].Visible = viewNotRep;

            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "HPG")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "MedAssets T1")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "MedAssets T2")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Premier")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Novation Part")].Visible = viewNotRep;
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Novation Comm")].Visible = viewNotRep;
        }

        /// <summary>Highlights specific columns based on the HeaderText of the column.</summary>
        /// <param name="gv">The GridView control</param>
        public static void HighLightGridViewColumns(System.Web.UI.WebControls.GridView gv)
        {
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Request Amt($)")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(255, 255, 0);

            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "GPO Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(183, 222, 232);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Cust Grp1 Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(183, 222, 232);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Local Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(183, 222, 232);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Current Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(183, 222, 232);

            //gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Competitor Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(255, 255, 0);
            //gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Expected $ volume (per item)")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(255, 255, 0);


            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "COG")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Request GM%")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);

            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Internal Floor")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(253, 233, 217);

            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "State ASP")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(184, 204, 228);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "US ASP")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(184, 204, 228);

            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Rep Discount")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "Rep Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "DOS/DE Discount")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "DOS/DE Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "RVP Discount")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
            gv.Columns[PricingRequestMaterialEntity.GetColumnIndexByHeaderText(gv, "RVP Price")].ItemStyle.BackColor = System.Drawing.Color.FromArgb(216, 228, 188);
        }

    } //// end class
}