﻿// ------------------------------------------------------------------
// <copyright file="PricingCustomerEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using SalesRequestForms.Classes;

    /// <summary>This is the Pricing Customer Entity.</summary>
    public class PricingCustomerEntity
    {
        /// <summary>This is the PricingCustomerID.</summary>
        private int intPricingCustomerID;
        
        /// <summary>This is the PricingRequestID.</summary>
        private int intPricingRequestID;

        /// <summary>This is the PricingCustomerType ID.</summary>
        private int intPricingCustomerTypeID;
        
        /// <summary>This is the CustomerID.</summary>
        private string strCustomerID;

        /// <summary>This is the CustomerName.</summary>
        private string strCustomerName;

        /// <summary>This is the CustomerStreet.</summary>
        private string strStreet;

        /// <summary>This is the City.</summary>
        private string strCity;

        /// <summary>This is the StateCode.</summary>
        private string strStateCode;

        /// <summary>This is the PostalCode.</summary>
        private string strPostalCode;

        /// <summary>This is the GpoDesc.</summary>
        private string strGpoDesc;

        /// <summary>Initializes a new instance of the PricingCustomerEntity class.</summary>
        public PricingCustomerEntity()
        {
            this.CustomerID = String.Empty;
            this.CustomerName = String.Empty;
            this.Street = String.Empty;
            this.City = String.Empty;
            this.StateCode = String.Empty;
            this.PostalCode = String.Empty;
            this.GpoDesc = String.Empty;
        }

        /// <summary>Gets or sets the PricingCustomerID.</summary>
        /// <value>The PricingCustomerID.<value> 
        public int PricingCustomerID { get { return this.intPricingCustomerID; } set { this.intPricingCustomerID = value; } }

        /// <summary>Gets or sets the PricingRequestID.</summary>
        /// <value>The PricingRequestID.<value> 
        public int PricingRequestID { get { return this.intPricingRequestID; } set { this.intPricingRequestID = value; } }

        /// <summary>Gets or sets the PricingCustomerTypeID.</summary>
        /// <value>The PricingCustomerTypeID.<value> 
        public int PricingCustomerTypeID { get { return this.intPricingCustomerTypeID; } set { this.intPricingCustomerTypeID = value; } }

        /// <summary>Gets or sets the CustomerID.</summary>
        /// <value>The CustomerID.<value> 
        public string CustomerID { get { return this.strCustomerID; } set { this.strCustomerID = value; } }

        /// <summary>Gets or sets the CustomerName.</summary>
        /// <value>The CustomerName.<value> 
        public string CustomerName { get { return this.strCustomerName; } set { this.strCustomerName = value; } }

        /// <summary>Gets or sets the Street.</summary>
        /// <value>The Street.<value> 
        public string Street { get { return this.strStreet; } set { this.strStreet = value; } }

        /// <summary>Gets or sets the City.</summary>
        /// <value>The City.<value> 
        public string City { get { return this.strCity; } set { this.strCity = value; } }

        /// <summary>Gets or sets the StateCode.</summary>
        /// <value>The StateCode.<value> 
        public string StateCode { get { return this.strStateCode; } set { this.strStateCode = value; } }

        /// <summary>Gets or sets the PostalCode.</summary>
        /// <value>The PostalCode.<value> 
        public string PostalCode { get { return this.strPostalCode; } set { this.strPostalCode = value; } }

        /// <summary>Gets or sets the GpoDesc.</summary>
        /// <value>The GpoDesc.<value> 
        public string GpoDesc { get { return this.strGpoDesc; } set { this.strGpoDesc = value; } }

        /// <summary>Receives a PriceRequest datarow and converts it to a PricingRequestEntity.</summary>
        /// <param name="r">The PriceRequest DataRow.</param>
        /// <returns>PricingRequestEntity</returns>
        public static PricingCustomerEntity GetEntityFromDataRow(DataRow r)
        {
            PricingCustomerEntity pc = new PricingCustomerEntity();

            if ((r["PricingCustomerID"] != null) && (r["PricingCustomerID"] != DBNull.Value))
                pc.PricingCustomerID = Convert.ToInt32(r["PricingCustomerID"].ToString());

            if ((r["PricingRequestID"] != null) && (r["PricingRequestID"] != DBNull.Value))
                pc.PricingRequestID = Convert.ToInt32(r["PricingRequestID"].ToString());

            if ((r["PricingCustomerTypeID"] != null) && (r["PricingCustomerTypeID"] != DBNull.Value))
                pc.PricingCustomerTypeID = Convert.ToInt32(r["PricingCustomerTypeID"].ToString());

            pc.CustomerID = r["CustomerID"].ToString();
            pc.CustomerName = r["CustomerName"].ToString();
            pc.Street = r["Street"].ToString();
            pc.City = r["City"].ToString();
            pc.StateCode = r["StateCode"].ToString();
            pc.PostalCode = r["PostalCode"].ToString();
            pc.GpoDesc = r["GpoDesc"].ToString();

            return pc;
        }

        /// <summary>Delete the PricingCustomers By PricingRequestID By PricingCustomerTypeID</summary>
        /// <param name="intPricingRequestID">the PricingRequestID </param>
        /// <param name="intPricingCustomerTypeID">the PricingCustomerTypeID</param>
        /// <param name="SqlConn">The SqlConnection.</param>
        /// <param name="myTransaction">The SqlTransaction.</param>
        /// <returns>The number of records affected</returns>
        public static int DeletePricingCustomerByPricingRequestIDByPricingCustomerTypeID(int intPricingRequestID, int intPricingCustomerTypeID, SqlConnection conn, SqlTransaction trans)
        {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

                myParamters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));
                myParamters.Add(new SqlParameter("@PricingCustomerTypeID", intPricingCustomerTypeID));

                int intRecordsInserted = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_DeletePricingCustomerByPricingRequestIDByPricingCustomerTypeID", myParamters, conn, trans);

                return intRecordsInserted;
        }

        /// <summary>Insert the Pricing Customer into the database</summary>
        /// <param name="customers">Collection PricingCustomerEntity</param>
        /// <param name="intPricingRequestID">the PricingRequestID </param>
        /// <param name="intPricingCustomerTypeID">the PricingCustomerTypeID</param>
        /// <param name="SqlConn">The SqlConnection.</param>
        /// <param name="myTransaction">The SqlTransaction.</param>
        /// <returns>number of updated records</returns>
        public static int InsertPricingCustomers(Collection<PricingCustomerEntity> customers, int intPricingRequestID, int intPricingCustomerTypeID, SqlConnection conn, SqlTransaction trans)
        {
            int intUpdatedRecords = 0;
            foreach (PricingCustomerEntity pc in customers)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

                SqlParameter SQLID = new SqlParameter("@PricingCustomerID", pc.PricingCustomerID);
                SQLID.Direction = ParameterDirection.Output;
                myParamters.Add(SQLID);

                myParamters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));
                myParamters.Add(new SqlParameter("@PricingCustomerTypeID", intPricingCustomerTypeID));
                myParamters.Add(new SqlParameter("@CustomerID", pc.CustomerID));
                myParamters.Add(new SqlParameter("@CustomerName", pc.CustomerName));
                myParamters.Add(new SqlParameter("@Street", pc.Street));
                myParamters.Add(new SqlParameter("@City", pc.City));
                myParamters.Add(new SqlParameter("@StateCode", pc.StateCode));
                myParamters.Add(new SqlParameter("@PostalCode", pc.PostalCode));
                myParamters.Add(new SqlParameter("@GpoDesc", pc.GpoDesc));

                int intRecordsInserted = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_InsertPricingCustomer", myParamters, conn, trans);
                intUpdatedRecords++;
            }

            return intUpdatedRecords;
        }

        /// <summary>Gets The PricingCustomerEntity by the PricingRequestID and PricingCustomerTypeID</summary>
        /// <param name="intPricingRequestID">The PricingRequest ID</param>
        /// <param name="intPricingCustomerTypeID">The PricingCustomerType ID</param>
        /// <returns>The PricingCustomerEntity Collection</returns>
        public static Collection<PricingCustomerEntity> GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID(int intPricingRequestID, int intPricingCustomerTypeID)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));
            myParamters.Add(new SqlParameter("@PricingCustomerTypeID", intPricingCustomerTypeID));

            Collection<PricingCustomerEntity> customers = new Collection<PricingCustomerEntity>();
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID", myParamters);
            
            foreach(DataRow r in dt.Rows)
            {
                PricingCustomerEntity pc = PricingCustomerEntity.GetEntityFromDataRow(r);
                customers.Add(pc);
            }

            return customers;
        }

        /// <summary>Gets the Sold To Customer Entity Collection for the CustomerID.</summary>
        /// <param name="strCustomerID">The Customer ID</param>
        /// <returns>The newly populated Collection<CustomerEntity></returns>
        public static Collection<PricingCustomerEntity> GetSoldToByCustomerID(string strCustomerID)
        {
            Collection<CustomerEntity> customers = CustomerEntity.GetSoldToByCustomerID(strCustomerID);

            Collection<PricingCustomerEntity> pCustomers = new Collection<PricingCustomerEntity>();

            foreach (CustomerEntity cust in customers)
            {
                PricingCustomerEntity pc = new PricingCustomerEntity();
                pc.CustomerID = cust.CustomerID;
                pc.CustomerName = cust.CustomerName;
                pc.City = cust.City;
                pc.GpoDesc = cust.GpoName;
                pc.PostalCode = cust.PostalCode;
                pc.StateCode = cust.StateCode;
                pc.Street = cust.Street;

                pCustomers.Add(pc);
            }

            return pCustomers;
        }


    }//// end class
}