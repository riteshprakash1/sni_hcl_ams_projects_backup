﻿// ------------------------------------------------------------------
// <copyright file="PricingRequestEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PricingRequest.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using SalesRequestForms.Classes;


    /// <summary>This is the Price Request Entity.</summary>
    public class PricingRequestEntity
    {

        /// <summary> This is the PricingRequestID</summary> 
        private int intPricingRequestID;

        /// <summary> This is the ProgramID</summary> 
        private int intProgramID;

        /// <summary> This is the SoldToCustomerID</summary> 
        private string strSoldToCustomerID;

        /// <summary> This is the SoldToCustomerName</summary> 
        private string strSoldToCustomerName;

        /// <summary> This is the SoldToStreet</summary> 
        private string strSoldToStreet;

        /// <summary> This is the SoldToCity</summary> 
        private string strSoldToCity;

        /// <summary> This is the SoldToStateCode</summary> 
        private string strSoldToStateCode;

        /// <summary> This is the SoldToPostalCode</summary> 
        private string strSoldToPostalCode;

        /// <summary> This is the RvpName</summary> 
        private string strRvpName;

        /// <summary> This is the DistrictName</summary> 
        private string strDistrictName;

        /// <summary> This is the DistrictID</summary> 
        private string strDistrictID;

        /// <summary> This is the SalesRepName</summary> 
        private string strSalesRepName;

        /// <summary> This is the SalesRepID</summary> 
        private string strSalesRepID;

        /// <summary> This is the GpoDesc</summary> 
        private string strGpoDesc;

        /// <summary> This is the CustGroup1</summary> 
        private string strCustGroup1;

        /// <summary> This is the CustGroup1ID</summary> 
        private string strCustGroup1ID;

        /// <summary> This is the AgreementTerms</summary> 
        private string strAgreementTerms;

        /// <summary> This is the BusinessJustification</summary> 
        private string strBusinessJustification;

        /// <summary> This is the Competitor</summary> 
        private string strCompetitor;

        /// <summary> This is the IncrementalRevenueEstimate</summary> 
        private string strIncrementalRevenueEstimate;

        /// <summary> This is the IncrementalRevenueProbability</summary> 
        private string strIncrementalRevenueProbability;

        /// <summary> This is the FormStamp</summary> 
        private string strFormStamp;

        /// <summary> This is the CreatedBy</summary> 
        private string strCreatedBy;

        /// <summary> This is the CreateDate</summary> 
        private string strCreateDate;

        /// <summary> This is the ModifiedBy</summary> 
        private string strModifiedBy;

        /// <summary> This is the SubmittedByName</summary> 
        private string strSubmittedByName;

        /// <summary> This is the SubmitDate</summary> 
        private string strSubmitDate;

        /// <summary> This is the ModifiedDate</summary> 
        private string strModifiedDate;

        /// <summary> This is the PricingRequestStatus</summary> 
        private string strPricingRequestStatus;

        /// <summary> This is the 12 Month Rolling Revenue</summary> 
        private double dblRequest12RollingRev;

        /// <summary> This is the Request ToDate Revenue</summary> 
        private double dblRequestToDateRev;

        /// <summary> This is the To-Date Updated On</summary> 
        private string strToDateUpdatedOn;

        /// <summary>Initializes a new instance of the <see cref="PricingRequestMaterialEntity"/> class.</summary>
        public PricingRequestEntity()
        {
            this.SoldToCustomerID = string.Empty;
            this.SoldToCustomerName = string.Empty;
            this.SoldToStreet = string.Empty;
            this.SoldToCity = string.Empty;
            this.SoldToStateCode = string.Empty;
            this.SoldToPostalCode = string.Empty;
            this.RvpName = string.Empty;
            this.DistrictName = string.Empty;
            this.DistrictID = string.Empty;
            this.SalesRepName = string.Empty;
            this.SalesRepID = string.Empty;
            this.GpoDesc = string.Empty;
            this.AgreementTerms = string.Empty;
            this.BusinessJustification = string.Empty;
            this.Competitor = string.Empty;
            this.IncrementalRevenueEstimate = string.Empty;
            this.IncrementalRevenueProbability = string.Empty;
            this.FormStamp = string.Empty;
            this.CreatedBy = string.Empty;
            this.CreateDate = string.Empty;
            this.ModifiedBy = string.Empty;
            this.SubmittedByName = string.Empty;
            this.ModifiedDate = string.Empty;
            this.PricingRequestStatus = string.Empty;
            this.CustGroup1 = string.Empty;
            this.CustGroup1ID = string.Empty;
            this.SubmitDate = string.Empty;
        }

        /// <summary>Gets or sets the PricingRequestID.</summary>
        /// <value>The PricingRequestID.<value> 
        public int PricingRequestID { get { return this.intPricingRequestID; } set { this.intPricingRequestID = value; } }

        /// <summary>Gets or sets the ProgramID.</summary>
        /// <value>The ProgramID.<value> 
        public int ProgramID { get { return this.intProgramID; } set { this.intProgramID = value; } }

        /// <summary>Gets or sets the SoldToCustomerID.</summary>
        /// <value>The SoldToCustomerID.<value> 
        public string SoldToCustomerID { get { return this.strSoldToCustomerID; } set { this.strSoldToCustomerID = value; } }

        /// <summary>Gets or sets the SoldToCustomerName.</summary>
        /// <value>The SoldToCustomerName.<value> 
        public string SoldToCustomerName { get { return this.strSoldToCustomerName; } set { this.strSoldToCustomerName = value; } }

        /// <summary>Gets or sets the SoldToStreet.</summary>
        /// <value>The SoldToStreet.<value> 
        public string SoldToStreet { get { return this.strSoldToStreet; } set { this.strSoldToStreet = value; } }

        /// <summary>Gets or sets the SoldToCity.</summary>
        /// <value>The SoldToCity.<value> 
        public string SoldToCity { get { return this.strSoldToCity; } set { this.strSoldToCity = value; } }

        /// <summary>Gets or sets the SoldToStateCode.</summary>
        /// <value>The SoldToStateCode.<value> 
        public string SoldToStateCode { get { return this.strSoldToStateCode; } set { this.strSoldToStateCode = value; } }

        /// <summary>Gets or sets the SoldToPostalCode.</summary>
        /// <value>The SoldToPostalCode.<value> 
        public string SoldToPostalCode { get { return this.strSoldToPostalCode; } set { this.strSoldToPostalCode = value; } }

        /// <summary>Gets or sets the RvpName.</summary>
        /// <value>The RvpName.<value> 
        public string RvpName { get { return this.strRvpName; } set { this.strRvpName = value; } }

        /// <summary>Gets or sets the DistrictName.</summary>
        /// <value>The DistrictName.<value> 
        public string DistrictName { get { return this.strDistrictName; } set { this.strDistrictName = value; } }

        /// <summary>Gets or sets the DistrictID.</summary>
        /// <value>The DistrictID.<value> 
        public string DistrictID { get { return this.strDistrictID; } set { this.strDistrictID = value; } }

        /// <summary>Gets or sets the SalesRepName.</summary>
        /// <value>The SalesRepName.<value> 
        public string SalesRepName { get { return this.strSalesRepName; } set { this.strSalesRepName = value; } }

        /// <summary>Gets or sets the SalesRepID.</summary>
        /// <value>The SalesRepID.<value> 
        public string SalesRepID { get { return this.strSalesRepID; } set { this.strSalesRepID = value; } }

        /// <summary>Gets or sets the GpoDesc.</summary>
        /// <value>The GpoDesc.<value> 
        public string GpoDesc { get { return this.strGpoDesc; } set { this.strGpoDesc = value; } }

        /// <summary>Gets or sets the CustGroup1.</summary>
        /// <value>The CustGroup1.<value> 
        public string CustGroup1 { get { return this.strCustGroup1; } set { this.strCustGroup1 = value; } }

        /// <summary>Gets or sets the CustGroup1ID.</summary>
        /// <value>The CustGroup1ID.<value> 
        public string CustGroup1ID { get { return this.strCustGroup1ID; } set { this.strCustGroup1ID = value; } }

        /// <summary>Gets or sets the AgreementTerms.</summary>
        /// <value>The AgreementTerms.<value> 
        public string AgreementTerms { get { return this.strAgreementTerms; } set { this.strAgreementTerms = value; } }

        /// <summary>Gets or sets the BusinessJustification.</summary>
        /// <value>The BusinessJustification.<value> 
        public string BusinessJustification { get { return this.strBusinessJustification; } set { this.strBusinessJustification = value; } }

        /// <summary>Gets or sets the Competitor.</summary>
        /// <value>The Competitor.<value> 
        public string Competitor { get { return this.strCompetitor; } set { this.strCompetitor = value; } }

        /// <summary>Gets or sets the IncrementalRevenueEstimate.</summary>
        /// <value>The IncrementalRevenueEstimate.<value> 
        public string IncrementalRevenueEstimate { get { return this.strIncrementalRevenueEstimate; } set { this.strIncrementalRevenueEstimate = value; } }

        /// <summary>Gets or sets the IncrementalRevenueProbability.</summary>
        /// <value>The IncrementalRevenueProbability.<value> 
        public string IncrementalRevenueProbability { get { return this.strIncrementalRevenueProbability; } set { this.strIncrementalRevenueProbability = value; } }

        /// <summary>Gets or sets the FormStamp.</summary>
        /// <value>The FormStamp.<value> 
        public string FormStamp { get { return this.strFormStamp; } set { this.strFormStamp = value; } }

        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.<value> 
        public string CreatedBy { get { return this.strCreatedBy; } set { this.strCreatedBy = value; } }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.<value> 
        public string CreateDate { get { return this.strCreateDate; } set { this.strCreateDate = value; } }

        /// <summary>Gets or sets the ModifiedBy.</summary>
        /// <value>The ModifiedBy.<value> 
        public string ModifiedBy { get { return this.strModifiedBy; } set { this.strModifiedBy = value; } }

        /// <summary>Gets or sets the SubmittedByName.</summary>
        /// <value>The SubmittedByName.<value> 
        public string SubmittedByName { get { return this.strSubmittedByName; } set { this.strSubmittedByName = value; } }

        /// <summary>Gets or sets the SubmitDate.</summary>
        /// <value>The SubmitDate.<value> 
        public string SubmitDate { get { return this.strSubmitDate; } set { this.strSubmitDate = value; } }

        /// <summary>Gets or sets the ModifiedDate.</summary>
        /// <value>The ModifiedDate.<value> 
        public string ModifiedDate { get { return this.strModifiedDate; } set { this.strModifiedDate = value; } }

        /// <summary>Gets or sets the PricingRequestStatus.</summary>
        /// <value>The PricingRequestStatus.<value> 
        public string PricingRequestStatus { get { return this.strPricingRequestStatus; } set { this.strPricingRequestStatus = value; } }

        /// <summary>Gets or sets the PricingRequestStatus.</summary>
        /// <value>The 12 Month Rolling Revenue.<value> 
        public double Request12RollingRev { get { return this.dblRequest12RollingRev; } set { this.dblRequest12RollingRev = value; } }

        /// <summary>Gets or sets the Request ToDate Revenue.</summary>
        /// <value>The Request ToDate Revenue.<value> 
        public double RequestToDateRev { get { return this.dblRequestToDateRev; } set { this.dblRequestToDateRev = value; } }

        /// <summary>Gets or sets the To-Date Updated On.</summary>
        /// <value>The Request To-Date Updated On.<value> 
        public string ToDateUpdatedOn { get { return this.strToDateUpdatedOn; } set { this.strToDateUpdatedOn = value; } }
        
        /// <summary>Gets The PricingRequestEntity by the PricingRequestEntityID</summary>
        /// <param name="intPricingRequestID">The PricingRequest ID</param>
        /// <returns>The PricingRequestEntity</returns>
        public static PricingRequestEntity GetPricingRequestByPricingRequestID(int intPricingRequestID)
        {
            PricingRequestEntity pr = null;
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingRequestByPricingRequestID", myParamters);
            if (dt.Rows.Count > 0)
            {
                pr = PricingRequestEntity.GetEntityFromDataRow(dt.Rows[0]);
            }

            return pr;
        }

        /// <summary>Gets The PricingRequestEntity List for the Search Query</summary>
        /// <param name="sQuery">the dynamic search query</param>
        /// <returns>The PricingRequestEntity List</returns>
        public static List<PricingRequestEntity> GetPricingRequestSearch(string sQuery)
        {
            DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);
//            DataTable dt = SQLUtility.OracleExecuteDynamicQuery(sQuery);

            List<PricingRequestEntity> myPriceRequests = new List<PricingRequestEntity>();

            foreach (DataRow r in dt.Rows)
            {
                PricingRequestEntity pr = PricingRequestEntity.GetEntityFromDataRow(r);
                myPriceRequests.Add(pr);
            }

            return myPriceRequests;

        }


///*** Remove if using Step Process */
//        /// <summary>Gets The PricingRequest Request Entity by the FormStamp</summary>
//        /// <param name="strFormStamp">The Form Stamp</param>
//        /// <returns>The PricingRequest Entity</returns>
//        public static PricingRequestEntity GetPricingRequestByFormStamp(string strFormStamp)
//        {
//            PricingRequestEntity pr = null;
//            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
//            myParamters.Add(new SqlParameter("@FormStamp", strFormStamp));

//            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPricingRequestByFormStamp", myParamters);
//            if (dt.Rows.Count > 0)
//            {
//                pr = PricingRequestEntity.GetEntityFromDataRow(dt.Rows[0]);
//            }

//            return pr;
//        }

        /// <summary>Receives a PriceRequest datarow and converts it to a PricingRequestEntity.</summary>
        /// <param name="r">The PriceRequest DataRow.</param>
        /// <returns>PricingRequestEntity</returns>
        public static PricingRequestEntity GetEntityFromDataRow(DataRow r)
        {
            PricingRequestEntity pr = new PricingRequestEntity();

            if ((r["PricingRequestID"] != null) && (r["PricingRequestID"] != DBNull.Value))
                pr.PricingRequestID = Convert.ToInt32(r["PricingRequestID"].ToString());

            if ((r["ProgramID"] != null) && (r["ProgramID"] != DBNull.Value))
                pr.ProgramID = Convert.ToInt32(r["ProgramID"].ToString());

            pr.SoldToCustomerID = r["SoldToCustomerID"].ToString();
            pr.SoldToCustomerName = r["SoldToCustomerName"].ToString();
            pr.SoldToStreet = r["SoldToStreet"].ToString();
            pr.SoldToCity = r["SoldToCity"].ToString();
            pr.SoldToStateCode = r["SoldToStateCode"].ToString();
            pr.SoldToPostalCode = r["SoldToPostalCode"].ToString();
            pr.RvpName = r["RvpName"].ToString();
            pr.DistrictName = r["DistrictName"].ToString();
            pr.DistrictID = r["DistrictID"].ToString();
            pr.SalesRepName = r["SalesRepName"].ToString();
            pr.SalesRepID = r["SalesRepID"].ToString();
            pr.GpoDesc = r["GpoDesc"].ToString();
            pr.CustGroup1 = r["CustGroup1"].ToString();
            pr.CustGroup1ID = r["CustGroup1ID"].ToString();
            pr.AgreementTerms = r["AgreementTerms"].ToString();
            pr.BusinessJustification = r["BusinessJustification"].ToString();
            pr.Competitor = r["Competitor"].ToString();
            pr.IncrementalRevenueEstimate = r["IncrementalRevenueEstimate"].ToString();
            pr.IncrementalRevenueProbability = r["IncrementalRevenueProbability"].ToString();
            pr.FormStamp = r["FormStamp"].ToString();
            pr.CreatedBy = r["CreatedBy"].ToString();
            pr.CreateDate = r["CreateDate"].ToString();
            pr.ModifiedBy = r["ModifiedBy"].ToString();
            pr.SubmittedByName = r["SubmittedByName"].ToString();
            pr.ModifiedDate = r["ModifiedDate"].ToString();
            pr.PricingRequestStatus = r["PricingRequestStatus"].ToString();

            if ((r.Table.Columns.Contains("Request12RollingRev")) && (r["Request12RollingRev"] != DBNull.Value))
                pr.Request12RollingRev = Convert.ToDouble(r["Request12RollingRev"].ToString());

            if ((r.Table.Columns.Contains("RequestToDateRev")) && (r["RequestToDateRev"] != DBNull.Value))
                pr.RequestToDateRev = Convert.ToDouble(r["RequestToDateRev"].ToString());

            if ((r.Table.Columns.Contains("ToDateUpdatedOn")) && (r["ToDateUpdatedOn"] != DBNull.Value))
                pr.ToDateUpdatedOn = r["ToDateUpdatedOn"].ToString();

            if (r.Table.Columns.Contains("SubmitDate"))
                pr.SubmitDate = r["SubmitDate"].ToString();

            return pr;
        }

        /// <summary>Inserts The Pricing Request into the database and updates the customers</summary>
        /// <param name="pr">PricingRequestEntity object</param>
        /// <param name="addCustomers">PricingCustomerEntity Collection of Additional </param>
        /// <returns>The Pricing Request Entity</returns>
        public static PricingRequestEntity CopyPricingRequest(int intPricingRequestID, string strUsername)
        {
            PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);

            pr.SubmittedByName = string.Empty;
            pr.CreatedBy = strUsername;
            pr.ModifiedBy = strUsername;
            pr.FormStamp = strUsername + DateTime.Now.ToUniversalTime().ToString().Replace(" ", string.Empty);
            pr.PricingRequestStatus = "In Progress";

            Collection<PricingCustomerEntity> addCustomers = PricingCustomerEntity.GetPricingCustomerByPricingRequestIDByPricingCustomerTypeID(intPricingRequestID, 1);

            PricingRequestEntity newPR = PricingRequestEntity.InsertPricingRequest(pr, addCustomers);

            DataTable dtMaterials = PricingRequestMaterialEntity.GetPriceRequestByCustomerTable(pr.SoldToCustomerID, pr.SoldToStateCode);

            

            List<PricingRequestMaterialEntity> materials = PricingRequestMaterialEntity.GetPricingRequestMaterialByPricingRequestID(intPricingRequestID);

            List<PricingRequestMaterialEntity> newMaterials = new List<PricingRequestMaterialEntity>();
            foreach (PricingRequestMaterialEntity m in materials)
            {
                DataRow[] rows = dtMaterials.Select("Material_Id = '" + m.Material_Id + "'");
                if(rows.Length > 0)
                {
                    PricingRequestMaterialEntity x = PricingRequestMaterialEntity.GetEntityFromDataRow(rows[0]);
                    x.Requested_Price = m.Requested_Price;
                    x.Competitor_Price = m.Competitor_Price;
                    x.Expected_Volume = m.Expected_Volume;
                    x.PricingRequestID = newPR.PricingRequestID;

                    newMaterials.Add(x);
                }
                
            }

            int intUpdates = PricingRequestMaterialEntity.SavePricingRequestMaterialsInDatabase(newPR.PricingRequestID, newMaterials);

            return newPR;
        }

        /// <summary>Inserts The Pricing Request into the database and updates the customers</summary>
        /// <param name="pr">PricingRequestEntity object</param>
        /// <param name="addCustomers">PricingCustomerEntity Collection of Additional </param>
        /// <returns>The Pricing Request Entity</returns>
        public static PricingRequestEntity InsertPricingRequest(PricingRequestEntity pr, Collection<PricingCustomerEntity> addCustomers)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

            SqlParameter SQLID = new SqlParameter("@PricingRequestID", pr.PricingRequestID);
            SQLID.Direction = ParameterDirection.Output;
            myParamters.Add(SQLID);

            myParamters.Add(new SqlParameter("@ProgramID", pr.ProgramID));
            myParamters.Add(new SqlParameter("@SoldToCustomerID", pr.SoldToCustomerID));
            myParamters.Add(new SqlParameter("@SoldToCustomerName", pr.SoldToCustomerName));
            myParamters.Add(new SqlParameter("@SoldToStreet", pr.SoldToStreet));
            myParamters.Add(new SqlParameter("@SoldToCity", pr.SoldToCity));
            myParamters.Add(new SqlParameter("@SoldToStateCode", pr.SoldToStateCode));
            myParamters.Add(new SqlParameter("@SoldToPostalCode", pr.SoldToPostalCode));
            myParamters.Add(new SqlParameter("@RvpName", pr.RvpName));
            myParamters.Add(new SqlParameter("@DistrictName", pr.DistrictName));
            myParamters.Add(new SqlParameter("@DistrictID", pr.DistrictID));
            myParamters.Add(new SqlParameter("@SalesRepName", pr.SalesRepName));
            myParamters.Add(new SqlParameter("@SalesRepID", pr.SalesRepID));
            myParamters.Add(new SqlParameter("@GpoDesc", pr.GpoDesc));
            myParamters.Add(new SqlParameter("@CustGroup1", pr.CustGroup1));
            myParamters.Add(new SqlParameter("@CustGroup1ID", pr.CustGroup1ID));
            myParamters.Add(new SqlParameter("@AgreementTerms", pr.AgreementTerms));
            myParamters.Add(new SqlParameter("@BusinessJustification", pr.BusinessJustification));
            myParamters.Add(new SqlParameter("@Competitor", pr.Competitor));
            myParamters.Add(new SqlParameter("@IncrementalRevenueEstimate", pr.IncrementalRevenueEstimate));
            myParamters.Add(new SqlParameter("@IncrementalRevenueProbability", pr.IncrementalRevenueProbability));
            myParamters.Add(new SqlParameter("@FormStamp", pr.FormStamp));
            myParamters.Add(new SqlParameter("@ModifiedBy", pr.ModifiedBy));
            myParamters.Add(new SqlParameter("@SubmittedByName", pr.SubmittedByName));
            myParamters.Add(new SqlParameter("@PricingRequestStatus", pr.PricingRequestStatus));

            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertPricingRequest", myParamters, conn, trans);

            if (insertParameteres["@PricingRequestID"] != null)
            {
                pr.PricingRequestID = Convert.ToInt32(insertParameteres["@PricingRequestID"].Value.ToString());
                Debug.WriteLine("Inserted PricingRequestID: " + pr.PricingRequestID.ToString());
            }

            PricingRequestEntity.UpdatePricingCustomers(pr.PricingRequestID, addCustomers, conn, trans);


            trans.Commit();

            SQLUtility.CloseSqlConnection(conn);
            return pr;
        }

        /// <summary>Updates The Pricing Request into the database and updates the customers</summary>
        /// <param name="pr">PricingRequestEntity object</param>
        /// <param name="addCustomers">PricingCustomerEntity Collection of Additional </param>
        /// <returns>The Pricing Request Entity</returns>
        public static PricingRequestEntity UpdatePricingRequest(PricingRequestEntity pr, Collection<PricingCustomerEntity> addCustomers)
        {
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            try
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

                myParamters.Add(new SqlParameter("@PricingRequestID", pr.PricingRequestID));
                myParamters.Add(new SqlParameter("@ProgramID", pr.ProgramID));
                myParamters.Add(new SqlParameter("@SoldToCustomerID", pr.SoldToCustomerID));
                myParamters.Add(new SqlParameter("@SoldToCustomerName", pr.SoldToCustomerName));
                myParamters.Add(new SqlParameter("@SoldToStreet", pr.SoldToStreet));
                myParamters.Add(new SqlParameter("@SoldToCity", pr.SoldToCity));
                myParamters.Add(new SqlParameter("@SoldToStateCode", pr.SoldToStateCode));
                myParamters.Add(new SqlParameter("@SoldToPostalCode", pr.SoldToPostalCode));
                myParamters.Add(new SqlParameter("@RvpName", pr.RvpName));
                myParamters.Add(new SqlParameter("@DistrictName", pr.DistrictName));
                myParamters.Add(new SqlParameter("@DistrictID", pr.DistrictID));
                myParamters.Add(new SqlParameter("@SalesRepName", pr.SalesRepName));
                myParamters.Add(new SqlParameter("@SalesRepID", pr.SalesRepID));
                myParamters.Add(new SqlParameter("@GpoDesc", pr.GpoDesc));
                myParamters.Add(new SqlParameter("@CustGroup1", pr.CustGroup1));
                myParamters.Add(new SqlParameter("@CustGroup1ID", pr.CustGroup1ID));
                myParamters.Add(new SqlParameter("@AgreementTerms", pr.AgreementTerms));
                myParamters.Add(new SqlParameter("@BusinessJustification", pr.BusinessJustification));
                myParamters.Add(new SqlParameter("@Competitor", pr.Competitor));
                myParamters.Add(new SqlParameter("@IncrementalRevenueEstimate", pr.IncrementalRevenueEstimate));
                myParamters.Add(new SqlParameter("@IncrementalRevenueProbability", pr.IncrementalRevenueProbability));
                myParamters.Add(new SqlParameter("@FormStamp", pr.FormStamp));
                myParamters.Add(new SqlParameter("@ModifiedBy", pr.ModifiedBy));
                myParamters.Add(new SqlParameter("@SubmittedByName", pr.SubmittedByName));
                myParamters.Add(new SqlParameter("@PricingRequestStatus", pr.PricingRequestStatus));

                int intUpdateCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_UpdatePricingRequest", myParamters, conn, trans);

                PricingRequestEntity.UpdatePricingCustomers(pr.PricingRequestID, addCustomers, conn, trans);

                trans.Commit();

                SQLUtility.CloseSqlConnection(conn);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("UpdatePricingRequest Error: " + ex.Message);
                
                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);
                throw (ex);

            }

            return pr;
        }


        /// <summary>Updates The Pricing Request PricingRequestStatus in the PricingRequest database table</summary>
        /// <param name="pr">PricingRequestEntity object</param>
        /// <returns>The number of records updated</returns>
        public static int UpdatePricingRequestStatus(PricingRequestEntity pr)
        {
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();
            int intUpdateCnt = 0;

            try
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

                myParamters.Add(new SqlParameter("@PricingRequestID", pr.PricingRequestID));
                myParamters.Add(new SqlParameter("@ModifiedBy", pr.ModifiedBy));
                myParamters.Add(new SqlParameter("@SubmittedByName", pr.SubmittedByName));
                myParamters.Add(new SqlParameter("@PricingRequestStatus", pr.PricingRequestStatus));

                intUpdateCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_UpdatePricingRequestStatusByPricingRequestID", myParamters, conn, trans);
                trans.Commit();

                SQLUtility.CloseSqlConnection(conn);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("UpdatePricingRequestStatus Error: " + ex.Message);

                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);
                throw (ex);
            }

            return intUpdateCnt;
        }

        /// <summary>Updates the PricingCustomer records for the Pricing Request</summary>
        /// <param name="intPricingRequestID">intPricingRequest ID</param>
        /// <param name="addCustomers">PricingCustomerEntity Collection of Additional </param>
        /// <param name="conn">The SqlConnection.</param>
        /// <param name="trans">The SqlTransaction.</param>
        /// <returns>The number of records affected</returns>
        protected static void UpdatePricingCustomers(int intPricingRequestID, Collection<PricingCustomerEntity> addCustomers, SqlConnection conn, SqlTransaction trans)
        {
            Collection<SqlParameter> deleteParameters = new Collection<SqlParameter>();
            deleteParameters.Add(new SqlParameter("PricingRequestID", intPricingRequestID));
            int deletedCustomerCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_DeletePricingCustomerByPricingRequestID", deleteParameters, conn, trans);

            foreach (PricingCustomerEntity cust in addCustomers)
            {
                Collection<SqlParameter> addCustParameters = new Collection<SqlParameter>();
                addCustParameters.Add(new SqlParameter("PricingRequestID", intPricingRequestID));
                addCustParameters.Add(new SqlParameter("PricingCustomerTypeID", cust.PricingCustomerTypeID));
                addCustParameters.Add(new SqlParameter("CustomerID", cust.CustomerID));
                addCustParameters.Add(new SqlParameter("CustomerName", cust.CustomerName));
                int addCustomerCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_InsertSmallPricingCustomer", addCustParameters, conn, trans);
            }

        }

        /// <summary>Loads the Sales ToDate and !2 Month Rolling to the Orders List for each Order.</summary>
        /// <param name="orders">The List of PriceRequestSales Orders</param>
        /// <returns>the Orders List after updating</returns>
        public static PricingRequestEntity LoadToDateRevenues(int intPricingRequestID)
        {
            PricingRequestEntity pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(intPricingRequestID);
            string strMaterialList = PricingRequestEntity.GetPriceRequestMaterialListByPricingRequestID(pr.PricingRequestID);

            if (strMaterialList != string.Empty)
            {
 //               SqlConnection gdwConn = null;

                try
                {
//                    gdwConn = SQLUtility.OpenGdwSqlConnection();

                    // Load the Request To-Date Revenue
                    string strRevenueQuery = PricingRequestEntity.GetQueryByQueryName("PricingRequestRevenueToDate");

                    // Load the 12 Month Rolling Revenue
                    string strRollingRevenueQuery = PricingRequestEntity.GetQueryByQueryName("PricingRequest12MonthRollingRevenue");

                    string[] arrayDate = pr.CreateDate.Split(' ');
                    string strCreateDate = arrayDate[0];

                    string strDynamicQuery = PricingRequestEntity.SetGDWQueryVariables(strRevenueQuery, strMaterialList, pr.SoldToCustomerID, strCreateDate);

                    Debug.WriteLine("strDynamicQuery: " + strDynamicQuery);

//                    DataTable dtGDW = SQLUtility.SqlExecuteDynamicQuery(strDynamicQuery, gdwConn);
                    DataTable dtGDW = SQLUtility.OracleExecuteDynamicQuery(strDynamicQuery);

                    if (dtGDW.Rows.Count > 0)
                    {
                        DataRow r = dtGDW.Rows[0];
                        if (r.Table.Columns.Contains("RequestToDateRev") && (r["RequestToDateRev"] != DBNull.Value))
                            pr.RequestToDateRev = Convert.ToDouble(r["RequestToDateRev"].ToString());
                    }

                    string strRollingDynamicQuery = PricingRequestEntity.SetGDWQueryVariables(strRollingRevenueQuery, strMaterialList, pr.SoldToCustomerID, strCreateDate);

//                    DataTable dtRollingGDW = SQLUtility.SqlExecuteDynamicQuery(strRollingDynamicQuery, gdwConn);
                    DataTable dtRollingGDW = SQLUtility.OracleExecuteDynamicQuery(strRollingDynamicQuery);

                    if (dtRollingGDW.Rows.Count > 0)
                    {
                        DataRow r = dtRollingGDW.Rows[0];
                        if (r.Table.Columns.Contains("Request12RollingRev") && (r["Request12RollingRev"] != DBNull.Value))
                            pr.Request12RollingRev = Convert.ToDouble(r["Request12RollingRev"].ToString());
                    }

//                    SQLUtility.CloseSqlConnection(gdwConn);

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("LoadRevenuesToOrdersList Error: " + ex.Message);
//                    SQLUtility.CloseSqlConnection(gdwConn);
                    throw (ex);
                }
            }
            else
            {
                pr.RequestToDateRev = 0.00;
                pr.Request12RollingRev = 0.00;
            }

            PricingRequestEntity.UpdatePriceRequestsToDateRevenueByPriceRequestID(pr);

            pr = PricingRequestEntity.GetPricingRequestByPricingRequestID(pr.PricingRequestID);


            return pr;
        }

        /// <summary>Get Submitted PriceRequest Material List.</summary>
        /// <param name="intPricingRequestID">The Pricing Request ID</param>
        /// <returns>The newly populated PriceRequestSales List</returns>
        public static string GetPriceRequestMaterialListByPricingRequestID(int intPricingRequestID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@PricingRequestID", intPricingRequestID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPriceRequestMaterialListByPricingRequestID", parameters);

            if (dt.Rows.Count > 0)
                return dt.Rows[0]["MaterialList"].ToString();
            else
                return string.Empty;

        }

        /// <summary>This retrives the query string using the QueryName.</summary>
        /// <param name="strQueryName">The name of the query</param>
        /// <returns>the query as a string</returns>
        protected static string GetQueryByQueryName(string strQueryName)
        {
            string sQuery = string.Empty;
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@QueryName", strQueryName));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetQueryByQueryName", parameters);
            if (dt.Rows.Count > 0)
            {
                sQuery = dt.Rows[0]["Query"].ToString();
            }

            return sQuery;
        }

        /// <summary> Sets the variables for the dynamic query</summary>
        /// <param name="strQuery">The base query</param>
        /// <param name="strMaterialWhere">The Material Where Clause</param>
        /// <param name="strCustomerID">The Customer ID</param>
        /// <param name="strOrderDate">The Order Date string</param>
        /// <returns>the revised Query</returns>
        protected static string SetGDWQueryVariables(string strQuery, string strMaterialWhere, string strCustomerID, string strOrderDate)
        {
            strQuery = strQuery.Replace("MaterialWhereClause", strMaterialWhere);
            strQuery = strQuery.Replace("xxCUSTOMERIDxx", strCustomerID);
            strQuery = strQuery.Replace("xxORDERDATExx", strOrderDate);

            //Debug.WriteLine("strQuery: " + strQuery);

            return strQuery;
        }



        /// <summary>Updates The Pricing Request To-Date Revenue in the PricingRequest database table</summary>
        /// <param name="pr">PricingRequestEntity object</param>
        /// <returns>The number of records updated</returns>
        public static int UpdatePriceRequestsToDateRevenueByPriceRequestID(PricingRequestEntity pr)
        {
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();
            int intUpdateCnt = 0;

            try
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

                myParamters.Add(new SqlParameter("@PricingRequestID", pr.PricingRequestID));
                myParamters.Add(new SqlParameter("@Request12RollingRev", pr.Request12RollingRev));
                myParamters.Add(new SqlParameter("@RequestToDateRev", pr.RequestToDateRev));

                intUpdateCnt = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_UpdatePriceRequestsToDateRevenueByPriceRequestID", myParamters, conn, trans);
                trans.Commit();

                SQLUtility.CloseSqlConnection(conn);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("sp_UpdatePriceRequestsToDateRevenueByPriceRequestID Error: " + ex.Message);

                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);
                throw (ex);
            }

            return intUpdateCnt;
        }

    }//// end class
}