﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="SalesRequestForms.Admin.UserList" MasterPageFile="~/Site.Master" Title="User List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="SearchPage">
        <table>
            <tr>
                <td style="font-size: large;text-decoration: underline;"><strong>Admininstrator User List</strong>
                    <asp:HiddenField ID="hdnProgramID" runat="server" />
                </td>
            </tr>           
            <tr>
                <td>
                <asp:GridView ID="gvUser" runat="server" Width="950px" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no users." OnRowDeleting="GV_Delete"  ShowFooter="true" OnRowUpdating="GV_Update" OnRowEditing="GV_Edit"
                     CellPadding="2" DataKeyNames="userID, userName, roleID, SendEmail" GridLines="Both" OnRowCancelingEdit="GV_Add">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 

                       <asp:TemplateField HeaderStyle-Width="15%" HeaderText="UserName" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" Font-Size="8pt" runat="server"  Text='<%# Eval("username")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddUserName" Width="150px" MaxLength="30" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredAddUserName" ValidationGroup="Add" ControlToValidate="txtAddUserName" runat="server"  ErrorMessage="User Name is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldUserExists" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name already exists.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldAddUserInAD" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name is not valid.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:BoundField DataField="firstname" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="First Name" />                       
                        <asp:BoundField DataField="lastname" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Last Name" />                       
                        <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Role" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRole" Font-Size="8pt" runat="server"  Text='<%# Eval("roleName")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlRole" width="125px" DataTextField="RoleName" DataValueField="RoleID" DataSource='<%# GetRoles()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldRole" ControlToValidate="ddlRole" ValidationGroup="Add" ForeColor="#FF7300" runat="server" ErrorMessage="Role is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField> 
                         <asp:BoundField DataField="RoleTypeName" HeaderStyle-Width="10%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Role Type" />                       
                       <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblSendEmail" Font-Size="8pt" runat="server"  Text='<%# Eval("SendEmail")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                 <asp:DropDownList ID="ddlSendEmail" Font-Size="8pt" runat="server">
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                 </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Territories" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblTerritories" Font-Size="8pt" runat="server"  Text='<%# Eval("Territories")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:GridView ID="gvTerritory" runat="server" Width="245px" AutoGenerateColumns="False" OnRowDataBound="GV_RowDataBoundTerritory"
                                    EmptyDataText="There are no territories." OnRowDeleting="GV_DeleteTerritory" ShowFooter="true" ShowHeader="false"
                                    OnRowCancelingEdit="GV_AddTerritory" CellPadding="2" DataKeyNames="UserTerritoryID" BorderStyle="None" GridLines="Both">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button id="btnTerritoryDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
									                ></asp:Button>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button id="btnTerritoryAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							                </FooterTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Territory ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTerritory" Font-Size="8pt" runat="server"  Text='<%# Eval("Territory")%>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtTerritory" MaxLength="20" Width="80px" Font-Size="8pt" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldRequiredTerritory" ValidationGroup="AddTerritory" ControlToValidate="txtTerritory" runat="server"  ErrorMessage="Territory is required.">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="vldTerritoryAssigned" ValidationGroup="AddTerritory" runat="server" ControlToValidate="txtTerritory" ErrorMessage="Territory is already assigned to another user."></asp:CustomValidator>   
                                                <asp:RegularExpressionValidator ID="vldDistributorID" ValidationGroup="AddTerritory" ControlToValidate="txtTerritory" runat="server" ErrorMessage="ASD/DE/ESD Territory ID must be 6 digits long." ValidationExpression="\d{6}?">*</asp:RegularExpressionValidator>
                                            </FooterTemplate>
                                        </asp:TemplateField>                            
                                    </Columns>
                                </asp:GridView>

                            </EditItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnEdit" ValidationGroup="Edit" Width="50px" Height="18" runat="server" Text="Edit" CommandName="Edit"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
									></asp:Button>
                            </EditItemTemplate>
                        </asp:TemplateField> 
                   </Columns>
                </asp:GridView> 
                
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vldSummary1"  ValidationGroup="FindUser" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><b>Find User Name</b></td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width:10%">Last Name:</td>
                            <td style="width:25%"><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" runat="server"></asp:TextBox></td>
                                <asp:RequiredFieldValidator ID="vldFindLastName" ValidationGroup="FindUser" ControlToValidate="txtLastName" runat="server" ErrorMessage="Find User - Last name is required.">*</asp:RequiredFieldValidator>
                            <td>
                                <asp:Button id="btnFindUser" ValidationGroup="FindUser" runat="server" Text="Find User"
				                    CssClass="buttonsSubmit" CausesValidation="false" onclick="btnFindUser_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:GridView ID="gvFindUsers" runat="server" Width="800" AutoGenerateColumns="False"
                        EmptyDataText="There are no users." OnRowEditing="GV_Select"
                         CellPadding="2" DataKeyNames="username" GridLines="Both" 
                        >
                        <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnSelect" Width="50px" Height="18" runat="server" Text="Select" CommandName="Edit"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								        ></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:BoundField DataField="username" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="UserName" />                       
                            <asp:BoundField DataField="firstname" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="First Name" />                       
                            <asp:BoundField DataField="lastname" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="Last Name" />                       
                            <asp:BoundField DataField="company" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="Company" />                       
                       </Columns>
                    </asp:GridView>                     
                </td>
            </tr>
       </table>    
    </div>
</asp:Content>
