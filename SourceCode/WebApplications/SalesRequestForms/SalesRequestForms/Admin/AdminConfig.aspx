﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeBehind="AdminConfig.aspx.cs" Inherits="SalesRequestForms.Admin.AdminConfig"  MasterPageFile="~/Site.Master" Title="Admin Config"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="SearchPage">
        <table width="960px">
             <tr>
                <td style="font-weight:bold; text-decoration:underline">App Settings</td>
            </tr>
           <tr>
                <td>
                        <asp:GridView ID="gvAppSettings" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no App Settings." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update"
                             CellPadding="2" GridLines="Both" DataKeyNames="SettingKey">
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                           <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="UpdateCostCenter" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="AddCostCenter" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Key">
                                <ItemTemplate>
                                    <asp:Label ID="lblSettingKeyr" Font-Size="8pt" runat="server"  Text='<%# Eval("SettingKey")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAddSettingKey" MaxLength="30" Width="100px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddCostCenter" ValidationGroup="AddSetting" ControlToValidate="txtAddSettingKey" runat="server"  ErrorMessage="Key is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldSettingKeyExists" runat="server" ValidationGroup="AddSetting" ControlToValidate="txtAddSettingKey" ErrorMessage="The Key already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="58%" HeaderText="Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblSettingValue" Font-Size="8pt" runat="server"  Text='<%# Eval("SettingValue")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSettingValue" MaxLength="100" Width="250px" Font-Size="8pt" runat="server" Text='<%# Eval("SettingValue")%>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAddSettingValue" MaxLength="100" Width="250px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddSettingValue" ValidationGroup="AddSetting" ControlToValidate="txtAddSettingValue" runat="server"  ErrorMessage="Value is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddSetting" runat="server" />
                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateSetting" runat="server" />
                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="UpdateSMTP" runat="server" />
                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="UpdateConnectionString" runat="server" />
                    <asp:Label ID="lblUpdateMsg" Font-Size="10pt" ForeColor="Red" runat="server"  Text=""></asp:Label>
                </td>
            </tr>
        </table>
        <table width="960px">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-weight:bold; text-decoration:underline">Connection String Information</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvConnString" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBoundConnSTring"
                        EmptyDataText="There are no Connection Strings."  ShowFooter="false" OnRowEditing="GV_EditConnString" OnRowUpdating="GV_UpdateConString"
                            CellPadding="2" GridLines="Both" DataKeyNames="SettingName">
                        <RowStyle HorizontalAlign="left" />
                        <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                        <Columns>
                            <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
								    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button id="btnUpdateConn" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
								    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ValidationGroup="UpdateConnectionString" ></asp:Button>
                            </EditItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblSettingName" Font-Size="8pt" runat="server"  Text='<%# Eval("SettingName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="79%" HeaderText="Connection String">
                            <ItemTemplate>
                                <asp:Label ID="lblConnectionString" Font-Size="8pt" runat="server"  Text='<%# Eval("ConnectionString")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtConnectionString" MaxLength="100" TextMode="MultiLine" Rows="3" Width="700px" Font-Size="9pt" runat="server" Text='<%# Eval("ConnectionString")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredUpdateSettingValue" ValidationGroup="UpdateConnectionString" ControlToValidate="txtConnectionString" runat="server"  ErrorMessage="Connection String is required.">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </asp:TemplateField>                            
                        </Columns>
                    </asp:GridView> 
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight:bold; text-decoration:underline">SMTP Information</td>
            </tr>
            <tr>
                <td style="width:10%">Host:</td>
                <td><asp:TextBox ID="txtHost" MaxLength="100" Width="250px" Font-Size="8pt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldRequiredHost" ValidationGroup="UpdateSMTP" ControlToValidate="txtHost" runat="server"  ErrorMessage="SMTP Host is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Port:</td>
                <td><asp:TextBox ID="txtPort" MaxLength="5" Width="250px" Font-Size="8pt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldRequiredPort" ValidationGroup="UpdateSMTP" ControlToValidate="txtPort" runat="server"  ErrorMessage="SMTP Port is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Button id="btnUpdate" Width="100px" Height="18" runat="server"  Text="Update SMTP"
				    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
					ValidationGroup="UpdateSMTP" onclick="btnUpdate_Click" ></asp:Button></td>
            </tr>
         </table>     
    </div>
</asp:Content>
