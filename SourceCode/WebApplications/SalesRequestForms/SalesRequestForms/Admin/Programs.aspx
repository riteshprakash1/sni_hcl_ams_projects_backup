﻿<%@ Page Title="Programs - Administration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Programs.aspx.cs" Inherits="SalesRequestForms.Admin.Programs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="font-size: large;text-decoration: underline;"><strong>Program Admininstration</strong></td>
        </tr>          
        <tr>
            <td><asp:Button id="btnNewProgram" Width="130px" Height="22px" runat="server" Text="Add New Program" 
				    Font-Size="10pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" 
                    BorderColor="White" onclick="btnNewProgram_Click"></asp:Button></td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <asp:GridView ID="gvPrograms" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no programs." ShowFooter="false" OnRowEditing="GV_View"
                        CellPadding="2" GridLines="Both" DataKeyNames="programID" AllowPaging="false">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                        <asp:ButtonField ItemStyle-Width="6%" ButtonType="Button" CausesValidation="false" Text="View" CommandName="Edit" />
                        <asp:BoundField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Program Name" DataField="programName" />
                        <asp:BoundField ItemStyle-Width="59%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Overview" DataField="overview" />
                        <asp:BoundField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Status" DataField="programStatus" />
                         <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Guide" ItemStyle-VerticalAlign="Top" >
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkGuide" Target="_blank" runat="server" Text="Admin"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
    </table>
</asp:Content>
