﻿<%@ Page Title="Pricing Eligible Materials" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PricingEligibleMaterials.aspx.cs" Inherits="SalesRequestForms.Admin.PricingEligibleMaterials" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="SearchPage">
        <table>
            <tr>
                <td style="font-size: large;text-decoration: underline;"><strong>Pricing Eligible Materials</strong></td>
            </tr>          
            <tr>
                <td>
                <asp:GridView ID="gvProduct" runat="server" Width="900px" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no products." OnRowDeleting="GV_Delete"  ShowFooter="true" 
                     CellPadding="2" DataKeyNames="productID" GridLines="Both" OnRowCancelingEdit="GV_Add">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" Font-Size="10pt" Font-Bold="true" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 
                       <asp:TemplateField HeaderStyle-Width="21%" HeaderText="Product ID" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProductID" Font-Size="10pt" runat="server"  Text='<%# Eval("ProductID")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtProductID" Width="150px" MaxLength="18" Font-Size="10pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredProductID" ValidationGroup="Add" ControlToValidate="txtProductID" runat="server"  ErrorMessage="Product ID is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldUProductIDExists" runat="server" ValidationGroup="Add" ControlToValidate="txtProductID" ErrorMessage="The Product ID already exists.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldProductIDInValid" runat="server" ValidationGroup="Add" ControlToValidate="txtProductID" ErrorMessage="The Product ID is not valid for the selected Hierarchy Level.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                       <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Hierarchy Level" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblHierarchyLevel" Font-Size="10pt" runat="server"  Text='<%# Eval("ProductLevel")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlProductLevel" width="175px" Font-Size="10pt" runat="server" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                     <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                                     <asp:ListItem Text="Level_4" Value ="Level_4" Selected="True"></asp:ListItem>
                                     <asp:ListItem Text="Level_5" Value ="Level_5"></asp:ListItem>
                                     <asp:ListItem Text="Level_6" Value ="Level_6"></asp:ListItem>
                                     <asp:ListItem Text="Material" Value ="Material"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldProductLevel" ControlToValidate="ddlProductLevel" ValidationGroup="Add" ForeColor="#FF7300" runat="server" ErrorMessage="Hierarchy Level is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProductDesc" Font-Size="10pt" runat="server"  Text='<%# Eval("ProductDesc")%>'></asp:Label>
                            </ItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                </asp:GridView> 
                
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                     
                </td>
            </tr>

       </table>    
    </div>
    </asp:Content>
