﻿<%@ Page Title="Program Detail - Administration" Language="C#" MasterPageFile="~/Site.Master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="ProgramDetail.aspx.cs" Inherits="SalesRequestForms.Admin.ProgramDetail" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="920px" cellpadding="3" cellspacing="3">
        <tr>
            <td colspan="4" style="font-size: large;text-decoration: underline;"><strong>Administrative Program Detail</strong></td>
        </tr>          
        <tr>
            <td>Program ID:</td>
            <td><asp:Label ID="lblProgramID" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td style="width:12%">Program Name: *&nbsp;&nbsp;<asp:RequiredFieldValidator 
                    ID="vldRequiredProgramName" runat="server" ControlToValidate="txtProgramName" 
                    ErrorMessage="The Program Name is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
            </td>
            <td style="width:25%"><asp:TextBox ID="txtProgramName" MaxLength="50" runat="server" Width="200px"></asp:TextBox></td>
            <td style="width:10%">Status:</td>
            <td><asp:DropDownList ID="ddlProgramStatus" runat="server">
                <asp:ListItem>Enabled</asp:ListItem>
                <asp:ListItem>Disabled</asp:ListItem>
            </asp:DropDownList></td>
        </tr>
        <tr>
            <td>Form Name: *&nbsp;&nbsp;<asp:RequiredFieldValidator ID="vldRequiredFormName" runat="server" ControlToValidate="txtFormName" 
                    ErrorMessage="The Form Name is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
            </td>
            <td><asp:TextBox ID="txtFormName" runat="server" MaxLength="50" Width="200px"></asp:TextBox></td>
            <td>Program Folder: *&nbsp;&nbsp;<asp:RequiredFieldValidator ID="vldRequiredProgramFolder" runat="server" ControlToValidate="txtProgramFolder" 
                    ErrorMessage="Program Folder is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
            </td>
            <td><asp:TextBox ID="txtProgramFolder" runat="server" MaxLength="50" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Overview:</td>
            <td style="vertical-align:top" colspan="3"><asp:TextBox ID="txtOverview" runat="server" 
                    TextMode="MultiLine" Rows="4" Width="700px"></asp:TextBox></td>
        </tr>    
        <tr>
            <td style="vertical-align:top">Guidelines:</td>
            <td style="vertical-align:top" colspan="3"><asp:TextBox ID="txtGuidelines" runat="server" TextMode="MultiLine" Rows="4" Width="700px"></asp:TextBox></td>
        </tr>    
        <tr>
            <td style="vertical-align:top">Program Description:</td>
            <td style="vertical-align:top" colspan="3"><asp:TextBox ID="txtProgramDescription" runat="server" TextMode="MultiLine" Rows="4" Width="700px"></asp:TextBox></td>
        </tr>    
        <tr>
            <td style="vertical-align:top">Features & Benefits:</td>
            <td style="vertical-align:top" colspan="3"><asp:TextBox ID="txtFeaturesBenefits" runat="server" TextMode="MultiLine" Rows="4" Width="700px"></asp:TextBox></td>
        </tr>    
        <tr>
            <td style="vertical-align:top">When to Use:</td>
            <td style="vertical-align:top" colspan="3"><asp:TextBox ID="txtWhenToUse" runat="server" TextMode="MultiLine" Rows="4" Width="700px"></asp:TextBox></td>
        </tr> 
         <tr>
            <td style="vertical-align:top"><asp:Label ID="LabelUserGuide" runat="server" Text="User's Guide:"></asp:Label></td>
            <td colspan="2">
                <asp:FileUpload ID="filUserGuide" Width="400" runat="server" />
                <asp:CustomValidator ID="vldUserGuideFileName" ValidationGroup="UserGuide" runat="server" ForeColor="#FF7300" ErrorMessage="The file must be a *.pdf file.">*</asp:CustomValidator>
                <asp:CustomValidator ID="vldUserGuideFileExists" ValidationGroup="UserGuide" runat="server" ForeColor="#FF7300" ErrorMessage="The file could not be uploaded.">*</asp:CustomValidator>
                <br /><asp:HyperLink ID="lnkUserGuide" Target="_blank" runat="server" Text="Current User's Guide"></asp:HyperLink>
            </td>
            <td>                
                <uc1:waitbutton id="btnUserGuideLoad"  runat="server" 
                    Text="Upload User's Guide" Font-Size="10pt" Height="22" 
				        CausesValidation="False"  Width="185px"
				        WaitText="Processing..." onclick="btnUserGuideLoad_Click" ></uc1:waitbutton>
            </td>
        </tr>
         <tr>
            <td style="vertical-align:top"><asp:Label ID="LabelAdministratorGuide" runat="server" Text="Administrator's Guide:"></asp:Label></td>
            <td colspan="2">
                <asp:FileUpload ID="filAdminGuide" Width="400" runat="server" />
                <asp:CustomValidator ID="vldAdminGuideFileName" ValidationGroup="AdminGuide" runat="server" ForeColor="#FF7300" ErrorMessage="The file must be a *.pdf file.">*</asp:CustomValidator>
                <asp:CustomValidator ID="vldAdminGuideFileExists" ValidationGroup="AdminGuide" runat="server" ForeColor="#FF7300" ErrorMessage="The file could not be uploaded.">*</asp:CustomValidator>
                <br /><asp:HyperLink ID="lnkAdminGuide" Target="_blank" runat="server" Text="Current Administrator's Guide"></asp:HyperLink>
            </td>
             <td>                
                <uc1:waitbutton id="btnAdminGuideLoad"  runat="server" 
                     Text="Upload Administrator's Guide" Font-Size="10pt" Height="22" 
				        CausesValidation="False"  Width="185px"
				        WaitText="Processing..." onclick="btnAdminGuideLoad_Click" ></uc1:waitbutton>
            </td>
       </tr>        
        <tr>
            <td style="text-align:center" colspan="4">
                <asp:Button ID="btnSubmit" runat="server" CausesValidation="False" 
                Text="Submit" onclick="btnSubmit_Click" Width="80px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">* - indicates required fields</td>
        </tr>
        <tr>
            <td style="text-align:center" colspan="4">
                <asp:ValidationSummary ID="vldSummary" Font-Size="10pt"  ForeColor="#FF7300" runat="server" /><br />
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AdminGuide" Font-Size="10pt"  ForeColor="#FF7300" runat="server" /><br />
                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UserGuide"  Font-Size="10pt"  ForeColor="#FF7300" runat="server" /><br />
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </td>
        </tr> 
        
    </table>
</asp:Content>
