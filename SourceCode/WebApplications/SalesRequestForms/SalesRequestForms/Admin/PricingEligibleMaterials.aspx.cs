﻿// --------------------------------------------------------------
// <copyright file="PricingEligibleMaterials.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SalesRequestForms.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PricingRequest.Classes;

    /// <summary>This is the Pricing Eligible Materials Administration page.</summary>   
    public partial class PricingEligibleMaterials : System.Web.UI.Page
    {
        private int intProgramID = 2;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            if (!AppUsersEntity.IsProgramAdmin(myUsers, intProgramID))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.BindProducts();
            }
        }

        /// <summary>Binds the PricingEligibleProducts collection to the GridView.</summary>
        protected void BindProducts()
        {
            Collection<PricingEligibleProductEntity> products = PricingEligibleProductEntity.GetPricingEligibleProducts();

            if (products.Count == 0)
            {
                PricingEligibleProductEntity dummy = new PricingEligibleProductEntity();
                products.Add(dummy);
            }

            this.gvProduct.DataSource = products;
            this.gvProduct.DataBind();
        }

        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('product');");

                
                if (this.gvProduct.DataKeys[e.Row.RowIndex]["productID"].ToString() == string.Empty)
                {
                    e.Row.Visible = false;
                }


            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtProductID = (TextBox)e.Row.FindControl("txtProductID");
                DropDownList ddlProductLevel = (DropDownList)e.Row.FindControl("ddlProductLevel");
                Label lblProductDesc = (Label)e.Row.FindControl("lblProductDesc");

                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtProductID, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the gvProduct GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strProductID = this.gvProduct.DataKeys[e.RowIndex]["productID"].ToString();
            int recordsDeleted = PricingEligibleProductEntity.DeletePricingEligibleProduct(strProductID);

            this.gvProduct.EditIndex = -1;
            this.BindProducts();
        }

        /// <summary>Handles the Edit event of the  GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            DropDownList ddlProductLevel = (DropDownList)this.gvProduct.FooterRow.FindControl("ddlProductLevel");
            TextBox txtProductID = (TextBox)this.gvProduct.FooterRow.FindControl("txtProductID");
            CustomValidator vldProductIDInValid = (CustomValidator)this.gvProduct.FooterRow.FindControl("vldProductIDInValid");
            CustomValidator vldUProductIDExists = (CustomValidator)this.gvProduct.FooterRow.FindControl("vldUProductIDExists");

            PricingEligibleProductEntity prod = PricingEligibleProductEntity.GetValidPricingEligibleProductByProductID(txtProductID.Text.Trim(), ddlProductLevel.SelectedValue);
            vldProductIDInValid.IsValid = Convert.ToBoolean(prod != null);

            PricingEligibleProductEntity prodExists = PricingEligibleProductEntity.GetPricingEligibleProductsByProductID(txtProductID.Text.Trim());
            vldUProductIDExists.IsValid = Convert.ToBoolean(prodExists == null);

            if (Page.IsValid)
            {
                int recordsInserted = PricingEligibleProductEntity.InsertPricingEligibleProduct(prod);

                this.gvProduct.EditIndex = -1;
                this.BindProducts();
            } // end Page.IsValid

        }




    } //// end class
}