﻿// --------------------------------------------------------------
// <copyright file="UserList.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms.Admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the User List page</summary>
    public partial class UserList : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("Start Page_Load");
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            //// if not an admin redirect to Unauthorized Page
            if (!AppUsersEntity.IsAdmin(myUsers)) 
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
               
                EnterButton.TieButton(this.txtLastName, this.btnFindUser);

                if (Request.QueryString["programID"] != null)
                    this.hdnProgramID.Value = Request.QueryString["programID"];
                
                this.BindDT();
            }
            Debug.WriteLine("End Page_Load");
        } //// end Page_Load

        /// <summary>Binds the users table to the gvUsers GridView.</summary>
        protected void BindDT()
        {
         //   DataTable dtUsers = SQLUtility.SqlExecuteQuery("sp_GetAdminUsers");
            List<AppUsersEntity> programUsers = AppUsersEntity.GetAppUserByProgramID(Convert.ToInt32(this.hdnProgramID.Value));

            if (programUsers.Count == 0)
                programUsers.Add(new AppUsersEntity());
            
            this.gvUser.DataSource = programUsers;
            this.gvUser.DataBind();
        }


        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtUserName = (TextBox)this.gvUser.FooterRow.FindControl("txtAddUserName");
            DropDownList ddlRole = (DropDownList)this.gvUser.FooterRow.FindControl("ddlRole");
             CustomValidator vldUserExists = (CustomValidator)this.gvUser.FooterRow.FindControl("vldUserExists");
            CustomValidator vldAddUserInAD = (CustomValidator)this.gvUser.FooterRow.FindControl("vldAddUserInAD");

            List<AppUsersEntity> testUsers = AppUsersEntity.GetAppUserByProgramIDByUsername(Convert.ToInt32(this.hdnProgramID.Value), txtUserName.Text);
            //List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));
            vldUserExists.IsValid = Convert.ToBoolean(testUsers.Count == 0);

            ADUserEntity myUser = null;

            if (txtUserName.Text != String.Empty)
            {
                myUser = ADUtility.GetADUserByUserName(txtUserName.Text);
                vldAddUserInAD.IsValid = Convert.ToBoolean(myUser != null);
            }

            if (Page.IsValid)
            {
                AppUsersEntity newUser = new AppUsersEntity();
                newUser.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                newUser.UserName = txtUserName.Text;
                newUser.LastName = myUser.LastName;
                newUser.FirstName = myUser.FirstName;
                newUser.ModifiedBy = Utility.CurrentUser(Request);

                if (newUser.RoleID == 6)
                    newUser.SendEmail = "Yes";

                AppUsersEntity.InsertAppUser(newUser);

                this.gvUser.EditIndex = -1;
                this.BindDT();

                this.txtLastName.Text = string.Empty;
                this.BindFindUser("CannotFindThisName");

            } //// Page.IsValid

        }  ////end GV_Add

        /// <summary>Handles the RowDataBound event of the gvUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;

            if (e.Row.RowType  == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('user');");

                Label lblUserName = (Label)e.Row.FindControl("lblUserName");
                if (lblUserName.Text == string.Empty)
                    e.Row.Visible = false;

                // Hide btnEdit if not DOS/DE roleID 6
                string strRoleID = this.gvUser.DataKeys[e.Row.RowIndex]["roleID"].ToString();
                Button btnEdit = (Button)e.Row.FindControl("btnEdit");
                if(btnEdit != null)
                    btnEdit.Visible = Convert.ToBoolean(strRoleID == "6");

                if (this.gvUser.EditIndex == e.Row.RowIndex && Convert.ToBoolean(strRoleID == "6"))
                {
                    string strSendEmail = this.gvUser.DataKeys[e.Row.RowIndex]["SendEmail"].ToString();
                    DropDownList ddlSendEmail = (DropDownList)e.Row.FindControl("ddlSendEmail");
                    if (ddlSendEmail.Items.FindByValue(strSendEmail) != null)
                        ddlSendEmail.Items.FindByValue(strSendEmail).Selected = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddUserName = (TextBox)e.Row.FindControl("txtAddUserName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddUserName, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the gvUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intUserID = Convert.ToInt32(this.gvUser.DataKeys[e.RowIndex]["userID"].ToString());
            AppUsersEntity.DeleteAppUser(intUserID);

            this.gvUser.EditIndex = -1;
            this.BindDT();
        }

        /// <summary>Handles the Click event of the btnFindUser control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnFindUser_Click(object sender, EventArgs e)
        {
            Page.Validate("FindUser");

            if (Page.IsValid)
            {
                this.BindFindUser(this.txtLastName.Text);
            }
        }  //// end btnFindUser_Click


        /// <summary>Binds the data to the gvFindUsers GridView</summary>
        /// <param name="strLastName">The lastname to lookup</param>
        protected void BindFindUser(string strLastName)
        {
            this.gvFindUsers.DataSource = ADUtility.GetADUsersByLastName(strLastName);
            this.gvFindUsers.DataBind();
        }  //// end BindFindUser

        /// <summary>Handles the Edit event of the gvFindUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Select(object sender, GridViewEditEventArgs e)
        {
            string strUserName = this.gvFindUsers.DataKeys[e.NewEditIndex]["username"].ToString();
            TextBox txtUserName = (TextBox)this.gvUser.FooterRow.FindControl("txtAddUserName");
            txtUserName.Text = strUserName;
        }


        /// <summary>Gets the UserRoles datatable.</summary>
        /// <returns>the UserRolesEntity collection</returns>
        public List<UserRolesEntity> GetRoles()
        {
            string strUser = Utility.CurrentUser(Request);

            List<UserRolesEntity> roles = UserRolesEntity.GetUserRolesByProgramID(Convert.ToInt32(this.hdnProgramID.Value));
            return roles;

        }


        /****************** Territory ************************************************/

        /// <summary>Handles the Edit event of the gvTerritory GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_AddTerritory(object sender, GridViewCancelEditEventArgs e)
        {
            string strUserName = this.gvUser.DataKeys[gvUser.EditIndex]["userName"].ToString();
           
            GridView gvTerritory = (GridView)sender;

            RegularExpressionValidator vldDistributorID = (RegularExpressionValidator)gvTerritory.FooterRow.FindControl("vldDistributorID");

            //vldSalesRepID.Visible = Convert.ToBoolean(this.ddlRole.SelectedValue == "3");
            //vldDistributorID.Visible = Convert.ToBoolean(this.ddlRole.SelectedValue == "2");

            Page.Validate("AddTerritory");

            TextBox txtTerritory = (TextBox)gvTerritory.FooterRow.FindControl("txtTerritory");
            CustomValidator vldTerritoryAssigned = (CustomValidator)gvTerritory.FooterRow.FindControl("vldTerritoryAssigned");
            vldTerritoryAssigned.IsValid = true;

            List<UserTerritoryEntity> territories = UserTerritoryEntity.GetUsersAssignedToTerritory(txtTerritory.Text);
            foreach (UserTerritoryEntity t in territories)
            {
                if(t.UserName.ToLower() != strUserName.ToLower())
                    vldTerritoryAssigned.IsValid = false;
            }


            if (Page.IsValid)
            {
                ADUserEntity creator = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

                UserTerritoryEntity.InsertUserTerritory(strUserName, txtTerritory.Text, creator.DisplayName);

                gvTerritory.EditIndex = -1;
                this.BindTerritory(strUserName, gvUser.EditIndex);

                this.gvUser.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
        }

        /// <summary>Handles the Delete event of the gvTerritory GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_DeleteTerritory(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            Debug.WriteLine("GV_DeleteTerritory");
            string strUserName = this.gvUser.DataKeys[gvUser.EditIndex]["userName"].ToString();
            GridView gvTerritory = (GridView)sender;
            int intUserTerritoryID = Int32.Parse(gvTerritory.DataKeys[e.RowIndex]["UserTerritoryID"].ToString());

            Debug.WriteLine("GV_DeleteTerritory: " + intUserTerritoryID.ToString());

            ADUserEntity creator = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
            UserTerritoryEntity.DeleteTerritoryByUserTerritoryId(intUserTerritoryID, creator.DisplayName);

            gvTerritory.EditIndex = -1;
            this.BindTerritory(strUserName, gvUser.EditIndex);

            this.gvUser.EditIndex = -1;
            this.BindDT();
        }

        /// <summary>Handles the Edit event of the GV GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvUser.EditIndex = e.NewEditIndex;
            this.BindDT();

            string strUserName = this.gvUser.DataKeys[e.NewEditIndex]["userName"].ToString();
            this.BindTerritory(strUserName, e.NewEditIndex);

        }

        /// <summary>Handles the Update event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            string strUserName = this.gvUser.DataKeys[e.RowIndex]["userName"].ToString();
            DropDownList ddlSendEmail = (DropDownList)this.gvUser.Rows[e.RowIndex].FindControl("ddlSendEmail");

            AppUsersEntity newUser = new AppUsersEntity();
            newUser.SendEmail = ddlSendEmail.SelectedValue;
            newUser.UserName = strUserName;
            newUser.ModifiedBy = Utility.CurrentUser(Request);

            AppUsersEntity.UpdateAppUser(newUser);
            
            this.gvUser.EditIndex = -1;
            this.BindDT();
        } // end GV_Update


        /// <summary>Binds the Territory List to the gvTerritory GridView.</summary>
        /// <param name="strUserName">The Username in the row.</param>
        /// <param name="rowIndex">The row index for gvUser</param>
        protected void BindTerritory(string strUserName, int rowIndex)
        {
            List<UserTerritoryEntity> tList = UserTerritoryEntity.GetUserTerritoriesByUserName(strUserName);
            Debug.WriteLine("BindTerritory tList Count: " + tList.Count.ToString());
            if (tList.Count == 0)
            {
                UserTerritoryEntity dummy = new UserTerritoryEntity();
                dummy.UserTerritoryID = -99;
                tList.Add(dummy);
            }

            GridView gvTerritory = (GridView)this.gvUser.Rows[rowIndex].FindControl("gvTerritory");
            Label lblUserName = (Label)this.gvUser.Rows[rowIndex].FindControl("lblUserName");
            lblUserName.Text = strUserName;

            gvTerritory.DataSource = tList;
            gvTerritory.DataBind();
        }//// end BindDT      
        
        
        
        ///// <summary>Binds the Territory List to the gvTerritory GridView.</summary>
        //protected void BindTerritory(List<UserTerritoryEntity> tList)
        //{
        //    Debug.WriteLine("BindTerritory tList.Count: " + tList.Count.ToString());

        //    if (tList.Count == 0)
        //    {
        //        UserTerritoryEntity dummy = new UserTerritoryEntity();
        //        dummy.UserTerritoryID = -99;
        //        tList.Add(dummy);
        //        Debug.WriteLine("BindTerritory Added Dummy - " + dummy.UserTerritoryID.ToString());
        //    }

        //    this.gvTerritory.DataSource = tList;
        //    this.gvTerritory.DataBind();
        //}//// end BindDT

        ///// <summary>Binds the Territory List to the gvTerritory GridView.</summary>
        //protected void BindTerritory()
        //{
        //    List<UserTerritoryEntity> tList = UserTerritoryEntity.GetUserTerritoriesByUserId(Convert.ToInt32(this.lblUserID.Text));
        //    Debug.WriteLine("BindTerritory tList Count: " + tList.Count.ToString());
        //    if (tList.Count == 0)
        //    {
        //        UserTerritoryEntity dummy = new UserTerritoryEntity();
        //        dummy.UserTerritoryID = -99;
        //        tList.Add(dummy);
        //    }

        //    this.gvTerritory.DataSource = tList;
        //    this.gvTerritory.DataBind();
        //}//// end BindDT

        /// <summary>Handles the RowDataBound event of the gvTerritory GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBoundTerritory(object sender, GridViewRowEventArgs e)
        {
                   Debug.WriteLine("IN GV_RowDataBoundTerritory");
            GridView gvTerritory = (GridView)sender;
        //    string strParameterName = gvInput.DataKeys[e.RowIndex]["ParameterName"].ToString();

            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnTerritoryDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Territory');");

                if (gvTerritory.DataKeys[e.Row.RowIndex]["UserTerritoryID"].ToString() != "-99")
                {
                    Debug.WriteLine("Territory Visble: " + gvTerritory.DataKeys[e.Row.RowIndex]["UserTerritoryID"].ToString());
                }
                else
                {
                    Debug.WriteLine("Territory Visble: False; Index:" + e.Row.RowIndex.ToString());

                }
                //// if the gvTerritory if blank hide the row
                e.Row.Visible = Convert.ToBoolean(gvTerritory.DataKeys[e.Row.RowIndex]["UserTerritoryID"].ToString() != "-99");
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtTerritory = (TextBox)e.Row.FindControl("txtTerritory");
                Button btnTerritoryAdd = (Button)e.Row.FindControl("btnTerritoryAdd");
                EnterButton.TieButton(txtTerritory, btnTerritoryAdd);
            }

        } // end gv_RowDataBound
    } //// end class
}
