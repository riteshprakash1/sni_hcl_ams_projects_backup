﻿<%@ Page Title="Country of Origin - File Upload" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FileLoadCoo.aspx.cs" Inherits="SalesRequestForms.Admin.FileLoadCoo" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="font-size:12pt">
           <table style="width:900px; text-align:left; font-size:15pt">
            <tr>
                <td style="font-size: large;text-decoration: underline;"><strong>Country of Origin Documents</strong>
                    <asp:HiddenField ID="hdnProgramType" runat="server" Value="CountryOfOrigin" />
                </td>
            </tr>          
            <tr>
                <td>
                <asp:GridView ID="gvDocuments" runat="server" Width="900px" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no documents." OnRowDeleting="GV_Delete"  ShowFooter="true" 
                     CellPadding="2" DataKeyNames="DocumentID, FolderName, FileName" GridLines="Both" OnRowCancelingEdit="GV_Add">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" Font-Size="10pt" Font-Bold="true" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 
                       <asp:TemplateField HeaderStyle-Width="21%" HeaderText="Published Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblPublishedName" Font-Size="10pt" runat="server"  Text='<%# Eval("PublishedName")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtPublishedName" Width="150px" MaxLength="50" Font-Size="10pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredPublishedName" ValidationGroup="Add" ControlToValidate="txtPublishedName" runat="server"  ErrorMessage="Published Name is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                       <asp:TemplateField HeaderStyle-Width="25%" HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblFileName" Font-Size="10pt" runat="server"  Text='<%# Eval("FileName")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:FileUpload ID="filDocument" Width="375px" runat="server" />
                                <asp:CustomValidator ID="vldFileName" runat="server" ForeColor="#FF7300" ErrorMessage="The file name already exists.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldFileExists" runat="server" ForeColor="#FF7300" ErrorMessage="The file could not be found.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-Width="25%" HeaderText="URL" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkURL" NavigateUrl='<%# Eval("DocumentURL")%>' Text='<%# Eval("PublishedName")%>' Target="_blank"  runat="server"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView> 
               
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                     
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblOutput" runat="server"></asp:Label>
                </td>
            </tr>

       </table> 
</div>
</asp:Content>
