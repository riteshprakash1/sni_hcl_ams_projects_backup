﻿// --------------------------------------------------------------
// <copyright file="ProgramDetail.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms.Admin
{

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Admin Program Detail page</summary>
    public partial class ProgramDetail : System.Web.UI.Page
    {
        /// <summary> Handles the Load event of the Page control. </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>       
        protected void Page_Load(object sender, EventArgs e)
        {
            //// if not an admin redirect to Unauthorized Page
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            if (!AppUsersEntity.IsAdmin(myUsers))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                Debug.WriteLine("!Page.IsPostBack: " + Convert.ToString(Request.QueryString["programId"] != null));
                if (Request.QueryString["programId"] != null)
                {
                    Debug.WriteLine("programId: " + Request.QueryString["programId"]);
                    this.LoadProgramData(Request.QueryString["programId"]);
                }
            } //// end IsPostBack

            this.lblMsg.Text = string.Empty;
            this.SetControl();

        }

        /// <summary>Retrieves the program data from the DB and populates the fields</summary>
        /// <param name="strProgramID">The program ID</param>
        protected void LoadProgramData(string strProgramID)
        {
            Debug.WriteLine("LoadProgramData");
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@programID", strProgramID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetProgramByID", myParamters);
            if (dt.Rows.Count > 0)
            {
                DataRow r = dt.Rows[0];
                this.lblProgramID.Text = r["programID"].ToString();
                this.txtFeaturesBenefits.Text = r["featuresBenefits"].ToString();
                this.txtGuidelines.Text = r["guidelines"].ToString();
                this.txtOverview.Text = r["overview"].ToString();
                this.txtProgramDescription.Text = r["programdescription"].ToString();
                this.txtProgramName.Text = r["programName"].ToString();
                this.txtWhenToUse.Text = r["whenToUse"].ToString();
                this.txtFormName.Text = r["programFormName"].ToString();
                this.txtProgramFolder.Text = r["programFolder"].ToString();

                if (this.ddlProgramStatus.Items.FindByValue(r["programstatus"].ToString()) != null)
                {
                    ddlProgramStatus.Items.FindByValue(r["programstatus"].ToString()).Selected = true;
                }

                this.LoadUserGuideLink();
                this.LoadAdminGuideLink();

            }
            else
            {
                this.lblProgramID.Text = string.Empty;
            }

        }

        /// <summary>Loads the User Guide Links if there is a file otherwise it is hidden</summary>
        protected void LoadUserGuideLink()
        {
            string strFileName = "UserGuide" + this.lblProgramID.Text + ".pdf";
            string strFullFileName = ConfigurationManager.AppSettings["GuideFolder"] + strFileName;

            if (File.Exists(strFullFileName))
            {
                this.lnkUserGuide.NavigateUrl = "~/Guides/" + strFileName;
                this.lnkUserGuide.Visible = true;
            }
            else
                this.lnkUserGuide.Visible = false;
        }

        /// <summary>Loads the User Guide Links if there is a file otherwise it is hidden</summary>
        protected void LoadAdminGuideLink()
        {
            string strFileName = "AdminGuide" + this.lblProgramID.Text + ".pdf";
            string strFullFileName = ConfigurationManager.AppSettings["GuideFolder"] + strFileName;

            if (File.Exists(strFullFileName))
            {
                this.lnkAdminGuide.NavigateUrl = "~/Guides/" + strFileName;
                this.lnkAdminGuide.Visible = true;
            }
            else
                this.lnkAdminGuide.Visible = false;
        }


        /// <summary>Handles the Click event of the btnSubmit control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid)
            {

                ////Load Paramters
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@programName", this.txtProgramName.Text));
                myParamters.Add(new SqlParameter("@overview", this.txtOverview.Text));
                myParamters.Add(new SqlParameter("@guidelines", this.txtGuidelines.Text));
                myParamters.Add(new SqlParameter("@programdescription", this.txtProgramDescription.Text));
                myParamters.Add(new SqlParameter("@featuresBenefits", this.txtFeaturesBenefits.Text));
                myParamters.Add(new SqlParameter("@whenToUse", this.txtWhenToUse.Text));
                myParamters.Add(new SqlParameter("@programStatus", this.ddlProgramStatus.SelectedValue));
                myParamters.Add(new SqlParameter("@programFormName", this.txtFormName.Text));
                myParamters.Add(new SqlParameter("@programFolder", this.txtProgramFolder.Text));
                myParamters.Add(new SqlParameter("@modifiedBy", Utility.CurrentUser(Request)));

                int rowsUpdated = 0;

                if (this.lblProgramID.Text == string.Empty)
                {
                    SqlParameter paramID = new SqlParameter("@programID", SqlDbType.Int);
                    paramID.Direction = ParameterDirection.Output;
                    myParamters.Add(paramID);

                    rowsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_InsertProgram", myParamters);
                    this.lblProgramID.Text = paramID.Value.ToString();
                }
                else
                {
                    myParamters.Add(new SqlParameter("@programID", Int32.Parse(this.lblProgramID.Text)));
                    rowsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateProgram", myParamters);
                }

                if (rowsUpdated > 0)
                {
                    this.lblMsg.Text = "Program updated successfully";
                    this.LoadProgramData(this.lblProgramID.Text);
                }
                else
                    this.lblMsg.Text = "Program update FAILED";

                this.SetControl();

            }
        }

        /// <summary>Handles the Click event of the btnUserGuideLoad control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnUserGuideLoad_Click(object sender, EventArgs e)
        {
            Page.Validate("UserGuide");

            this.vldUserGuideFileName.IsValid = Convert.ToBoolean((this.filUserGuide.FileName.ToLower().EndsWith("pdf") == true));
            this.vldUserGuideFileExists.IsValid = filUserGuide.HasFile;

            // Added so screen accurately displays the characters import from the file
            Response.ContentEncoding = System.Text.Encoding.Default;

            string strFileName = String.Empty;
            if (Page.IsValid)
            {
                try
                {
                    strFileName = ConfigurationManager.AppSettings["GuideFolder"] + "UserGuide" + this.lblProgramID.Text + ".pdf";
                    Debug.WriteLine("User's Guide Import: " + strFileName);
                    
                    if(File.Exists(strFileName))
                    {
                        File.Delete(strFileName);
                    }

                    this.filUserGuide.SaveAs(strFileName);

                    this.LoadUserGuideLink();
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "Upload Failed: " + ex.Message;
                }
            }
        }

        /// <summary>Handles the Click event of the btnAdminGuideLoad control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnAdminGuideLoad_Click(object sender, EventArgs e)
        {
            Page.Validate("AdminGuide");

            this.vldAdminGuideFileName.IsValid = Convert.ToBoolean((this.filAdminGuide.FileName.ToLower().EndsWith("pdf") == true));
            this.vldAdminGuideFileExists.IsValid = filAdminGuide.HasFile;

            // Added so screen accurately displays the characters import from the file
            Response.ContentEncoding = System.Text.Encoding.Default;

            string strFileName = String.Empty;
            if (Page.IsValid)
            {
                try
                {
                    strFileName = ConfigurationManager.AppSettings["GuideFolder"] + "AdminGuide" + this.lblProgramID.Text + ".pdf";
                    Debug.WriteLine("Admin's Guide Import: " + strFileName);

                    if (File.Exists(strFileName))
                    {
                        File.Delete(strFileName);
                    }

                    this.filAdminGuide.SaveAs(strFileName);
                    this.LoadAdminGuideLink();
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "Upload Failed: " + ex.Message;
                }
            }
        }

        /// <summary>Handles the Click event of the btnAdmiGuideLoad control.</summary>
        protected void SetControl()
        {
            bool showUpload = Convert.ToBoolean(this.lblProgramID.Text != string.Empty);

            this.filAdminGuide.Visible = showUpload;
            this.LabelAdministratorGuide.Visible = showUpload;
            this.btnAdminGuideLoad.Visible = showUpload;
            this.vldAdminGuideFileExists.Visible = showUpload;
            this.vldAdminGuideFileName.Visible = showUpload;

            this.filUserGuide.Visible = showUpload;
            this.LabelUserGuide.Visible = showUpload;
            this.btnUserGuideLoad.Visible = showUpload;
            this.vldUserGuideFileExists.Visible = showUpload;
            this.vldUserGuideFileName.Visible = showUpload;

            List<AppUsersEntity> users  = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));
            //if not program admin remove update buttons
            bool isProgramAdmin = AppUsersEntity.IsProgramAdmin(users, Convert.ToInt32(this.lblProgramID.Text));
            this.btnAdminGuideLoad.Visible = isProgramAdmin;
            this.btnSubmit.Visible = isProgramAdmin;
            this.btnUserGuideLoad.Visible = isProgramAdmin;

            bool isSuperAdmin = AppUsersEntity.IsSuperAdmin(users);
            this.txtProgramFolder.Enabled = isSuperAdmin;
            this.txtFormName.Enabled = isSuperAdmin;
        }
    } //// end class
}