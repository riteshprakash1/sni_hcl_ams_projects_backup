﻿<%@ Page Title="Administration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SalesRequestForms.Admin.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <table width="100%">
            <tr>
                <td colspan="2"><asp:Label ID="LabelPlacedCapital" runat="server" Text="Placed Capital" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td style="width:5%">&nbsp;</td>
                <td><asp:LinkButton ID="lnkProgramRates" runat="server" 
                        Text="Program Rates & Variables" onclick="lnkProgramRates_Click"></asp:LinkButton></td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkPlacedCapitalMaterials" runat="server" 
                        Text="Placed Capital Materials" onclick="lnkPlacedCapitalMaterials_Click"></asp:LinkButton></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkPlacedCapitalUserList" runat="server" 
                        Text="User List" onclick="lnkPlacedCapitalUserList_Click"></asp:LinkButton></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID="LabelPricingRequest" runat="server" Text="Pricing Request" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkPricingEligibleMaterials" runat="server" 
                        Text="Pricing Eligible Materials" 
                        onclick="lnkPricingEligibleMaterials_Click"></asp:LinkButton></td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkImportPricingMaterialMatrix" runat="server" 
                        Text="Import Pricing Material Matrix" 
                        onclick="lnkImportPricingMaterialMatrix_Click"></asp:LinkButton></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkPricingRequestUserList" runat="server" 
                        Text="User List" onclick="lnkPricingRequestUserList_Click"></asp:LinkButton></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID="LabelCountryOfOrigin" runat="server" Text="Country of Origin" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkFileLoadCOO" runat="server" 
                        Text="File Load" onclick="lnkFileLoadCOO_Click"></asp:LinkButton></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID="LabelGeneral" runat="server" Text="General" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="lnkPrograms" runat="server" 
                        Text="Programs" onclick="lnkPrograms_Click"></asp:LinkButton></td>
            </tr>
        </table>
</asp:Content>
