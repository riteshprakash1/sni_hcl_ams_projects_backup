﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SalesRequestForms.Classes;

namespace SalesRequestForms.Admin
{
    public partial class EmailTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsConfigAdmin())
            {
                Response.Redirect("~/Default.aspx", true);
            }

            if (!Page.IsPostBack)
            {
                this.txtEmailFrom.Text = ConfigurationManager.AppSettings["PlacedCapitalFromEmail"];
                this.txtEmailTo.Text = ConfigurationManager.AppSettings["PlacedCapitalToEmail"];
                this.txtSubject.Text = ConfigurationManager.AppSettings["PlacedCapitalRequestEmailSubject"];

                EnterButton.TieButton(this.txtEmailFrom, this.btnSendEmail);
                EnterButton.TieButton(this.txtEmailTo, this.btnSendEmail);
                EnterButton.TieButton(this.txtEmailCC, this.btnSendEmail);
                EnterButton.TieButton(this.txtSubject, this.btnSendEmail);
                EnterButton.TieButton(this.txtBody, this.btnSendEmail);
            }

            this.lblErrorMsg.Text = string.Empty;
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.SendEmailAsync(this.txtEmailTo.Text, this.txtEmailFrom.Text, this.txtEmailCC.Text, this.txtSubject.Text, this.txtBody.Text, Convert.ToBoolean(this.ddlIsHtml.SelectedValue));
                this.lblErrorMsg.Text = "Email Sent.";
            }
            catch (Exception ex)
            {
                this.lblErrorMsg.Text = ex.Message;
            }
        }

        /// <summary>Determines whether [is config admin].</summary>
        /// <returns><c>true</c> if [is config admin]; otherwise, <c>false</c>.</returns>
        private bool IsConfigAdmin()
        {
            // Get the UserName
            string[] arrayAuth = Request.ServerVariables["AUTH_USER"].Split('\\');
            string strUser = String.Empty;
            if (arrayAuth.Length > 1)
            {
                strUser = arrayAuth[1].ToLower().Trim();
            }
            else
            {
                return false;
            }

            string strConfigAdmin = string.Empty;
            if (ConfigurationManager.AppSettings["ConfigAdmin"] != null)
            {
                strConfigAdmin = ConfigurationManager.AppSettings["ConfigAdmin"];
            }

            string[] strAdmins = strConfigAdmin.Split(',');

            foreach (string strAdmin in strAdmins)
            {
                if (strAdmin.ToLower().Trim() == strUser)
                {
                    return true;
                }
            }

            return false;
        }

        protected void btnConfigAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminConfig.aspx", true);
        }
    }
}