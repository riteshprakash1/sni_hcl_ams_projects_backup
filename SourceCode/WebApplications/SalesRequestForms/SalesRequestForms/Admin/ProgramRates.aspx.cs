﻿// --------------------------------------------------------------
// <copyright file="ProgramRates.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Program Rate Administration page.</summary>   
    public partial class ProgramRates : System.Web.UI.Page
    {
        private int intProgramID = 1;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            //// if not an admin redirect to Unauthorized Page
            if (!AppUsersEntity.IsProgramAdmin(myUsers, intProgramID))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.BindRates();
            }
        }

        /// <summary>Handles the RowDataBound event of the gvProgramRates GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('program rate');");

                if (e.Row.RowIndex == this.gvProgramRates.EditIndex)
                {
                    TextBox txtCapitalGroup = (TextBox)e.Row.FindControl("txtCapitalGroup");
                    TextBox txtRate = (TextBox)e.Row.FindControl("txtRate");
                    TextBox txtRateText = (TextBox)e.Row.FindControl("txtRateText");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtCapitalGroup, btnUpdate);
                    EnterButton.TieButton(txtRate, btnUpdate);
                    EnterButton.TieButton(txtRateText, btnUpdate);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddCapitalGroup = (TextBox)e.Row.FindControl("txtAddCapitalGroup");
                TextBox txtAddRate = (TextBox)e.Row.FindControl("txtAddRate");
                TextBox txtAddRateText = (TextBox)e.Row.FindControl("txtAddRateText");

                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddRateText, btnAdd);
                EnterButton.TieButton(txtAddRate, btnAdd);
                EnterButton.TieButton(txtAddCapitalGroup, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Binds the rate table to the gvMaterial GridView.
        /// </summary>
        protected void BindRates()
        {
            DataTable dtRates = SQLUtility.SqlExecuteQuery("sp_GetProgramRates");
            this.gvProgramRates.DataSource = dtRates;
            this.gvProgramRates.DataBind();
        }

        /// <summary>Handles the Delete event of the gvProgramRates GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strProgramRateID = this.gvProgramRates.DataKeys[e.RowIndex]["ProgramRateID"].ToString();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@ProgramRateID", Convert.ToInt32(strProgramRateID)));

            int recordsDeleted = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteProgramRate", parameters);
            this.gvProgramRates.EditIndex = -1;
            this.BindRates();
        }

        /// <summary>Handles the Edit event of the gvCost GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvProgramRates.EditIndex = e.NewEditIndex;
            this.BindRates();
        }

        /// <summary>Handles the Update event of the gvCost GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateRate");

            if (Page.IsValid)
            {
                string strProgramRateID = this.gvProgramRates.DataKeys[e.RowIndex]["ProgramRateID"].ToString();
                DropDownList ddlProgram = (DropDownList)this.gvProgramRates.Rows[e.RowIndex].FindControl("ddlProgram");
                DropDownList ddlRateDescription = (DropDownList)this.gvProgramRates.Rows[e.RowIndex].FindControl("ddlRateDescription");
                TextBox txtCapitalGroup = (TextBox)this.gvProgramRates.Rows[e.RowIndex].FindControl("txtCapitalGroup");
                TextBox txtRate = (TextBox)this.gvProgramRates.Rows[e.RowIndex].FindControl("txtRate");
                TextBox txtRateText = (TextBox)this.gvProgramRates.Rows[e.RowIndex].FindControl("txtRateText");

                Collection<SqlParameter> parameters = new Collection<SqlParameter>();
                parameters.Add(new SqlParameter("@ProgramRateID", Convert.ToInt32(strProgramRateID)));
                parameters.Add(new SqlParameter("@ProgramID", Convert.ToInt32(ddlProgram.SelectedValue)));
                parameters.Add(new SqlParameter("@CapitalGroup", txtCapitalGroup.Text));
                parameters.Add(new SqlParameter("@RateDescription", ddlRateDescription.SelectedValue));
                parameters.Add(new SqlParameter("@Rate", Convert.ToDouble(txtRate.Text)));
                parameters.Add(new SqlParameter("@RateText", txtRateText.Text));
                parameters.Add(new SqlParameter("@ModifiedBy", ADUtility.GetUserNameOfAppUser(Request)));

                int recordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateProgramRate", parameters);

                this.gvProgramRates.EditIndex = -1;
                this.BindRates();
            } // end Page.IsValid
        } // end GV_Update

        /// <summary>Handles the Edit event of the gvProgramRates GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddRate");

            if (Page.IsValid)
            {
                DropDownList ddlProgram = (DropDownList)this.gvProgramRates.FooterRow.FindControl("ddlAddProgram");
                DropDownList ddlRateDescription = (DropDownList)this.gvProgramRates.FooterRow.FindControl("ddlAddRateDescription");
                TextBox txtCapitalGroup = (TextBox)this.gvProgramRates.FooterRow.FindControl("txtAddCapitalGroup");
                TextBox txtRate = (TextBox)this.gvProgramRates.FooterRow.FindControl("txtAddRate");
                TextBox txtRateText = (TextBox)this.gvProgramRates.FooterRow.FindControl("txtAddRateText");

                Collection<SqlParameter> parameters = new Collection<SqlParameter>();
                SqlParameter paramProgramRateID = new SqlParameter("@ProgramRateID", SqlDbType.Int);
                paramProgramRateID.Direction = ParameterDirection.Output;
                parameters.Add(paramProgramRateID);

                parameters.Add(new SqlParameter("@ProgramID", Convert.ToInt32(ddlProgram.SelectedValue)));
                parameters.Add(new SqlParameter("@CapitalGroup", txtCapitalGroup.Text));
                parameters.Add(new SqlParameter("@RateDescription", ddlRateDescription.SelectedValue));
                parameters.Add(new SqlParameter("@Rate", Convert.ToDouble(txtRate.Text)));
                parameters.Add(new SqlParameter("@RateText", txtRateText.Text));
                parameters.Add(new SqlParameter("@ModifiedBy", ADUtility.GetUserNameOfAppUser(Request)));

                int recordsInserted = SQLUtility.SqlExecuteNonQueryCount("sp_InsertProgramRate", parameters);

                this.gvProgramRates.EditIndex = -1;
                this.BindRates();
            } // end Page.IsValid

        }

        /// <summary>Gets the rate description collection.</summary>
        /// <returns>the rate description collection</returns>
        public Collection<ListItem> GetRateDescriptions()
        {
            if (Session["RateDescriptionItems"] == null)
            {
                DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetRateDescriptions");

                Collection<ListItem> items = new Collection<ListItem>();
                items.Add(new ListItem("-- Select --", ""));

                foreach (DataRow r in dt.Rows)
                {
                    items.Add(new ListItem(r["RateDescription"].ToString(), r["RateDescription"].ToString()));
                }

                this.DebugListItems(items, "RateDescriptionItems");
                Session["RateDescriptionItems"] = items;
                return items;
            }
            else
            {
                return (Collection<ListItem>)Session["RateDescriptionItems"];
            }
        }

        private void DebugListItems(Collection<ListItem> items, string stringListName)
        {
            foreach (ListItem i in items)
            {
                Debug.WriteLine(stringListName + ": " + i.Text + "; Value: " + i.Value);
            }
        }

        /// <summary>Gets the Programs collection.</summary>
        /// <returns>the Programs collection</returns>
        public Collection<ListItem> GetPrograms()
        {
            if (Session["ProgramItems"] == null)
            {
                DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPrograms");

                Collection<ListItem> items = new Collection<ListItem>();
                items.Add(new ListItem("-- Select --", ""));

                foreach (DataRow r in dt.Rows)
                {
                    items.Add(new ListItem(r["programName"].ToString(), r["programID"].ToString()));
                }

                this.DebugListItems(items, "ProgramItems");
                Session["ProgramItems"] = items;
                return items;
            }
            else
            {
                return (Collection<ListItem>)Session["ProgramItems"];
            }
        }

    }//// end class
}