﻿// ------------------------------------------------------------------
// <copyright file="FileLoadCoo.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary> This is the FileLoadCoo class. </summary>    
    public partial class FileLoadCoo : System.Web.UI.Page
    {
        private int intProgramID = 2;
        
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));
            if (!AppUsersEntity.IsProgramAdmin(myUsers, intProgramID))
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);


            if (!Page.IsPostBack)
            {
                this.BindDocuments();
            }

            this.lblOutput.Text = string.Empty;
        }

        /// <summary>Binds the AppDocuments List to the GridView.</summary>
        protected void BindDocuments()
        {
            List<DocumentEntity> docs = DocumentEntity.GetAppDocumentsByProgramType(this.hdnProgramType.Value);

            string strMainURL =  Page.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            foreach (DocumentEntity doc in docs)
            {
                doc.DocumentURL = strMainURL + "\\COO\\" + doc.FileName;
            }


            if (docs.Count == 0)
            {
                DocumentEntity dummy = new DocumentEntity();
                docs.Add(dummy);
                Debug.WriteLine("Making Dummy Doc: " + dummy.DocumentID.ToString());
            }

            this.gvDocuments.DataSource = docs;
            this.gvDocuments.DataBind();
        }

        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('document');");


                if (this.gvDocuments.DataKeys[e.Row.RowIndex]["DocumentID"].ToString() == "0")
                {
                    e.Row.Visible = false;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtPublishedName = (TextBox)e.Row.FindControl("txtPublishedName");

                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtPublishedName, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the gvDocument GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intDocumentID = Convert.ToInt32(this.gvDocuments.DataKeys[e.RowIndex]["DocumentID"].ToString());
            string strFileName = this.gvDocuments.DataKeys[e.RowIndex]["FileName"].ToString();
            int recordsDeleted = DocumentEntity.DeleteAppDocument(intDocumentID);

            this.gvDocuments.EditIndex = -1;
            this.BindDocuments();

            string strFolder = ConfigurationManager.AppSettings["CountryOfOriginFolder"];
            string strFullFile = strFolder + strFileName;
            if (File.Exists(strFullFile))
                File.Delete(strFullFile);
        }

        /// <summary>Handles the Edit event of the  GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            FileUpload filDocument = (FileUpload)this.gvDocuments.FooterRow.FindControl("filDocument");
            TextBox txtPublishedName = (TextBox)this.gvDocuments.FooterRow.FindControl("txtPublishedName");
            CustomValidator vldFileName = (CustomValidator)this.gvDocuments.FooterRow.FindControl("vldFileName");
            CustomValidator vldFileExists = (CustomValidator)this.gvDocuments.FooterRow.FindControl("vldFileExists");
            RequiredFieldValidator vldRequiredPublishedName = (RequiredFieldValidator)this.gvDocuments.FooterRow.FindControl("vldRequiredPublishedName");

            vldFileExists.IsValid = filDocument.HasFile;
            string strFileName = filDocument.FileName;
            Debug.WriteLine("strFileName: " + strFileName);

            List<DocumentEntity> docs = DocumentEntity.GetAppDocumentsByProgramTypeByFileName(this.hdnProgramType.Value, strFileName);
            vldFileName.IsValid = Convert.ToBoolean(docs.Count == 0);

            string strFolder = ConfigurationManager.AppSettings["CountryOfOriginFolder"];
            string strFullFileName = strFolder + strFileName;

            if (Page.IsValid)
            {
                Debug.WriteLine("COO File Import: " + strFullFileName);
                filDocument.SaveAs(strFullFileName);

                string strUsername = ADUtility.GetUserNameOfAppUser(Request);
                int recordsInserted = DocumentEntity.InsertAppDocument(strFileName, strFolder, txtPublishedName.Text, this.hdnProgramType.Value, strUsername);

                this.gvDocuments.EditIndex = -1;
                this.BindDocuments();
            } // end Page.IsValid

        }

    }
}