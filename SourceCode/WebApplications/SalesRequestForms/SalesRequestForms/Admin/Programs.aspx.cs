﻿// --------------------------------------------------------------
// <copyright file="Programs.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms.Admin
{

    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Programs page</summary>
    public partial class Programs : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            //// if not an admin redirect to Unauthorized Page
            if (!AppUsersEntity.IsAdmin(myUsers))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPrograms");
                this.gvPrograms.DataSource = dt;
                this.gvPrograms.DataBind();
            }

            // Remove ability to add new program
            this.btnNewProgram.Visible = false;

        } //// end Page_Load

        /// <summary>
        /// Handles the Edit event of the gvPrograms GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_View(object sender, GridViewEditEventArgs e)
        {
            string strProgramID = this.gvPrograms.DataKeys[e.NewEditIndex]["programID"].ToString();
            string strURL = "ProgramDetail.aspx?programId=" + strProgramID;
            Debug.WriteLine("strURL: " + strURL);
            Response.Redirect("ProgramDetail.aspx?programId=" + strProgramID, true);
            //  Response.Redirect("ProgramDetail.aspx");
        }

        /// <summary>Binds the users table to the gvPrograms GridView.</summary>
        protected void BindDT()
        {
            DataTable dtPrograms = SQLUtility.SqlExecuteQuery("sp_GetPrograms");

            this.gvPrograms.DataSource = dtPrograms;
            this.gvPrograms.DataBind();

        }//// end BindDT

        /// <summary>Handles the Click event of the btnNewProgram control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnNewProgram_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProgramDetail.aspx", true);
        }

        /// <summary>Handles the RowDataBound event of the gvPrograms GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
 
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnkGuide = (HyperLink)e.Row.FindControl("lnkGuide");
                string strProgramID = this.gvPrograms.DataKeys[e.Row.RowIndex]["programID"].ToString();

                string strFileName = "AdminGuide" + strProgramID + ".pdf";
                string strRelativeUrl = "~/Guides/" + strFileName;

                strFileName = ConfigurationManager.AppSettings["GuideFolder"] + strFileName;

                if (File.Exists(strFileName))
                    lnkGuide.NavigateUrl = strRelativeUrl;

                lnkGuide.Visible = File.Exists(strFileName);
            }


        } // end gv_RowDataBound

    }//// end class
}