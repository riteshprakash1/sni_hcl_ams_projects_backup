﻿<%@ Page Title="Placed Capital Materials - Administration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlacedCapitalMaterials.aspx.cs" Inherits="SalesRequestForms.Admin.PlacedCapitalMaterials" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="font-size: large;text-decoration: underline;"><strong>Administrative Placed Capital Materials</strong></td>
        </tr>          
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="AddMaterial" runat="server" />
                <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="UpdateMaterial" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvMaterial" runat="server" Width="100%" AutoGenerateColumns="False"  ShowFooter="true" 
                    DataKeyNames="MaterialID" CellPadding="2" GridLines="Both" EmptyDataText="There are no Materials." 
                    OnRowDeleting="GV_Delete" OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update" OnRowDataBound="GV_RowDataBound">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                              <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="UpdateMaterial" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="AddMaterial" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderStyle-Width="18%" HeaderText="Capital Group" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCapitalGroup" Font-Size="8pt" runat="server"  Text='<%# Eval("CapitalGroup")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCapitalGroup" MaxLength="50" Font-Size="8pt" runat="server" Text='<%# Eval("CapitalGroup")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldCapitalGroup" ControlToValidate="txtCapitalGroup" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Capital Group is required.">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddCapitalGroup" MaxLength="50" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldAddCapitalGroup" ControlToValidate="txtAddCapitalGroup" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Capital Group is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="11%" HeaderText="Material ID" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblMaterialID" Font-Size="8pt" runat="server"  Text='<%# Eval("MaterialID")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddMaterialID" MaxLength="20" Font-Size="8pt" runat="server" Width="65px"></asp:TextBox>
                                <asp:CustomValidator ID="vldMaterialIDExist" ControlToValidate="txtAddMaterialID" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="The Material ID already exists.">*</asp:CustomValidator>
                                <asp:RequiredFieldValidator ID="vldAddMaterialID" ControlToValidate="txtAddMaterialID" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Material ID is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="48%" HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblMaterialDesc" Font-Size="8pt" runat="server"  Text='<%# Eval("MaterialDesc")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMaterialDesc" MaxLength="50" Font-Size="8pt" runat="server" Width="325px" Text='<%# Eval("MaterialDesc")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldMaterialDesc" ControlToValidate="txtMaterialDesc" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Material Desc is required.">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddMaterialDesc" MaxLength="50" Font-Size="8pt" runat="server" Width="325px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldAddMaterialDesc" ControlToValidate="txtAddMaterialDesc" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Description is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="15%" HeaderText="List Price" HeaderStyle-CssClass="TARight" FooterStyle-CssClass="TARight" ItemStyle-CssClass="TARight">
                            <ItemTemplate>
                                <asp:Label ID="lblListPrice" Font-Size="8pt" runat="server" Text='<%#String.Format("{0:0.00}",Eval("ListPrice")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:RequiredFieldValidator ID="vldRequiredListPrice" ControlToValidate="txtListPrice" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="List Price is required.">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="vldListPrice" runat="server" ControlToValidate="txtListPrice" ForeColor="#FF7300" ValueToCompare="0.00"
                                ErrorMessage="The List Price must be a number greater than or equal to zero." ValidationGroup="UpdateMaterial" 
                                Operator="GreaterThanEqual" Type="Currency">*</asp:CompareValidator>
                                <asp:TextBox ID="txtListPrice" Font-Size="8pt" runat="server" Width="65px" Text='<%#String.Format("{0:0.00}",Eval("ListPrice")) %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:RequiredFieldValidator ID="vldAddRequiredListPrice" ControlToValidate="txtAddListPrice" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="List Price is required.">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="vldAddListPrice" runat="server" ControlToValidate="txtAddListPrice" ForeColor="#FF7300" ValueToCompare="0.00"
                                ErrorMessage="The List Price must be a number greater than or equal to zero with no more than two decimal places." ValidationGroup="AddMaterial" 
                                Operator="GreaterThanEqual" Type="Currency">*</asp:CompareValidator>                            
                                <asp:TextBox ID="txtAddListPrice" Font-Size="8pt" runat="server" Width="65px"></asp:TextBox>
                            </FooterTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>
                               </ItemTemplate>
                            </asp:TemplateField>                            
                    </Columns>
                </asp:GridView>  
             </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddMaterial" runat="server" />
                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateMaterial" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
