﻿<%@ Page Title="Program Rates - Administration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProgramRates.aspx.cs" Inherits="SalesRequestForms.Admin.ProgramRates" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="font-size: large;text-decoration: underline;"><strong>Program Rates & Variables</strong></td>
        </tr>         
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="AddRate" runat="server" />
                <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="UpdateRate" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvProgramRates" runat="server" Width="100%" AutoGenerateColumns="False"  ShowFooter="true" 
                    DataKeyNames="ProgramRateID" CellPadding="2" GridLines="Both" EmptyDataText="There are no Program Rates." 
                    OnRowDeleting="GV_Delete" OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update" OnRowDataBound="GV_RowDataBound">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="UpdateMaterial" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="AddMaterial" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                         <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Program" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProgram" Font-Size="8pt" runat="server"  Text='<%# Eval("programName")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlProgram" width="175px" DataTextField="Text" DataValueField="Value" SelectedValue='<%# Bind("ProgramID") %>' DataSource='<%# GetPrograms()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldPrograms" ControlToValidate="ddlProgram" ValidationGroup="UpdateRate" ForeColor="#FF7300" runat="server" ErrorMessage="Program is required.">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAddProgram" width="175px" DataTextField="Text" DataValueField="Value" DataSource='<%# GetPrograms()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldAddProgram" ControlToValidate="ddlAddProgram" ValidationGroup="AddRate" ForeColor="#FF7300" runat="server" ErrorMessage="Programs is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="14%" HeaderText="Capital Group" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCapitalGroup" Font-Size="8pt" runat="server"  Text='<%# Eval("CapitalGroup")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCapitalGroup" MaxLength="50" Font-Size="8pt" runat="server" Text='<%# Eval("CapitalGroup")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldCapitalGroup" ControlToValidate="txtCapitalGroup" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Capital Group is required.">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddCapitalGroup" MaxLength="50" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldAddCapitalGroup" ControlToValidate="txtAddCapitalGroup" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Capital Group is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="23%" HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRateDescription" Font-Size="8pt" runat="server"  Text='<%# Eval("RateDescription")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlRateDescription" width="200px" DataTextField="Text" DataValueField="Value" SelectedValue='<%# Bind("RateDescription") %>' DataSource='<%# GetRateDescriptions()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldRateDescription" ControlToValidate="ddlRateDescription" ValidationGroup="UpdateRate" ForeColor="#FF7300" runat="server" ErrorMessage="Rate Description is required.">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAddRateDescription" width="200px" DataTextField="Text" DataValueField="Value" DataSource='<%# GetRateDescriptions()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldAddRateDescription" ControlToValidate="ddlAddRateDescription" ValidationGroup="AddRate" ForeColor="#FF7300" runat="server" ErrorMessage="Rate Description is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:TemplateField HeaderStyle-Width="7%" HeaderText="Rate/Variable"  ItemStyle-CssClass="TARight" FooterStyle-CssClass="TARight" HeaderStyle-CssClass="TARight">
                            <ItemTemplate>
                                <asp:Label ID="lblRate" Font-Size="8pt" runat="server" Text='<%#String.Format("{0:0.00}",Eval("Rate")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:RequiredFieldValidator ID="vldRequiredRate" ControlToValidate="txtRate" ValidationGroup="UpdateRate" ForeColor="#FF7300" runat="server" ErrorMessage="Rate is required.">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="vldRate" runat="server" ControlToValidate="txtRate" ForeColor="#FF7300" ValueToCompare="0.00"
                                ErrorMessage="The Rate must be a number greater than or equal to zero." ValidationGroup="UpdateRate" 
                                Operator="GreaterThanEqual" Type="Double">*</asp:CompareValidator>
                                <asp:TextBox ID="txtRate" Font-Size="8pt" runat="server" Width="45px" Text='<%#String.Format("{0:0.00}",Eval("Rate")) %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:RequiredFieldValidator ID="vldAddRequiredRate" ControlToValidate="txtAddRate" ValidationGroup="AddMaterial" ForeColor="#FF7300" runat="server" ErrorMessage="Rate is required.">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="vldAddRate" runat="server" ControlToValidate="txtAddRate" ForeColor="#FF7300" ValueToCompare="0.00"
                                ErrorMessage="The Rate must be a number greater than or equal to zero." ValidationGroup="AddRate" 
                                Operator="GreaterThanEqual" Type="Double">*</asp:CompareValidator>                            
                                <asp:TextBox ID="txtAddRate" Font-Size="8pt" runat="server" Width="45px"></asp:TextBox>
                            </FooterTemplate>
                            </asp:TemplateField> 
                        <asp:TemplateField HeaderStyle-Width="12%" HeaderText="Rate/Variable Text" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRateText" Font-Size="8pt" runat="server" Text='<%# Eval("RateText")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRateText" Font-Size="8pt" runat="server" Width="150px" Text='<%# Eval("RateText")%>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddRateText" Font-Size="8pt" runat="server" Width="150px"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>
                               </ItemTemplate>
                            </asp:TemplateField>                            
                    </Columns> 	
                </asp:GridView>  
             </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddRate" runat="server" />
                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateRate" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
