﻿<%@ Page Title="Import Pricing Material Matrix" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ImportPricingMaterials.aspx.cs" Inherits="SalesRequestForms.Admin.ImportPricingMaterials" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <span>Import Pricing Material Matrix</span>
    <table style="width:800px">
        <tr>
            <td colspan="3">The import process will perform the following actions:<br />
            - all existing Pricing Material Matrix records will be deleted from the database table<br />
            - all the Pricing Material Matrix records from the import file will be added to the database table<br />
            - if any part of the operation fails the old Pricing Material Matrix records will be restored to the database table and an error message will be displayed<br />
            - if the opoeration is successful a message will be displayed indicatiing the number of records deleted and the number of records imported<br /><br /> 
            </td>
        </tr>
        <tr>
            <td colspan="3">1.	Verify the Excel file spreadsheet has the same format as this 
                <asp:LinkButton ID="btnExcelFormat" Text="Sample Import Spreadsheet" 
                    runat="server" onclick="btnExcelFormat_Click">Sample Import Spreadsheet</asp:LinkButton>.<br />
                - including the column names and spreadsheet tab name ('Parts List').<br />
                2.	Browse to the excel file you created on your computer.<br />
                3.	Click the upload button.<br />
                4.	Wait for it to process.<br />
            </td>
        </tr>
        <tr>
            <td>
                File (xlsx):
            </td>
            <td>
                <asp:FileUpload ID="filMaterials" Width="300" runat="server" />
                <asp:CustomValidator ID="vldFileName" runat="server" ForeColor="#FF7300" ErrorMessage="Unable to process; must be a *.xlsx file.">*</asp:CustomValidator>
                <asp:CustomValidator ID="vldFileExists" runat="server" ForeColor="#FF7300" ErrorMessage="The file could not be uploaded.">*</asp:CustomValidator>
            </td>
            <td>
                
                <uc1:waitbutton id="btnLoad"  runat="server" Text='Upload' Font-Size="10pt" Height="22" 
				        CausesValidation="False"  Width="100"
				        WaitText="Processing..." onclick="btnLoad_Click"></uc1:waitbutton>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td><asp:ValidationSummary ID="ValidationSummary1" ForeColor="#FF7300" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblOutput" runat="server" ForeColor="#FF7300" Text=""></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>
