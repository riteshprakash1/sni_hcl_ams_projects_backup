﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SalesRequestForms.Classes;
using SalesRequestForms.PlacedCapital.Classes;

namespace SalesRequestForms.Admin
{
    public partial class PlacedCapitalMaterials : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //// if not an admin redirect to Unauthorized Page
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            if (!AppUsersEntity.IsProgramAdmin(myUsers, 1))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.BindMaterial();
            }

        }

        /// <summary>Binds the material entity collection to the gvMaterial GridView.</summary>
        protected void BindMaterial()
        {
            Collection<MaterialEntity> collMaterial = MaterialEntity.GetMaterialEntityCollection("sp_GetAllPlacedCapitalMaterial");
            this.gvMaterial.DataSource = collMaterial;
            this.gvMaterial.DataBind();
        }

        /// <summary>Handles the Delete event of the gvMaterial GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strMaterialID = this.gvMaterial.DataKeys[e.RowIndex]["MaterialID"].ToString();

            int rowsDeleted = MaterialEntity.DeletePlacedCapitalMaterial(strMaterialID);
            this.gvMaterial.EditIndex = -1;
            this.BindMaterial();
        }

        /// <summary>Handles the Edit event of the gvCost GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvMaterial.EditIndex = e.NewEditIndex;
            this.BindMaterial();
        }

        /// <summary>Handles the Update event of the gvCost GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateMaterial");

            if (Page.IsValid)
            {
                string strMaterialID = this.gvMaterial.DataKeys[e.RowIndex]["MaterialID"].ToString();
                TextBox txtMaterialDesc = (TextBox)this.gvMaterial.Rows[e.RowIndex].FindControl("txtMaterialDesc");
                TextBox txtCapitalGroup = (TextBox)this.gvMaterial.Rows[e.RowIndex].FindControl("txtCapitalGroup");
                TextBox txtListPrice = (TextBox)this.gvMaterial.Rows[e.RowIndex].FindControl("txtListPrice");

                MaterialEntity m = new MaterialEntity();
                m.MaterialID = strMaterialID;
                m.MaterialDesc = txtMaterialDesc.Text;
                m.CapitalGroup = txtCapitalGroup.Text;
                m.ListPrice = Convert.ToDouble(txtListPrice.Text);

                int rowsUpdated = MaterialEntity.UpdatePlacedCapitalMaterial(m, ADUtility.GetUserNameOfAppUser(Request));

                this.gvMaterial.EditIndex = -1;
                this.BindMaterial();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>Handles the Edit event of the gvMaterial GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddMaterial");
            TextBox txtMaterialID = (TextBox)this.gvMaterial.FooterRow.FindControl("txtAddMaterialID");
            CustomValidator vldMaterialIDExist = (CustomValidator)this.gvMaterial.FooterRow.FindControl("vldMaterialIDExist");

            MaterialEntity mat = MaterialEntity.GetPlacedCapitalMaterialByMaterialID(txtMaterialID.Text);

            vldMaterialIDExist.IsValid = Convert.ToBoolean(mat == null);

            if (Page.IsValid)
            {
                TextBox txtMaterialDesc = (TextBox)this.gvMaterial.FooterRow.FindControl("txtAddMaterialDesc");
                TextBox txtCapitalGroup = (TextBox)this.gvMaterial.FooterRow.FindControl("txtAddCapitalGroup");
                TextBox txtListPrice = (TextBox)this.gvMaterial.FooterRow.FindControl("txtAddListPrice");

                MaterialEntity m = new MaterialEntity();
                m.MaterialID = txtMaterialID.Text;
                m.MaterialDesc = txtMaterialDesc.Text;
                m.CapitalGroup = txtCapitalGroup.Text;
                m.ListPrice = Convert.ToDouble(txtListPrice.Text);

                int rowsUpdated = MaterialEntity.InsertPlacedCapitalMaterial(m, ADUtility.GetUserNameOfAppUser(Request));

                this.gvMaterial.EditIndex = -1;
                this.BindMaterial();
            } // end Page.IsValid

        }

        /// <summary>Handles the RowDataBound event of the gvMaterial GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('placed capital material');");

                if (e.Row.RowIndex == this.gvMaterial.EditIndex)
                {
                    TextBox txtCapitalGroup = (TextBox)e.Row.FindControl("txtCapitalGroup");
                    TextBox txtMaterialDesc = (TextBox)e.Row.FindControl("txtMaterialDesc");
                    TextBox txtListPrice = (TextBox)e.Row.FindControl("txtListPrice");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtCapitalGroup, btnUpdate);
                    EnterButton.TieButton(txtMaterialDesc, btnUpdate);
                    EnterButton.TieButton(txtListPrice, btnUpdate);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddCapitalGroup = (TextBox)e.Row.FindControl("txtAddCapitalGroup");
                TextBox txtAddMaterialID = (TextBox)e.Row.FindControl("txtAddMaterialID");
                TextBox txtAddMaterialDesc = (TextBox)e.Row.FindControl("txtAddMaterialDesc");
                TextBox txtAddListPrice = (TextBox)e.Row.FindControl("txtAddListPrice");

                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddMaterialID, btnAdd);
                EnterButton.TieButton(txtAddMaterialDesc, btnAdd);
                EnterButton.TieButton(txtAddCapitalGroup, btnAdd);
                EnterButton.TieButton(txtAddListPrice, btnAdd);
            }
        } // end gv_RowDataBound


    } //// end class
}