﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="EmailTest.aspx.cs" Inherits="SalesRequestForms.Admin.EmailTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table>
        <tr>
            <td>Email To:</td>
            <td><asp:TextBox ID="txtEmailTo" runat="server" Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Email From:</td>
            <td><asp:TextBox ID="txtEmailFrom" runat="server" Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Email CC:</td>
            <td><asp:TextBox ID="txtEmailCC" runat="server" Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>HTML Format:</td>
            <td><asp:DropDownList ID="ddlIsHtml" runat="server">
                <asp:ListItem>true</asp:ListItem>
                <asp:ListItem>false</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        
        <tr>
            <td>Subject:</td>
            <td><asp:TextBox ID="txtSubject" runat="server" Width="500px"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="vertical-align:top">Body:</td>
            <td><asp:TextBox ID="txtBody" runat="server" TextMode="MultiLine" Rows="3"  Width="500px"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Button ID="btnSendEmail" runat="server" Text="Send Email" 
                    onclick="btnSendEmail_Click" /></td>
            <td style="text-align:center"><asp:Button ID="btnConfigAdmin" runat="server" 
                    Text="Go to Config Admin" onclick="btnConfigAdmin_Click"/></td>
        </tr>
        <tr>
            <td colspan="2"><asp:Label ID="lblErrorMsg" runat="server" ForeColor="#FF7300"></asp:Label></td>
        </tr>
    </table>
</asp:Content>
