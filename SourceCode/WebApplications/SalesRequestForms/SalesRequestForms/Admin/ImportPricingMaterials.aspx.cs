﻿// ------------------------------------------------------------------
// <copyright file="ImportPricingMaterials.cs.aspx" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Web.UI.WebControls;

    using SalesRequestForms.Classes;
    using System.Data.OleDb;

    /// <summary> This is the page to import the eligible pricing materials into the database.</summary>
    public partial class ImportPricingMaterials : System.Web.UI.Page
    {
        private int intProgramID = 2;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            if (!AppUsersEntity.IsProgramAdmin(myUsers, intProgramID))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            this.lblOutput.Text = string.Empty;
        }

        /// <summary> Handles the Click event of the btnLoad control. </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            Page.Validate();

            this.vldFileName.IsValid = Convert.ToBoolean((this.filMaterials.FileName.ToLower().EndsWith("xlsx") == true));
            this.vldFileExists.IsValid = filMaterials.HasFile;

            // Added so screen accurately displays the characters import from the file
            Response.ContentEncoding = System.Text.Encoding.Default;

            string strFileName = String.Empty;
            if (Page.IsValid)
            {
                try
                {
                    strFileName = ConfigurationManager.AppSettings["MatrixImportFolder"] + "MatrixFile-" + DateTime.Now.ToBinary().ToString() + ".xlsx";
                    Debug.WriteLine("Material File Import: " + strFileName);
                    this.filMaterials.SaveAs(strFileName);

                    this.ImportDataFromExcel(strFileName);
                }
                catch (Exception ex)
                {
                    this.lblOutput.Text = "Import Operation Failed: " + ex.Message;
                    this.lblOutput.Text = "Operation Failed during the File Import process: " + ex.Message;
                }
            }
            else
            {
                this.vldFileName.ErrorMessage = "Unable to process " + this.filMaterials.FileName + " must be a *.xlsx file.";
            }
        }


        /// <summary>This imports the data from the Excel file into SQL Server</summary>
        /// <param name="excelfilepath">The fully qualified name of the excel file.</param>
        public void ImportDataFromExcel(string excelfilepath)
        {
            // make sure your sheet name is correct, here sheet name is Parts List
            string strExcelQuery = "select Material_Id, Marketing_Manager, Internal_Floor_Price, Rep_Price, Rep_Discount, DE_Price, DE_Discount, RVP_Price, RVP_Discount from [Parts List$]";
            Debug.WriteLine("strExcelQuery: " + strExcelQuery);

            SqlConnection sqlConn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = sqlConn.BeginTransaction();

            string strProcessLocation = "Table Delete Process";
            
            try
            {
                DataTable dtExcel = SQLUtility.SqlExecuteDynamicExeclQuery(strExcelQuery, excelfilepath);
                Debug.WriteLine("dtExcel rows: " + dtExcel.Rows.Count.ToString());

                Collection<SqlParameter> deleteParameters = new Collection<SqlParameter>();
                int intDeletedRecords = SQLUtility.SqlExecuteNonQueryTransactionCount("sp_DeleteAllPricingMaterialMatrix", deleteParameters, sqlConn, trans);

                int intInsertedRecords = 0;

                strProcessLocation = "Table Insert Process";
                
                foreach (DataRow r in dtExcel.Rows)
                {
                    intInsertedRecords++;
                    strProcessLocation = "Table Insert Process (Row: " + (intInsertedRecords+1).ToString() + ") ";
                    Collection<SqlParameter> insertParameters = new Collection<SqlParameter>();

                    insertParameters.Add(new SqlParameter("@Material_Id", r["Material_Id"].ToString()));
                    insertParameters.Add(new SqlParameter("@Marketing_Manager", r["Marketing_Manager"].ToString()));
                    insertParameters.Add(new SqlParameter("@Internal_Floor_Price", r["Internal_Floor_Price"].ToString()));

                    if (r["Material_Id"].ToString() == "010670")
                        Debug.Write("010670: " + r["Rep_Discount"].ToString());

                    if(r["Rep_Discount"].ToString() == string.Empty)
                        insertParameters.Add(new SqlParameter("@Rep_Discount", Convert.ToDecimal(0.00)));
                    else
                        insertParameters.Add(new SqlParameter("@Rep_Discount", Convert.ToDecimal(r["Rep_Discount"].ToString())));

                    if (r["Rep_Price"].ToString() == string.Empty)
                        insertParameters.Add(new SqlParameter("@Rep_Price", Convert.ToDecimal(0.00)));
                    else
                        insertParameters.Add(new SqlParameter("@Rep_Price", Convert.ToDouble(r["Rep_Price"].ToString())));

                    if (r["DE_Discount"].ToString() == string.Empty)
                        insertParameters.Add(new SqlParameter("@DE_Discount", Convert.ToDecimal(0.00)));
                    else
                        insertParameters.Add(new SqlParameter("@DE_Discount", Convert.ToDecimal(r["DE_Discount"].ToString())));

                    if (r["DE_Price"].ToString() == string.Empty)
                        insertParameters.Add(new SqlParameter("@DE_Price", Convert.ToDecimal(0.00)));
                    else
                        insertParameters.Add(new SqlParameter("@DE_Price", Convert.ToDouble(r["DE_Price"].ToString())));

                    if (r["RVP_Discount"].ToString() == string.Empty)
                        insertParameters.Add(new SqlParameter("@RVP_Discount", Convert.ToDecimal(0.00)));
                    else
                        insertParameters.Add(new SqlParameter("@RVP_Discount", Convert.ToDecimal(r["RVP_Discount"].ToString())));

                    if (r["RVP_Price"].ToString() == string.Empty)
                        insertParameters.Add(new SqlParameter("@RVP_Price", Convert.ToDecimal(0.00)));
                    else
                        insertParameters.Add(new SqlParameter("@RVP_Price", Convert.ToDouble(r["RVP_Price"].ToString())));

                    SQLUtility.SqlExecuteNonQueryTransactionCount("sp_InsertPricingMaterialMatrix", insertParameters, sqlConn, trans);

                }

                trans.Commit();
                sqlConn.Close();

                this.lblOutput.Text = intDeletedRecords.ToString() + " rows deleted from Pricing Material Matrix table.<br>" + intInsertedRecords.ToString() + " rows imported into the Pricing Material Matrix table.";
            }
            catch (Exception ex)
            {
                trans.Rollback();
                this.lblOutput.Text = "Operation Failed during the " + strProcessLocation + ": "  + ex.Message;
                LogException.HandleException(ex, Request);
            }
        }

        protected void btnExcelFormat_Click(object sender, EventArgs e)
        {
            string strSampleFilePath = Server.MapPath("~/admin/SampleMaterialMatrix.xlsx"); 
            
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + strSampleFilePath);
            Response.TransmitFile(strSampleFilePath);
            Response.End();

        }


    }////end class
}