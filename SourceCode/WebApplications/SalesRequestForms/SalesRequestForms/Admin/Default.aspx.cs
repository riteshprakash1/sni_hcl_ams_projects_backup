﻿// ------------------------------------------------------------------
// <copyright file="Default.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary> This is the Default class. </summary>    
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

            if (!AppUsersEntity.IsAdmin(myUsers))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.SetControls(myUsers);
            }
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkProgramRates_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/ProgramRates.aspx", false);
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkPlacedCapitalMaterials_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/PlacedCapitalMaterials.aspx", false);
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkPricingEligibleMaterials_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/PricingEligibleMaterials.aspx", false);
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkImportPricingMaterialMatrix_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/ImportPricingMaterials.aspx", false);
        }

        /// <summary>Sets the visible attribute of the controls</summary>
        /// <param name="myUsers">The AppUsersEntity List.</param>
        protected void SetControls(List<AppUsersEntity> myUsers)
        {
            bool IsPlacedCapitalAdmin = AppUsersEntity.IsProgramAdmin(myUsers, 1);
            this.lnkPlacedCapitalMaterials.Visible = IsPlacedCapitalAdmin;
            this.lnkPlacedCapitalUserList.Visible = IsPlacedCapitalAdmin;
            this.lnkProgramRates.Visible = IsPlacedCapitalAdmin;
            this.LabelPlacedCapital.Visible = IsPlacedCapitalAdmin;

            bool IsPricingRequestAdmin = AppUsersEntity.IsProgramAdmin(myUsers, 2);
            this.lnkPricingEligibleMaterials.Visible = IsPricingRequestAdmin;
            this.lnkPricingRequestUserList.Visible = IsPricingRequestAdmin;
            this.lnkImportPricingMaterialMatrix.Visible = IsPricingRequestAdmin;
            this.LabelPricingRequest.Visible = IsPricingRequestAdmin;

        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkPlacedCapitalUserList_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/UserList.aspx?programID=1", false);
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkPricingRequestUserList_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/UserList.aspx?programID=2", false);
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkPrograms_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/Programs.aspx", false);
        }

        /// <summary>Handles the click event of the link control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void lnkFileLoadCOO_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/FileLoadCoo.aspx", false);
        }
    }
}