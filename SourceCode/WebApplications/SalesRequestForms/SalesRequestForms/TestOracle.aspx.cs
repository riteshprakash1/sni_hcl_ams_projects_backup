﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.Diagnostics;

namespace SalesRequestForms
{
    public partial class TestOracle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            OleDbConnection myConn = OpenOracleDatahubSqlConnection();
            Response.Write("Connection State" + myConn.State.ToString());
            myConn.Close();
            Response.Write("<br>Connection State:" + myConn.State.ToString());
            myConn.Dispose();


            string strQuery = "SELECT  DISTINCT CUSTMR_ACCNT_NO as Customer_ID, CUSTMR_ACCNT_1_NM as Customer_Name, STREET_ADDRESS as Street, City_NM as CITY, REGION_CD as State_Province_Code, POSTAL_CD  as Postal_Code, CUSTMR_SLS_GRP_1_CD as Gpo_ID, CUSTMR_SLS_GRP_1_DSC as Gpo, CUSTMR_SLS_GRP_2_CD as Cust_Group1_Id, CUSTMR_SLS_GRP_2_DSC as Cust_Group1, PARTNER as PARTNER_FUNCTION, PARTNER_FUNCTION_DESC, SLS_ORGANSTN_CD  as Sales_Org_ID FROM SEC_WEBPRC.WEBPRC_01_v WHERE CUSTMR_ACCNT_NO  = '0000203605'";
            DataTable dt = OracleExecuteDynamicQuery(strQuery);
            Response.Write("<br>Query Row Count: " + dt.Rows.Count.ToString());

        }



        /// <summary>Opens a new DataHub Oracle connection.</summary>
        /// <returns>Returns a Oracle connection</returns>
        public static OleDbConnection OpenOracleDatahubSqlConnection()
        {
            OleDbConnection myConnection = new OleDbConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DataHubConn"].ToString();
                myConnection.Open();
            }

            return myConnection;
        }

        /// <summary>Executes a query.</summary>
        /// <param name="sQuery">The text of the query to execute</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable OracleExecuteDynamicQuery(string sQuery)
        {
            DataTable myDataTable = new DataTable();
            OleDbConnection OracleConn = OpenOracleDatahubSqlConnection();

            using (OracleConn)
            {
                OleDbCommand command = new OleDbCommand();
                command.Connection = OracleConn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;

                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(command);
                myDataAdapter.Fill(myDataTable);
                myDataAdapter.Dispose();
            }

            return myDataTable;
        }
    }
}