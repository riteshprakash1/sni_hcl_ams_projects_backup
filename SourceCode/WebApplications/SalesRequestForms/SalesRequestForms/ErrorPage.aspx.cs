﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace SalesRequestForms
{
    /// <summary>
    /// This is the page users are redirected 
    /// to when an application error occurs.
    /// </summary>
    public partial class ErrorPage : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ErrorPage Page_Load");

            if (Request.QueryString["ErrorMsg"] != null)
            {
                if(Request.QueryString["ErrorMsg"].ToLower() == "a potentially dangerous request.form value")
                    this.lblErrorMsg.Text = "Invalid character(s) as input '&lt;' '&gt;'.  Please click the browser back button and remove the characters from any of the input fields.";
                else
                    this.lblErrorMsg.Text = "<b>Error Message:</b> " + Request.QueryString["ErrorMsg"];
            }
        }
    }
}
