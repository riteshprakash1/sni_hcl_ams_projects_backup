using System;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SalesRequestForms.Classes
{
	/// <summary>
	/// Summary description for Impersonator.
	/// </summary>
	public class Impersonator
	{

		public const int LOGON32_LOGON_INTERACTIVE = 2;
		public const int LOGON32_PROVIDER_DEFAULT = 0;

		WindowsImpersonationContext impersonationContext;

		public Impersonator()
		{
			//
			// TODO: Add constructor logic here
			//
		} // END constructor

		[DllImport("advapi32.dll")]
		public static extern int LogonUserA(String lpszUserName,
			String lpszDomain, String lpszPassword, int dwLogonType,
			int dwLogonProvider, ref IntPtr phToken);

		[DllImport("advapi32.dll", CharSet=CharSet.Auto, SetLastError=true)]
		public static extern int DuplicateToken(IntPtr hToken,
			int impersonationLevel, ref IntPtr hNewToken);

		[DllImport("advapi32.dll", CharSet=CharSet.Auto, SetLastError=true)]
		public static extern bool RevertToSelf();

		[DllImport("kernel32.dll", CharSet=CharSet.Auto)]
		public static extern  bool CloseHandle(IntPtr handle);

		public void GetUserInfo(string user, string domain, string pswd)
		{
			if(this.impersonateValidUser(user, domain, pswd))
			{
				Debug.WriteLine("Impersonator ID : " + WindowsIdentity.GetCurrent().Name);
				//Insert your code that runs under the security context of a specific user here.
				undoImpersonation();
				Debug.WriteLine("undoImpersonator ID : " + WindowsIdentity.GetCurrent().Name);
			}
			else
			{
				Debug.WriteLine("impersonateValidUser FAILED");
			}
		}

		public bool impersonateValidUser(String userName, String domain, String password)
		{
			WindowsIdentity tempWindowsIdentity;
			IntPtr token = IntPtr.Zero;
			IntPtr tokenDuplicate = IntPtr.Zero;

			if(RevertToSelf())
			{
				if(LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
					LOGON32_PROVIDER_DEFAULT, ref token) != 0)
				{
					if(DuplicateToken(token, 2, ref tokenDuplicate) != 0)
					{
						tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
						impersonationContext = tempWindowsIdentity.Impersonate();
						if (impersonationContext != null)
						{
							CloseHandle(token);
							CloseHandle(tokenDuplicate);
							return true;
						}
					} // END Dup
				} // END LogonUserA
			} // END RevertToSelf()
			if(token!= IntPtr.Zero)
				CloseHandle(token);
			if(tokenDuplicate!=IntPtr.Zero)
				CloseHandle(tokenDuplicate);
			return false;
		}

		public void undoImpersonation()
		{
            Debug.WriteLine("undoImpersonation");
            impersonationContext.Undo();
		}

		public bool IsValidLogon(String userName, String domain, String password)
		{
			IntPtr token = IntPtr.Zero;
			bool isValid = false;

			if(RevertToSelf())
			{
				if(LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
					LOGON32_PROVIDER_DEFAULT, ref token) != 0)
				{
					isValid = true;
					RevertToSelf();
				}
			} // END if RevertToSelf()

			return isValid;
		} // END IsValidLogon


	} // END class
} // END namespace
