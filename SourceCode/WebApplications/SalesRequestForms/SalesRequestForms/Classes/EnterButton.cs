﻿// --------------------------------------------------------------
// <copyright file="EnterButton.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;

    /// <summary>
    /// This associates a button control with a textbox control so when the user hits the enter key 
    /// after inputting data in a textbox the program executes the associated button's onclcik event.
    /// </summary>
    public class EnterButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnterButton"/> class.
        /// </summary>
        public EnterButton()
        {
        }

        /// <summary>This ties a textbox to a button.</summary>
        /// <param name="textBoxToTie">
        ///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
        ///     and the html control should accept an 'onkeydown' attribute.
        /// </param>
        /// <param name="buttonToTie">This is the button to tie to. All we need from this is it's ClientID. The Html tag it renders should support click()</param>
        public static void TieButton(System.Web.UI.Control textBoxToTie, System.Web.UI.Control buttonToTie)
        {
            string formName;
            try
            {
                int i = 0;
                System.Web.UI.Control c = buttonToTie.Parent;
                //// Step up the control hierarchy until either:
                //// 1) We find an HtmlForm control
                //// 2) We find a Page control - not what we want, but we should stop searching because we a Page will be higher than the HtmlForm.
                //// 3) We complete 500 iterations. Obviously we are in a loop, and should stop.
                while (!(c is System.Web.UI.HtmlControls.HtmlForm) & !(c is System.Web.UI.Page) && i < 500)
                {
                    c = c.Parent;
                    i++;
                }
                //// If we have found an HtmlForm, we use it's ClientID for the formName.
                //// If not, we use the first form on the page ("forms[0]").
                if (c is System.Web.UI.HtmlControls.HtmlForm)
                {
                    formName = c.ClientID;
                }
                else
                {
                    formName = "forms[0]";
                }
            }
            catch
            {
                ////If we catch an exception, we should use the first form on the page ("forms[0]").
                formName = "forms[0]";
            }
            //// Tie the button.
            TieButton(textBoxToTie, buttonToTie, formName);
        }

        /// <summary>This ties a textbox to a button.</summary>
        /// <param name="textBoxToTie">
        ///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
        ///     and the html control should accept an 'onkeydown' attribute.
        /// </param>
        /// <param name="buttonToTie">This is the button to tie to. All we need from this is it's ClientID. The Html tag it renders should support click()</param>
        /// <param name="formName">This is the ClientID of the form that the button resides in.</param>
        public static void TieButton(System.Web.UI.Control textBoxToTie, System.Web.UI.Control buttonToTie, string formName)
        {
            //// This is our javascript - we fire the client-side click event of the button if the enter key is pressed.
            string javascriptString = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {document." + formName + ".elements['" + buttonToTie.UniqueID + "'].click();return false;} else return true; ";
            //// We attach this to the onkeydown attribute - we have to cater for HtmlControl or WebControl.
            if (textBoxToTie is System.Web.UI.HtmlControls.HtmlControl)
            {
                ((System.Web.UI.HtmlControls.HtmlControl)textBoxToTie).Attributes.Add("onkeydown", javascriptString);
            }
            else if (textBoxToTie is System.Web.UI.WebControls.WebControl)
            {
                ((System.Web.UI.WebControls.WebControl)textBoxToTie).Attributes.Add("onkeydown", javascriptString);
            }
            else
            {
                //// We throw an exception if TextBoxToTie is not of type HtmlControl or WebControl.
                throw new ArgumentException("Control TextBoxToTie should be derived from either System.Web.UI.HtmlControls.HtmlControl or System.Web.UI.WebControls.WebControl", "TextBoxToTie");
            }
        }

 /************************************ Do Tab ****************************************************************/

        /// <summary>This ties a textbox to a button.</summary>
        /// <param name="textBoxToTie">
        ///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
        ///     and the html control should accept an 'onkeydown' attribute.
        /// </param>
        public static void TieButtonDoTab(System.Web.UI.WebControls.TextBox textBoxToTie)
        {
            //// This is our javascript - we fire the client-side click event of the button if the enter key is pressed.
            string javascriptString = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {event.keyCode=9;return event.keyCode;} else return true; ";
            //// We attach this to the onkeydown attribute - we have to cater for HtmlControl or WebControl.

            textBoxToTie.Attributes.Add("onkeydown", javascriptString);
        }

        /// <summary>Ties the button do tab.</summary>
        /// <param name="textBoxToTie">The text box to tie.</param>
        public static void TieButtonDoTab(System.Web.UI.HtmlControls.HtmlInputText textBoxToTie)
        {
            //// This is our javascript - we fire the client-side click event of the button if the enter key is pressed.
            string javascriptString = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {event.keyCode=9;return event.keyCode;} else return true; ";
            //// We attach this to the onkeydown attribute - we have to cater for HtmlControl or WebControl.

            textBoxToTie.Attributes.Add("onkeydown", javascriptString);
        }

        /************************************ Do Nothing ****************************************************************/

        /// <summary>This ties a textbox to a button.</summary>
        /// <param name="textBoxToTie">
        ///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
        ///     and the html control should accept an 'onkeydown' attribute.
        /// </param>
        /// <param name="buttonToTie">This is the button to tie to. All we need from this is it's ClientID. The Html tag it renders should support click()</param>
        public static void TieButtonDoNothing(System.Web.UI.Control textBoxToTie, System.Web.UI.Control buttonToTie)
        {
            string formName;
            try
            {
                int i = 0;
                System.Web.UI.Control c = buttonToTie.Parent;
                //// Step up the control hierarchy until either:
                //// 1) We find an HtmlForm control
                //// 2) We find a Page control - not what we want, but we should stop searching because we a Page will be higher than the HtmlForm.
                //// 3) We complete 500 iterations. Obviously we are in a loop, and should stop.
                while (!(c is System.Web.UI.HtmlControls.HtmlForm) & !(c is System.Web.UI.Page) && i < 500)
                {
                    c = c.Parent;
                    i++;
                }
                //// If we have found an HtmlForm, we use it's ClientID for the formName.
                //// If not, we use the first form on the page ("forms[0]").
                if (c is System.Web.UI.HtmlControls.HtmlForm)
                {
                    formName = c.ClientID;
                }
                else
                {
                    formName = "forms[0]";
                }
            }
            catch
            {
                ////If we catch an exception, we should use the first form on the page ("forms[0]").
                formName = "forms[0]";
            }
            //// Tie the button.
            TieButtonDoNothing(textBoxToTie, buttonToTie, formName);
        }

        /// <summary>This ties a textbox to a button.</summary>
        /// <param name="textBoxToTie">
        ///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
        ///     and the html control should accept an 'onkeydown' attribute.
        /// </param>
        /// <param name="buttonToTie">This is the button to tie to. All we need from this is it's ClientID. The Html tag it renders should support click()</param>
        /// <param name="formName">This is the ClientID of the form that the button resides in.</param>
        public static void TieButtonDoNothing(System.Web.UI.Control textBoxToTie, System.Web.UI.Control buttonToTie, string formName)
        {
            //// This is our javascript - we fire the client-side click event of the button if the enter key is pressed.
            string javascriptString = "if ((event.which && event.which == 9) || (event.keyCode && event.keyCode == 9)) {event.keyCode=13;return event.keyCode;} else return true; ";
            //// We attach this to the onkeydown attribute - we have to cater for HtmlControl or WebControl.
            if (textBoxToTie is System.Web.UI.HtmlControls.HtmlControl)
            {
                ((System.Web.UI.HtmlControls.HtmlControl)textBoxToTie).Attributes.Add("onkeydown", javascriptString);
            }
            else if (textBoxToTie is System.Web.UI.WebControls.WebControl)
            {
                ((System.Web.UI.WebControls.WebControl)textBoxToTie).Attributes.Add("onkeydown", javascriptString);
            }
            else
            {
                //// We throw an exception if TextBoxToTie is not of type HtmlControl or WebControl.
                throw new ArgumentException("Control TextBoxToTie should be derived from either System.Web.UI.HtmlControls.HtmlControl or System.Web.UI.WebControls.WebControl", "TextBoxToTie");
            }
        }
    } //// End Class EnterButton
}