﻿// ------------------------------------------------------------------
// <copyright file="UserTerritoryEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the User Document Entity. </summary>    
    public class DocumentEntity
    {

        // documentID, [fileName], folderName, publishedName, programType

        /// <summary>This is the DocumentID.</summary>
        private int intDocumentID;

        /// <summary>This is the FileName.</summary>
        private string strFileName;

        /// <summary>This is the FolderName.</summary>
        private string strFolderName;

        /// <summary>This is the PublishedName.</summary>
        private string strPublishedName;
        
        /// <summary>This is the ProgramType.</summary>
        private string strProgramType;
        
        /// <summary>This is the Created By.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>This is the Document URL.</summary>
        private string strDocumentURL;

        /// <summary>Initializes a new instance of the DocumentEntity class.</summary>
        public DocumentEntity()
        {
            this.DocumentID = 0;
            this.CreatedBy = String.Empty;
            this.CreateDate = String.Empty;
            this.FileName = String.Empty;
            this.FolderName = String.Empty;
            this.ProgramType = String.Empty;
            this.PublishedName = String.Empty;
            this.DocumentURL = String.Empty;
        }

        /// <summary>Gets or sets the DocumentID.</summary>
        /// <value>The DocumentID.<value> 
        public int DocumentID { get { return this.intDocumentID; } set { this.intDocumentID = value; } }

        /// <summary>Gets or sets the FileName.</summary>
        /// <value>The FileName.<value> 
        public string FileName { get { return this.strFileName; } set { this.strFileName = value; } }

        /// <summary>Gets or sets the FolderName.</summary>
        /// <value>The FolderName.<value> 
        public string FolderName { get { return this.strFolderName; } set { this.strFolderName = value; } }

        /// <summary>Gets or sets the PublishedName.</summary>
        /// <value>The PublishedName.<value> 
        public string PublishedName { get { return this.strPublishedName; } set { this.strPublishedName = value; } }

        /// <summary>Gets or sets the ProgramType.</summary>
        /// <value>The ProgramType.<value> 
        public string ProgramType { get { return this.strProgramType; } set { this.strProgramType = value; } }

        /// <summary>Gets or sets the Document URL.</summary>
        /// <value>The Document URL.<value> 
        public string DocumentURL { get { return this.strDocumentURL; } set { this.strDocumentURL = value; } }

        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.<value> 
        public string CreatedBy { get { return this.strCreatedBy; } set { this.strCreatedBy = value; } }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.<value> 
        public string CreateDate { get { return this.strCreateDate; } set { this.strCreateDate = value; } }

        /// <summary>Receives a Document datarow and converts it to a DocumentEntity.</summary>
        /// <param name="r">The DocumentEntity DataRow.</param>
        /// <returns>DocumentEntity</returns>
        protected static DocumentEntity GetEntityFromDataRow(DataRow r)
        {
            DocumentEntity t = new DocumentEntity();

            if (r.Table.Columns.Contains("DocumentID"))
            {
                if ((r["DocumentID"] != null) && (r["DocumentID"] != DBNull.Value))
                    t.DocumentID = Convert.ToInt32(r["DocumentID"].ToString());
            }

            t.CreatedBy = r["CreatedBy"].ToString();
            t.CreateDate = r["CreateDate"].ToString();
            t.FileName = r["FileName"].ToString();
            t.FolderName = r["FolderName"].ToString();
            t.ProgramType = r["ProgramType"].ToString();
            t.PublishedName = r["PublishedName"].ToString();

            if (r.Table.Columns.Contains("DocumentURL"))
            {
                    t.DocumentURL = r["DocumentURL"].ToString();
            }

            return t;
        }


        /// <summary>Gets the AppDocument by the Document ID.</summary>
        /// <param name="intDocumentID">the DocumentID</param>
        /// <returns>The newly populated DocumentEntity</returns>
        public static DocumentEntity GetAppDocumentsByDocumentID(int intDocumentID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@documentID", intDocumentID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppDocumentsByDocumentID", parameters);
            //Debug.WriteLine("sp_GetUserTerritoriesByUserId UserTerritory Rows: " + dt.Rows.Count.ToString());

            if (dt.Rows.Count > 0)
            {
                return DocumentEntity.GetEntityFromDataRow(dt.Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>Gets the AppDocument by the Program Typ.</summary>
        /// <param name="strProgramType">the Program Type</param>
        /// <returns>The newly populated DocumentEntity List</returns>
        public static List<DocumentEntity> GetAppDocumentsByProgramType(string strProgramType)
        {
            List<DocumentEntity> list = new List<DocumentEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@programType", strProgramType));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppDocumentsByProgramType", parameters);
            //Debug.WriteLine("sp_GetUserTerritoriesByUserId UserTerritory Rows: " + dt.Rows.Count.ToString());
            
            foreach (DataRow r in dt.Rows)
            {
                DocumentEntity d = DocumentEntity.GetEntityFromDataRow(r);
                list.Add(d);
            }

            return list;
        }

        /// <summary>Gets the AppDocument by the Program Type by FileName.</summary>
        /// <param name="strProgramType">the Program Type</param>
        /// <param name="strFileName">the FileName</param>
        /// <returns>The newly populated DocumentEntity List</returns>
        public static List<DocumentEntity> GetAppDocumentsByProgramTypeByFileName(string strProgramType, string strFileName)
        {
            List<DocumentEntity> list = new List<DocumentEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@programType", strProgramType));
            parameters.Add(new SqlParameter("@fileName", strFileName));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppDocumentsByProgramTypeByFileName", parameters);

            foreach (DataRow r in dt.Rows)
            {
                DocumentEntity d = DocumentEntity.GetEntityFromDataRow(r);
                list.Add(d);
            }

            return list;
        }

        /// <summary>Deletes the AppDocument record by the DocumentI.</summary>
        /// <param name="intDocumentID">the DocumentID</param>
        /// <returns>the count of the records deleted</returns>
        public static int DeleteAppDocument(int intDocumentID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@documentID", intDocumentID));

            int intRecordsDeleted = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteAppDocument", parameters);

            return intRecordsDeleted;
        }

        /// <summary>Insert Document record</summary>
        /// <param name="strFileName">the FileName</param>
        /// <param name="strFolderName">the FolderName</param>
        /// <param name="strPublishedName">the Published Name</param>
        /// <param name="strProgramType">the Program Type</param>
        /// <returns>the count of the records inserted</returns>
        public static int InsertAppDocument(string strFileName, string strFolderName, string strPublishedName, string strProgramType, string strUserName)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@fileName", strFileName));
            parameters.Add(new SqlParameter("@folderName", strFolderName));
            parameters.Add(new SqlParameter("@publishedName", strPublishedName));
            parameters.Add(new SqlParameter("@programType", strProgramType));
            parameters.Add(new SqlParameter("@createdBy", strUserName));

            int intRecordsInserted = SQLUtility.SqlExecuteNonQueryCount("sp_InsertAppDocument", parameters);

            return intRecordsInserted;
        }

    }//// end class
}