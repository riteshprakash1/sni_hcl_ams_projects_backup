﻿// ------------------------------------------------------------------
// <copyright file="CustomerEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    
    /// <summary>
    /// This is the Customer Entity.
    /// </summary>
    public class CustomerEntity
    {
    
        /// <summary>
        /// This is the CustomerName.
        /// </summary>
        private string strCustomerName;

        /// <summary>
        /// This is the CustomerID.
        /// </summary>
        private string strCustomerID;

        /// <summary>
        /// This is the CustomerAddress.
        /// </summary>
        private string strStreet;

        /// <summary>
        /// This is the Gpo.
        /// </summary>
        private string strGpo;

        /// <summary>This is the GpoId.</summary>
        private string strGpoId;

        /// <summary>This is the Customer Group1 ID.</summary>
        private string strCustGroup1ID;

        /// <summary>This is the Customer Group1.</summary>
        private string strCustGroup1;

        /// <summary>This is the SalesOrgId.</summary>
        private string strSalesOrgId;

        /// <summary>This is the City.</summary>
        private string strCity;

        /// <summary>This is the StateCode.</summary>
        private string strStateCode;

        /// <summary>This is the PostalCode.</summary>
        private string strPostalCode;

        /// <summary>This is the Partner Function.</summary>
        private string strPartnerFunction;

        /// <summary>This is the Partner Function Desc.</summary>
        private string strPartnerFunctionDesc;
            

        /// <summary>Initializes a new instance of the <see cref="CustomerEntity"/> class.</summary>
        public CustomerEntity()
        {
            this.CustomerName = String.Empty;
            this.CustomerID = String.Empty;
            this.Street = String.Empty;
            this.City = String.Empty;
            this.StateCode = String.Empty;
            this.PostalCode = String.Empty;
            this.GpoName = String.Empty;
            this.GpoID = String.Empty;
            this.PartnerFunction = String.Empty;
            this.PartnerFunctionDesc = String.Empty;
            this.SalesOrgId = String.Empty;
            this.CustGroup1 = String.Empty;
            this.CustGroup1ID = String.Empty;
        }

        /// <summary>Gets or sets the name of the customer.</summary>
        /// <value>The name of the customer.</value>
        public string CustomerName
        {
            get { return this.strCustomerName; }

            set { this.strCustomerName = value; }
        }

        /// <summary>Gets or sets the Customer ID.</summary>
        /// <value>The Customer ID.</value>
        public string CustomerID
        {
            get {return this.strCustomerID;}

            set { this.strCustomerID = value; }
        }

        /// <summary>Gets or sets the Street.</summary>
        /// <value>The Street.</value>
        public string Street
        {
            get { return this.strStreet; }

            set { this.strStreet = value; }
        }

        /// <summary>Gets or sets the Gpo Name.</summary>
        /// <value>The Gpo Name.</value>
        public string GpoName
        {
            get { return this.strGpo; }

            set { this.strGpo = value; }
        }

        /// <summary>Gets or sets the Gpo ID.</summary>
        /// <value>The Gpo ID.</value>
        public string GpoID
        {
            get { return this.strGpoId; }

            set { this.strGpoId = value; }
        }

        /// <summary>Gets or sets the Customer Group1 ID.</summary>
        /// <value>The Customer Group1 ID.</value>
        public string CustGroup1ID
        {
            get { return this.strCustGroup1ID; }

            set { this.strCustGroup1ID = value; }
        }

        /// <summary>Gets or sets the Customer Group1.</summary>
        /// <value>The Customer Group1.</value>
        public string CustGroup1
        {
            get { return this.strCustGroup1; }

            set { this.strCustGroup1 = value; }
        }


        /// <summary>Gets or sets the SalesOrgId.</summary>
        /// <value>The SalesOrgId.</value>
        public string SalesOrgId
        {
            get { return this.strSalesOrgId; }

            set { this.strSalesOrgId = value; }
        }

        /// <summary>Gets or sets the City.</summary>
        /// <value>The City.</value>
        public string City
        {
            get { return this.strCity; }

            set { this.strCity = value; }
        }

        /// <summary>Gets or sets the StateCode.</summary>
        /// <value>The StateCode.</value>
        public string StateCode
        {
            get { return this.strStateCode; }

            set { this.strStateCode = value; }
        }

        /// <summary>Gets or sets the PostalCode.</summary>
        /// <value>The PostalCode.</value>
        public string PostalCode
        {
            get { return this.strPostalCode; }

            set { this.strPostalCode = value; }
        }

        /// <summary>Gets or sets the Partner Function Desc.</summary>
        /// <value>The Partner Function Desc.</value>
        public string PartnerFunctionDesc
        {
            get { return this.strPartnerFunctionDesc; }

            set { this.strPartnerFunctionDesc = value; }
        }

        /// <summary>Gets or sets the Partner Function.</summary>
        /// <value>The Partner Function.</value>
        public string PartnerFunction
        {
            get { return this.strPartnerFunction; }

            set { this.strPartnerFunction = value; }
        }


        /// <summary>Sorts the Customers table.</summary>
        /// <param name="dtUsers">The Customers datatable.</param>
        /// <param name="sortBy">The sort string</param>
        /// <returns>the sorted datatable</returns>
        public static DataTable SortCustomerTable(DataTable dtCustomers, string sortBy)
        {
            DataTable dtSorted = dtCustomers.Clone();
            DataRow[] rows = dtCustomers.Select(null, sortBy);
            foreach (DataRow r in rows)
            {
                dtSorted.ImportRow(r);
            }

            return dtSorted;
        }

        /// <summary>Gets the Customer entity table.</summary>
        /// <returns>The Customer DataTable</returns>
        public static DataTable GetCustomerEntityTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Customer_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Customer_ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Street", typeof(string)));
            dt.Columns.Add(new DataColumn("City", typeof(string)));
            dt.Columns.Add(new DataColumn("State_Province_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Postal_Code", typeof(string)));
            dt.Columns.Add(new DataColumn("GpoName", typeof(string)));
            dt.Columns.Add(new DataColumn("GpoID", typeof(string)));
            dt.Columns.Add(new DataColumn("SalesOrgID", typeof(string)));
            dt.Columns.Add(new DataColumn("Partner_Function_Desc", typeof(string)));
            dt.Columns.Add(new DataColumn("Partner_Function", typeof(string)));
            dt.Columns.Add(new DataColumn("CustGroup1", typeof(string)));
            dt.Columns.Add(new DataColumn("CustGroup1ID", typeof(string)));

            return dt;
        }

        /// <summary>Sets the customer data row.</summary>
        /// <param name="user">The Customer object.</param>
        /// <param name="r">The DataRow from the Customer table.</param>
        /// <returns>THe newly populated DataRow</returns>
        public static DataRow SetUserDataRow(CustomerEntity customer, DataRow r)
        {
            r["Customer_Name"] = customer.CustomerName;
            r["Customer_ID"] = customer.CustomerID;

            return r;
        }

        /// <summary>Gets the Sold To Customer Entity Collection for the CustomerID.</summary>
        /// <param name="strCustomerID">The Customer ID</param>
        /// <returns>The newly populated Collection<CustomerEntity></returns>
        public static Collection<CustomerEntity> GetSoldToByCustomerID(string strCustomerID)
        {
            string sQuery = string.Empty;
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@QueryName", "SoldToCustomerByCustomerID"));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetQueryByQueryName", parameters);
            if(dt.Rows.Count > 0)
            sQuery = dt.Rows[0]["Query"].ToString() + strCustomerID.PadLeft(10, '0') + "' ";

            Collection<CustomerEntity> myCustomers = CustomerEntity.GetCustomerCollectionByQuery(sQuery);

            Debug.WriteLine("Customer Query: " + sQuery);
            Debug.WriteLine("Customer Rows: " + myCustomers.Count.ToString());

            return myCustomers;
        }

        /// <summary>Gets the Ship To Customer Entity Collection for the CustomerID.</summary>
        /// <param name="strCustomerID">The Customer ID</param>
        /// <returns>The newly populated Collection<CustomerEntity></returns>
        public static Collection<CustomerEntity> GetShipToByCustomerID(string strCustomerID)
        {
            string sQuery = string.Empty;
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@QueryName", "ShipToCustomerByCustomerID"));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetQueryByQueryName", parameters);
            if (dt.Rows.Count > 0)
            {
                sQuery = dt.Rows[0]["Query"].ToString();
                sQuery = sQuery.Replace("xxCUSTOMERIDxx", strCustomerID.PadLeft(10, '0'));
            }
            Debug.WriteLine("ShipToCustomerByCustomerID Query: " + sQuery);

            Collection<CustomerEntity> myCustomers = CustomerEntity.GetCustomerCollectionByQuery(sQuery);

            return myCustomers;
        }
        
        /// <summary>Gets the Customer Entity Collection for the Query.</summary>
        /// <param name="sQuery">The Query to use</param>
        /// <returns>The newly populated Collection<CustomerEntity></returns>
        protected static Collection<CustomerEntity> GetCustomerCollectionByQuery(string sQuery)
        {
            Collection<CustomerEntity> myCustomers = new Collection<CustomerEntity>();

//            DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery, SQLUtility.OpenGdwSqlConnection());
            DataTable dt = SQLUtility.OracleExecuteDynamicQuery(sQuery);

            foreach (DataRow row in dt.Rows)
            {
                myCustomers.Add(CustomerEntity.GetCustomerEntityFromDataRow(row));
            }

            return myCustomers;
        }

        /// <summary>Receives a Customer datarow and converts it to a CustomerEntity.</summary>
        /// <param name="r">The Customer DataRow.</param>
        /// <returns>CustomerEntity</returns>
        protected static CustomerEntity GetCustomerEntityFromDataRow(DataRow r)
        {
            CustomerEntity cust = new CustomerEntity();

            cust.Street = r["Street"].ToString();
            cust.City = r["City"].ToString();
            cust.CustomerID = r["Customer_ID"].ToString();
            cust.CustomerName = r["Customer_Name"].ToString();
            cust.StateCode = r["State_Province_Code"].ToString();
            cust.PostalCode = r["Postal_Code"].ToString();
            cust.GpoName = r["Gpo"].ToString();
            cust.GpoID = r["Gpo_Id"].ToString();
            cust.PartnerFunction = r["Partner_Function"].ToString();
            cust.PartnerFunctionDesc = r["Partner_Function_Desc"].ToString();
            cust.SalesOrgId = r["Sales_Org_Id"].ToString();
            cust.CustGroup1ID = r["Cust_Group1_Id"].ToString();
            cust.CustGroup1 = r["Cust_Group1"].ToString();

            return cust;
        }

        /// <summary>Inserts the Collection of CustomerEntities into the Additional Customers table into the database</summary>
        /// <param name="soldToCustomers">Collection<CustomerEntity></param>
        /// <param name="intPlacedCapitalRequestID">The Placed Capital Request ID</param>
        public static void InsertAdditionalSoldToCustomers(Collection<CustomerEntity> soldToCustomers, int intPlacedCapitalRequestID)
        {
            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            try
            {
                foreach (CustomerEntity cust in soldToCustomers)
                {
                    Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

                    myParamters.Add(new SqlParameter("@PlacedCapitalRequestID", intPlacedCapitalRequestID));
                    myParamters.Add(new SqlParameter("@CustomerID", cust.CustomerID));
                    myParamters.Add(new SqlParameter("@CustomerName", cust.CustomerName));
                    myParamters.Add(new SqlParameter("@Street", cust.Street));
                    myParamters.Add(new SqlParameter("@City", cust.City));
                    myParamters.Add(new SqlParameter("@StateCode", cust.StateCode));
                    myParamters.Add(new SqlParameter("@PostalCode", cust.PostalCode));
                    myParamters.Add(new SqlParameter("@GpoDesc", cust.GpoName));

                    SqlParameterCollection updatedParamteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertAdditionalCustomers", myParamters, conn, trans);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("InsertPlacedCapitalRequest Error: " + ex.Message);
                trans.Rollback();
                SQLUtility.CloseSqlConnection(conn);
                throw (ex);

            }

            trans.Commit();

            SQLUtility.CloseSqlConnection(conn);
        }

        /// <summary>Gets the Customer Entity Collection for Additional Customer by PlacedCapitalRequestID.</summary>
        /// <param name="intPlacedCapitalRequestID">The Placed Capital Request ID</param>
        /// <returns>The newly populated Collection<CustomerEntity></returns>
        public static Collection<CustomerEntity> GetAdditionalCustomerCollection(int intPlacedCapitalRequestID)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@PlacedCapitalRequestID", intPlacedCapitalRequestID));
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAdditionalCustomersByPlacedCapitalRequestID", myParamters);

            Collection<CustomerEntity> myCustomers = new Collection<CustomerEntity>();

            foreach (DataRow r in dt.Rows)
            {
                CustomerEntity cust = new CustomerEntity();

                cust.CustomerID = r["CustomerID"].ToString();
                cust.CustomerName = r["CustomerName"].ToString();
                cust.Street = r["Street"].ToString();
                cust.City = r["City"].ToString();
                cust.StateCode = r["StateCode"].ToString();
                cust.PostalCode = r["PostalCode"].ToString();
                cust.GpoName = r["GpoDesc"].ToString();

                myCustomers.Add(cust);
            }

            return myCustomers;
        }

    } //// end class
}