﻿// ------------------------------------------------------------------
// <copyright file="UserTerritoryEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the User Territory Entity. </summary>    
    public class UserTerritoryEntity
    {
        /// <summary>This is the UserTerritoryID.</summary>
        private int intUserTerritoryID;

        /// <summary>This is the UserName.</summary>
        private string strUserName;

        /// <summary>This is the Territory.</summary>
        private string strTerritory;

        /// <summary>This is the Created By.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreatedDate.</summary>
        private string strCreatedDate;

        /// <summary>Initializes a new instance of the UserTerritoryEntity class.</summary>
        public UserTerritoryEntity()
        {
            this.Territory = String.Empty;
            this.CreatedBy = String.Empty;
            this.CreatedDate = String.Empty;
            this.UserName = String.Empty;
        }

        /// <summary>Gets or sets the UserTerritoryID.</summary>
        /// <value>The UserTerritoryID.</value>
        public int UserTerritoryID
        {
            get { return this.intUserTerritoryID; }
            set { this.intUserTerritoryID = value; }
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get { return this.strUserName; }
            set { this.strUserName = value; }
        }

        /// <summary>Gets or sets the Territory.</summary>
        /// <value>The Territory.</value>
        public string Territory
        {
            get { return this.strTerritory; }
            set { this.strTerritory = value; }
        }

        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.</value>
        public string CreatedBy
        {
            get { return this.strCreatedBy; }
            set { this.strCreatedBy = value; }
        }

        /// <summary>Gets or sets the CreatedDate.</summary>
        /// <value>The CreatedDate.</value>
        public string CreatedDate
        {
            get { return this.strCreatedDate; }
            set { this.strCreatedDate = value; }
        }

        /// <summary>Receives a UserTerritory datarow and converts it to a UserTerritoryEntity.</summary>
        /// <param name="r">The UserTerritory DataRow.</param>
        /// <returns>UserTerritoryEntity</returns>
        protected static UserTerritoryEntity GetEntityFromDataRow(DataRow r)
        {
            UserTerritoryEntity t = new UserTerritoryEntity();

            if ((r["UserName"] != null) && (r["UserName"] != DBNull.Value))
                t.UserName = r["UserName"].ToString();

            if ((r["UserTerritoryID"] != null) && (r["UserTerritoryID"] != DBNull.Value))
                t.UserTerritoryID = Convert.ToInt32(r["UserTerritoryID"].ToString());

            t.Territory = r["Territory"].ToString();
            t.CreatedDate = r["CreatedDate"].ToString();
            t.CreatedBy = r["CreatedBy"].ToString();

            return t;
        }


        /// <summary>Gets all the UserTerritory List.</summary>
        /// <param name="strUserName">the UserName</param>
        /// <returns>The newly populated UserTerritoryEntity List</returns>
        public static List<UserTerritoryEntity> GetUserTerritoriesByUserName(string strUserName)
        {
            List<UserTerritoryEntity> territories = new List<UserTerritoryEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userName", strUserName));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUserTerritoriesByUserName", parameters);
            //Debug.WriteLine("sp_GetUserTerritoriesByUserId UserTerritory Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserTerritoryEntity t = UserTerritoryEntity.GetEntityFromDataRow(r);
                territories.Add(t);
            }

            return territories;
        }

        /// <summary>Gets user territories by territory.</summary>
        /// <param name="strTerritory">the Territory</param>
        /// <returns>The newly populated UserTerritoryEntity List</returns>
        public static List<UserTerritoryEntity> GetUsersAssignedToTerritory(string strTerritory)
        {
            List<UserTerritoryEntity> territories = new List<UserTerritoryEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@territory", strTerritory));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUsersAssignedToTerritory", parameters);

            foreach (DataRow r in dt.Rows)
            {
                UserTerritoryEntity t = UserTerritoryEntity.GetEntityFromDataRow(r);
                territories.Add(t);
            }

            return territories;
        }


        /// <summary>Deletes the territory records by the userTerritoryId.</summary>
        /// <param name="userTerritoryId">the userTerritoryId</param>
        /// <param name="strModifiedBy">the Modified By User</param>
        /// <returns>the count of the records deleted</returns>
        public static int DeleteTerritoryByUserTerritoryId(int userTerritoryId, string strModifiedBy)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userTerritoryId", userTerritoryId));

            int intRecordsDeleted = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteUserTerritoryByUserTerritoryId", parameters);

            return intRecordsDeleted;
        }

        /// <summary>Insert territory record</summary>
        /// <param name="strUserName">the UserName</param>
        /// <param name="strTerrirory">the Terrirory</param>
        /// <param name="strModifiedBy">the Modified By User</param>
        /// <returns>the count of the records inserted</returns>
        public static int InsertUserTerritory(string strUserName, string strTerrirory, string strModifiedBy)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userName", strUserName));
            parameters.Add(new SqlParameter("@territory", strTerrirory));
            parameters.Add(new SqlParameter("@modifiedBY", strModifiedBy));

            int intRecordsInserted = SQLUtility.SqlExecuteNonQueryCount("sp_InsertUserTerritory", parameters);

            return intRecordsInserted;
        }

        ///// <summary>Delete all territories by userID then inserts the list of territories</summary>
        ///// <param name="territories">the List of UserTerritoryEntity objects</param>
        ///// <param name="intUserID">the UserID</param>
        ///// <returns>true/false as to whether the transaction was successful</returns>
        //public static bool UpdateUserTerritories(List<UserTerritoryEntity> territories, int intUserID)
        //{
        //    SqlConnection conn = SQLUtility.OpenSqlConnection();
        //    SqlTransaction trans = conn.BeginTransaction();

        //    bool updateGood = false;

        //    try
        //    {
        //        Collection<SqlParameter> myDeleteParameters = new Collection<SqlParameter>();
        //        myDeleteParameters.Add(new SqlParameter("@userId", intUserID));
        //        SqlParameterCollection deleteParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_DeleteUserTerritoriesByUserId", myDeleteParameters, conn, trans);

        //        foreach (UserTerritoryEntity t in territories)
        //        {
        //            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

        //            myParameters.Add(new SqlParameter("@userID", intUserID));
        //            myParameters.Add(new SqlParameter("@territory", t.Territory));

        //            SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertUserTerritory", myParameters, conn, trans);
        //        }

        //        trans.Commit();
        //        updateGood = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("Rollback UpdateUserTerritories: " + ex.Message);
        //        trans.Rollback();
        //    }

        //    SQLUtility.CloseSqlConnection(conn);
        //    Debug.WriteLine("updateGood: " + updateGood.ToString());
        //    return updateGood;
        //}


    }//// end class
}