﻿// ------------------------------------------------------------------
// <copyright file="AppUsersEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Application User's Entity. </summary>    
    public class AppUsersEntity
    {
        /// <summary>This is the UserID.</summary>
        private int intUserID;

        /// <summary>This is the RoleID.</summary>
        private int intRoleID;

        /// <summary>This is the RoleTypeID.</summary>
        private int intRoleTypeID;
        
        /// <summary>This is the ProgramID.</summary>
        private int intProgramID;

        /// <summary>This is the Username.</summary>
        private string strUserName;

        /// <summary>This is the Lastname.</summary>
        private string strLastName;

        /// <summary>This is the Firstname.</summary>
        private string strFirstName;

        /// <summary>This is the Role Name.</summary>
        private string strRoleName;

        /// <summary>This is the Role Type Name.</summary>
        private string strRoleTypeName;

        /// <summary>This is the Territories.</summary>
        private string strTerritories;

        /// <summary>This is the ProgramName.</summary>
        private string strProgramName;

        /// <summary>This is the SendEmail.</summary>
        private string strSendEmail;

        /// <summary>This is the Created By.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>This is the Modified By.</summary>
        private string strModifiedBy;

        /// <summary>This is the ModifiedDate.</summary>
        private string strModifiedDate;
        
        /// <summary>Initializes a new instance of the AppUsersEntity class.</summary>
        public AppUsersEntity()
        {
            this.UserName = String.Empty;
            this.LastName = String.Empty;
            this.FirstName = String.Empty;
            this.RoleName = String.Empty;
            this.RoleTypeName = String.Empty;
            this.ProgramName = String.Empty;
            this.Territories = String.Empty;
            this.SendEmail = String.Empty;

            this.CreatedBy = String.Empty;
            this.CreateDate = String.Empty;
            this.ModifiedBy = String.Empty;
            this.ModifiedDate = String.Empty;
        }

        /// <summary>Gets or sets the User ID.</summary>
        /// <value>The User ID.</value>
        public int UserID
        {
            get{ return this.intUserID;}
            set{this.intUserID = value;}
        }

        /// <summary>Gets or sets the ProgramID.</summary>
        /// <value>The ProgramID.</value>
        public int ProgramID
        {
            get { return this.intProgramID; }
            set { this.intProgramID = value; }
        }

        /// <summary>Gets or sets the RoleID.</summary>
        /// <value>The RoleID.</value>
        public int RoleID
        {
            get { return this.intRoleID; }
            set { this.intRoleID = value; }
        }

        /// <summary>Gets or sets the RoleTypeID.</summary>
        /// <value>The RoleTypeID.</value>
        public int RoleTypeID
        {
            get { return this.intRoleTypeID; }
            set { this.intRoleTypeID = value; }
        }
        

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get{ return this.strUserName;}
            set{this.strUserName = value;}
        }

        /// <summary>Gets or sets the Lastname.</summary>
        /// <value>The Lastname.</value>
        public string LastName
        {
            get{ return this.strLastName;}
            set{this.strLastName = value;}
        }

        /// <summary>Gets or sets the Firstname.</summary>
        /// <value>The First name.</value>
        public string FirstName
        {
            get{ return this.strFirstName;}
            set{this.strFirstName = value;}
        }

        /// <summary>Gets or sets the Role Name.</summary>
        /// <value>The Role Name.</value>
        public string RoleName
        {
            get { return this.strRoleName; }
            set { this.strRoleName = value; }
        }

        /// <summary>Gets or sets the Role Type Name.</summary>
        /// <value>The Role Type Name.</value>
        public string RoleTypeName
        {
            get { return this.strRoleTypeName; }
            set { this.strRoleTypeName = value; }
        }

        /// <summary>Gets or sets the Territories.</summary>
        /// <value>The Territories.</value>
        public string Territories
        {
            get { return this.strTerritories; }
            set { this.strTerritories = value; }
        }

        /// <summary>Gets or sets the ProgramName.</summary>
        /// <value>The Program Name.</value>
        public string ProgramName
        {
            get { return this.strProgramName; }
            set { this.strProgramName = value; }
        }

        /// <summary>Gets or sets the SendEmail.</summary>
        /// <value>The SendEmail.</value>
        public string SendEmail
        {
            get { return this.strSendEmail; }
            set { this.strSendEmail = value; }
        }
        
        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.</value>
        public string CreatedBy
        {
            get { return this.strCreatedBy; }
            set { this.strCreatedBy = value; }
        }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.</value>
        public string CreateDate
        {
            get { return this.strCreateDate; }
            set { this.strCreateDate = value; }
        }

        /// <summary>Gets or sets the ModifiedBy.</summary>
        /// <value>The ModifiedBy.</value>
        public string ModifiedBy
        {
            get { return this.strModifiedBy; }
            set { this.strModifiedBy = value; }
        }

        /// <summary>Gets or sets the ModifiedDate.</summary>
        /// <value>The ModifiedDate.</value>
        public string ModifiedDate
        {
            get { return this.strModifiedDate; }
            set { this.strModifiedDate = value; }
        }


        /// <summary>Determines whether the user is a superAdmin</summary>
        /// <param name="users">The (List AppUsersEntity</param>
        /// <returns>true/ false</returns>
        public static bool IsSuperAdmin(List<AppUsersEntity> users)
        {
            int superAdminIdex = users.FindIndex(item => item.RoleID == 1);

            bool blnIsSuperAdmin = Convert.ToBoolean(superAdminIdex >= 0);
            //Debug.WriteLine("superAdminIdex: " + superAdminIdex.ToString() + ";blnIsSuperAdmin: " + blnIsSuperAdmin.ToString());

            return blnIsSuperAdmin;
        }

        /// <summary>Determines whether the user is a program Admin</summary>
        /// <param name="users">The List AppUsersEntity</param>
        /// <param name="intProgramID">The ProgramID</param>
        /// <returns>true/ false</returns>
        public static bool IsProgramAdmin(List<AppUsersEntity> users, int intProgramID)
        {
            if (AppUsersEntity.IsSuperAdmin(users))
                return true;
            else
            {
                int ProgramAdminIndex = users.FindIndex(item => item.RoleTypeID == 1 && item.ProgramID == intProgramID);

                bool blnIsProgramAdmin = Convert.ToBoolean(ProgramAdminIndex >= 0);
                Debug.WriteLine("ProgramAdminIdex: " + ProgramAdminIndex.ToString() + ";blnIsProgramAdmin: " + blnIsProgramAdmin.ToString());

                return blnIsProgramAdmin;
            }
        }

        /// <summary>Determines whether the user is a program Admin</summary>
        /// <param name="users">The (List AppUsersEntity</param>
        /// <returns>true/ false</returns>
        public static bool IsAdmin(List<AppUsersEntity> users)
        {
            int AdminIndex = users.FindIndex(item => item.RoleTypeID == 1);

            bool blnIsAdmin = Convert.ToBoolean(AdminIndex >= 0);
            Debug.WriteLine("AdminIdex: " + AdminIndex.ToString() + ";blnIsAdmin: " + blnIsAdmin.ToString());

            return blnIsAdmin;
        }

        /// <summary>Gets the AppUsersEntity List by the UserName.</summary>
        /// <param name="strUserName">The Username</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAppUserByUserName(string strUserName)
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@username", strUserName));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUserByUserName", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Gets the AppUsersEntity List by the ProgramID.</summary>
        /// <param name="intProgramID">The programID</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAppUserByProgramID(int intProgramID)
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@programID", intProgramID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUserByProgramID", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Gets the Role ID by Username by ProgramID. If no roleID is found, returns 0</summary>
        /// <param name="intProgramID">The programID</param>
        /// <param name="strUsername">The strUsername</param>
        /// <returns>The role ID</returns>
        public static int GetAppUserRoleIDByProgramIDByUsername(int intProgramID, string strUsername)
        {
            List<AppUsersEntity> users = GetAppUserByUserName(strUsername);

            if (AppUsersEntity.IsSuperAdmin(users))
                return 1;
            else
            {
                int roleIndex = users.FindIndex(item => item.ProgramID == intProgramID);

                if (roleIndex >= 0)
                {
                    AppUsersEntity myUser = users[roleIndex];
                    return myUser.RoleID;
                }
                else
                    return 0;
            }
        }

        /// <summary>Gets the AppUsersEntity List by the ProgramID by the username.</summary>
        /// <param name="intProgramID">The programID</param>
        /// <param name="strUsername">The strUsername</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAppUserByProgramIDByUsername(int intProgramID, string strUsername)
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@programID", intProgramID));
            parameters.Add(new SqlParameter("@username", strUsername));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUserByProgramIDByUsername", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Gets the AppUsersEntity List by the ProgramID by the username.</summary>
        /// <param name="intProgramID">The programID</param>
        /// <param name="strTerritory">The Territory</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static AppUsersEntity GetAppUserByTerritoryByProgramID(int intProgramID, string strTerritory)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@programID", intProgramID));
            parameters.Add(new SqlParameter("@territory", strTerritory));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUserByTerritoryByProgramID", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            AppUsersEntity myUser = null;

            if(dt.Rows.Count > 0)
            {
                myUser = AppUsersEntity.GetEntityFromDataRow(dt.Rows[0]);
            }

            return myUser;
        }

        /// <summary>Gets all the AppUsersEntity Collection.</summary>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAppUsers()
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUsers", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Receives a AppUsers datarow and converts it to a AppUsersEntity.</summary>
        /// <param name="r">The App User DataRow.</param>
        /// <returns>AppUsersEntity</returns>
        public static AppUsersEntity GetEntityFromDataRow(DataRow r)
        {
            AppUsersEntity appUser = new AppUsersEntity();

            if ((r["UserID"] != null) && (r["UserID"] != DBNull.Value))
                appUser.UserID = Convert.ToInt32(r["UserID"].ToString());

            if ((r["RoleID"] != null) && (r["RoleID"] != DBNull.Value))
                appUser.RoleID = Convert.ToInt32(r["RoleID"].ToString());

            if (r.Table.Columns.Contains("ProgramID"))
            {
                if ((r["ProgramID"] != null) && (r["ProgramID"] != DBNull.Value))
                    appUser.ProgramID = Convert.ToInt32(r["ProgramID"].ToString());
            }

            if ((r["RoleTypeID"] != null) && (r["RoleTypeID"] != DBNull.Value))
                appUser.RoleTypeID = Convert.ToInt32(r["RoleTypeID"].ToString());

            if (r.Table.Columns.Contains("Territories"))
                appUser.Territories = r["Territories"].ToString();

            if (r.Table.Columns.Contains("SendEmail"))
                appUser.SendEmail = r["SendEmail"].ToString();
 
            appUser.CreateDate = r["CreateDate"].ToString();
            appUser.CreatedBy = r["CreatedBy"].ToString();
            appUser.FirstName = r["FirstName"].ToString();
            appUser.LastName = r["LastName"].ToString();
            appUser.ModifiedBy = r["ModifiedBy"].ToString();
            appUser.ModifiedDate = r["ModifiedDate"].ToString();

            appUser.ProgramName = r["ProgramName"].ToString();
            appUser.RoleName = r["RoleName"].ToString();
            appUser.RoleTypeName = r["RoleTypeName"].ToString();
            appUser.UserName = r["UserName"].ToString();

            return appUser;
        }

        /// <summary>Inserts The App User into the database</summary>
        /// <param name="u">AppUsersEntity object</param>
        /// <returns>The AppUsers Entity</returns>
        public static AppUsersEntity InsertAppUser(AppUsersEntity u)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            SqlParameter SQLID = new SqlParameter("@userID", u.UserID);
            SQLID.Direction = ParameterDirection.Output;
            myParameters.Add(SQLID);

            myParameters.Add(new SqlParameter("@roleID", u.RoleID));
            myParameters.Add(new SqlParameter("@ModifiedBy", u.ModifiedBy));
            myParameters.Add(new SqlParameter("@firstName", u.FirstName));
            myParameters.Add(new SqlParameter("@lastName", u.LastName));
            myParameters.Add(new SqlParameter("@username", u.UserName));
            myParameters.Add(new SqlParameter("@sendEmail", u.SendEmail));

            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertAppUser", myParameters, conn, trans);

            if (insertParameteres["@userID"] != null)
            {
                u.UserID = Convert.ToInt32(insertParameteres["@userID"].Value.ToString());
                Debug.WriteLine("Inserted UserID: " + u.UserID.ToString());
            }

            trans.Commit();

            SQLUtility.CloseSqlConnection(conn);
            return u;
        }

        /// <summary>Updates The App User record in the database</summary>
        /// <param name="u">AppUsersEntity object</param>
        /// <returns>records updated count</returns>
        public static int UpdateAppUser(AppUsersEntity u)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            myParameters.Add(new SqlParameter("@ModifiedBy", u.ModifiedBy));
            myParameters.Add(new SqlParameter("@username", u.UserName));
            myParameters.Add(new SqlParameter("@sendEmail", u.SendEmail));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateAppUser", myParameters);
            Debug.WriteLine("sp_UpdateAppUser: " + intRecordsUpdated.ToString());

            return intRecordsUpdated;
        }

        /// <summary>Delete the App User record in the database</summary>
        /// <param name="intUserID">the UserID</param>
        /// <returns>records updated count</returns>
        public static int DeleteAppUser(int intUserID)
        {
            Debug.WriteLine("DeleteAppUser: ");
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@userID", intUserID));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteAppUserByUserID", myParameters);

            return intRecordsUpdated;
        }

    }//// end class
}