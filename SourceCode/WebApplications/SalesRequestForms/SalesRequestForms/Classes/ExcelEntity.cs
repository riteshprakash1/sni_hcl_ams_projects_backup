﻿// ------------------------------------------------------------------
// <copyright file="ExcelEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary>This is the Excel Entity.</summary>
    public class ExcelEntity
    {
        /// <summary> This is the Row</summary> 
        private int intRow;

        /// <summary> This is the Column</summary> 
        private string strColumn;

        /// <summary>Initializes a new instance of the ExcelEntity class.</summary>
        public ExcelEntity()
        {
            this.Column = string.Empty;
        }

        /// <summary>Initializes a new instance of the ExcelEntity class.</summary>
        public ExcelEntity(int intMyRow, string strMyColumn)
        {
            this.Column = strMyColumn;
            this.Row = intMyRow;
        }


        /// <summary>Gets or sets the Row.</summary>
        /// <value>The Row.<value> 
        public int Row { get { return this.intRow; } set { this.intRow = value; } }

        /// <summary>Gets or sets the Column.</summary>
        /// <value>The Column.<value> 
        public string Column { get { return this.strColumn; } set { this.strColumn = value; } }

        /// <summary>Loads a list with the Excel Entities</summary>
        /// <returns>The List ExcelEntity</returns>
        public static List<ExcelEntity> LoadExcelEntity()
        {
            List<ExcelEntity> list = new List<ExcelEntity>();

            list.Add(new ExcelEntity(1, "A"));
            list.Add(new ExcelEntity(2, "B"));
            list.Add(new ExcelEntity(3, "C"));
            list.Add(new ExcelEntity(4, "D"));
            list.Add(new ExcelEntity(5, "E"));
            list.Add(new ExcelEntity(6, "F"));
            list.Add(new ExcelEntity(7, "G"));
            list.Add(new ExcelEntity(8, "H"));
            list.Add(new ExcelEntity(9, "I"));
            list.Add(new ExcelEntity(10, "J"));
            list.Add(new ExcelEntity(11, "K"));
            list.Add(new ExcelEntity(12, "L"));
            list.Add(new ExcelEntity(13, "M"));
            list.Add(new ExcelEntity(14, "N"));
            list.Add(new ExcelEntity(15, "O"));
            list.Add(new ExcelEntity(16, "P"));
            list.Add(new ExcelEntity(17, "Q"));
            list.Add(new ExcelEntity(18, "R"));
            list.Add(new ExcelEntity(19, "S"));
            list.Add(new ExcelEntity(20, "T"));
            list.Add(new ExcelEntity(21, "U"));
            list.Add(new ExcelEntity(22, "V"));
            list.Add(new ExcelEntity(23, "W"));
            list.Add(new ExcelEntity(24, "X"));
            list.Add(new ExcelEntity(25, "Y"));
            list.Add(new ExcelEntity(26, "Z"));
            list.Add(new ExcelEntity(27, "AA"));
            list.Add(new ExcelEntity(28, "AB"));
            list.Add(new ExcelEntity(29, "AC"));
            list.Add(new ExcelEntity(30, "AD"));
            list.Add(new ExcelEntity(31, "AE"));
            list.Add(new ExcelEntity(32, "AF"));
            list.Add(new ExcelEntity(33, "AG"));
            list.Add(new ExcelEntity(34, "AH"));
            list.Add(new ExcelEntity(35, "AI"));
            list.Add(new ExcelEntity(36, "AJ"));
            list.Add(new ExcelEntity(37, "AK"));
            list.Add(new ExcelEntity(38, "AL"));
            list.Add(new ExcelEntity(39, "AM"));
            list.Add(new ExcelEntity(40, "AN"));
            list.Add(new ExcelEntity(41, "AO"));
            list.Add(new ExcelEntity(42, "AP"));
            list.Add(new ExcelEntity(43, "AQ"));
            list.Add(new ExcelEntity(44, "AR"));
            list.Add(new ExcelEntity(45, "AS"));
            list.Add(new ExcelEntity(46, "AT"));
            list.Add(new ExcelEntity(47, "AU"));
            list.Add(new ExcelEntity(48, "AV"));
            list.Add(new ExcelEntity(49, "AW"));
            list.Add(new ExcelEntity(50, "AX"));
            list.Add(new ExcelEntity(51, "AY"));
            list.Add(new ExcelEntity(52, "AZ"));
            list.Add(new ExcelEntity(53, "BA"));
            list.Add(new ExcelEntity(54, "BB"));
            list.Add(new ExcelEntity(55, "BC"));
            list.Add(new ExcelEntity(56, "BD"));
            list.Add(new ExcelEntity(57, "BE"));
            list.Add(new ExcelEntity(58, "BF"));
            list.Add(new ExcelEntity(59, "BG"));
            list.Add(new ExcelEntity(60, "BH"));
            list.Add(new ExcelEntity(61, "BI"));
            list.Add(new ExcelEntity(62, "BJ"));
            list.Add(new ExcelEntity(63, "BK"));
            list.Add(new ExcelEntity(64, "BL"));
            list.Add(new ExcelEntity(65, "BM"));
            list.Add(new ExcelEntity(66, "BN"));
            list.Add(new ExcelEntity(67, "BO"));
            list.Add(new ExcelEntity(68, "BP"));
            list.Add(new ExcelEntity(69, "BQ"));
            list.Add(new ExcelEntity(70, "BR"));
            list.Add(new ExcelEntity(71, "BS"));
            list.Add(new ExcelEntity(72, "BT"));
            list.Add(new ExcelEntity(73, "BU"));
            list.Add(new ExcelEntity(74, "BV"));
            list.Add(new ExcelEntity(75, "BW"));
            list.Add(new ExcelEntity(76, "BX"));
            list.Add(new ExcelEntity(77, "BY"));
            list.Add(new ExcelEntity(78, "BZ"));

            return list;
        }
        
        /// <summary>Get the Entity based on the column number</summary>
        /// <param name="intColumnNumber">The Column Number</param>
        /// <param name="list">The List ExcelEntity </param>
        /// <returns>The List ExcelEntity</returns>
        public static ExcelEntity FindExcelEntity(int intColumnNumber, List<ExcelEntity> list)
        {
            int intIndex = list.FindIndex(item => item.Row == intColumnNumber);

            if (intIndex >= 0)
                return list[intIndex];
            else
                return null;
        }



    }//// end class
}