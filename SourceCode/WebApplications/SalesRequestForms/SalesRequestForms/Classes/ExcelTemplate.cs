﻿using DocumentFormat.OpenXml.Packaging;
using Ap = DocumentFormat.OpenXml.ExtendedProperties;
using Vt = DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using A = DocumentFormat.OpenXml.Drawing;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace SalesRequestForms.Classes
{
    public class ExcelTemplate
    {
        // Creates a SpreadsheetDocument.
        public void CreatePackage(string filePath)
        {
            using(SpreadsheetDocument package = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook))
            {
                CreateParts(package);
            }
        }

        // Adds child parts and generates content of the specified part.
        private void CreateParts(SpreadsheetDocument document)
        {
            ExtendedFilePropertiesPart extendedFilePropertiesPart1 = document.AddNewPart<ExtendedFilePropertiesPart>("rId3");
            GenerateExtendedFilePropertiesPart1Content(extendedFilePropertiesPart1);

            WorkbookPart workbookPart1 = document.AddWorkbookPart();
            GenerateWorkbookPart1Content(workbookPart1);

            ThemePart themePart1 = workbookPart1.AddNewPart<ThemePart>("rId3");
            GenerateThemePart1Content(themePart1);

            WorksheetPart worksheetPart1 = workbookPart1.AddNewPart<WorksheetPart>("rId2");
            GenerateWorksheetPart1Content(worksheetPart1);

            SpreadsheetPrinterSettingsPart spreadsheetPrinterSettingsPart1 = worksheetPart1.AddNewPart<SpreadsheetPrinterSettingsPart>("rId1");
            GenerateSpreadsheetPrinterSettingsPart1Content(spreadsheetPrinterSettingsPart1);

            WorksheetPart worksheetPart2 = workbookPart1.AddNewPart<WorksheetPart>("rId1");
            GenerateWorksheetPart2Content(worksheetPart2);

            SharedStringTablePart sharedStringTablePart1 = workbookPart1.AddNewPart<SharedStringTablePart>("rId5");
            GenerateSharedStringTablePart1Content(sharedStringTablePart1);

            WorkbookStylesPart workbookStylesPart1 = workbookPart1.AddNewPart<WorkbookStylesPart>("rId4");
            GenerateWorkbookStylesPart1Content(workbookStylesPart1);

            SetPackageProperties(document);
        }

        // Generates content of extendedFilePropertiesPart1.
        private void GenerateExtendedFilePropertiesPart1Content(ExtendedFilePropertiesPart extendedFilePropertiesPart1)
        {
            Ap.Properties properties1 = new Ap.Properties();
            properties1.AddNamespaceDeclaration("vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
            Ap.Application application1 = new Ap.Application();
            application1.Text = "Microsoft Excel";
            Ap.DocumentSecurity documentSecurity1 = new Ap.DocumentSecurity();
            documentSecurity1.Text = "0";
            Ap.ScaleCrop scaleCrop1 = new Ap.ScaleCrop();
            scaleCrop1.Text = "false";

            Ap.HeadingPairs headingPairs1 = new Ap.HeadingPairs();

            Vt.VTVector vTVector1 = new Vt.VTVector(){ BaseType = Vt.VectorBaseValues.Variant, Size = (UInt32Value)2U };

            Vt.Variant variant1 = new Vt.Variant();
            Vt.VTLPSTR vTLPSTR1 = new Vt.VTLPSTR();
            vTLPSTR1.Text = "Worksheets";

            variant1.Append(vTLPSTR1);

            Vt.Variant variant2 = new Vt.Variant();
            Vt.VTInt32 vTInt321 = new Vt.VTInt32();
            vTInt321.Text = "2";

            variant2.Append(vTInt321);

            vTVector1.Append(variant1);
            vTVector1.Append(variant2);

            headingPairs1.Append(vTVector1);

            Ap.TitlesOfParts titlesOfParts1 = new Ap.TitlesOfParts();

            Vt.VTVector vTVector2 = new Vt.VTVector(){ BaseType = Vt.VectorBaseValues.Lpstr, Size = (UInt32Value)2U };
            Vt.VTLPSTR vTLPSTR2 = new Vt.VTLPSTR();
            vTLPSTR2.Text = "Pricing Request Header List";
            Vt.VTLPSTR vTLPSTR3 = new Vt.VTLPSTR();
            vTLPSTR3.Text = "Material List";

            vTVector2.Append(vTLPSTR2);
            vTVector2.Append(vTLPSTR3);

            titlesOfParts1.Append(vTVector2);
            Ap.Company company1 = new Ap.Company();
            company1.Text = "Smith and Nephew Inc";
            Ap.LinksUpToDate linksUpToDate1 = new Ap.LinksUpToDate();
            linksUpToDate1.Text = "false";
            Ap.SharedDocument sharedDocument1 = new Ap.SharedDocument();
            sharedDocument1.Text = "false";
            Ap.HyperlinksChanged hyperlinksChanged1 = new Ap.HyperlinksChanged();
            hyperlinksChanged1.Text = "false";
            Ap.ApplicationVersion applicationVersion1 = new Ap.ApplicationVersion();
            applicationVersion1.Text = "14.0300";

            properties1.Append(application1);
            properties1.Append(documentSecurity1);
            properties1.Append(scaleCrop1);
            properties1.Append(headingPairs1);
            properties1.Append(titlesOfParts1);
            properties1.Append(company1);
            properties1.Append(linksUpToDate1);
            properties1.Append(sharedDocument1);
            properties1.Append(hyperlinksChanged1);
            properties1.Append(applicationVersion1);

            extendedFilePropertiesPart1.Properties = properties1;
        }

        // Generates content of workbookPart1.
        private void GenerateWorkbookPart1Content(WorkbookPart workbookPart1)
        {
            Workbook workbook1 = new Workbook();
            workbook1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            FileVersion fileVersion1 = new FileVersion(){ ApplicationName = "xl", LastEdited = "5", LowestEdited = "5", BuildVersion = "9303" };
            WorkbookProperties workbookProperties1 = new WorkbookProperties(){ DefaultThemeVersion = (UInt32Value)124226U };

            BookViews bookViews1 = new BookViews();
            WorkbookView workbookView1 = new WorkbookView(){ XWindow = 120, YWindow = 45, WindowWidth = (UInt32Value)8595U, WindowHeight = (UInt32Value)13860U, ActiveTab = (UInt32Value)1U };

            bookViews1.Append(workbookView1);

            Sheets sheets1 = new Sheets();
            Sheet sheet1 = new Sheet(){ Name = "Pricing Request Header List", SheetId = (UInt32Value)5U, Id = "rId1" };
            Sheet sheet2 = new Sheet(){ Name = "Material List", SheetId = (UInt32Value)4U, Id = "rId2" };

            sheets1.Append(sheet1);
            sheets1.Append(sheet2);
            CalculationProperties calculationProperties1 = new CalculationProperties(){ CalculationId = (UInt32Value)145621U };

            workbook1.Append(fileVersion1);
            workbook1.Append(workbookProperties1);
            workbook1.Append(bookViews1);
            workbook1.Append(sheets1);
            workbook1.Append(calculationProperties1);

            workbookPart1.Workbook = workbook1;
        }

        // Generates content of themePart1.
        private void GenerateThemePart1Content(ThemePart themePart1)
        {
            A.Theme theme1 = new A.Theme(){ Name = "Office Theme" };
            theme1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            A.ThemeElements themeElements1 = new A.ThemeElements();

            A.ColorScheme colorScheme1 = new A.ColorScheme(){ Name = "Office" };

            A.Dark1Color dark1Color1 = new A.Dark1Color();
            A.SystemColor systemColor1 = new A.SystemColor(){ Val = A.SystemColorValues.WindowText, LastColor = "000000" };

            dark1Color1.Append(systemColor1);

            A.Light1Color light1Color1 = new A.Light1Color();
            A.SystemColor systemColor2 = new A.SystemColor(){ Val = A.SystemColorValues.Window, LastColor = "FFFFFF" };

            light1Color1.Append(systemColor2);

            A.Dark2Color dark2Color1 = new A.Dark2Color();
            A.RgbColorModelHex rgbColorModelHex1 = new A.RgbColorModelHex(){ Val = "1F497D" };

            dark2Color1.Append(rgbColorModelHex1);

            A.Light2Color light2Color1 = new A.Light2Color();
            A.RgbColorModelHex rgbColorModelHex2 = new A.RgbColorModelHex(){ Val = "EEECE1" };

            light2Color1.Append(rgbColorModelHex2);

            A.Accent1Color accent1Color1 = new A.Accent1Color();
            A.RgbColorModelHex rgbColorModelHex3 = new A.RgbColorModelHex(){ Val = "4F81BD" };

            accent1Color1.Append(rgbColorModelHex3);

            A.Accent2Color accent2Color1 = new A.Accent2Color();
            A.RgbColorModelHex rgbColorModelHex4 = new A.RgbColorModelHex(){ Val = "C0504D" };

            accent2Color1.Append(rgbColorModelHex4);

            A.Accent3Color accent3Color1 = new A.Accent3Color();
            A.RgbColorModelHex rgbColorModelHex5 = new A.RgbColorModelHex(){ Val = "9BBB59" };

            accent3Color1.Append(rgbColorModelHex5);

            A.Accent4Color accent4Color1 = new A.Accent4Color();
            A.RgbColorModelHex rgbColorModelHex6 = new A.RgbColorModelHex(){ Val = "8064A2" };

            accent4Color1.Append(rgbColorModelHex6);

            A.Accent5Color accent5Color1 = new A.Accent5Color();
            A.RgbColorModelHex rgbColorModelHex7 = new A.RgbColorModelHex(){ Val = "4BACC6" };

            accent5Color1.Append(rgbColorModelHex7);

            A.Accent6Color accent6Color1 = new A.Accent6Color();
            A.RgbColorModelHex rgbColorModelHex8 = new A.RgbColorModelHex(){ Val = "F79646" };

            accent6Color1.Append(rgbColorModelHex8);

            A.Hyperlink hyperlink1 = new A.Hyperlink();
            A.RgbColorModelHex rgbColorModelHex9 = new A.RgbColorModelHex(){ Val = "0000FF" };

            hyperlink1.Append(rgbColorModelHex9);

            A.FollowedHyperlinkColor followedHyperlinkColor1 = new A.FollowedHyperlinkColor();
            A.RgbColorModelHex rgbColorModelHex10 = new A.RgbColorModelHex(){ Val = "800080" };

            followedHyperlinkColor1.Append(rgbColorModelHex10);

            colorScheme1.Append(dark1Color1);
            colorScheme1.Append(light1Color1);
            colorScheme1.Append(dark2Color1);
            colorScheme1.Append(light2Color1);
            colorScheme1.Append(accent1Color1);
            colorScheme1.Append(accent2Color1);
            colorScheme1.Append(accent3Color1);
            colorScheme1.Append(accent4Color1);
            colorScheme1.Append(accent5Color1);
            colorScheme1.Append(accent6Color1);
            colorScheme1.Append(hyperlink1);
            colorScheme1.Append(followedHyperlinkColor1);

            A.FontScheme fontScheme1 = new A.FontScheme(){ Name = "Office" };

            A.MajorFont majorFont1 = new A.MajorFont();
            A.LatinFont latinFont1 = new A.LatinFont(){ Typeface = "Cambria" };
            A.EastAsianFont eastAsianFont1 = new A.EastAsianFont(){ Typeface = "" };
            A.ComplexScriptFont complexScriptFont1 = new A.ComplexScriptFont(){ Typeface = "" };
            A.SupplementalFont supplementalFont1 = new A.SupplementalFont(){ Script = "Jpan", Typeface = "ＭＳ Ｐゴシック" };
            A.SupplementalFont supplementalFont2 = new A.SupplementalFont(){ Script = "Hang", Typeface = "맑은 고딕" };
            A.SupplementalFont supplementalFont3 = new A.SupplementalFont(){ Script = "Hans", Typeface = "宋体" };
            A.SupplementalFont supplementalFont4 = new A.SupplementalFont(){ Script = "Hant", Typeface = "新細明體" };
            A.SupplementalFont supplementalFont5 = new A.SupplementalFont(){ Script = "Arab", Typeface = "Times New Roman" };
            A.SupplementalFont supplementalFont6 = new A.SupplementalFont(){ Script = "Hebr", Typeface = "Times New Roman" };
            A.SupplementalFont supplementalFont7 = new A.SupplementalFont(){ Script = "Thai", Typeface = "Tahoma" };
            A.SupplementalFont supplementalFont8 = new A.SupplementalFont(){ Script = "Ethi", Typeface = "Nyala" };
            A.SupplementalFont supplementalFont9 = new A.SupplementalFont(){ Script = "Beng", Typeface = "Vrinda" };
            A.SupplementalFont supplementalFont10 = new A.SupplementalFont(){ Script = "Gujr", Typeface = "Shruti" };
            A.SupplementalFont supplementalFont11 = new A.SupplementalFont(){ Script = "Khmr", Typeface = "MoolBoran" };
            A.SupplementalFont supplementalFont12 = new A.SupplementalFont(){ Script = "Knda", Typeface = "Tunga" };
            A.SupplementalFont supplementalFont13 = new A.SupplementalFont(){ Script = "Guru", Typeface = "Raavi" };
            A.SupplementalFont supplementalFont14 = new A.SupplementalFont(){ Script = "Cans", Typeface = "Euphemia" };
            A.SupplementalFont supplementalFont15 = new A.SupplementalFont(){ Script = "Cher", Typeface = "Plantagenet Cherokee" };
            A.SupplementalFont supplementalFont16 = new A.SupplementalFont(){ Script = "Yiii", Typeface = "Microsoft Yi Baiti" };
            A.SupplementalFont supplementalFont17 = new A.SupplementalFont(){ Script = "Tibt", Typeface = "Microsoft Himalaya" };
            A.SupplementalFont supplementalFont18 = new A.SupplementalFont(){ Script = "Thaa", Typeface = "MV Boli" };
            A.SupplementalFont supplementalFont19 = new A.SupplementalFont(){ Script = "Deva", Typeface = "Mangal" };
            A.SupplementalFont supplementalFont20 = new A.SupplementalFont(){ Script = "Telu", Typeface = "Gautami" };
            A.SupplementalFont supplementalFont21 = new A.SupplementalFont(){ Script = "Taml", Typeface = "Latha" };
            A.SupplementalFont supplementalFont22 = new A.SupplementalFont(){ Script = "Syrc", Typeface = "Estrangelo Edessa" };
            A.SupplementalFont supplementalFont23 = new A.SupplementalFont(){ Script = "Orya", Typeface = "Kalinga" };
            A.SupplementalFont supplementalFont24 = new A.SupplementalFont(){ Script = "Mlym", Typeface = "Kartika" };
            A.SupplementalFont supplementalFont25 = new A.SupplementalFont(){ Script = "Laoo", Typeface = "DokChampa" };
            A.SupplementalFont supplementalFont26 = new A.SupplementalFont(){ Script = "Sinh", Typeface = "Iskoola Pota" };
            A.SupplementalFont supplementalFont27 = new A.SupplementalFont(){ Script = "Mong", Typeface = "Mongolian Baiti" };
            A.SupplementalFont supplementalFont28 = new A.SupplementalFont(){ Script = "Viet", Typeface = "Times New Roman" };
            A.SupplementalFont supplementalFont29 = new A.SupplementalFont(){ Script = "Uigh", Typeface = "Microsoft Uighur" };
            A.SupplementalFont supplementalFont30 = new A.SupplementalFont(){ Script = "Geor", Typeface = "Sylfaen" };

            majorFont1.Append(latinFont1);
            majorFont1.Append(eastAsianFont1);
            majorFont1.Append(complexScriptFont1);
            majorFont1.Append(supplementalFont1);
            majorFont1.Append(supplementalFont2);
            majorFont1.Append(supplementalFont3);
            majorFont1.Append(supplementalFont4);
            majorFont1.Append(supplementalFont5);
            majorFont1.Append(supplementalFont6);
            majorFont1.Append(supplementalFont7);
            majorFont1.Append(supplementalFont8);
            majorFont1.Append(supplementalFont9);
            majorFont1.Append(supplementalFont10);
            majorFont1.Append(supplementalFont11);
            majorFont1.Append(supplementalFont12);
            majorFont1.Append(supplementalFont13);
            majorFont1.Append(supplementalFont14);
            majorFont1.Append(supplementalFont15);
            majorFont1.Append(supplementalFont16);
            majorFont1.Append(supplementalFont17);
            majorFont1.Append(supplementalFont18);
            majorFont1.Append(supplementalFont19);
            majorFont1.Append(supplementalFont20);
            majorFont1.Append(supplementalFont21);
            majorFont1.Append(supplementalFont22);
            majorFont1.Append(supplementalFont23);
            majorFont1.Append(supplementalFont24);
            majorFont1.Append(supplementalFont25);
            majorFont1.Append(supplementalFont26);
            majorFont1.Append(supplementalFont27);
            majorFont1.Append(supplementalFont28);
            majorFont1.Append(supplementalFont29);
            majorFont1.Append(supplementalFont30);

            A.MinorFont minorFont1 = new A.MinorFont();
            A.LatinFont latinFont2 = new A.LatinFont(){ Typeface = "Calibri" };
            A.EastAsianFont eastAsianFont2 = new A.EastAsianFont(){ Typeface = "" };
            A.ComplexScriptFont complexScriptFont2 = new A.ComplexScriptFont(){ Typeface = "" };
            A.SupplementalFont supplementalFont31 = new A.SupplementalFont(){ Script = "Jpan", Typeface = "ＭＳ Ｐゴシック" };
            A.SupplementalFont supplementalFont32 = new A.SupplementalFont(){ Script = "Hang", Typeface = "맑은 고딕" };
            A.SupplementalFont supplementalFont33 = new A.SupplementalFont(){ Script = "Hans", Typeface = "宋体" };
            A.SupplementalFont supplementalFont34 = new A.SupplementalFont(){ Script = "Hant", Typeface = "新細明體" };
            A.SupplementalFont supplementalFont35 = new A.SupplementalFont(){ Script = "Arab", Typeface = "Arial" };
            A.SupplementalFont supplementalFont36 = new A.SupplementalFont(){ Script = "Hebr", Typeface = "Arial" };
            A.SupplementalFont supplementalFont37 = new A.SupplementalFont(){ Script = "Thai", Typeface = "Tahoma" };
            A.SupplementalFont supplementalFont38 = new A.SupplementalFont(){ Script = "Ethi", Typeface = "Nyala" };
            A.SupplementalFont supplementalFont39 = new A.SupplementalFont(){ Script = "Beng", Typeface = "Vrinda" };
            A.SupplementalFont supplementalFont40 = new A.SupplementalFont(){ Script = "Gujr", Typeface = "Shruti" };
            A.SupplementalFont supplementalFont41 = new A.SupplementalFont(){ Script = "Khmr", Typeface = "DaunPenh" };
            A.SupplementalFont supplementalFont42 = new A.SupplementalFont(){ Script = "Knda", Typeface = "Tunga" };
            A.SupplementalFont supplementalFont43 = new A.SupplementalFont(){ Script = "Guru", Typeface = "Raavi" };
            A.SupplementalFont supplementalFont44 = new A.SupplementalFont(){ Script = "Cans", Typeface = "Euphemia" };
            A.SupplementalFont supplementalFont45 = new A.SupplementalFont(){ Script = "Cher", Typeface = "Plantagenet Cherokee" };
            A.SupplementalFont supplementalFont46 = new A.SupplementalFont(){ Script = "Yiii", Typeface = "Microsoft Yi Baiti" };
            A.SupplementalFont supplementalFont47 = new A.SupplementalFont(){ Script = "Tibt", Typeface = "Microsoft Himalaya" };
            A.SupplementalFont supplementalFont48 = new A.SupplementalFont(){ Script = "Thaa", Typeface = "MV Boli" };
            A.SupplementalFont supplementalFont49 = new A.SupplementalFont(){ Script = "Deva", Typeface = "Mangal" };
            A.SupplementalFont supplementalFont50 = new A.SupplementalFont(){ Script = "Telu", Typeface = "Gautami" };
            A.SupplementalFont supplementalFont51 = new A.SupplementalFont(){ Script = "Taml", Typeface = "Latha" };
            A.SupplementalFont supplementalFont52 = new A.SupplementalFont(){ Script = "Syrc", Typeface = "Estrangelo Edessa" };
            A.SupplementalFont supplementalFont53 = new A.SupplementalFont(){ Script = "Orya", Typeface = "Kalinga" };
            A.SupplementalFont supplementalFont54 = new A.SupplementalFont(){ Script = "Mlym", Typeface = "Kartika" };
            A.SupplementalFont supplementalFont55 = new A.SupplementalFont(){ Script = "Laoo", Typeface = "DokChampa" };
            A.SupplementalFont supplementalFont56 = new A.SupplementalFont(){ Script = "Sinh", Typeface = "Iskoola Pota" };
            A.SupplementalFont supplementalFont57 = new A.SupplementalFont(){ Script = "Mong", Typeface = "Mongolian Baiti" };
            A.SupplementalFont supplementalFont58 = new A.SupplementalFont(){ Script = "Viet", Typeface = "Arial" };
            A.SupplementalFont supplementalFont59 = new A.SupplementalFont(){ Script = "Uigh", Typeface = "Microsoft Uighur" };
            A.SupplementalFont supplementalFont60 = new A.SupplementalFont(){ Script = "Geor", Typeface = "Sylfaen" };

            minorFont1.Append(latinFont2);
            minorFont1.Append(eastAsianFont2);
            minorFont1.Append(complexScriptFont2);
            minorFont1.Append(supplementalFont31);
            minorFont1.Append(supplementalFont32);
            minorFont1.Append(supplementalFont33);
            minorFont1.Append(supplementalFont34);
            minorFont1.Append(supplementalFont35);
            minorFont1.Append(supplementalFont36);
            minorFont1.Append(supplementalFont37);
            minorFont1.Append(supplementalFont38);
            minorFont1.Append(supplementalFont39);
            minorFont1.Append(supplementalFont40);
            minorFont1.Append(supplementalFont41);
            minorFont1.Append(supplementalFont42);
            minorFont1.Append(supplementalFont43);
            minorFont1.Append(supplementalFont44);
            minorFont1.Append(supplementalFont45);
            minorFont1.Append(supplementalFont46);
            minorFont1.Append(supplementalFont47);
            minorFont1.Append(supplementalFont48);
            minorFont1.Append(supplementalFont49);
            minorFont1.Append(supplementalFont50);
            minorFont1.Append(supplementalFont51);
            minorFont1.Append(supplementalFont52);
            minorFont1.Append(supplementalFont53);
            minorFont1.Append(supplementalFont54);
            minorFont1.Append(supplementalFont55);
            minorFont1.Append(supplementalFont56);
            minorFont1.Append(supplementalFont57);
            minorFont1.Append(supplementalFont58);
            minorFont1.Append(supplementalFont59);
            minorFont1.Append(supplementalFont60);

            fontScheme1.Append(majorFont1);
            fontScheme1.Append(minorFont1);

            A.FormatScheme formatScheme1 = new A.FormatScheme(){ Name = "Office" };

            A.FillStyleList fillStyleList1 = new A.FillStyleList();

            A.SolidFill solidFill1 = new A.SolidFill();
            A.SchemeColor schemeColor1 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };

            solidFill1.Append(schemeColor1);

            A.GradientFill gradientFill1 = new A.GradientFill(){ RotateWithShape = true };

            A.GradientStopList gradientStopList1 = new A.GradientStopList();

            A.GradientStop gradientStop1 = new A.GradientStop(){ Position = 0 };

            A.SchemeColor schemeColor2 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Tint tint1 = new A.Tint(){ Val = 50000 };
            A.SaturationModulation saturationModulation1 = new A.SaturationModulation(){ Val = 300000 };

            schemeColor2.Append(tint1);
            schemeColor2.Append(saturationModulation1);

            gradientStop1.Append(schemeColor2);

            A.GradientStop gradientStop2 = new A.GradientStop(){ Position = 35000 };

            A.SchemeColor schemeColor3 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Tint tint2 = new A.Tint(){ Val = 37000 };
            A.SaturationModulation saturationModulation2 = new A.SaturationModulation(){ Val = 300000 };

            schemeColor3.Append(tint2);
            schemeColor3.Append(saturationModulation2);

            gradientStop2.Append(schemeColor3);

            A.GradientStop gradientStop3 = new A.GradientStop(){ Position = 100000 };

            A.SchemeColor schemeColor4 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Tint tint3 = new A.Tint(){ Val = 15000 };
            A.SaturationModulation saturationModulation3 = new A.SaturationModulation(){ Val = 350000 };

            schemeColor4.Append(tint3);
            schemeColor4.Append(saturationModulation3);

            gradientStop3.Append(schemeColor4);

            gradientStopList1.Append(gradientStop1);
            gradientStopList1.Append(gradientStop2);
            gradientStopList1.Append(gradientStop3);
            A.LinearGradientFill linearGradientFill1 = new A.LinearGradientFill(){ Angle = 16200000, Scaled = true };

            gradientFill1.Append(gradientStopList1);
            gradientFill1.Append(linearGradientFill1);

            A.GradientFill gradientFill2 = new A.GradientFill(){ RotateWithShape = true };

            A.GradientStopList gradientStopList2 = new A.GradientStopList();

            A.GradientStop gradientStop4 = new A.GradientStop(){ Position = 0 };

            A.SchemeColor schemeColor5 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Shade shade1 = new A.Shade(){ Val = 51000 };
            A.SaturationModulation saturationModulation4 = new A.SaturationModulation(){ Val = 130000 };

            schemeColor5.Append(shade1);
            schemeColor5.Append(saturationModulation4);

            gradientStop4.Append(schemeColor5);

            A.GradientStop gradientStop5 = new A.GradientStop(){ Position = 80000 };

            A.SchemeColor schemeColor6 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Shade shade2 = new A.Shade(){ Val = 93000 };
            A.SaturationModulation saturationModulation5 = new A.SaturationModulation(){ Val = 130000 };

            schemeColor6.Append(shade2);
            schemeColor6.Append(saturationModulation5);

            gradientStop5.Append(schemeColor6);

            A.GradientStop gradientStop6 = new A.GradientStop(){ Position = 100000 };

            A.SchemeColor schemeColor7 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Shade shade3 = new A.Shade(){ Val = 94000 };
            A.SaturationModulation saturationModulation6 = new A.SaturationModulation(){ Val = 135000 };

            schemeColor7.Append(shade3);
            schemeColor7.Append(saturationModulation6);

            gradientStop6.Append(schemeColor7);

            gradientStopList2.Append(gradientStop4);
            gradientStopList2.Append(gradientStop5);
            gradientStopList2.Append(gradientStop6);
            A.LinearGradientFill linearGradientFill2 = new A.LinearGradientFill(){ Angle = 16200000, Scaled = false };

            gradientFill2.Append(gradientStopList2);
            gradientFill2.Append(linearGradientFill2);

            fillStyleList1.Append(solidFill1);
            fillStyleList1.Append(gradientFill1);
            fillStyleList1.Append(gradientFill2);

            A.LineStyleList lineStyleList1 = new A.LineStyleList();

            A.Outline outline1 = new A.Outline(){ Width = 9525, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill2 = new A.SolidFill();

            A.SchemeColor schemeColor8 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Shade shade4 = new A.Shade(){ Val = 95000 };
            A.SaturationModulation saturationModulation7 = new A.SaturationModulation(){ Val = 105000 };

            schemeColor8.Append(shade4);
            schemeColor8.Append(saturationModulation7);

            solidFill2.Append(schemeColor8);
            A.PresetDash presetDash1 = new A.PresetDash(){ Val = A.PresetLineDashValues.Solid };

            outline1.Append(solidFill2);
            outline1.Append(presetDash1);

            A.Outline outline2 = new A.Outline(){ Width = 25400, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill3 = new A.SolidFill();
            A.SchemeColor schemeColor9 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };

            solidFill3.Append(schemeColor9);
            A.PresetDash presetDash2 = new A.PresetDash(){ Val = A.PresetLineDashValues.Solid };

            outline2.Append(solidFill3);
            outline2.Append(presetDash2);

            A.Outline outline3 = new A.Outline(){ Width = 38100, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill4 = new A.SolidFill();
            A.SchemeColor schemeColor10 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };

            solidFill4.Append(schemeColor10);
            A.PresetDash presetDash3 = new A.PresetDash(){ Val = A.PresetLineDashValues.Solid };

            outline3.Append(solidFill4);
            outline3.Append(presetDash3);

            lineStyleList1.Append(outline1);
            lineStyleList1.Append(outline2);
            lineStyleList1.Append(outline3);

            A.EffectStyleList effectStyleList1 = new A.EffectStyleList();

            A.EffectStyle effectStyle1 = new A.EffectStyle();

            A.EffectList effectList1 = new A.EffectList();

            A.OuterShadow outerShadow1 = new A.OuterShadow(){ BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false };

            A.RgbColorModelHex rgbColorModelHex11 = new A.RgbColorModelHex(){ Val = "000000" };
            A.Alpha alpha1 = new A.Alpha(){ Val = 38000 };

            rgbColorModelHex11.Append(alpha1);

            outerShadow1.Append(rgbColorModelHex11);

            effectList1.Append(outerShadow1);

            effectStyle1.Append(effectList1);

            A.EffectStyle effectStyle2 = new A.EffectStyle();

            A.EffectList effectList2 = new A.EffectList();

            A.OuterShadow outerShadow2 = new A.OuterShadow(){ BlurRadius = 40000L, Distance = 23000L, Direction = 5400000, RotateWithShape = false };

            A.RgbColorModelHex rgbColorModelHex12 = new A.RgbColorModelHex(){ Val = "000000" };
            A.Alpha alpha2 = new A.Alpha(){ Val = 35000 };

            rgbColorModelHex12.Append(alpha2);

            outerShadow2.Append(rgbColorModelHex12);

            effectList2.Append(outerShadow2);

            effectStyle2.Append(effectList2);

            A.EffectStyle effectStyle3 = new A.EffectStyle();

            A.EffectList effectList3 = new A.EffectList();

            A.OuterShadow outerShadow3 = new A.OuterShadow(){ BlurRadius = 40000L, Distance = 23000L, Direction = 5400000, RotateWithShape = false };

            A.RgbColorModelHex rgbColorModelHex13 = new A.RgbColorModelHex(){ Val = "000000" };
            A.Alpha alpha3 = new A.Alpha(){ Val = 35000 };

            rgbColorModelHex13.Append(alpha3);

            outerShadow3.Append(rgbColorModelHex13);

            effectList3.Append(outerShadow3);

            A.Scene3DType scene3DType1 = new A.Scene3DType();

            A.Camera camera1 = new A.Camera(){ Preset = A.PresetCameraValues.OrthographicFront };
            A.Rotation rotation1 = new A.Rotation(){ Latitude = 0, Longitude = 0, Revolution = 0 };

            camera1.Append(rotation1);

            A.LightRig lightRig1 = new A.LightRig(){ Rig = A.LightRigValues.ThreePoints, Direction = A.LightRigDirectionValues.Top };
            A.Rotation rotation2 = new A.Rotation(){ Latitude = 0, Longitude = 0, Revolution = 1200000 };

            lightRig1.Append(rotation2);

            scene3DType1.Append(camera1);
            scene3DType1.Append(lightRig1);

            A.Shape3DType shape3DType1 = new A.Shape3DType();
            A.BevelTop bevelTop1 = new A.BevelTop(){ Width = 63500L, Height = 25400L };

            shape3DType1.Append(bevelTop1);

            effectStyle3.Append(effectList3);
            effectStyle3.Append(scene3DType1);
            effectStyle3.Append(shape3DType1);

            effectStyleList1.Append(effectStyle1);
            effectStyleList1.Append(effectStyle2);
            effectStyleList1.Append(effectStyle3);

            A.BackgroundFillStyleList backgroundFillStyleList1 = new A.BackgroundFillStyleList();

            A.SolidFill solidFill5 = new A.SolidFill();
            A.SchemeColor schemeColor11 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };

            solidFill5.Append(schemeColor11);

            A.GradientFill gradientFill3 = new A.GradientFill(){ RotateWithShape = true };

            A.GradientStopList gradientStopList3 = new A.GradientStopList();

            A.GradientStop gradientStop7 = new A.GradientStop(){ Position = 0 };

            A.SchemeColor schemeColor12 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Tint tint4 = new A.Tint(){ Val = 40000 };
            A.SaturationModulation saturationModulation8 = new A.SaturationModulation(){ Val = 350000 };

            schemeColor12.Append(tint4);
            schemeColor12.Append(saturationModulation8);

            gradientStop7.Append(schemeColor12);

            A.GradientStop gradientStop8 = new A.GradientStop(){ Position = 40000 };

            A.SchemeColor schemeColor13 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Tint tint5 = new A.Tint(){ Val = 45000 };
            A.Shade shade5 = new A.Shade(){ Val = 99000 };
            A.SaturationModulation saturationModulation9 = new A.SaturationModulation(){ Val = 350000 };

            schemeColor13.Append(tint5);
            schemeColor13.Append(shade5);
            schemeColor13.Append(saturationModulation9);

            gradientStop8.Append(schemeColor13);

            A.GradientStop gradientStop9 = new A.GradientStop(){ Position = 100000 };

            A.SchemeColor schemeColor14 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Shade shade6 = new A.Shade(){ Val = 20000 };
            A.SaturationModulation saturationModulation10 = new A.SaturationModulation(){ Val = 255000 };

            schemeColor14.Append(shade6);
            schemeColor14.Append(saturationModulation10);

            gradientStop9.Append(schemeColor14);

            gradientStopList3.Append(gradientStop7);
            gradientStopList3.Append(gradientStop8);
            gradientStopList3.Append(gradientStop9);

            A.PathGradientFill pathGradientFill1 = new A.PathGradientFill(){ Path = A.PathShadeValues.Circle };
            A.FillToRectangle fillToRectangle1 = new A.FillToRectangle(){ Left = 50000, Top = -80000, Right = 50000, Bottom = 180000 };

            pathGradientFill1.Append(fillToRectangle1);

            gradientFill3.Append(gradientStopList3);
            gradientFill3.Append(pathGradientFill1);

            A.GradientFill gradientFill4 = new A.GradientFill(){ RotateWithShape = true };

            A.GradientStopList gradientStopList4 = new A.GradientStopList();

            A.GradientStop gradientStop10 = new A.GradientStop(){ Position = 0 };

            A.SchemeColor schemeColor15 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Tint tint6 = new A.Tint(){ Val = 80000 };
            A.SaturationModulation saturationModulation11 = new A.SaturationModulation(){ Val = 300000 };

            schemeColor15.Append(tint6);
            schemeColor15.Append(saturationModulation11);

            gradientStop10.Append(schemeColor15);

            A.GradientStop gradientStop11 = new A.GradientStop(){ Position = 100000 };

            A.SchemeColor schemeColor16 = new A.SchemeColor(){ Val = A.SchemeColorValues.PhColor };
            A.Shade shade7 = new A.Shade(){ Val = 30000 };
            A.SaturationModulation saturationModulation12 = new A.SaturationModulation(){ Val = 200000 };

            schemeColor16.Append(shade7);
            schemeColor16.Append(saturationModulation12);

            gradientStop11.Append(schemeColor16);

            gradientStopList4.Append(gradientStop10);
            gradientStopList4.Append(gradientStop11);

            A.PathGradientFill pathGradientFill2 = new A.PathGradientFill(){ Path = A.PathShadeValues.Circle };
            A.FillToRectangle fillToRectangle2 = new A.FillToRectangle(){ Left = 50000, Top = 50000, Right = 50000, Bottom = 50000 };

            pathGradientFill2.Append(fillToRectangle2);

            gradientFill4.Append(gradientStopList4);
            gradientFill4.Append(pathGradientFill2);

            backgroundFillStyleList1.Append(solidFill5);
            backgroundFillStyleList1.Append(gradientFill3);
            backgroundFillStyleList1.Append(gradientFill4);

            formatScheme1.Append(fillStyleList1);
            formatScheme1.Append(lineStyleList1);
            formatScheme1.Append(effectStyleList1);
            formatScheme1.Append(backgroundFillStyleList1);

            themeElements1.Append(colorScheme1);
            themeElements1.Append(fontScheme1);
            themeElements1.Append(formatScheme1);
            A.ObjectDefaults objectDefaults1 = new A.ObjectDefaults();
            A.ExtraColorSchemeList extraColorSchemeList1 = new A.ExtraColorSchemeList();

            theme1.Append(themeElements1);
            theme1.Append(objectDefaults1);
            theme1.Append(extraColorSchemeList1);

            themePart1.Theme = theme1;
        }

        // Generates content of worksheetPart1.
        private void GenerateWorksheetPart1Content(WorksheetPart worksheetPart1)
        {
            Worksheet worksheet1 = new Worksheet(){ MCAttributes = new MarkupCompatibilityAttributes(){ Ignorable = "x14ac" }  };
            worksheet1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            worksheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            worksheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");
            SheetDimension sheetDimension1 = new SheetDimension(){ Reference = "A1:AJ5" };

            SheetViews sheetViews1 = new SheetViews();
            SheetView sheetView1 = new SheetView(){ TabSelected = true, ZoomScaleNormal = (UInt32Value)100U, WorkbookViewId = (UInt32Value)0U };

            sheetViews1.Append(sheetView1);
            SheetFormatProperties sheetFormatProperties1 = new SheetFormatProperties(){ DefaultRowHeight = 15D, DyDescent = 0.25D };

            Columns columns1 = new Columns();
            Column column1 = new Column(){ Min = (UInt32Value)1U, Max = (UInt32Value)1U, Width = 9.140625D, Style = (UInt32Value)1U };
            Column column2 = new Column(){ Min = (UInt32Value)2U, Max = (UInt32Value)2U, Width = 33D, Style = (UInt32Value)1U, BestFit = true, CustomWidth = true };
            Column column3 = new Column(){ Min = (UInt32Value)3U, Max = (UInt32Value)3U, Width = 19.28515625D, Style = (UInt32Value)1U, BestFit = true, CustomWidth = true };
            Column column4 = new Column(){ Min = (UInt32Value)4U, Max = (UInt32Value)4U, Width = 18.140625D, Style = (UInt32Value)1U, BestFit = true, CustomWidth = true };
            Column column5 = new Column(){ Min = (UInt32Value)5U, Max = (UInt32Value)5U, Width = 21.85546875D, Style = (UInt32Value)1U, BestFit = true, CustomWidth = true };
            Column column6 = new Column(){ Min = (UInt32Value)6U, Max = (UInt32Value)6U, Width = 12.85546875D, Style = (UInt32Value)1U, BestFit = true, CustomWidth = true };
            Column column7 = new Column(){ Min = (UInt32Value)7U, Max = (UInt32Value)7U, Width = 9.42578125D, Style = (UInt32Value)3U, BestFit = true, CustomWidth = true };
            Column column8 = new Column(){ Min = (UInt32Value)8U, Max = (UInt32Value)11U, Width = 9.140625D, Style = (UInt32Value)3U };
            Column column9 = new Column(){ Min = (UInt32Value)12U, Max = (UInt32Value)12U, Width = 15D, Style = (UInt32Value)10U, BestFit = true, CustomWidth = true };
            Column column10 = new Column(){ Min = (UInt32Value)13U, Max = (UInt32Value)21U, Width = 9.140625D, Style = (UInt32Value)3U };
            Column column11 = new Column(){ Min = (UInt32Value)22U, Max = (UInt32Value)22U, Width = 9.140625D, Style = (UInt32Value)1U };
            Column column12 = new Column(){ Min = (UInt32Value)23U, Max = (UInt32Value)36U, Width = 9.140625D, Style = (UInt32Value)3U };

            columns1.Append(column1);
            columns1.Append(column2);
            columns1.Append(column3);
            columns1.Append(column4);
            columns1.Append(column5);
            columns1.Append(column6);
            columns1.Append(column7);
            columns1.Append(column8);
            columns1.Append(column9);
            columns1.Append(column10);
            columns1.Append(column11);
            columns1.Append(column12);

            SheetData sheetData1 = new SheetData();

            Row row1 = new Row(){ RowIndex = (UInt32Value)1U, Spans = new ListValue<StringValue>() { InnerText = "1:36" }, DyDescent = 0.25D };

            Cell cell1 = new Cell(){ CellReference = "A1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue1 = new CellValue();
            cellValue1.Text = "0";

            cell1.Append(cellValue1);

            Cell cell2 = new Cell(){ CellReference = "B1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue2 = new CellValue();
            cellValue2.Text = "2";

            cell2.Append(cellValue2);

            Cell cell3 = new Cell(){ CellReference = "C1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue3 = new CellValue();
            cellValue3.Text = "4";

            cell3.Append(cellValue3);

            Cell cell4 = new Cell(){ CellReference = "D1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue4 = new CellValue();
            cellValue4.Text = "6";

            cell4.Append(cellValue4);

            Cell cell5 = new Cell(){ CellReference = "E1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue5 = new CellValue();
            cellValue5.Text = "8";

            cell5.Append(cellValue5);

            Cell cell6 = new Cell(){ CellReference = "F1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue6 = new CellValue();
            cellValue6.Text = "10";

            cell6.Append(cellValue6);

            Cell cell7 = new Cell(){ CellReference = "G1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue7 = new CellValue();
            cellValue7.Text = "12";

            cell7.Append(cellValue7);

            Cell cell8 = new Cell(){ CellReference = "H1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue8 = new CellValue();
            cellValue8.Text = "13";

            cell8.Append(cellValue8);

            Cell cell9 = new Cell(){ CellReference = "I1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue9 = new CellValue();
            cellValue9.Text = "14";

            cell9.Append(cellValue9);

            Cell cell10 = new Cell(){ CellReference = "J1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue10 = new CellValue();
            cellValue10.Text = "15";

            cell10.Append(cellValue10);

            Cell cell11 = new Cell(){ CellReference = "K1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue11 = new CellValue();
            cellValue11.Text = "16";

            cell11.Append(cellValue11);

            Cell cell12 = new Cell(){ CellReference = "L1", StyleIndex = (UInt32Value)9U, DataType = CellValues.SharedString };
            CellValue cellValue12 = new CellValue();
            cellValue12.Text = "17";

            cell12.Append(cellValue12);

            Cell cell13 = new Cell(){ CellReference = "M1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue13 = new CellValue();
            cellValue13.Text = "18";

            cell13.Append(cellValue13);

            Cell cell14 = new Cell(){ CellReference = "N1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue14 = new CellValue();
            cellValue14.Text = "19";

            cell14.Append(cellValue14);

            Cell cell15 = new Cell(){ CellReference = "O1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue15 = new CellValue();
            cellValue15.Text = "20";

            cell15.Append(cellValue15);

            Cell cell16 = new Cell(){ CellReference = "P1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue16 = new CellValue();
            cellValue16.Text = "21";

            cell16.Append(cellValue16);

            Cell cell17 = new Cell(){ CellReference = "Q1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue17 = new CellValue();
            cellValue17.Text = "22";

            cell17.Append(cellValue17);

            Cell cell18 = new Cell(){ CellReference = "R1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue18 = new CellValue();
            cellValue18.Text = "23";

            cell18.Append(cellValue18);

            Cell cell19 = new Cell(){ CellReference = "S1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue19 = new CellValue();
            cellValue19.Text = "24";

            cell19.Append(cellValue19);

            Cell cell20 = new Cell(){ CellReference = "T1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue20 = new CellValue();
            cellValue20.Text = "25";

            cell20.Append(cellValue20);

            Cell cell21 = new Cell(){ CellReference = "U1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue21 = new CellValue();
            cellValue21.Text = "26";

            cell21.Append(cellValue21);

            Cell cell22 = new Cell(){ CellReference = "V1", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue22 = new CellValue();
            cellValue22.Text = "27";

            cell22.Append(cellValue22);

            Cell cell23 = new Cell(){ CellReference = "W1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue23 = new CellValue();
            cellValue23.Text = "29";

            cell23.Append(cellValue23);

            Cell cell24 = new Cell(){ CellReference = "X1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue24 = new CellValue();
            cellValue24.Text = "30";

            cell24.Append(cellValue24);

            Cell cell25 = new Cell(){ CellReference = "Y1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue25 = new CellValue();
            cellValue25.Text = "31";

            cell25.Append(cellValue25);

            Cell cell26 = new Cell(){ CellReference = "Z1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue26 = new CellValue();
            cellValue26.Text = "32";

            cell26.Append(cellValue26);

            Cell cell27 = new Cell(){ CellReference = "AA1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue27 = new CellValue();
            cellValue27.Text = "33";

            cell27.Append(cellValue27);

            Cell cell28 = new Cell(){ CellReference = "AB1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue28 = new CellValue();
            cellValue28.Text = "34";

            cell28.Append(cellValue28);

            Cell cell29 = new Cell(){ CellReference = "AC1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue29 = new CellValue();
            cellValue29.Text = "35";

            cell29.Append(cellValue29);

            Cell cell30 = new Cell(){ CellReference = "AD1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue30 = new CellValue();
            cellValue30.Text = "36";

            cell30.Append(cellValue30);

            Cell cell31 = new Cell(){ CellReference = "AE1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue31 = new CellValue();
            cellValue31.Text = "37";

            cell31.Append(cellValue31);

            Cell cell32 = new Cell(){ CellReference = "AF1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue32 = new CellValue();
            cellValue32.Text = "38";

            cell32.Append(cellValue32);

            Cell cell33 = new Cell(){ CellReference = "AG1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue33 = new CellValue();
            cellValue33.Text = "39";

            cell33.Append(cellValue33);

            Cell cell34 = new Cell(){ CellReference = "AH1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue34 = new CellValue();
            cellValue34.Text = "40";

            cell34.Append(cellValue34);

            Cell cell35 = new Cell(){ CellReference = "AI1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue35 = new CellValue();
            cellValue35.Text = "41";

            cell35.Append(cellValue35);

            Cell cell36 = new Cell(){ CellReference = "AJ1", StyleIndex = (UInt32Value)4U, DataType = CellValues.SharedString };
            CellValue cellValue36 = new CellValue();
            cellValue36.Text = "42";

            cell36.Append(cellValue36);

            row1.Append(cell1);
            row1.Append(cell2);
            row1.Append(cell3);
            row1.Append(cell4);
            row1.Append(cell5);
            row1.Append(cell6);
            row1.Append(cell7);
            row1.Append(cell8);
            row1.Append(cell9);
            row1.Append(cell10);
            row1.Append(cell11);
            row1.Append(cell12);
            row1.Append(cell13);
            row1.Append(cell14);
            row1.Append(cell15);
            row1.Append(cell16);
            row1.Append(cell17);
            row1.Append(cell18);
            row1.Append(cell19);
            row1.Append(cell20);
            row1.Append(cell21);
            row1.Append(cell22);
            row1.Append(cell23);
            row1.Append(cell24);
            row1.Append(cell25);
            row1.Append(cell26);
            row1.Append(cell27);
            row1.Append(cell28);
            row1.Append(cell29);
            row1.Append(cell30);
            row1.Append(cell31);
            row1.Append(cell32);
            row1.Append(cell33);
            row1.Append(cell34);
            row1.Append(cell35);
            row1.Append(cell36);

            Row row2 = new Row(){ RowIndex = (UInt32Value)2U, Spans = new ListValue<StringValue>() { InnerText = "1:36" }, DyDescent = 0.25D };

            Cell cell37 = new Cell(){ CellReference = "A2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue37 = new CellValue();
            cellValue37.Text = "1";

            cell37.Append(cellValue37);

            Cell cell38 = new Cell(){ CellReference = "B2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue38 = new CellValue();
            cellValue38.Text = "3";

            cell38.Append(cellValue38);

            Cell cell39 = new Cell(){ CellReference = "C2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue39 = new CellValue();
            cellValue39.Text = "5";

            cell39.Append(cellValue39);

            Cell cell40 = new Cell(){ CellReference = "D2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue40 = new CellValue();
            cellValue40.Text = "7";

            cell40.Append(cellValue40);

            Cell cell41 = new Cell(){ CellReference = "E2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue41 = new CellValue();
            cellValue41.Text = "9";

            cell41.Append(cellValue41);

            Cell cell42 = new Cell(){ CellReference = "F2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue42 = new CellValue();
            cellValue42.Text = "11";

            cell42.Append(cellValue42);

            Cell cell43 = new Cell(){ CellReference = "G2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue43 = new CellValue();
            cellValue43.Text = "326";

            cell43.Append(cellValue43);

            Cell cell44 = new Cell(){ CellReference = "H2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue44 = new CellValue();
            cellValue44.Text = "239.11";

            cell44.Append(cellValue44);

            Cell cell45 = new Cell(){ CellReference = "I2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue45 = new CellValue();
            cellValue45.Text = "290";

            cell45.Append(cellValue45);

            Cell cell46 = new Cell(){ CellReference = "J2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue46 = new CellValue();
            cellValue46.Text = "500";

            cell46.Append(cellValue46);

            Cell cell47 = new Cell(){ CellReference = "K2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue47 = new CellValue();
            cellValue47.Text = "0.11";

            cell47.Append(cellValue47);

            Cell cell48 = new Cell(){ CellReference = "L2", StyleIndex = (UInt32Value)10U };
            CellValue cellValue48 = new CellValue();
            cellValue48.Text = "0.86";

            cell48.Append(cellValue48);

            Cell cell49 = new Cell(){ CellReference = "M2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue49 = new CellValue();
            cellValue49.Text = "285";

            cell49.Append(cellValue49);

            Cell cell50 = new Cell(){ CellReference = "N2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue50 = new CellValue();
            cellValue50.Text = "0";

            cell50.Append(cellValue50);

            Cell cell51 = new Cell(){ CellReference = "O2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue51 = new CellValue();
            cellValue51.Text = "259.86";

            cell51.Append(cellValue51);

            Cell cell52 = new Cell(){ CellReference = "P2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue52 = new CellValue();
            cellValue52.Text = "0";

            cell52.Append(cellValue52);

            Cell cell53 = new Cell(){ CellReference = "Q2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue53 = new CellValue();
            cellValue53.Text = "239.11";

            cell53.Append(cellValue53);

            Cell cell54 = new Cell(){ CellReference = "R2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue54 = new CellValue();
            cellValue54.Text = "255.0838";

            cell54.Append(cellValue54);

            Cell cell55 = new Cell(){ CellReference = "S2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue55 = new CellValue();
            cellValue55.Text = "237.10900000000001";

            cell55.Append(cellValue55);

            Cell cell56 = new Cell(){ CellReference = "T2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue56 = new CellValue();
            cellValue56.Text = "41.87";

            cell56.Append(cellValue56);

            Cell cell57 = new Cell(){ CellReference = "U2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue57 = new CellValue();
            cellValue57.Text = "41.87";

            cell57.Append(cellValue57);

            Cell cell58 = new Cell(){ CellReference = "V2", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue58 = new CellValue();
            cellValue58.Text = "28";

            cell58.Append(cellValue58);

            Cell cell59 = new Cell(){ CellReference = "W2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue59 = new CellValue();
            cellValue59.Text = "1";

            cell59.Append(cellValue59);

            Cell cell60 = new Cell(){ CellReference = "X2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue60 = new CellValue();
            cellValue60.Text = "0";

            cell60.Append(cellValue60);

            Cell cell61 = new Cell(){ CellReference = "Y2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue61 = new CellValue();
            cellValue61.Text = "189.88";

            cell61.Append(cellValue61);

            Cell cell62 = new Cell(){ CellReference = "Z2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue62 = new CellValue();
            cellValue62.Text = "256";

            cell62.Append(cellValue62);

            Cell cell63 = new Cell(){ CellReference = "AA2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue63 = new CellValue();
            cellValue63.Text = "250";

            cell63.Append(cellValue63);

            Cell cell64 = new Cell(){ CellReference = "AB2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue64 = new CellValue();
            cellValue64.Text = "264";

            cell64.Append(cellValue64);

            Cell cell65 = new Cell(){ CellReference = "AC2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue65 = new CellValue();
            cellValue65.Text = "259.86";

            cell65.Append(cellValue65);

            Cell cell66 = new Cell(){ CellReference = "AD2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue66 = new CellValue();
            cellValue66.Text = "167.57";

            cell66.Append(cellValue66);

            Cell cell67 = new Cell(){ CellReference = "AE2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue67 = new CellValue();
            cellValue67.Text = "0";

            cell67.Append(cellValue67);

            Cell cell68 = new Cell(){ CellReference = "AF2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue68 = new CellValue();
            cellValue68.Text = "0";

            cell68.Append(cellValue68);

            Cell cell69 = new Cell(){ CellReference = "AG2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue69 = new CellValue();
            cellValue69.Text = "0";

            cell69.Append(cellValue69);

            Cell cell70 = new Cell(){ CellReference = "AH2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue70 = new CellValue();
            cellValue70.Text = "0";

            cell70.Append(cellValue70);

            Cell cell71 = new Cell(){ CellReference = "AI2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue71 = new CellValue();
            cellValue71.Text = "0";

            cell71.Append(cellValue71);

            Cell cell72 = new Cell(){ CellReference = "AJ2", StyleIndex = (UInt32Value)3U };
            CellValue cellValue72 = new CellValue();
            cellValue72.Text = "0";

            cell72.Append(cellValue72);

            row2.Append(cell37);
            row2.Append(cell38);
            row2.Append(cell39);
            row2.Append(cell40);
            row2.Append(cell41);
            row2.Append(cell42);
            row2.Append(cell43);
            row2.Append(cell44);
            row2.Append(cell45);
            row2.Append(cell46);
            row2.Append(cell47);
            row2.Append(cell48);
            row2.Append(cell49);
            row2.Append(cell50);
            row2.Append(cell51);
            row2.Append(cell52);
            row2.Append(cell53);
            row2.Append(cell54);
            row2.Append(cell55);
            row2.Append(cell56);
            row2.Append(cell57);
            row2.Append(cell58);
            row2.Append(cell59);
            row2.Append(cell60);
            row2.Append(cell61);
            row2.Append(cell62);
            row2.Append(cell63);
            row2.Append(cell64);
            row2.Append(cell65);
            row2.Append(cell66);
            row2.Append(cell67);
            row2.Append(cell68);
            row2.Append(cell69);
            row2.Append(cell70);
            row2.Append(cell71);
            row2.Append(cell72);

            Row row3 = new Row(){ RowIndex = (UInt32Value)3U, Spans = new ListValue<StringValue>() { InnerText = "1:36" }, DyDescent = 0.25D };

            Cell cell73 = new Cell(){ CellReference = "A3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue73 = new CellValue();
            cellValue73.Text = "43";

            cell73.Append(cellValue73);

            Cell cell74 = new Cell(){ CellReference = "B3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue74 = new CellValue();
            cellValue74.Text = "44";

            cell74.Append(cellValue74);

            Cell cell75 = new Cell(){ CellReference = "C3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue75 = new CellValue();
            cellValue75.Text = "5";

            cell75.Append(cellValue75);

            Cell cell76 = new Cell(){ CellReference = "D3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue76 = new CellValue();
            cellValue76.Text = "7";

            cell76.Append(cellValue76);

            Cell cell77 = new Cell(){ CellReference = "E3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue77 = new CellValue();
            cellValue77.Text = "9";

            cell77.Append(cellValue77);

            Cell cell78 = new Cell(){ CellReference = "F3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue78 = new CellValue();
            cellValue78.Text = "11";

            cell78.Append(cellValue78);

            Cell cell79 = new Cell(){ CellReference = "G3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue79 = new CellValue();
            cellValue79.Text = "155";

            cell79.Append(cellValue79);

            Cell cell80 = new Cell(){ CellReference = "H3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue80 = new CellValue();
            cellValue80.Text = "0";

            cell80.Append(cellValue80);

            Cell cell81 = new Cell(){ CellReference = "I3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue81 = new CellValue();
            cellValue81.Text = "102";

            cell81.Append(cellValue81);

            Cell cell82 = new Cell(){ CellReference = "J3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue82 = new CellValue();
            cellValue82.Text = "7000";

            cell82.Append(cellValue82);

            Cell cell83 = new Cell(){ CellReference = "K3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue83 = new CellValue();
            cellValue83.Text = "0.34";

            cell83.Append(cellValue83);

            Cell cell84 = new Cell(){ CellReference = "L3", StyleIndex = (UInt32Value)10U };
            CellValue cellValue84 = new CellValue();
            cellValue84.Text = "0.59";

            cell84.Append(cellValue84);

            Cell cell85 = new Cell(){ CellReference = "M3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue85 = new CellValue();
            cellValue85.Text = "100";

            cell85.Append(cellValue85);

            Cell cell86 = new Cell(){ CellReference = "N3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue86 = new CellValue();
            cellValue86.Text = "0";

            cell86.Append(cellValue86);

            Cell cell87 = new Cell(){ CellReference = "O3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue87 = new CellValue();
            cellValue87.Text = "0";

            cell87.Append(cellValue87);

            Cell cell88 = new Cell(){ CellReference = "P3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue88 = new CellValue();
            cellValue88.Text = "0";

            cell88.Append(cellValue88);

            Cell cell89 = new Cell(){ CellReference = "Q3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue89 = new CellValue();
            cellValue89.Text = "155";

            cell89.Append(cellValue89);

            Cell cell90 = new Cell(){ CellReference = "R3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue90 = new CellValue();
            cellValue90.Text = "0";

            cell90.Append(cellValue90);

            Cell cell91 = new Cell(){ CellReference = "S3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue91 = new CellValue();
            cellValue91.Text = "0";

            cell91.Append(cellValue91);

            Cell cell92 = new Cell(){ CellReference = "T3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue92 = new CellValue();
            cellValue92.Text = "41.87";

            cell92.Append(cellValue92);

            Cell cell93 = new Cell(){ CellReference = "U3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue93 = new CellValue();
            cellValue93.Text = "41.87";

            cell93.Append(cellValue93);

            Cell cell94 = new Cell(){ CellReference = "V3", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue94 = new CellValue();
            cellValue94.Text = "28";

            cell94.Append(cellValue94);

            Cell cell95 = new Cell(){ CellReference = "W3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue95 = new CellValue();
            cellValue95.Text = "1";

            cell95.Append(cellValue95);

            Cell cell96 = new Cell(){ CellReference = "X3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue96 = new CellValue();
            cellValue96.Text = "0";

            cell96.Append(cellValue96);

            Cell cell97 = new Cell(){ CellReference = "Y3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue97 = new CellValue();
            cellValue97.Text = "0";

            cell97.Append(cellValue97);

            Cell cell98 = new Cell(){ CellReference = "Z3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue98 = new CellValue();
            cellValue98.Text = "0";

            cell98.Append(cellValue98);

            Cell cell99 = new Cell(){ CellReference = "AA3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue99 = new CellValue();
            cellValue99.Text = "0";

            cell99.Append(cellValue99);

            Cell cell100 = new Cell(){ CellReference = "AB3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue100 = new CellValue();
            cellValue100.Text = "0";

            cell100.Append(cellValue100);

            Cell cell101 = new Cell(){ CellReference = "AC3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue101 = new CellValue();
            cellValue101.Text = "0";

            cell101.Append(cellValue101);

            Cell cell102 = new Cell(){ CellReference = "AD3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue102 = new CellValue();
            cellValue102.Text = "0";

            cell102.Append(cellValue102);

            Cell cell103 = new Cell(){ CellReference = "AE3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue103 = new CellValue();
            cellValue103.Text = "0";

            cell103.Append(cellValue103);

            Cell cell104 = new Cell(){ CellReference = "AF3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue104 = new CellValue();
            cellValue104.Text = "0";

            cell104.Append(cellValue104);

            Cell cell105 = new Cell(){ CellReference = "AG3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue105 = new CellValue();
            cellValue105.Text = "0";

            cell105.Append(cellValue105);

            Cell cell106 = new Cell(){ CellReference = "AH3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue106 = new CellValue();
            cellValue106.Text = "0";

            cell106.Append(cellValue106);

            Cell cell107 = new Cell(){ CellReference = "AI3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue107 = new CellValue();
            cellValue107.Text = "0";

            cell107.Append(cellValue107);

            Cell cell108 = new Cell(){ CellReference = "AJ3", StyleIndex = (UInt32Value)3U };
            CellValue cellValue108 = new CellValue();
            cellValue108.Text = "0";

            cell108.Append(cellValue108);

            row3.Append(cell73);
            row3.Append(cell74);
            row3.Append(cell75);
            row3.Append(cell76);
            row3.Append(cell77);
            row3.Append(cell78);
            row3.Append(cell79);
            row3.Append(cell80);
            row3.Append(cell81);
            row3.Append(cell82);
            row3.Append(cell83);
            row3.Append(cell84);
            row3.Append(cell85);
            row3.Append(cell86);
            row3.Append(cell87);
            row3.Append(cell88);
            row3.Append(cell89);
            row3.Append(cell90);
            row3.Append(cell91);
            row3.Append(cell92);
            row3.Append(cell93);
            row3.Append(cell94);
            row3.Append(cell95);
            row3.Append(cell96);
            row3.Append(cell97);
            row3.Append(cell98);
            row3.Append(cell99);
            row3.Append(cell100);
            row3.Append(cell101);
            row3.Append(cell102);
            row3.Append(cell103);
            row3.Append(cell104);
            row3.Append(cell105);
            row3.Append(cell106);
            row3.Append(cell107);
            row3.Append(cell108);

            Row row4 = new Row(){ RowIndex = (UInt32Value)4U, Spans = new ListValue<StringValue>() { InnerText = "1:36" }, DyDescent = 0.25D };

            Cell cell109 = new Cell(){ CellReference = "A4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue109 = new CellValue();
            cellValue109.Text = "45";

            cell109.Append(cellValue109);

            Cell cell110 = new Cell(){ CellReference = "B4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue110 = new CellValue();
            cellValue110.Text = "46";

            cell110.Append(cellValue110);

            Cell cell111 = new Cell(){ CellReference = "C4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue111 = new CellValue();
            cellValue111.Text = "5";

            cell111.Append(cellValue111);

            Cell cell112 = new Cell(){ CellReference = "D4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue112 = new CellValue();
            cellValue112.Text = "7";

            cell112.Append(cellValue112);

            Cell cell113 = new Cell(){ CellReference = "E4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue113 = new CellValue();
            cellValue113.Text = "9";

            cell113.Append(cellValue113);

            Cell cell114 = new Cell(){ CellReference = "F4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue114 = new CellValue();
            cellValue114.Text = "11";

            cell114.Append(cellValue114);

            Cell cell115 = new Cell(){ CellReference = "G4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue115 = new CellValue();
            cellValue115.Text = "283";

            cell115.Append(cellValue115);

            Cell cell116 = new Cell(){ CellReference = "H4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue116 = new CellValue();
            cellValue116.Text = "217.77";

            cell116.Append(cellValue116);

            Cell cell117 = new Cell(){ CellReference = "I4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue117 = new CellValue();
            cellValue117.Text = "260";

            cell117.Append(cellValue117);

            Cell cell118 = new Cell(){ CellReference = "J4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue118 = new CellValue();
            cellValue118.Text = "805";

            cell118.Append(cellValue118);

            Cell cell119 = new Cell(){ CellReference = "K4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue119 = new CellValue();
            cellValue119.Text = "0.08";

            cell119.Append(cellValue119);

            Cell cell120 = new Cell(){ CellReference = "L4", StyleIndex = (UInt32Value)10U };
            CellValue cellValue120 = new CellValue();
            cellValue120.Text = "0.89";

            cell120.Append(cellValue120);

            Cell cell121 = new Cell(){ CellReference = "M4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue121 = new CellValue();
            cellValue121.Text = "265";

            cell121.Append(cellValue121);

            Cell cell122 = new Cell(){ CellReference = "N4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue122 = new CellValue();
            cellValue122.Text = "0";

            cell122.Append(cellValue122);

            Cell cell123 = new Cell(){ CellReference = "O4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue123 = new CellValue();
            cellValue123.Text = "236.66";

            cell123.Append(cellValue123);

            Cell cell124 = new Cell(){ CellReference = "P4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue124 = new CellValue();
            cellValue124.Text = "0";

            cell124.Append(cellValue124);

            Cell cell125 = new Cell(){ CellReference = "Q4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue125 = new CellValue();
            cellValue125.Text = "217.77";

            cell125.Append(cellValue125);

            Cell cell126 = new Cell(){ CellReference = "R4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue126 = new CellValue();
            cellValue126.Text = "218.4529";

            cell126.Append(cellValue126);

            Cell cell127 = new Cell(){ CellReference = "S4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue127 = new CellValue();
            cellValue127.Text = "218.73820000000001";

            cell127.Append(cellValue127);

            Cell cell128 = new Cell(){ CellReference = "T4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue128 = new CellValue();
            cellValue128.Text = "28.68";

            cell128.Append(cellValue128);

            Cell cell129 = new Cell(){ CellReference = "U4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue129 = new CellValue();
            cellValue129.Text = "28.68";

            cell129.Append(cellValue129);

            Cell cell130 = new Cell(){ CellReference = "V4", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue130 = new CellValue();
            cellValue130.Text = "28";

            cell130.Append(cellValue130);

            Cell cell131 = new Cell(){ CellReference = "W4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue131 = new CellValue();
            cellValue131.Text = "1";

            cell131.Append(cellValue131);

            Cell cell132 = new Cell(){ CellReference = "X4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue132 = new CellValue();
            cellValue132.Text = "0";

            cell132.Append(cellValue132);

            Cell cell133 = new Cell(){ CellReference = "Y4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue133 = new CellValue();
            cellValue133.Text = "178.35";

            cell133.Append(cellValue133);

            Cell cell134 = new Cell(){ CellReference = "Z4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue134 = new CellValue();
            cellValue134.Text = "233";

            cell134.Append(cellValue134);

            Cell cell135 = new Cell(){ CellReference = "AA4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue135 = new CellValue();
            cellValue135.Text = "228";

            cell135.Append(cellValue135);

            Cell cell136 = new Cell(){ CellReference = "AB4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue136 = new CellValue();
            cellValue136.Text = "240";

            cell136.Append(cellValue136);

            Cell cell137 = new Cell(){ CellReference = "AC4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue137 = new CellValue();
            cellValue137.Text = "236.66";

            cell137.Append(cellValue137);

            Cell cell138 = new Cell(){ CellReference = "AD4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue138 = new CellValue();
            cellValue138.Text = "174.98";

            cell138.Append(cellValue138);

            Cell cell139 = new Cell(){ CellReference = "AE4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue139 = new CellValue();
            cellValue139.Text = "0";

            cell139.Append(cellValue139);

            Cell cell140 = new Cell(){ CellReference = "AF4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue140 = new CellValue();
            cellValue140.Text = "0";

            cell140.Append(cellValue140);

            Cell cell141 = new Cell(){ CellReference = "AG4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue141 = new CellValue();
            cellValue141.Text = "0";

            cell141.Append(cellValue141);

            Cell cell142 = new Cell(){ CellReference = "AH4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue142 = new CellValue();
            cellValue142.Text = "0";

            cell142.Append(cellValue142);

            Cell cell143 = new Cell(){ CellReference = "AI4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue143 = new CellValue();
            cellValue143.Text = "0";

            cell143.Append(cellValue143);

            Cell cell144 = new Cell(){ CellReference = "AJ4", StyleIndex = (UInt32Value)3U };
            CellValue cellValue144 = new CellValue();
            cellValue144.Text = "0";

            cell144.Append(cellValue144);

            row4.Append(cell109);
            row4.Append(cell110);
            row4.Append(cell111);
            row4.Append(cell112);
            row4.Append(cell113);
            row4.Append(cell114);
            row4.Append(cell115);
            row4.Append(cell116);
            row4.Append(cell117);
            row4.Append(cell118);
            row4.Append(cell119);
            row4.Append(cell120);
            row4.Append(cell121);
            row4.Append(cell122);
            row4.Append(cell123);
            row4.Append(cell124);
            row4.Append(cell125);
            row4.Append(cell126);
            row4.Append(cell127);
            row4.Append(cell128);
            row4.Append(cell129);
            row4.Append(cell130);
            row4.Append(cell131);
            row4.Append(cell132);
            row4.Append(cell133);
            row4.Append(cell134);
            row4.Append(cell135);
            row4.Append(cell136);
            row4.Append(cell137);
            row4.Append(cell138);
            row4.Append(cell139);
            row4.Append(cell140);
            row4.Append(cell141);
            row4.Append(cell142);
            row4.Append(cell143);
            row4.Append(cell144);

            Row row5 = new Row(){ RowIndex = (UInt32Value)5U, Spans = new ListValue<StringValue>() { InnerText = "1:36" }, DyDescent = 0.25D };

            Cell cell145 = new Cell(){ CellReference = "A5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue145 = new CellValue();
            cellValue145.Text = "47";

            cell145.Append(cellValue145);

            Cell cell146 = new Cell(){ CellReference = "B5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue146 = new CellValue();
            cellValue146.Text = "48";

            cell146.Append(cellValue146);

            Cell cell147 = new Cell(){ CellReference = "C5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue147 = new CellValue();
            cellValue147.Text = "5";

            cell147.Append(cellValue147);

            Cell cell148 = new Cell(){ CellReference = "D5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue148 = new CellValue();
            cellValue148.Text = "7";

            cell148.Append(cellValue148);

            Cell cell149 = new Cell(){ CellReference = "E5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue149 = new CellValue();
            cellValue149.Text = "9";

            cell149.Append(cellValue149);

            Cell cell150 = new Cell(){ CellReference = "F5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue150 = new CellValue();
            cellValue150.Text = "11";

            cell150.Append(cellValue150);

            Cell cell151 = new Cell(){ CellReference = "G5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue151 = new CellValue();
            cellValue151.Text = "311";

            cell151.Append(cellValue151);

            Cell cell152 = new Cell(){ CellReference = "H5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue152 = new CellValue();
            cellValue152.Text = "239.11";

            cell152.Append(cellValue152);

            Cell cell153 = new Cell(){ CellReference = "I5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue153 = new CellValue();
            cellValue153.Text = "285";

            cell153.Append(cellValue153);

            Cell cell154 = new Cell(){ CellReference = "J5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue154 = new CellValue();
            cellValue154.Text = "3000";

            cell154.Append(cellValue154);

            Cell cell155 = new Cell(){ CellReference = "K5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue155 = new CellValue();
            cellValue155.Text = "0.08";

            cell155.Append(cellValue155);

            Cell cell156 = new Cell(){ CellReference = "L5", StyleIndex = (UInt32Value)10U };
            CellValue cellValue156 = new CellValue();
            cellValue156.Text = "0.84";

            cell156.Append(cellValue156);

            Cell cell157 = new Cell(){ CellReference = "M5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue157 = new CellValue();
            cellValue157.Text = "290";

            cell157.Append(cellValue157);

            Cell cell158 = new Cell(){ CellReference = "N5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue158 = new CellValue();
            cellValue158.Text = "0";

            cell158.Append(cellValue158);

            Cell cell159 = new Cell(){ CellReference = "O5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue159 = new CellValue();
            cellValue159.Text = "259.86";

            cell159.Append(cellValue159);

            Cell cell160 = new Cell(){ CellReference = "P5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue160 = new CellValue();
            cellValue160.Text = "0";

            cell160.Append(cellValue160);

            Cell cell161 = new Cell(){ CellReference = "Q5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue161 = new CellValue();
            cellValue161.Text = "239.11";

            cell161.Append(cellValue161);

            Cell cell162 = new Cell(){ CellReference = "R5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue162 = new CellValue();
            cellValue162.Text = "203.911";

            cell162.Append(cellValue162);

            Cell cell163 = new Cell(){ CellReference = "S5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue163 = new CellValue();
            cellValue163.Text = "238.935";

            cell163.Append(cellValue163);

            Cell cell164 = new Cell(){ CellReference = "T5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue164 = new CellValue();
            cellValue164.Text = "46.55";

            cell164.Append(cellValue164);

            Cell cell165 = new Cell(){ CellReference = "U5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue165 = new CellValue();
            cellValue165.Text = "46.55";

            cell165.Append(cellValue165);

            Cell cell166 = new Cell(){ CellReference = "V5", StyleIndex = (UInt32Value)1U, DataType = CellValues.SharedString };
            CellValue cellValue166 = new CellValue();
            cellValue166.Text = "28";

            cell166.Append(cellValue166);

            Cell cell167 = new Cell(){ CellReference = "W5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue167 = new CellValue();
            cellValue167.Text = "1";

            cell167.Append(cellValue167);

            Cell cell168 = new Cell(){ CellReference = "X5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue168 = new CellValue();
            cellValue168.Text = "0";

            cell168.Append(cellValue168);

            Cell cell169 = new Cell(){ CellReference = "Y5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue169 = new CellValue();
            cellValue169.Text = "189.88";

            cell169.Append(cellValue169);

            Cell cell170 = new Cell(){ CellReference = "Z5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue170 = new CellValue();
            cellValue170.Text = "256";

            cell170.Append(cellValue170);

            Cell cell171 = new Cell(){ CellReference = "AA5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue171 = new CellValue();
            cellValue171.Text = "250";

            cell171.Append(cellValue171);

            Cell cell172 = new Cell(){ CellReference = "AB5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue172 = new CellValue();
            cellValue172.Text = "264";

            cell172.Append(cellValue172);

            Cell cell173 = new Cell(){ CellReference = "AC5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue173 = new CellValue();
            cellValue173.Text = "259.86";

            cell173.Append(cellValue173);

            Cell cell174 = new Cell(){ CellReference = "AD5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue174 = new CellValue();
            cellValue174.Text = "169.29";

            cell174.Append(cellValue174);

            Cell cell175 = new Cell(){ CellReference = "AE5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue175 = new CellValue();
            cellValue175.Text = "0";

            cell175.Append(cellValue175);

            Cell cell176 = new Cell(){ CellReference = "AF5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue176 = new CellValue();
            cellValue176.Text = "0";

            cell176.Append(cellValue176);

            Cell cell177 = new Cell(){ CellReference = "AG5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue177 = new CellValue();
            cellValue177.Text = "0";

            cell177.Append(cellValue177);

            Cell cell178 = new Cell(){ CellReference = "AH5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue178 = new CellValue();
            cellValue178.Text = "0";

            cell178.Append(cellValue178);

            Cell cell179 = new Cell(){ CellReference = "AI5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue179 = new CellValue();
            cellValue179.Text = "0";

            cell179.Append(cellValue179);

            Cell cell180 = new Cell(){ CellReference = "AJ5", StyleIndex = (UInt32Value)3U };
            CellValue cellValue180 = new CellValue();
            cellValue180.Text = "0";

            cell180.Append(cellValue180);

            row5.Append(cell145);
            row5.Append(cell146);
            row5.Append(cell147);
            row5.Append(cell148);
            row5.Append(cell149);
            row5.Append(cell150);
            row5.Append(cell151);
            row5.Append(cell152);
            row5.Append(cell153);
            row5.Append(cell154);
            row5.Append(cell155);
            row5.Append(cell156);
            row5.Append(cell157);
            row5.Append(cell158);
            row5.Append(cell159);
            row5.Append(cell160);
            row5.Append(cell161);
            row5.Append(cell162);
            row5.Append(cell163);
            row5.Append(cell164);
            row5.Append(cell165);
            row5.Append(cell166);
            row5.Append(cell167);
            row5.Append(cell168);
            row5.Append(cell169);
            row5.Append(cell170);
            row5.Append(cell171);
            row5.Append(cell172);
            row5.Append(cell173);
            row5.Append(cell174);
            row5.Append(cell175);
            row5.Append(cell176);
            row5.Append(cell177);
            row5.Append(cell178);
            row5.Append(cell179);
            row5.Append(cell180);

            sheetData1.Append(row1);
            sheetData1.Append(row2);
            sheetData1.Append(row3);
            sheetData1.Append(row4);
            sheetData1.Append(row5);
            PageMargins pageMargins1 = new PageMargins(){ Left = 0.7D, Right = 0.7D, Top = 0.75D, Bottom = 0.75D, Header = 0.3D, Footer = 0.3D };
            PageSetup pageSetup1 = new PageSetup(){ Orientation = OrientationValues.Portrait, Id = "rId1" };

            worksheet1.Append(sheetDimension1);
            worksheet1.Append(sheetViews1);
            worksheet1.Append(sheetFormatProperties1);
            worksheet1.Append(columns1);
            worksheet1.Append(sheetData1);
            worksheet1.Append(pageMargins1);
            worksheet1.Append(pageSetup1);

            worksheetPart1.Worksheet = worksheet1;
        }

        // Generates content of spreadsheetPrinterSettingsPart1.
        private void GenerateSpreadsheetPrinterSettingsPart1Content(SpreadsheetPrinterSettingsPart spreadsheetPrinterSettingsPart1)
        {
            System.IO.Stream data = GetBinaryDataStream(spreadsheetPrinterSettingsPart1Data);
            spreadsheetPrinterSettingsPart1.FeedData(data);
            data.Close();
        }

        // Generates content of worksheetPart2.
        private void GenerateWorksheetPart2Content(WorksheetPart worksheetPart2)
        {
            Worksheet worksheet2 = new Worksheet(){ MCAttributes = new MarkupCompatibilityAttributes(){ Ignorable = "x14ac" }  };
            worksheet2.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            worksheet2.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            worksheet2.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");
            SheetDimension sheetDimension2 = new SheetDimension(){ Reference = "A1:E18" };

            SheetViews sheetViews2 = new SheetViews();

            SheetView sheetView2 = new SheetView(){ WorkbookViewId = (UInt32Value)0U };
            Selection selection1 = new Selection(){ ActiveCell = "D6", SequenceOfReferences = new ListValue<StringValue>() { InnerText = "D6:E8" } };

            sheetView2.Append(selection1);

            sheetViews2.Append(sheetView2);
            SheetFormatProperties sheetFormatProperties2 = new SheetFormatProperties(){ DefaultRowHeight = 15D, DyDescent = 0.25D };

            Columns columns2 = new Columns();
            Column column13 = new Column(){ Min = (UInt32Value)1U, Max = (UInt32Value)1U, Width = 25.7109375D, CustomWidth = true };
            Column column14 = new Column(){ Min = (UInt32Value)2U, Max = (UInt32Value)2U, Width = 50.7109375D, CustomWidth = true };
            Column column15 = new Column(){ Min = (UInt32Value)4U, Max = (UInt32Value)4U, Width = 12.7109375D, CustomWidth = true };
            Column column16 = new Column(){ Min = (UInt32Value)5U, Max = (UInt32Value)5U, Width = 30.7109375D, CustomWidth = true };

            columns2.Append(column13);
            columns2.Append(column14);
            columns2.Append(column15);
            columns2.Append(column16);

            SheetData sheetData2 = new SheetData();

            Row row6 = new Row(){ RowIndex = (UInt32Value)1U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell181 = new Cell(){ CellReference = "A1", StyleIndex = (UInt32Value)6U, DataType = CellValues.SharedString };
            CellValue cellValue181 = new CellValue();
            cellValue181.Text = "49";

            cell181.Append(cellValue181);
            Cell cell182 = new Cell(){ CellReference = "B1", StyleIndex = (UInt32Value)1U };

            Cell cell183 = new Cell(){ CellReference = "D1", StyleIndex = (UInt32Value)5U, DataType = CellValues.SharedString };
            CellValue cellValue182 = new CellValue();
            cellValue182.Text = "67";

            cell183.Append(cellValue182);

            row6.Append(cell181);
            row6.Append(cell182);
            row6.Append(cell183);

            Row row7 = new Row(){ RowIndex = (UInt32Value)2U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell184 = new Cell(){ CellReference = "A2", DataType = CellValues.SharedString };
            CellValue cellValue183 = new CellValue();
            cellValue183.Text = "50";

            cell184.Append(cellValue183);

            Cell cell185 = new Cell(){ CellReference = "D2", StyleIndex = (UInt32Value)6U, DataType = CellValues.SharedString };
            CellValue cellValue184 = new CellValue();
            cellValue184.Text = "68";

            cell185.Append(cellValue184);

            Cell cell186 = new Cell(){ CellReference = "E2", StyleIndex = (UInt32Value)6U, DataType = CellValues.SharedString };
            CellValue cellValue185 = new CellValue();
            cellValue185.Text = "69";

            cell186.Append(cellValue185);

            row7.Append(cell184);
            row7.Append(cell185);
            row7.Append(cell186);

            Row row8 = new Row(){ RowIndex = (UInt32Value)3U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell187 = new Cell(){ CellReference = "A3", DataType = CellValues.SharedString };
            CellValue cellValue186 = new CellValue();
            cellValue186.Text = "51";

            cell187.Append(cellValue186);
            Cell cell188 = new Cell(){ CellReference = "D3", StyleIndex = (UInt32Value)1U };

            row8.Append(cell187);
            row8.Append(cell188);

            Row row9 = new Row(){ RowIndex = (UInt32Value)4U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell189 = new Cell(){ CellReference = "A4", DataType = CellValues.SharedString };
            CellValue cellValue187 = new CellValue();
            cellValue187.Text = "52";

            cell189.Append(cellValue187);
            Cell cell190 = new Cell(){ CellReference = "B4", StyleIndex = (UInt32Value)7U };
            Cell cell191 = new Cell(){ CellReference = "D4", StyleIndex = (UInt32Value)1U };

            row9.Append(cell189);
            row9.Append(cell190);
            row9.Append(cell191);

            Row row10 = new Row(){ RowIndex = (UInt32Value)5U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell192 = new Cell(){ CellReference = "A5", DataType = CellValues.SharedString };
            CellValue cellValue188 = new CellValue();
            cellValue188.Text = "53";

            cell192.Append(cellValue188);
            Cell cell193 = new Cell(){ CellReference = "B5", StyleIndex = (UInt32Value)1U };

            row10.Append(cell192);
            row10.Append(cell193);

            Row row11 = new Row(){ RowIndex = (UInt32Value)6U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell194 = new Cell(){ CellReference = "A6", DataType = CellValues.SharedString };
            CellValue cellValue189 = new CellValue();
            cellValue189.Text = "54";

            cell194.Append(cellValue189);
            Cell cell195 = new Cell(){ CellReference = "D6", StyleIndex = (UInt32Value)5U };

            row11.Append(cell194);
            row11.Append(cell195);

            Row row12 = new Row(){ RowIndex = (UInt32Value)7U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell196 = new Cell(){ CellReference = "A7", DataType = CellValues.SharedString };
            CellValue cellValue190 = new CellValue();
            cellValue190.Text = "55";

            cell196.Append(cellValue190);
            Cell cell197 = new Cell(){ CellReference = "D7", StyleIndex = (UInt32Value)6U };
            Cell cell198 = new Cell(){ CellReference = "E7", StyleIndex = (UInt32Value)6U };

            row12.Append(cell196);
            row12.Append(cell197);
            row12.Append(cell198);

            Row row13 = new Row(){ RowIndex = (UInt32Value)8U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell199 = new Cell(){ CellReference = "A8", DataType = CellValues.SharedString };
            CellValue cellValue191 = new CellValue();
            cellValue191.Text = "56";

            cell199.Append(cellValue191);
            Cell cell200 = new Cell(){ CellReference = "D8", StyleIndex = (UInt32Value)1U };

            row13.Append(cell199);
            row13.Append(cell200);

            Row row14 = new Row(){ RowIndex = (UInt32Value)9U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell201 = new Cell(){ CellReference = "A9", DataType = CellValues.SharedString };
            CellValue cellValue192 = new CellValue();
            cellValue192.Text = "57";

            cell201.Append(cellValue192);

            row14.Append(cell201);

            Row row15 = new Row(){ RowIndex = (UInt32Value)10U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell202 = new Cell(){ CellReference = "A10", DataType = CellValues.SharedString };
            CellValue cellValue193 = new CellValue();
            cellValue193.Text = "58";

            cell202.Append(cellValue193);

            row15.Append(cell202);

            Row row16 = new Row(){ RowIndex = (UInt32Value)11U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell203 = new Cell(){ CellReference = "A11", DataType = CellValues.SharedString };
            CellValue cellValue194 = new CellValue();
            cellValue194.Text = "59";

            cell203.Append(cellValue194);

            row16.Append(cell203);

            Row row17 = new Row(){ RowIndex = (UInt32Value)12U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell204 = new Cell(){ CellReference = "A12", DataType = CellValues.SharedString };
            CellValue cellValue195 = new CellValue();
            cellValue195.Text = "60";

            cell204.Append(cellValue195);

            row17.Append(cell204);

            Row row18 = new Row(){ RowIndex = (UInt32Value)13U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell205 = new Cell(){ CellReference = "A13", DataType = CellValues.SharedString };
            CellValue cellValue196 = new CellValue();
            cellValue196.Text = "61";

            cell205.Append(cellValue196);

            row18.Append(cell205);

            Row row19 = new Row(){ RowIndex = (UInt32Value)14U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell206 = new Cell(){ CellReference = "A14", DataType = CellValues.SharedString };
            CellValue cellValue197 = new CellValue();
            cellValue197.Text = "62";

            cell206.Append(cellValue197);

            row19.Append(cell206);

            Row row20 = new Row(){ RowIndex = (UInt32Value)15U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell207 = new Cell(){ CellReference = "A15", DataType = CellValues.SharedString };
            CellValue cellValue198 = new CellValue();
            cellValue198.Text = "63";

            cell207.Append(cellValue198);

            row20.Append(cell207);

            Row row21 = new Row(){ RowIndex = (UInt32Value)16U, Spans = new ListValue<StringValue>() { InnerText = "1:5" }, DyDescent = 0.25D };

            Cell cell208 = new Cell(){ CellReference = "A16", DataType = CellValues.SharedString };
            CellValue cellValue199 = new CellValue();
            cellValue199.Text = "64";

            cell208.Append(cellValue199);
            Cell cell209 = new Cell(){ CellReference = "B16", StyleIndex = (UInt32Value)8U };

            row21.Append(cell208);
            row21.Append(cell209);

            Row row22 = new Row(){ RowIndex = (UInt32Value)17U, Spans = new ListValue<StringValue>() { InnerText = "1:1" }, DyDescent = 0.25D };

            Cell cell210 = new Cell(){ CellReference = "A17", DataType = CellValues.SharedString };
            CellValue cellValue200 = new CellValue();
            cellValue200.Text = "65";

            cell210.Append(cellValue200);

            row22.Append(cell210);

            Row row23 = new Row(){ RowIndex = (UInt32Value)18U, Spans = new ListValue<StringValue>() { InnerText = "1:1" }, DyDescent = 0.25D };

            Cell cell211 = new Cell(){ CellReference = "A18", DataType = CellValues.SharedString };
            CellValue cellValue201 = new CellValue();
            cellValue201.Text = "66";

            cell211.Append(cellValue201);

            row23.Append(cell211);

            sheetData2.Append(row6);
            sheetData2.Append(row7);
            sheetData2.Append(row8);
            sheetData2.Append(row9);
            sheetData2.Append(row10);
            sheetData2.Append(row11);
            sheetData2.Append(row12);
            sheetData2.Append(row13);
            sheetData2.Append(row14);
            sheetData2.Append(row15);
            sheetData2.Append(row16);
            sheetData2.Append(row17);
            sheetData2.Append(row18);
            sheetData2.Append(row19);
            sheetData2.Append(row20);
            sheetData2.Append(row21);
            sheetData2.Append(row22);
            sheetData2.Append(row23);
            PageMargins pageMargins2 = new PageMargins(){ Left = 0.7D, Right = 0.7D, Top = 0.75D, Bottom = 0.75D, Header = 0.3D, Footer = 0.3D };

            worksheet2.Append(sheetDimension2);
            worksheet2.Append(sheetViews2);
            worksheet2.Append(sheetFormatProperties2);
            worksheet2.Append(columns2);
            worksheet2.Append(sheetData2);
            worksheet2.Append(pageMargins2);

            worksheetPart2.Worksheet = worksheet2;
        }

        // Generates content of sharedStringTablePart1.
        private void GenerateSharedStringTablePart1Content(SharedStringTablePart sharedStringTablePart1)
        {
            SharedStringTable sharedStringTable1 = new SharedStringTable(){ Count = (UInt32Value)85U, UniqueCount = (UInt32Value)70U };

            SharedStringItem sharedStringItem1 = new SharedStringItem();
            Text text1 = new Text();
            text1.Text = "Material_Id";

            sharedStringItem1.Append(text1);

            SharedStringItem sharedStringItem2 = new SharedStringItem();
            Text text2 = new Text();
            text2.Text = "014725";

            sharedStringItem2.Append(text2);

            SharedStringItem sharedStringItem3 = new SharedStringItem();
            Text text3 = new Text();
            text3.Text = "Material_Desc";

            sharedStringItem3.Append(text3);

            SharedStringItem sharedStringItem4 = new SharedStringItem();
            Text text4 = new Text();
            text4.Text = "RAP-PAC LTX FREE";

            sharedStringItem4.Append(text4);

            SharedStringItem sharedStringItem5 = new SharedStringItem();
            Text text5 = new Text();
            text5.Text = "Marketing_Manager";

            sharedStringItem5.Append(text5);

            SharedStringItem sharedStringItem6 = new SharedStringItem();
            Text text6 = new Text();
            text6.Text = "";

            sharedStringItem6.Append(text6);

            SharedStringItem sharedStringItem7 = new SharedStringItem();
            Text text7 = new Text();
            text7.Text = "Level_4_Desc";

            sharedStringItem7.Append(text7);

            SharedStringItem sharedStringItem8 = new SharedStringItem();
            Text text8 = new Text();
            text8.Text = "Knee Consumables";

            sharedStringItem8.Append(text8);

            SharedStringItem sharedStringItem9 = new SharedStringItem();
            Text text9 = new Text();
            text9.Text = "Level_5_Desc";

            sharedStringItem9.Append(text9);

            SharedStringItem sharedStringItem10 = new SharedStringItem();
            Text text10 = new Text();
            text10.Text = "Ligament Consumables";

            sharedStringItem10.Append(text10);

            SharedStringItem sharedStringItem11 = new SharedStringItem();
            Text text11 = new Text();
            text11.Text = "Level_6_Desc";

            sharedStringItem11.Append(text11);

            SharedStringItem sharedStringItem12 = new SharedStringItem();
            Text text12 = new Text();
            text12.Text = "1.5 RAP - PAC";

            sharedStringItem12.Append(text12);

            SharedStringItem sharedStringItem13 = new SharedStringItem();
            Text text13 = new Text();
            text13.Text = "List_Price";

            sharedStringItem13.Append(text13);

            SharedStringItem sharedStringItem14 = new SharedStringItem();
            Text text14 = new Text();
            text14.Text = "Local_Price";

            sharedStringItem14.Append(text14);

            SharedStringItem sharedStringItem15 = new SharedStringItem();
            Text text15 = new Text();
            text15.Text = "Requested_Price";

            sharedStringItem15.Append(text15);

            SharedStringItem sharedStringItem16 = new SharedStringItem();
            Text text16 = new Text();
            text16.Text = "Expected_Volume";

            sharedStringItem16.Append(text16);

            SharedStringItem sharedStringItem17 = new SharedStringItem();
            Text text17 = new Text();
            text17.Text = "Requested_Discount";

            sharedStringItem17.Append(text17);

            SharedStringItem sharedStringItem18 = new SharedStringItem();
            Text text18 = new Text();
            text18.Text = "Requested_GM";

            sharedStringItem18.Append(text18);

            SharedStringItem sharedStringItem19 = new SharedStringItem();
            Text text19 = new Text();
            text19.Text = "Competitor_Price";

            sharedStringItem19.Append(text19);

            SharedStringItem sharedStringItem20 = new SharedStringItem();
            Text text20 = new Text();
            text20.Text = "Rolling_Revenue";

            sharedStringItem20.Append(text20);

            SharedStringItem sharedStringItem21 = new SharedStringItem();
            Text text21 = new Text();
            text21.Text = "GPO_Price";

            sharedStringItem21.Append(text21);

            SharedStringItem sharedStringItem22 = new SharedStringItem();
            Text text22 = new Text();
            text22.Text = "CG1_Price";

            sharedStringItem22.Append(text22);

            SharedStringItem sharedStringItem23 = new SharedStringItem();
            Text text23 = new Text();
            text23.Text = "Net_Price";

            sharedStringItem23.Append(text23);

            SharedStringItem sharedStringItem24 = new SharedStringItem();
            Text text24 = new Text();
            text24.Text = "State_ASP";

            sharedStringItem24.Append(text24);

            SharedStringItem sharedStringItem25 = new SharedStringItem();
            Text text25 = new Text();
            text25.Text = "US_ASP";

            sharedStringItem25.Append(text25);

            SharedStringItem sharedStringItem26 = new SharedStringItem();
            Text text26 = new Text();
            text26.Text = "Standard_Cost";

            sharedStringItem26.Append(text26);

            SharedStringItem sharedStringItem27 = new SharedStringItem();
            Text text27 = new Text();
            text27.Text = "COG";

            sharedStringItem27.Append(text27);

            SharedStringItem sharedStringItem28 = new SharedStringItem();
            Text text28 = new Text();
            text28.Text = "Sellable_UOM";

            sharedStringItem28.Append(text28);

            SharedStringItem sharedStringItem29 = new SharedStringItem();
            Text text29 = new Text();
            text29.Text = "EA";

            sharedStringItem29.Append(text29);

            SharedStringItem sharedStringItem30 = new SharedStringItem();
            Text text30 = new Text();
            text30.Text = "Qty_Per_Sellable_UOM";

            sharedStringItem30.Append(text30);

            SharedStringItem sharedStringItem31 = new SharedStringItem();
            Text text31 = new Text();
            text31.Text = "Internal_Floor_Price";

            sharedStringItem31.Append(text31);

            SharedStringItem sharedStringItem32 = new SharedStringItem();
            Text text32 = new Text();
            text32.Text = "HPG_Price";

            sharedStringItem32.Append(text32);

            SharedStringItem sharedStringItem33 = new SharedStringItem();
            Text text33 = new Text();
            text33.Text = "MedAssets_T1_Price";

            sharedStringItem33.Append(text33);

            SharedStringItem sharedStringItem34 = new SharedStringItem();
            Text text34 = new Text();
            text34.Text = "MedAssets_T2_Price";

            sharedStringItem34.Append(text34);

            SharedStringItem sharedStringItem35 = new SharedStringItem();
            Text text35 = new Text();
            text35.Text = "Premier_Price";

            sharedStringItem35.Append(text35);

            SharedStringItem sharedStringItem36 = new SharedStringItem();
            Text text36 = new Text();
            text36.Text = "Novation_Participant_Price";

            sharedStringItem36.Append(text36);

            SharedStringItem sharedStringItem37 = new SharedStringItem();
            Text text37 = new Text();
            text37.Text = "Novation_Committed_Price";

            sharedStringItem37.Append(text37);

            SharedStringItem sharedStringItem38 = new SharedStringItem();
            Text text38 = new Text();
            text38.Text = "Rep_Discount";

            sharedStringItem38.Append(text38);

            SharedStringItem sharedStringItem39 = new SharedStringItem();
            Text text39 = new Text();
            text39.Text = "Rep_Price";

            sharedStringItem39.Append(text39);

            SharedStringItem sharedStringItem40 = new SharedStringItem();
            Text text40 = new Text();
            text40.Text = "DE_Discount";

            sharedStringItem40.Append(text40);

            SharedStringItem sharedStringItem41 = new SharedStringItem();
            Text text41 = new Text();
            text41.Text = "DE_Price";

            sharedStringItem41.Append(text41);

            SharedStringItem sharedStringItem42 = new SharedStringItem();
            Text text42 = new Text();
            text42.Text = "RVP_Discount";

            sharedStringItem42.Append(text42);

            SharedStringItem sharedStringItem43 = new SharedStringItem();
            Text text43 = new Text();
            text43.Text = "RVP_Price";

            sharedStringItem43.Append(text43);

            SharedStringItem sharedStringItem44 = new SharedStringItem();
            Text text44 = new Text();
            text44.Text = "014725D";

            sharedStringItem44.Append(text44);

            SharedStringItem sharedStringItem45 = new SharedStringItem();
            Text text45 = new Text();
            text45.Text = "DEMO,RAP-PAC ACL ACCESSORY KIT";

            sharedStringItem45.Append(text45);

            SharedStringItem sharedStringItem46 = new SharedStringItem();
            Text text46 = new Text();
            text46.Text = "7209217";

            sharedStringItem46.Append(text46);

            SharedStringItem sharedStringItem47 = new SharedStringItem();
            Text text47 = new Text();
            text47.Text = "RAP-PAC 1.5 LTX FREE";

            sharedStringItem47.Append(text47);

            SharedStringItem sharedStringItem48 = new SharedStringItem();
            Text text48 = new Text();
            text48.Text = "7210151";

            sharedStringItem48.Append(text48);

            SharedStringItem sharedStringItem49 = new SharedStringItem();
            Text text49 = new Text();
            text49.Text = "RAP PAC LTX";

            sharedStringItem49.Append(text49);

            SharedStringItem sharedStringItem50 = new SharedStringItem();
            Text text50 = new Text();
            text50.Text = "Price Request ID:";

            sharedStringItem50.Append(text50);

            SharedStringItem sharedStringItem51 = new SharedStringItem();
            Text text51 = new Text();
            text51.Text = "Status:";

            sharedStringItem51.Append(text51);

            SharedStringItem sharedStringItem52 = new SharedStringItem();
            Text text52 = new Text();
            text52.Text = "Submitted By:";

            sharedStringItem52.Append(text52);

            SharedStringItem sharedStringItem53 = new SharedStringItem();
            Text text53 = new Text();
            text53.Text = "Submitted Date:";

            sharedStringItem53.Append(text53);

            SharedStringItem sharedStringItem54 = new SharedStringItem();
            Text text54 = new Text();
            text54.Text = "Customer Sold To Number:";

            sharedStringItem54.Append(text54);

            SharedStringItem sharedStringItem55 = new SharedStringItem();
            Text text55 = new Text();
            text55.Text = "Sales Rep:";

            sharedStringItem55.Append(text55);

            SharedStringItem sharedStringItem56 = new SharedStringItem();
            Text text56 = new Text();
            text56.Text = "RVP:";

            sharedStringItem56.Append(text56);

            SharedStringItem sharedStringItem57 = new SharedStringItem();
            Text text57 = new Text();
            text57.Text = "DE:";

            sharedStringItem57.Append(text57);

            SharedStringItem sharedStringItem58 = new SharedStringItem();
            Text text58 = new Text();
            text58.Text = "Customer Name:";

            sharedStringItem58.Append(text58);

            SharedStringItem sharedStringItem59 = new SharedStringItem();
            Text text59 = new Text();
            text59.Text = "Customer Address:";

            sharedStringItem59.Append(text59);

            SharedStringItem sharedStringItem60 = new SharedStringItem();
            Text text60 = new Text();
            text60.Text = "Customer City, State, Zip:";

            sharedStringItem60.Append(text60);

            SharedStringItem sharedStringItem61 = new SharedStringItem();
            Text text61 = new Text();
            text61.Text = "GPO:";

            sharedStringItem61.Append(text61);

            SharedStringItem sharedStringItem62 = new SharedStringItem();
            Text text62 = new Text();
            text62.Text = "Customer Group1:";

            sharedStringItem62.Append(text62);

            SharedStringItem sharedStringItem63 = new SharedStringItem();
            Text text63 = new Text(){ Space = SpaceProcessingModeValues.Preserve };
            text63.Text = "Agreement Term: ";

            sharedStringItem63.Append(text63);

            SharedStringItem sharedStringItem64 = new SharedStringItem();
            Text text64 = new Text(){ Space = SpaceProcessingModeValues.Preserve };
            text64.Text = "Competitor: ";

            sharedStringItem64.Append(text64);

            SharedStringItem sharedStringItem65 = new SharedStringItem();
            Text text65 = new Text(){ Space = SpaceProcessingModeValues.Preserve };
            text65.Text = "Business Opportunities, Justification, and Strategic Rationale for Price: ";

            sharedStringItem65.Append(text65);

            SharedStringItem sharedStringItem66 = new SharedStringItem();
            Text text66 = new Text(){ Space = SpaceProcessingModeValues.Preserve };
            text66.Text = "What is estimated 12 month incremental revenue for products on request? ";

            sharedStringItem66.Append(text66);

            SharedStringItem sharedStringItem67 = new SharedStringItem();
            Text text67 = new Text(){ Space = SpaceProcessingModeValues.Preserve };
            text67.Text = "What is the probability of incremental revenue for  products on request (%)? ";

            sharedStringItem67.Append(text67);

            SharedStringItem sharedStringItem68 = new SharedStringItem();
            Text text68 = new Text();
            text68.Text = "Additional Acct# \'s that are part of this deal";

            sharedStringItem68.Append(text68);

            SharedStringItem sharedStringItem69 = new SharedStringItem();
            Text text69 = new Text();
            text69.Text = "Customer ID";

            sharedStringItem69.Append(text69);

            SharedStringItem sharedStringItem70 = new SharedStringItem();
            Text text70 = new Text();
            text70.Text = "Customer Name";

            sharedStringItem70.Append(text70);

            sharedStringTable1.Append(sharedStringItem1);
            sharedStringTable1.Append(sharedStringItem2);
            sharedStringTable1.Append(sharedStringItem3);
            sharedStringTable1.Append(sharedStringItem4);
            sharedStringTable1.Append(sharedStringItem5);
            sharedStringTable1.Append(sharedStringItem6);
            sharedStringTable1.Append(sharedStringItem7);
            sharedStringTable1.Append(sharedStringItem8);
            sharedStringTable1.Append(sharedStringItem9);
            sharedStringTable1.Append(sharedStringItem10);
            sharedStringTable1.Append(sharedStringItem11);
            sharedStringTable1.Append(sharedStringItem12);
            sharedStringTable1.Append(sharedStringItem13);
            sharedStringTable1.Append(sharedStringItem14);
            sharedStringTable1.Append(sharedStringItem15);
            sharedStringTable1.Append(sharedStringItem16);
            sharedStringTable1.Append(sharedStringItem17);
            sharedStringTable1.Append(sharedStringItem18);
            sharedStringTable1.Append(sharedStringItem19);
            sharedStringTable1.Append(sharedStringItem20);
            sharedStringTable1.Append(sharedStringItem21);
            sharedStringTable1.Append(sharedStringItem22);
            sharedStringTable1.Append(sharedStringItem23);
            sharedStringTable1.Append(sharedStringItem24);
            sharedStringTable1.Append(sharedStringItem25);
            sharedStringTable1.Append(sharedStringItem26);
            sharedStringTable1.Append(sharedStringItem27);
            sharedStringTable1.Append(sharedStringItem28);
            sharedStringTable1.Append(sharedStringItem29);
            sharedStringTable1.Append(sharedStringItem30);
            sharedStringTable1.Append(sharedStringItem31);
            sharedStringTable1.Append(sharedStringItem32);
            sharedStringTable1.Append(sharedStringItem33);
            sharedStringTable1.Append(sharedStringItem34);
            sharedStringTable1.Append(sharedStringItem35);
            sharedStringTable1.Append(sharedStringItem36);
            sharedStringTable1.Append(sharedStringItem37);
            sharedStringTable1.Append(sharedStringItem38);
            sharedStringTable1.Append(sharedStringItem39);
            sharedStringTable1.Append(sharedStringItem40);
            sharedStringTable1.Append(sharedStringItem41);
            sharedStringTable1.Append(sharedStringItem42);
            sharedStringTable1.Append(sharedStringItem43);
            sharedStringTable1.Append(sharedStringItem44);
            sharedStringTable1.Append(sharedStringItem45);
            sharedStringTable1.Append(sharedStringItem46);
            sharedStringTable1.Append(sharedStringItem47);
            sharedStringTable1.Append(sharedStringItem48);
            sharedStringTable1.Append(sharedStringItem49);
            sharedStringTable1.Append(sharedStringItem50);
            sharedStringTable1.Append(sharedStringItem51);
            sharedStringTable1.Append(sharedStringItem52);
            sharedStringTable1.Append(sharedStringItem53);
            sharedStringTable1.Append(sharedStringItem54);
            sharedStringTable1.Append(sharedStringItem55);
            sharedStringTable1.Append(sharedStringItem56);
            sharedStringTable1.Append(sharedStringItem57);
            sharedStringTable1.Append(sharedStringItem58);
            sharedStringTable1.Append(sharedStringItem59);
            sharedStringTable1.Append(sharedStringItem60);
            sharedStringTable1.Append(sharedStringItem61);
            sharedStringTable1.Append(sharedStringItem62);
            sharedStringTable1.Append(sharedStringItem63);
            sharedStringTable1.Append(sharedStringItem64);
            sharedStringTable1.Append(sharedStringItem65);
            sharedStringTable1.Append(sharedStringItem66);
            sharedStringTable1.Append(sharedStringItem67);
            sharedStringTable1.Append(sharedStringItem68);
            sharedStringTable1.Append(sharedStringItem69);
            sharedStringTable1.Append(sharedStringItem70);

            sharedStringTablePart1.SharedStringTable = sharedStringTable1;
        }

        // Generates content of workbookStylesPart1.
        private void GenerateWorkbookStylesPart1Content(WorkbookStylesPart workbookStylesPart1)
        {
            Stylesheet stylesheet1 = new Stylesheet(){ MCAttributes = new MarkupCompatibilityAttributes(){ Ignorable = "x14ac" }  };
            stylesheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            stylesheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");

            Fonts fonts1 = new Fonts(){ Count = (UInt32Value)3U, KnownFonts = true };

            Font font1 = new Font();
            FontSize fontSize1 = new FontSize(){ Val = 11D };
            Color color1 = new Color(){ Theme = (UInt32Value)1U };
            FontName fontName1 = new FontName(){ Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering1 = new FontFamilyNumbering(){ Val = 2 };
            FontScheme fontScheme2 = new FontScheme(){ Val = FontSchemeValues.Minor };

            font1.Append(fontSize1);
            font1.Append(color1);
            font1.Append(fontName1);
            font1.Append(fontFamilyNumbering1);
            font1.Append(fontScheme2);

            Font font2 = new Font();
            Bold bold1 = new Bold();
            FontSize fontSize2 = new FontSize(){ Val = 11D };
            Color color2 = new Color(){ Theme = (UInt32Value)1U };
            FontName fontName2 = new FontName(){ Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering2 = new FontFamilyNumbering(){ Val = 2 };
            FontScheme fontScheme3 = new FontScheme(){ Val = FontSchemeValues.Minor };

            font2.Append(bold1);
            font2.Append(fontSize2);
            font2.Append(color2);
            font2.Append(fontName2);
            font2.Append(fontFamilyNumbering2);
            font2.Append(fontScheme3);

            Font font3 = new Font();
            FontSize fontSize3 = new FontSize(){ Val = 11D };
            Color color3 = new Color(){ Theme = (UInt32Value)1U };
            FontName fontName3 = new FontName(){ Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering3 = new FontFamilyNumbering(){ Val = 2 };
            FontScheme fontScheme4 = new FontScheme(){ Val = FontSchemeValues.Minor };

            font3.Append(fontSize3);
            font3.Append(color3);
            font3.Append(fontName3);
            font3.Append(fontFamilyNumbering3);
            font3.Append(fontScheme4);

            fonts1.Append(font1);
            fonts1.Append(font2);
            fonts1.Append(font3);

            Fills fills1 = new Fills(){ Count = (UInt32Value)3U };

            Fill fill1 = new Fill();
            PatternFill patternFill1 = new PatternFill(){ PatternType = PatternValues.None };

            fill1.Append(patternFill1);

            Fill fill2 = new Fill();
            PatternFill patternFill2 = new PatternFill(){ PatternType = PatternValues.Gray125 };

            fill2.Append(patternFill2);

            Fill fill3 = new Fill();

            PatternFill patternFill3 = new PatternFill(){ PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor1 = new ForegroundColor(){ Indexed = (UInt32Value)22U };
            BackgroundColor backgroundColor1 = new BackgroundColor(){ Indexed = (UInt32Value)64U };

            patternFill3.Append(foregroundColor1);
            patternFill3.Append(backgroundColor1);

            fill3.Append(patternFill3);

            fills1.Append(fill1);
            fills1.Append(fill2);
            fills1.Append(fill3);

            Borders borders1 = new Borders(){ Count = (UInt32Value)1U };

            Border border1 = new Border();
            LeftBorder leftBorder1 = new LeftBorder();
            RightBorder rightBorder1 = new RightBorder();
            TopBorder topBorder1 = new TopBorder();
            BottomBorder bottomBorder1 = new BottomBorder();
            DiagonalBorder diagonalBorder1 = new DiagonalBorder();

            border1.Append(leftBorder1);
            border1.Append(rightBorder1);
            border1.Append(topBorder1);
            border1.Append(bottomBorder1);
            border1.Append(diagonalBorder1);

            borders1.Append(border1);

            CellStyleFormats cellStyleFormats1 = new CellStyleFormats(){ Count = (UInt32Value)2U };
            CellFormat cellFormat1 = new CellFormat(){ NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            CellFormat cellFormat2 = new CellFormat(){ NumberFormatId = (UInt32Value)9U, FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, ApplyFont = false, ApplyFill = false, ApplyBorder = false, ApplyAlignment = false, ApplyProtection = false };

            cellStyleFormats1.Append(cellFormat1);
            cellStyleFormats1.Append(cellFormat2);

            CellFormats cellFormats1 = new CellFormats(){ Count = (UInt32Value)11U };
            CellFormat cellFormat3 = new CellFormat(){ NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat4 = new CellFormat(){ NumberFormatId = (UInt32Value)49U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true };
            CellFormat cellFormat5 = new CellFormat(){ NumberFormatId = (UInt32Value)49U, FontId = (UInt32Value)1U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyFill = true };
            CellFormat cellFormat6 = new CellFormat(){ NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true };
            CellFormat cellFormat7 = new CellFormat(){ NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)1U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyFill = true };
            CellFormat cellFormat8 = new CellFormat(){ NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true };
            CellFormat cellFormat9 = new CellFormat(){ NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyFill = true };

            CellFormat cellFormat10 = new CellFormat(){ NumberFormatId = (UInt32Value)22U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyAlignment = true };
            Alignment alignment1 = new Alignment(){ Horizontal = HorizontalAlignmentValues.Left };

            cellFormat10.Append(alignment1);

            CellFormat cellFormat11 = new CellFormat(){ NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyAlignment = true };
            Alignment alignment2 = new Alignment(){ WrapText = true };

            cellFormat11.Append(alignment2);
            CellFormat cellFormat12 = new CellFormat(){ NumberFormatId = (UInt32Value)9U, FontId = (UInt32Value)1U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)1U, ApplyFont = true, ApplyFill = true };
            CellFormat cellFormat13 = new CellFormat(){ NumberFormatId = (UInt32Value)9U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)1U, ApplyFont = true };

            cellFormats1.Append(cellFormat3);
            cellFormats1.Append(cellFormat4);
            cellFormats1.Append(cellFormat5);
            cellFormats1.Append(cellFormat6);
            cellFormats1.Append(cellFormat7);
            cellFormats1.Append(cellFormat8);
            cellFormats1.Append(cellFormat9);
            cellFormats1.Append(cellFormat10);
            cellFormats1.Append(cellFormat11);
            cellFormats1.Append(cellFormat12);
            cellFormats1.Append(cellFormat13);

            CellStyles cellStyles1 = new CellStyles(){ Count = (UInt32Value)2U };
            CellStyle cellStyle1 = new CellStyle(){ Name = "Normal", FormatId = (UInt32Value)0U, BuiltinId = (UInt32Value)0U };
            CellStyle cellStyle2 = new CellStyle(){ Name = "Percent", FormatId = (UInt32Value)1U, BuiltinId = (UInt32Value)5U };

            cellStyles1.Append(cellStyle1);
            cellStyles1.Append(cellStyle2);
            DifferentialFormats differentialFormats1 = new DifferentialFormats(){ Count = (UInt32Value)0U };
            TableStyles tableStyles1 = new TableStyles(){ Count = (UInt32Value)0U, DefaultTableStyle = "TableStyleMedium2", DefaultPivotStyle = "PivotStyleLight16" };

            StylesheetExtensionList stylesheetExtensionList1 = new StylesheetExtensionList();

            StylesheetExtension stylesheetExtension1 = new StylesheetExtension(){ Uri = "{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" };
            stylesheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");
            X14.SlicerStyles slicerStyles1 = new X14.SlicerStyles(){ DefaultSlicerStyle = "SlicerStyleLight1" };

            stylesheetExtension1.Append(slicerStyles1);

            stylesheetExtensionList1.Append(stylesheetExtension1);

            stylesheet1.Append(fonts1);
            stylesheet1.Append(fills1);
            stylesheet1.Append(borders1);
            stylesheet1.Append(cellStyleFormats1);
            stylesheet1.Append(cellFormats1);
            stylesheet1.Append(cellStyles1);
            stylesheet1.Append(differentialFormats1);
            stylesheet1.Append(tableStyles1);
            stylesheet1.Append(stylesheetExtensionList1);

            workbookStylesPart1.Stylesheet = stylesheet1;
        }

        private void SetPackageProperties(OpenXmlPackage document)
        {
            document.PackageProperties.Creator = "Smith and Nephew Users";
            document.PackageProperties.Created = System.Xml.XmlConvert.ToDateTime("2013-11-04T15:04:06Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            document.PackageProperties.Modified = System.Xml.XmlConvert.ToDateTime("2013-11-04T17:21:27Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            document.PackageProperties.LastModifiedBy = "Smith and Nephew Users";
        }

        #region Binary Data
        private string spreadsheetPrinterSettingsPart1Data = "SABQACAAUABoAG8AdABvAHMAbQBhAHIAdAAgAFAAbAB1AHMAIABCADIAMAA5AGEALQBtAAAAAAAAAAAAAAAAAAEEAAbcAAAeQ/+ABwEAAQDqCm8IZAABAA8AWAICAAEAWAICAAEATABlAHQAdABlAHIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAACAAAAAgAAAAEBAAD/////AAAAAAAAAAAAAAAAAAAAAERJTlUiANgAFAPsGkxYzggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAA2AAAAFNNVEoAAAAAEADIAEgAUAAgAFAAaABvAHQAbwBzAG0AYQByAHQAIABQAGwAdQBzACAAQgAyADAAOQBhAC0AbQAAAElucHV0QmluAEZPUk1TT1VSQ0UAUkVTRExMAFVuaXJlc0RMTABQYXBlclNpemUATEVUVEVSAE9yaWVudGF0aW9uAFBPUlRSQUlUAFJlc29sdXRpb24ANjAwZHBpAENvbG9yTW9kZQBDb2xvcjI0AEhQU2ltcGxpZmllZFVJAFRydWUAAAAAAAAAAAAAAAAAAAAA7BoAAElVUEgBABIAH6gzBMIvS0Ogan0IKNDTIVgCAABYAgAAVQAAAEsAAABAEwAAMhkAAOwTAADIGQAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAFt4AwBDOlxXaW5kb3dzXHNwbHdvdzY0LmV4ZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLBgAAwwYAADgJAQB42u2dzW8UZRzHf7PbbrfaNxCUlpcWkFIskt1u2W6Rt74BiUgEqlYJ0WgrVBEIL0a9wIGDBw/Gg4kHEg/+ASZcvKgbjScvHr2piRFjOKxGSQwk9ffM/Gb22dln6NTuVsvz/WyePs/MPDOz89vZzzzz0meJqOM5hwI2pYnWSPldTic55TipOv0y/iCnZioPLwS1/EZe1iCX73C6lSQadrx1cpG2NBK1cJ7h1NcYvRz13lZwmuI6fZx/zGm20Xu/av7mFFET5wVOq1PeNqjhp7ickuVPcuEap+08YXvaoTtzN4Pll95x5qj0+0FtlS+VrrRLMXmVw/bNJ++le/fcnYugne47St86/BkVu4l+3Hf3bx7hXFW7TkJNcyRN+yOYTvlQHUPqlHqJiOl+ShiSE2M+Z4Hb5tR4ef8XnCVO+ueSXGDcVN0/+c9fnG5zGuVv8hin8XukBP036XXDtqvt5W++s+C4OV6ulDGVKMdxWqaRjJ+SL1ZUjJT41mh1/aRribz3OLe/Oxx997vsCjmhjb3F6bi8zyfVNt4t5XInRoi+45TmlBnZ/fnKvTxlr5/zor7U05UmKs7dTFGRl1Fi114jzRFO6DutaJBYzMcrnK7Ldl2XNC3Twp5okHK7HAjUPOHvtV+3T4aLXLGN5CDB+Qecd3H+heTfS/6H5C1N3sGll/Nkg7uiHjm+qLd1uJU/jgmiTzlv4PWkOa6nb/Bsk0Sf8fRLTQ4dphm6xK8ZuuAG/jyd49cZd7iHhy/Ty/z3bbro1nmDx41wnfNc9zT/fZVrXaQhPrq9STt4nWf45U+ZpVORU2Yipqi1ZHhalqdccN8L0RFeI/F6T9BZHj7L854kYBuHaJSO8Wd/lvcYb9+c5FztL8AGxvi7f5k/91kxFbALdf72oXYIdw+XxWKQL0VTs2MZN8tB7Zmgt9w2ygU+Cr1IRzkdQFAsJIEQAKMTjnEaRVAsJIkQgEgnjCAoFtKAEACjEybgBEtpRAiA0QmTcAKcAOCEkBNwPQFOAHCC7oQxBAVOAHCC5gTci4QTAJygO+EgggInADhBc8IhBAVOAHAC7jtYTgohAEYnPA8nwAkATgg5YRxBsZAmhAAYnfAM2glwAoATQk7AM0s2kkYIgNEJR3EvEgAAAKme4fx+VfQu2RT+fcpU6DwjLfM0S/6A5A9K3iJ5q+RtVO7yTq2jQ/IVkq+U/CHJV0m+WvKHJX9E8jWS+91Kdkm+VvJ1kq8n73/BN8g2dcs2bQzFwO8CLu7/jf/Q4eV+3AYpRwNuL6MAEP3G+8ftjvIw9gyge3U+fjH6ZQgBBPALiCTuc2U/Gf2SQwAB/AIiifs8itkvAwggCPyiA78ARdz72D8b/bITAQRov4BI4t4T/9Xol2EEELgcTaL9AqppjlnvtdZKv+x0/VJAAIG7T6zqhF9ANZ0x6329Mtx+KfA+NEi76AgnbygjQ0Nsn0JQzleMz0u5PBfOse5H4Beg6FpCv8Ap8Auwi7V19wvaLPALsJV1dfYLnAK/AHtZX2O/4HoLgF+Az4Y6+gVOgV+A3XTX3C9oswD4BXj0LMovQ8brL+UxhaA0rI3PVtXM8ssvDbjjwfLAf67uQAv8AqrZuAi/ZCn3L554yVa0enyfhF1zBJZZVn55H8/vAgOb6uoXOAV+ATazuQ5+QZsFfoFfgGJLjfyCcyL4BX4BYXpr7hc4BX6BX4DH1pqdH6HNAr/AL6CSvkX5ZWfF3ehsxdCA8W61GpoMDJOf10BgefgF96eBiW118AuMAr/AL0DxWA39ghYL/AK/AJ3+mvil7Af8YomdflGg/10QJq4PTklHMYlgvozbGgHAIfS/C8zE7UG32GryyyACCOAXEEncXwCYNbZf0L83gF9ANLvieijklzybKQ+/gAi/eMcfWMZ2nohZ7yODX4aCO9Le0EBQwnkT/JLhfQHX52xnd8x6o0a/4PfVQLRfMvCL9eyJWW/ccP0lH/QvlXfPlfKhPuu88fiValv9MkQ5BMdy9sast66ruv1SCHqlU0PDwX84ekN4Us5mv3j7B67P2c6+mPVeCLVfcOUOzH9+hPaL7eyPWe9c1fUX74wIgCi/DOA6v/WM4PwI1PH8aJeUsDfYyWjMes8aru9mcJYE7uGXYdxftJ6xmPX2GO9Pq3tEx93+pTLSXxSef4FfyscfXN+1nfGY9Z42+GU46KHOG8KvqMEvle0XXN+1nYmY9b5qq/RLzm294MkWgP8/AgAAsPSoZon6DcekHIc2aeVtWvlxrdyvlfu0co9W3qGVt2rlXq28RStv18qbtfKjWlnnHxFTAU8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAW3gDAA==";

        private System.IO.Stream GetBinaryDataStream(string base64String)
        {
            return new System.IO.MemoryStream(System.Convert.FromBase64String(base64String));
        }

        #endregion



    }//// end class
}