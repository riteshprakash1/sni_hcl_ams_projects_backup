﻿// --------------------------------------------------------------
// <copyright file="Utility.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Configuration;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Mail;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Messaging;
    using System.Web;

    /// <summary>This class has some useful methods.</summary>
    public class Utility
    {
        /// <summary>Prevents a default instance of the <see cref="Utility"/> class from being created.</summary>
        private Utility()
        {
        }

        /// <summary>Gets the Authenticated User.</summary>
        /// <param name="req">The web session request.</param>
        /// <returns>username string</returns>
        public static string CurrentUser(System.Web.HttpRequest req)
        {
            string[] arrayAuth = req.ServerVariables["AUTH_USER"].Split('\\');;

            if (arrayAuth.Length > 1)
            {
                return arrayAuth[1];
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>The delegate for the DeleteDirectoryFiles method.</summary>
        /// <param name="strExportFolder">The fully qualified export directory address.</param>
        /// <param name="req">The HttpRequest.</param>
        public delegate string DeleteDirectoryFilesDelegate(string strFolderName, System.Web.HttpRequest req);


        /// <summary>Deletes the files older then yesterday in the directory specified.</summary>
        /// <param name="strExportFolder">The fully qualified export directory address.</param>
        /// <param name="req">The HttpRequest.</param>
        protected static string DeleteDirectoryFiles(string strFolderName, System.Web.HttpRequest req)
        {
                DateTime yesterday = DateTime.Now.AddDays(-1);
                Debug.WriteLine("yesterday: " + yesterday.ToString());

                DirectoryInfo exportDirectory = new DirectoryInfo(strFolderName);
                FileInfo[] exportFiles = exportDirectory.GetFiles();

                foreach (FileInfo f in exportFiles)
                {
                    try
                    {
                        if (f.CreationTime < yesterday)
                            f.Delete();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("RemoveOldExportFiles Error: " + ex.Message);
                        LogException.HandleException(ex, req);
                    }
                }

            return "files deleted";
        }

        /// <summary>The OnCallback for the DeleteDirectoryFiles method.</summary>
        /// <param name="ar">The IAsyncResult.</param>
        public static void GetDeleteDirectoryOnCallback(IAsyncResult ar)
        {
            DeleteDirectoryFilesDelegate del = (DeleteDirectoryFilesDelegate)((AsyncResult)ar).AsyncDelegate;
            try
            {
                string result = del.EndInvoke(ar);
                Debug.WriteLine("GetDeleteDirectoryOnCallback: result is " + result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetDeleteDirectoryOnCallback, problem occurred: " + ex.Message);
            }
        }

        /// <summary>The Asynchronous method for the DeleteDirectoryFiles method.</summary>
        /// <param name="strExportFolder">The fully qualified export directory address.</param>
        /// <param name="req">The HttpRequest.</param>
        public static string DeleteDirectoryFilesAsync(string strFolderName, System.Web.HttpRequest req)
        {
            DeleteDirectoryFilesDelegate dc = new DeleteDirectoryFilesDelegate(Utility.DeleteDirectoryFiles);
            AsyncCallback cb = new AsyncCallback(Utility.GetDeleteDirectoryOnCallback);
            IAsyncResult ar = dc.BeginInvoke(strFolderName, req, cb, null);

            return "ok";
        }

        /********************************************/


        public delegate string SendEmailDelegate(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml);

        public static void GetResultsOnCallback(IAsyncResult ar)
        {
            SendEmailDelegate del = (SendEmailDelegate)((AsyncResult)ar).AsyncDelegate;
            try
            {
                string result;
                result = del.EndInvoke(ar);
                Debug.WriteLine("\nOn CallBack: result is " + result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\nOn CallBack, problem occurred: " + ex.Message);
            }
        }

        public static string SendEmailAsync(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml)
         {
            SendEmailDelegate dc = new SendEmailDelegate(Utility.SendEmail);
            AsyncCallback cb = new AsyncCallback(Utility.GetResultsOnCallback);
            IAsyncResult ar = dc.BeginInvoke(strTo, strFrom, strCc, strSubj, strBody, isHtml, cb, null);

            return "ok";
        }

        /// <summary>Sends the email.</summary>
        /// <param name="strTo">The to address.</param>
        /// <param name="strFrom">The from address.</param>
        /// <param name="strSubj">The subjubject.</param>
        /// <param name="strBody">The body text.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        public static string SendEmail(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml)
        {
            MailAddress from = new MailAddress(strFrom);
            MailAddress to = new MailAddress(strTo);

            //// Specify the message content.
            MailMessage message = new MailMessage(from, to);
            Debug.WriteLine("From: " + message.From.Address);

            if (strCc != String.Empty)
            {
                message.CC.Add(strCc);
            }

            if (ConfigurationManager.AppSettings["ccEmail"] != null)
            {
                if (ConfigurationManager.AppSettings["ccEmail"] != String.Empty)
                {
                    message.CC.Add(ConfigurationManager.AppSettings["ccEmail"]);
                }
            }

            message.IsBodyHtml = isHtml;
            message.Subject = strSubj;
            message.Body = strBody;

            SmtpClient client = new SmtpClient();
            Debug.WriteLine("client: " + client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Debug.WriteLine(excm.Message);
                LogException.HandleException(excm, strSubj);
            }

            return "sent";
        }

        /// <summary>Sends the email.</summary>
        /// <param name="strTo">The to address.</param>
        /// <param name="strFrom">The from address.</param>
        /// <param name="strCc">The string list of cc addresses.</param>
        /// <param name="strSubj">The subjubject.</param>
        /// <param name="strBody">The body text.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        public static string SendEmail(string strTo, string strFrom, List<string> lstCC, string strSubj, string strBody, bool isHtml)
        {
            MailAddress from = new MailAddress(strFrom);
            MailAddress to = new MailAddress(strTo);

            //// Specify the message content.
            MailMessage message = new MailMessage(from, to);
            Debug.WriteLine("From: " + message.From.Address);

            foreach(string strCc in lstCC)
            {
                message.CC.Add(strCc);
                Debug.WriteLine("CC: " + strCc);
            }

            if (ConfigurationManager.AppSettings["ccEmail"] != null)
            {
                if (ConfigurationManager.AppSettings["ccEmail"] != String.Empty)
                {
                    message.CC.Add(ConfigurationManager.AppSettings["ccEmail"]);
                }
            }

            message.IsBodyHtml = isHtml;
            message.Subject = strSubj;
            message.Body = strBody;

            SmtpClient client = new SmtpClient();
            Debug.WriteLine("client: " + client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Debug.WriteLine(excm.Message);
                LogException.HandleException(excm, strSubj);
            }

            return "sent";
        }

    }//// end class
}