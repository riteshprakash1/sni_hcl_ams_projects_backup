﻿// ------------------------------------------------------------------
// <copyright file="UserRolesEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the User Roles Entity. </summary>    
    public class UserRolesEntity
    {

        /// <summary>This is the RoleID.</summary>
        private int intRoleID;

        /// <summary>This is the ProgramID.</summary>
        private int intProgramID;

        /// <summary>This is the UserID.</summary>
        private int intRoleTypeID;

        /// <summary>This is the Role Name.</summary>
        private string strRoleName;

        /// <summary>Initializes a new instance of the UserRolesEntity class.</summary>
        public UserRolesEntity()
        {
            this.RoleName = String.Empty;
        }

        /// <summary>Gets or sets the Role Type ID.</summary>
        /// <value>The Role Type ID.</value>
        public int RoleTypeID
        {
            get{ return this.intRoleTypeID;}
            set { this.intRoleTypeID = value; }
        }

        /// <summary>Gets or sets the ProgramID.</summary>
        /// <value>The ProgramID.</value>
        public int ProgramID
        {
            get { return this.intProgramID; }
            set { this.intProgramID = value; }
        }

        /// <summary>Gets or sets the RoleID.</summary>
        /// <value>The RoleID.</value>
        public int RoleID
        {
            get { return this.intRoleID; }
            set { this.intRoleID = value; }
        }

        /// <summary>Gets or sets the RoleName.</summary>
        /// <value>The RoleName.</value>
        public string RoleName
        {
            get { return this.strRoleName; }
            set { this.strRoleName = value; }
        }

        /// <summary>Receives a UserRole datarow and converts it to a UserRolesEntity.</summary>
        /// <param name="r">The UserRole DataRow.</param>
        /// <returns>UserRolesEntity</returns>
        public static UserRolesEntity GetEntityFromDataRow(DataRow r)
        {
            UserRolesEntity role = new UserRolesEntity();

            if ((r["RoleTypeID"] != null) && (r["RoleTypeID"] != DBNull.Value))
                role.RoleTypeID = Convert.ToInt32(r["RoleTypeID"].ToString());

            if ((r["RoleID"] != null) && (r["RoleID"] != DBNull.Value))
                role.RoleID = Convert.ToInt32(r["RoleID"].ToString());

            if ((r["ProgramID"] != null) && (r["ProgramID"] != DBNull.Value))
                role.ProgramID = Convert.ToInt32(r["ProgramID"].ToString());

            role.RoleName = r["RoleName"].ToString();

            return role;
        }

        /// <summary>Gets the UserRoles List by the Program ID.</summary>
        /// <param name="intProgramID">The ProgramID</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<UserRolesEntity> GetUserRolesByProgramID(int intProgramID)
        {
            List<UserRolesEntity> roles = new List<UserRolesEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@programID", intProgramID));
            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUserRolesByProgramID", parameters);

            Debug.WriteLine("Role Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserRolesEntity myRole = UserRolesEntity.GetEntityFromDataRow(r);
                roles.Add(myRole);
            }

            return roles;
        }

        /// <summary>Gets the UserRoles List by the Program ID.</summary>
        /// <param name="strUsername">The username</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<UserRolesEntity> GetUserListRolesByUsername(string strUsername)
        {
            List<UserRolesEntity> roles = new List<UserRolesEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUserListRolesByUsername", parameters);
            parameters.Add(new SqlParameter("@Username", strUsername));
            Debug.WriteLine("Role Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserRolesEntity myRole = UserRolesEntity.GetEntityFromDataRow(r);
                roles.Add(myRole);
            }

            return roles;
        }

        /// <summary>Gets all the UserRoles List.</summary>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<UserRolesEntity> GetUserRoles()
        {
            List<UserRolesEntity> roles = new List<UserRolesEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUserRoles", parameters);
            Debug.WriteLine("Role Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserRolesEntity myRole = UserRolesEntity.GetEntityFromDataRow(r);
                roles.Add(myRole);
            }

            return roles;
        }

        /// <summary>Gets all the RoleTypes.</summary>
        /// <returns>The RoleTypes datatable</returns>
        public static DataTable GetRoleTypes()
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetRoleTypes", parameters);
            Debug.WriteLine("RoleTypes Rows: " + dt.Rows.Count.ToString());

            return dt;
        }

    }//// end class
}