﻿// ------------------------------------------------------------------
// <copyright file="SalesRepEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    
    /// <summary>
    /// This is the Sales Rep Entity.
    /// </summary>    
    public class SalesRepEntity
    {
        /// <summary>This is the CustomerID.</summary>
        private string strCustomerID;

        /// <summary>This is the RepName.</summary>
        private string strRepName;

        /// <summary>This is the RepID.</summary>
        private string strRepID;

        /// <summary>This is the DistrictID.</summary>
        private string strDistrictID;

        /// <summary>This is the DistrictName.</summary>
        private string strDistrictName;

        /// <summary>This is the RvpRegion.</summary>
        private string strRvpRegion;

        /// <summary>This is the strRvp Name.</summary>
        private string strRvpName;

        /// <summary>This is the SalesOrgId.</summary>
        private string strSalesOrgID;

        /// <summary>This is the Partner Function.</summary>
        private string strPartnerFunction;

        /// <summary>This is the Partner Function Desc.</summary>
        private string strPartnerFunctionDesc;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEntity"/> class.
        /// </summary>
        public SalesRepEntity()
        {
            this.CustomerID = String.Empty;
            this.RepName = String.Empty;
            this.RepID = String.Empty;
            this.DistrictID = String.Empty;
            this.DistrictName = String.Empty;
            this.RepID = String.Empty;
            this.RvpName = String.Empty;
            this.RvpRegion = String.Empty;
            this.SalesOrgID = String.Empty;
            this.PartnerFunction = String.Empty;
            this.PartnerFunctionDesc = String.Empty;
        }

        /// <summary>Gets or sets the Customer ID.</summary>
        /// <value>The Customer ID.</value>
        public string CustomerID
        {
            get
            {
                return this.strCustomerID;
            }

            set
            {
                this.strCustomerID = value;
            }
        }
        
        /// <summary>Gets or sets the name of the Rep.</summary>
        /// <value>The name of the rep.</value>
        public string RepName
        {
            get
            {
                return this.strRepName;
            }

            set
            {
                this.strRepName = value;
            }
        }

        /// <summary>Gets or sets the ID of the Rep.</summary>
        /// <value>The ID of the rep.</value>
        public string RepID
        {
            get
            {
                return this.strRepID;
            }

            set
            {
                this.strRepID = value;
            }
        }

        /// <summary>Gets or sets the name of the District.</summary>
        /// <value>The name of the District.</value>
        public string DistrictName
        {
            get
            {
                return this.strDistrictName;
            }

            set
            {
                this.strDistrictName = value;
            }
        }

        /// <summary>Gets or sets the ID of the District.</summary>
        /// <value>The ID of the District.</value>
        public string DistrictID
        {
            get
            {
                return this.strDistrictID;
            }

            set
            {
                this.strDistrictID = value;
            }
        }

        /// <summary>Gets or sets the name of the Rvp.</summary>
        /// <value>The name of the Rvp.</value>
        public string RvpName
        {
            get
            {
                return this.strRvpName;
            }

            set
            {
                this.strRvpName = value;
            }
        }

        /// <summary>Gets or sets the region of the RVP.</summary>
        /// <value>The Region of the RVP.</value>
        public string RvpRegion
        {
            get
            {
                return this.strRvpRegion;
            }

            set
            {
                this.strRvpRegion = value;
            }
        }

        /// <summary>Gets or sets the ID of the Sales Org.</summary>
        /// <value>The ID of the Sales Org.</value>
        public string SalesOrgID
        {
            get
            {
                return this.strSalesOrgID;
            }

            set
            {
                this.strSalesOrgID = value;
            }
        }

        /// <summary>Gets or sets the Partner Function Desc.</summary>
        /// <value>The Partner Function Desc.</value>
        public string PartnerFunctionDesc
        {
            get
            {
                return this.strPartnerFunctionDesc;
            }

            set
            {
                this.strPartnerFunctionDesc = value;
            }
        }

        /// <summary>Gets or sets the Partner Function.</summary>
        /// <value>The Partner Function.</value>
        public string PartnerFunction
        {
            get
            {
                return this.strPartnerFunction;
            }

            set
            {
                this.strPartnerFunction = value;
            }
        }

        /// <summary>Gets the SalesRepEntity Collection for the Customer.</summary>
        /// <param name="strCustomerID">The Customer ID</param>
        /// <returns>The newly populated Collection<SalesRepEntity></returns>
        public static Collection<SalesRepEntity> GetSalesRepsByCustomerID(string strCustomerID)
        {
            Collection<SalesRepEntity> mySalesReps = new Collection<SalesRepEntity>();

            string sQuery = string.Empty;
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@QueryName", "SalesRepsByCustomer"));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetQueryByQueryName", parameters);
            if (dt.Rows.Count > 0)
            {
                sQuery = dt.Rows[0]["Query"].ToString();
                sQuery = sQuery.Replace("xxCUSTOMERIDxx", strCustomerID);
            }

            Debug.WriteLine("GetSalesRepsByCustomerID sQuery: " + sQuery);

//            DataTable dtReps = SQLUtility.SqlExecuteDynamicQuery(sQuery, SQLUtility.OpenGdwSqlConnection());
            DataTable dtReps = SQLUtility.OracleExecuteDynamicQuery(sQuery);

            foreach (DataRow row in dtReps.Rows)
            {
                mySalesReps.Add(SalesRepEntity.GetSalesRepEntityFromDataRow(row));
            }

            return mySalesReps;
        }

        /// <summary>Receives a SalesRep datarow and converts it to a SalesRepEntity.</summary>
        /// <param name="r">The Sales Rep DataRow.</param>
        /// <returns>SalesRepEntity</returns>
        protected static SalesRepEntity GetSalesRepEntityFromDataRow(DataRow r)
        {
            SalesRepEntity sr = new SalesRepEntity();
            
            sr.CustomerID = r["customer_ID"].ToString();
            sr.PartnerFunction = r["Partner_Function"].ToString();
            sr.PartnerFunctionDesc = r["Partner_Function_Desc"].ToString();
            sr.SalesOrgID = r["Sales_Org_Id"].ToString();
            sr.RepID = r["Rep_Id"].ToString();
            sr.RepName = r["Rep_Name"].ToString();
            sr.DistrictID = r["District_Id"].ToString();
            sr.DistrictName = r["District_Name"].ToString();
            sr.RvpName = r["RVP_Name"].ToString();
            sr.RvpRegion = r["RVP_Region"].ToString();

            return sr;
        }

    } //// end class
}