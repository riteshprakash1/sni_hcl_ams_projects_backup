﻿<%@ Page Title="Message Board" Language="C#" AutoEventWireup="true" CodeBehind="MessageBoard.aspx.cs" Inherits="SalesRequestForms.MessageBoard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Message Board</title>
    <link href="Styles/global.css" rel="stylesheet" type="text/css" />
    <link href="Styles/StyleNED.css" rel="stylesheet" type="text/css" />
    <link href="Styles/HintStyle.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>

</head>
<body id="myBody" runat="server">

    <form id="form1" runat="server">
    <div id="shell">
    
        <table width="100%">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="5" width="100%" align="center" border="0">
                        <tr>
                            <td width="30%">
                                <div align="left">
                                    <asp:Image ID="Image1" ImageUrl="~/images/logo.gif" runat="server" Width="221" AlternateText="Smith &amp; Nephew" />
                                </div>
                            </td>
		                    <td style="vertical-align:top;text-align:left;color:#FF7300;font-size:9pt">&nbsp;</td>
                            <td width="30%">
                                <div align="right">
                                    <asp:Button ID="btnAppName" BorderStyle="None" ForeColor="DarkGray" BackColor="White"
                                        BorderColor="White" Text="Sales Request Forms" Font-Size="16pt" Font-Bold="True" runat="server"
                                        CausesValidation="False"></asp:Button></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                    </table>
                    <table style="background-color:#676264; text-align:center; table-layout:fixed" width="100%" >
                        <tr>
                            <td style="text-align:left">&nbsp;</td>
                        </tr>
                    </table>

                </td>
            </tr>
			<tr>
				<td style="font-size: large; text-decoration: underline"><strong>APPLICATION DOWN FOR MAINTENANCE</strong></td>
			</tr>
			<tr>
				<td style="font-size:10pt">Please note this application will be down for maintenance beginning at 8:00pm EST on 28-March until 8:00am EST on 3-April.</td>
			</tr>
        </table>
    </div>
    </form>
</body>
</html>

