﻿// --------------------------------------------------------------
// <copyright file="Default.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Default page.</summary>   
    public partial class _Default : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindDT();
            }
        } //// end Page_Load

        /// <summary>Handles the RowDataBound event of the gvPrograms GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //// if not an admin redirect to Unauthorized Page
            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

                //// if not an admin and status not enabled hide row
            if (!AppUsersEntity.IsAdmin(myUsers))
            {            
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string strProgramStatus = this.gvPrograms.DataKeys[e.Row.RowIndex]["programStatus"].ToString();
                    e.Row.Visible = Convert.ToBoolean(strProgramStatus.ToLower() == "enabled");
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnkGuide = (HyperLink)e.Row.FindControl("lnkGuide");
                string strProgramID = this.gvPrograms.DataKeys[e.Row.RowIndex]["programID"].ToString();

                string strFileName = "UserGuide" + strProgramID + ".pdf";
                string strRelativeUrl = "~/Guides/" + strFileName;
                    
                strFileName = ConfigurationManager.AppSettings["GuideFolder"] + strFileName;

                if (File.Exists(strFileName))
                    lnkGuide.NavigateUrl = strRelativeUrl;

                lnkGuide.Visible = File.Exists(strFileName);
            }


        } // end gv_RowDataBound


        /// <summary>Handles the Edit event of the gvPrograms GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_View(object sender, GridViewEditEventArgs e)
        {
            string strProgramID = this.gvPrograms.DataKeys[e.NewEditIndex]["programID"].ToString();
            string strURL = "ProgramDetail.aspx?programId=" + strProgramID;
            Debug.WriteLine("strURL: " + strURL);
            Response.Redirect("ProgramDetail.aspx?programId=" + strProgramID, true);
            //  Response.Redirect("ProgramDetail.aspx");
        }

        /// <summary>Binds the users table to the gvCosts GridView.</summary>
        protected void BindDT()
        {
            DataTable dtPrograms = SQLUtility.SqlExecuteQuery("sp_GetPrograms");

            this.gvPrograms.DataSource = dtPrograms;
            this.gvPrograms.DataBind();

            List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));
            this.gvPrograms.Columns[4].Visible = Convert.ToBoolean(AppUsersEntity.IsAdmin(myUsers));

        }//// end BindDT


        /// <summary>Handles the Update event of the gvCost GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            string strFormName = this.gvPrograms.DataKeys[e.RowIndex]["programFormName"].ToString();
            string strFolder = this.gvPrograms.DataKeys[e.RowIndex]["programFolder"].ToString();
            string strRedirect = "~/" + strFolder + "/" + strFormName + ".aspx";
            Debug.WriteLine("strRedirect: " + strRedirect);
            Response.Redirect(strRedirect, true);
        }

    }
}
