﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="SalesRequestForms.ErrorPage" Title="Error Page" %>

<!doctype html>

<html>
<head id="Head1" runat="server">
    <title>Error Page</title>
    <link href="Styles/global.css" rel="stylesheet" type="text/css" />
    <link href="Styles/StyleNED.css" rel="stylesheet" type="text/css" />
    <link href="Styles/HintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body id="myBody" runat="server">

    <form id="form1" runat="server">
        <div id="shell">

        <table style="width:100%">
            <tr>
                <td>
                
                    <table cellspacing="0" cellpadding="5" width="100%" align="center" border="0">
                        <tr>
                            <td style="width:30%">
                                <div align="left">
                                    <asp:Image ID="Image1" ImageUrl="~/images/logo.gif" runat="server" Width="221" AlternateText="Smith &amp; Nephew" />
                                </div>
                            </td>
		                    <td style="vertical-align:top; text-align:left;color:#FF7300">&nbsp;</td>
                            <td style="width:35%">
                                <div align="right">
                                    <asp:Label ID="lblAppName" BorderStyle="None" ForeColor="DarkGray" BackColor="White"
                                        BorderColor="White" Text="Sales Request Forms" Font-Size="16pt" Font-Bold="True" runat="server"
                                        CausesValidation="False"></asp:Label></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
            <table align="center" style="width:600px" border="1">
                <tr style="height:200px; vertical-align:middle">
                    <td style="text-align:center">
                        <asp:Label ID="lblErrorHeading" ForeColor="#FF7300" Font-Size="14pt" runat="server" Text="Application Error"></asp:Label>
                         <br /><br />
                        <asp:Label ID="lblMsg" Font-Size="11pt" runat="server" Text="An application error occurred.  
                        An email has been sent to the application developer.  
                        You may be contacted by the application developer to troubleshoot the issue.  
                        Click on one of the links above to be redirected to the application." ></asp:Label>
                        <br /><br />
                        <asp:Label ID="lblErrorMsg" Font-Size="11pt" runat="server" Text="Application Error"></asp:Label>
                    </td>
                </tr>
            </table>


        </div>
    </form>
    </body>
</html>
