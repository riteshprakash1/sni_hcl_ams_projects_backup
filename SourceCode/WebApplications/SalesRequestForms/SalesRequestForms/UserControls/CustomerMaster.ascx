﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerMaster.ascx.cs" Inherits="SalesRequestForms.UserControls.CustomerMaster" %>
<%@ Register Namespace="SalesRequestForms.Classes" Assembly="SalesRequestForms" TagPrefix="uc2" %>
<table width="100%">
    <tr>
        <td style="width:31%">Customer Sold To Number: *</td>
        <td>
            <asp:TextBox ID="txtCustomerID" pattern="[0-9]*" runat="server" MaxLength="10" width="80px" ontextchanged="txtCustomerID_TextChanged" AutoPostBack="True"></asp:TextBox>
            <asp:HiddenField ID="hdnCustomerID" runat="server" Value="" />
            <asp:RequiredFieldValidator ID="vldRequiredCustomerID" ControlToValidate="txtCustomerID" runat="server" ForeColor="#FF7300" 
                ErrorMessage="The Customer Account Number is required.">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="vldCustomerIDExists"  ControlToValidate="txtCustomerID" runat="server" ForeColor="#FF7300" 
                ErrorMessage="The Customer Account Number was not found."  ValidationGroup="FindCustomer">*</asp:CustomValidator>
            <asp:CustomValidator ID="vldCustomerIDExistsSubmit"  ControlToValidate="txtCustomerID" runat="server" ForeColor="#FF7300" 
                ErrorMessage="The Customer Account Number was not found.">*</asp:CustomValidator>
        &nbsp;
            <uc2:waitbutton id="btnSearch"  runat="server" ValidationGroup="FindCustomer" CausesValidation="False" 
                CssClass="buttonsSubmit" Text="Find Customer" onclick="BtnSearch_Click"
				        WaitText="Processing..." ></uc2:waitbutton>
        </td>
    </tr>
    <tr>
        <td>Sales Rep: *</td>
        <td><asp:DropDownList ID="ddlSalesRep" runat="server" Font-Size="8pt" Width="300px" 
                onselectedindexchanged="ddlSalesRep_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="vldRequiredSalesRep" ControlToValidate="ddlSalesRep" runat="server" ForeColor="#FF7300" 
                ErrorMessage="The Sales Rep is required.">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>RVP:</td>
        <td><asp:Label ID="lblRvpName" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>DE:</td>
        <td><asp:Label ID="lblDeName" runat="server"></asp:Label><asp:Label ID="lblDistrictID" runat="server" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td>Customer Name:</td>
        <td><asp:Label ID="lblCustomerName" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Customer Address:</td>
        <td><asp:Label ID="lblCustomerAddress" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Customer City, State, Zip:</td>
        <td><asp:Label ID="lblCustomerCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblCustomerState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblCustomerZip" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>GPO:</td>
        <td><asp:Label ID="lblGPO" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelCustGroup1" runat="server" Text="Customer Group1:"></asp:Label></td>
        <td><asp:Label ID="lblCustGroup1" runat="server"></asp:Label><asp:Label ID="LabelCustGroup1Filler" runat="server"></asp:Label><asp:Label ID="lblCustGroup1ID" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2"><asp:ValidationSummary ID="vldSummaryCustomer" ValidationGroup="FindCustomer" runat="server"  ForeColor="#FF7300" Font-Size="10pt" />
            <asp:HiddenField ID="hdnSalesOrg" runat="server" Value="" />
        </td>
    </tr>
</table>