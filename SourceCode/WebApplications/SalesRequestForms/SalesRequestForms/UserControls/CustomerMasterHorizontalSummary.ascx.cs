﻿// -------------------------------------------------------------
// <copyright file="CustomerMasterHorizontalSummary.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.UserControls
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>This is the read only Customer Master Horizontal Summary user control</summary>
    public partial class CustomerMasterHorizontalSummary : System.Web.UI.UserControl
    {

        /// <summary>Gets or sets the CustomerID.</summary>
        /// <value>The CustomerID.</value>
        public string CustomerID
        {
            get { return this.lblCustomerID.Text; }

            set { this.lblCustomerID.Text = value; }
        }

        /// <summary>Gets or sets the CustomerAddress.</summary>
        /// <value>The CustomerAddress.</value>
        public string CustomerAddress
        {
            get { return this.lblCustomerAddress.Text; }

            set { this.lblCustomerAddress.Text = value; }
        }

        /// <summary>Gets or sets the CustomerCity.</summary>
        /// <value>The CustomerCity.</value>
        public string CustomerCity
        {
            get { return this.lblCustomerCity.Text; }

            set { this.lblCustomerCity.Text = value; }
        }

        /// <summary>Gets or sets the CustomerName.</summary>
        /// <value>The CustomerName.</value>
        public string CustomerName
        {
            get { return this.lblCustomerName.Text; }

            set { this.lblCustomerName.Text = value; }
        }

        /// <summary>Gets or sets the CustomerState.</summary>
        /// <value>The CustomerState.</value>
        public string CustomerState
        {
            get { return this.lblCustomerState.Text; }

            set { this.lblCustomerState.Text = value; }
        }

        /// <summary>Gets or sets the CustomerZip.</summary>
        /// <value>The CustomerZip.</value>
        public string CustomerZip
        {
            get { return this.lblCustomerZip.Text; }

            set { this.lblCustomerZip.Text = value; }
        }

        /// <summary>Gets or sets the DeName.</summary>
        /// <value>The DeName.</value>
        public string DeName
        {
            get { return this.lblDeName.Text; }

            set { this.lblDeName.Text = value; }
        }

        /// <summary>Gets or sets the DistrictID.</summary>
        /// <value>The DistrictID.</value>
        public string DistrictID
        {
            get { return this.lblDistrictID.Text; }

            set { this.lblDistrictID.Text = value; }
        }

        /// <summary>Gets or sets the GPO.</summary>
        /// <value>The GPO.</value>
        public string GPO
        {
            get { return this.lblGPO.Text; }

            set { this.lblGPO.Text = value; }
        }

        /// <summary>Gets or sets the CustGroup1.</summary>
        /// <value>The CustGroup1.</value>
        public string CustGroup1
        {
            get { return this.lblCustGroup1.Text; }

            set { this.lblCustGroup1.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1.</summary>
        /// <value>The Cust Group1.</value>
        public string CustGroup1ID
        {
            get { return this.lblCustGroup1ID.Text; }

            set { this.lblCustGroup1ID.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1 Filler.</summary>
        /// <value>The Cust Group1 Filler.</value>
        public string CustGroup1Filler
        {
            get { return this.LabelCustGroup1Filler.Text; }

            set { this.LabelCustGroup1Filler.Text = value; }
        }

        /// <summary>Gets or sets the RepName.</summary>
        /// <value>The RepName.</value>
        public string RepName
        {
            get { return this.lblSalesRepName.Text; }

            set { this.lblSalesRepName.Text = value; }
        }

        /// <summary>Gets or sets the RepID.</summary>
        /// <value>The RepID.</value>
        public string RepID
        {
            get { return this.lblSalesRepID.Text; }

            set { this.lblSalesRepID.Text = value; }
        }

        /// <summary>Gets or sets the RvpName.</summary>
        /// <value>The RvpName.</value>
        public string RvpName
        {
            get { return this.lblRvpName.Text; }

            set { this.lblRvpName.Text = value; }
        }


        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>        
        protected void Page_Load(object sender, EventArgs e)
        {


        }
    }
}