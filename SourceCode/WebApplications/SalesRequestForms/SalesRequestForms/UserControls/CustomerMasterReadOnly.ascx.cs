﻿// -------------------------------------------------------------
// <copyright file="CustomerMasterReadOnly.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.UserControls
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>This is the read only customer master user control</summary>
    public partial class CustomerMasterReadOnly : System.Web.UI.UserControl
    {
        /// <summary>Gets or sets the SubmittedBy.</summary>
        /// <value>The SubmittedBy.</value>
        public string SubmittedBy
        {
            get { return this.lblSubmittedBy.Text; }

            set { this.lblSubmittedBy.Text = value; }
        }

        /// <summary>Gets or sets the SubmittedDate.</summary>
        /// <value>The SubmittedDate.</value>
        public string SubmittedDate
        {
            get { return this.lblSubmittedDate.Text; }

            set { this.lblSubmittedDate.Text = value; }
        }

        /// <summary>Gets or sets the CustomerID.</summary>
        /// <value>The CustomerID.</value>
        public string CustomerID
        {
            get { return this.lblCustomerID.Text; }

            set { this.lblCustomerID.Text = value; }
        }

        /// <summary>Gets or sets the CustomerAddress.</summary>
        /// <value>The CustomerAddress.</value>
        public string CustomerAddress
        {
            get { return this.lblCustomerAddress.Text; }

            set { this.lblCustomerAddress.Text = value; }
        }

        /// <summary>Gets or sets the CustomerCity.</summary>
        /// <value>The CustomerCity.</value>
        public string CustomerCity
        {
            get { return this.lblCustomerCity.Text; }

            set { this.lblCustomerCity.Text = value; }
        }

        /// <summary>Gets or sets the CustomerName.</summary>
        /// <value>The CustomerName.</value>
        public string CustomerName
        {
            get { return this.lblCustomerName.Text; }

            set { this.lblCustomerName.Text = value; }
        }

        /// <summary>Gets or sets the CustomerState.</summary>
        /// <value>The CustomerState.</value>
        public string CustomerState
        {
            get { return this.lblCustomerState.Text; }

            set { this.lblCustomerState.Text = value; }
        }

        /// <summary>Gets or sets the CustomerZip.</summary>
        /// <value>The CustomerZip.</value>
        public string CustomerZip
        {
            get { return this.lblCustomerZip.Text; }

            set { this.lblCustomerZip.Text = value; }
        }

        /// <summary>Gets or sets the DeName.</summary>
        /// <value>The DeName.</value>
        public string DeName
        {
            get { return this.lblDeName.Text; }

            set { this.lblDeName.Text = value; }
        }

        /// <summary>Gets or sets the DistrictID.</summary>
        /// <value>The DistrictID.</value>
        public string DistrictID
        {
            get { return this.lblDistrictID.Text; }

            set { this.lblDistrictID.Text = value; }
        }

        /// <summary>Gets or sets the GPO.</summary>
        /// <value>The GPO.</value>
        public string GPO
        {
            get { return this.lblGPO.Text; }

            set { this.lblGPO.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1.</summary>
        /// <value>The Cust Group1.</value>
        public string CustGroup1
        {
            get { return this.lblCustGroup1.Text; }

            set { this.lblCustGroup1.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1.</summary>
        /// <value>The Cust Group1.</value>
        public string CustGroup1ID
        {
            get { return this.lblCustGroup1ID.Text; }

            set { this.lblCustGroup1ID.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1 Filler.</summary>
        /// <value>The Cust Group1 Filler.</value>
        public string CustGroup1Filler
        {
            get { return this.LabelCustGroup1Filler.Text; }

            set { this.LabelCustGroup1Filler.Text = value; }
        }

        /// <summary>Gets or sets the RepName.</summary>
        /// <value>The RepName.</value>
        public string RepName
        {
            get { return this.lblSalesRepName.Text; }

            set { this.lblSalesRepName.Text = value; }
        }

        /// <summary>Gets or sets the RepID.</summary>
        /// <value>The RepID.</value>
        public string RepID
        {
            get { return this.lblSalesRepID.Text; }

            set { this.lblSalesRepID.Text = value; }
        }

        /// <summary>Gets or sets the RvpName.</summary>
        /// <value>The RvpName.</value>
        public string RvpName
        {
            get { return this.lblRvpName.Text; }

            set { this.lblRvpName.Text = value; }
        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>Make the controls visible or not</summary>
        /// <param name="isVisible">booleen to make the controls visible or not.</param>
        public void CustomerGroup1Visible(bool isVisible)
        {
            this.lblCustGroup1.Visible = isVisible;
            this.LabelCustGroup1.Visible = isVisible;
            this.lblCustGroup1ID.Visible = isVisible;
            this.LabelCustGroup1Filler.Visible = isVisible;
        } //// end CustomerGroup1Visible


    }
}