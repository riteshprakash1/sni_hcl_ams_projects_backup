﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SalesRequestForms.Classes;

namespace SalesRequestForms.UserControls
{
    public partial class CustomerMaster : System.Web.UI.UserControl
    {

        /// <summary>Gets CustomerMaster PageValidators.</summary>
        /// <value>The CustomerMaster PageValidators</value>
        public ValidatorCollection PageValidators
        {
            get { return Page.Validators; }
        }
        
        /// <summary>Gets or sets the CustomerID.</summary>
        /// <value>The CustomerID.</value>
        public string CustomerID
        {
            get { return this.txtCustomerID.Text; }

            set { this.txtCustomerID.Text = value; }
        }

        /// <summary>Gets or sets the CustomerID Enabled property.</summary>
        /// <value>The CustomerID enabled.</value>
        public bool CustomerIDEnabled
        {
            get { return this.txtCustomerID.Enabled; }

            set { this.txtCustomerID.Enabled = value; }
        }

        /// <summary>Gets or sets the HiddenCustomerID.</summary>
        /// <value>The HiddenCustomerID.</value>
        public string HiddenCustomerID
        {
            get { return this.hdnCustomerID.Value; }

            set { this.hdnCustomerID.Value = value; }
        }

        /// <summary>Gets or sets the CustomerAddress.</summary>
        /// <value>The CustomerAddress.</value>
        public string CustomerAddress
        {
            get { return this.lblCustomerAddress.Text; }

            set { this.lblCustomerAddress.Text = value; }
        }

        /// <summary>Gets or sets the CustomerCity.</summary>
        /// <value>The CustomerCity.</value>
        public string CustomerCity
        {
            get { return this.lblCustomerCity.Text; }

            set { this.lblCustomerCity.Text = value; }
        }

        /// <summary>Gets or sets the CustomerName.</summary>
        /// <value>The CustomerName.</value>
        public string CustomerName
        {
            get { return this.lblCustomerName.Text; }

            set { this.lblCustomerName.Text = value; }
        }

        /// <summary>Gets or sets the CustomerState.</summary>
        /// <value>The CustomerState.</value>
        public string CustomerState
        {
            get { return this.lblCustomerState.Text; }

            set { this.lblCustomerState.Text = value; }
        }

        /// <summary>Gets or sets the CustomerZip.</summary>
        /// <value>The CustomerZip.</value>
        public string CustomerZip
        {
            get { return this.lblCustomerZip.Text; }

            set { this.lblCustomerZip.Text = value; }
        }

        /// <summary>Gets or sets the DeName.</summary>
        /// <value>The DeName.</value>
        public string DeName
        {
            get { return this.lblDeName.Text; }

            set { this.lblDeName.Text = value; }
        }

        /// <summary>Gets or sets the DistrictID.</summary>
        /// <value>The DistrictID.</value>
        public string DistrictID
        {
            get { return this.lblDistrictID.Text; }

            set { this.lblDistrictID.Text = value; }
        }

        /// <summary>Gets or sets the GPO.</summary>
        /// <value>The GPO.</value>
        public string GPO
        {
            get { return this.lblGPO.Text; }

            set { this.lblGPO.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1.</summary>
        /// <value>The Cust Group1.</value>
        public string CustGroup1
        {
            get { return this.lblCustGroup1.Text; }

            set { this.lblCustGroup1.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1.</summary>
        /// <value>The Cust Group1.</value>
        public string CustGroup1ID
        {
            get { return this.lblCustGroup1ID.Text; }

            set { this.lblCustGroup1ID.Text = value; }
        }

        /// <summary>Gets or sets the Cust Group1 Filler.</summary>
        /// <value>The Cust Group1 Filler.</value>
        public string CustGroup1Filler
        {
            get { return this.LabelCustGroup1Filler.Text; }

            set { this.LabelCustGroup1Filler.Text = value; }
        }

        /// <summary>Gets or sets the RepName.</summary>
        /// <value>The RepName.</value>
        public string RepName
        {
            get { return this.ddlSalesRep.SelectedItem.Text; }

            set { this.ddlSalesRep.SelectedItem.Text = value; }
        }

        /// <summary>Gets or sets the RepID.</summary>
        /// <value>The RepID.</value>
        public string RepID
        {
            get { return this.ddlSalesRep.SelectedItem.Value; }

            set { this.ddlSalesRep.SelectedItem.Value = value; }
        }

        /// <summary>Gets or sets the RvpName.</summary>
        /// <value>The RvpName.</value>
        public string RvpName
        {
            get { return this.lblRvpName.Text; }

            set { this.lblRvpName.Text = value; }
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("In CustomerMaster Page_Load");
            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtCustomerID, this.btnSearch);
                Session["CustomerReps"] = null;
            }

        } //// end Page_Load

        /// <summary>Make the controls visible or not</summary>
        /// <param name="isVisible">booleen to make the controls visible or not.</param>
        public void CustomerGroup1Visible(bool isVisible)
        {
            this.lblCustGroup1.Visible = isVisible;
            this.LabelCustGroup1.Visible = isVisible;
            this.lblCustGroup1ID.Visible = isVisible;
            this.LabelCustGroup1Filler.Visible = isVisible;
        } //// end CustomerGroup1Visible


        /// <summary>Handles the Text Changed event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void txtCustomerID_TextChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("In txtCustomerID_TextChanged");
            this.BtnSearch_Click(sender, e);
        }

        public event EventHandler CustomerSearchClick;

        public void OnCustomerSearchClick(EventArgs e)
        {
            Debug.WriteLine("CustomerMaster OnSearchClick");
            if (CustomerSearchClick != null)
            {
                Debug.WriteLine("CustomerMaster OnSearchClick NOT NULL");
                CustomerSearchClick(this, e);
            }
        }

        /// <summary>Handles the BtnSearch Click event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("In BtnSearch_Click");

            Page.Validate("FindCustomer");

            if (Page.IsValid)
            {
                Debug.WriteLine("In Page.IsValid");
                Collection<CustomerEntity> myCustomers = CustomerEntity.GetSoldToByCustomerID(this.txtCustomerID.Text.PadLeft(10, '0'));

                if (myCustomers.Count > 0)
                {
                    if (myCustomers.Count == 1)
                    {
                        CustomerEntity cust = myCustomers[0];
                        this.lblCustomerAddress.Text = cust.Street;
                        this.lblGPO.Text = cust.GpoName;
                        this.lblCustGroup1.Text = cust.CustGroup1;

                        if (cust.CustGroup1ID != string.Empty)
                            this.LabelCustGroup1Filler.Text = " - ";
                        else
                            this.LabelCustGroup1Filler.Text = string.Empty;

                        this.lblCustGroup1ID.Text = cust.CustGroup1ID;
                        this.lblCustomerName.Text = cust.CustomerName;
                        this.lblCustomerCity.Text = cust.City;
                        this.lblCustomerState.Text = cust.StateCode;
                        this.lblCustomerZip.Text = cust.PostalCode;
                        this.hdnSalesOrg.Value = cust.SalesOrgId;
                        this.hdnCustomerID.Value = cust.CustomerID;

                        Collection<SalesRepEntity> myReps = SalesRepEntity.GetSalesRepsByCustomerID(this.txtCustomerID.Text.PadLeft(10, '0'));
                        this.LoadSalesRepDropDown(myReps);
                        Session["CustomerReps"] = myReps;

                        this.OnCustomerSearchClick(e);


                    }
                }
                else
                {
                    this.vldCustomerIDExists.IsValid = false;
                }
            }
        }

        /// <summary>Handles the ddlSalesRep Selected Index Changed event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlSalesRep_SelectedIndexChanged(object sender, EventArgs e)
        {
            Collection<SalesRepEntity> myReps = new Collection<SalesRepEntity>();
            if (Session["CustomerReps"] != null)
            {
                myReps = (Collection<SalesRepEntity>)Session["CustomerReps"];
            }
            else
            {
                myReps = SalesRepEntity.GetSalesRepsByCustomerID(this.txtCustomerID.Text.PadLeft(10, '0'));
            }

            Debug.WriteLine("myReps count: " + myReps.Count.ToString());

            foreach (SalesRepEntity rep in myReps)
            {
                Debug.WriteLine("myReps compare: " + Convert.ToBoolean(rep.RepID == this.ddlSalesRep.SelectedValue).ToString() + "; " + rep.RepID + " : " + this.ddlSalesRep.SelectedValue);

                if (rep.RepID == this.ddlSalesRep.SelectedValue)
                {
                    this.lblDeName.Text = rep.DistrictName;
                    this.lblDistrictID.Text = rep.DistrictID;
                    this.lblRvpName.Text = rep.RvpName;
                    break;
                }
            }
        }

        /// <summary>Loads the Sales Rep Drop Down List</summary>
        /// <param name="reps">Collect of<SalesRepEntity></param>
        public void LoadSalesRepDropDown(Collection<SalesRepEntity> reps)
        {
            this.ddlSalesRep.ClearSelection();
            this.ddlSalesRep.Items.Clear();
            this.ddlSalesRep.Items.Add(new ListItem("-- Select --", string.Empty));

            foreach (SalesRepEntity rep in reps)
            {
                this.ddlSalesRep.Items.Add(new ListItem(rep.RepName + " (" + rep.RepID.Substring(4, 6) + ")", rep.RepID));
            }
        }


    }//// end class
}