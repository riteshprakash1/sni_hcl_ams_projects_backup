﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerMasterHorizontalSummary.ascx.cs" Inherits="SalesRequestForms.UserControls.CustomerMasterHorizontalSummary" %>
<table width="960px">
    <tr>
        <td style="width:14%"><strong>Customer ID:</strong></td>
        <td style="width:36%"><asp:Label ID="lblCustomerID" runat="server"></asp:Label></td>
        <td style="width:15%"><strong>Sales Rep:</strong></td>
        <td style="width:35%"><asp:Label ID="lblSalesRepName" runat="server"></asp:Label><asp:Label ID="lblSalesRepID" runat="server" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td><strong>Customer Name:</strong></td>
        <td><asp:Label ID="lblCustomerName" runat="server"></asp:Label></td>
        <td><strong>DE:</strong></td>
        <td><asp:Label ID="lblDeName" runat="server"></asp:Label><asp:Label ID="lblDistrictID" runat="server" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td><strong>Address:</strong></td>
        <td><asp:Label ID="lblCustomerAddress" runat="server"></asp:Label></td>
        <td><strong>RVP:</strong></td>
        <td><asp:Label ID="lblRvpName" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td><strong>City, State, Zip:</strong></td>
        <td><asp:Label ID="lblCustomerCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblCustomerState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblCustomerZip" runat="server"></asp:Label></td>
        <td><strong>GPO:</strong></td>
        <td><asp:Label ID="lblGPO" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td><strong>Customer Group 1:</strong></td>
        <td><asp:Label ID="lblCustGroup1" runat="server"></asp:Label><asp:Label ID="LabelCustGroup1Filler" runat="server"></asp:Label><asp:Label ID="lblCustGroup1ID" runat="server"></asp:Label></td>
    </tr>
</table>