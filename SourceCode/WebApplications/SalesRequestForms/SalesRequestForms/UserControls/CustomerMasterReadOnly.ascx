﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerMasterReadOnly.ascx.cs" Inherits="SalesRequestForms.UserControls.CustomerMasterReadOnly" %>
<table width="100%">
    <tr>
        <td>Submitted By:</td>
        <td><asp:Label ID="lblSubmittedBy" runat="server"></asp:Label></td>
    </tr>    
    <tr>
        <td>Submitted Date:</td>
        <td><asp:Label ID="lblSubmittedDate" runat="server"></asp:Label></td>
    </tr>       
    <tr>
        <td style="width:32%">Customer Sold To Number:</td>
        <td><asp:Label ID="lblCustomerID" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Sales Rep:</td>
        <td><asp:Label ID="lblSalesRepName" runat="server"></asp:Label><asp:Label ID="lblSalesRepID" runat="server" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td>RVP:</td>
        <td><asp:Label ID="lblRvpName" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>DE:</td>
        <td><asp:Label ID="lblDeName" runat="server"></asp:Label><asp:Label ID="lblDistrictID" runat="server" Visible="false"></asp:Label></td>
    </tr>
    <tr>
        <td>Customer Name:</td>
        <td><asp:Label ID="lblCustomerName" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Customer Address:</td>
        <td><asp:Label ID="lblCustomerAddress" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Customer City, State, Zip:</td>
        <td><asp:Label ID="lblCustomerCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblCustomerState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblCustomerZip" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>GPO:</td>
        <td><asp:Label ID="lblGPO" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelCustGroup1" runat="server" Text="Customer Group1:"></asp:Label></td>
        <td><asp:Label ID="lblCustGroup1" runat="server"></asp:Label><asp:Label ID="LabelCustGroup1Filler" runat="server"></asp:Label><asp:Label ID="lblCustGroup1ID" runat="server"></asp:Label></td>
    </tr>
</table>