﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="SalesRequestForms._Default"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <table width="100%">
        <tr>
            <td style="font-size: large;text-decoration: underline;"><strong>Programs</strong></td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <asp:GridView ID="gvPrograms" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no programs." ShowFooter="false" OnRowEditing="GV_View" OnRowUpdating="GV_Update"
                        CellPadding="2" GridLines="Both" DataKeyNames="programID, programStatus, programFormName, programFolder" AllowPaging="false">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                        <asp:ButtonField ButtonType="Link" CausesValidation="false" HeaderStyle-Width="5%" Text="Details" CommandName="Edit" ItemStyle-VerticalAlign="Top" />
                        <asp:ButtonField ButtonType="Link" CausesValidation="false" HeaderStyle-Width="9%" Text="Request Form" CommandName="Update" ItemStyle-VerticalAlign="Top" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="11%" ItemStyle-Font-Bold="true" HeaderText="Program Name" DataField="programName" ItemStyle-VerticalAlign="Top" />
                        <asp:TemplateField HeaderStyle-Width="35%" HeaderText="Program Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProgramDescription" runat="server"  Text='<%# Eval("programdescription").ToString().Replace("\n", "<br/>")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Overview" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblOverview" runat="server"  Text='<%# Eval("overview").ToString().Replace("\n", "<br/>")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderStyle-Width="5%" HeaderText="Guide" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkGuide" Target="_blank" runat="server" Text="User"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-Width="5%" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Status" DataField="programStatus" />
                    </Columns>
                </asp:GridView> 
                
            </td>
            
        </tr>
        <tr>
            <td><asp:Label ID="lblAdminMsg" runat="server" Text="The status column and disabled programs are only visible to administrators."></asp:Label></td>
        </tr>
    </table>

 </asp:Content>
