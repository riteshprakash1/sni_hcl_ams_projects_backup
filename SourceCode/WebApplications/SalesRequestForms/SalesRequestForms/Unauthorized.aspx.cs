﻿// -------------------------------------------------------------
// <copyright file="Unauthorized.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms
{
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    
    /// <summary>
    /// This is the page users are redirected to if 
    /// they try to access a page they are not authorized view.
    /// </summary>
    public partial class Unauthorized : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string strTitle = string.Empty;

            if (Request.QueryString["PageTitle"] != null)
            {
                strTitle = Request.QueryString["PageTitle"] + " ";
            }

            Utility myUtility = (Utility)Session["Utility"];
            string strAdmins = String.Empty;

            if (myUtility != null)
            {
                //// This is the App Administrators.
                strAdmins = ConfigurationManager.AppSettings["AppAdministrator"];
                if (strAdmins != String.Empty)
                {
                    strAdmins = ", " + strAdmins;
                }
            }

            string strMsg = "You do not have permission to view the " + strTitle;
            strMsg += "page.  If you believe you should have access permission, please ";
            strMsg += "contact the application administrator(s)";
            strMsg += strAdmins;
            strMsg += ".";
            
            this.lblMsg.Text = strMsg;
        }
    } //// end class
}
