﻿// --------------------------------------------------------------
// <copyright file="Site.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRequestForms
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;

    /// <summary>This is the Master page for the Web Application.</summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        /// <summary>Handles the OnInit event of the Page control.  This fixes Menu rendering in Chrome and Safari.</summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {    // For Chrome and Safari    
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                if (Request.Browser.Adapters.Count > 0)
                {
                    Request.Browser.Adapters.Clear();
                    Response.Redirect(Page.Request.Url.AbsoluteUri);
                }
            }
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl ("~/Scripts/ShowHint.js")); 

            // Will redirect to MessageBoard page if set to true and not equal to MessageBoard
            if(this.ShowMessageBoard())
            {
                    Response.Redirect(ConfigurationManager.AppSettings["SiteURL"] + ConfigurationManager.AppSettings["ShowMessagePageName"], true);
            }


            if (!Page.IsPostBack)
            {
 //               Response.Write("User Agent: " + Request.Headers["User-Agent"].ToLower());
                this.NavMenu.Items.Add(new MenuItem("Home", "Default.aspx"));
                this.NavMenu.Items.Add(new MenuItem("Price Request Search", "", "", "~/PricingRequest/PriceRequestSearch.aspx"));

                try
                {
                    List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

                    if (AppUsersEntity.IsAdmin(myUsers))
                        this.NavMenu.Items.Add(new MenuItem("Administration", "", "", "~/Admin/Default.aspx"));

                }
                catch (Exception ex)
                {
                    LogException.HandleException(ex, Request);
                }

            }
        }

        /// <summary>Determines whether to redirect to the message board page.</summary>
        /// <returns>true/false</returns>
        protected bool ShowMessageBoard()
        {
            bool blShowMessageBoard = false;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ShowMessageBoard"]))
            {
                string sPath = HttpContext.Current.Request.Url.AbsolutePath;
                string[] strarry = sPath.Split('/');
                int lengh = strarry.Length;
                string sPageName = strarry[lengh - 1];

                Debug.WriteLine("Page Name: " + sPageName);
                string strRedirectPageName = ConfigurationManager.AppSettings["ShowMessagePageName"].ToLower();


                blShowMessageBoard = Convert.ToBoolean((sPageName.ToLower() != ConfigurationManager.AppSettings["ShowMessagePageName"].ToLower()) &&
                    (sPageName.ToLower() != "adminconfig.aspx"));
            }

            return blShowMessageBoard;
        }

        /// <summary>Called when MenuItem is clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.MenuEventArgs"/> instance containing the event data.</param>
        public void OnClick(Object sender, MenuEventArgs e)
        {
            Debug.WriteLine("In OnClick");
            //MessageLabel.Text = "You selected " + e.Item.Text + ".";
            Debug.WriteLine("Value: " + e.Item.Value + "; Text: " + e.Item.Text);
            e.Item.Selected = true;
            string strPage = "~/" + e.Item.Value.Replace("Admin", "Admin/");
            Response.Redirect(strPage, true);
        }

        /// <summary>Handles the Click event of the btnAppName control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAppName_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx", true);
        }
    } //// end class
}