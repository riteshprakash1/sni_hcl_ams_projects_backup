﻿<%@ Page Title="Program Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProgramDetail.aspx.cs" Inherits="SalesRequestForms.ProgramDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="920px" cellpadding="3" cellspacing="3">
        <tr>
            <td colspan="2" style="font-size: large;text-decoration: underline;"><strong>Program Detail</strong></td>
        </tr>        
        <tr>
            <td style="width:14%"><b>Program Name: </b></td>
            <td><asp:Label ID="lblProgramName" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td style="vertical-align:top;"><b>Overview: </b></td>
            <td style="vertical-align:top"><asp:Label ID="lblOverview" runat="server"></asp:Label></td>
        </tr>    
        <tr>
            <td style="vertical-align:top"><b>Guidelines: </b></td>
            <td style="vertical-align:top"><asp:Label ID="lblGuidelines" runat="server"></asp:Label></td>
        </tr>    
        <tr>
            <td style="vertical-align:top"><b>Program Description: </b></td>
            <td style="vertical-align:top"><asp:Label ID="lblProgramDescription" runat="server"></asp:Label></td>
        </tr>    
        <tr>
            <td style="vertical-align:top"><b>Features & Benefits: </b></td>
            <td style="vertical-align:top"><asp:Label ID="lblFeaturesBenefits" runat="server"></asp:Label></td>
        </tr>    
        <tr>
            <td style="vertical-align:top"><b>When to Use: </b></td>
            <td style="vertical-align:top"><asp:Label ID="lblWhenToUse" runat="server"></asp:Label></td>
        </tr>  
        
        <tr>
            <td style="text-align:center" colspan="4">
                <asp:Button ID="btnHome" runat="server" CausesValidation="False" 
                    CssClass="buttonsSubmit" Text="Home" onclick="btnHome_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRequestForm" runat="server" CausesValidation="False" 
                CssClass="buttonsSubmit" Text="Request Form" onclick="btnRequestForm_Click" /> 
            </td>   
        </tr>
    </table>
</asp:Content>