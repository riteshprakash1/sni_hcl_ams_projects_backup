﻿// -------------------------------------------------------------
// <copyright file="PlacedCapitalRequest.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PlacedCapital
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PlacedCapital.Classes;

    /// <summary>This is the Placed Capital Request page.</summary>   
    public partial class PlacedCapitalRequest : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>        
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("In PlacedCapitalRequest Page_Load");
            if (!Page.IsPostBack)
            {
                CustomerMasterReadOnly1.CustomerGroup1Visible(false);

                EnterButton.TieButton(this.txtPlacedCapitalRequestID, btnSubmit); 
                Debug.WriteLine("requestID: " + Request.QueryString["requestID"]);
                this.lblPlacedCapitalRequestMsg.Text = "Placed Capital Request not found";

                if (Request.QueryString["requestID"] != null)
                {
                    this.txtPlacedCapitalRequestID.Text = Request.QueryString["requestID"];
                    PlacedCapitalRequestEntity pc = PlacedCapitalRequestEntity.GetPlacedCapitalRequestByID(Convert.ToInt32(this.txtPlacedCapitalRequestID.Text));
                    if (pc != null)
                    {
                        List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));

                        if (AppUsersEntity.IsProgramAdmin(myUsers, Convert.ToInt32(this.hdnProgramID.Value)) || (pc.CreatedBy.ToLower() == ADUtility.GetUserNameOfAppUser(Request).ToLower()))
                        {
                            this.lblPlacedCapitalRequestMsg.Text = string.Empty;
                            this.LoadPageFromPlacedCapitalRequestEntity(pc);
                        }
                        else
                        {
                            this.lblPlacedCapitalRequestMsg.Text = "You are not authorized to view the request.  You must be the creator of the request or an Administrator.";
                        }
                    }
                }
            }
        } //// end Page_Load

        /// <summary>Loads the page controls from the PlacedCapitalRequestEntity instance</summary>
        /// <param name="pc">the PlacedCapitalRequestEntity instance</param>
        private void LoadPageFromPlacedCapitalRequestEntity(PlacedCapitalRequestEntity pc)
        {
            this.lblContactName.Text = pc.FacilityContact;
            this.lblEmailFax.Text = pc.EmailFax;
            //this.lblContractTerm.Text = pc.ContractTerm;
            this.lblCustomerPO.Text = pc.CustomerPO;
            this.lblShipToCustomerAddress.Text = pc.ShipToStreet;
            this.lblShipToCustomerCity.Text = pc.ShipToCity;
            this.lblShipToCustomerName.Text = pc.ShipToCustomerName;
            this.lblShipToCustomerState.Text = pc.ShipToStateCode;
            this.lblShipToCustomerZip.Text = pc.ShipToPostalCode;
            this.lblShipToCustomerID.Text = pc.ShipToCustomerID;

            CustomerMasterReadOnly1.SubmittedDate = pc.CreateDate;
            CustomerMasterReadOnly1.SubmittedBy = pc.SubmittedByName;
            CustomerMasterReadOnly1.DeName = pc.DistrictName;
            CustomerMasterReadOnly1.RvpName = pc.RvpName;
            CustomerMasterReadOnly1.GPO = pc.GpoDesc;
            CustomerMasterReadOnly1.RepName = pc.SalesRepName;
            CustomerMasterReadOnly1.CustomerID = pc.SoldToCustomerID;
            CustomerMasterReadOnly1.CustomerName = pc.SoldToCustomerName;
            CustomerMasterReadOnly1.CustomerAddress = pc.SoldToStreet;
            CustomerMasterReadOnly1.CustomerCity = pc.SoldToCity;
            CustomerMasterReadOnly1.CustomerState = pc.SoldToStateCode;
            CustomerMasterReadOnly1.CustomerZip = pc.SoldToPostalCode;

            Collection<MaterialEntity> materials = MaterialEntity.GetPlacedCapitalMaterialRequestByID("sp_GetPlacedCapitalMaterialRequestsByID", pc.PlacedCapitalRequestID);
            this.BindMaterial(materials);

            Collection<MaterialEntity> materialSummary = MaterialEntity.GetPlacedCapitalMaterialSummaryFromDetailCollection(materials);
            this.BindSummaryMaterial(materialSummary);

            Collection<CustomerEntity> additioanlCustomers = CustomerEntity.GetAdditionalCustomerCollection(pc.PlacedCapitalRequestID);
            this.BindAdditionalCustomers(additioanlCustomers);
        }

        /// <summary>Binds the Customer entity collection to the gvSoldTo GridView.</summary>
        /// <param name="customers">customer entity collection</param>
        protected void BindAdditionalCustomers(Collection<CustomerEntity> customers)
        {
            this.gvSoldTo.DataSource = customers;
            this.gvSoldTo.DataBind();
        }


        /// <summary>Binds the summary material entity collection to the gvSummaryMaterial GridView.</summary>
        /// <param name="materials">material entity collection</param>
        protected void BindSummaryMaterial(Collection<MaterialEntity> materials)
        {
            this.gvSummaryMaterial.DataSource = materials;
            this.gvSummaryMaterial.DataBind();
        }

        /// <summary>Binds the material entity collection to the gvMaterial GridView.</summary>
        /// <param name="materials">material entity collection</param>
        protected void BindMaterial(Collection<MaterialEntity> materials)
        {

            this.gvMaterial.DataSource = materials;
            this.gvMaterial.DataBind();
        }

        protected void txtPlacedCapitalRequestID_TextChanged(object sender, EventArgs e)
        {
            Page.Validate();

            if(Page.IsValid)
                Response.Redirect("PlacedCapitalRequest.aspx?requestID=" + this.txtPlacedCapitalRequestID.Text, true);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid)
                Response.Redirect("PlacedCapitalRequest.aspx?requestID=" + this.txtPlacedCapitalRequestID.Text, true);
        }

    }//// end class
}