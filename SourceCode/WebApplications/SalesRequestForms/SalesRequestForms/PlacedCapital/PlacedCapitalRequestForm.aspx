﻿<%@ Page Title="Placed Capital Request Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlacedCapitalRequestForm.aspx.cs" Inherits="SalesRequestForms.PlacedCapital.PlacedCapitalRequestForm" %>
<%@ Register src="../UserControls/CustomerMaster.ascx" tagname="CustomerMaster" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="text-align:center;font-size: medium;"><strong>PLACED CAPITAL REQUEST FORM</strong></td>
        </tr>
    </table>
    <table width="100%" style="background-color: #FFFF00">
        <tr>
            <td style="font-weight:bold; font-size:10pt"><asp:Label ID="lblCompletionTime" runat="server" Text="ONCE REQUEST IS SUBMITTED, PLEASE ALLOW UP TO 3-5 DAYS FOR COMPLETION."></asp:Label></td>
        </tr>
        <tr>
            <td style="font-weight:bold; font-size:10pt"><asp:Label ID="lblRefurbishNote" runat="server" Text="*Note - refurbished product will be used whenever possible"></asp:Label></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td><asp:HyperLink ID="lnkInstructionsLabel" runat="server" CssClass="hintanchor" NavigateUrl="#">Form Instructions</asp:HyperLink></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td style="width:50%; vertical-align:top">
            <uc1:CustomerMaster ID="CustomerMaster1" runat="server" />
                <asp:HiddenField ID="hdnProgramID" Value="1" runat="server" />
                <asp:HiddenField ID="hdnSubmittedBy" runat="server" /><asp:HiddenField ID="hdnFormStamp" runat="server" />
                    <table width="100%">
                       <tr>
                            <td style="width:30%">Contact Name at Facility: *</td>
                            <td><asp:TextBox ID="txtContactName" MaxLength="50" runat="server" Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredContactName" ControlToValidate="txtContactName" runat="server" ErrorMessage="Contact Name at Facility is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>        
                        <tr>
                            <td>Email: *</td>
                            <td><asp:TextBox ID="txtEmailFax" MaxLength="50" runat="server" Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredEmail" ControlToValidate="txtEmailFax" runat="server" ErrorMessage="Email is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="vldEmailFormat"  runat="server" ControlToValidate="txtEmailFax" ErrorMessage="Email format is not correct." ForeColor="#FF7300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Customer PO#: *</td>
                            <td><asp:TextBox ID="txtCustomerPO" MaxLength="50" runat="server" Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredCustomerPO" ControlToValidate="txtCustomerPO" runat="server" ErrorMessage="Customer PO# is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldSubmission" runat="server" ErrorMessage="This Placed Capital Request was already submitted." ForeColor="#FF7300">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">* - indicates required fields</td>
                        </tr>
                    </table>           
                </td>
            <td style="width:50%; vertical-align:top">
                <table width="100%">
                    <tr>
                        <td style="color: red; text-decoration: underline; font-weight:bold; text-align: center;">COMPLIANCE INFORMATION REQUIRED</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Does Customer Purchase Related Disposables from a different facility other than where equipment will be 
                        located?  IF SO, please list additional Customer numbers below so this revenue can be considered 
                        against the annual disposable commitments.
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:160; font-weight:bold">Additional Sold To Numbers:</td>
                    </tr>
                    <tr>
                         <td>
                             <asp:GridView ID="gvSoldTo" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GV_SoldToRowDataBound"
                                 OnRowDeleting="GV_SoldToDelete"  ShowFooter="true" DataKeyNames="CustomerID" 
                                 CellPadding="2" GridLines="Both" OnRowCancelingEdit="GV_SoldToAdd">
                                <RowStyle HorizontalAlign="left" />
                                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                                <Columns>
                                      <asp:TemplateField HeaderStyle-Width="12%" ItemStyle-HorizontalAlign="Left" FooterStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							                    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								                ></asp:Button>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							                    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>                                
					                    </FooterTemplate>
                                    </asp:TemplateField> 
                                   <asp:TemplateField HeaderStyle-Width="24%" HeaderText="Customer ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerID" Font-Size="8pt" runat="server"  Text='<%# Eval("CustomerID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtCustomerID" MaxLength="10" width="80px" Font-Size="8pt" runat="server"></asp:TextBox>
                                            <asp:CustomValidator ID="vldCustomerIDExistsSubmitted" runat="server" ForeColor="#FF7300" 
                                                ErrorMessage="The Additional Sold To Customer Account Number was not found.">*</asp:CustomValidator>
                                            <asp:CustomValidator ID="vldCustomerIDExists" runat="server" ForeColor="#FF7300" ValidationGroup="AdditionalSoldTo" 
                                                ErrorMessage="The Additional Sold To Customer Account Number was not found.">*</asp:CustomValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>                            
                                   <asp:TemplateField HeaderStyle-Width="64%" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerName" Font-Size="8pt" runat="server"  Text='<%# Eval("CustomerName")%>'></asp:Label>
                                        </ItemTemplate>
                                   </asp:TemplateField>                            
                           
                               </Columns>
                            </asp:GridView>                
                            <asp:Label ID="lblAddCustomerMsg" Font-Size="8pt" runat="server"  Text="Click 'Add' after each entry if needing multiple selections."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>Customer Ship To</b> if Different than Customer Sold to Ship to;</td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtShipToCustomerID" runat="server" MaxLength="10" width="80px"
                            AutoPostBack="True" ontextchanged="txtShipToCustomerID_TextChanged"></asp:TextBox>
                            <asp:CustomValidator ID="vldShipToCustomerIDExists" runat="server" ControlToValidate="txtShipToCustomerID" 
                                ErrorMessage="The Ship To Customer Account Number was not found." ForeColor="#FF7300" ValidationGroup="FindShipToCustomer">*</asp:CustomValidator>
                            <asp:CustomValidator ID="vldShipToCustomerIDExistsSubmit" runat="server" ControlToValidate="txtShipToCustomerID" 
                                ErrorMessage="The Ship To Customer Account Number was not found." ForeColor="#FF7300">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShipToCustomerName" runat="server"></asp:Label><br />
                            <asp:Label ID="lblShipToCustomerAddress" runat="server"></asp:Label><br />
                            <asp:Label ID="lblShipToCustomerCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblShipToCustomerState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblShipToCustomerZip" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            
            </td>
            
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td style="text-align:center">
                <asp:Button ID="btnSubmit" runat="server" CausesValidation="False" 
                CssClass="buttonsSubmit" Text="Submit Request" onclick="btnSubmit_Click" /></td>
        </tr>
         <tr>
            <td><asp:ValidationSummary ID="ValidationSummary3" runat="server" ForeColor="#FF7300" Font-Size="10pt"/>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" Font-Size="10pt"/>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="AdditionalSoldTo" ForeColor="#FF7300" Font-Size="10pt"/>
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ValidationGroup="FindShipToCustomer" ForeColor="#FF7300" Font-Size="10pt"/>
            </td>
        </tr>       
        <tr>
            <td><asp:CustomValidator ID="vldMaterialListSubmission" runat="server" ErrorMessage="No Material Quantities entered." ForeColor="#FF7300">*</asp:CustomValidator><br />
                <asp:GridView ID="gvMaterial" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound" 
                    ShowFooter="false" DataKeyNames="MaterialID" OnRowEditing="GV_MaterialEdit" CellPadding="2" GridLines="Both">
                <RowStyle HorizontalAlign="left" />
                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="13%" HeaderText="Capital Group" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblCapitalGroup" Font-Size="8pt" runat="server"  Text='<%# Eval("CapitalGroup")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                    <asp:TemplateField HeaderStyle-Width="8%" HeaderText="Qty" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtQty" MaxLength="10" width="40px" Font-Size="8pt" runat="server" AutoPostBack="true" Text='<%# Eval("Qty")%>' ontextchanged="txtMaterialQty_TextChanged"></asp:TextBox>
                            <asp:CompareValidator ID="vldQty" runat="server" ControlToValidate="txtQty" ForeColor="#FF7300" ValueToCompare="0"
                            ErrorMessage="The Qty must be an integer greater than or equal to zero." ValidationGroup="UpdateMaterial"  
                            Operator="GreaterThanEqual" Type="Integer">*</asp:CompareValidator>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                    <asp:TemplateField HeaderStyle-Width="13%" HeaderText="Material ID" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblMaterialID" Font-Size="8pt" runat="server"  Text='<%# Eval("MaterialID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                    <asp:TemplateField HeaderStyle-Width="38%" HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblMaterialDesc" Font-Size="8pt" runat="server"  Text='<%# Eval("MaterialDesc")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                    <asp:TemplateField HeaderText="Annual Value of Equipment" ItemStyle-Font-Size="8pt" HeaderStyle-HorizontalAlign="Right" ItemStyle-CssClass="TARight" HeaderStyle-Width="14%">
                        <ItemTemplate>
                            <asp:Label ID="lblAnnualValue" Font-Size="8pt" runat="server"  Text='<%#String.Format("{0:c0}",Eval("AnnualValue")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                    <asp:TemplateField HeaderText="Annual Target Purchase" ItemStyle-Font-Size="8pt" HeaderStyle-HorizontalAlign="Right" ItemStyle-CssClass="TARight" HeaderStyle-Width="14%">
                        <ItemTemplate>
                            <asp:Label ID="lblAnnualRequiredPurchases" Font-Size="8pt" runat="server"  Text='<%#String.Format("{0:c0}",Eval("AnnualRequiredPurchases")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                </Columns>
            </asp:GridView>  
            </td>
        </tr>
        <tr>
            <td><asp:ValidationSummary ID="vldMaterialSummary" runat="server" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" Font-Size="10pt" /></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td style="width:60%;">
                <table width="100%">
                    <tr>
                         <td style="width:100%; font-size:10pt; background-color: #FFFF00;color: #CC3300; font-weight:bold; text-align:center">
                            <asp:Label ID="lblSummaryNote" runat="server"  Text="CUSTOMER MUST MEET THEIR ANNUAL TARGET <span style=&quot;color: black&quot;>PURCHASE</span> LISTED IN THE TABLE TO THE RIGHT PER CAPITAL GROUP, IF CUSTOMER CANNOT COMMIT TO THE ANNUAL TARGET PURCHASE PER CAPITAL GROUP, THIS REQUEST WILL BE REJECTED."></asp:Label>
                        </td>                   
                    </tr>
                  </table>  
            </td>
            <td align="center">
                <asp:GridView ID="gvSummaryMaterial" runat="server" Width="270px" AutoGenerateColumns="False" HorizontalAlign="Right"
                    ShowFooter="false" DataKeyNames="MaterialID" CellPadding="2" GridLines="Both">
                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Capital Group" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblCapitalGroup" Font-Size="8pt" runat="server"  Text='<%# Eval("CapitalGroup")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                    <asp:TemplateField ItemStyle-Font-Size="8pt" HeaderText="TARGET PURCHASE" HeaderStyle-CssClass="TARight"  ItemStyle-CssClass="TARight" HeaderStyle-Width="50%">
                        <ItemTemplate>
                            <asp:Label ID="lblAnnualRequiredPurchases" Font-Size="8pt" runat="server"  Text='<%#String.Format("{0:c0}",Eval("AnnualRequiredPurchases")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                            
                </Columns>
            </asp:GridView>  
            </td>
        </tr>
    </table>
</asp:Content>