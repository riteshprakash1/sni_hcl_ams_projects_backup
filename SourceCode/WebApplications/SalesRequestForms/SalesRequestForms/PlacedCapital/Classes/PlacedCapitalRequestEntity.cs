﻿// ------------------------------------------------------------------
// <copyright file="PlacedCapitalRequestEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PlacedCapital.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using SalesRequestForms.Classes;

    /// <summary>
    /// This is the Placed Capital Request Entity.
    /// </summary>
    public class PlacedCapitalRequestEntity
    {
        /// <summary>This is the PlacedCapitalRequestID.</summary>
        private int intPlacedCapitalRequestID;

        /// <summary>This is the ProgramID.</summary>
        private int intProgramID;

        /// <summary>This is the SoldToCustomerID.</summary>
        private string strSoldToCustomerID;

        /// <summary>This is the SoldToCustomerName.</summary>
        private string strSoldToCustomerName;

        /// <summary>This is the SoldToStreet.</summary>
        private string strSoldToStreet;

        /// <summary>This is the SoldToCity.</summary>
        private string strSoldToCity;

        /// <summary>This is the SoldToStateCode.</summary>
        private string strSoldToStateCode;

        /// <summary>This is the SoldToPostalCode.</summary>
        private string strSoldToPostalCode;

        /// <summary>This is the SoldToPostalCode.</summary>
        private string strRvpName;

        /// <summary>This is the DistrictName.</summary>
        private string strDistrictName;

        /// <summary>This is the SalesRepName.</summary>
        private string strSalesRepName;

        /// <summary>This is the SalesRepID.</summary>
        private string strSalesRepID;

        /// <summary>This is the GpoDesc.</summary>
        private string strGpoDesc;

        /// <summary>This is the ShipToCustomerID.</summary>
        private string strShipToCustomerID;

        /// <summary>This is the ShipToCustomerName.</summary>
        private string strShipToCustomerName;

        /// <summary>This is the ShipToStreet.</summary>
        private string strShipToStreet;

        /// <summary>This is the ShipToCity.</summary>
        private string strShipToCity;

        /// <summary>This is the ShipToStateCode.</summary>
        private string strShipToStateCode;

        /// <summary>This is the ShipToPostalCode.</summary>
        private string strShipToPostalCode;

        /// <summary>This is the EmailFax.</summary>
        private string strEmailFax;

        /// <summary>This is the FacilityContact.</summary>
        private string strFacilityContact;

        /// <summary>This is the ContractTerm.</summary>
        private string strContractTerm;

        /// <summary>This is the FormStamp.</summary>
        private string strFormStamp;

        /// <summary>This is the CreatedBy.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>This is the ModifiedBy.</summary>
        private string strModifiedBy;

        /// <summary>This is the ModifiedDate.</summary>
        private string strModifiedDate;

        /// <summary>This is the Submitted By Name.</summary>
        private string strSubmittedByName;

        /// <summary>This is the Customer PO.</summary>
        private string strCustomerPO;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlacedCapitalRequestEntity"/> class.
        /// </summary>
        public PlacedCapitalRequestEntity()
        {
            this.PlacedCapitalRequestID = 0;
            this.ProgramID = 0;
            this.SoldToCustomerID = string.Empty;
            this.SoldToCustomerName = string.Empty;
            this.SoldToStreet = string.Empty;
            this.SoldToCity = string.Empty;
            this.SoldToStateCode = string.Empty;
            this.SoldToPostalCode = string.Empty;
            this.RvpName = string.Empty;
            this.DistrictName = string.Empty;
            this.SalesRepName = string.Empty;
            this.SalesRepID = string.Empty;
            this.GpoDesc = string.Empty;
            this.ShipToCustomerID = string.Empty;
            this.ShipToCustomerName = string.Empty;
            this.ShipToStreet = string.Empty;
            this.ShipToCity = string.Empty;
            this.ShipToStateCode = string.Empty;
            this.ShipToPostalCode = string.Empty;
            this.EmailFax = string.Empty;
            this.FacilityContact = string.Empty;
            this.ContractTerm = string.Empty;
            this.CreatedBy = string.Empty;
            this.ModifiedBy = string.Empty;
            this.SubmittedByName = string.Empty;
            this.CustomerPO = string.Empty;
            this.FormStamp = string.Empty;
        }

        /// <summary>Gets or sets the PlacedCapitalRequestID.</summary>         
        /// <value>The PlacedCapitalRequestID.</value> 
        public int PlacedCapitalRequestID
        {  
            get{ return this.intPlacedCapitalRequestID;}
            set { this.intPlacedCapitalRequestID = value; } 
        }

        /// <summary>Gets or sets the ProgramID.</summary>         
        /// <value>The ProgramID.</value> 
        public int ProgramID
        {  
            get{ return this.intProgramID;}
            set { this.intProgramID = value; } 
        }
        
        /// <summary>Gets or sets the SoldToCustomerID.</summary>         
        /// <value>The SoldToCustomerID.</value> 
        public string SoldToCustomerID
        {  
            get{ return this.strSoldToCustomerID;}
            set { this.strSoldToCustomerID = value; } 
        }
        
        /// <summary>Gets or sets the SoldToCustomerName.</summary>         
        /// <value>The SoldToCustomerName.</value> 
        public string SoldToCustomerName
        {  get{ return this.strSoldToCustomerName;}
            set { this.strSoldToCustomerName = value; } 
        }

        
        /// <summary>Gets or sets the SoldToStreet.</summary>         
        /// <value>The SoldToStreet.</value> 
        public string SoldToStreet
        {  
            get{ return this.strSoldToStreet;}
            set { this.strSoldToStreet = value; } 
        }
        
        /// <summary>Gets or sets the SoldToCity.</summary>         
        /// <value>The SoldToCity.</value> 
        public string SoldToCity
        {  
            get{ return this.strSoldToCity;}
            set { this.strSoldToCity = value; } 
        }
        
        /// <summary>Gets or sets the SoldToStateCode.</summary>         
        /// <value>The SoldToStateCode.</value> 
        public string SoldToStateCode
        {  
            get{ return this.strSoldToStateCode;}
            set { this.strSoldToStateCode = value; } 
        }
        
        /// <summary>Gets or sets the SoldToPostalCode.</summary>         
        /// <value>The SoldToPostalCode.</value> 
        public string SoldToPostalCode
        {  
            get{ return this.strSoldToPostalCode;}
            set { this.strSoldToPostalCode = value; } 
        }

        /// <summary>Gets or sets the RvpName.</summary>         
        /// <value>The RvpName.</value> 
        public string RvpName
        {
            get { return this.strRvpName; }
            set { this.strRvpName = value; }
        }
        /// <summary>Gets or sets the DistrictName.</summary>         
        /// <value>The DistrictName.</value> 
        public string DistrictName
        {
            get { return this.strDistrictName; }
            set { this.strDistrictName = value; }
        }
        /// <summary>Gets or sets the SalesRepName.</summary>         
        /// <value>The SalesRepName.</value> 
        public string SalesRepName
        {
            get { return this.strSalesRepName; }
            set { this.strSalesRepName = value; }
        }

        /// <summary>Gets or sets the SalesRepName.</summary>         
        /// <value>The SalesRepName.</value> 
        public string SalesRepID
        {
            get { return this.strSalesRepID; }
            set { this.strSalesRepID = value; }
        }

        /// <summary>Gets or sets the GpoDesc.</summary>         
        /// <value>The GpoDesc.</value> 
        public string GpoDesc
        {
            get { return this.strGpoDesc; }
            set { this.strGpoDesc = value; }
        }

        /// <summary>Gets or sets the ShipToCustomerID.</summary>         
        /// <value>The ShipToCustomerID.</value> 
        public string ShipToCustomerID
        {  
            get{ return this.strShipToCustomerID;}
            set { this.strShipToCustomerID = value; } 
        }
        
        /// <summary>Gets or sets the ShipToCustomerName.</summary>         
        /// <value>The ShipToCustomerName.</value> 
        public string ShipToCustomerName
        {  
            get{ return this.strShipToCustomerName;}
            set { this.strShipToCustomerName = value; } 
        }
        
        /// <summary>Gets or sets the ShipToStreet.</summary>         
        /// <value>The ShipToStreet.</value> 
        public string ShipToStreet
        {  
            get{ return this.strShipToStreet;}
            set { this.strShipToStreet = value; } 
        }
        
        /// <summary>Gets or sets the ShipToCity.</summary>         
        /// <value>The ShipToCity.</value> 
        public string ShipToCity
        {  
            get{ return this.strShipToCity;}
            set { this.strShipToCity = value; } 
        }

        /// <summary>Gets or sets the ShipToStateCode.</summary>         
        /// <value>The ShipToStateCode.</value> 
        public string ShipToStateCode
        {  
            get{ return this.strShipToStateCode;}
            set { this.strShipToStateCode = value; } 
        }

        /// <summary>Gets or sets the ShipToPostalCode.</summary>         
        /// <value>The ShipToPostalCode.</value> 
        public string ShipToPostalCode
        {  
            get{ return this.strShipToPostalCode;}
            set { this.strShipToPostalCode = value; } 
        }

        /// <summary>Gets or sets the EmailFax.</summary>         
        /// <value>The EmailFax.</value> 
        public string EmailFax
        {  
            get{ return this.strEmailFax;}
            set { this.strEmailFax = value; } 
        }

        /// <summary>Gets or sets the FacilityContact.</summary>         
        /// <value>The FacilityContact.</value> 
        public string FacilityContact
        {
            get { return this.strFacilityContact; }
            set { this.strFacilityContact = value; } 
        }

        /// <summary>Gets or sets the ContractTerm.</summary>         
        /// <value>The ContractTerm.</value> 
        public string ContractTerm
        {
            get { return this.strContractTerm; }
            set { this.strContractTerm = value; } 
        }

        /// <summary>Gets or sets the FormStamp.</summary>         
        /// <value>The FormStamp.</value> 
        public string FormStamp
        {
            get { return this.strFormStamp; }
            set { this.strFormStamp = value; } 
        }
        
        /// <summary>Gets or sets the CreatedBy.</summary>         
        /// <value>The CreatedBy.</value> 
        public string CreatedBy
        {  
            get{ return this.strCreatedBy;}
            set { this.strCreatedBy = value; } 
        }

        /// <summary>Gets or sets the CreateDate.</summary>         
        /// <value>The CreateDate.</value> 
        public string CreateDate
        {  get{ return this.strCreateDate;}
            set { this.strCreateDate = value; } 
        }

        /// <summary>Gets or sets the ModifiedBy.</summary>         
        /// <value>The ModifiedBy.</value> 
        public string ModifiedBy
        {  
            get{ return this.strModifiedBy;}
            set { this.strModifiedBy = value; } 
        }

        /// <summary>Gets or sets the ModifiedDate.</summary>         
        /// <value>The ModifiedDate.</value> 
        public string ModifiedDate
        {  
            get{ return this.strModifiedDate;}
            set { this.strModifiedDate = value; } 
        }

        /// <summary>Gets or sets the SubmittedByName.</summary>         
        /// <value>The SubmittedByName.</value> 
        public string SubmittedByName
        {
            get { return this.strSubmittedByName; }
            set { this.strSubmittedByName = value; }
        }

        /// <summary>Gets or sets the CustomerPO.</summary>         
        /// <value>The CustomerPO.</value> 
        public string CustomerPO
        {
            get { return this.strCustomerPO; }
            set { this.strCustomerPO = value; }
        }

        /// <summary>Inserts The Placed Capital Request and Materials into the database</summary>
        /// <param name="pcRequest">PlaceCapitalRequest object</param>
        /// <param name="materials">Collection<MaterialEntity></param>
        /// <returns>The PlacedCapitalRequest Entity<PlacedCapitalRequestEntity></returns>
        public static PlacedCapitalRequestEntity InsertPlacedCapitalRequest(PlacedCapitalRequestEntity pcRequest, Collection<MaterialEntity> materials)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();

            SqlParameter SQLID = new SqlParameter("@PlacedCapitalRequestID", pcRequest.PlacedCapitalRequestID);
            SQLID.Direction = ParameterDirection.Output;
            myParamters.Add(SQLID);
            
            myParamters.Add(new SqlParameter("@ProgramID", pcRequest.ProgramID));
            myParamters.Add(new SqlParameter("@SoldToCustomerID", pcRequest.SoldToCustomerID));
            myParamters.Add(new SqlParameter("@SoldToCustomerName", pcRequest.SoldToCustomerName));
            myParamters.Add(new SqlParameter("@SoldToStreet", pcRequest.SoldToStreet));
            myParamters.Add(new SqlParameter("@SoldToCity", pcRequest.SoldToCity));
            myParamters.Add(new SqlParameter("@SoldToStateCode", pcRequest.SoldToStateCode));
            myParamters.Add(new SqlParameter("@SoldToPostalCode", pcRequest.SoldToPostalCode));
            myParamters.Add(new SqlParameter("@RvpName", pcRequest.RvpName));
            myParamters.Add(new SqlParameter("@DistrictName", pcRequest.DistrictName));
            myParamters.Add(new SqlParameter("@SalesRepName", pcRequest.SalesRepName));
            myParamters.Add(new SqlParameter("@SalesRepID", pcRequest.SalesRepID));
            myParamters.Add(new SqlParameter("@GpoDesc", pcRequest.GpoDesc));
            myParamters.Add(new SqlParameter("@ShipToCustomerID", pcRequest.ShipToCustomerID));
            myParamters.Add(new SqlParameter("@ShipToCustomerName", pcRequest.ShipToCustomerName));
            myParamters.Add(new SqlParameter("@ShipToStreet", pcRequest.ShipToStreet));
            myParamters.Add(new SqlParameter("@ShipToCity", pcRequest.ShipToCity));
            myParamters.Add(new SqlParameter("@ShipToStateCode", pcRequest.ShipToStateCode));
            myParamters.Add(new SqlParameter("@ShipToPostalCode", pcRequest.ShipToPostalCode));
            myParamters.Add(new SqlParameter("@EmailFax", pcRequest.EmailFax));
            myParamters.Add(new SqlParameter("@FacilityContact", pcRequest.FacilityContact));
            myParamters.Add(new SqlParameter("@ContractTerm", pcRequest.ContractTerm));
            myParamters.Add(new SqlParameter("@FormStamp", pcRequest.FormStamp));
            myParamters.Add(new SqlParameter("@ModifiedBy", pcRequest.ModifiedBy));
            myParamters.Add(new SqlParameter("@SubmittedByName", pcRequest.SubmittedByName));
            myParamters.Add(new SqlParameter("@CustomerPO", pcRequest.CustomerPO));

            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            SqlParameterCollection updatedParamteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertPlacedCapitalRequest", myParamters, conn, trans);

            if (updatedParamteres["@PlacedCapitalRequestID"] != null)
            {
                SqlParameter requestID = updatedParamteres["@PlacedCapitalRequestID"];
                pcRequest.PlacedCapitalRequestID = Convert.ToInt32(requestID.Value.ToString());
                
                // loop thru the MaterialEntity collection and insert them into the database
                foreach (MaterialEntity material in materials)
                {
                    material.PlacedCapitalRequestID = pcRequest.PlacedCapitalRequestID;
                    Collection<SqlParameter> materialParameters = MaterialEntity.GetSqlInsertParamaters(material);

                    SqlParameterCollection updatedMaterialParamteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertPlacedCapitalMaterialRequest", materialParameters, conn, trans);
                }
            }

            trans.Commit();

            SQLUtility.CloseSqlConnection(conn);
            return pcRequest;
        }


        /// <summary>Sets the PlacedCapitalRequestEntity object from a datarow</summary>
        /// <param name="r">The Data Row</param>
        /// <returns>The newly populated PlacedCapitalRequestEntity</returns>
        protected static PlacedCapitalRequestEntity GetPlacedCapitalRequestEntityFromDataRow(DataRow r)
        {
            PlacedCapitalRequestEntity pcRequest = new PlacedCapitalRequestEntity();

            if (r["PlacedCapitalRequestID"].ToString() != string.Empty)
                pcRequest.PlacedCapitalRequestID = Convert.ToInt32(r["PlacedCapitalRequestID"].ToString());

            if (r["ProgramID"].ToString() != string.Empty)
                pcRequest.ProgramID = Convert.ToInt32(r["ProgramID"].ToString());

            pcRequest.SoldToCustomerID = r["SoldToCustomerID"].ToString();
            pcRequest.SoldToCustomerName = r["SoldToCustomerName"].ToString();
            pcRequest.SoldToStreet = r["SoldToStreet"].ToString();
            pcRequest.SoldToCity = r["SoldToCity"].ToString();
            pcRequest.SoldToStateCode = r["SoldToStateCode"].ToString();
            pcRequest.SoldToPostalCode = r["SoldToPostalCode"].ToString();
            pcRequest.RvpName = r["RvpName"].ToString();
            pcRequest.DistrictName = r["DistrictName"].ToString();
            pcRequest.SalesRepName = r["SalesRepName"].ToString();
            pcRequest.SalesRepID = r["SalesRepID"].ToString();
            pcRequest.GpoDesc = r["GpoDesc"].ToString();
            pcRequest.ShipToCustomerID = r["ShipToCustomerID"].ToString();
            pcRequest.ShipToCustomerName = r["ShipToCustomerName"].ToString();
            pcRequest.ShipToStreet = r["ShipToStreet"].ToString();
            pcRequest.ShipToCity = r["ShipToCity"].ToString();
            pcRequest.ShipToStateCode = r["ShipToStateCode"].ToString();
            pcRequest.ShipToPostalCode = r["ShipToPostalCode"].ToString();
            pcRequest.EmailFax = r["EmailFax"].ToString();
            pcRequest.FacilityContact = r["FacilityContact"].ToString();
            pcRequest.ContractTerm = r["ContractTerm"].ToString();
            pcRequest.FormStamp = r["FormStamp"].ToString();

            pcRequest.CreatedBy = r["CreatedBy"].ToString();
            pcRequest.CreateDate = r["CreateDate"].ToString();
            pcRequest.ModifiedBy = r["ModifiedBy"].ToString();
            pcRequest.ModifiedDate = r["ModifiedDate"].ToString();
            pcRequest.SubmittedByName = r["SubmittedByName"].ToString();
            pcRequest.CustomerPO = r["CustomerPO"].ToString();

            return pcRequest;
        }
    
        /// <summary>Gets The Placed Capital Request Entity by the PlacedCapitalRequestID</summary>
        /// <param name="intPlacedCapitalRequestID">The PlaceCapitalRequest ID</param>
        /// <returns>The PlacedCapitalRequest Entity<PlacedCapitalRequestEntity></returns>
        public static PlacedCapitalRequestEntity GetPlacedCapitalRequestByID(int intPlacedCapitalRequestID)
        {
            PlacedCapitalRequestEntity pcRequest = null;
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@PlacedCapitalRequestID", intPlacedCapitalRequestID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPlacedCapitalRequestByID", myParamters);
            if (dt.Rows.Count > 0)
            {
                pcRequest = PlacedCapitalRequestEntity.GetPlacedCapitalRequestEntityFromDataRow(dt.Rows[0]);
            }

            return pcRequest;
        }

        /// <summary>Gets The Placed Capital Request Entity by the PlacedCapitalRequestID</summary>
        /// <param name="strFormStamp">The Form Stamp</param>
        /// <returns>The PlacedCapitalRequest Entity<PlacedCapitalRequestEntity></returns>
        public static PlacedCapitalRequestEntity GetPlacedCapitalRequestByFormStamp(string strFormStamp)
        {
            PlacedCapitalRequestEntity pcRequest = null;
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@FormStamp", strFormStamp));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetPlacedCapitalRequestByFormStamp", myParamters);
            if (dt.Rows.Count > 0)
            {
                pcRequest = PlacedCapitalRequestEntity.GetPlacedCapitalRequestEntityFromDataRow(dt.Rows[0]);
            }

            return pcRequest;
        }


    }//// end class
}