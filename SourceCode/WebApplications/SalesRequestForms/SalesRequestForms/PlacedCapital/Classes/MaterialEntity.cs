﻿// ------------------------------------------------------------------
// <copyright file="CustomerEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRequestForms.PlacedCapital.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using SalesRequestForms.Classes;

    /// <summary>This is the Material Entity.</summary>
    public class MaterialEntity
    {

        /// <summary>This is the CapitalGroup.</summary>
        private string strCapitalGroup;

        /// <summary>This is the MaterialID.</summary>
        private string strMaterialID;

        /// <summary>This is the MaterialDesc.</summary>
        private string strMaterialDesc;

        /// <summary>This is the ListPrice.</summary>
        private double dblListPrice;

        /// <summary>This is the AnnualRequiredPurchases.</summary>
        private double dblAnnualRequiredPurchases;

        /// <summary>This is the ContractValue.</summary>
        private double dblContractValue;

        /// <summary>This is the AnnualValue.</summary>
        private double dblAnnualValue;

        /// <summary>This is the AnnualValue.</summary>
        private int intQty;

        /// <summary>This is the ServiceUpchargeRate.</summary>
        private double dblServiceUpchargeRate;

        /// <summary>This is the MarginUpchargeRate.</summary>
        private double dblMarginUpchargeRate;

        /// <summary>This is the OverheadChargeRate.</summary>
        private double dblOverheadChargeRate;

        /// <summary>This is the CommercialMarginRate.</summary>
        private double dblCommercialMarginRate;

        /// <summary>This is the ServiceChargeAmt.</summary>
        private double dblServiceChargeAmt;

        /// <summary>This is the OverheadChargeAmt.</summary>
        private double dblOverheadChargeAmt;

        /// <summary>This is the Depreciation.</summary>
        private double dblDepreciation;

        /// <summary>This is the PlacedCapitalRequestID.</summary>
        private int intPlacedCapitalRequestID;

        /// <summary>Initializes a new instance of the <see cref="MaterialEntity"/> class.</summary>
        public MaterialEntity()
        {
            this.CapitalGroup = String.Empty;
            this.MaterialID = String.Empty;
            this.MaterialDesc = String.Empty;
            this.AnnualValue = 0.0;
            this.AnnualRequiredPurchases = 0.0;
            this.ContractValue = 0.0;
            this.Qty = 0;
            this.Depreciation = 3.0;
        }

        /// <summary>Gets or sets the CapitalGroup.</summary>
        /// <value>The CapitalGroup.</value>
        public string CapitalGroup
        {
            get{ return this.strCapitalGroup;}

            set{ this.strCapitalGroup = value;}
        }

        /// <summary>Gets or sets the MaterialID.</summary>
        /// <value>The MaterialID.</value>
        public string MaterialID
        {
            get{ return this.strMaterialID;}

            set{ this.strMaterialID = value;}
        }

        /// <summary>Gets or sets the MaterialDesc.</summary>
        /// <value>The MaterialDesc.</value>
        public string MaterialDesc
        {
            get{ return this.strMaterialDesc;}

            set{ this.strMaterialDesc = value;}
        }

        /// <summary>Gets or sets the ListPrice.</summary>
        /// <value>The ListPrice.</value>
        public double ListPrice
        {
            get{ return this.dblListPrice;}

            set{ this.dblListPrice = value;}
        }

        /// <summary>Gets or sets the AnnualRequiredPurchases.</summary>
        /// <value>The AnnualRequiredPurchases.</value>
        public double AnnualRequiredPurchases
        {
            get{ return this.dblAnnualRequiredPurchases;}

            set{ this.dblAnnualRequiredPurchases = value;}
        }

        /// <summary>Gets or sets the ContractValue.</summary>
        /// <value>The ContractValue.</value>
        public double ContractValue
        {
            get { return this.dblContractValue; }

            set { this.dblContractValue = value; }
        }
        
        /// <summary>Gets or sets the AnnualValue.</summary>
        /// <value>The AnnualValue.</value>
        public double AnnualValue
        {
            get{ return this.dblAnnualValue;}

            set{ this.dblAnnualValue = value;}
        }

        /// <summary>Gets or sets the Qty.</summary>
        /// <value>The Qty.</value>
        public int Qty
        {
            get{ return this.intQty;}

            set{ this.intQty = value;}
        }

        /// <summary>Gets or sets the ServiceUpchargeRate.</summary>
        /// <value>The ServiceUpchargeRate.</value>
        public double ServiceUpchargeRate
        {
            get { return this.dblServiceUpchargeRate; }

            set { this.dblServiceUpchargeRate = value; }
        }

        /// <summary>Gets or sets the MarginUpchargeRate.</summary>
        /// <value>The MarginUpchargeRate.</value>
        public double MarginUpchargeRate
        {
            get { return this.dblMarginUpchargeRate; }

            set { this.dblMarginUpchargeRate = value; }
        }

        /// <summary>Gets or sets the OverheadChargeRate.</summary>
        /// <value>The OverheadChargeRate.</value>
        public double OverheadChargeRate
        {
            get { return this.dblOverheadChargeRate; }

            set { this.dblOverheadChargeRate = value; }
        }

        /// <summary>Gets or sets the CommercialMarginRate.</summary>
        /// <value>The CommercialMarginRate.</value>
        public double CommercialMarginRate
        {
            get { return this.dblCommercialMarginRate; }

            set { this.dblCommercialMarginRate = value; }
        }

        /// <summary>Gets or sets the OverheadChargeAmt.</summary>
        /// <value>The OverheadChargeAmt.</value>
        public double OverheadChargeAmt
        {
            get { return this.dblOverheadChargeAmt; }

            set { this.dblOverheadChargeAmt = value; }
        }

        /// <summary>Gets or sets the ServiceChargeAmt.</summary>
        /// <value>The ServiceChargeAmt.</value>
        public double ServiceChargeAmt
        {
            get { return this.dblServiceChargeAmt; }

            set { this.dblServiceChargeAmt = value; }
        }

        /// <summary>Gets or sets the Depreciation.</summary>
        /// <value>The Depreciation.</value>
        public double Depreciation
        {
            get { return this.dblDepreciation; }

            set { this.dblDepreciation = value; }
        }

        /// <summary>Gets or sets the PlacedCapitalRequestID.</summary>
        /// <value>The PlacedCapitalRequestID.</value>
        public int PlacedCapitalRequestID
        {
            get { return this.intPlacedCapitalRequestID; }

            set { this.intPlacedCapitalRequestID = value; }
        }

        /// <summary>Gets the Capital Group Summary Material Collection based on the detailed Material Collection.</summary>
        /// <param name="materials">Detailed Material Collection<MaterialEntity></param>
        /// <returns>The newly populated Summary Material Collection<MaterialEntity></returns>
        public static Collection<MaterialEntity> GetSummaryMaterialEntityCollection(Collection<MaterialEntity> materials)
        {
            Collection<MaterialEntity> summaryMaterials = new Collection<MaterialEntity>();
            string strCapitalGroup = string.Empty;
            MaterialEntity material = null;

            foreach (MaterialEntity detailMaterial in materials)
            {
                if (strCapitalGroup != detailMaterial.CapitalGroup)
                {
                    material = new MaterialEntity();
                    material.CapitalGroup = detailMaterial.CapitalGroup;
                    strCapitalGroup = detailMaterial.CapitalGroup;
                    summaryMaterials.Add(material);
                }
            }

            return summaryMaterials;
        }

        /// <summary>Sets the MaterialEntity object from a datarow</summary>
        /// <param name="r">The Data Row</param>
        /// <returns>The newly populated MaterialEntity</returns>
        protected static MaterialEntity GetMaterialEntityFromDataRow(DataRow r)
        {
            MaterialEntity material = new MaterialEntity();

            material.CapitalGroup = r["CapitalGroup"].ToString();
            material.MaterialID = r["MaterialID"].ToString();
            material.MaterialDesc = r["MaterialDesc"].ToString();
            if (r["ListPrice"].ToString() != string.Empty)
                material.ListPrice = Convert.ToDouble(r["ListPrice"].ToString());
            else
                material.ListPrice = 0.0;

            if (r["AnnualRequiredPurchases"].ToString() != string.Empty)
                material.AnnualRequiredPurchases = Convert.ToDouble(r["AnnualRequiredPurchases"].ToString());

            if (r["AnnualValue"].ToString() != string.Empty)
                material.AnnualValue = Convert.ToDouble(r["AnnualValue"].ToString());

            if (r["Qty"].ToString() != string.Empty)
                material.Qty = Convert.ToInt32(r["Qty"].ToString());

            if (r["ServiceUpchargeRate"].ToString() != string.Empty)
                material.ServiceUpchargeRate = Convert.ToDouble(r["ServiceUpchargeRate"].ToString());
            else
                material.ServiceUpchargeRate = 0.0;

            if (r["MarginUpchargeRate"].ToString() != string.Empty)
                material.MarginUpchargeRate = Convert.ToDouble(r["MarginUpchargeRate"].ToString());
            else
                material.MarginUpchargeRate = 0.0;

            if (r["OverheadChargeRate"].ToString() != string.Empty)
                material.OverheadChargeRate = Convert.ToDouble(r["OverheadChargeRate"].ToString());
            else
                material.OverheadChargeRate = 0.0;

            if (r["CommercialMarginRate"].ToString() != string.Empty)
                material.CommercialMarginRate = Convert.ToDouble(r["CommercialMarginRate"].ToString());
            else
                material.CommercialMarginRate = 0.0;

            if (r["Depreciation"].ToString() != string.Empty)
                material.Depreciation = Convert.ToDouble(r["Depreciation"].ToString());
            else
                material.Depreciation = 3.0;

            material.OverheadChargeAmt = material.ListPrice * material.OverheadChargeRate;

            material.ServiceChargeAmt = material.ListPrice * material.ServiceUpchargeRate;

            return material;
        }

        /// <summary>Sets the Material Collection based on the store procedure.</summary>
        /// <param name="strStoredProcedure">The Stored Procedure to execute</param>
        /// <param name="intPlacedCapitalRequestID">The Placed Capital Request ID</param>
        /// <returns>The newly populated Material Collection<MaterialEntity></returns>
        public static Collection<MaterialEntity> GetPlacedCapitalMaterialRequestByID(string strStoredProcedure, int intPlacedCapitalRequestID)
        {
            Collection<MaterialEntity> materials = new Collection<MaterialEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@PlacedCapitalRequestID", intPlacedCapitalRequestID));


            DataTable dt = SQLUtility.SqlExecuteQuery(strStoredProcedure, parameters);
            Debug.WriteLine("Material Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                MaterialEntity material = MaterialEntity.GetMaterialEntityFromDataRow(r);
                materials.Add(material);
            }

            return materials;
        }

        /// <summary>Sets the Material Collection based on the store procedure.</summary>
        /// <param name="strStoredProcedure">The Stored Procedure to execute</param>
        /// <returns>The newly populated Material Collection<MaterialEntity></returns>
        public static Collection<MaterialEntity> GetMaterialEntityCollection(string strStoredProcedure)
        {
            Collection<MaterialEntity> materials = new Collection<MaterialEntity>();

            DataTable dt = SQLUtility.SqlExecuteQuery(strStoredProcedure);
            Debug.WriteLine("Material Rows: " + dt.Rows.Count.ToString());

            foreach(DataRow r in dt.Rows)
            {
                MaterialEntity material = MaterialEntity.GetMaterialEntityFromDataRow(r);
                materials.Add(material);
            }

            return materials;
        }

        /// <summary>Gets the ContactValue based on the rates.</summary>
        /// <param name="mat">The MaterialEntity instance</param>
        /// <returns>the contract value as a double</returns>
        public static double GetContactValue(MaterialEntity mat)
        {
            double dblContactValue = 0.0;

            mat.OverheadChargeAmt = mat.ListPrice * mat.OverheadChargeRate;

            mat.ServiceChargeAmt = mat.ListPrice * mat.ServiceUpchargeRate;

            dblContactValue = mat.Qty * ((mat.ListPrice + mat.OverheadChargeAmt + mat.ServiceChargeAmt) / mat.MarginUpchargeRate);

            Debug.WriteLine("ListPrice: " + mat.ListPrice.ToString() + "; OverheadChargeRate: " + mat.OverheadChargeRate.ToString() + "; OverheadChargeAmt: " + mat.OverheadChargeAmt.ToString() +
                 "; ServiceUpchargeRate: " + mat.ServiceUpchargeRate.ToString() + "; ServiceChargeAmt: " + mat.ServiceChargeAmt.ToString() +
                  "; MarginUpchargeRate: " + mat.MarginUpchargeRate.ToString() + "; Qty: " + mat.Qty.ToString() + "; dblContactValue: " + dblContactValue.ToString());

            return dblContactValue;
        }

        /// <summary>Gets the AnnualValue based on the Contrat Value.</summary>
        /// <param name="mat">The MaterialEntity instance</param>
        /// <returns>the annual value as a double</returns>
        public static double GetAnnualValue(MaterialEntity mat)
        {
            return mat.ContractValue / mat.Depreciation;
        }

        /// <summary>Gets the AnnualRequiredPurchases based on the AnnualValue and Commercial Rate.</summary>
        /// <param name="mat">The MaterialEntity instance</param>
        /// <returns>the Annual Required Purchases as a double</returns>
        public static double GetAnnualRequiredPurchases(MaterialEntity mat)
        {
            // Update 9/16/2016     TASK0153913
            //return mat.AnnualValue / (1 - mat.CommercialMarginRate);
            return mat.AnnualValue * (1 - mat.CommercialMarginRate);
        }


        /// <summary>Gets the SQL Insert Parameters based for the PlacedCapitalMaterialRequest table.</summary>
        /// <param name="mat">The MaterialEntity instance</param>
        /// <returns>Collection SqlParameter </returns>
        public static Collection<SqlParameter> GetSqlInsertParamaters(MaterialEntity mat)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@AnnualRequiredPurchases", mat.AnnualRequiredPurchases));
            myParamters.Add(new SqlParameter("@AnnualValue", mat.AnnualValue));
            myParamters.Add(new SqlParameter("@CapitalGroup", mat.CapitalGroup));
            myParamters.Add(new SqlParameter("@CommercialMarginRate", mat.CommercialMarginRate));
            myParamters.Add(new SqlParameter("@ContractValue", mat.ContractValue));
            myParamters.Add(new SqlParameter("@MarginUpchargeRate", mat.MarginUpchargeRate));
            myParamters.Add(new SqlParameter("@MaterialDesc", mat.MaterialDesc));
            myParamters.Add(new SqlParameter("@MaterialID", mat.MaterialID));
            myParamters.Add(new SqlParameter("@OverheadChargeRate", mat.OverheadChargeRate));
            myParamters.Add(new SqlParameter("@PlacedCapitalRequestID", mat.PlacedCapitalRequestID));
            myParamters.Add(new SqlParameter("@Qty", mat.Qty));
            myParamters.Add(new SqlParameter("@ListPrice", mat.ListPrice));
            myParamters.Add(new SqlParameter("@ServiceUpchargeRate", mat.ServiceUpchargeRate));
            myParamters.Add(new SqlParameter("@Depreciation", mat.Depreciation));

            SqlParameter paramID = new SqlParameter("@PlacedCapitalMaterialRequestID", 0);
            paramID.Direction = ParameterDirection.Output;
            myParamters.Add(paramID);

            return myParamters;
        }


        /// <summary>Inserts a new record into the PlacedCapitalMaterial table with the information in the MaterialEntity.</summary>
        /// <param name="mat">The <MaterialEntity> instance</param>
        /// <param name="strUser">The application user</param>
        /// <returns>int number of records created</returns>
        public static int InsertPlacedCapitalMaterial(MaterialEntity mat, string strUser)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@CapitalGroup", mat.CapitalGroup));
            myParamters.Add(new SqlParameter("@MaterialDesc", mat.MaterialDesc));
            myParamters.Add(new SqlParameter("@MaterialID", mat.MaterialID));
            myParamters.Add(new SqlParameter("@ListPrice", mat.ListPrice));
            myParamters.Add(new SqlParameter("@modifiedBy", strUser));

            int recordsCreated = SQLUtility.SqlExecuteNonQueryCount("sp_InsertPlacedCapitalMaterial", myParamters);

            return recordsCreated;
        }

        /// <summary>Updates a new record into the PlacedCapitalMaterial table with the information in the MaterialEntity.</summary>
        /// <param name="mat">The <MaterialEntity> instance</param>
        /// <param name="strUser">The application user</param>
        /// <returns>int number of records updated</returns>
        public static int UpdatePlacedCapitalMaterial(MaterialEntity mat, string strUser)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@CapitalGroup", mat.CapitalGroup));
            myParamters.Add(new SqlParameter("@MaterialDesc", mat.MaterialDesc));
            myParamters.Add(new SqlParameter("@MaterialID", mat.MaterialID));
            myParamters.Add(new SqlParameter("@ListPrice", mat.ListPrice));
            myParamters.Add(new SqlParameter("@modifiedBy", strUser));

            int recordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdatePlacedCapitalMaterial", myParamters);

            return recordsUpdated;
        }

        /// <summary>Deletes a PlacedCapitalMaterial record by the materialID.</summary>
        /// <param name="mat">The <MaterialEntity> instance</param>
        /// <returns>int number of records deleted</returns>
        public static int DeletePlacedCapitalMaterial(string strMaterialID)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@MaterialID", strMaterialID));

            int recordsDeleted = SQLUtility.SqlExecuteNonQueryCount("sp_DeletePlacedCapitalMaterialByMaterialID", myParamters);

            return recordsDeleted;
        }

        /// <summary>Deletes a PlacedCapitalMaterial record by the materialID.</summary>
        /// <param name="mat">The <MaterialEntity> instance</param>
        /// <returns>int number of records deleted</returns>
        public static MaterialEntity GetPlacedCapitalMaterialByMaterialID(string strMaterialID)
        {
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@MaterialID", strMaterialID));

            DataTable dtMaterial = SQLUtility.SqlExecuteQuery("sp_GetPlacedCapitalMaterialByMaterialID", myParamters);
            MaterialEntity mat = null;

            if (dtMaterial.Rows.Count > 0)
            {
                DataRow r = dtMaterial.Rows[0];
                mat = new MaterialEntity();

                mat.CapitalGroup = r["CapitalGroup"].ToString();
                mat.MaterialID = r["MaterialID"].ToString();
                mat.MaterialDesc = r["MaterialDesc"].ToString();
                mat.ListPrice = Convert.ToDouble(r["ListPrice"].ToString());
            }

            return mat;
        }

        /// <summary>Get the Collection<MaterialEntity> from the Detail Collection<MaterialEntity></summary>
        /// <param name="collMaterial">The Collection<MaterialEntity> instance</param>
        /// <returns> Collection<MaterialEntity></returns>
        public static Collection<MaterialEntity> GetPlacedCapitalMaterialSummaryFromDetailCollection(Collection<MaterialEntity> collMaterial)
        {
            Collection<MaterialEntity> summaryCollMaterial = MaterialEntity.GetSummaryMaterialEntityCollection(collMaterial);

            MaterialEntity totalMaterialEntity = new MaterialEntity();
            totalMaterialEntity.CapitalGroup = "TOTAL";

            // loop thru the detail to get the totals by capital group and overal total
            foreach (MaterialEntity m in collMaterial)
            {
                m.ContractValue = MaterialEntity.GetContactValue(m);
                m.AnnualValue = MaterialEntity.GetAnnualValue(m);
                m.AnnualRequiredPurchases = MaterialEntity.GetAnnualRequiredPurchases(m);

                totalMaterialEntity.ContractValue += m.ContractValue;
                totalMaterialEntity.AnnualValue += m.AnnualValue;
                totalMaterialEntity.AnnualRequiredPurchases += m.AnnualRequiredPurchases;
                totalMaterialEntity.Qty += m.Qty;

                foreach (MaterialEntity summMaterial in summaryCollMaterial)
                {
                    if (m.CapitalGroup == summMaterial.CapitalGroup)
                    {
                        summMaterial.ContractValue += m.ContractValue;
                        summMaterial.AnnualValue += m.AnnualValue;
                        summMaterial.AnnualRequiredPurchases += m.AnnualRequiredPurchases;
                        summMaterial.Qty += m.Qty;
                    }
                }  /// end summary loop

            } /// end detail Material loop
          
            summaryCollMaterial.Add(totalMaterialEntity);
              
            return summaryCollMaterial;

        }

    } //// end class
}