﻿<%@ Page Title="Place Capital Request" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlacedCapitalRequest.aspx.cs" Inherits="SalesRequestForms.PlacedCapital.PlacedCapitalRequest" %>
<%@ Register src="../UserControls/CustomerMasterReadOnly.ascx" tagname="CustomerMasterReadOnly" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="text-align:center;font-size: medium;"><strong>PLACED CAPITAL REQUEST</strong></td>
        </tr>
    </table>
    <table width="100%" style="background-color: #FFFF00;">
        <tr>
            <td style="font-weight:bold; font-size:10pt"><asp:Label ID="lblCompletionTime" runat="server" Text="ONCE REQUEST IS SUBMITTED, PLEASE ALLOW UP TO 3-5 DAYS FOR COMPLETION."></asp:Label></td>
        </tr>
        <tr>
            <td style="font-weight:bold; font-size:10pt"><asp:Label ID="lblRefurbishNote" runat="server" Text="*Note - refurbished product will be used whenever possible"></asp:Label></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td colspan="2"><asp:ValidationSummary ID="ValidationSummary3" runat="server" ForeColor="#FF7300" Font-Size="10pt"/>
                                    <asp:HiddenField ID="hdnProgramID" runat="server" Value="1" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:32%">Placed Capital Request ID:</td>
                        <td><asp:TextBox ID="txtPlacedCapitalRequestID" runat="server" Width="40px" 
                                ontextchanged="txtPlacedCapitalRequestID_TextChanged" AutoPostBack="true" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vldRequiredPlacedCapitalRequestID" ControlToValidate="txtPlacedCapitalRequestID" runat="server" ErrorMessage="Placed Capital Request ID is required.">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="vldPlacedCapitalRequestID" ControlToValidate="txtPlacedCapitalRequestID" runat="server" ErrorMessage="Placed Capital Request ID must be an integer greater than zero." Operator="GreaterThan" ValueToCompare="0" Type="Integer">*</asp:CompareValidator>
                            <asp:Button ID="btnSubmit" runat="server" CausesValidation="False" 
                                CssClass="buttonsSubmit" Text="View Request" onclick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:50%; vertical-align:top">
                <uc2:CustomerMasterReadOnly ID="CustomerMasterReadOnly1" runat="server" />
                <table width="100%">
                    <tr>
                        <td colspan="2" style="text-align:center; font-size:12pt; color:#FF7300"><asp:Label ID="lblPlacedCapitalRequestMsg" runat="server"></asp:Label></td>
                    </tr>        
                    <tr>
                        <td style="width:32%">Contact Name at Facility:</td>
                        <td><asp:Label ID="lblContactName" runat="server"></asp:Label></td>
                    </tr>        <tr>
                        <td>Email:</td>
                        <td><asp:Label ID="lblEmailFax" runat="server"></asp:Label></td>
                    </tr>
                        <tr>
                            <td>Customer PO#: </td>
                            <td><asp:Label ID="lblCustomerPO" runat="server"></asp:Label></td>
                        </tr>
                </table>           
            </td>
            <td style="width:50%; vertical-align:top">
                <table width="100%">
                    <tr>
                        <td style="color: red; text-decoration: underline; font-weight:bold; text-align: center;">COMPLIANCE INFORMATION REQUIRED</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Does Customer Purchase Related Disposables from a different facility other than where equipment will be 
                        located?  IF SO, please list additional Customer numbers below so this revenue can be considered 
                        against the annual disposable commitments.
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:160; font-weight:bold">Additional Sold To Numbers:</td>
                    </tr>
                    <tr>
                         <td>
                             <asp:GridView ID="gvSoldTo" runat="server" Width="100%" AutoGenerateColumns="False" ShowFooter="false" DataKeyNames="CustomerID" 
                                 CellPadding="2" GridLines="Both">
                                <RowStyle HorizontalAlign="left" />
                                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer ID" DataField="CustomerID" />
                                    <asp:BoundField ItemStyle-Width="75%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer Name" DataField="CustomerName" />
                                </Columns>
                            </asp:GridView>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>Customer Ship</b> to if Different than Customer Sold to Ship to;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="width:28%">Customer ID:</td>
                                    <td><asp:Label ID="lblShipToCustomerID" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Customer Name:</td>
                                    <td><asp:Label ID="lblShipToCustomerName" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Customer Address:</td>
                                    <td><asp:Label ID="lblShipToCustomerAddress" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Customer City, State, Zip:</td>
                                    <td><asp:Label ID="lblShipToCustomerCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblShipToCustomerState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblShipToCustomerZip" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
         </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
                <asp:GridView ID="gvMaterial" runat="server" Width="100%" AutoGenerateColumns="False" 
                    ShowFooter="false" DataKeyNames="MaterialID" CellPadding="2" GridLines="Both">
                <HeaderStyle BackColor="#dcdcdc"/>
                <RowStyle HorizontalAlign="Right" />
                <Columns>
                   <asp:BoundField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Capital Group" DataField="CapitalGroup" />
                   <asp:BoundField ItemStyle-Width="8%"  ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Qty" DataField="Qty" />
                   <asp:BoundField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material ID" DataField="MaterialID" />
                   <asp:BoundField ItemStyle-Width="38%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Material Desc" DataField="MaterialDesc" />
                   <asp:BoundField ItemStyle-Width="14%" DataFormatString="{0:c0}" ItemStyle-CssClass="TARight" HeaderStyle-HorizontalAlign="Right" HeaderText="Annual Value of Equipment" DataField="AnnualValue" />
                   <asp:BoundField ItemStyle-Width="14%"  ItemStyle-CssClass="TARight" DataFormatString="{0:c0}"  HeaderStyle-HorizontalAlign="Right" HeaderText="Annual Target Purchase" DataField="AnnualRequiredPurchases" />
                </Columns>
            </asp:GridView>  
            </td>
        </tr>
        <tr>
            <td><asp:ValidationSummary ID="vldMaterialSummary" runat="server" ValidationGroup="UpdateMaterial" ForeColor="#FF7300" Font-Size="10pt" /></td>
        </tr>
     </table>
    <table width="100%">
        <tr>
            <td style="width:60%;">
                <table width="100%">
                    <tr>
                         <td style="width:100%; background-color: #FFFF00;color: #CC3300; font-weight:bold; text-align:center">
                            CUSTOMER MUST MEET THEIR ANNUAL TARGET <span style="color: black">PURCHASE</span> LISTED IN THE TABLE TO THE RIGHT PER CAPITAL GROUP, IF CUSTOMER CANNOT COMMIT TO THE ANNUAL TARGET PURCHASE PER CAPITAL GROUP, THIS REQUEST WILL BE REJECTED.
                        </td>                   
                    </tr>
                  </table>  
            </td>
            <td>
                <asp:GridView ID="gvSummaryMaterial" runat="server" Width="270px" AutoGenerateColumns="False" HorizontalAlign="Right"
                    ShowFooter="false" DataKeyNames="MaterialID" CellPadding="2" GridLines="Both" 
                        style="text-align: right">
                <RowStyle HorizontalAlign="left" />
                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                <Columns>
                   <asp:BoundField ItemStyle-Width="50%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Capital Group" DataField="CapitalGroup" />
                   <asp:BoundField ItemStyle-Width="50%" DataFormatString="{0:c0}"  ItemStyle-CssClass="TARight" HeaderStyle-CssClass="TARight" HeaderText="Target Purchase" DataField="AnnualRequiredPurchases" />
                </Columns>
            </asp:GridView> 
            </td>
        </tr>
    </table>
</asp:Content>