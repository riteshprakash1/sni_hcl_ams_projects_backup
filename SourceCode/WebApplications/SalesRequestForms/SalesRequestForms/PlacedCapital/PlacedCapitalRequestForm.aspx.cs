﻿// -------------------------------------------------------------
// <copyright file="PlacedCapitalRequestForm.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRequestForms.PlacedCapital
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using SalesRequestForms.Classes;
    using SalesRequestForms.PlacedCapital.Classes;

    /// <summary>This is the Placed Capital Request Form page.</summary>   
    public partial class PlacedCapitalRequestForm : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Control postBackCtrl = this.GetPostBackControl(Page);

            if (!Page.IsPostBack)
            {
                CustomerMaster1.CustomerGroup1Visible(false);
                
                ADUserEntity myUser = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
                this.hdnSubmittedBy.Value = myUser.DisplayName;
                this.hdnFormStamp.Value = myUser.UserName + DateTime.Now.ToUniversalTime().ToString().Replace(" ", string.Empty);

                // Contract Term removed via change request TASK0125725
                //Collection<SqlParameter> parameters = new Collection<SqlParameter>();
                //parameters.Add(new SqlParameter("@ProgramID", Convert.ToInt32(this.hdnProgramID.Value)));

                //DataTable dtRates = SQLUtility.SqlExecuteQuery("sp_GetProgramRatesByProgramID", parameters);
                //DataRow[] rows = dtRates.Select("RateDescription = 'Contract Term'");
                //if (rows.Length > 0)
                //    this.lblContractTerm.Text = rows[0]["RateText"].ToString();

                Collection<CustomerEntity> collCustomers = new Collection<CustomerEntity>();
                collCustomers.Add(new CustomerEntity());
                this.BindSoldTo(collCustomers);

                Collection<MaterialEntity> collMaterial = MaterialEntity.GetMaterialEntityCollection("sp_GetAllPlacedCapitalMaterial");
                this.BindMaterial(collMaterial);

                string strInstructions = "1. Enter the Sold To Customer ID and click \"Find\" to populate the customer information and Sales Rep drop down list<br> ";
                strInstructions += "2. Select the Sales Rep<br>";
                strInstructions += "3. Fill out remaining customer information and equipment quantities<br>";
                strInstructions += "4. After you have completed the form, click the \"Submit Request\" button";

                this.lnkInstructionsLabel.Attributes.Add("onmouseover", "showhint('" + strInstructions + " ', this, event, '700px', 0, 90);");

            }
            else
            {
                Debug.WriteLine("Page.IsPostBack");
            }

        }


        /// <summary>Handles the RowDataBound event of the gvMaterial GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                if (txtQty.Text == "0")
                {
                    txtQty.Text = string.Empty;
                }

                EnterButton.TieButtonDoTab(txtQty);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="page">The web page.</param>
        /// <returns>The control that caused the postback</returns>
        public Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
                Debug.WriteLine("postBackCtrl ClientID: " + control.ID);
            }

            foreach (string ctl in page.Request.Form)
            {
                Control c = page.FindControl(ctl);
                if (c is System.Web.UI.WebControls.Button)
                {
                    control = c;
                    Debug.WriteLine("postBackCtrl ClientID: " + control.ID);
                    break;
                }
            }

            return control;
        }

        /// <summary>Reloads the Material Grids and makes the necessary calculations</summary>
        protected void LoadMaterialGrids()
        {
            Collection<MaterialEntity> collMaterial = MaterialEntity.GetMaterialEntityCollection("sp_GetAllPlacedCapitalMaterial");
            Collection<MaterialEntity> summaryCollMaterial = MaterialEntity.GetSummaryMaterialEntityCollection(collMaterial);

            MaterialEntity totalMaterialEntity = new MaterialEntity();
            totalMaterialEntity.CapitalGroup = "TOTAL";

            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    TextBox txtQty = (TextBox)r.FindControl("txtQty");
                    Label lblMaterialID = (Label)r.FindControl("lblMaterialID");

                    foreach (MaterialEntity m in collMaterial)
                    {
                        if (m.MaterialID == lblMaterialID.Text)
                        {
                            if (txtQty.Text != string.Empty)
                            {
                                m.Qty = Convert.ToInt32(txtQty.Text);

                                m.ContractValue = MaterialEntity.GetContactValue(m);
                                m.AnnualValue = MaterialEntity.GetAnnualValue(m);
                                m.AnnualRequiredPurchases = MaterialEntity.GetAnnualRequiredPurchases(m);

                                //mat.AnnualValue / (1 - mat.CommercialMarginRate)  mat.ContractValue / mat.Depreciation
                                Debug.WriteLine("MaterialID: " + m.MaterialID + " - ContractValue: " + m.ContractValue.ToString() + " - Depreciation: " + m.Depreciation.ToString() + " - AnnualValue: " + m.AnnualValue.ToString() + " - CommercialMarginRate: " + m.CommercialMarginRate.ToString() + " - AnnualRequiredPurchases: " + m.AnnualRequiredPurchases.ToString()); 

                                totalMaterialEntity.ContractValue += m.ContractValue;
                                totalMaterialEntity.AnnualValue += m.AnnualValue;
                                totalMaterialEntity.AnnualRequiredPurchases += m.AnnualRequiredPurchases;
                                totalMaterialEntity.Qty += m.Qty;

                                foreach (MaterialEntity summMaterial in summaryCollMaterial)
                                {
                                    if (m.CapitalGroup == summMaterial.CapitalGroup)
                                    {
                                        summMaterial.ContractValue += m.ContractValue;
                                        summMaterial.AnnualValue += m.AnnualValue;
                                        summMaterial.AnnualRequiredPurchases += m.AnnualRequiredPurchases;
                                        summMaterial.Qty += m.Qty;
                                    }
                                }  /// end summary loop
                            }
                        } /// end if materialID match
                    } /// end detail Material loop
                } /// end r.RowType == DataControlRowType.DataRow
            } /// end GridViewRow loop

            this.BindMaterial(collMaterial);

            summaryCollMaterial.Add(totalMaterialEntity);
            this.BindSummaryMaterial(summaryCollMaterial);
        }



        /// <summary>Handles the Text Changed event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void txtMaterialQty_TextChanged(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((TextBox)sender).NamingContainer;
            int intFocusRowIndex = row.RowIndex;
            Debug.WriteLine("GridViewRow Index: " + intFocusRowIndex.ToString());

            Page.Validate("UpdateMaterial");
            if (Page.IsValid)
            {

                this.LoadMaterialGrids();

                if (this.gvMaterial.Rows.Count > (intFocusRowIndex + 1))
                    intFocusRowIndex += 1;

                GridViewRow focusRow = this.gvMaterial.Rows[intFocusRowIndex];
                TextBox txtFocusQty = (TextBox)focusRow.FindControl("txtQty");
                txtFocusQty.Focus();

            }

        }

        /// <summary>Handles the Edit event of the gvMaterial GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_MaterialEdit(object sender, GridViewEditEventArgs e)
        {
            Debug.WriteLine("NewEditIndex: " + e.NewEditIndex.ToString());
            //this.gvBusiness.EditIndex = e.NewEditIndex;
            //this.BindDT();
        }

        /// <summary>Handles the RowDataBound event of the gvReps GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_SoldToRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCustomerID = (Label)e.Row.FindControl("lblCustomerID");
                Label lblCustomerName = (Label)e.Row.FindControl("lblCustomerName");

                //// if the Sold To are blank hide the row
                e.Row.Visible = Convert.ToBoolean((lblCustomerID.Text != String.Empty) || (lblCustomerName.Text != String.Empty));
            }
        } // end GV_RepsRowDataBound

        /// <summary>Binds the summary material entity collection to the gvSummaryMaterial GridView.</summary>
        /// <param name="materials">material entity collection</param>
        protected void BindSummaryMaterial(Collection<MaterialEntity> materials)
        {
            this.gvSummaryMaterial.DataSource = materials;
            this.gvSummaryMaterial.DataBind();
        }

        /// <summary>Binds the material entity collection to the gvMaterial GridView.</summary>
        /// <param name="materials">material entity collection</param>
        protected void BindMaterial(Collection<MaterialEntity> materials)
        {

            this.gvMaterial.DataSource = materials;
            this.gvMaterial.DataBind();
        }

        /// <summary>Handles the Delete event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_SoldToDelete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            Collection<CustomerEntity> myCustomers = this.LoadAdditionalSoldToCustomersFromPage();
            myCustomers.RemoveAt(e.RowIndex);
            this.BindSoldTo(myCustomers);
        }

        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_SoldToAdd(object sender, GridViewCancelEditEventArgs e)
        {
            TextBox txtCustomerID = (TextBox)this.gvSoldTo.FooterRow.FindControl("txtCustomerID");
            Collection<CustomerEntity> newCustomers = CustomerEntity.GetSoldToByCustomerID(txtCustomerID.Text);

            if (newCustomers.Count > 0)
            {
                Collection<CustomerEntity> myCustomers = this.LoadAdditionalSoldToCustomersFromPage();
                myCustomers.Add(newCustomers[0]);

                this.BindSoldTo(myCustomers);
            }
            else
            {
                Page.Validate("AdditionalSoldTo");
                CustomValidator vldCustomerIDExists = (CustomValidator)this.gvSoldTo.FooterRow.FindControl("vldCustomerIDExists");
                vldCustomerIDExists.IsValid = false;
            }
            
        } ////end GV_Add

        /// <summary>Adds the Sold To Customer to the GridView.</summary>
        protected void SoldToAdd()
        {
            TextBox txtCustomerID = (TextBox)this.gvSoldTo.FooterRow.FindControl("txtCustomerID");
            Collection<CustomerEntity> newCustomers = CustomerEntity.GetSoldToByCustomerID(txtCustomerID.Text);

            if (newCustomers.Count > 0)
            {
                Collection<CustomerEntity> myCustomers = this.LoadAdditionalSoldToCustomersFromPage();
                myCustomers.Add(newCustomers[0]);

                this.BindSoldTo(myCustomers);
            }
            else
            {
                Page.Validate("AdditionalSoldTo");
                CustomValidator vldCustomerIDExists = (CustomValidator)this.gvSoldTo.FooterRow.FindControl("vldCustomerIDExists");
                vldCustomerIDExists.IsValid = false;
            }


        } ////end SoldToAdd

        /// <summary>Binds the users table to the gvReps GridView.</summary>
        /// <param name="myReps">AccountChangeReps collection</param>
        protected void BindSoldTo(Collection<CustomerEntity> myCustomers)
        {
            if (myCustomers.Count == 0)
            {
                CustomerEntity customer = new CustomerEntity();
                myCustomers.Add(customer);
            }

            this.gvSoldTo.DataSource = myCustomers;
            this.gvSoldTo.DataBind();

            TextBox txtCustomerID = (TextBox)this.gvSoldTo.FooterRow.FindControl("txtCustomerID");
            Button btnAdd = (Button)this.gvSoldTo.FooterRow.FindControl("btnAdd");
            EnterButton.TieButton(txtCustomerID, btnAdd);
        }

        /// <summary>Handles the Text Changed event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void txtShipToCustomerID_TextChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("txtShipToCustomerID_TextChanged");
            Page.Validate("FindShipToCustomer");

            if (Page.IsValid)
            {
                Collection<CustomerEntity> myCustomers = CustomerEntity.GetShipToByCustomerID(this.txtShipToCustomerID.Text);

                if (myCustomers.Count > 0)
                {
                    //Use the first instance which will be the WE Partner if there are more than one 
                    CustomerEntity cust = myCustomers[0];
                    this.lblShipToCustomerAddress.Text = cust.Street;
                    this.lblShipToCustomerName.Text = cust.CustomerName;
                    this.lblShipToCustomerCity.Text = cust.City;
                    this.lblShipToCustomerState.Text = cust.StateCode;
                    this.lblShipToCustomerZip.Text = cust.PostalCode;
                }
                else
                {
                    this.vldShipToCustomerIDExists.IsValid = false;
                }
            }
        }

        /// <summary>Handles the Text Changed event of the Page txtCustomerID control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void txtAdditionalSoldToCustomerID_TextChanged(object sender, EventArgs e)
        {
            this.SoldToAdd();
        }

        /// <summary>Handles the Submit button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            TextBox txtCustomerID = (TextBox)this.gvSoldTo.FooterRow.FindControl("txtCustomerID");
            Debug.WriteLine("Customer Empty: " + Convert.ToBoolean(txtCustomerID.Text != string.Empty).ToString());
            if (txtCustomerID.Text != string.Empty)
            {
                Collection<CustomerEntity> newCustomers = CustomerEntity.GetSoldToByCustomerID(txtCustomerID.Text);

                if (newCustomers.Count > 0)
                {
                    Collection<CustomerEntity> gvCustomers = this.LoadAdditionalSoldToCustomersFromPage();
                    gvCustomers.Add(newCustomers[0]);

                    this.BindSoldTo(gvCustomers);
                }
                else
                {
                    CustomValidator vldCustomerIDExistsSubmitted = (CustomValidator)this.gvSoldTo.FooterRow.FindControl("vldCustomerIDExistsSubmitted");
                    vldCustomerIDExistsSubmitted.IsValid = false;
                }
            }


            if (this.txtShipToCustomerID.Text != string.Empty)
            {
                Debug.WriteLine("this.txtShipToCustomerID.Text != string.Empty");
//                Page.Validate("FindShipToCustomer");
                Collection<CustomerEntity> myCustomers = CustomerEntity.GetShipToByCustomerID(this.txtShipToCustomerID.Text);
                this.vldShipToCustomerIDExistsSubmit.IsValid = Convert.ToBoolean(myCustomers.Count > 0);
            }

            this.Page.Validate("UpdateMaterial");

            Collection<MaterialEntity> submittedMaterials = null;
            if (Page.IsValid)
            {
                //materials items that have a qty greater than zero
                submittedMaterials = this.GetSubmittedMaterials();
                vldMaterialListSubmission.IsValid = Convert.ToBoolean(submittedMaterials.Count > 0);
            }


            ADUserEntity user = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

            Debug.WriteLine("Page.IsValid: " + Page.IsValid.ToString() + "; CustomerMaster1.Page.IsValid: " + CustomerMaster1.Page.IsValid.ToString());

            PlacedCapitalRequestEntity pcFormStamp = PlacedCapitalRequestEntity.GetPlacedCapitalRequestByFormStamp(this.hdnFormStamp.Value);
            if (pcFormStamp != null)
            {
                vldSubmission.IsValid = false;
                vldSubmission.ErrorMessage = "This Placed Capital Request was already submitted.  The Placed Capital Request ID is " + pcFormStamp.PlacedCapitalRequestID.ToString();
            }

            if (Page.IsValid)
            {
                PlacedCapitalRequestEntity pcRequest = this.LoadPlacedCapitalRequestEntityFromPage();
                PlacedCapitalRequestEntity insertedPCRequest = PlacedCapitalRequestEntity.InsertPlacedCapitalRequest(pcRequest, submittedMaterials);
                Collection<CustomerEntity> additionalSoldTos = this.LoadAdditionalSoldToCustomersFromPage();

                Debug.WriteLine("additionalSoldTos Count: " + additionalSoldTos.Count.ToString());
                CustomerEntity.InsertAdditionalSoldToCustomers(additionalSoldTos, pcRequest.PlacedCapitalRequestID);


                PlacedCapitalRequestEntity pc = PlacedCapitalRequestEntity.GetPlacedCapitalRequestByID(insertedPCRequest.PlacedCapitalRequestID);
                string strSubject = System.Configuration.ConfigurationManager.AppSettings["PlacedCapitalRequestEmailSubject"];
                string strFromEmail = System.Configuration.ConfigurationManager.AppSettings["PlacedCapitalFromEmail"];
                string strToEmail = System.Configuration.ConfigurationManager.AppSettings["PlacedCapitalToEmail"];

                bool blnIsProduction = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isProduction"]);
                if (!blnIsProduction)
                    strToEmail = user.Email;

                Utility.SendEmailAsync(strToEmail, strFromEmail, user.Email, strSubject, this.GetEmailBody(pc, submittedMaterials, additionalSoldTos), true);

                Response.Redirect("PlacedCapitalRequest.aspx?requestID=" + pcRequest.PlacedCapitalRequestID.ToString(), true);
            }
        }

        /// <summary>Load the CustomerEntity Collection with the Additional Sold To Customer from the SoldTo Gridview.</summary>
        /// <returns>The Collection CustomerEntity</returns>
        protected Collection<CustomerEntity> LoadAdditionalSoldToCustomersFromPage()
        {
            Collection<CustomerEntity> soldToCustomers = new Collection<CustomerEntity>();

            foreach (GridViewRow r in this.gvSoldTo.Rows)
            {
                if ((r.RowType == DataControlRowType.DataRow ) && r.Visible)
                {
                    Label lblCustomerID = (Label)r.FindControl("lblCustomerID");
                    Label lblCustomerName = (Label)r.FindControl("lblCustomerName");

                    CustomerEntity cust = new CustomerEntity();
                    cust.CustomerID = lblCustomerID.Text;
                    cust.CustomerName = lblCustomerName.Text;

                    Debug.WriteLine("r.RowType: " + r.RowType.ToString() + "; ID: " + cust.CustomerID + "; Name: " + cust.CustomerName);
                    soldToCustomers.Add(cust);
                }
            }

            return soldToCustomers;
        }


        /// <summary>Load the PlacedCapitalRequestEntity from the data.</summary>
        /// <returns>The PlacedCapitalRequestEntity</returns>
        protected PlacedCapitalRequestEntity LoadPlacedCapitalRequestEntityFromPage()
        {
            PlacedCapitalRequestEntity p = new PlacedCapitalRequestEntity();

            p.ProgramID = Convert.ToInt32(this.hdnProgramID.Value);
            p.SoldToCustomerID = CustomerMaster1.CustomerID.PadLeft(10, '0');
            p.SoldToCustomerName = CustomerMaster1.CustomerName;
            p.SoldToStreet = CustomerMaster1.CustomerAddress;
            p.SoldToCity = CustomerMaster1.CustomerCity;
            p.SoldToStateCode = CustomerMaster1.CustomerState;
            p.SoldToPostalCode = CustomerMaster1.CustomerZip;
            if(this.txtShipToCustomerID.Text != string.Empty)
                p.ShipToCustomerID = this.txtShipToCustomerID.Text.PadLeft(10, '0');

            p.ShipToCustomerName = this.lblShipToCustomerName.Text;
            p.ShipToStreet = this.lblShipToCustomerAddress.Text;
            p.ShipToCity = this.lblShipToCustomerCity.Text;
            p.ShipToStateCode = this.lblShipToCustomerState.Text;
            p.ShipToPostalCode = this.lblShipToCustomerZip.Text;
            p.EmailFax = this.txtEmailFax.Text;
            p.FacilityContact = this.txtContactName.Text;
            //p.ContractTerm = this.lblContractTerm.Text;
            p.FormStamp = this.hdnFormStamp.Value;
            p.ModifiedBy = ADUtility.GetUserNameOfAppUser(Request);
            p.SubmittedByName = this.hdnSubmittedBy.Value;
            p.CustomerPO = this.txtCustomerPO.Text;

            p.SalesRepID = CustomerMaster1.RepID;
            p.SalesRepName = CustomerMaster1.RepName;
            p.RvpName = CustomerMaster1.RvpName;
            p.DistrictName = CustomerMaster1.DeName;
            p.GpoDesc = CustomerMaster1.GPO;

            return p;
        }

        /// <summary>Get the MaterialEntity collection for materials items that have a qty greater than zero.</summary>
        /// <returns>The Collection MaterialEntity</returns>
        protected Collection<MaterialEntity> GetSubmittedMaterials()
        {
            Collection<MaterialEntity> collMaterial = MaterialEntity.GetMaterialEntityCollection("sp_GetAllPlacedCapitalMaterial");
            Collection<MaterialEntity> submittedMaterials = new Collection<MaterialEntity>();
            foreach (GridViewRow r in this.gvMaterial.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    TextBox txtQty = (TextBox)r.FindControl("txtQty");
                    Label lblMaterialID = (Label)r.FindControl("lblMaterialID");

                    foreach (MaterialEntity m in collMaterial)
                    {
                        if (m.MaterialID == lblMaterialID.Text)
                        {
                            if ((txtQty.Text != string.Empty) && (txtQty.Text != "0"))
                            {
                                m.Qty = Convert.ToInt32(txtQty.Text);

                                m.ContractValue = MaterialEntity.GetContactValue(m);
                                m.AnnualValue = MaterialEntity.GetAnnualValue(m);
                                m.AnnualRequiredPurchases = MaterialEntity.GetAnnualRequiredPurchases(m);
                                submittedMaterials.Add(m);
                            }
                        } /// end if materialID match
                    } /// end detail Material loop
                } /// end r.RowType == DataControlRowType.DataRow
            } /// end GridViewRow loop

            return submittedMaterials;
        }

        /// <summary>This writes a string that with the data for the body of the email.</summary>
        /// <param name="pc">The PlacedCapitalRequestEntity object.</param>
        /// <param name="materials">The MaterialEntity Collection</param>
        /// <param name="soldTos">The CustomerEntity Collection</param>
        /// <returns>The email body string</returns>
        protected string GetEmailBody(PlacedCapitalRequestEntity pc, Collection<MaterialEntity> materials, Collection<CustomerEntity> soldTos)
        {
            string tablewidth = "960px";

            string strEmailBody = string.Empty;
            strEmailBody += "<style>";
            strEmailBody += "td{ text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:8pt}";
            strEmailBody += "</style>";
            strEmailBody += "<div style=\"width:" + tablewidth + "\">";

            strEmailBody += "<table style=\"width:" + tablewidth + "\">";
             strEmailBody += "<tr>";
             strEmailBody += "<td style=\"text-align:center;font-size: medium;\"><strong>PLACED CAPITAL REQUEST</strong></td>";
             strEmailBody += "</tr>";
             strEmailBody += "</table>";
             strEmailBody += "<table width=\"" + tablewidth + "\" style=\"background-color: #FFFF00;\">";
             strEmailBody += "<tr>";
             strEmailBody += "<td>" + this.lblCompletionTime.Text + "</td>";
             strEmailBody += "</tr>";
             strEmailBody += "<tr>";
             strEmailBody += "<td>" + this.lblRefurbishNote.Text + "</td>";
             strEmailBody += "</tr>";
             strEmailBody += "</table>";
            
            strEmailBody += "<table style=\"width:" + tablewidth + "\">";
            strEmailBody += "<tr><td style=\"width:50%; vertical-align:top\">";
            strEmailBody += "<table width=\"100%\">";

            strEmailBody += "<tr>";
            strEmailBody += "<td>Placed Capital Request ID:</td>";
            strEmailBody += "<td>" + pc.PlacedCapitalRequestID.ToString() + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Submitted By:</td>";
            strEmailBody += "<td>" + pc.SubmittedByName + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Submitted Date:</td>";
            strEmailBody += "<td>" + pc.CreateDate + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td style=\"width:35%\">Customer Sold To Number:</td>";
            strEmailBody += "<td>" + pc.SoldToCustomerID + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Rep Name:</td>";
            strEmailBody += "<td>" + pc.SalesRepName + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>RVP:</td>";
            strEmailBody += "<td>" + pc.RvpName + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>DE:</td>";
            strEmailBody += "<td>" + pc.DistrictName + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>GPO:</td>";
            strEmailBody += "<td>" + pc.GpoDesc + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Customer Name:</td>";
            strEmailBody += "<td>" + pc.SoldToCustomerName + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Customer Address:</td>";
            strEmailBody += "<td>" + pc.SoldToStreet + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Customer City, State, Zip:</td>";
            strEmailBody += "<td>" + pc.SoldToCity + ",&nbsp;" + pc.SoldToStateCode + "&nbsp;&nbsp;" + pc.SoldToPostalCode + "</td>";
            strEmailBody += "</tr>";

            strEmailBody += "<tr>";
            strEmailBody += "<td>Contact Name at Facility: </td>";
            strEmailBody += "<td>" + pc.FacilityContact + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Email: </td>";
            strEmailBody += "<td>" + pc.EmailFax + "</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>Customer PO#: </td>";
            strEmailBody += "<td>" + pc.CustomerPO + "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "</td>";
            strEmailBody += "<td style=\"width:50%; vertical-align:top\">";
            strEmailBody += "<table width=\"100%\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"color: red; text-decoration: underline; font-weight:bold; text-align: center;\">COMPLIANCE INFORMATION REQUIRED</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td style=\"text-align: left;\">Does Customer Purchase Related Disposables from a different facility other than where equipment will be ";
            strEmailBody += "located?  IF SO, please list additional Customer numbers below so this revenue can be considered against the annual disposable commitments.</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td valign=\"top\" style=\"width:160\">Additional Sold To Numbers:</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td>";
            strEmailBody += "<div>";
            // Start Additional Sold To
            strEmailBody += "<table cellspacing=\"0\" cellpadding=\"2\" rules=\"all\" border=\"1\" style=\"width:100%;border-collapse:collapse;\">";
            strEmailBody += "<tr align=\"left\" style=\"background-color:Gainsboro;\">";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:30%;\">Customer ID</th>";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:70%;\">Customer Name</th>";
            strEmailBody += "</tr>";

            foreach (CustomerEntity c in soldTos)
            {
                strEmailBody += "<tr><td>" + c.CustomerID + "</td><td>" + c.CustomerName + "</td></tr>";
            }

            strEmailBody += "</table>";
            strEmailBody += "</div>";
            strEmailBody += "<tr>";
            strEmailBody += "<td>&nbsp;</td>";
            strEmailBody += "</tr>";
            // Start Ship To
            strEmailBody += "<tr>";
            strEmailBody += "<td>Customer <b>Ship To</b> if Different than Customer Sold To</td>";
            strEmailBody += "</tr><tr>";
            strEmailBody += "<td><table width=\"100%\">";
            strEmailBody += "<tr><td>Customer Number:</td><td>" + this.txtShipToCustomerID.Text + "</td></tr>";
            strEmailBody += "<tr><td>Name:</td><td>" + this.lblShipToCustomerName.Text + "</td></tr>";
            strEmailBody += "<tr><td>Address:</td><td>" + this.lblShipToCustomerAddress.Text + "</td></tr>";
            strEmailBody += "<tr><td>City, State, Zip:</td><td>" + this.lblShipToCustomerCity.Text + ",&nbsp;" + this.lblShipToCustomerState.Text + "&nbsp;&nbsp;" + this.lblShipToCustomerZip.Text + "</td></tr>";
            strEmailBody += "</td></tr></table>";

            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";

            // Start Material Detail
            strEmailBody += "<table style=\"width:" + tablewidth + "\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td>";
            strEmailBody += "<div>";
            strEmailBody += "<table cellspacing=\"0\" cellpadding=\"2\" rules=\"all\" border=\"1\" style=\"width:100%;border-collapse:collapse;\">";
            strEmailBody += "<tr align=\"left\" style=\"background-color:Gainsboro;\">";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:13%;\">Capital Group</th>";
            strEmailBody += "<th align=\"right\" scope=\"col\" style=\"width:8%;\">Qty</th>";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:13%;\">Material ID</th>";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:38%;\">Description</th>";
            strEmailBody += "<th align=\"right\" scope=\"col\" style=\"width:14%;\">Annual Value of Equipment</th>";
            strEmailBody += "<th align=\"right\" scope=\"col\" style=\"width:14%;\">Annual Target Purchase</th>";
            strEmailBody += "</tr>";

            foreach (MaterialEntity m in materials)
            {
                strEmailBody += "<tr align=\"left\">";
                strEmailBody += "<td style=\"font-size:8pt;\">" + m.CapitalGroup + "</td>";
                strEmailBody += "<td align=\"right\" style=\"font-size:8pt;text-align:right;\">" + m.Qty.ToString() + "</td>";
                strEmailBody += "<td style=\"font-size:8pt;\">" + m.MaterialID + "</td>";
                strEmailBody += "<td style=\"font-size:8pt;\">" + m.MaterialDesc + "</td>";
                strEmailBody += "<td align=\"right\" style=\"font-size:8pt;text-align:right;\">" + String.Format("{0:c0}", m.AnnualValue) + "</td>";
                strEmailBody += "<td align=\"right\" style=\"font-size:8pt;text-align:right;\">" + String.Format("{0:c0}", m.AnnualRequiredPurchases) + "</td>";
                strEmailBody += "</tr>";
            }

            strEmailBody += "</table>";
            strEmailBody += "</div>";
            strEmailBody += "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";

            // Start Material Summary
            strEmailBody += "<table style=\"width:" + tablewidth + "\">";
            strEmailBody += "<tr><td>&nbsp;</td></tr>";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"width:60%;\">";
            strEmailBody += "<table width=\"100%\">";
            strEmailBody += "<tr>";
            strEmailBody += "<td style=\"width:100%; font-size:10pt; background-color: #FFFF00;color: #CC3300; font-weight:bold; text-align:center\">";
            strEmailBody += this.lblSummaryNote.Text;
            strEmailBody += "</td>";                   
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "</td>";

            strEmailBody += "<td>";
            strEmailBody += "<table align=\"right\" cellspacing=\"0\" cellpadding=\"2\" rules=\"all\" border=\"1\" style=\"width:270px;border-collapse:collapse;\">";
            strEmailBody += "<tr align=\"left\" style=\"background-color:Gainsboro;\">";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:50%;\">Capital Group</th>";
            strEmailBody += "<th align=\"left\" scope=\"col\" style=\"width:50%;text-align:right\">Target Purchase</th>";
            strEmailBody += "</tr>";

            foreach (GridViewRow r in this.gvSummaryMaterial.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    Label lblCapitalGroup = (Label)r.FindControl("lblCapitalGroup");
                    Label lblAnnualRequiredPurchases = (Label)r.FindControl("lblAnnualRequiredPurchases");

                    strEmailBody += "<tr align=\"left\">";
                    strEmailBody += "<td style=\"font-size:8pt;\">" + lblCapitalGroup.Text + "</td>";
                    strEmailBody += "<td align=\"right\" style=\"font-size:8pt;text-align:right\">" + String.Format("{0:c0}", lblAnnualRequiredPurchases.Text) + "</td>";
                    strEmailBody += "</tr>";
                }
            }

            strEmailBody += "</table>";
            strEmailBody += "</td>";
            strEmailBody += "</tr>";
            strEmailBody += "</table>";
            strEmailBody += "</div>";

            return strEmailBody;
        }

    } //// end class
}