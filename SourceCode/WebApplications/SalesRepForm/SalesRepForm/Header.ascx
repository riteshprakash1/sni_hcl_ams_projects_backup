﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="SalesRepForm.Header" %>

<table cellspacing="0" cellpadding="5" style="width:960px; text-align:center;" border="0">
    <tr>
        <td style="width:30%">
            <div  style="text-align:left">
                <asp:Image ID="Image1" ImageUrl="~/images/logo.gif" runat="server" Width="221" AlternateText="Smith &amp; Nephew" />
            </div>
        </td>
		<td style="width:40%; vertical-align:top; text-align:left;color:#FF7300">&nbsp;</td>
        <td style="width:30%; text-align:right">
                <asp:Button ID="btnAppName" BorderStyle="None" ForeColor="DarkGray" BackColor="White"
                    BorderColor="White" Text="Sales Rep Form" Font-Size="16pt" Font-Bold="True" runat="server"
                    CausesValidation="False"></asp:Button>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr />
        </td>
    </tr>
</table>
        <table width="100%" border="0" id="tblRequestorInformation" runat="server">
            <tr>
                <td colspan="2" class="titleheader">Requestor Information</td>
            </tr>
            <tr>
                <td style="width:10%">Requestor Name:</td>
                <td style="width:30%"><asp:TextBox ID="txtRequestorName" Enabled="false" Font-Size="8pt" MaxLength="75" runat="server" Width="200px"></asp:TextBox></td>
                <td style="width:12%">Requestor Phone: *</td>
                <td><asp:TextBox ID="txtRequestorPhone" Font-Size="8pt" MaxLength="30" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" 
                        ControlToValidate="txtRequestorPhone" ErrorMessage="Requestor Phone is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Requestor Email: *</td>
                <td><asp:TextBox ID="txtRequestorEmail" MaxLength="75" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" 
                        ControlToValidate="txtRequestorEmail" ErrorMessage="Requestor Email is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtRequestorEmail" runat="server" 
                        ErrorMessage="Requestor Email format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
            </tr>
        </table>