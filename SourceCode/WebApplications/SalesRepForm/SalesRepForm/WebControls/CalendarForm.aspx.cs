﻿// -------------------------------------------------------------
// <copyright file="CalendarForm.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRepForm.WebControls
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// This is the Calendar Form page.
    /// </summary>
    public partial class CalendarForm : System.Web.UI.Page
    {
        #region "Private and Protected Attributes"

        /// <summary>
        /// The color of the even row.
        /// </summary>
        private string myEvenRowColor;

        /// <summary>
        /// This is our selected date.
        /// </summary>
        private string mySelectedDate;

        /// <summary>
        /// This is our control id.
        /// </summary>
        private string myControlId;
        #endregion //Private and Protected Attributes

        #region "Public Properties"
        /// <summary>
        /// Gets or sets the control id.
        /// </summary>
        /// <value>The control id.</value>
        public string ControlId
        {
            get { return this.myControlId; }
            set { this.myControlId = value; }
        }

        /// <summary>
        /// Gets or sets the selected date.
        /// </summary>
        /// <value>The selected date.</value>
        public string SelectedDate
        {
            get { return this.mySelectedDate; }
            set { this.mySelectedDate = value; }
        }

        /// <summary>
        /// Gets or sets the color of the even row.
        /// </summary>
        /// <value>The color of the even row.</value>
        public string EvenRowColor
        {
            get { return this.myEvenRowColor; }
            set { this.myEvenRowColor = value; }
        }
        #endregion //Public Properties

        #region "Constructors / Destructors"
        #endregion //Constructors / Destructors

        #region "Public Methods"
        #endregion //Public Methods

        #region "Private Methods"
        #endregion //Private Methods

        #region "Events"
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ControlId = Request.QueryString["ControlId"];
        }

        /// <summary>
        /// Handles the SelectionChanged event of the controlCalendar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void _controlCalendar_SelectionChanged(object sender, EventArgs e)
        {
            ////  _selectedDate = _controlCalendar.SelectedDate.ToShortDateString();
            this.SelectedDate = this._controlCalendar.SelectedDate.ToString("MM/dd/yyyy");
            this.Literal1.Text = "<script type='text/javascript'>ReturnDate();</script>";
        }
        #endregion //Events
    } //// end class
}
