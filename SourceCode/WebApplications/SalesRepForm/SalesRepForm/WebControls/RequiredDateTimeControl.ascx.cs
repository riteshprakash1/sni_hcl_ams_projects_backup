﻿// -------------------------------------------------------------
// <copyright file="RequiredDateTimeControl.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRepForm.WebControls
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// This is the Required Date Time UserControl.
    /// </summary>
    public partial class RequiredDateTimeControl : System.Web.UI.UserControl
    {
        #region "Protected Attributes"

        /// <summary>
        /// This is our calendar page path.
        /// </summary>
        private string myCalendarPagePath;

        /// <summary>
        /// This is our control id.
        /// </summary>
        private string myControlID;
        #endregion //Private Attributes

        #region "Public Properties"
        /// <summary>
        /// Gets or sets the control id.
        /// </summary>
        /// <value>The control id.</value>
        public string ControlID
        {
            get { return this.myControlID; }
            set { this.myControlID = value; }
        }

        /// <summary>
        /// Gets or sets the calendar page path.
        /// </summary>
        /// <value>The calendar page path.</value>
        public string CalendarPagePath
        {
            get { return this.myCalendarPagePath; }
            set { this.myCalendarPagePath = value; }
        }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>The date for the control.</value>
        public string Date
        {
            get
            {
                return this._dateTextBox.Text;
            }

            set
            {
                this._dateTextBox.Text = value.Replace("1/1/2001", String.Empty).Replace("1/1/0001", String.Empty).Replace("1/1/0001 12:00:00 AM", String.Empty);
            }
        }

        /// <summary>
        /// Sets the date control text box width in pixels.
        /// </summary>
        /// <value>The date control text box width in pixels.</value>
        public string DateControlTextBoxWidthInPixels
        {
            set
            {
                this._dateTextBox.Width = Unit.Pixel(Convert.ToInt32(value));
            }
        }
        #endregion //Public Properties

        #region "Events"
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ControlID = this.ID;

            this.CalendarPagePath = "WebControls/CalendarForm.aspx";

            this.ibtnCalendar.OnClientClick = "javascript:OpenCalendarWindow('" + this.CalendarPagePath + "', 'ctl00$ContentPlaceHolder1$" + this.ControlID + "');";
            ////}
        }
        #endregion ////Events
    } // end class
}