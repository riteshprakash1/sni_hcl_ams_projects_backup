﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RepSystemAccess.aspx.cs" Inherits="SalesRepForm.RepSystemAccess"  MasterPageFile="MasterRep.Master" Title="System Access Change"%>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table style="width:100%">
            <tr>
                <td class="titleheader">Sales/Service Rep Information</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td colspan="4"><asp:ValidationSummary ID="vldSummary2" ForeColor="#FF7300" runat="server" />
                    <asp:Label ID="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
                    <asp:HiddenField ID="hdnNonEmployeeID" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnChangeType" Value="Rep System Access Change" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width:12%">Last Name: *</td>
                <td style="width:24%"><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" 
                        runat="server" Width="200px" ontextchanged="txtLastName_TextChanged" 
                        AutoPostBack="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtLastName" ErrorMessage="Last Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td style="width:11%">Given First Name: *</td>
                <td style="width:23%"><asp:TextBox ID="txtFirstName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtFirstName" ErrorMessage="Given First Name is required.">*</asp:RequiredFieldValidator></td>
                <td style="width:12%">Middle Name:</td>
                <td><asp:TextBox ID="txtMiddleName" MaxLength="25" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox></td>
            </tr>
             <tr>
                <td colspan="5">
                   <asp:GridView ID="gvFindUsers" runat="server" Width="750" AutoGenerateColumns="False"
                        EmptyDataText="There are no users." OnRowEditing="GV_Select"
                         CellPadding="2" DataKeyNames="NonEmployeeID,MidName,Street1,Street2,PostalCode" GridLines="Both" 
                        >
                        <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnSelect" Width="50px" Height="18" runat="server" Text="Select" CommandName="Edit"
				                        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
					                    ></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastName" Font-Size="8pt" runat="server"  Text='<%# Eval("lastname")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstName" Font-Size="8pt" runat="server"  Text='<%# Eval("firstname")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress1" Font-Size="8pt" runat="server"  Text='<%# Eval("street1")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City">
                                <ItemTemplate>
                                    <asp:Label ID="lblCity" Font-Size="8pt" runat="server"  Text='<%# Eval("city")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State">
                                <ItemTemplate>
                                    <asp:Label ID="lblState" Font-Size="8pt" runat="server"  Text='<%# Eval("state")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zip Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblZip" Font-Size="8pt" runat="server"  Text='<%# Eval("PostalCode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                       </Columns>
                    </asp:GridView>                     
                </td>
                 <td style="text-align:center; vertical-align:top"><asp:Button ID="btnClearGrid" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                        Text="Hide Grid" onclick="btnClearGrid_Click" />
                </td>
           </tr>
            <tr>
                <td>Address 1: *</td>
                <td><asp:TextBox ID="txtAddress1" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtAddress1" ErrorMessage="Address 1 is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Address 2:</td>
                <td><asp:TextBox ID="txtAddress2" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>City: *</td>
                <td><asp:TextBox ID="txtCity" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtCity" ErrorMessage="City is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>State: *</td>
                <td><asp:DropDownList ID="ddlState" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="ddlState" ErrorMessage="State is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Zip Code: *</td>
                <td><asp:TextBox ID="txtZipCode" MaxLength="20" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                        ControlToValidate="txtZipCode" ErrorMessage="Zip Code is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Office Phone: *</td>
                <td><asp:TextBox ID="txtOfficePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                        ControlToValidate="txtOfficePhone" ErrorMessage="Office Phone is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Fax:</td>
                <td><asp:TextBox ID="txtFax" MaxLength="30" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox></td>
                <td>Mobile Phone: *</td>
                <td><asp:TextBox ID="txtMobile" MaxLength="30" Font-Size="8pt" runat="server" 
                        Width="125px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                        ControlToValidate="txtMobile" ErrorMessage="Mobile Phone is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Sales District/<br />Territory Name: *</td>
                <td><asp:TextBox ID="txtDistrictName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txtDistrictName" ErrorMessage="Sales District/Territory Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Sales District/ Territory Number: *</td>
                <td><asp:TextBox ID="txtDistrictID" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                        ControlToValidate="txtDistrictID" ErrorMessage="Sales District/Territory Number is required.">*</asp:RequiredFieldValidator></td>
            </tr>
        </table>
        <table style="width:960px">
            <tr>
                <td class="titleheader">Other Requests</td>
                <td>&nbsp;</td>
            </tr>
        </table>
         <table style="width:100%">
            <tr>
                <td style="width:15%">Sales Life On-line:</td>
                <td style="width:12%"><asp:DropDownList ID="ddlOrthoOnline" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width:12%">Ortho On-line Service:</td>
                <td><asp:DropDownList ID="ddlOrthoOnlineService" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Sales Reporting Service:</td>
                <td><asp:DropDownList ID="ddlSalesReporting" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>Learn Center Access:</td>
                <td><asp:DropDownList ID="ddlLearnCenter" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Kronos:</td>
                <td><asp:DropDownList ID="ddlKronos" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td style="font-size:9pt">Any Technology Requests – please ensure these are requested through the  
                    <asp:Label ID="lblServicePortalName" runat="server"></asp:Label> 
                    (<asp:HyperLink ID="lnkServicePortal" Target="_blank" CssClass="ServicePortal" runat="server">HyperLink</asp:HyperLink>)</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table style="width:960px">
             <tr>
                <td style="vertical-align:top">Comments: </td>
                <td colspan="5"><asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="3"  
                        Font-Size="8pt" runat="server" Width="500px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table style="width:960px">
            <tr>
                <td style="width:33%; text-align:center"><asp:Button ID="btnSubmit" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" /></td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnClear" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Clear" 
                        onclick="btnClear_Click" /></td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnMenu" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Menu" 
                        onclick="btnMenu_Click" /></td>
            </tr>
        </table>
        <asp:Literal ID="ltlEmail" runat="server"></asp:Literal>
    </div>
</asp:Content>