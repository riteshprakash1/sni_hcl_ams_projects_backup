﻿// ------------------------------------------------------------------
// <copyright file="NewSalesRep.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    using SalesRepForm.Classes;

    /// <summary>
    /// This is the NewSalesRep page.
    /// </summary>
    public partial class NewSalesRep : System.Web.UI.Page
    {
        private Header Header1;

        /// <summary>
        /// Gets or sets the relationship.
        /// </summary>
        /// <value>The relationship.</value>
        private string Relationship
        {
            get
            {
                string strValue = String.Empty;
                foreach (ListItem i in rdbRelationship.Items)
                {
                    if (i.Selected)
                    {
                        strValue = i.Text;
                    }
                }

                return strValue;
            }

            set
            {
                foreach (ListItem i in rdbRelationship.Items)
                {
                    i.Selected = Convert.ToBoolean(value == i.Text);
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Header1 = (Header)Page.Master.FindControl("Header1");
                        
            if (!Page.IsPostBack)
            {
                EnterButton.TieButtonDoTab(this.txtAddress1);
                EnterButton.TieButtonDoTab(this.txtAddress2);
                EnterButton.TieButtonDoTab(this.txtAssistantName);
                EnterButton.TieButtonDoTab(this.txtCity);
                EnterButton.TieButtonDoTab(this.txtCostCenter);
                ////EnterButton.TieButtonDoTab(this.txtEffectiveDate);
                EnterButton.TieButtonDoTab(this.txtFax);
                EnterButton.TieButtonDoTab(this.txtFirstName);
                EnterButton.TieButtonDoTab(this.txtHomePhone);
                EnterButton.TieButtonDoTab(this.txtLastName);
                EnterButton.TieButtonDoTab(this.txtManager);
                EnterButton.TieButtonDoTab(this.txtMiddleName);
                EnterButton.TieButtonDoTab(this.txtMobile);
                EnterButton.TieButtonDoTab(this.txtOfficePhone);
                EnterButton.TieButtonDoTab(this.txtPreferredName);
                EnterButton.TieButtonDoTab(this.txtRepEmail);
                EnterButton.TieButtonDoTab(this.txtDistrictName);
                EnterButton.TieButtonDoTab(this.txtDistrictID);
                EnterButton.TieButtonDoTab(this.Header1.RequestorEmail);
                EnterButton.TieButtonDoTab(this.Header1.RequestorName);
                EnterButton.TieButtonDoTab(this.Header1.RequestorPhone);
                EnterButton.TieButtonDoTab(this.txtZipCode);
                EnterButton.TieButtonDoTab(this.txtSNEmail);
                EnterButton.TieButtonDoTab(this.txtEmployeeID);

                this.lnkServicePortal.Text = ConfigurationManager.AppSettings["ServicePortalURL"];
                this.lnkServicePortal.NavigateUrl = ConfigurationManager.AppSettings["ServicePortalURL"];
                this.lblServicePortalName.Text = ConfigurationManager.AppSettings["ServicePortalName"];

                string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                NonEmployee sr = new NonEmployee(connectionString);

                IEnumerable<States> myStates = from state in sr.States
                                               orderby state.States1
                                               select state;
                this.ddlState.Items.Clear();
                ddlState.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (States st in myStates)
                {
                    ddlState.Items.Add(new ListItem(st.States1, st.Statescode));
                }

                //GbuProducts
                IEnumerable<GbuProducts> myGbuProducts = from products in sr.GbuProducts
                                                   orderby products.GbuProductName
                                                         select products;

                this.ddlGbuProduct.Items.Clear();
                ddlGbuProduct.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (GbuProducts p in myGbuProducts)
                {
                    this.ddlGbuProduct.Items.Add(new ListItem(p.GbuProductName,p.GbuProductName));
                }

                //SalesRepTypes
                IEnumerable<SalesRepType> mySRTypes = from srTypes in sr.SalesRepType
                                                      orderby srTypes.SalesRepTypeID
                                                      select srTypes;

                this.ddlRepType.Items.Clear();
                ddlRepType.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (SalesRepType t in mySRTypes)
                {
                    this.ddlRepType.Items.Add(new ListItem(t.SalesRepTypeName, t.SalesRepTypeName));
                }

            }

            this.lblMsg.Text = String.Empty;
        } // end Page_Load
 
        /// <summary>
        /// Sets the email body.
        /// </summary>
        /// <returns>the email body string</returns>
        protected string SetEmailBody()
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">New Sales/Service Rep Information</td>";
            strBody += "</tr>";
            strBody += "</table>";
            strBody += "<table border=1>";
            strBody += "<tr>";
            strBody += "<td>Relationship:</td>";
            strBody += "<td>" + this.Relationship + "</td>";
            strBody += "<td>&nbsp;</td><td>&nbsp;</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Type:</td>";
            strBody += "<td>" + this.ddlRepType.SelectedValue + "</td>";
            strBody += "<td>Effective Date:</td>";
            strBody += "<td>" + this.txtEffectiveDate.Date + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>GBU Product Line:</td>";
            strBody += "<td>" + this.ddlGbuProduct.SelectedValue + "</td>";
            strBody += "<td>Employee ID:</td>";
            strBody += "<td>" + this.txtEmployeeID.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td style=\"width:20%\">Last Name:</td>";
            strBody += "<td style=\"width:25%\">" + txtLastName.Text + "</td>";
            strBody += "<td style=\"width:25%\">Given First Name:</td>";
            strBody += "<td>" + txtFirstName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>" + this.txtMiddleName.Text + "</td>";
            strBody += "<td>Preferred First Name:</td>";
            strBody += "<td>" + this.txtPreferredName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Address 1:</td>";
            strBody += "<td>" + this.txtAddress1.Text + "</td>";
            strBody += "<td>Address 2:</td>";
            strBody += "<td>" + this.txtAddress2.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>City:</td>";
            strBody += "<td>" + this.txtCity.Text + "</td>";
            strBody += "<td>State:</td>";
            strBody += "<td>" + this.ddlState.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Zip Code:</td>";
            strBody += "<td>" + this.txtZipCode.Text + "</td>";
            strBody += "<td>Home Phone:</td>";
            strBody += "<td>" + this.txtHomePhone.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Office Phone:</td>";
            strBody += "<td>" + this.txtOfficePhone.Text + "</td>";
            strBody += "<td>Fax:</td>";
            strBody += "<td>" + this.txtFax.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Mobile Phone:</td>";
            strBody += "<td>" + this.txtMobile.Text + "</td>";
            strBody += "<td>Personal E-Mail:</td>";
            strBody += "<td>" + this.txtRepEmail.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Cost Center:</td>";
            strBody += "<td>" + this.txtCostCenter.Text + "</td>";
            strBody += "<td>Sales Rep Being Assisted by:</td>";
            strBody += "<td>" + this.txtAssistantName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Territory/District Mgr:</td>";
            strBody += "<td>" + this.txtManager.Text + "</td>";
            strBody += "<td>Transfer From S&amp;N Role:</td>";
            strBody += "<td>" + this.ddlTransferRole.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Sales District/<br>Territory Name:</td>";
            strBody += "<td>" + this.txtDistrictName.Text + "</td>";
            strBody += "<td>Sales District/Territory Number:</td>";
            strBody += "<td>" + this.txtDistrictID.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Sales Rep ID:</td>";
            strBody += "<td>" + this.txtSalesRepID.Text + "</td>";
            strBody += "<td>S&N E-Mail:</td>";
            strBody += "<td>" + this.txtSNEmail.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Prior Experience:</td>";
            strBody += "<td>" + this.ddlPriorExperience.SelectedValue + "</td>";
            strBody += "<td>Prior S&N Experience:</td>";
            strBody += "<td>" + this.ddlPriorSNExperience.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Experience Comments:</td>";
            strBody += "<td valign=\"top\" colspan=\"3\">" + this.txtExperienceComments.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Other Requests</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Sales Life On-line:</td>";
            strBody += "<td>" + this.ddlOrthoOnline.SelectedValue + "</td>";
            strBody += "<td>Ortho On-line Service:</td>";
            strBody += "<td>" + this.ddlOrthoOnlineService.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Sales Reporting Service:</td>";
            strBody += "<td>" + this.ddlSalesReporting.SelectedValue + "</td>";
            strBody += "<td>Learn Center Access:</td>";
            strBody += "<td>" + this.ddlLearnCenter.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Kronos:</td>";
            strBody += "<td>" + this.ddlKronos.SelectedValue + "</td>";
            strBody += "<td>&nbsp;</td><td>&nbsp;</td>";
            strBody += "</tr>";
            strBody += "</table>";

            return strBody;
        }

        /// <summary>
        /// Handles the Click event of the btnMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewSalesRep.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnSubmitNndChange control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmitNndChange_Click(object sender, EventArgs e)
        {
            string strID = this.SubmitForm();

            if (strID != "0")
            {
                Response.Redirect("AccountChange.aspx?PersonID=" + strID);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitForm();
        } // end btnSubmit_Click

        private string SubmitForm()
        {
            string strRequestID = "0";
            Page.Validate();

            if (this.Relationship == "1099")
            {
                ////this.vldBackgroundCheck.IsValid = this.chkBackgroundCheck.Checked;
                this.vldContractSigned.IsValid = this.chkContractSigned.Checked;
            }

            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    ChangeRequest p = new ChangeRequest();
                    DateTime datNow = DateTime.Now;

                    p.ChangeType = this.hdnChangeType.Value;
                    p.Address1 = this.txtAddress1.Text;
                    p.Address2 = this.txtAddress2.Text;
                    p.Assistant = this.txtAssistantName.Text;
                    ////p.BackgroundCheck = chkBackgroundCheck.Checked.ToString();
                    p.City = this.txtCity.Text;
                    p.ContractSigned = this.chkContractSigned.Checked.ToString();
                    p.CostCenter = this.txtCostCenter.Text;
                    p.CreateDate = datNow;
                    p.CreatedBy = ADUtility.GetAppUsername(Request);
                    p.CTRep = string.Empty;
                    p.DataWarehouse = string.Empty;
                    p.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Date);
                    p.EmailService = string.Empty;
                    p.EmployeeID = this.txtEmployeeID.Text;
                    p.ExperienceComments = this.txtExperienceComments.Text;
                    p.Fax = this.txtFax.Text;
                    p.FirstName = this.txtFirstName.Text;
                    p.GBUProduct = this.ddlGbuProduct.SelectedValue;
                    p.HomePhone = this.txtHomePhone.Text;
                    p.Kronos = this.ddlKronos.SelectedValue;
                    p.LastName = this.txtLastName.Text;
                    p.LearnCenter = this.ddlLearnCenter.SelectedValue;
                    p.ManagerName = this.txtManager.Text;
                    p.DistrictName = this.txtDistrictName.Text;
                    p.District = this.txtDistrictID.Text;
                    p.MiddleName = this.txtMiddleName.Text;
                    p.Mobile = this.txtMobile.Text;
                    p.OfficePhone = this.txtOfficePhone.Text;
                    p.OrthoOnline = this.ddlOrthoOnline.SelectedValue;
                    p.OrthoOnlineService = this.ddlOrthoOnlineService.SelectedValue;
                    p.PreferredName = this.txtPreferredName.Text;
                    p.PriorExperience = this.ddlPriorExperience.SelectedValue;
                    p.PriorSNExperience = this.ddlPriorSNExperience.SelectedValue;
                    p.Relationship = this.Relationship;
                    p.EmailAddress = this.txtRepEmail.Text;
                    p.SNEmailAddress = this.txtSNEmail.Text;
                    p.RepType = this.ddlRepType.SelectedValue;
                    p.RequestorEmail = this.Header1.RequestorEmail.Text;
                    p.RequestorName = this.Header1.RequestorName.Text;
                    p.RequestorPhone = this.Header1.RequestorPhone.Text;
                    p.SalesReporting = this.ddlSalesReporting.SelectedValue;
                    p.SAP = string.Empty;
                    p.SNUG = string.Empty;
                    p.State = this.ddlState.SelectedValue;
                    p.TransferSNRole = this.ddlTransferRole.SelectedValue;
                    p.UpdateDate = datNow;
                    p.UpdatedBy = ADUtility.GetAppUsername(Request);
                    p.VoiceMail = string.Empty;
                    p.Zip = this.txtZipCode.Text;

                    p.MGMobileAccess = string.Empty;
                    p.MGMobileDevice = string.Empty;
                    p.MGWebAccess = string.Empty;
                    p.SalesRepID = this.txtSalesRepID.Text;

                    sr.ChangeRequest.InsertOnSubmit(p);
                    sr.SubmitChanges();

                    Debug.WriteLine("New ID: " + p.RequestID.ToString());

                    string strSubject = "New Sales Rep Form Submitted by " + p.RequestorName;
                    string strBody = this.SetEmailBody();

                    ////                 this.ltlEmail.Text = strBody;

                    Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, Page.Title, null);

                    strRequestID = p.RequestID.ToString();
                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } // end Page.IsValid

            return strRequestID;
        }

    } //// end class
}  //// end namespace
