﻿// ------------------------------------------------------------------
// <copyright file="ADUserEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// This is the Active Directory User Entity.
    /// </summary>
    public class ADUserEntity
    {
        /// <summary>
        /// This is the username (AD alias SAMAccountName).
        /// </summary>
        private string strUsername;

        /// <summary>
        /// This is the first name (AD alias givenname).
        /// </summary>
        private string strFirstName;

        /// <summary>
        /// This is the last name (AD alias sn).
        /// </summary>
        private string strLastName;

        /// <summary>
        /// This is the display name (AD alias displayname).
        /// </summary>
        private string strDisplayName;

        /// <summary>
        /// This is the Email Address (AD alias mail).
        /// </summary>
        private string strEmail;

        /// <summary>
        /// This is the adspath (AD alias adspath).
        /// </summary>
        private string strAdspath;
 
        /// <summary>
        /// This is the CN (AD alias CN).
        /// </summary>
        private string strCN;
        
        /// <summary>
        /// This is the DistinguishedName (AD alias distinguishedname).
        /// </summary>
        private string strDistinguishedName;

        /// <summary>
        /// This is the Department (AD alias department).
        /// </summary>
        private string strDepartment;

        /// <summary>
        /// This is the DirectoryLocation (AD alias directorylocation).
        /// </summary>
        private string strDirectoryLocation;

        /// <summary>
        /// This is the Manager (AD alias manager).
        /// </summary>
        private string strManager;

        /// <summary>
        /// This is the Mobile (AD alias mobile).
        /// </summary>
        private string strMobile;

        /// <summary>
        /// This is the TelephoneNumber (AD alias telephonenumber).
        /// </summary>
        private string strTelephoneNumber;

        /// <summary>
        /// This is the Title (AD alias title).
        /// </summary>
        private string strTitle;

        /// <summary>
        /// This is the PhysicalDeliveryOfficeName (AD alias physicaldeliveryofficename).
        /// </summary>
        private string strPhysicalDeliveryOfficeName;

        /// <summary>
        /// This is the Company (AD alias company).
        /// </summary>
        private string strCompany;

        /// <summary>
        /// Initializes a new instance of the <see cref="ADUserEntity"/> class.
        /// </summary>
        public ADUserEntity()
        {
            this.UserName = String.Empty;
            this.LastName = String.Empty;
            this.FirstName = String.Empty;
            this.DisplayName = String.Empty;
            this.Email = String.Empty;
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName
        {
            get
            {
                return this.strUsername;
            }

            set
            {
                this.strUsername = value;
            }
        }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName
        {
            get
            {
                return this.strLastName;
            }

            set
            {
                this.strLastName = value;
            }
        }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName
        {
            get
            {
                return this.strFirstName;
            }

            set
            {
                this.strFirstName = value;
            }
        }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public string DisplayName
        {
            get
            {
                return this.strDisplayName;
            }

            set
            {
                this.strDisplayName = value;
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get
            {
                return this.strEmail;
            }

            set
            {
                this.strEmail = value;
            }
        }

        /// <summary>
        /// Gets or sets the adspath.
        /// </summary>
        /// <value>The adspath.</value>
        public string Adspath
        {
            get
            {
                return this.strAdspath;
            }

            set
            {
                this.strAdspath = value;
            }
        }

        /// <summary>
        /// Gets or sets the CN.
        /// </summary>
        /// <value>The CN used by the ads</value>
        public string CN
        {
            get
            {
                return this.strCN;
            }

            set
            {
                this.strCN = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the distinguished.
        /// </summary>
        /// <value>The name of the distinguished.</value>
        public string DistinguishedName
        {
            get
            {
                return this.strDistinguishedName;
            }

            set
            {
                this.strDistinguishedName = value;
            }
        }

        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        /// <value>The department.</value>
        public string Department
        {
            get
            {
                return this.strDepartment;
            }

            set
            {
                this.strDepartment = value;
            }
        }

        /// <summary>
        /// Gets or sets the directory location.
        /// </summary>
        /// <value>The directory location.</value>
        public string DirectoryLocation
        {
            get
            {
                return this.strDirectoryLocation;
            }

            set
            {
                this.strDirectoryLocation = value;
            }
        }

        /// <summary>
        /// Gets or sets the manager.
        /// </summary>
        /// <value>The manager.</value>
        public string Manager
        {
            get
            {
                return this.strManager;
            }

            set
            {
                this.strManager = value;
            }
        }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile
        {
            get
            {
                return this.strMobile;
            }

            set
            {
                this.strMobile = value;
            }
        }

        /// <summary>
        /// Gets or sets the telephone number.
        /// </summary>
        /// <value>The telephone number.</value>
        public string TelephoneNumber
        {
            get
            {
                return this.strTelephoneNumber;
            }

            set
            {
                this.strTelephoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title
        {
            get
            {
                return this.strTitle;
            }

            set
            {
                this.strTitle = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the physical delivery office.
        /// </summary>
        /// <value>The name of the physical delivery office.</value>
        public string PhysicalDeliveryOfficeName
        {
            get
            {
                return this.strPhysicalDeliveryOfficeName;
            }

            set
            {
                this.strPhysicalDeliveryOfficeName = value;
            }
        }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>The company.</value>
        public string Company
        {
            get
            {
                return this.strCompany;
            }

            set
            {
                this.strCompany = value;
            }
        }

        /// <summary>
        /// Sorts the user table.
        /// </summary>
        /// <param name="dtUsers">The users datatable.</param>
        /// <param name="sortBy">The sort string</param>
        /// <returns>the sorted datatable</returns>
        public static DataTable SortUserTable(DataTable dtUsers, string sortBy)
        {
            DataTable dtSorted = dtUsers.Clone();
            DataRow[] rows = dtUsers.Select(null, sortBy);
            foreach (DataRow r in rows)
            {
                dtSorted.ImportRow(r);
            }

            return dtSorted;
        }

        /// <summary>
        /// Gets the AD user entity table.
        /// </summary>
        /// <returns>The AD User DataTable</returns>
        public static DataTable GetADUserEntityTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("UserName", typeof(string)));
            dt.Columns.Add(new DataColumn("LastName", typeof(string)));
            dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
            dt.Columns.Add(new DataColumn("DisplayName", typeof(string)));
            dt.Columns.Add(new DataColumn("Email", typeof(string)));
            dt.Columns.Add(new DataColumn("adspath", typeof(string)));
            dt.Columns.Add(new DataColumn("cn", typeof(string)));
            dt.Columns.Add(new DataColumn("distinguishedname", typeof(string)));
            dt.Columns.Add(new DataColumn("department", typeof(string)));
            dt.Columns.Add(new DataColumn("directorylocation", typeof(string)));
            dt.Columns.Add(new DataColumn("manager", typeof(string)));
            dt.Columns.Add(new DataColumn("mobile", typeof(string)));
            dt.Columns.Add(new DataColumn("telephonenumber", typeof(string)));
            dt.Columns.Add(new DataColumn("title", typeof(string)));
            dt.Columns.Add(new DataColumn("physicaldeliveryofficename", typeof(string)));
            dt.Columns.Add(new DataColumn("company", typeof(string)));
            return dt;
        }

        /// <summary>
        /// Sets the user from property collection.
        /// </summary>
        /// <param name="userCollection">The user collection.</param>
        /// <returns>the ADUserEntity</returns>
        public static ADUserEntity SetUserFromPropertyCollection(System.DirectoryServices.PropertyCollection userCollection)
        {
            ADUserEntity myUser = new ADUserEntity();
            if (userCollection["mail"].Value != null)
            {
                myUser.Email = userCollection["mail"].Value.ToString();
            }

            if (userCollection["sn"].Value != null)
            {
                myUser.LastName = userCollection["sn"].Value.ToString();
            }

            if (userCollection["givenname"].Value != null)
            {
                myUser.FirstName = userCollection["givenname"].Value.ToString();
            }

            if (userCollection["displayname"].Value != null)
            {
                myUser.DisplayName = userCollection["displayname"].Value.ToString();
            }

            if (userCollection["samaccountname"].Value != null)
            {
                myUser.UserName = userCollection["samaccountname"].Value.ToString();
            }

            if (userCollection["adspath"].Value != null)
            {
                myUser.Adspath = userCollection["adspath"].Value.ToString();
            }

            if (userCollection["cn"].Value != null)
            {
                myUser.CN = userCollection["cn"].Value.ToString();
            }

            if (userCollection["company"].Value != null)
            {
                myUser.Company = userCollection["company"].Value.ToString();
            }

            if (userCollection["department"].Value != null)
            {
                myUser.Department = userCollection["department"].Value.ToString();
            }

            if (userCollection["directorylocation"].Value != null)
            {
                myUser.DirectoryLocation = userCollection["directorylocation"].Value.ToString();
            }

            if (userCollection["distinguishedname"].Value != null)
            {
                myUser.DistinguishedName = userCollection["distinguishedname"].Value.ToString();
            }

            if (userCollection["manager"].Value != null)
            {
                myUser.Manager = userCollection["manager"].Value.ToString();
            }

            if (userCollection["mobile"].Value != null)
            {
                myUser.Mobile = userCollection["mobile"].Value.ToString();
            }

            if (userCollection["physicaldeliveryofficename"].Value != null)
            {
                myUser.PhysicalDeliveryOfficeName = userCollection["physicaldeliveryofficename"].Value.ToString();
            }

            if (userCollection["telephonenumber"].Value != null)
            {
                myUser.TelephoneNumber = userCollection["telephonenumber"].Value.ToString();
            }

            if (userCollection["title"].Value != null)
            {
                myUser.Title = userCollection["title"].Value.ToString();
            }

            return myUser;
        }

        /// <summary>
        /// Sets the user data row.
        /// </summary>
        /// <param name="user">The ADUser object.</param>
        /// <param name="r">The DataRow from the ADUser table.</param>
        /// <returns>THe newly populated DataRow</returns>
        public static DataRow SetUserDataRow(ADUserEntity user, DataRow r)
        {
            r["Email"] = user.Email;
            r["LastName"] = user.LastName;
            r["FirstName"] = user.FirstName;
            r["DisplayName"] = user.DisplayName;
            r["UserName"] = user.UserName;
            r["adspath"] = user.Adspath;
            r["cn"] = user.CN;
            r["distinguishedname"] = user.DistinguishedName;
            r["department"] = user.Department;
            r["directorylocation"] = user.DirectoryLocation;
            r["manager"] = user.Manager;
            r["mobile"] = user.Mobile;
            r["telephonenumber"] = user.TelephoneNumber;
            r["title"] = user.Title;
            r["physicaldeliveryofficename"] = user.PhysicalDeliveryOfficeName;
            r["company"] = user.Company;

            return r;
        }
    } //// end class
}
