﻿// --------------------------------------------------------------
// <copyright file="Utility.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace SalesRepForm.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Mail;
    using System.Web.UI;

    /// <summary>
    /// The utility class
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// These are the Web Config values.
        /// </summary>
        private DataTable tblWebConfig; ////DEC

        /// <summary>
        /// Initializes a new instance of the <see cref="Utility"/> class.
        /// </summary>
        /// <param name="req">The request</param>
        public Utility(System.Web.HttpRequest req)
        {
        } //// end constructor

        /// <summary>
        /// Gets or sets the web config.
        /// </summary>
        /// <value>The web config.</value>
        public DataTable TblWebConfig
        {
            get
            {
                return this.tblWebConfig;
            }

            set
            {
                this.tblWebConfig = value;
            }
        }

        /// <summary>
        /// Gets the email style string
        /// </summary>
        /// <value>The email style string.</value>
        public static string EmailStyle
        {
            get
            {
                string strBody = "<style type=\"text/css\">";
                strBody += "td{ text-align:left; font-family:Arial, Helvetica, sans-serif; font-size:8pt}";
                strBody += ".titleheader {background-color:ActiveBorder;font-size:12pt;font-weight:bold;}";
                strBody += "table{width:700px; text-align:center}";
                strBody += "</style>";

                return strBody;
            }
        }

        /// <summary>
        /// Gets email header string.
        /// </summary>
        /// <param name="strName">The requestor name string.</param>
        /// <param name="strEmail">The requestor email string.</param>
        /// <param name="strPhone">The requestor phone string.</param>
        /// <returns>the body string</returns>
        public static string GetEmailHeader(string strName, string strPhone, string strEmail)
        {
            string strBody = Utility.EmailStyle;
            strBody += "<table border=1>";
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Requestor Information</td>";
            strBody += "</tr><tr>";
            strBody += "<td style=\"width:15%\">Requestor Name:</td>";
            strBody += "<td style=\"width:35%\">" + strName + "</td>";
            strBody += "<td style=\"width:15%\">Requestor Phone:</td>";
            strBody += "<td>" + strPhone + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Requestor Email:</td>";
            strBody += "<td>" + strEmail + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td colspan=\"4\">&nbsp;</td>";
            strBody += "</tr>";

            return strBody;
        }



        /// <summary>
        /// Gets the Nonemployees by Last Name.
        /// </summary>
        /// <param name="strLastName">The lastname string.</param>
        /// <returns>IEnumerable NonEmployee1</returns>
        public static IEnumerable<NonEmployee1> GetNonEmployeesByLastname(string strLastName)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
            NonEmployee sr = new NonEmployee(connectionString);
            IEnumerable<NonEmployee1> vNED = (from ned in sr.NonEmployee1
                        where ned.LastName == strLastName
                        orderby ned.FirstName
                        select ned);

            return vNED;
        }

        /// <summary>
        /// Gets the config value.
        /// </summary>
        /// <param name="strKey">The key string.</param>
        /// <returns>config value string</returns>
        public static string GetConfigValue(string strKey)
        {
            string strValue = String.Empty;

            string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
            NonEmployee sr = new NonEmployee(connectionString);
            strValue = (from config in sr.WebConfig
                        where config.ConfigKey == strKey
                        select config.ConfigValue).Single();

            return strValue;
        }

        /// <summary>
        /// Gets the config value.
        /// </summary>
        /// <param name="configEnum">IEnumerable WebConfig</param>
        /// <param name="strKey">The key string.</param>
        /// <returns>config value string</returns>
        public static string GetConfigValue(IEnumerable<WebConfig> configEnum, string strKey)
        {
            string strValue = String.Empty;

            try
            {
                strValue = (from config in configEnum
                            where config.ConfigKey == strKey
                            select config.ConfigValue).Single();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetConfigValue Error for " + strKey + ": " + ex);
                throw;
            }

            return strValue;
        }

        /// <summary>
        /// Gets the config value.
        /// </summary>
        /// <returns>IEnumerable WebConfig</returns>
        public static IEnumerable<WebConfig> GetConfigENum()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
            NonEmployee sr = new NonEmployee(connectionString);
            IEnumerable<WebConfig> enumConfig = from config in sr.WebConfig
                        select config;

            return enumConfig;
        }

        /// <summary>
        /// Gets the Distribution List by Title.
        /// </summary>
        /// <param name="strTitle">The Title string.</param>
        /// <returns>IEnumerable SrFormDistribution</returns>
        public static IEnumerable<SrFormDistribution> GetDistributionListByTitle(string strTitle)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
            NonEmployee sr = new NonEmployee(connectionString);
            IEnumerable<SrFormDistribution> enumDistribution = from dist in sr.SrFormDistribution
                                                               where dist.PageTitle == strTitle
                                                               select dist;

            return enumDistribution;
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="strTo">The to address.</param>
        /// <param name="strSubj">The subjubject.</param>
        /// <param name="strBody">The body text.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        /// <param name="strTitle">The title of the page executing the SendEmail method.</param>
        /// <param name="attach">the System.Net.Mail.Attachment.</param>
        public static void SendEmail(string strTo, string strSubj, string strBody, bool isHtml, string strTitle, System.Net.Mail.Attachment attach)
        {
            IEnumerable<WebConfig> enumConfig = Utility.GetConfigENum();

            string strFromEmail = Utility.GetConfigValue(enumConfig, "emailNotifySRFormFrom");
            string strCcEmail = Utility.GetConfigValue(enumConfig, "ccEmailSRForm");
            string strSendNotification = Utility.GetConfigValue(enumConfig, "sendNotification");
            bool isProduction = Convert.ToBoolean(Utility.GetConfigValue(enumConfig, "isProduction"));

            MailAddress from = new MailAddress(strFromEmail);
            MailAddress to = new MailAddress(strTo);
            //// Specify the message content.
            
            MailMessage message = new MailMessage(from, to);

            if (attach != null)
            {
                message.Attachments.Add(attach);
            }

            // if production get distribution list and add to strTo
            IEnumerable<SrFormDistribution> enumDistribution = Utility.GetDistributionListByTitle(strTitle);
            foreach (SrFormDistribution lst in enumDistribution)
            {
                if (isProduction)
                {
                    message.To.Add(lst.EmailAddress);
                }
                else
                {
                    Debug.WriteLine("Distribution List: " + lst.EmailAddress);
                }
            }

            foreach (MailAddress m in message.To)
            {
                Debug.WriteLine("strTo: " + m.Address);
            }
            
            Debug.WriteLine("From: " + message.From.Address + "; ccMail: " + strCcEmail + "; sendNotification: " + strSendNotification);

            if (strCcEmail != String.Empty)
            {
                message.CC.Add(strCcEmail);
            }

            message.IsBodyHtml = isHtml;
            message.Subject = strSubj;
            message.Body = strBody;

            SmtpClient client = new SmtpClient();
            Debug.WriteLine("client: " + client.Host);

            if (Convert.ToBoolean(strSendNotification))
            {
                try
                {
                    ////client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                    ////client.SendAsync(message, "Sending..");
                    client.Send(message);
                }
                catch (Exception excm)
                {
                    Debug.WriteLine(excm.Message);

                    throw;
                }
            }

            message.Dispose();
        }

        /// <summary>
        /// Gets the post back control.
        /// </summary>
        /// <param name="page">The page control.</param>
        /// <returns>the control</returns>
        public static Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        control = c;

                        break;
                    }
                }
            }

            return control;
        }
    } //// end class
}
