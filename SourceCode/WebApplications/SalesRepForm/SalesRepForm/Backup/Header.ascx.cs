﻿// -------------------------------------------------------------
// <copyright file="Header.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using SalesRepForm.Classes;
    
    /// <summary>
    /// This is the Header UserControl.  
    /// The Header is added to each of the web pages.
    /// </summary>
    public partial class Header : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets the name of the requestor.
        /// </summary>
        /// <value>The name of the requestor.</value>
        public TextBox RequestorName
        {
            get
            {
                return this.txtRequestorName;
            }
 
            set
            {
                this.txtRequestorName = value;
            }
        }

        /// <summary>
        /// Gets or sets the requestor email.
        /// </summary>
        /// <value>The requestor email.</value>
        public TextBox RequestorEmail
        {
            get
            {
                return this.txtRequestorEmail;
            }

            set
            {
                this.txtRequestorEmail = value;
            }
        }

        /// <summary>
        /// Gets or sets the requestor phone.
        /// </summary>
        /// <value>The requestor phone.</value>
        public TextBox RequestorPhone
        {
            get
            {
                return this.txtRequestorPhone;
            }

            set
            {
                this.txtRequestorPhone = value;
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.tblRequestorInformation.Visible = Convert.ToBoolean(Page.Title != "Menu");

                this.btnAppName.Text = Page.Title;

                if (Page.Title != "Menu")
                {
                    string strUser = ADUtility.GetAppUsername(Request);
                    ADUserEntity myUser = ADUtility.GetADUserByUserName(strUser);

                    if (myUser != null)
                    {
                        this.txtRequestorName.Text = myUser.DisplayName;
                        this.txtRequestorEmail.Text = myUser.Email;
                        ////this.txtRequestorPhone.Text = myUser.TelephoneNumber;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnAppName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAppName_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    } // end class
}