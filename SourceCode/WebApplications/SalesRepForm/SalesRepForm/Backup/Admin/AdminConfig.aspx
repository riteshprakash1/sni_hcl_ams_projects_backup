﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="AdminConfig.aspx.cs" Inherits="SalesRepForm.Admin.AdminConfig" %>

<%@ Register src="AdminHeader.ascx" tagname="AdminHeader" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Configuration Admin</title>
    <script type="text/javascript">
        function confirm_delete()
        {
            if (confirm("Are you sure you want to delete this key?")==true)
		        return true;
	        else
		        return false;
        }    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:AdminHeader ID="AdminHeader1" runat="server" />
        <table width="700">
            <tr>
                <td>
                        <asp:GridView ID="gvAppSettings" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no App Settings." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update"
                             CellPadding="2" GridLines="Both" DataKeyNames="SettingKey">
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                           <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="UpdateCostCenter" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="AddCostCenter" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Key">
                                <ItemTemplate>
                                    <asp:Label ID="lblSettingKeyr" Font-Size="8pt" runat="server"  Text='<%# Eval("SettingKey")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAddSettingKey" MaxLength="30" Width="100px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddCostCenter" ValidationGroup="AddSetting" ControlToValidate="txtAddSettingKey" runat="server"  ErrorMessage="Key is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldSettingKeyExists" runat="server" ValidationGroup="AddSetting" ControlToValidate="txtAddSettingKey" ErrorMessage="The Key already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="58%" HeaderText="Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblSettingValue" Font-Size="8pt" runat="server"  Text='<%# Eval("SettingValue")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSettingValue" MaxLength="100" Width="250px" Font-Size="8pt" runat="server" Text='<%# Eval("SettingValue")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdateSettingValue" ValidationGroup="UpdateSetting" ControlToValidate="txtSettingValue" runat="server"  ErrorMessage="Value is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAddSettingValue" MaxLength="100" Width="250px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddSettingValue" ValidationGroup="AddSetting" ControlToValidate="txtAddSettingValue" runat="server"  ErrorMessage="Value is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddSetting" runat="server" />
                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateSetting" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
