﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminHeader.ascx.cs" Inherits="SalesRepForm.Admin.AdminHeader" %>
<table cellspacing="0" cellpadding="5" style="width:960px; text-align:center;" border="0">
    <tr>
        <td style="width:60%">
            <div  style="text-align:left">
                <asp:Image ID="Image1" ImageUrl="~/images/logo.gif" runat="server" Width="221" AlternateText="Smith &amp; Nephew" />
            </div>
        </td>
        <td style="width:40%">
            <div style="text-align:right">
                <asp:Button ID="btnAppName" BorderStyle="None" ForeColor="DarkGray" BackColor="White"
                    BorderColor="White" Text="Sales Rep Form" Font-Size="16pt" Font-Bold="True" runat="server"
                    CausesValidation="False"></asp:Button></div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
</table>