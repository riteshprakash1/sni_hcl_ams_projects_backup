﻿// ------------------------------------------------------------------
// <copyright file="RepPersonalInformation.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    using SalesRepForm.Classes;

    /// <summary>
    /// This is the code behind for the RepPersonalInformation page
    /// </summary>
    public partial class RepPersonalInformation : System.Web.UI.Page
    {
        private Header Header1;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Header1 = (Header)Page.Master.FindControl("Header1");

            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtAddress1, this.btnSubmit);
                EnterButton.TieButton(this.txtAddress2, this.btnSubmit);
                EnterButton.TieButton(this.txtCity, this.btnSubmit);
                EnterButton.TieButton(this.txtFirstName, this.btnSubmit);
                EnterButton.TieButton(this.txtLastName, this.btnSubmit);
                EnterButton.TieButton(this.txtMiddleName, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorEmail, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorName, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorPhone, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedAddress1, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedAddress2, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedCity, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedFirstName, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedLastName, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedMiddleName, this.btnSubmit);
                EnterButton.TieButton(this.txtRevisedZipCode, this.btnSubmit);
                EnterButton.TieButton(this.txtZipCode, this.btnSubmit);

                string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                NonEmployee sr = new NonEmployee(connectionString);

                IEnumerable<States> myStates = from state in sr.States
                                               orderby state.States1
                                               select state;
                this.ddlState.Items.Clear();
                this.ddlRevisedState.Items.Clear();
                this.ddlState.Items.Add(new ListItem("--Select--", String.Empty));
                this.ddlRevisedState.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (States st in myStates)
                {
                    this.ddlState.Items.Add(new ListItem(st.States1, st.Statescode));
                    this.ddlRevisedState.Items.Add(new ListItem(st.States1, st.Statescode));
                }

                this.btnClearGrid.Visible = false;
                this.lblMsg.Text = String.Empty;
            }

            this.gvFindUsers.Visible = false;

        } // end Page_Load

        /// <summary>
        /// Sets the email body.
        /// </summary>
        /// <returns>EmailBody string</returns>
        protected string SetEmailBody()
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" style=\"background-color:ActiveBorder;font-size:12pt;font-weight:bold;\">Change Personal Information</td>";
            strBody += "</tr><tr>";
            strBody += "<td colspan=\"6\" style=\"background-color:#ffffff;color:#78BDE8; font-size:14pt;font-weight: bold; height:35px; vertical-align:bottom\">";
            strBody += "Current</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Last Name:</td>";
            strBody += "<td>&nbsp;" + this.txtLastName.Text + "</td>";
            strBody += "<td>Given First Name:</td>";
            strBody += "<td>&nbsp;" + this.txtFirstName.Text + "</td>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>&nbsp;" + this.txtMiddleName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Address 1:</td>";
            strBody += "<td>&nbsp;" + this.txtAddress1.Text + "</td>";
            strBody += "<td>Address 1:</td>";
            strBody += "<td>&nbsp;" + this.txtAddress2.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>City:</td>";
            strBody += "<td>&nbsp;" + this.txtCity.Text + "</td>";
            strBody += "<td>State:</td>";
            strBody += "<td>&nbsp;" + this.ddlState.SelectedValue + "</td>";
            strBody += "<td>Zip Code:</td>";
            strBody += "<td>&nbsp;" + this.txtZipCode.Text + "</td>";
            strBody += "</tr><tr>";

            strBody += "<td>Personal E-Mail:</td>";
            strBody += "<td>" + this.txtRepEmail.Text + "</td>";
            strBody += "<td>Home Phone:</td>";
            strBody += "<td>" + this.txtHomePhone.Text + "</td>";
            strBody += "<td>Office Phone:</td>";
            strBody += "<td>" + this.txtOfficePhone.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Mobile Phone:</td>";
            strBody += "<td>" + this.txtMobile.Text + "</td>";
            strBody += "<td>Fax:</td>";
            strBody += "<td>" + this.txtFax.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td colspan=\"6\" style=\"background-color:#ffffff;color:#78BDE8; font-size:14pt;font-weight: bold; height:35px; vertical-align:bottom\">";
            strBody += "Revised</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Effective Date:</td>";
            strBody += "<td>&nbsp;" + this.txtEffectiveDate.Date + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Last Name:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedLastName.Text + "</td>";
            strBody += "<td>Given First Name:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedFirstName.Text + "</td>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedMiddleName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Address 1:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedAddress1.Text + "</td>";
            strBody += "<td>Address 1:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedAddress2.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>City:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedCity.Text + "</td>";
            strBody += "<td>State:</td>";
            strBody += "<td>&nbsp;" + this.ddlRevisedState.SelectedValue + "</td>";
            strBody += "<td>Zip Code:</td>";
            strBody += "<td>&nbsp;" + this.txtRevisedZipCode.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Personal E-Mail:</td>";
            strBody += "<td>" + this.txtRevisedEmail.Text + "</td>";
            strBody += "<td>Home Phone:</td>";
            strBody += "<td>" + this.txtRevisedHomePhone.Text + "</td>";
            strBody += "<td>Office Phone:</td>";
            strBody += "<td>" + this.txtRevisedOfficePhone.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Mobile Phone:</td>";
            strBody += "<td>" + this.txtRevisedMobile.Text + "</td>";
            strBody += "<td>Fax:</td>";
            strBody += "<td>" + this.txtRevisedFax.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Comments:</td>";
            strBody += "<td valign=\"top\" colspan=\"5\">" + this.txtComments.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr></table>";

            return strBody;
        }

        /// <summary>
        /// Handles the Click event of the btnMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("RepPersonalInformation.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitForm();
        } // end btnSubmit_Click

        /// <summary>
        /// Handles the Edit event of the gvFindUsers GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Select(object sender, GridViewEditEventArgs e)
        {
            this.hdnNonEmployeeID.Value = this.gvFindUsers.DataKeys[e.NewEditIndex]["NonEmployeeID"].ToString();
            if (this.gvFindUsers.DataKeys[e.NewEditIndex]["MidName"] != null)
            {
                this.txtMiddleName.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["MidName"].ToString();
            }

            if (this.gvFindUsers.DataKeys[e.NewEditIndex]["Street2"] != null)
            {
                this.txtAddress2.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["Street2"].ToString();
            }

            Label lblFirstName = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblFirstName");
            Label lblAddress1 = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblAddress1");
            Label lblCity = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblCity");
            Label lblState = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblState");
            Label lblZip = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblZip");

            txtFirstName.Text = lblFirstName.Text;
            this.txtAddress1.Text = lblAddress1.Text;
            this.txtCity.Text = lblCity.Text;
            this.txtZipCode.Text = lblZip.Text;

            if (this.ddlState.Items.FindByValue(lblState.Text) != null)
            {
                this.ddlState.ClearSelection();
                this.ddlState.Items.FindByValue(lblState.Text).Selected = true;
            }

            this.btnClearGrid_Click(sender, e);
        }

        /// <summary>
        /// Handles the TextChanged event of the txtLastName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void txtLastName_TextChanged(object sender, EventArgs e)
        {
            if (this.txtLastName.Text != String.Empty)
            {
                this.gvFindUsers.DataSource = Utility.GetNonEmployeesByLastname(this.txtLastName.Text);
                this.gvFindUsers.DataBind();
            }

            this.txtFirstName.Focus();
            this.gvFindUsers.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
            this.btnClearGrid.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
        }

        /// <summary>
        /// Handles the button click event of the btnClearGrid Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void btnClearGrid_Click(object sender, EventArgs e)
        {
            this.gvFindUsers.DataSource = new DataTable();
            this.gvFindUsers.DataBind();

            this.txtFirstName.Focus();
            this.gvFindUsers.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
            this.btnClearGrid.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
        }

        /// <summary>
        /// Submits the form.
        /// </summary>
        /// <returns></returns>
        private string SubmitForm()
        {
            string strRequestID = "0";
            Page.Validate();

            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    PersonalInfoChange p = new PersonalInfoChange();
                    DateTime datNow = DateTime.Now;

                    p.RequestorEmail = this.Header1.RequestorEmail.Text;
                    p.RequestorName = this.Header1.RequestorName.Text;
                    p.RequestorPhone = this.Header1.RequestorPhone.Text;

                    p.NonEmployeeID = Int32.Parse(this.hdnNonEmployeeID.Value);
                    p.FirstName = this.txtFirstName.Text;
                    p.LastName = this.txtLastName.Text;
                    p.MiddleName = this.txtMiddleName.Text;
                    p.Address1 = this.txtAddress1.Text;
                    p.Address2 = this.txtAddress2.Text;
                    p.City = this.txtCity.Text;
                    p.State = this.ddlState.SelectedValue;
                    p.Zip = this.txtZipCode.Text;

                    p.OfficePhone = this.txtOfficePhone.Text;
                    p.HomePhone = this.txtHomePhone.Text;
                    p.Fax = this.txtFax.Text;
                    p.Mobile = this.txtMobile.Text;
                    p.EmailAddress = this.txtRepEmail.Text;

                    p.OfficePhoneRevised = this.txtRevisedOfficePhone.Text;
                    p.HomePhoneRevised = this.txtRevisedHomePhone.Text;
                    p.FaxRevised = this.txtRevisedFax.Text;
                    p.MobileRevised = this.txtRevisedMobile.Text;
                    p.EmailAddressRevised = this.txtRevisedEmail.Text;

                    p.EffectiveDate = Convert.ToDateTime(txtEffectiveDate.Date);
                    p.FirstNameRevised = this.txtRevisedFirstName.Text;
                    p.LastNameRevised = this.txtRevisedLastName.Text;
                    p.MiddleNameRevised = this.txtRevisedMiddleName.Text;
                    p.Address1Revised = this.txtRevisedAddress1.Text;
                    p.Address2Revised = this.txtRevisedAddress2.Text;
                    p.CityRevised = this.txtRevisedCity.Text;
                    p.StateRevised = this.ddlRevisedState.SelectedValue;
                    p.ZipRevised = this.txtRevisedZipCode.Text;
                    p.Comments = this.txtComments.Text;

                    p.CreateDate = datNow;
                    p.CreatedBy = ADUtility.GetAppUsername(Request);
                    p.UpdateDate = datNow;
                    p.UpdatedBy = ADUtility.GetAppUsername(Request);

                    sr.PersonalInfoChange.InsertOnSubmit(p);
                    sr.SubmitChanges();

                    Debug.WriteLine("New ID: " + p.ChangeID.ToString());

                    ////string strFrom = myUtility.GetConfigValue("emailNotifySRFormFrom");
                    string strSubject = "Personal Information Change Form Submitted by " + p.RequestorName;
                    string strBody = this.SetEmailBody();

                    ////this.ltlEmail.Text = strBody;

                    Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, Page.Title, null);

                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } // end Page.IsValid

            return strRequestID;
        }
    } //// end class
}
