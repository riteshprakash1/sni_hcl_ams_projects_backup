﻿// ------------------------------------------------------------------
// <copyright file="NonRep.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    using SalesRepForm.Classes;

    /// <summary>
    /// This is the code behind for the NonRep page
    /// </summary>
    public partial class NonRep : System.Web.UI.Page
    {
        private Header Header1;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Header1 = (Header)Page.Master.FindControl("Header1");

            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtDistrictName, this.btnSubmit);
                EnterButton.TieButton(this.txtEmail, this.btnSubmit);
                EnterButton.TieButton(this.txtSupervisorName, this.btnSubmit);
                EnterButton.TieButton(this.txtSupervisorTitle, this.btnSubmit);
                EnterButton.TieButton(this.txtFirstName, this.btnSubmit);
                EnterButton.TieButton(this.txtLastName, this.btnSubmit);
                EnterButton.TieButton(this.txtMiddleName, this.btnSubmit);
                EnterButton.TieButton(this.txtPreferredName, this.btnSubmit);
                EnterButton.TieButton(this.txtTitle, this.btnSubmit);
                EnterButton.TieButton(this.txtCostCenter, this.btnSubmit);
                EnterButton.TieButton(this.txtForwardEmail, this.btnSubmit);

                EnterButton.TieButton(this.Header1.RequestorEmail, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorName, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorPhone, this.btnSubmit);

                this.gvFindUsers.Visible = false;
                this.btnClearGrid.Visible = false;
            } // end IsPostBack

            this.lblMsg.Text = String.Empty;
        } // end Page_Load



        protected string SetEmailBody()
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Non-Rep Information</td>";
            strBody += "</tr>";
            strBody += "</table>";
            strBody += "<table border=1>";
            strBody += "<tr>";
            strBody += "<td style=\"width:20%\">Last Name:</td>";
            strBody += "<td style=\"width:25%\">" + this.txtLastName.Text + "</td>";
            strBody += "<td style=\"width:25%\">Given First Name:</td>";
            strBody += "<td>" + this.txtFirstName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>" + this.txtMiddleName.Text + "</td>";
            strBody += "<td>Preferred First Name:</td>";
            strBody += "<td>" + this.txtPreferredName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Title:</td>";
            strBody += "<td>" + this.txtTitle.Text + "</td>";
            strBody += "<td>Cost Center:</td>";
            strBody += "<td>" + this.txtCostCenter.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Change Type:</td>";
            strBody += "<td>" + this.ddlChangeType.SelectedValue + "</td>";
            strBody += "<td>Effective Date:</td>";
            strBody += "<td>" + this.txtEffectiveDate.Date + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Supervisor Name:</td>";
            strBody += "<td>" + this.txtSupervisorName.Text + "</td>";
            strBody += "<td>Supervisor Title:</td>";
            strBody += "<td>" + this.txtSupervisorTitle.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Territory/District Name:</td>";
            strBody += "<td>" + this.txtDistrictName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>HCP Facing:</td>";
            strBody += "<td>" + this.ddlHcpFacing.SelectedValue + "</td>";
            strBody += "<td>Email:</td>";
            strBody += "<td>" + this.txtEmail.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Technology Request</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Voice Mail:</td>";
            strBody += "<td>" + this.ddlVoiceMail.SelectedValue + "</td>";
            strBody += "<td>Sales Life On-line:</td>";
            strBody += "<td>" + this.ddlOrthoOnline.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>SNUG (VPN) Access:</td>";
            strBody += "<td>" + this.ddlSnugAccess.SelectedValue + "</td>";
            strBody += "<td>Ortho On-line Service:</td>";
            strBody += "<td>" + this.ddlOrthoOnlineService.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Email:</td>";
            strBody += "<td>" + this.ddlEmailService.SelectedValue + "</td>";
            strBody += "<td>Sales Reporting Service:</td>";
            strBody += "<td>" + this.ddlSalesReporting.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>CT Rep:</td>";
            strBody += "<td>" + this.ddlCTRep.SelectedValue + "</td>";
            strBody += "<td>Learn Center Access:</td>";
            strBody += "<td>" + this.ddlLearnCenter.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>SAP:</td>";
            strBody += "<td>" + this.ddlSAP.SelectedValue + "</td>";
            strBody += "<td>Data Warehouse:</td>";
            strBody += "<td>" + this.ddlDataWarehouse.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Kronos:</td>";
            strBody += "<td>" + this.ddlKronos.SelectedValue + "</td>";
            strBody += "<td>&nbsp;</td><td>&nbsp;</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Forward S&amp;N mail to this email:</td>";
            strBody += "<td colspan=\"3\">" + this.txtForwardEmail.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Comments:</td>";
            strBody += "<td valign=\"top\" colspan=\"3\">" + this.txtComments.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr>";
            strBody += "</table>";

            return strBody;
        }

        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("NonRep.aspx", true);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitForm();
        }

        protected void txtLastName_TextChanged(object sender, EventArgs e)
        {
            if (this.txtLastName.Text != String.Empty)
            {
                this.gvFindUsers.DataSource = Utility.GetNonEmployeesByLastname(this.txtLastName.Text);
                this.gvFindUsers.DataBind();
            }

            this.txtFirstName.Focus();
            this.gvFindUsers.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
            this.btnClearGrid.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
        }

        /// <summary>
        /// Handles the button click event of the btnClearGrid Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void btnClearGrid_Click(object sender, EventArgs e)
        {
            this.gvFindUsers.DataSource = new DataTable();
            this.gvFindUsers.DataBind();

            this.txtFirstName.Focus();
            this.gvFindUsers.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
            this.btnClearGrid.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
        }

        /// <summary>
        /// Handles the Edit event of the gvFindUsers GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Select(object sender, GridViewEditEventArgs e)
        {
            if (this.ddlChangeType.SelectedValue.ToLower() != "new")
            {
                this.hdnNonEmployeeID.Value = this.gvFindUsers.DataKeys[e.NewEditIndex]["NonEmployeeID"].ToString();
                if (this.gvFindUsers.DataKeys[e.NewEditIndex]["MidName"] != null)
                {
                    this.txtMiddleName.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["MidName"].ToString();
                }

                if (this.gvFindUsers.DataKeys[e.NewEditIndex]["EmailAddress"] != null)
                {
                    this.txtEmail.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["EmailAddress"].ToString();
                }

                ////if (this.gvFindUsers.DataKeys[e.NewEditIndex]["SalesDistrict"] != null)
                ////  this.txtDistrictName.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["SalesDistrict"].ToString();

                Label lblFirstName = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblFirstName");

                txtFirstName.Text = lblFirstName.Text;
            }

            this.btnClearGrid_Click(sender, e);
        }

        private string SubmitForm()
        {
            string strRequestID = "0";
            Page.Validate();

            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    ChangeRequest cr = new ChangeRequest();
                    DateTime datNow = DateTime.Now;

                    cr.NonEmployeeID = Int32.Parse(this.hdnNonEmployeeID.Value);
                    cr.RequestorEmail = this.Header1.RequestorEmail.Text;
                    cr.RequestorName = this.Header1.RequestorName.Text;
                    cr.RequestorPhone = this.Header1.RequestorPhone.Text;

                    cr.ChangeType = this.ddlChangeType.SelectedValue + " Non-Rep";
                    cr.DataWarehouse = this.ddlDataWarehouse.SelectedValue;
                    cr.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Date);
                    cr.FirstName = txtFirstName.Text;
                    cr.Kronos = this.ddlKronos.SelectedValue;
                    cr.LastName = txtLastName.Text;
                    cr.MiddleName = txtMiddleName.Text;
                    cr.Title = this.txtTitle.Text;
                    cr.CostCenter = this.txtCostCenter.Text;
                    cr.PreferredName = this.txtPreferredName.Text;
                    cr.ManagerName = this.txtSupervisorName.Text;
                    cr.ManagerTitle = this.txtSupervisorTitle.Text;
                    cr.DistrictName = this.txtDistrictName.Text;
                    cr.HcpFacing = this.ddlHcpFacing.SelectedValue;
                    cr.EmailAddress = this.txtEmail.Text;

                    cr.VoiceMail = ddlVoiceMail.SelectedValue;
                    cr.OrthoOnline = this.ddlOrthoOnline.SelectedValue;
                    cr.SNUG = this.ddlSnugAccess.SelectedValue;
                    cr.OrthoOnlineService = this.ddlOrthoOnlineService.SelectedValue;
                    cr.EmailService = this.ddlEmailService.SelectedValue;
                    cr.SalesReporting = this.ddlSalesReporting.SelectedValue;
                    cr.SAP = this.ddlSAP.SelectedValue;
                    cr.CTRep = this.ddlCTRep.SelectedValue;
                    cr.LearnCenter = this.ddlLearnCenter.SelectedValue;
                    cr.ForwardEmail = this.txtForwardEmail.Text;
                    cr.Comments = this.txtComments.Text;

                    cr.UpdateDate = datNow;
                    cr.UpdatedBy = ADUtility.GetAppUsername(Request);
                    cr.CreateDate = datNow;
                    cr.CreatedBy = ADUtility.GetAppUsername(Request);

                    sr.ChangeRequest.InsertOnSubmit(cr);
                    sr.SubmitChanges();

                    Debug.WriteLine("New NonRepID: " + cr.RequestID.ToString());

                    string strSubject = this.ddlChangeType.SelectedValue + " Non-Rep Form Submitted by " + cr.RequestorName;
                    string strBody = this.SetEmailBody();

                    ////this.ltlEmail.Text = strBody;

                    Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, Page.Title, null);

                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } // end Page.IsValid

            return strRequestID;

        }
    } // end class
}
