﻿// ------------------------------------------------------------------
// <copyright file="AccountChange.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    using SalesRepForm.Classes;

    /// <summary>
    /// This is the AccountChange page.
    /// </summary>
    public partial class AccountChange : System.Web.UI.Page
    {
        /// <summary>
        /// This is an instance of the Header class
        /// </summary>
        private Header Header1;

        /// <summary>
        /// Gets or sets the postedFile session.
        /// </summary>
        /// <value>The name of the user.</value>
        public HttpPostedFile PostedFile
        {
            get
            {
                if (Page.Session["postedFile"] != null)
                {
                    return (HttpPostedFile)Page.Session["postedFile"];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                Page.Session["postedFile"] = value;
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Header1 = (Header)Page.Master.FindControl("Header1");

            if (!Page.IsPostBack)
            {
                this.btnSubmit.Attributes.Add("onclick", String.Format("document.getElementById('{0}').value= document.getElementById('{1}').value;", this.HiddenField1.ClientID, this.fileUpload.ClientID));
                
                EnterButton.TieButton(this.txtFromRep, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorEmail, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorName, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorPhone, this.btnSubmit);
                EnterButton.TieButton(this.txtToRep, this.btnSubmit);
                
                this.BindDT(this.LoadStatesGV());
                this.BindDTReps(this.LoadRepsGV());

                this.SetControls();
            }

            if (this.fileUpload.PostedFile != null && this.fileUpload.PostedFile.ContentLength > 0)
            {
                this.PostedFile = this.fileUpload.PostedFile;
            }

            ////Debug.WriteLine("Page_Load Onclick Attribute: " + this.btnSubmit.Attributes["onclick"]);
            this.lblMsg.Text = String.Empty;
        } // end Page_Load

        /// <summary>
        /// Handles the Click event of the btnMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("btnMenu_Click");
            Response.Redirect("Default.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("btnClear_Click");
            Response.Redirect("AccountChange.aspx", true);
        }

        /// <summary>
        /// Handles the Add event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_RepsAdd(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddRep");

            if (Page.IsValid)
            {
                this.AddRep();
            } //// Page.IsValid
        } ////end GV_Add

        /// <summary>
        /// Binds the users table to the gvReps GridView.
        /// </summary>
        /// <param name="myReps">AccountChangeReps collection</param>
        protected void BindDTReps(Collection<AccountChangeReps> myReps)
        {
            if (myReps.Count == 0)
            {
                AccountChangeReps rep = new AccountChangeReps();
                rep.FromRep = String.Empty;
                rep.ToRep = String.Empty;
                rep.AccountName = String.Empty;
                rep.AccountNumber = String.Empty;
                myReps.Add(rep);
            }

            this.gvReps.DataSource = myReps;
            this.gvReps.DataBind();
        }

        /// <summary>
        /// Binds the DT.
        /// </summary>
        /// <param name="myStates">AccountChangeStates Collection</param>
        protected void BindDT(Collection<AccountChangeStates> myStates)
        {
            if (myStates.Count == 0)
            {
                AccountChangeStates st = new AccountChangeStates();
                st.State = String.Empty;
                st.Country = String.Empty;
                myStates.Add(st);
            }

            this.gvState.DataSource = myStates;
            this.gvState.DataBind();
        }

        /// <summary>
        /// Handles the RowDataBound event of the gvReps GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RepsRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblMultiFromRep = (Label)e.Row.FindControl("lblMultiFromRep");
                Label lblMultiToRep = (Label)e.Row.FindControl("lblMultiToRep");
                Label lblMultiAccountNumber = (Label)e.Row.FindControl("lblMultiAccountNumber");
                Label lblMultiAccountName = (Label)e.Row.FindControl("lblMultiAccountName");

                //// if the Reps are blank hide the row
                e.Row.Visible = Convert.ToBoolean((lblMultiFromRep.Text != String.Empty) || (lblMultiToRep.Text != String.Empty)
                    || (lblMultiAccountNumber.Text != String.Empty) || (lblMultiAccountName.Text != String.Empty));
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In GV_RepsRowDataBound Footer");

                TextBox txtMultiToRep = (TextBox)e.Row.FindControl("txtMultiToRep");
                TextBox txtMultiFromRep = (TextBox)e.Row.FindControl("txtMultiFromRep");
                TextBox txtMultiAccountName = (TextBox)e.Row.FindControl("txtMultiAccountName");
                TextBox txtMultiAccountNumber = (TextBox)e.Row.FindControl("txtMultiAccountNumber");
                Button btnRepAdd = (Button)e.Row.FindControl("btnRepAdd");

                EnterButton.TieButton(txtMultiFromRep, btnRepAdd);
                EnterButton.TieButton(txtMultiToRep, btnRepAdd);
                EnterButton.TieButton(txtMultiAccountName, btnRepAdd);
                EnterButton.TieButton(txtMultiAccountNumber, btnRepAdd);
            }
        } // end GV_RepsRowDataBound

        /// <summary>
        /// Handles the RowDataBound event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCountry = (Label)e.Row.FindControl("lblCountry");
                Label lblState = (Label)e.Row.FindControl("lblState");

                //// if the CostCenter if blank hide the row
                e.Row.Visible = Convert.ToBoolean((lblCountry.Text != String.Empty) || (lblState.Text != String.Empty));
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In GV_RowDataBound Footer");

                string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                NonEmployee sr = new NonEmployee(connectionString);
                IEnumerable<States> myStates = from state in sr.States
                                               orderby state.States1
                                               select state;

                DropDownList ddlState = (DropDownList)e.Row.FindControl("ddlState");
                ddlState.Items.Clear();
                ddlState.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (States st in myStates)
                {
                    ddlState.Items.Add(new ListItem(st.States1, st.States1));
                }

                IEnumerable<Countries> myCountries = from country in sr.Countries
                                               orderby country.Countries1
                                                select country;

                DropDownList ddlCountry = (DropDownList)e.Row.FindControl("ddlCountry");
                ddlCountry.Items.Clear();
                ddlCountry.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (Countries c in myCountries)
                {
                    ddlCountry.Items.Add(new ListItem(c.Countries1, c.Countries1));
                }
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvRep GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_RepDelete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            Collection<AccountChangeReps> myReps = this.LoadRepsGV();
            myReps.RemoveAt(e.RowIndex);
            this.BindDTReps(myReps);
        }

        /// <summary>
        /// Handles the Delete event of the gvState GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            Collection<AccountChangeStates> myStates = this.LoadStatesGV();
            myStates.RemoveAt(e.RowIndex);
            this.BindDT(myStates);
        }

        /// <summary>
        /// Handles the Add event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            if (Page.IsValid)
            {
                this.AddState();
            } //// Page.IsValid
        } ////end GV_Add

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            Debug.WriteLine("File: " + fileUpload.FileName);

            if (this.ddlChangeType.SelectedIndex == 1)
            {
                this.SetRepRequired();
            }

            if (this.ddlChangeType.SelectedIndex == 2)
            {
                Debug.WriteLine("gvState.Rows: " + gvState.Rows.Count.ToString());

                this.vldStateCountry.IsValid = this.StateCountryIsValid();

                if (fileUpload.FileName != String.Empty)
                {
                    this.vldFileUpload.IsValid = this.fileUpload.HasFile;
                }

                if (this.vldFileUpload.IsValid)
                {
                    // if file size is < maxSize in Web.Config
                    IEnumerable<WebConfig> enumConfig = Utility.GetConfigENum();
                    int maxSize = Int32.Parse(Utility.GetConfigValue(enumConfig, "attachSize"));
                        int fSize = this.fileUpload.PostedFile.ContentLength;
                        double dblDiv = Convert.ToDouble(fSize / maxSize);
                        Debug.WriteLine("dblDiv: " + dblDiv + "; fSize: " + fSize.ToString() + "; maxSize: " + maxSize.ToString() + "Small: " + Convert.ToBoolean(dblDiv < 1.0).ToString());
                        this.vldAttachSize.IsValid = Convert.ToBoolean(dblDiv < 1.0);
                }
            }

            Debug.WriteLine("Page.IsValid: " + Page.IsValid.ToString());
            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    SalesRepForm.Classes.AccountChange ac = new SalesRepForm.Classes.AccountChange();
                    DateTime datNow = DateTime.Now;

                    ac.RequestorEmail = this.Header1.RequestorEmail.Text;
                    ac.RequestorName = this.Header1.RequestorName.Text;
                    ac.RequestorPhone = this.Header1.RequestorPhone.Text;

                    ac.AccountChangeType = this.ddlChangeType.SelectedValue;
                    ac.Other = this.txtOtherBusinessLine.Text;

                    ac.TransferFrom = this.txtFromRep.Text;
                    ac.TransferTo = this.txtToRep.Text;

                    string strBusinessLine = String.Empty;
                    foreach (ListItem i in this.chbBusinessLines.Items)
                    {
                        if (i.Selected)
                        {
                            strBusinessLine += i.Value + "; ";
                        }
                    }

                    ac.BusinessLines = strBusinessLine.Trim().TrimEnd(';');

                    ac.EffectiveDate = Convert.ToDateTime(txtEffectiveDate.Date);
                    ac.AttachFile = fileUpload.FileName;

                    ac.CreateDate = datNow;
                    ac.CreatedBy = ADUtility.GetAppUsername(Request);
                    ac.UpdateDate = datNow;
                    ac.UpdatedBy = ADUtility.GetAppUsername(Request);

                    sr.AccountChange.InsertOnSubmit(ac);
                    sr.SubmitChanges();

                    Debug.WriteLine("New ID: " + ac.AccountChangeID.ToString());
                    int intAccountChangeID = ac.AccountChangeID;

                    if (gvState.Visible)
                    {
                        DropDownList ddlCountry = (DropDownList)this.gvState.FooterRow.FindControl("ddlCountry");
                        DropDownList ddlState = (DropDownList)this.gvState.FooterRow.FindControl("ddlState");
                        if (ddlCountry.SelectedIndex != 0 || ddlState.SelectedIndex != 0)
                        {
                            AccountChangeStates st = new AccountChangeStates();
                            st.AccountChangeID = intAccountChangeID;
                            st.State = ddlState.SelectedValue;
                            st.Country = ddlCountry.SelectedValue;

                            sr.AccountChangeStates.InsertOnSubmit(st);
                            sr.SubmitChanges();
                        }
                        
                        foreach (GridViewRow r in this.gvState.Rows)
                        {
                            AccountChangeStates st = new AccountChangeStates();
                            Label lblCountry = (Label)r.FindControl("lblCountry");
                            Label lblState = (Label)r.FindControl("lblState");
                            if (lblState.Text != String.Empty || lblCountry.Text != String.Empty)
                            {
                                st.AccountChangeID = intAccountChangeID;
                                st.State = lblState.Text;
                                st.Country = lblCountry.Text;

                                sr.AccountChangeStates.InsertOnSubmit(st);
                                sr.SubmitChanges();
                            }
                        }
                    }

                    if (this.gvReps.Visible)
                    {
                        TextBox txtMultiFromRep = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiFromRep");
                        TextBox txtMultiToRep = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiToRep");
                        TextBox txtMultiAccountNumber = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiAccountNumber");
                        TextBox txtMultiAccountName = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiAccountName");
                        if (txtMultiFromRep.Text != String.Empty && txtMultiToRep.Text != String.Empty && 
                            txtMultiAccountName.Text != String.Empty && txtMultiAccountNumber.Text != String.Empty)
                        {
                            AccountChangeReps rep = new AccountChangeReps();
                            rep.AccountChangeID = intAccountChangeID;
                            rep.FromRep = txtMultiFromRep.Text;
                            rep.ToRep = txtMultiToRep.Text;
                            rep.AccountName = txtMultiAccountName.Text;
                            rep.AccountNumber = txtMultiAccountNumber.Text;

                            sr.AccountChangeReps.InsertOnSubmit(rep);
                            sr.SubmitChanges();
                        }

                        foreach (GridViewRow r in this.gvReps.Rows)
                        {
                            AccountChangeReps rep = new AccountChangeReps();
                            Label lblMultiFromRep = (Label)r.FindControl("lblMultiFromRep");
                            Label lblMultiToRep = (Label)r.FindControl("lblMultiToRep");
                            Label lblMultiAccountName = (Label)r.FindControl("lblMultiAccountName");
                            Label lblMultiAccountNumber = (Label)r.FindControl("lblMultiAccountNumber");
                            if (lblMultiFromRep.Text != String.Empty || lblMultiToRep.Text != String.Empty)
                            {
                                rep.AccountChangeID = intAccountChangeID;
                                rep.FromRep = lblMultiFromRep.Text;
                                rep.ToRep = lblMultiToRep.Text;
                                rep.AccountName = lblMultiAccountName.Text;
                                rep.AccountNumber = lblMultiAccountNumber.Text;
                                sr.AccountChangeReps.InsertOnSubmit(rep);
                                sr.SubmitChanges();
                            }
                        }
                    }

                    ////string strFrom = myUtility.GetConfigValue("emailNotifySRFormFrom");
                    string strSubject = "Account Change Submitted by " + ac.RequestorName;
                    string strBody = this.SetEmailBody(ac.BusinessLines, intAccountChangeID);

                    ////this.ltlEmail.Text = strBody;

                    if (ac.AttachFile == String.Empty)
                    {
                        Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, Page.Title, null);
                    }
                    else
                    {
                        HttpPostedFile myFile = fileUpload.PostedFile;

                        if (myFile != null)
                        {
                            /* Get size of the file */
                            int attachFileLength = myFile.ContentLength;
                            /* Make sure the size of the file is > 0 */
                            if (attachFileLength > 0)
                            {
                                /* Get the file name */
                                string strFileName = Path.GetFileName(myFile.FileName);

                                // create fully qualified filename
                                strFileName = Server.MapPath("attachments\\" + strFileName);
                                Debug.WriteLine("strFileName: " + strFileName);

                                if (File.Exists(strFileName))
                                {
                                    File.Delete(strFileName);
                                }

                                /* Save the file on the server */
                                myFile.SaveAs(strFileName);

                                /* Create the email attachment with the uploaded file */
                                System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(strFileName);

                                Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, Page.Title, attach);

                                if (File.Exists(strFileName))
                                {
                                    File.Delete(strFileName);
                                }
                            }
                        }
                    }

                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } 
            else
            {
                // add file name is not blank add validation error to reset name
                if (this.HiddenField1.Value != String.Empty)
                {
                    this.vldResetFileUpload.IsValid = false;
                    this.vldResetFileUpload.ErrorMessage += this.HiddenField1.Value;
                }
            }
        }

        /// <summary>
        /// Sets the email body.
        /// </summary>
        /// <param name="strBusiness">The STR business.</param>
        /// <param name="acctChangeID">The acct change ID.</param>
        /// <returns>string strBody</returns>
        protected string SetEmailBody(string strBusiness, int acctChangeID)
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Change Information</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Change Type:</td>";
            strBody += "<td>&nbsp;" + this.ddlChangeType.SelectedValue + "</td>";
            strBody += "<td>Effective Date:</td>";
            strBody += "<td>&nbsp;" + this.txtEffectiveDate.Date + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Business Lines<br>Affected:</td>";
            strBody += "<td colspan=\"3\">&nbsp;" + strBusiness + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Other:</td>";
            strBody += "<td colspan=\"3\">" + this.txtOtherBusinessLine.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr><tr>";

            string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
            NonEmployee sr = new NonEmployee(connectionString);

            // if individual change
            if (this.ddlChangeType.SelectedIndex == 1)
            {
                strBody += "<tr>";
                strBody += "<td>&nbsp;</td>";
                strBody += "<td valign=\"top\" colspan=\"3\">";
                strBody += "<table width=\"100%\" border=1>";
                strBody += "<tr>";
                strBody += "<td><b>Account Number</b></td>";
                strBody += "<td><b>Account Name</b></td>";
                strBody += "<td><b>From Rep</b></td>";
                strBody += "<td><b>To Rep</b></td>";
                strBody += "</tr>";

                IEnumerable<AccountChangeReps> myReps = from rep in sr.AccountChangeReps
                                                        where rep.AccountChangeID == acctChangeID
                                                        select rep;

                foreach (AccountChangeReps rep in myReps)
                {
                    if (rep.FromRep != String.Empty && rep.ToRep != String.Empty)
                    {
                        strBody += "<tr>";
                        strBody += "<td>" + rep.AccountNumber + "</td>";
                        strBody += "<td>" + rep.AccountName + "</td>";
                        strBody += "<td>" + rep.FromRep + "</td>";
                        strBody += "<td>" + rep.ToRep + "</td>";
                        strBody += "</tr>";
                    }
                }

                strBody += "</table>";
                strBody += "</td>";
                strBody += "</tr>";
            }

            // mass change
            if (this.ddlChangeType.SelectedIndex == 2)
            {
                strBody += "<td>From Rep:</td>";
                strBody += "<td>&nbsp;" + this.txtFromRep.Text + "</td>";
                strBody += "<td>To Rep:</td>";
                strBody += "<td>&nbsp;" + this.txtToRep.Text + "</td>";
                strBody += "</tr>";

                IEnumerable<AccountChangeStates> myStates = from state in sr.AccountChangeStates
                                                        where state.AccountChangeID == acctChangeID
                                                            select state;

                strBody += "<tr>";
                strBody += "<td>&nbsp;</td>";
                strBody += "<td valign=\"top\" colspan=\"3\">";
                strBody += "<table width=\"100%\" border=1>";
                strBody += "<tr>";
                strBody += "<td><b>State</b></td>";
                strBody += "<td><b>Country</b></td>";
                strBody += "</tr>";
                foreach (AccountChangeStates st in myStates)
                {
                    if (st.State != String.Empty || st.Country != String.Empty)
                    {
                        strBody += "<tr>";
                        strBody += "<td>" + st.State + "</td>";
                        strBody += "<td>" + st.Country + "</td>";
                        strBody += "</tr>";
                    }
                }

                strBody += "</table>";
                strBody += "</td>";
                strBody += "</tr>";
            }

            strBody += "</table>";

            return strBody;
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlChangeType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlChangeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetControls();
        }

        /// <summary>
        /// Sets the controls on the page.
        /// </summary>
        private void SetControls()
        {
            Debug.WriteLine("SelectedIndex: " + this.ddlChangeType.SelectedIndex.ToString());
            this.tblDetails.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex != 0);
            this.lblEffectiveDate.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex != 0);
            this.txtEffectiveDate.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex != 0);

            if (this.ddlChangeType.SelectedIndex != 0)
            {
                this.gvReps.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
                this.lblIndividualReps.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
                this.lblAddRepMsg.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);

                this.gvState.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldStateCountry.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.fileUpload.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldFileUpload.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lblAttachFile.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lblAddMsg.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lblAttachMsg.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldAttachSize.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);

                this.lblFrom.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.txtFromRep.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldFromRep.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldFromRepFormat.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lblTo.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.txtToRep.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldToRep.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.vldToRepFormat.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lbl6DigitsFrom.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lbl6DigitsTo.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
                this.lblStateCountry.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            }
        }

        /// <summary>
        /// Sets the rep required.
        /// </summary>
        private void SetRepRequired()
        {
            bool hasDataRow = false;
            int rowNum = 0;
            string rowType = String.Empty;

            foreach (GridViewRow r in gvReps.Rows)
            {
                rowNum = r.RowIndex;
                rowType = r.RowType.ToString();
                string strAccountNumber = this.gvReps.DataKeys[r.RowIndex]["AccountNumber"].ToString();
                if (strAccountNumber != String.Empty)
                {
                    hasDataRow = true;
                    break;
                }
            }

            TextBox txtMultiToRep = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiToRep");
            TextBox txtMultiFromRep = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiFromRep");
            TextBox txtMultiAccountNumber = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiAccountNumber");
            TextBox txtMultiAccountName = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiAccountName");

            bool areEmpty = Convert.ToBoolean(txtMultiAccountName.Text == String.Empty &&
                txtMultiAccountNumber.Text == String.Empty && txtMultiFromRep.Text == String.Empty && txtMultiToRep.Text == String.Empty);

            // if all the fields are empty and there is a gridview row with data
            if (areEmpty && hasDataRow)
            {
                RequiredFieldValidator vldFromRep = (RequiredFieldValidator)this.gvReps.FooterRow.FindControl("vldFromRep");
                RequiredFieldValidator vldToRep = (RequiredFieldValidator)this.gvReps.FooterRow.FindControl("vldToRep");
                RequiredFieldValidator vldAccountNumber = (RequiredFieldValidator)this.gvReps.FooterRow.FindControl("vldAccountNumber");
                RequiredFieldValidator vldAccountName = (RequiredFieldValidator)this.gvReps.FooterRow.FindControl("vldAccountName");

                vldFromRep.IsValid = true;
                vldToRep.IsValid = true;
                vldAccountNumber.IsValid = true;
                vldAccountName.IsValid = true;
            }
        }

         /// <summary>
        /// Adds the state.
        /// </summary>
        private void AddState()
        {
            DropDownList ddlCountry = (DropDownList)this.gvState.FooterRow.FindControl("ddlCountry");
            DropDownList ddlState = (DropDownList)this.gvState.FooterRow.FindControl("ddlState");

            Collection<AccountChangeStates> myStates = this.LoadStatesGV();
            AccountChangeStates st = new AccountChangeStates();
            st.State = ddlState.SelectedValue;
            st.Country = ddlCountry.SelectedValue;
            myStates.Add(st);

            this.BindDT(myStates);
        }

        /// <summary>
        /// Adds the rep.
        /// </summary>
        private void AddRep()
        {
            TextBox txtMultiToRep = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiToRep");
            TextBox txtMultiFromRep = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiFromRep");
            TextBox txtMultiAccountName = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiAccountName");
            TextBox txtMultiAccountNumber = (TextBox)this.gvReps.FooterRow.FindControl("txtMultiAccountNumber");

            Collection<AccountChangeReps> myReps = this.LoadRepsGV();
            AccountChangeReps rep = new AccountChangeReps();

            rep.FromRep = txtMultiFromRep.Text;
            rep.ToRep = txtMultiToRep.Text;
            rep.AccountName = txtMultiAccountName.Text;
            rep.AccountNumber = txtMultiAccountNumber.Text;
            myReps.Add(rep);

            this.BindDTReps(myReps);
        }

        /// <summary>
        /// States the country is valid.
        /// </summary>
        /// <returns>true/ false</returns>
        private bool StateCountryIsValid()
        {
            DropDownList ddlCountry = (DropDownList)this.gvState.FooterRow.FindControl("ddlCountry");
            DropDownList ddlState = (DropDownList)this.gvState.FooterRow.FindControl("ddlState");

            bool isValid = Convert.ToBoolean(ddlCountry.SelectedIndex != 0 || ddlState.SelectedIndex != 0);

            if (isValid)
            {
                return isValid;
            }

            foreach (GridViewRow r in this.gvState.Rows)
            {
                if (r.Visible)
                {
                    return true;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Loads the states GV.
        /// </summary>
        /// <returns>Collection AccountChangeStates</returns>
        private Collection<AccountChangeStates> LoadStatesGV()
        {
            Collection<AccountChangeStates> collStates = new Collection<AccountChangeStates>();
            foreach (GridViewRow r in this.gvState.Rows)
            {
                AccountChangeStates st = new AccountChangeStates();
                Label lblCountry = (Label)r.FindControl("lblCountry");
                Label lblState = (Label)r.FindControl("lblState");

                st.State = lblState.Text;
                st.Country = lblCountry.Text;
                collStates.Add(st);
            }

            return collStates;
        }

        /// <summary>
        /// Loads the reps GV.
        /// </summary>
        /// <returns>Collection AccountChangeReps</returns>
        private Collection<AccountChangeReps> LoadRepsGV()
        {
            Collection<AccountChangeReps> collReps = new Collection<AccountChangeReps>();
            foreach (GridViewRow r in this.gvReps.Rows)
            {
                AccountChangeReps rep = new AccountChangeReps();
                Label lblMultiFromRep = (Label)r.FindControl("lblMultiFromRep");
                Label lblMultiToRep = (Label)r.FindControl("lblMultiToRep");
                Label lblMultiAccountNumber = (Label)r.FindControl("lblMultiAccountNumber");
                Label lblMultiAccountName = (Label)r.FindControl("lblMultiAccountName");

                rep.FromRep = lblMultiFromRep.Text;
                rep.ToRep = lblMultiToRep.Text;
                rep.AccountName = lblMultiAccountName.Text;
                rep.AccountNumber = lblMultiAccountNumber.Text;
                collReps.Add(rep);
            }

            return collReps;
        }
    } // end class
}   // end namespace
