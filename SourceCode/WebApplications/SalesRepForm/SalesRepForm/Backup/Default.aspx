﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SalesRepForm._Default" MasterPageFile="MasterRep.Master" Title="Menu"%>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td colspan="2" class="titleheader">Select One</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><asp:LinkButton ID="lnkNewRep" ToolTip="Add a New Rep" runat="server" onclick="lnkNewRep_Click">New Sales/Service Rep Induction</asp:LinkButton></td>
            </tr>
            <tr>
                <td><asp:LinkButton ID="lnkTransfer" ToolTip="Change current status or partial/complete termination of a rep" runat="server" onclick="lnkTransfer_Click">Rep Termination/Status Change</asp:LinkButton></td>
            </tr>
            <tr>
                <td><asp:LinkButton ID="lnkAccountChange" runat="server" ToolTip="Assign/reassign accounts to reps" 
                        onclick="lnkAccountChange_Click">Account Change</asp:LinkButton></td>
            </tr>
            <tr>
                <td><asp:LinkButton ID="lnkPersonalInfo" runat="server" ToolTip="Change/Update rep personal information"
                         onclick="lnkPersonalInfo_Click">Change Personal Information</asp:LinkButton></td>
            </tr>
            <tr>
                <td><asp:LinkButton ID="lnkSystemAccess" runat="server" ToolTip="IT Request to change system access, e-mail forwarding" 
                        onclick="lnkSystemAccess_Click">System Access</asp:LinkButton></td>
            </tr>
            <tr>
                <td><asp:LinkButton ID="lnkNonRep" ToolTip="New or Term non-rep, distributor personnel" runat="server" onclick="lnkNonRep_Click">Non-Rep</asp:LinkButton></td>
            </tr>
        </table>
    </div>
</asp:Content>

