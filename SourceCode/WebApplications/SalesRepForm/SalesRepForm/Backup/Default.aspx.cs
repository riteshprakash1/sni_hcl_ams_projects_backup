﻿// ------------------------------------------------------------------
// <copyright file="Default.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;

    /// <summary>
    /// This is the code behind for the default page
    /// </summary>
    public partial class _Default : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the Click event of the lnkNewRep control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkNewRep_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewSalesRep.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the lnkAccountChange control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkAccountChange_Click(object sender, EventArgs e)
        {
            Response.Redirect("AccountChange.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the lnkPersonalInfo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkPersonalInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect("RepPersonalInformation.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the lnkTransfer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkTransfer_Click(object sender, EventArgs e)
        {
            Response.Redirect("RepStatusChange.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the lnkSystemAccess control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkSystemAccess_Click(object sender, EventArgs e)
        {
            Response.Redirect("RepSystemAccess.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the lnkNonRep control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkNonRep_Click(object sender, EventArgs e)
        {
            Response.Redirect("NonRep.aspx", true);
        }
    }
}
