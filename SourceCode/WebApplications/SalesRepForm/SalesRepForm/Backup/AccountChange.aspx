﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountChange.aspx.cs" Inherits="SalesRepForm.AccountChange" MasterPageFile="MasterRep.Master" Title="Account Change Form" %>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc2" Namespace="SalesRepForm.Classes" Assembly="SalesRepForm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table width="100%" border="0">
            <tr>
                <td class="titleheader">Account Change Information</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td colspan="4"><asp:ValidationSummary ID="vldSummary2" ForeColor="#FF7300" runat="server" />
                    <asp:ValidationSummary ID="vldSummary1" ValidationGroup="AddRep" ForeColor="#FF7300" runat="server" />
                    <asp:Label ID="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
                </td>
            </tr>
           <tr>
                <td style="width:10%">Change Type: *</td>
                <td style="width:30%">
                    <asp:DropDownList ID="ddlChangeType" runat="server" CausesValidation="false" AutoPostBack="true" 
                        onselectedindexchanged="ddlChangeType_SelectedIndexChanged">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem Value="Individual Account Change" Text="Individual Account Change"></asp:ListItem>
                        <asp:ListItem Value="Mass Account Change" Text="Mass Account Change"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldRequireChangeType" ControlToValidate="ddlChangeType" runat="server" ErrorMessage="Change Type is required.">*</asp:RequiredFieldValidator>
                </td>
                <td style="width:12%"><asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date: *"></asp:Label></td>
                <td><uc1:RequiredDateTimeControl ID="txtEffectiveDate" DateControlTextBoxWidthInPixels="100" runat="server" /></td>
            </tr>
        </table>
        <table width="100%" border="0" id="tblDetails" runat="server">
            <tr>
                <td valign="top">Business Lines Affected:</td>
                <td colspan="3">
                    <asp:CheckBoxList ID="chbBusinessLines" runat="server" RepeatColumns="4" 
                        RepeatDirection="Horizontal" Width="700px">
                         <asp:ListItem>Trauma(30*)</asp:ListItem>
                        <asp:ListItem>Knees(40*)</asp:ListItem>
                        <asp:ListItem>Hips(45*)</asp:ListItem>
                        <asp:ListItem>Cement/Shoulder(47*)</asp:ListItem>
                        <asp:ListItem>Exogen(4950*)</asp:ListItem>
                        <asp:ListItem>Supatz(4951*)</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
                   </asp:CheckBoxList>
                </td>
            </tr> 
            <tr>
                <td>Other: </td>
                <td colspan="3"><asp:TextBox ID="txtOtherBusinessLine" TextMode="MultiLine" Rows="2" Font-Size="8pt" runat="server" Width="650px"></asp:TextBox></td>
            </tr>          
            <tr>
               <td>&nbsp;</td>
            </tr> 
            <tr>
                <td style="width:10%"><asp:Label ID="lblFrom" runat="server" Text="From Rep: *"></asp:Label></td>
                <td style="width:20%"><asp:TextBox ID="txtFromRep" MaxLength="6" Font-Size="8pt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldFromRep" ControlToValidate="txtFromRep" runat="server" ErrorMessage="From Rep is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="vldFromRepFormat" runat="server"  ErrorMessage="From Rep must be six digits long." ControlToValidate="txtFromRep" 
                        ValidationExpression="\d{6}">*</asp:RegularExpressionValidator>
                </td>
                <td style="width:10%"><asp:Label ID="lblTo" runat="server" Text="To Rep: *"></asp:Label></td>
                <td><asp:TextBox ID="txtToRep" MaxLength="6" Font-Size="8pt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldToRep" ControlToValidate="txtToRep" runat="server" ErrorMessage="To Rep is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="vldToRepFormat" runat="server"  ErrorMessage="To Rep must be six digits long." ControlToValidate="txtToRep" 
                        ValidationExpression="\d{6}">*</asp:RegularExpressionValidator>
               </td>
            </tr>          
            <tr>
                <td>&nbsp;</td>
                <td valign="top"><asp:Label ID="lbl6DigitsFrom" runat="server" Text="6 Digit Rep #"></asp:Label></td>
                <td>&nbsp;</td>
                <td valign="top"><asp:Label ID="lbl6DigitsTo" runat="server" Text="6 Digit Rep #"></asp:Label></td>
            </tr> 
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary ID="vldSummary3" ValidationGroup="Add" ForeColor="#FF7300" runat="server" />
                </td>
            </tr>  
            <tr>
                <td><asp:Label ID="lblIndividualReps" runat="server" Text="Reps: *"></asp:Label></td>
                <td colspan="3">
                    <asp:GridView ID="gvReps" runat="server" Width="800" AutoGenerateColumns="False"  OnRowDataBound="GV_RepsRowDataBound"
                         OnRowDeleting="GV_RepDelete"  ShowFooter="true" DataKeyNames="AccountNumber" 
                         CellPadding="2" GridLines="Both" OnRowCancelingEdit="GV_RepsAdd">
                        <RowStyle HorizontalAlign="left" />
                        <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                        <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnRepDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								        ></asp:Button>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnRepAdd"  ValidationGroup="AddRep" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					            </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Account Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblMultiAccountNumber" Font-Size="8pt" runat="server"  Text='<%# Eval("AccountNumber")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMultiAccountNumber" MaxLength="50" Width="125px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldAccountNumber" ValidationGroup="AddRep" ControlToValidate="txtMultiAccountNumber" runat="server" ErrorMessage="Account Number is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Account Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblMultiAccountName" Font-Size="8pt" runat="server"  Text='<%# Eval("AccountName")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMultiAccountName" MaxLength="100" Width="200px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldAccountName" ValidationGroup="AddRep" ControlToValidate="txtMultiAccountName" runat="server" ErrorMessage="Account Name is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="15%" HeaderText="From Rep">
                                <ItemTemplate>
                                    <asp:Label ID="lblMultiFromRep" Font-Size="8pt" runat="server"  Text='<%# Eval("fromRep")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMultiFromRep" MaxLength="6" Width="75px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldFromRep" ValidationGroup="AddRep" ControlToValidate="txtMultiFromRep" runat="server" ErrorMessage="From Rep is required.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="vldFromRepFormat" runat="server" ValidationGroup="AddRep" ErrorMessage="From Rep must be six digits long." ControlToValidate="txtMultiFromRep" 
                                    ValidationExpression="\d{6}">*</asp:RegularExpressionValidator>                                
                                    <br /><asp:Label ID="lblMultiFrom6" Font-Size="8pt" runat="server"  Text="6 Digit Rep #"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="15%" HeaderText="To Rep">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMultiToRep" Font-Size="8pt" runat="server"  Text='<%# Eval("toRep")%>'></asp:Label>
                                    </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMultiToRep" MaxLength="6" Width="75px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldToRep" ValidationGroup="AddRep" ControlToValidate="txtMultiToRep" runat="server" ErrorMessage="To Rep is required.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="vldToRepFormat" ValidationGroup="AddRep" runat="server"  ErrorMessage="To Rep must be six digits long." ControlToValidate="txtMultiToRep" 
                                        ValidationExpression="\d{6}">*</asp:RegularExpressionValidator>
                                    <br /><asp:Label ID="lblMultiTo6" Font-Size="8pt" runat="server"  Text="6 Digit Rep #"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                       </Columns>
                    </asp:GridView>                
                     <asp:Label ID="lblAddRepMsg" Font-Size="8pt" runat="server"  Text="Click 'Add' after each entry if needing multiple selections."></asp:Label>
                  </td>
            </tr>         
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>          
            <tr>
                <td><asp:Label ID="lblStateCountry" runat="server" Text="State/ Country: *"></asp:Label>
                <asp:CustomValidator ID="vldStateCountry" runat="server" ErrorMessage="You must enter at least one state or country.">*</asp:CustomValidator></td>
                <td colspan="3">
                    <asp:GridView ID="gvState" runat="server" Width="500" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                         OnRowDeleting="GV_Delete"  ShowFooter="true" 
                         CellPadding="2" GridLines="Both" OnRowCancelingEdit="GV_Add">
                        <RowStyle HorizontalAlign="left" />
                        <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                        <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								        ></asp:Button>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					            </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="25%" HeaderText="State">
                                <ItemTemplate>
                                    <asp:Label ID="lblState" Font-Size="8pt" runat="server"  Text='<%# Eval("state")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlState" width="125px" Font-Size="8pt" runat="server"></asp:DropDownList>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Country">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCountry" Font-Size="8pt" runat="server"  Text='<%# Eval("country")%>'></asp:Label>
                                    </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlCountry" width="175px" Font-Size="8pt" runat="server"></asp:DropDownList>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                       </Columns>
                    </asp:GridView> 
                    <asp:Label ID="lblAddMsg" Font-Size="8pt" runat="server"  Text="Click 'Add' after each entry if needing multiple selections."></asp:Label>
                </td>
            </tr>                 
             <tr>
                <td colspan="4">&nbsp;</td>
            </tr>          
             <tr>
                <td><asp:Label ID="lblAttachFile" runat="server" Text="Attach File:"></asp:Label>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
                <td colspan="3">
                    <asp:FileUpload Width="650px" Font-Size="8pt" ID="fileUpload" runat="server" />
                    <asp:CustomValidator ID="vldFileUpload" runat="server" ErrorMessage="The file does not exist.  Please check the path.">*</asp:CustomValidator>
                    <asp:CustomValidator ID="vldAttachSize" runat="server" ErrorMessage="The attachment is too large.  It must be less than 1 MB.">*</asp:CustomValidator>
                    <asp:CustomValidator ID="vldResetFileUpload" runat="server" ErrorMessage="Please reset your attachment file<br>">*</asp:CustomValidator>
                   <br /> <asp:Label ID="lblAttachMsg" Font-Size="8pt" runat="server"  Text="Enter attachment after completing all other fields and prior to clicking the 'Submit' button."></asp:Label>
               </td>
            </tr>          
       </table>
        <table width="100%" id="tblButtons" runat="server">
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:33%; text-align:center">
                    <uc2:WaitButton ID="btnSubmit" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                        OnClick="btnSubmit_Click" Text="Submit" WaitText="Processing..." ToolTip="Click here to submit.">
                    </uc2:WaitButton>
                </td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnClear" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                        Text="Clear" onclick="btnClear_Click" 
                         /></td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnMenu" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                        Text="Menu" onclick="btnMenu_Click" 
                         /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>          
        </table>
    </div>
</asp:Content>
