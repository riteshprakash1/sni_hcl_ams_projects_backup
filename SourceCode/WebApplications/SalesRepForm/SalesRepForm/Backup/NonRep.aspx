﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonRep.aspx.cs" Inherits="SalesRepForm.NonRep" MasterPageFile="MasterRep.Master" Title="Non-Rep Form" %>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table width="100%" border="0">
            <tr>
                <td class="titleheader">Non-Rep Information</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td colspan="4"><asp:ValidationSummary ID="vldSummary2" ForeColor="#FF7300" runat="server" />
                    <asp:Label ID="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
                    <asp:HiddenField ID="hdnNonEmployeeID" Value="0" runat="server" />
               </td>
            </tr>
            <tr>
                <td style="width:10%">Change Type: *</td>
                <td style="width:30%"><asp:DropDownList ID="ddlChangeType" Font-Size="8pt" runat="server" 
                        Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem Value="Change" Text="Change"></asp:ListItem>
                        <asp:ListItem Value="New" Text="New"></asp:ListItem>
                        <asp:ListItem Value="Terminate" Text="Terminate"></asp:ListItem>
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                        ControlToValidate="ddlChangeType" ErrorMessage="Change Type is required.">*</asp:RequiredFieldValidator>
               </td>
                <td style="width:12%">Effective Date: *</td>
                <td><uc1:RequiredDateTimeControl ID="txtEffectiveDate" DateControlTextBoxWidthInPixels="100" runat="server" /></td>
            </tr>
            <tr>
                <td>Last Name: *</td>
                <td><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" runat="server" 
                        Width="200px" AutoPostBack="True" ontextchanged="txtLastName_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtLastName" ErrorMessage="Last Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Given First Name: *</td>
                <td><asp:TextBox ID="txtFirstName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtFirstName" ErrorMessage="Given First Name is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td colspan="4">
                    <table width="100%">
                        <tr>
                            <td>
                              <asp:GridView ID="gvFindUsers" runat="server" Width="750" AutoGenerateColumns="False"
                                    EmptyDataText="There are no users." OnRowEditing="GV_Select"
                                     CellPadding="2" DataKeyNames="NonEmployeeID,MidName,EmailAddress,SupervisorName,SalesDistrict,HcpFacing" GridLines="Both" 
                                    >
                                    <Columns>
                                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button id="btnSelect" Width="50px" Height="18" runat="server" Text="Select" CommandName="Edit"
				                                    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
					                                ></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastName" Font-Size="8pt" runat="server"  Text='<%# Eval("lastname")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" Font-Size="8pt" runat="server"  Text='<%# Eval("firstname")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress1" Font-Size="8pt" runat="server"  Text='<%# Eval("street1")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" Font-Size="8pt" runat="server"  Text='<%# Eval("city")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" Font-Size="8pt" runat="server"  Text='<%# Eval("state")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Zip Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblZip" Font-Size="8pt" runat="server"  Text='<%# Eval("PostalCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                   </Columns>
                                </asp:GridView> 
                            </td>
                            <td style="text-align:center; vertical-align:top"><asp:Button ID="btnClearGrid" 
                                    CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                                    Text="Hide Grid" onclick="btnClearGrid_Click" />
                            </td>
                        </tr>
                    </table>
                     
                </td>
            </tr>            
            <tr>
                <td>Middle Name:</td>
                <td><asp:TextBox ID="txtMiddleName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                <td>Preferred First Name:</td>
                <td><asp:TextBox ID="txtPreferredName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Title:</td>
                <td><asp:TextBox ID="txtTitle" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                <td>Cost Center:</td>
                <td><asp:TextBox ID="txtCostCenter" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Supervisor Name: *</td>
                <td><asp:TextBox ID="txtSupervisorName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtSupervisorName" ErrorMessage="Supervisor Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Supervisor Title:</td>
                <td><asp:TextBox ID="txtSupervisorTitle" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txtSupervisorTitle" ErrorMessage="Supervisor Title is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Territory/District Name: *</td>
                <td><asp:TextBox ID="txtDistrictName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtDistrictName" ErrorMessage="Territory/District Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>HCP Facing: *</td>
                <td><asp:DropDownList ID="ddlHcpFacing" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="ddlHcpFacing" ErrorMessage="HCP Facing is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Email:</td>
                <td><asp:TextBox ID="txtEmail" MaxLength="75" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtEmail" runat="server" 
                        ErrorMessage="Email format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td class="titleheader">Technology Request</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td style="width:12%">Voice Mail:</td>
                <td style="width:30%"><asp:DropDownList ID="ddlVoiceMail" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width:14%">Sales Life On-line:</td>
                <td><asp:DropDownList ID="ddlOrthoOnline" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>SNUG (VPN) Access:</td>
                <td><asp:DropDownList ID="ddlSnugAccess" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>Ortho On-line Service:</td>
                <td><asp:DropDownList ID="ddlOrthoOnlineService" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><asp:DropDownList ID="ddlEmailService" Font-Size="8pt" runat="server" 
                        Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>Sales Reporting Service:</td>
                <td><asp:DropDownList ID="ddlSalesReporting" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>CT Rep:</td>
                <td><asp:DropDownList ID="ddlCTRep" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
                <td>Learn Center Access:</td>
                <td><asp:DropDownList ID="ddlLearnCenter" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
             <tr>
                <td>SAP:</td>
                <td><asp:DropDownList ID="ddlSAP" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
                <td>Data Warehouse:</td>
                <td><asp:DropDownList ID="ddlDataWarehouse" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Kronos:</td>
                <td><asp:DropDownList ID="ddlKronos" Font-Size="8pt" runat="server" Width="125px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Forward S&amp;N mail to this email: </td>
                <td><asp:TextBox ID="txtForwardEmail" MaxLength="75" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtForwardEmail" runat="server" 
                        ErrorMessage="Forward Email Address format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
               </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3" style="text-align:left; vertical-align:text-top">Email will not be forwarded if left blank</td>
            </tr>           
            <tr>
                <td valign="top">Comments: </td>
                <td colspan="3"><asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="3"  
                        Font-Size="8pt" runat="server" Width="600px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table width="960">
            <tr>
                <td style="width:33%; text-align:center"><asp:Button ID="btnSubmit" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" /></td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnClear" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Clear" 
                        onclick="btnClear_Click" /></td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnMenu" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Menu" 
                        onclick="btnMenu_Click" /></td>
            </tr>
        </table>
        <asp:Literal ID="ltlEmail" runat="server"></asp:Literal>
    </div>
</asp:Content>
