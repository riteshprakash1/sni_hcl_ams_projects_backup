﻿// ------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.SessionState;
    using SalesRepForm.Classes;

    /// <summary>
    /// The global class
    /// </summary>
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Handles the Start event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the Start event of the Session control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Session_Start(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the BeginRequest event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the AuthenticateRequest event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the Error event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();
            string authUser = Request.LogonUserIdentity.Name.ToString();
            string strErrorMsg = ex.Message;

            try
            {
                ex = Server.GetLastError().GetBaseException();
                ////System.Diagnostics.Debug.WriteLine("Application Error: " + ex.Message + "; Inner: " + ex.ToString());
                Debug.WriteLine("Application Error: ");
                LogException exc = new LogException();
                exc.HandleException(ex, Request);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine("ExceptionHandler Error: " + exc.Message);
            }

            Server.ClearError();

            string strUrl = Request.Url.ToString().Replace(Request.Url.PathAndQuery, String.Empty);
            string strErrorPage = ConfigurationManager.AppSettings["ErrorPage"];
            Debug.WriteLine("strUrl: " + strUrl);
            Debug.WriteLine("strUrl: " + strUrl + strErrorPage + "?ErrorMsg=" + strErrorMsg);
            Response.Redirect(strUrl + strErrorPage + "?ErrorMsg=" + strErrorMsg, false);
        }

        /// <summary>
        /// Handles the End event of the Session control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Session_End(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the End event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}