﻿// ------------------------------------------------------------------
// <copyright file="RepStatusChange.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    using SalesRepForm.Classes;

    /// <summary>
    /// This is the code behind for the RepStatusChange page
    /// </summary>
    public partial class RepStatusChange : System.Web.UI.Page
    {
        private Header Header1;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Header1 = (Header)Page.Master.FindControl("Header1");

            if (!Page.IsPostBack)
            {
                EnterButton.TieButtonDoTab(this.txtFirstName);
                EnterButton.TieButtonDoTab(this.txtLastName);
                EnterButton.TieButtonDoTab(this.txtMiddleName);
                EnterButton.TieButtonDoTab(this.Header1.RequestorEmail);
                EnterButton.TieButtonDoTab(this.Header1.RequestorName);
                EnterButton.TieButtonDoTab(this.Header1.RequestorPhone);
                EnterButton.TieButtonDoTab(this.txtUserName);
                EnterButton.TieButtonDoTab(this.txtManager);

                this.SetControls();
            }

            this.lblMsg.Text = String.Empty;
        } //// end Page_Load

        /// <summary>
        /// Sets the transfer email body.
        /// </summary>
        /// <returns>EmailBody string</returns>
        protected string SetTransferEmailBody()
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Sales Rep Information</td>";
            strBody += "</tr>";
            strBody += "</table>";
            strBody += "<table border=1>";
            strBody += "<tr>";
            strBody += "<td style=\"width:20%\">Last Name:</td>";
            strBody += "<td style=\"width:25%\">&nbsp;" + this.txtLastName.Text + "</td>";
            strBody += "<td style=\"width:20%\">Given First Name:</td>";
            strBody += "<td>&nbsp;" + this.txtFirstName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>&nbsp;" + this.txtMiddleName.Text + "</td>";
            strBody += "<td>Effective Date:</td>";
            strBody += "<td>&nbsp;" + this.txtEffectiveDate.Date + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Territory/District Mgr:</td>";
            strBody += "<td>&nbsp;" + this.txtManager.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Transfer Type:</td>";
            strBody += "<td>&nbsp;" + this.ddlTransferType.SelectedValue + "</td>";
            strBody += "<td>Compensation:</td>";
            strBody += "<td>&nbsp;" + this.txtCompensation.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Transfer From:</td>";
            strBody += "<td>&nbsp;" + this.txtTransferFrom.Text + "</td>";
            strBody += "<td>Transfer To:</td>";
            strBody += "<td>&nbsp;" + this.txtTransferTo.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Comments:</td>";
            strBody += "<td valign=\"top\" colspan=\"3\">" + this.txtComments.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr></table>";

            return strBody;
        }



        /// <summary>
        /// Sets the term email body.
        /// </summary>
        /// <returns>EmailBody string</returns>
        protected string SetTermEmailBody()
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Terminated Rep Information</td>";
            strBody += "</tr><tr>";
            strBody += "<td style=\"width:20%\">Last Name:</td>";
            strBody += "<td style=\"width:25%\">&nbsp;" + this.txtLastName.Text + "</td>";
            strBody += "<td style=\"width:20%\">Given First Name:</td>";
            strBody += "<td>&nbsp;" + this.txtFirstName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>&nbsp;" + this.txtMiddleName.Text + "</td>";
            strBody += "<td>Effective Date:</td>";
            strBody += "<td>&nbsp;" + this.txtEffectiveDate.Date + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Territory/District Mgr:</td>";
            strBody += "<td>&nbsp;" + this.txtManager.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Staying with S&N:</td>";
            strBody += "<td>&nbsp;" + this.ddlStayWithSN.SelectedValue + "</td>";
            strBody += "<td>GBU Transfer:</td>";
            strBody += "<td>&nbsp;" + this.ddlGBUTransfer.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Delete System Access:</td>";
            strBody += "<td>&nbsp;" + this.ddlDeleteSystemAccess.SelectedValue + "</td>";
            strBody += "<td>User Name/ ID:</td>";
            strBody += "<td>&nbsp;" + this.txtUserName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Comments:</td>";
            strBody += "<td valign=\"top\" colspan=\"3\">" + this.txtComments.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr>";
            strBody += "</table>";

            return strBody;
        }

        /// <summary>
        /// Handles the Click event of the btnMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("RepStatusChange.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnSubmitNndChange control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmitNndChange_Click(object sender, EventArgs e)
        {
            switch (this.ddlChangeType.SelectedIndex)
            {
                case 1:
                    this.SubmitTermForm();
                    Response.Redirect("AccountChange.aspx");
                    break;
                case 2:
                    this.SubmitTransferForm();
                    Response.Redirect("AccountChange.aspx");
                    break;
                case 0:
                    Page.Validate();
                    break;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            switch (this.ddlChangeType.SelectedIndex)
            {
                case 1:
                    this.SubmitTermForm();
                    break;
                case 2:
                    this.SubmitTransferForm();
                    break;
                case 0:
                    Page.Validate();
                    break;
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlChangeType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlChangeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetControls();
        }

        /// <summary>
        /// Submits the term form.
        /// </summary>
        /// <returns>RequestID string</returns>
        private string SubmitTermForm()
        {
            string strRequestID = "0";
            Page.Validate();

            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    ChangeRequest cr = new ChangeRequest();
                    DateTime datNow = DateTime.Now;

                    cr.NonEmployeeID = Int32.Parse(this.hdnNonEmployeeID.Value);
                    cr.ChangeType = this.hdnChangeType.Value;
                    cr.CreateDate = datNow;
                    cr.CreatedBy = ADUtility.GetAppUsername(Request);
                    cr.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Date);
                    cr.Comments = this.txtComments.Text;
                    cr.StayingWithSN = this.ddlStayWithSN.SelectedValue;
                    cr.GBUTransfer = this.ddlGBUTransfer.SelectedValue;
                    cr.DeleteTechAccess = this.ddlDeleteSystemAccess.SelectedValue;
                    cr.FirstName = this.txtFirstName.Text;
                    cr.LastName = this.txtLastName.Text;
                    cr.MiddleName = this.txtMiddleName.Text;
                    cr.ManagerName = this.txtManager.Text;
                    cr.UserName = this.txtUserName.Text;
                    cr.RequestorEmail = this.Header1.RequestorEmail.Text;
                    cr.RequestorName = this.Header1.RequestorName.Text;
                    cr.RequestorPhone = this.Header1.RequestorPhone.Text;
                    cr.UpdateDate = datNow;
                    cr.UpdatedBy = ADUtility.GetAppUsername(Request);

                    sr.ChangeRequest.InsertOnSubmit(cr);
                    sr.SubmitChanges();

                    Debug.WriteLine("New ID: " + cr.RequestID.ToString());

                    string strSubject = "Terminated Sales Rep Form Submitted by " + cr.RequestorName;
                    string strBody = this.SetTermEmailBody();

                    ////this.ltlEmail.Text = strBody;

                    Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, this.ddlChangeType.SelectedValue, null);

                    strRequestID = cr.RequestID.ToString();
                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } // end Page.IsValid

            return strRequestID;
        }
        
        /// <summary>
        /// Submits the transfer form.
        /// </summary>
        /// <returns>RequestID string</returns>
        private string SubmitTransferForm()
        {
            string strRequestID = "0";
            Page.Validate();

            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    TransferRequest tr = new TransferRequest();
                    DateTime datNow = DateTime.Now;

                    tr.RequestorEmail = this.Header1.RequestorEmail.Text;
                    tr.RequestorName = this.Header1.RequestorName.Text;
                    tr.RequestorPhone = this.Header1.RequestorPhone.Text;

                    tr.FirstName = this.txtFirstName.Text;
                    tr.LastName = this.txtLastName.Text;
                    tr.MiddleName = this.txtMiddleName.Text;
                    tr.ManagerName = this.txtManager.Text;

                    tr.Compensation = this.txtCompensation.Text;
                    tr.TransferFrom = this.txtTransferFrom.Text;
                    tr.TransferTo = this.txtTransferTo.Text;
                    tr.TransferType = this.ddlTransferType.SelectedValue;

                    tr.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Date);
                    tr.Comments = this.txtComments.Text;

                    tr.CreateDate = datNow;
                    tr.CreatedBy = ADUtility.GetAppUsername(Request);
                    tr.UpdateDate = datNow;
                    tr.UpdatedBy = ADUtility.GetAppUsername(Request);

                    sr.TransferRequest.InsertOnSubmit(tr);
                    sr.SubmitChanges();

                    Debug.WriteLine("New ID: " + tr.TransferRequestID.ToString());

                    string strSubject = "Transfer/ Status Change Submitted by " + tr.RequestorName;
                    string strBody = this.SetTransferEmailBody();

                    ////this.ltlEmail.Text = strBody;

                    Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, this.ddlChangeType.SelectedValue, null);

                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } // end Page.IsValid

            return strRequestID;
        }

        /// <summary>
        /// Sets the controls.
        /// </summary>
        private void SetControls()
        {
            this.lblCompensation.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            this.txtCompensation.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);

            this.lblGBUTransfer.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.ddlGBUTransfer.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.vldGBUTransfer.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);

            this.lblDeleteSystemAccess.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.ddlDeleteSystemAccess.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.vldDeleteSystemAccess.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);

            this.lblStayWithSN.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.ddlStayWithSN.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.vldStayWithSN.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);

            this.lblTransferFrom.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            this.txtTransferFrom.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            this.lblTransferTo.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            this.txtTransferTo.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            
            this.lblTransferType.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            this.ddlTransferType.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);
            this.vldTransferType.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 2);

            this.lblUserName.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.txtUserName.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
            this.vldUserName.Visible = Convert.ToBoolean(this.ddlChangeType.SelectedIndex == 1);
        }
    } // end class
}
