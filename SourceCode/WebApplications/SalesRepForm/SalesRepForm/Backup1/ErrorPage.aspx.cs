﻿// ------------------------------------------------------------------
// <copyright file="ErrorPage.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;

    /// <summary>
    /// This is the page users are redirected 
    /// to when an application error occurs.
    /// </summary>
    public partial class ErrorPage : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ErrorMsg"] != null)
            {
                this.lblErrorMsg.Text = "<b>Error Message:</b> " + Request.QueryString["ErrorMsg"];
            }
        }
    }
}
