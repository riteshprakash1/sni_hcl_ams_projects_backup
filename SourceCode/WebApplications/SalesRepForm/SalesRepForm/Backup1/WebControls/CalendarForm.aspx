﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarForm.aspx.cs" Inherits="SalesRepForm.WebControls.CalendarForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </title>

    <script language="javascript" type="text/javascript">
    function ReturnDate()
    {        
        try
        {      
            evalString = "window.opener.document.forms[0]." 
                            + document.forms[0].controlId.value 
                            + "$_dateTextBox.value = document.forms[0].hiddenDate.value;";
            eval(evalString);    
        }
        catch(e)
        {
            alert(e.message);
        }
        this.close();          
    }
    </script>

</head>
<body style="background-color: <%=EvenRowColor%>">
    <form id="_calendarForm" runat="server">
    <div>
        <input type="hidden" id="hiddenDate" value="<%=SelectedDate%>" />
        <input type="hidden" id="controlId" value="<%=ControlId%>" />
        <table align="center">
            <tr>
                <td align="center">
                    <asp:Calendar ID="_controlCalendar" runat="server" OnSelectionChanged="_controlCalendar_SelectionChanged">
                    </asp:Calendar>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <input type="button" value="close" style="width: 75px" onclick="ReturnDate();" id="Button1" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
