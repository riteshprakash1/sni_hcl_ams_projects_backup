﻿//---------------------------------------------------
// <copyright file="SqlUtility.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
//---------------------------------------------------
namespace SalesRepForm.Classes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Globalization;

    /// <summary>
    /// This is SQL helper class.
    /// </summary>
    public sealed class SqlUtility
    {
        /// <summary>
        /// This is my default connection.
        /// </summary>
        private static SqlConnection connection = null;

        /// <summary>
        /// Prevents a default instance of the <see cref="SqlUtility"/> class from being created.
        /// </summary>
        private SqlUtility()
        {
        }

        /// <summary>
        /// Execute non query.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>True or false.</returns>
        public static bool SqlExecuteNonQuery(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            bool boolPassed = false;
            SqlConnection myConnectionInstance = SqlUtility.GetSqlConnection();
            SqlTransaction myTransaction = myConnectionInstance.BeginTransaction();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, myConnectionInstance, myTransaction);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            myTransaction.Commit();

            if (intCommandOutcome != 0)
            {
                boolPassed = true;
            }

            return boolPassed;
        }

        /// <summary>
        /// Execute non query and return the number of updated records.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static int SqlExecuteNonQueryCount(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection myConnectionInstance = SqlUtility.GetSqlConnection();
            SqlTransaction myTransaction = myConnectionInstance.BeginTransaction();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, myConnectionInstance, myTransaction);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            myTransaction.Commit();

            return intCommandOutcome;
        }

        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            SqlConnection myConnectionInstance = SqlUtility.GetSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = myConnectionInstance;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            return myDataTable;
        }

        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            SqlConnection myConnectionInstance = SqlUtility.GetSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = myConnectionInstance;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            return myDataTable;
        }

        /// <summary>
        /// SQLs the execute query.
        /// </summary>
        /// <param name="conn">The SQL conn.</param>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <returns>the DataTable</returns>
        public static DataTable SqlExecuteStoredProcedure(SqlConnection conn, string storedProcedureName)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;

            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            return myDataTable;
        }

        /// <summary>
        /// Executes a query.
        /// </summary>
        /// <param name="strQuery">The text of the query to execute</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteDynamicQuery(string strQuery)
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Locale = CultureInfo.InvariantCulture;
            SqlConnection myConnectionInstance = SqlUtility.GetSqlConnection();
            SqlUtility.GetSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = myConnectionInstance;
            command.CommandType = CommandType.Text;
            command.CommandText = strQuery;

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            return myDataTable;
        }

        /// <summary>
        /// Closes the SQL connection.
        /// </summary>
        /// <param name="connection">The SQL connection.</param>
        public static void CloseSqlConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            else if (connection.State == ConnectionState.Broken)
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Sets the table.
        /// </summary>
        /// <param name="sp">The stored procedure.</param>
        /// <returns>The filled datatable</returns>
        public static DataTable SetTable(string sp)
        {
            SqlCommand cmdSetTable = new SqlCommand();
            cmdSetTable.CommandText = sp;
            cmdSetTable.CommandType = CommandType.StoredProcedure;
            cmdSetTable.Connection = SqlUtility.GetSqlConnection();

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
            da.Fill(dt);
            da.Dispose();

            return dt;
        } //// END SetTable()

        /// <summary>
        /// Sets the table.
        /// </summary>
        /// <param name="sp">The stored procedure.</param>
        /// <param name="id1">The id for the stored procedure.</param>
        /// <returns>the datatable</returns>
        public static DataTable SetTable(string sp, int id1)
        {
            SqlCommand cmdSetTable = new SqlCommand();
            cmdSetTable.CommandText = sp;
            cmdSetTable.CommandType = CommandType.StoredProcedure;
            cmdSetTable.Connection = SqlUtility.GetSqlConnection();
            cmdSetTable.Parameters.Add(new SqlParameter("@ID", id1));

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
            da.Fill(dt);
            da.Dispose();

            return dt;
        } //// END SetTable()

        /// <summary>
        /// Sets the table.
        /// </summary>
        /// <param name="sp">The stored procedure.</param>
        /// <param name="str1">The string parameter for the stored procedure.</param>
        /// <returns>The filled datatable</returns>
        public static DataTable SetTable(string sp, string str1)
        {
            SqlCommand cmdSetTable = new SqlCommand();
            cmdSetTable.CommandText = sp;
            cmdSetTable.CommandType = CommandType.StoredProcedure;
            cmdSetTable.Connection = SqlUtility.GetSqlConnection();
            cmdSetTable.Parameters.Add(new SqlParameter("@str1", str1));

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
            da.Fill(dt);
            da.Dispose();

            return dt;
        } //// END SetTable()

        /// <summary>
        /// Sets the query table.
        /// </summary>
        /// <param name="strQuery">The query string.</param>
        /// <returns>The filled datatable</returns>
        public static DataTable SetQueryTable(string strQuery)
        {
            SqlCommand cmdSetTable = new SqlCommand();
            cmdSetTable.CommandText = strQuery;
            cmdSetTable.CommandType = CommandType.Text;
            cmdSetTable.Connection = SqlUtility.GetSqlConnection();
            DataTable dt = new DataTable();

            SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
            da.Fill(dt);
            da.Dispose();

            return dt;
        } //// END SetQueryTable()

        /// <summary>
        /// Gets the SQL connection.
        /// </summary>
        /// <returns>Returns a SQL connection</returns>
        private static SqlConnection GetSqlConnection()
        {
            ////Debug.WriteLine("sqlConn: " + ConfigurationSettings.AppSettings["sqlConn"].ToString());
            ////This is going to get a new SQL connection.
            if (connection == null)
            {
                connection = new SqlConnection();
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                Debug.WriteLine("Used Conn: " + connection.ConnectionString);
                connection.Open();
                return connection;
            }
            else if (connection.State == ConnectionState.Closed)
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                connection.Open();
                return connection;
            }
            else if (connection.State == ConnectionState.Open)
            {
                return connection;
            }
            else if (connection.State == ConnectionState.Broken)
            {
                connection.Close();
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                connection.Open();
                return connection;
            }
            else
            {
                SqlConnection tmpConnection = new SqlConnection();
                tmpConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                tmpConnection.Open();
                return tmpConnection;
            }
        }
    } //// end class
}
