﻿// ------------------------------------------------------------------
// <copyright file="RepSystemAccess.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2010 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SalesRepForm
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Xml.Linq;
    using SalesRepForm.Classes;

    /// <summary>
    /// This is the code behind for the RepSystemAccess page
    /// </summary>
    public partial class RepSystemAccess : System.Web.UI.Page
    {
        private Header Header1;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Header1 = (Header)Page.Master.FindControl("Header1");

            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtAddress1, this.btnSubmit);
                EnterButton.TieButton(this.txtAddress2, this.btnSubmit);
                EnterButton.TieButton(this.txtCity, this.btnSubmit);
                EnterButton.TieButton(this.txtFax, this.btnSubmit);
                EnterButton.TieButton(this.txtFirstName, this.btnSubmit);
                EnterButton.TieButton(this.txtLastName, this.btnSubmit);
                EnterButton.TieButton(this.txtMiddleName, this.btnSubmit);
                EnterButton.TieButton(this.txtMobile, this.btnSubmit);
                EnterButton.TieButton(this.txtOfficePhone, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorEmail, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorName, this.btnSubmit);
                EnterButton.TieButton(this.Header1.RequestorPhone, this.btnSubmit);
                EnterButton.TieButton(this.txtZipCode, this.btnSubmit);

                this.lnkServicePortal.Text = ConfigurationManager.AppSettings["ServicePortalURL"];
                this.lnkServicePortal.NavigateUrl = ConfigurationManager.AppSettings["ServicePortalURL"];
                this.lblServicePortalName.Text = ConfigurationManager.AppSettings["ServicePortalName"];

                string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                NonEmployee sr = new NonEmployee(connectionString);

                //States
                IEnumerable<States> myStates = from state in sr.States
                                               orderby state.States1
                                               select state;
                this.ddlState.Items.Clear();
                ddlState.Items.Add(new ListItem("--Select--", String.Empty));
                foreach (States st in myStates)
                {
                    ddlState.Items.Add(new ListItem(st.States1, st.Statescode));
                }

                this.gvFindUsers.Visible = false;
                this.btnClearGrid.Visible = false;
            }

            this.lblMsg.Text = String.Empty;

        } // end Page_Load

        /// <summary>
        /// Sets the email body.
        /// </summary>
        /// <returns></returns>
        protected string SetEmailBody()
        {
            string strBody = Utility.GetEmailHeader(this.Header1.RequestorName.Text, this.Header1.RequestorPhone.Text, this.Header1.RequestorEmail.Text);
            strBody += "<tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Sales/Service Rep Information</td>";
            strBody += "</tr>";
            strBody += "</table>";
            strBody += "<table border=1>";
            strBody += "<tr>";
            strBody += "<td style=\"width:20%\">Last Name:</td>";
            strBody += "<td style=\"width:25%\">" + this.txtLastName.Text + "</td>";
            strBody += "<td style=\"width:25%\">Given First Name:</td>";
            strBody += "<td>" + this.txtFirstName.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Middle Name:</td>";
            strBody += "<td>" + this.txtMiddleName.Text + "</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Address 1:</td>";
            strBody += "<td>" + this.txtAddress1.Text + "</td>";
            strBody += "<td>Address 2:</td>";
            strBody += "<td>" + this.txtAddress2.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>City:</td>";
            strBody += "<td>" + this.txtCity.Text + "</td>";
            strBody += "<td>State:</td>";
            strBody += "<td>" + this.ddlState.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Zip Code:</td>";
            strBody += "<td>" + this.txtZipCode.Text + "</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Office Phone:</td>";
            strBody += "<td>" + this.txtOfficePhone.Text + "</td>";
            strBody += "<td>Fax:</td>";
            strBody += "<td>" + this.txtFax.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Mobile Phone:</td>";
            strBody += "<td>" + this.txtMobile.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Sales District/Territory Name:</td>";
            strBody += "<td>" + this.txtDistrictName.Text + "</td>";
            strBody += "<td>Sales District/Territory Number:</td>";
            strBody += "<td>" + this.txtDistrictID.Text + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td colspan=\"2\" class=\"titleheader\">Other Requests</td>";
            strBody += "</tr>";
            strBody += "</table>";
            strBody += "<table border=1>";
            strBody += "<tr>";
            strBody += "<td style=\"width:25%\">Sales Life On-line:</td>";
            strBody += "<td style=\"width:25%\">" + this.ddlOrthoOnline.SelectedValue + "</td>";
            strBody += "<td style=\"width:25%\">Ortho On-line Service:</td>";
            strBody += "<td>" + this.ddlOrthoOnlineService.SelectedValue + "</td>";
            strBody += "</tr><tr>";
            strBody += "<td>Sales Reporting Service:</td>";
            strBody += "<td>" + this.ddlSalesReporting.SelectedValue + "</td>";
            strBody += "<td>Learn Center Access:</td>";
            strBody += "<td>" + this.ddlLearnCenter.SelectedValue + "</td>";
            strBody += "</tr><tr>";
 
            strBody += "<td>Kronos:</td>";
            strBody += "<td>" + this.ddlKronos.SelectedValue + "</td>";
            strBody += "<td>&nbsp;</td><td>&nbsp;</td>";
            strBody += "</tr><tr>";
            strBody += "<td valign=\"top\">Comments:</td>";
            strBody += "<td valign=\"top\" colspan=\"3\">" + this.txtComments.Text.Replace("\r", "<br>") + "</td>";
            strBody += "</tr>";
            strBody += "</table>";

            return strBody;
        }

        /// <summary>
        /// Handles the Click event of the btnMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("RepSystemAccess.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitForm();
        }

        /// <summary>
        /// Handles the TextChanged event of the txtLastName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void txtLastName_TextChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("txtLastName_TextChanged Fired");
            if (this.txtLastName.Text != String.Empty)
            {
                this.gvFindUsers.DataSource = Utility.GetNonEmployeesByLastname(this.txtLastName.Text);
                this.gvFindUsers.DataBind();
            }

            Debug.WriteLine("gvFindUsers.Rows: " + gvFindUsers.Rows.Count.ToString());

            this.txtFirstName.Focus();
            this.gvFindUsers.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
            this.btnClearGrid.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
        }

        /// <summary>
        /// Handles the button click event of the btnClearGrid Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void btnClearGrid_Click(object sender, EventArgs e)
        {
            this.gvFindUsers.DataSource = new DataTable();
            this.gvFindUsers.DataBind();

            this.txtFirstName.Focus();
            this.gvFindUsers.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
            this.btnClearGrid.Visible = Convert.ToBoolean(this.gvFindUsers.Rows.Count > 0);
        }

        /// <summary>
        /// Handles the Edit event of the gvFindUsers GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Select(object sender, GridViewEditEventArgs e)
        {
            this.hdnNonEmployeeID.Value = this.gvFindUsers.DataKeys[e.NewEditIndex]["NonEmployeeID"].ToString();
            if (this.gvFindUsers.DataKeys[e.NewEditIndex]["MidName"] != null)
            {
                this.txtMiddleName.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["MidName"].ToString();
            }

            if (this.gvFindUsers.DataKeys[e.NewEditIndex]["Street2"] != null)
            {
                this.txtAddress2.Text = this.gvFindUsers.DataKeys[e.NewEditIndex]["Street2"].ToString();
            }

            Label lblFirstName = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblFirstName");
            Label lblAddress1 = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblAddress1");
            Label lblCity = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblCity");
            Label lblState = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblState");
            Label lblZip = (Label)this.gvFindUsers.Rows[e.NewEditIndex].FindControl("lblZip");

            txtFirstName.Text = lblFirstName.Text;
            this.txtAddress1.Text = lblAddress1.Text;
            this.txtCity.Text = lblCity.Text;
            this.txtZipCode.Text = lblZip.Text;

            if (this.ddlState.Items.FindByValue(lblState.Text) != null)
            {
                this.ddlState.ClearSelection();
                this.ddlState.Items.FindByValue(lblState.Text).Selected = true;
            }

            this.btnClearGrid_Click(sender, e);
        }

        /// <summary>
        /// Submits the form.
        /// </summary>
        /// <returns></returns>
        private string SubmitForm()
        {
            string strRequestID = "0";
            Page.Validate();

            if (Page.IsValid)
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["SqlConn"].ConnectionString;
                    NonEmployee sr = new NonEmployee(connectionString);
                    ChangeRequest p = new ChangeRequest();
                    DateTime datNow = DateTime.Now;

                    p.NonEmployeeID = Int32.Parse(this.hdnNonEmployeeID.Value);
                    p.ChangeType = this.hdnChangeType.Value;
                    p.Address1 = this.txtAddress1.Text;
                    p.Address2 = this.txtAddress2.Text;
                    p.City = this.txtCity.Text;
                    p.Comments = this.txtComments.Text;
                    p.CreateDate = datNow;
                    p.CreatedBy = ADUtility.GetAppUsername(Request);
                    p.DataWarehouse = string.Empty;
                    p.EmailService = string.Empty;
                    p.ExperienceComments = this.txtComments.Text;
                    p.Fax = this.txtFax.Text;
                    p.FirstName = this.txtFirstName.Text;
                    p.Kronos = this.ddlKronos.SelectedValue;
                    p.LastName = this.txtLastName.Text;
                    p.LearnCenter = this.ddlLearnCenter.SelectedValue;
                    p.MiddleName = this.txtMiddleName.Text;
                    p.Mobile = this.txtMobile.Text;
                    p.DistrictName = this.txtDistrictName.Text;
                    p.District = this.txtDistrictID.Text;
                    p.OfficePhone = this.txtOfficePhone.Text;
                    p.OrthoOnline = this.ddlOrthoOnline.SelectedValue;
                    p.OrthoOnlineService = this.ddlOrthoOnlineService.SelectedValue;
                    p.RequestorEmail = this.Header1.RequestorEmail.Text;
                    p.RequestorName = this.Header1.RequestorName.Text;
                    p.RequestorPhone = this.Header1.RequestorPhone.Text;
                    p.SAP = string.Empty;
                    p.SalesReporting = ddlSalesReporting.SelectedValue;
                    p.SNUG = string.Empty;
                    p.State = this.ddlState.SelectedValue;
                    p.UpdateDate = datNow;
                    p.UpdatedBy = ADUtility.GetAppUsername(Request);
                    p.VoiceMail = string.Empty;
                    p.Zip = this.txtZipCode.Text;

                    p.MGMobileAccess = string.Empty;
                    p.MGMobileDevice = string.Empty;
                    p.MGWebAccess = string.Empty;

                    sr.ChangeRequest.InsertOnSubmit(p);
                    sr.SubmitChanges();

                    Debug.WriteLine("New ID: " + p.RequestID.ToString());

                    string strSubject = "Rep System Access Change Form Submitted by " + p.RequestorName;
                    string strBody = this.SetEmailBody();

                    ////this.ltlEmail.Text = strBody;

                    Utility.SendEmail(this.Header1.RequestorEmail.Text, strSubject, strBody, true, Page.Title, null);

                    strRequestID = p.RequestID.ToString();
                    this.lblMsg.Text = "The form was successfully submitted.";
                }
                catch (Exception ex)
                {
                    this.lblMsg.Text = "The form submission failed.<br>" + ex.Message;
                    LogException exc = new LogException();
                    exc.HandleException(ex, Request);
                }
            } // end Page.IsValid

            return strRequestID;
        }

    } //// end class
}
