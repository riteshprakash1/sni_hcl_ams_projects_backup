﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RepPersonalInformation.aspx.cs" Inherits="SalesRepForm.RepPersonalInformation" MasterPageFile="MasterRep.Master" Title="Personal Information Change" %>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table width="100%" border="0">
            <tr>
                <td class="titleheader">Change Personal Information</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td colspan="4" style="background-color:#ffffff;color:#78BDE8; font-size:14pt;font-weight: bold; height:35px; vertical-align:bottom">
                    Current
                </td>
            </tr>
            <tr>
                <asp:HiddenField ID="hdnChangeType" Value="Rep Personal Information Change" runat="server" />
                    <asp:HiddenField ID="hdnNonEmployeeID" Value="0" runat="server" />
                <td style="width:10%">Last Name: *</td>
                <td style="width:24%"><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" 
                        runat="server" Width="200px" ontextchanged="txtLastName_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtLastName" ErrorMessage="Current Last Name is required.">*</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                        ControlToValidate="txtLastName" ValidationGroup="FindUser" ErrorMessage="Current Last Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td style="width:13%">Given First Name: *</td>
                <td style="width:23%"><asp:TextBox ID="txtFirstName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtFirstName" ErrorMessage="Current Given First Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td style="width:10%">Middle Name:</td>
                <td style="width:13%"><asp:TextBox ID="txtMiddleName" MaxLength="25" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox></td>
            </tr>
             <tr>
                <td colspan="5">
                  <asp:GridView ID="gvFindUsers" runat="server" Width="800" AutoGenerateColumns="False"
                        EmptyDataText="There are no users." OnRowEditing="GV_Select"
                         CellPadding="2" DataKeyNames="NonEmployeeID,MidName,Street1,Street2,PostalCode" GridLines="Both" 
                        >
                        <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnSelect" Width="50px" Height="18" runat="server" Text="Select" CommandName="Edit"
				                        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
					                    ></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastName" Font-Size="8pt" runat="server"  Text='<%# Eval("lastname")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstName" Font-Size="8pt" runat="server"  Text='<%# Eval("firstname")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress1" Font-Size="8pt" runat="server"  Text='<%# Eval("street1")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City">
                                <ItemTemplate>
                                    <asp:Label ID="lblCity" Font-Size="8pt" runat="server"  Text='<%# Eval("city")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State">
                                <ItemTemplate>
                                    <asp:Label ID="lblState" Font-Size="8pt" runat="server"  Text='<%# Eval("state")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zip Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblZip" Font-Size="8pt" runat="server"  Text='<%# Eval("PostalCode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                       </Columns>
                    </asp:GridView>                  
                </td>
                <td style="text-align:center; vertical-align:top"><asp:Button ID="btnClearGrid" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                        Text="Hide Grid" onclick="btnClearGrid_Click" />
                </td>
            </tr>
           <tr>
                <td>Address 1: *</td>
                <td><asp:TextBox ID="txtAddress1" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtAddress1" ErrorMessage="Current Address 1 is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Address 2:</td>
                <td><asp:TextBox ID="txtAddress2" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>City: *</td>
                <td><asp:TextBox ID="txtCity" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtCity" ErrorMessage="Current City is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>State: *</td>
                <td><asp:DropDownList ID="ddlState" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="ddlState" ErrorMessage="Current State is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Zip Code: *</td>
                <td><asp:TextBox ID="txtZipCode" MaxLength="20" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                        ControlToValidate="txtZipCode" ErrorMessage="Current Zip Code is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Personal E-Mail: *</td>
                <td><asp:TextBox ID="txtRepEmail" MaxLength="75" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                        ControlToValidate="txtRepEmail" ErrorMessage="Email is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="vldEmailFormat" 
                        ControlToValidate="txtRepEmail" runat="server" 
                        ErrorMessage="Email format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
                <td>Home Phone:</td>
                <td><asp:TextBox ID="txtHomePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                <td>Office Phone: </td>
                <td><asp:TextBox ID="txtOfficePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Mobile Phone: *</td>
                <td><asp:TextBox ID="txtMobile" MaxLength="30" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                        ControlToValidate="txtMobile" ErrorMessage="Mobile Phone is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Fax:</td>
                <td><asp:TextBox ID="txtFax" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>             
            <tr>
                <td colspan="4"><asp:ValidationSummary ID="vldSummary2" ForeColor="#FF7300" runat="server" />
                    <asp:Label ID="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
                </td>
            </tr>
           <tr>
                <td colspan="4" style="background-color:#ffffff;color:#78BDE8; font-size:14pt;font-weight: bold; height:35px; vertical-align:bottom">
                    Revised
                </td>
            </tr>
            <tr>
                <td>Effective Date: *</td>
                <td><uc1:RequiredDateTimeControl ID="txtEffectiveDate" DateControlTextBoxWidthInPixels="100" runat="server" /></td>
            </tr>
            <tr>
                <td>Last Name: *</td>
                <td><asp:TextBox ID="txtRevisedLastName" Font-Size="8pt" MaxLength="50" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txtRevisedLastName" ErrorMessage="Revised Last Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Given First Name: *</td>
                <td><asp:TextBox ID="txtRevisedFirstName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                        ControlToValidate="txtFirstName" ErrorMessage="Revised Given First Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Middle Name:</td>
                <td><asp:TextBox ID="txtRevisedMiddleName" MaxLength="25" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Address 1: *</td>
                <td><asp:TextBox ID="txtRevisedAddress1" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                        ControlToValidate="txtRevisedAddress1" ErrorMessage="Revised Address 1 is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Address 2:</td>
                <td><asp:TextBox ID="txtRevisedAddress2" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>City: *</td>
                <td><asp:TextBox ID="txtRevisedCity" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                        ControlToValidate="txtRevisedCity" ErrorMessage="Revised City is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>State: *</td>
                <td><asp:DropDownList ID="ddlRevisedState" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                        ControlToValidate="ddlRevisedState" ErrorMessage="Revised State is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Zip Code: *</td>
                <td><asp:TextBox ID="txtRevisedZipCode" MaxLength="20" Font-Size="8pt" runat="server" Width="125px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                        ControlToValidate="txtRevisedZipCode" ErrorMessage="Revised Zip Code is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Personal E-Mail: *</td>
                <td><asp:TextBox ID="txtRevisedEmail" MaxLength="75" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                        ControlToValidate="txtRepEmail" ErrorMessage="Email is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                        ControlToValidate="txtRepEmail" runat="server" 
                        ErrorMessage="Email format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
                <td>Home Phone:</td>
                <td><asp:TextBox ID="txtRevisedHomePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                <td>Office Phone: </td>
                <td><asp:TextBox ID="txtRevisedOfficePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Mobile Phone: *</td>
                <td><asp:TextBox ID="txtRevisedMobile" MaxLength="30" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                        ControlToValidate="txtRevisedMobile" ErrorMessage="Revised Mobile Phone is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Fax:</td>
                <td><asp:TextBox ID="txtRevisedFax" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>               
            <tr>
                <td valign="top">Comments: </td>
                <td colspan="5"><asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="3"  
                        Font-Size="8pt" runat="server" Width="500px"></asp:TextBox>
                </td>
            </tr>
       </table>
        <table width="960">
            <tr>
                <td style="width:33%; text-align:center"><asp:Button ID="btnSubmit" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" /></td>
                <td style="width:33%; text-align:center"><asp:Button ID="btnClear" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Clear" 
                        onclick="btnClear_Click" /></td>
                <td style="width:34%; text-align:center"><asp:Button ID="btnMenu" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Menu" 
                        onclick="btnMenu_Click" /></td>
            </tr>
        </table>
        <asp:Literal ID="ltlEmail" runat="server"></asp:Literal>
    </div>
</asp:Content>
