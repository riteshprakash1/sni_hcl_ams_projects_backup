﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewSalesRep.aspx.cs" Inherits="SalesRepForm.NewSalesRep" MasterPageFile="MasterRep.Master" Title="New Sales/Service Rep Induction" %>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
         <table style="width:100%">
            <tr>
                <td class="titleheader">New Sales/Service Rep Information</td>
                <td>&nbsp;</td>
            </tr>
             <tr>
                <td colspan="2"><asp:ValidationSummary ID="vldSummary2" ForeColor="#FF7300" runat="server" />
                    <asp:Label ID="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
                </td>
            </tr>
       </table>
        <table style="width:100%">
            <tr>
                <asp:HiddenField ID="hdnChangeType" Value="New Sales/Service Rep Induction" runat="server" />
                <td>Relationship: *&nbsp;
                    <asp:RequiredFieldValidator ID="vldRequiredRelationship" runat="server"  
                        ErrorMessage="Relationship is required." ControlToValidate="rdbRelationship">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:RadioButtonList ID="rdbRelationship" runat="server" RepeatColumns="3" 
                        RepeatDirection="Horizontal" Width="200px">
                        <asp:ListItem>Employee</asp:ListItem>
                        <asp:ListItem>1099</asp:ListItem>
                    </asp:RadioButtonList>
               </td>
               <td>&nbsp;</td>
            </tr>           
            <tr>
                <td style="width:13%">Type: *</td>
                <td style="width:29%"><asp:DropDownList ID="ddlRepType" Font-Size="8pt" 
                        runat="server" Width="200px">
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                        ControlToValidate="ddlRepType" ErrorMessage="Rep Type is required.">*</asp:RequiredFieldValidator>
               </td>
                <td style="width:14%">Effective Date: *</td>
                <td><uc1:RequiredDateTimeControl ID="txtEffectiveDate" DateControlTextBoxWidthInPixels="100" runat="server" /></td>
            </tr>
            <tr>
                <td>GBU Product Line: *</td>
                <td><asp:DropDownList ID="ddlGbuProduct" Font-Size="8pt" runat="server" 
                        Width="200px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                        ControlToValidate="ddlGbuProduct" ErrorMessage="GBU Product Line is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Employee ID:</td>
                <td><asp:TextBox ID="txtEmployeeID" Font-Size="8pt" MaxLength="25" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Last Name: *</td>
                <td><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtLastName" ErrorMessage="Last Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Given First Name: *</td>
                <td><asp:TextBox ID="txtFirstName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtFirstName" ErrorMessage="Given First Name is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Middle Name:</td>
                <td><asp:TextBox ID="txtMiddleName" MaxLength="25" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                <td>Preferred First Name:</td>
                <td><asp:TextBox ID="txtPreferredName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Address 1: *</td>
                <td><asp:TextBox ID="txtAddress1" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtAddress1" ErrorMessage="Address 1 is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Address 2:</td>
                <td><asp:TextBox ID="txtAddress2" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>City: *</td>
                <td><asp:TextBox ID="txtCity" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtCity" ErrorMessage="City is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>State: *</td>
                <td><asp:DropDownList ID="ddlState" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="ddlState" ErrorMessage="State is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Zip Code: *</td>
                <td><asp:TextBox ID="txtZipCode" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                        ControlToValidate="txtZipCode" ErrorMessage="Zip Code is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Home Phone:</td>
                <td><asp:TextBox ID="txtHomePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Office Phone: </td>
                <td><asp:TextBox ID="txtOfficePhone" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td>Fax:</td>
                <td><asp:TextBox ID="txtFax" MaxLength="30" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Mobile Phone: *</td>
                <td><asp:TextBox ID="txtMobile" MaxLength="30" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                        ControlToValidate="txtMobile" ErrorMessage="Mobile Phone is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Sales Rep Being Assisted by:</td>
                <td><asp:TextBox ID="txtAssistantName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Personal E-Mail: *</td>
                <td><asp:TextBox ID="txtRepEmail" MaxLength="75" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                        ControlToValidate="txtRepEmail" ErrorMessage="Email is required.">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="vldEmailFormat" 
                        ControlToValidate="txtRepEmail" runat="server" 
                        ErrorMessage="Email format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
                <td>Cost Center:</td>
                <td><asp:TextBox ID="txtCostCenter" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Territory/District Mgr: *</td>
                <td><asp:TextBox ID="txtManager" MaxLength="50" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                        ControlToValidate="txtManager" ErrorMessage="Territory/District Mgr is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Transfer From S&amp;N Role: *</td>
                <td><asp:DropDownList ID="ddlTransferRole" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                        ControlToValidate="ddlTransferRole" ErrorMessage="Transfer From S&N Role is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Sales District/<br />Territory Name: *</td>
                <td><asp:TextBox ID="txtDistrictName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txtDistrictName" ErrorMessage="Sales District/Territory Name is required.">*</asp:RequiredFieldValidator></td>
                <td>Sales District/<br />Territory Number: *</td>
                <td><asp:TextBox ID="txtDistrictID" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                        ControlToValidate="txtDistrictID" ErrorMessage="Sales District/Territory Number is required.">*</asp:RequiredFieldValidator></td>
           </tr>
            <tr>
                <td>Sales Rep ID:</td>
                <td><asp:TextBox ID="txtSalesRepID" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                <td>S&N E-Mail: *</td>
                <td><asp:TextBox ID="txtSNEmail" MaxLength="75" Font-Size="8pt" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldRequiredSNEMail" runat="server" 
                        ControlToValidate="txtSNEmail" ErrorMessage="S&N Email is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="vldSNEmailFormat" 
                        ControlToValidate="txtSNEmail" runat="server" 
                        ErrorMessage="S&N Email format is not correct." 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
            </tr>
           <tr>
                <td>Prior Experience: *</td>
                <td><asp:DropDownList ID="ddlPriorExperience" Font-Size="8pt" runat="server" Width="200px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem Value="0" Text="0"></asp:ListItem>
                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                        <asp:ListItem Value="1-3" Text="1-3"></asp:ListItem>
                        <asp:ListItem Value="3-5" Text="3-5"></asp:ListItem>
                        <asp:ListItem Value="5+" Text="5+"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                        ControlToValidate="ddlPriorExperience" ErrorMessage="Prior Experience is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>Prior S&amp;N Experience: *</td>
                <td><asp:DropDownList ID="ddlPriorSNExperience" Font-Size="8pt" runat="server" Width="175px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                        ControlToValidate="ddlPriorSNExperience" ErrorMessage="Prior S&N Experience is required.">*</asp:RequiredFieldValidator>
                </td>
           </tr>
           <tr>
                <td style="vertical-align:top">Experience Comments: *</td>
                <td colspan="3"><asp:TextBox ID="txtExperienceComments" TextMode="MultiLine" 
                        Rows="2"  Font-Size="8pt" runat="server" Width="630px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" 
                        ControlToValidate="txtExperienceComments" ErrorMessage="Experience Comments required.">*</asp:RequiredFieldValidator>
                </td>
           </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td class="titleheader">Other Requests</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td style="width:17%">Sales Life On-line:</td>
                <td style="width:12%"><asp:DropDownList ID="ddlOrthoOnline" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width:12%">Ortho On-line Service:</td>
                <td><asp:DropDownList ID="ddlOrthoOnlineService" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Sales Reporting Service:</td>
                <td><asp:DropDownList ID="ddlSalesReporting" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>Learn Center Access:</td>
                <td><asp:DropDownList ID="ddlLearnCenter" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Kronos:</td>
                <td><asp:DropDownList ID="ddlKronos" Font-Size="8pt" runat="server" Width="70px">
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td style="font-size:9pt">Any Technology Requests – please ensure these are requested through the  
                    <asp:Label ID="lblServicePortalName" runat="server"></asp:Label> 
                    (<asp:HyperLink ID="lnkServicePortal" Target="_blank" CssClass="ServicePortal" runat="server">HyperLink</asp:HyperLink>)</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td colspan="4"><asp:CheckBox ID="chkContractSigned" runat="server" Text="I verify that above rep's contract (1099) or offer letter (employee) has been signed and is on file with corporate." />
                    <asp:CustomValidator ID="vldContractSigned" runat="server" ErrorMessage="I certify that if rep is an independent (1099) District rep that all background check have been completed.">*</asp:CustomValidator>
                </td>
            </tr>
        </table>
        <table style="width:960">
            <tr>
                <td style="width:25%; text-align:center"><asp:Button ID="btnSubmitNndChange" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                        Text="Submit & Change Acct" onclick="btnSubmitNndChange_Click" /></td>
                <td style="width:25%; text-align:center"><asp:Button ID="btnSubmit" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" /></td>
                <td style="width:25%; text-align:center"><asp:Button ID="btnClear" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Clear" 
                        onclick="btnClear_Click" /></td>
                <td style="width:25%; text-align:center"><asp:Button ID="btnMenu" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Menu" 
                        onclick="btnMenu_Click" /></td>
            </tr>
        </table>
        <asp:Literal ID="ltlEmail" runat="server"></asp:Literal>
    </div>

</asp:Content>