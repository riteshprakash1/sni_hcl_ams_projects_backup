﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RepStatusChange.aspx.cs" Inherits="SalesRepForm.RepStatusChange" MasterPageFile="MasterRep.Master" Title="Rep Termination/Status Change"%>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table width="100%" border="0">
            <tr>
                <td class="titleheader">Sales Rep Information</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td colspan="4"><asp:ValidationSummary ID="vldSummary2" ForeColor="#FF7300" runat="server" />
                    <asp:Label ID="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
                    <asp:HiddenField ID="hdnNonEmployeeID" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnChangeType" Value="Rep Termination" runat="server" />
               </td>
            </tr>
             <tr>
                <td>Change Type: *</td>
                <td><asp:DropDownList ID="ddlChangeType" Font-Size="8pt" runat="server" 
                        Width="200px" onselectedindexchanged="ddlChangeType_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem Value="Terminate" Text="Terminate"></asp:ListItem>
                        <asp:ListItem Value="Change/Transfer" Text="Change/Transfer"></asp:ListItem>
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="vldChangeType" runat="server" 
                        ControlToValidate="ddlChangeType" ErrorMessage="Change Type is required.">*</asp:RequiredFieldValidator>
               </td>
            </tr>
           <tr>
                <td style="width:15%">Last Name: *</td>
                <td style="width:27%"><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" runat="server" 
                        Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtLastName" ErrorMessage="Last Name is required.">*</asp:RequiredFieldValidator>
                </td>
                <td style="width:14%">Given First Name: *</td>
                <td><asp:TextBox ID="txtFirstName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtFirstName" ErrorMessage="Given First Name is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>Middle Name:</td>
                <td><asp:TextBox ID="txtMiddleName" MaxLength="25" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
                 <td>Effective Date: *</td>
                <td><uc1:RequiredDateTimeControl ID="txtEffectiveDate" DateControlTextBoxWidthInPixels="100" runat="server" /></td>
           </tr>
           <tr>
                <td>Territory/District Mgr:</td>
                <td><asp:TextBox ID="txtManager" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
           </tr>
            <tr>
                <td><asp:Label ID="lblStayWithSN" runat="server" Text="Staying with S&N: *"></asp:Label></td>
                <td><asp:DropDownList ID="ddlStayWithSN" Font-Size="8pt" runat="server" Width="200px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldStayWithSN" runat="server" 
                        ControlToValidate="ddlStayWithSN" ErrorMessage="Staying with S&N is required.">*</asp:RequiredFieldValidator>
                </td>
                <td><asp:Label ID="lblGBUTransfer" runat="server" Text="GBU Transfer: *"></asp:Label></td>
                <td><asp:DropDownList ID="ddlGBUTransfer" Font-Size="8pt" runat="server" Width="200px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldGBUTransfer" runat="server" 
                        ControlToValidate="ddlGBUTransfer" ErrorMessage="GBU Transfer is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblDeleteSystemAccess" runat="server" Text="Delete System Access: *"></asp:Label></td>
                <td><asp:DropDownList ID="ddlDeleteSystemAccess" Font-Size="8pt" runat="server" Width="200px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>Othro</asp:ListItem>
                        <asp:ListItem>Endo</asp:ListItem>
                        <asp:ListItem>All</asp:ListItem>
                        <asp:ListItem>None</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldDeleteSystemAccess" runat="server" 
                        ControlToValidate="ddlDeleteSystemAccess" ErrorMessage="Delete System Access is required.">*</asp:RequiredFieldValidator>
                </td>
                <td><asp:Label ID="lblUserName" runat="server" Text="User Name/ ID: *"></asp:Label></td>
                <td><asp:TextBox ID="txtUserName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldUserName" runat="server" 
                        ControlToValidate="txtUserName" ErrorMessage="User Name/ ID is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblTransferType" runat="server" Text="Transfer Type: *"></asp:Label></td>
                <td><asp:DropDownList ID="ddlTransferType" Font-Size="8pt" runat="server" Width="200px">
                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        <asp:ListItem>1099 to Employee</asp:ListItem>
                        <asp:ListItem>Employee to 1099</asp:ListItem>
                        <asp:ListItem>Service to Sales</asp:ListItem>
                        <asp:ListItem>Product Line</asp:ListItem>
                        <asp:ListItem>Non-Rep to Rep</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldTransferType" runat="server" 
                        ControlToValidate="ddlTransferType" ErrorMessage="Transfer Type is required.">*</asp:RequiredFieldValidator>
                </td>
                <td><asp:Label ID="lblCompensation" runat="server" Text="Compensation:"></asp:Label></td>
                <td><asp:TextBox ID="txtCompensation" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblTransferFrom" runat="server" Text="Transfer From:"></asp:Label></td>
                <td><asp:TextBox ID="txtTransferFrom" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td><asp:Label ID="lblTransferTo" runat="server" Text="Transfer To:"></asp:Label></td>
                <td><asp:TextBox ID="txtTransferTo" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>            
            
            <tr>
                <td valign="top">Comments: </td>
                <td colspan="3"><asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="3"  
                        Font-Size="8pt" runat="server" Width="600px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table width="960">
            <tr>
                <td style="width:25%; text-align:center"><asp:Button ID="btnSubmitNndChange" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" 
                        Text="Submit & Change Acct" onclick="btnSubmitNndChange_Click" /></td>
                <td style="width:25%; text-align:center"><asp:Button ID="btnSubmit" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" /></td>
                <td style="width:25%; text-align:center"><asp:Button ID="btnClear" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Clear" 
                        onclick="btnClear_Click" /></td>
                <td style="width:25%; text-align:center"><asp:Button ID="btnMenu" 
                        CausesValidation="false" CssClass="buttonsSubmit" runat="server" Text="Menu" 
                        onclick="btnMenu_Click" /></td>
            </tr>
        </table>
         <asp:Literal ID="ltlEmail" runat="server"></asp:Literal>
   </div>
</asp:Content>