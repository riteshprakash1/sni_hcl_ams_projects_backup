﻿// ------------------------------------------------------------------
// <copyright file="ProcessRunner.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------


namespace SynchOldCrystalUsers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    
    class ProcessRunner
    {
        /// <summary>Initializes a new instance of the ProcessRunner class.</summary>
        public ProcessRunner()
        {

        }


        public void ExecuteSynch()
        {

            string strMethodCalled = "************* Started ExecuteSynch *************";
            try
            {
                ProcessRunner.WriteToLogFile(strMethodCalled, "DailyLogFile");

                SynchEntity newSynch = SynchEntity.InsertSynchOldCrystal(new SynchEntity("Starting"));
                SynchEntity lastSynch = SynchEntity.GetLastSuccessfulSynch();

                Console.WriteLine("newSynch ID: " + newSynch.SynchID.ToString());
                Console.WriteLine("Last Date: " + lastSynch.EndDate);

                List<AppUsersEntity> users = AppUsersEntity.GetAllUsersSinceLastSynchDate(lastSynch.EndDate);
                Console.WriteLine("Synch User Count: " + users.Count.ToString());
                ProcessRunner.WriteToLogFile("Users to Synch: " + users.Count.ToString(), "DailyLogFile");

                List<OldCrystalUserEntity> oldUsers = new List<OldCrystalUserEntity>();

                int intTestCounter = 0;
                foreach (AppUsersEntity u in users)
                {
                    ADUserEntity userAD = ADUtility.GetADUserByUserName(u.UserName);
                    OldCrystalUserEntity old = new OldCrystalUserEntity();
                    old.CreatedBy = u.CreatedBy;
                    old.CreatedDate = u.CreateDate;

                    if (u.RoleID == 2 || u.RoleID == 3 || u.RoleID == 4)
                        old.Role = u.RoleName;
                    else
                        old.Role = "Administrator";

                    old.UserID = u.UserID;
                    old.UserName = u.UserName;

                 //   if (u.RoleID == 2 || u.RoleID == 3)
                        old.SnID = this.GetTerritoryList(u.UserID);

                    if (u.UserStatus == "Active")
                        old.Status = "Active";
                    else
                        old.Status = "Inactive";

                    if (userAD != null)
                    {
                        old.Email = userAD.Email;
                        old.FirstName = userAD.FirstName;
                        old.LastName = userAD.LastName;
                        old.Psw = userAD.FirstName.Substring(0, 1) + userAD.LastName.Substring(0, 1) + u.UserID.ToString();
                    }
                    else
                    {
                        old.Status = "Inactive";
                        old.SnID = string.Empty;
                        old.UserID = 0;
                    }

                    oldUsers.Add(old);

                    intTestCounter++;
                    if (intTestCounter > 25)
                        break;
                }

                //this.gvOldCRystal.DataSource = oldUsers;
                //this.gvOldCRystal.DataBind();
                ProcessRunner.WriteToLogFile("Starting Update of Old Crystal Users", "DailyLogFile");
                
                newSynch.RecordsUpdated = this.UpdateOldCrystalUsers(oldUsers);
                newSynch.SynchStatus = "Completed Successfully";

                Console.WriteLine("newSynch ID: " + newSynch.SynchID.ToString() + "; RecordsUpdated: " + newSynch.RecordsUpdated.ToString());

                int synchUpdated = SynchEntity.UpdateSynchOldCrystal(newSynch);
                ProcessRunner.WriteToLogFile("Update of Old Crystal Users Completed; " + synchUpdated.ToString() + " records updated", "DailyLogFile");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                ProcessRunner.WriteToLogFile("Error: " + ex.Message, "DailyLogFile");
                LogException.HandleException(ex);
            }
        }

        /// <summary>Opens the log file, writes the message, then closes the file.</summary>
        /// <param name="strMsg">The message.</param>
        /// <param name="strLogFile">The name of the log file to write to.</param>
        public static void WriteToLogFile(string strMsg, string strLogFile)
        {
            string strFileName = ConfigurationManager.AppSettings[strLogFile];
            try
            {
                System.IO.FileInfo fileLog = new System.IO.FileInfo(strFileName);
                System.IO.StreamWriter str = fileLog.AppendText();

                string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
                str.WriteLine(strTime + strMsg + "\n");
                str.Flush(); //// close log file
                str.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Writing Log: " + ex.Message);
                LogException.HandleException(ex);
            }
        } //// END WriteToLogFile


        /// <summary>Loops thru the List of OldCrystalUserEntities and updates them in the Old Crystal Database</summary>
        /// <param name="oldUsers">The OldCrystalUserEntity List.</param>
        /// <returns>records update count</returns>
        private int UpdateOldCrystalUsers(List<OldCrystalUserEntity> oldUsers)
        {
            int intTotalRecordsUpdated = 0;
            SqlConnection sqlConn = ProcessRunner.OpenSqlConnection("OldCrystal");

       //     ProcessRunner.WriteToLogFile("Starting Changes Update", "ChangeLogFile");

            foreach (OldCrystalUserEntity u in oldUsers)
            {
                Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

                myParameters.Add(new SqlParameter("@CreatedBy", u.CreatedBy));
                myParameters.Add(new SqlParameter("@CreatedDate", u.CreatedDate));
                myParameters.Add(new SqlParameter("@Dept", u.Dept));
                myParameters.Add(new SqlParameter("@Email", u.Email));
                myParameters.Add(new SqlParameter("@FirstName", u.FirstName));
                myParameters.Add(new SqlParameter("@LastName", u.LastName));
                myParameters.Add(new SqlParameter("@Psw", u.Psw));
                myParameters.Add(new SqlParameter("@Role", u.Role));
                myParameters.Add(new SqlParameter("@SnID", u.SnID));
                myParameters.Add(new SqlParameter("@Status", u.Status));
                myParameters.Add(new SqlParameter("@UserName", u.UserName));
                myParameters.Add(new SqlParameter("@userId", u.UserID));

                int intRecordsUpdated = ProcessRunner.SqlExecuteNonQuery("sp_SynchCryUser", myParameters, sqlConn);

                string strMsg = "Updated User: " + u.LastName + ", " + u.FirstName + "; Username: " + u.UserName;
                ProcessRunner.WriteToLogFile(strMsg, "ChangeLogFile");

                Console.WriteLine(strMsg);

                intTotalRecordsUpdated += intRecordsUpdated;
            }

            ProcessRunner.CloseSqlConnection(sqlConn);

            return intTotalRecordsUpdated;

        }

        /// <summary>Queries the database to get the Territory records for the userID supplied and returns a string with all the territories delimited with a comma</summary>
        /// <param name="intUserID">The UserID.</param>
        /// <returns>comma delimited territory string</returns>
        private string GetTerritoryList(int intUserID)
        {
            List<UserTerritoryEntity> tList = UserTerritoryEntity.GetUserTerritoriesByUserId(intUserID);

            string strTerritoryList = string.Empty;
            foreach (UserTerritoryEntity t in tList)
            {
                strTerritoryList += t.Territory + ",";
            }

            strTerritoryList = strTerritoryList.TrimEnd(',', ' ');

            return strTerritoryList;
        }


        /// <summary>Execute non query and return the parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static Collection<SqlParameter> SqlExecuteNonQueryReturnParameters(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection SqlConn = ProcessRunner.OpenSqlConnection();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            ProcessRunner.CloseSqlConnection(SqlConn);

            return parameters;
        }

        /// <summary>Execute non query and return the number of updated records.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static int SqlExecuteNonQueryCount(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection SqlConn = ProcessRunner.OpenSqlConnection();
            SqlTransaction myTransaction = SqlConn.BeginTransaction();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn, myTransaction);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            myTransaction.Commit();

            ProcessRunner.CloseSqlConnection(SqlConn);

            return intCommandOutcome;
        }

        /// <summary>Execute non query and return the parameter collection used to get values in Output parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="SqlConn">The SqlConnection.</param>
        /// <returns>The number of records affected</returns>
        public static int SqlExecuteNonQuery(string storedProcedureName, Collection<SqlParameter> parameters, SqlConnection SqlConn)
        {
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            return intCommandOutcome;
        }

        /// <summary>Executes a query.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            DataTable myDataTable = new DataTable();
            SqlConnection SqlConn = ProcessRunner.OpenSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = SqlConn;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            ProcessRunner.CloseSqlConnection(SqlConn);

            return myDataTable;
        }

        /// <summary>Opens a new SQL connection.</summary>
        /// <returns>Returns a SQL connection</returns>
        public static SqlConnection OpenSqlConnection()
        {
            SqlConnection myConnection = new SqlConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["sqlConn"].ToString();
                myConnection.Open();
            }

            return myConnection;

        }

        /// <summary>Opens a new SQL connection.</summary>
        /// <param name="strConnName">The connection string name</param>
        /// <returns>Returns a SQL connection</returns>
        public static SqlConnection OpenSqlConnection(string strConnName)
        {
            SqlConnection myConnection = new SqlConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings[strConnName].ToString();
                myConnection.Open();
            }

            return myConnection;

        }

        /// <summary>Closes the SQL connection.</summary>
        /// <param name="connection">The SQL connection.</param>
        public static void CloseSqlConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            else if (connection.State == ConnectionState.Broken)
            {
                connection.Close();
            }
        }

    } //// end class
}
