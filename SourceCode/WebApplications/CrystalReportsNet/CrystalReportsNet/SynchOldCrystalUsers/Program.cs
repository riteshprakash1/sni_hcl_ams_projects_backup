﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SynchOldCrystalUsers
{
    class Program
    {
        static void Main(string[] args)
        {
            bool blnRunProgram = Convert.ToBoolean(ConfigurationManager.AppSettings["RunSynchProgram"].ToString());
            ProcessRunner pr = new ProcessRunner();
            if(blnRunProgram == true)
                pr.ExecuteSynch();
            else
                ProcessRunner.WriteToLogFile("RunSynchProgram turned off in config file", "DailyLogFile");

        }
    }
}
