﻿// ------------------------------------------------------------------
// <copyright file="OldCrystalUserEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SynchOldCrystalUsers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary>This is the SynchEntity Entity.</summary>
    public class OldCrystalUserEntity
    {
        // firstName, lastName, userName, psw, dept, snid, status, email, createdBy, createdDate, role, userID

        /// <summary>This is the FirstName.</summary>
        private string strFirstName;

        /// <summary>This is the LastName.</summary>
        private string strLastName;

        /// <summary>This is the UserName.</summary>
        private string strUserName;

        /// <summary>This is the Psw.</summary>
        private string strPsw;

        /// <summary>This is the Dept.</summary>
        private string strDept;

        /// <summary>This is the SnID.</summary>
        private string strSnID;

        /// <summary>This is the Status.</summary>
        private string strStatus;

        /// <summary>This is the Email.</summary>
        private string strEmail;

        /// <summary>This is the CreatedBy.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreatedDate.</summary>
        private string strCreatedDate;

        /// <summary>This is the Role.</summary>
        private string strRole;

        /// <summary>This is the UserID.</summary>
        private int intUserID;

        /// <summary>Initializes a new instance of the SynchEntity class.</summary>
        public OldCrystalUserEntity()
        {
            this.UserID = 0;
            this.CreatedBy = string.Empty;
            this.CreatedDate = string.Empty;
            this.Dept = string.Empty;
            this.Email = string.Empty;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Psw = string.Empty;
            this.Role = string.Empty;
            this.SnID = string.Empty;
            this.Status = string.Empty;
            this.UserName = string.Empty;
        }


        /// <summary>Gets or sets the UserID.</summary>
        /// <value>The UserID.</value>
        public int UserID
        {
            get { return this.intUserID; }

            set { this.intUserID = value; }
        }

        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.</value>
        public string CreatedBy
        {
            get { return this.strCreatedBy; }

            set { this.strCreatedBy = value; }
        }

        /// <summary>Gets or sets the CreatedDate.</summary>
        /// <value>The CreatedDate.</value>
        public string CreatedDate
        {
            get { return this.strCreatedDate; }

            set { this.strCreatedDate = value; }
        }

        /// <summary>Gets or sets the Dept.</summary>
        /// <value>The Dept.</value>
        public string Dept
        {
            get { return this.strDept; }

            set { this.strDept = value; }
        }

        /// <summary>Gets or sets the Email.</summary>
        /// <value>The Email.</value>
        public string Email
        {
            get { return this.strEmail; }

            set { this.strEmail = value; }
        }

        /// <summary>Gets or sets the FirstName.</summary>
        /// <value>The FirstName.</value>
        public string FirstName
        {
            get { return this.strFirstName; }

            set { this.strFirstName = value; }
        }

        /// <summary>Gets or sets the LastName.</summary>
        /// <value>The LastName.</value>
        public string LastName
        {
            get { return this.strLastName; }

            set { this.strLastName = value; }
        }

        /// <summary>Gets or sets the Psw.</summary>
        /// <value>The Psw.</value>
        public string Psw
        {
            get { return this.strPsw; }

            set { this.strPsw = value; }
        }

        /// <summary>Gets or sets the Role.</summary>
        /// <value>The Role.</value>
        public string Role
        {
            get { return this.strRole; }

            set { this.strRole = value; }
        }

        /// <summary>Gets or sets the SnID.</summary>
        /// <value>The SnID.</value>
        public string SnID
        {
            get { return this.strSnID; }

            set { this.strSnID = value; }
        }

        /// <summary>Gets or sets the Status.</summary>
        /// <value>The Status.</value>
        public string Status
        {
            get { return this.strStatus; }

            set { this.strStatus = value; }
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get { return this.strUserName; }

            set { this.strUserName = value; }
        }

    }//// end class
}