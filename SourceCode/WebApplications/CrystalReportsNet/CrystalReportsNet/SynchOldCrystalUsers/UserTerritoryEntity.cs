﻿// ------------------------------------------------------------------
// <copyright file="UserTerritoryEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SynchOldCrystalUsers

{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the User Territory Entity. </summary>    
    public class UserTerritoryEntity
    {
        /// <summary>This is the UserTerritoryID.</summary>
        private int intUserTerritoryID;

        /// <summary>This is the userID.</summary>
        private int intUserID;

        /// <summary>This is the Territory.</summary>
        private string strTerritory;

        /// <summary>This is the Created By.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreatedDate.</summary>
        private string strCreatedDate;

        /// <summary>Initializes a new instance of the UserTerritoryEntity class.</summary>
        public UserTerritoryEntity()
        {
            this.Territory = String.Empty;
            this.CreatedBy = String.Empty;
            this.CreatedDate = String.Empty;
        }

        /// <summary>Gets or sets the UserTerritoryID.</summary>
        /// <value>The UserTerritoryID.</value>
        public int UserTerritoryID
        {
            get { return this.intUserTerritoryID; }
            set { this.intUserTerritoryID = value; }
        }

        /// <summary>Gets or sets the UserID.</summary>
        /// <value>The UserID.</value>
        public int UserID
        {
            get { return this.intUserID; }
            set { this.intUserID = value; }
        }

        /// <summary>Gets or sets the Territory.</summary>
        /// <value>The Territory.</value>
        public string Territory
        {
            get { return this.strTerritory; }
            set { this.strTerritory = value; }
        }

        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.</value>
        public string CreatedBy
        {
            get { return this.strCreatedBy; }
            set { this.strCreatedBy = value; }
        }

        /// <summary>Gets or sets the CreatedDate.</summary>
        /// <value>The CreatedDate.</value>
        public string CreatedDate
        {
            get { return this.strCreatedDate; }
            set { this.strCreatedDate = value; }
        }

        /// <summary>Receives a UserTerritory datarow and converts it to a UserTerritoryEntity.</summary>
        /// <param name="r">The UserTerritory DataRow.</param>
        /// <returns>UserTerritoryEntity</returns>
        protected static UserTerritoryEntity GetEntityFromDataRow(DataRow r)
        {
            UserTerritoryEntity t = new UserTerritoryEntity();

            if ((r["UserID"] != null) && (r["UserID"] != DBNull.Value))
                t.UserID = Convert.ToInt32(r["UserID"].ToString());

            if ((r["UserTerritoryID"] != null) && (r["UserTerritoryID"] != DBNull.Value))
                t.UserTerritoryID = Convert.ToInt32(r["UserTerritoryID"].ToString());

            t.Territory = r["Territory"].ToString();
            t.CreatedDate = r["CreatedDate"].ToString();
            t.CreatedBy = r["CreatedBy"].ToString();

            return t;
        }


        /// <summary>Gets all the UserTerritory List.</summary>
        /// <param name="userId">the userId</param>
        /// <returns>The newly populated UserTerritoryEntity List</returns>
        public static List<UserTerritoryEntity> GetUserTerritoriesByUserId(int userId)
        {
            List<UserTerritoryEntity> territories = new List<UserTerritoryEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userID", userId));

            DataTable dt = ProcessRunner.SqlExecuteQuery("sp_GetUserTerritoriesByUserId", parameters);
            //Debug.WriteLine("sp_GetUserTerritoriesByUserId UserTerritory Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserTerritoryEntity t = UserTerritoryEntity.GetEntityFromDataRow(r);
                territories.Add(t);
            }

            return territories;
        }

        /// <summary>Deletes the territory records by the userTerritoryId.</summary>
        /// <param name="userTerritoryId">the userTerritoryId</param>
        /// <param name="strModifiedBy">the Modified By User</param>
        /// <returns>the count of the records deleted</returns>
        public static int DeleteTerritoryByUserTerritoryId(int userTerritoryId, string strModifiedBy)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userTerritoryId", userTerritoryId));
            parameters.Add(new SqlParameter("@modifiedBY", strModifiedBy));

            int intRecordsDeleted = ProcessRunner.SqlExecuteNonQueryCount("sp_DeleteUserTerritoryByUserTerritoryId", parameters);

            return intRecordsDeleted;
        }

        /// <summary>Insert territory record</summary>
        /// <param name="intUserId">the UserId</param>
        /// <param name="strTerrirory">the Terrirory</param>
        /// <param name="strModifiedBy">the Modified By User</param>
        /// <returns>the count of the records inserted</returns>
        public static int InsertUserTerritory(int intUserId, string strTerrirory, string strModifiedBy)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userId", intUserId));
            parameters.Add(new SqlParameter("@territory", strTerrirory));
            parameters.Add(new SqlParameter("@modifiedBY", strModifiedBy));

            int intRecordsInserted = ProcessRunner.SqlExecuteNonQueryCount("sp_InsertUserTerritory", parameters);

            return intRecordsInserted;
        }

        ///// <summary>Delete all territories by userID then inserts the list of territories</summary>
        ///// <param name="territories">the List of UserTerritoryEntity objects</param>
        ///// <param name="intUserID">the UserID</param>
        ///// <returns>true/false as to whether the transaction was successful</returns>
        //public static bool UpdateUserTerritories(List<UserTerritoryEntity> territories, int intUserID)
        //{
        //    SqlConnection conn = SQLUtility.OpenSqlConnection();
        //    SqlTransaction trans = conn.BeginTransaction();

        //    bool updateGood = false;

        //    try
        //    {
        //        Collection<SqlParameter> myDeleteParameters = new Collection<SqlParameter>();
        //        myDeleteParameters.Add(new SqlParameter("@userId", intUserID));
        //        SqlParameterCollection deleteParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_DeleteUserTerritoriesByUserId", myDeleteParameters, conn, trans);

        //        foreach (UserTerritoryEntity t in territories)
        //        {
        //            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

        //            myParameters.Add(new SqlParameter("@userID", intUserID));
        //            myParameters.Add(new SqlParameter("@territory", t.Territory));

        //            SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertUserTerritory", myParameters, conn, trans);
        //        }

        //        trans.Commit();
        //        updateGood = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("Rollback UpdateUserTerritories: " + ex.Message);
        //        trans.Rollback();
        //    }

        //    SQLUtility.CloseSqlConnection(conn);
        //    Debug.WriteLine("updateGood: " + updateGood.ToString());
        //    return updateGood;
        //}


    }//// end class
}