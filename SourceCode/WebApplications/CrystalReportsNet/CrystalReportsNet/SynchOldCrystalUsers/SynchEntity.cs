﻿// ------------------------------------------------------------------
// <copyright file="SynchEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace SynchOldCrystalUsers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;


    /// <summary>This is the SynchEntity Entity.</summary>
    public class SynchEntity
    {
        /// <summary>This is the SynchID.</summary>
        private int intSynchID;

        /// <summary>This is the RecordsUpdated.</summary>
        private int intRecordsUpdated;

        /// <summary>This is the DisplayName.</summary>
        private string strSynchStatus;

        /// <summary>This is the StartDate.</summary>
        private string strStartDate;

        /// <summary>This is the EndDate.</summary>
        private string strEndDate;

        /// <summary>Initializes a new instance of the SynchEntity class.</summary>
        public SynchEntity()
        {
            this.StartDate = String.Empty;
            this.EndDate = String.Empty;
            this.SynchStatus = String.Empty;
            this.RecordsUpdated = 0;
            this.SynchID = 0;
        }

        /// <summary>Initializes a new instance of the SynchEntity class.</summary>
        /// <param name="strStatus">the Synch status</param>
        public SynchEntity(string strStatus)
        {
            this.StartDate = String.Empty;
            this.EndDate = String.Empty;
            this.SynchStatus = strStatus;
            this.RecordsUpdated = 0;
            this.SynchID = 0;
        }

        /// <summary>Gets or sets the SynchStatus.</summary>
        /// <value>The SynchStatus.</value>
        public string SynchStatus
        {
            get { return this.strSynchStatus; }

            set { this.strSynchStatus = value; }
        }

        /// <summary>Gets or sets the StartDate.</summary>
        /// <value>The StartDate.</value>
        public string StartDate
        {
            get { return this.strStartDate; }

            set { this.strStartDate = value; }
        }

        /// <summary>Gets or sets the EndDate.</summary>
        /// <value>The EndDate.</value>
        public string EndDate
        {
            get { return this.strEndDate; }

            set { this.strEndDate = value; }
        }

        /// <summary>Gets or sets the number of RecordsUpdated.</summary>
        /// <value>The RecordsUpdated.</value>
        public int RecordsUpdated
        {
            get { return this.intRecordsUpdated; }

            set { this.intRecordsUpdated = value; }
        }

        /// <summary>Gets or sets the number of SynchID.</summary>
        /// <value>The SynchID.</value>
        public int SynchID
        {
            get { return this.intSynchID; }

            set { this.intSynchID = value; }
        }

        /// <summary>Insert Synch record</summary>
        /// <param name="r">the SynchEntity object</param>
        /// <returns>the SynchEntity with the new ID</returns>
        public static SynchEntity InsertSynchOldCrystal(SynchEntity s)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            SqlParameter paramID = new SqlParameter("@SynchID", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            parameters.Add(paramID);

            parameters.Add(new SqlParameter("@synchStatus", s.SynchStatus));

            parameters = ProcessRunner.SqlExecuteNonQueryReturnParameters("sp_InsertSynchOldCrystal", parameters);
            s.SynchID = Convert.ToInt32(paramID.Value.ToString());

            return s;
        }

        /// <summary>Update Synch record</summary>
        /// <param name="r">the SynchEntity object</param>
        /// <returns>the number of records updated</returns>
        public static int UpdateSynchOldCrystal(SynchEntity s)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@synchID", s.SynchID));
            parameters.Add(new SqlParameter("@synchStatus", s.SynchStatus));
            parameters.Add(new SqlParameter("@recordsUpdated", s.RecordsUpdated));

            int recordsUpdated = ProcessRunner.SqlExecuteNonQueryCount("sp_UpdateSynchOldCrystal", parameters);

            return recordsUpdated;
        }



        /// <summary>Receives a Synch datarow and converts it to a SynchEntity.</summary>
        /// <param name="r">The Synch DataRow.</param>
        /// <returns>SynchEntity</returns>
        public static SynchEntity GetEntityFromDataRow(DataRow r)
        {
            SynchEntity stat = new SynchEntity();

            stat.SynchStatus = r["SynchStatus"].ToString();
            stat.EndDate = r["EndDate"].ToString();
            stat.StartDate = r["StartDate"].ToString();

            if ((r["SynchID"] != null) && (r["SynchID"] != DBNull.Value))
                stat.SynchID = Convert.ToInt32(r["SynchID"].ToString());

            if ((r["RecordsUpdated"] != null) && (r["RecordsUpdated"] != DBNull.Value))
                stat.RecordsUpdated = Convert.ToInt32(r["RecordsUpdated"].ToString());

            return stat;
        }

        /// <summary>Gets the Last Successful Synch record.</summary>
        /// <returns>The newly populated SynchEntity</returns>
        public static SynchEntity GetLastSuccessfulSynch()
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = ProcessRunner.SqlExecuteQuery("sp_GetLastSuccessfulSynch", parameters);

            SynchEntity s = new SynchEntity();

            if (dt.Rows.Count > 0)
            {
                s = SynchEntity.GetEntityFromDataRow(dt.Rows[0]);
            }
            else
                s.EndDate = DateTime.Now.AddDays(-2).ToShortDateString(); // +" " + DateTime.Now.AddMonths(-6).ToShortTimeString();

            return s;
        }

    } //// end class SynchEntity
}
