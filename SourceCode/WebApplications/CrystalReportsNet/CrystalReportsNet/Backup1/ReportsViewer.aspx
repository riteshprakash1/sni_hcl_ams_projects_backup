﻿<%@ Page  Title="Report Viewer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportsViewer.aspx.cs" Inherits="CrystalReportsNet.ReportsViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692FBEA5521E1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table style="width:1170px">
        <tr>
            <td><asp:HiddenField ID="hdnReportID" runat="server" /><asp:HiddenField ID="hdnReportTrackerID" runat="server" />
                <CR:CrystalReportViewer width="1160px" height="750px" ID="crViewer" BestFitPage="False" PageZoomFactor="79" 
                    HasCrystalLogo="False" runat="server" DisplayStatusbar="true" EnableParameterPrompt="true" HasDrillUpButton="true" HasGotoPageButton="True"
                     HasToggleParameterPanelButton="False" HasToggleGroupTreeButton="true" HasPageNavigationButtons="True" HasRefreshButton="false"   
                    EnableDrillDown="true" DisplayPage="true"
                    AutoDataBind="False" ReuseParameterValuesOnRefresh="true" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>


