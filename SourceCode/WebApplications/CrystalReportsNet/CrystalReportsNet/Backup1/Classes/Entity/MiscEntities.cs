﻿// ------------------------------------------------------------------
// <copyright file="MiscEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web.UI.WebControls;

    /// <summary>This is the Parameter Input Entity.</summary>
    public class InputEntity
    {
        /// <summary>This is the ParameterName.</summary>
        private string strParameterName;

        /// <summary>This is the Start Value.</summary>
        private string strStartValue;

        /// <summary>This is the End Value.</summary>
        private string strEndValue;

        /// <summary>This is the Discrete Or Range.</summary>
        private string strDiscreteOrRange;

        /// <summary>Initializes a new instance of the InputEntity class.</summary>
        public InputEntity()
        {
            this.ParameterName = String.Empty;
            this.DiscreteOrRange = String.Empty;
            this.EndValue = String.Empty;
            this.StartValue = String.Empty;
        }

        /// <summary>Gets or sets the name of the Parameter.</summary>
        /// <value>The name of the Parameter.</value>
        public string ParameterName
        {
            get { return this.strParameterName; }

            set { this.strParameterName = value; }
        }

        /// <summary>Gets or sets the name of the Start Value.</summary>
        /// <value>The name of the Start Value.</value>
        public string StartValue
        {
            get { return this.strStartValue; }

            set { this.strStartValue = value; }
        }

        /// <summary>Gets or sets the name of the End Value.</summary>
        /// <value>The name of the End Value.</value>
        public string EndValue
        {
            get { return this.strEndValue; }

            set { this.strEndValue = value; }
        }

        /// <summary>Gets or sets the Discrete Or Range indicator.</summary>
        /// <value>The Discrete Or Range indicator.</value>
        public string DiscreteOrRange
        {
            get { return this.strDiscreteOrRange; }

            set { this.strDiscreteOrRange = value; }
        }
    } //// end class InputEntity


    /// <summary>This is the Parameter DefaultEntry Entity.</summary>
    [Serializable]
    public class DefaultEntryEntity
    {
        /// <summary>This is the Default Value.</summary>
        private string strDefaultValue;

        /// <summary>This is the Default Description.</summary>
        private string strDefaultDescription;

        /// <summary>Initializes a new instance of the DefaultEntryEntity class.</summary>
        public DefaultEntryEntity()
        {
            this.DefaultValue = String.Empty;
            this.DefaultDescription = String.Empty;
        }

        /// <summary>Initializes a new instance of the DefaultEntryEntity class.</summary>
        /// <param name="strValue">The DefaultEntry Value.</param>
        /// <param name="strDesc">The DefaultEntry Description.</param>
        public DefaultEntryEntity(string strValue, string strDesc)
        {
            this.DefaultValue = strValue;
            this.DefaultDescription = strDesc;
        }

        /// <summary>Gets or sets the Default Value.</summary>
        /// <value>The Default Value.</value>
        public string DefaultValue
        {
            get { return this.strDefaultValue; }

            set { this.strDefaultValue = value; }
        }

        /// <summary>Gets or sets the Default Description.</summary>
        /// <value>The Default Description.</value>
        public string DefaultDescription
        {
            get { return this.strDefaultDescription; }

            set { this.strDefaultDescription = value; }
        }

    } //// end class DefaultEntryEntity


    /// <summary>This is the Stats Entity.</summary>
    public class StatsEntity
    {
        /// <summary>This is the Processings.</summary>
        private int intProcessings;

        /// <summary>This is the DisplayName.</summary>
        private string strDisplayName;

        /// <summary>This is the Header.</summary>
        private string strHeader;

        /// <summary>This is the PublishedName.</summary>
        private string strPublishedName;

        /// <summary>This is the LastRun.</summary>
        private string strLastRun;

        /// <summary>Initializes a new instance of the DefaultEntryEntity class.</summary>
        public StatsEntity()
        {
            this.DisplayName = String.Empty;
            this.Header = String.Empty;
            this.PublishedName = String.Empty;
            this.LastRun = String.Empty;
            this.Processings = 0;
        }

        /// <summary>Initializes a new instance of the StatsEntity class.</summary>
        /// <param name="strValue">The StatsEntity Value.</param>
        /// <param name="strDesc">The StatsEntity Description.</param>
        public StatsEntity(string strValue, string strDesc)
        {
            this.DisplayName = strValue;
            this.Processings = 0;
        }

        /// <summary>Gets or sets the DisplayName.</summary>
        /// <value>The DisplayName.</value>
        public string DisplayName
        {
            get { return this.strDisplayName; }

            set { this.strDisplayName = value; }
        }

        /// <summary>Gets or sets the Header.</summary>
        /// <value>The Header.</value>
        public string Header
        {
            get { return this.strHeader; }

            set { this.strHeader = value; }
        }

        /// <summary>Gets or sets the PublishedName.</summary>
        /// <value>The PublishedName.</value>
        public string PublishedName
        {
            get { return this.strPublishedName; }

            set { this.strPublishedName = value; }
        }

        /// <summary>Gets or sets the LastRun.</summary>
        /// <value>The LastRun.</value>
        public string LastRun
        {
            get { return this.strLastRun; }

            set { this.strLastRun = value; }
        }

        /// <summary>Gets or sets the number of Processings.</summary>
        /// <value>The Processings.</value>
        public int Processings
        {
            get { return this.intProcessings; }

            set { this.intProcessings = value; }
        }


        /// <summary>Receives a Stat datarow and converts it to a StatsEntity.</summary>
        /// <param name="r">The Stat DataRow.</param>
        /// <returns>StatsEntity</returns>
        public static StatsEntity GetEntityFromDataRow(DataRow r)
        {
          //  processings, max(CreateDate) as lastRun, DisplayName, header, publishedName
            StatsEntity stat = new StatsEntity();

            if ((r["processings"] != null) && (r["processings"] != DBNull.Value))
                stat.Processings = Convert.ToInt32(r["processings"].ToString());

            if ((r["lastRun"] != null) && (r["lastRun"] != DBNull.Value))
                stat.LastRun = r["lastRun"].ToString();

            if (r["DisplayName"] != null)
                stat.DisplayName = r["DisplayName"].ToString();

            if ((r["header"] != null) && (r["DisplayName"] != DBNull.Value))
                stat.Header = r["header"].ToString();

            if ((r["publishedName"] != null) && (r["DisplayName"] != DBNull.Value))
                stat.PublishedName = r["publishedName"].ToString();

            return stat;
        }

        /// <summary>Gets the Statistics based on the users input selections.</summary>
        /// <param name="strStartDate">The Start Date.</param>
        /// <param name="strEndDate">The End Date.</param>
        /// <param name="strReportRun">The Report Run.</param>
        /// <param name="strUserName">The UserName.</param>
        /// <returns>The newly populated StatsEntity List</returns>
        public static List<StatsEntity> GetStatistics(string strStartDate, string strEndDate, string strReportRun)
        {
            List<StatsEntity> stats = new List<StatsEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@startDate", strStartDate));
            parameters.Add(new SqlParameter("@endDate", strEndDate));
            parameters.Add(new SqlParameter("@reportRun", strReportRun));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetStatistics", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                StatsEntity row = StatsEntity.GetEntityFromDataRow(r);
                stats.Add(row);
            }

            return stats;
        }

    } //// end class StatsEntity



    /// <summary>This is the UserStatsEntity Entity.</summary>
    public class UserStatsEntity
    {
        /// <summary>This is the ReportTrackerId.</summary>
        private int intReportTrackerId;
        
        /// <summary>This is the ReportID.</summary>
        private int intReportID;

          /// <summary>This is the DisplayName.</summary>
        private string strDisplayName;

        /// <summary>This is the UserName.</summary>
        private string strUserName;

        /// <summary>This is the PublishedName.</summary>
        private string strReportName;

        /// <summary>This is the UserTerritories.</summary>
        private string strUserTerritories;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>Initializes a new instance of the UserStatsEntity class.</summary>
        public UserStatsEntity()
        {
            this.DisplayName = String.Empty;
            this.CreateDate = String.Empty;
            this.ReportName = String.Empty;
            this.UserName = String.Empty;
            this.UserTerritories = String.Empty;
            this.ReportID = 0;
            this.ReportTrackerId = 0;
        }

        /// <summary>Gets or sets the DisplayName.</summary>
        /// <value>The DisplayName.</value>
        public string DisplayName
        {
            get { return this.strDisplayName; }

            set { this.strDisplayName = value; }
        }

        /// <summary>Gets or sets the ReportName.</summary>
        /// <value>The ReportName.</value>
        public string ReportName
        {
            get { return this.strReportName; }

            set { this.strReportName = value; }
        }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.</value>
        public string CreateDate
        {
            get { return this.strCreateDate; }

            set { this.strCreateDate = value; }
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get { return this.strUserName; }

            set { this.strUserName = value; }
        }

        /// <summary>Gets or sets the UserTerritories.</summary>
        /// <value>The UserTerritories.</value>
        public string UserTerritories
        {
            get { return this.strUserTerritories; }

            set { this.strUserTerritories = value; }
        }

        /// <summary>Gets or sets the number of ReportID.</summary>
        /// <value>The ReportID.</value>
        public int ReportID
        {
            get { return this.intReportID; }

            set { this.intReportID = value; }
        }

        /// <summary>Gets or sets the number of ReportTrackerId.</summary>
        /// <value>The ReportTrackerId.</value>
        public int ReportTrackerId
        {
            get { return this.intReportTrackerId; }

            set { this.intReportTrackerId = value; }
        }


        /// <summary>Receives a UserStat datarow and converts it to a UserStatsEntity.</summary>
        /// <param name="r">The UserStat DataRow.</param>
        /// <returns>UserStatsEntity</returns>
        public static UserStatsEntity GetEntityFromDataRow(DataRow r)
        {
            //  processings, max(CreateDate) as lastRun, DisplayName, header, publishedName
            UserStatsEntity stat = new UserStatsEntity();

            stat.DisplayName = r["DisplayName"].ToString();
            stat.CreateDate = r["CreateDate"].ToString();
            stat.ReportName = r["ReportName"].ToString();
            stat.UserName = r["UserName"].ToString();
            stat.UserTerritories = r["UserTerritories"].ToString();


            if ((r["ReportID"] != null) && (r["ReportID"] != DBNull.Value))
                stat.ReportID = Convert.ToInt32(r["ReportID"].ToString());

            if ((r["ReportTrackerId"] != null) && (r["ReportTrackerId"] != DBNull.Value))
                stat.ReportTrackerId = Convert.ToInt32(r["ReportTrackerId"].ToString());


            return stat;
        }

        /// <summary>Gets the Statistics based on the users input selections.</summary>
        /// <param name="strStartDate">The Start Date.</param>
        /// <param name="strEndDate">The End Date.</param>
        /// <param name="strReportRun">The Report Run.</param>
        /// <param name="strUserName">The UserName.</param>
        /// <returns>The newly populated UserStatsEntity List</returns>
        public static List<UserStatsEntity> GetUserStatistics(string strStartDate, string strEndDate, string strReportTop, string strUserName)
        {
            List<UserStatsEntity> stats = new List<UserStatsEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@startDate", strStartDate));
            parameters.Add(new SqlParameter("@endDate", strEndDate));
            parameters.Add(new SqlParameter("@reportTop", strReportTop));
            parameters.Add(new SqlParameter("@userName", strUserName));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUserStatistics", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserStatsEntity row = UserStatsEntity.GetEntityFromDataRow(r);
                stats.Add(row);
            }

            return stats;
        }

    } //// end class UserStatsEntity



}//// END NameSpace