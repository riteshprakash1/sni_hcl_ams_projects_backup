﻿// ------------------------------------------------------------------
// <copyright file="ReportTrackerParameterEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Report Tracker Parameter Entity. </summary>  
    public class ReportTrackerParameterEntity
    {
        /// <summary>This is the ReportTrackerParameterID.</summary>
        private int intReportTrackerParameterID;

        /// <summary>This is the ReportTrackerID.</summary>
        private int intReportTrackerID;

        /// <summary>This is the Parameter Name.</summary>
        private string strParameterName;

        /// <summary>This is the Parameter Type.</summary>
        private string strParameterType;

        /// <summary>This is the Parameter Value.</summary>
        private string strParameterValue;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>Initializes a new instance of the ReportTrackerParameterEntity class.</summary>
        public ReportTrackerParameterEntity()
        {
            this.CreateDate = String.Empty;
            this.ParameterName = String.Empty;
            this.ParameterType = String.Empty;
            this.ParameterValue = String.Empty;
        }

        /// <summary>Gets or sets the ReportTrackerParameterID.</summary>
        /// <value>The ReportTrackerParameterID.</value>
        public int ReportTrackerParameterID
        {
            get { return this.intReportTrackerParameterID; }
            set { this.intReportTrackerParameterID = value; }
        }

        /// <summary>Gets or sets the ReportTrackerID.</summary>
        /// <value>The ReportTrackerID.</value>
        public int ReportTrackerID
        {
            get { return this.intReportTrackerID; }
            set { this.intReportTrackerID = value; }
        }

        /// <summary>Gets or sets the ParameterName.</summary>
        /// <value>The ParameterName.</value>
        public string ParameterName
        {
            get { return this.strParameterName; }
            set { this.strParameterName = value; }
        }

        /// <summary>Gets or sets the ParameterType.</summary>
        /// <value>The ParameterType.</value>
        public string ParameterType
        {
            get { return this.strParameterType; }
            set { this.strParameterType = value; }
        }

        /// <summary>Gets or sets the ParameterValue.</summary>
        /// <value>The ParameterValue.</value>
        public string ParameterValue
        {
            get { return this.strParameterValue; }
            set { this.strParameterValue = value; }
        }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.</value>
        public string CreateDate
        {
            get { return this.strCreateDate; }
            set { this.strCreateDate = value; }
        }

        /// <summary>Receives a ReportTrackerParameter datarow and converts it to a ReportTrackerParameterEntity.</summary>
        /// <param name="r">The ReportTrackerParameterEntity DataRow.</param>
        /// <returns>ReportTrackerParameterEntity</returns>
        protected static ReportTrackerParameterEntity GetEntityFromDataRow(DataRow r)
        {
            ReportTrackerParameterEntity re = new ReportTrackerParameterEntity();

            if ((r["ReportTrackerID"] != null) && (r["ReportTrackerID"] != DBNull.Value))
                re.ReportTrackerID = Convert.ToInt32(r["ReportTrackerID"].ToString());

            if ((r["ReportTrackerParameterID"] != null) && (r["ReportTrackerParameterID"] != DBNull.Value))
                re.ReportTrackerParameterID = Convert.ToInt32(r["ReportTrackerParameterID"].ToString());

            re.CreateDate = r["CreateDate"].ToString();
            re.ParameterName = r["ParameterName"].ToString();
            re.ParameterType = r["ParameterType"].ToString();
            re.ParameterValue = r["ParameterValue"].ToString();

            return re;
        }

        /// <summary>Gets the Report Tracker Parameter based on the ReportTrackerId.</summary>
        /// <param name="intReportTrackerID">The Report Tracker ID.</param>
        /// <returns>The newly populated ReportTrackerParameterEntity List</returns>
        public static List<ReportTrackerParameterEntity> GetReportTrackerParametersByReportTrackerId(int intReportTrackerID)
        {
            List<ReportTrackerParameterEntity> rptParams = new List<ReportTrackerParameterEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@ReportTrackerId", intReportTrackerID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetReportTrackerParametersByReportTrackerId", parameters);

            foreach (DataRow r in dt.Rows)
            {
                ReportTrackerParameterEntity row = ReportTrackerParameterEntity.GetEntityFromDataRow(r);
                rptParams.Add(row);
            }

            return rptParams;
        }

        /// <summary>Insert ReportTrackerParameter record</summary>
        /// <param name="r">the ReportTrackerParameterEntity object</param>
        /// <returns>the ReportTrackerParameterEntity with the new ID</returns>
        public static ReportTrackerParameterEntity InsertReportTrackerParameter(ReportTrackerParameterEntity r)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            SqlParameter paramID = new SqlParameter("@ReportTrackerParameterID", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            parameters.Add(paramID);
            
            parameters.Add(new SqlParameter("@ReportTrackerID", r.ReportTrackerID));
            parameters.Add(new SqlParameter("@ParameterName", r.ParameterName));
            parameters.Add(new SqlParameter("@ParameterType", r.ParameterType));
            parameters.Add(new SqlParameter("@ParameterValue", r.ParameterValue));

            parameters = SQLUtility.SqlExecuteNonQueryReturnParameters("sp_InsertReportTrackerParameter", parameters);
            r.ReportTrackerParameterID = Convert.ToInt32(paramID.Value.ToString());

            return r;
        }

    } //// end class
}