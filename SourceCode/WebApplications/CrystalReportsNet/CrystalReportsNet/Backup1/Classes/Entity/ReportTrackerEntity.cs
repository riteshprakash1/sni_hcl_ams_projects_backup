﻿// ------------------------------------------------------------------
// <copyright file="ReportTrackerEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Report Tracker Entity. </summary>  
    public class ReportTrackerEntity
    {
        /// <summary>This is the ReportTrackerID.</summary>
        private int intReportTrackerID;

        /// <summary>This is the ReportID.</summary>
        private int intReportID;
        
        /// <summary>This is the User Name.</summary>
        private string strUserName;

        /// <summary>This is the DisplayName.</summary>
        private string strDisplayName;

        /// <summary>This is the UserRole.</summary>
        private string strUserRole;

        /// <summary>This is the UserTerritories.</summary>
        private string strUserTerritories;

        /// <summary>This is the Server Name.</summary>
        private string strServerName;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>Initializes a new instance of the ReportTrackerEntity class.</summary>
        public ReportTrackerEntity()
        {
            this.CreateDate = String.Empty;
            this.ServerName = String.Empty;
            this.UserName = String.Empty;
            this.DisplayName = String.Empty;
            this.UserRole = String.Empty;
            this.UserTerritories = String.Empty;
        }

        /// <summary>Gets or sets the ReportTrackerID.</summary>
        /// <value>The ReportTrackerID.</value>
        public int ReportTrackerID
        {
            get { return this.intReportTrackerID; }
            set { this.intReportTrackerID = value; }
        }

        /// <summary>Gets or sets the ReportID.</summary>
        /// <value>The ReportID.</value>
        public int ReportID
        {
            get { return this.intReportID; }
            set { this.intReportID = value; }
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get { return this.strUserName; }
            set { this.strUserName = value; }
        }

        /// <summary>Gets or sets the DisplayName.</summary>
        /// <value>The DisplayName.</value>
        public string DisplayName
        {
            get { return this.strDisplayName; }
            set { this.strDisplayName = value; }
        }
        /// <summary>Gets or sets the UserRole.</summary>
        /// <value>The UserRole.</value>
        public string UserRole
        {
            get { return this.strUserRole; }
            set { this.strUserRole = value; }
        }
        /// <summary>Gets or sets the UserTerritories.</summary>
        /// <value>The UserTerritories.</value>
        public string UserTerritories
        {
            get { return this.strUserTerritories; }
            set { this.strUserTerritories = value; }
        }

        /// <summary>Gets or sets the ServerName.</summary>
        /// <value>The ServerName.</value>
        public string ServerName
        {
            get { return this.strServerName; }
            set { this.strServerName = value; }
        }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.</value>
        public string CreateDate
        {
            get { return this.strCreateDate; }
            set { this.strCreateDate = value; }
        }

        /// <summary>Receives a ReportTracker datarow and converts it to a ReportTrackerEntity.</summary>
        /// <param name="r">The ReportTrackerEntity DataRow.</param>
        /// <returns>ReportTrackerEntity</returns>
        protected static ReportTrackerEntity GetEntityFromDataRow(DataRow r)
        {
            ReportTrackerEntity re = new ReportTrackerEntity();

            if ((r["ReportTrackerID"] != null) && (r["ReportTrackerID"] != DBNull.Value))
                re.ReportTrackerID = Convert.ToInt32(r["ReportTrackerID"].ToString());

            if ((r["ReportID"] != null) && (r["ReportID"] != DBNull.Value))
                re.ReportID = Convert.ToInt32(r["ReportID"].ToString());

            re.CreateDate = r["CreateDate"].ToString();
            re.ServerName = r["ServerName"].ToString();
            re.UserName = r["UserName"].ToString();

            re.DisplayName = r["DisplayName"].ToString();
            re.UserRole = r["UserRole"].ToString();
            re.UserTerritories = r["UserTerritories"].ToString();

            return re;
        }

        /// <summary>Gets the Report Tracker based on the ReportTrackerId.</summary>
        /// <param name="intReportTrackerID">The Report Tracker ID.</param>
        /// <returns>The newly populated ReportTrackerEntity</returns>
        public static ReportTrackerEntity GetReportTrackerByReportTrackerId(int intReportTrackerID)
        {
            List<ReportTrackerParameterEntity> rptParams = new List<ReportTrackerParameterEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@ReportTrackerId", intReportTrackerID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetReportTrackerByReportTrackerId", parameters);

            ReportTrackerEntity rt = null;
            if (dt.Rows.Count > 0)
            {
                rt = ReportTrackerEntity.GetEntityFromDataRow(dt.Rows[0]);
            }

            return rt;
        }

        /// <summary>Insert ReportTracker record</summary>
        /// <param name="r">the ReportTrackerEntity object</param>
        /// <returns>the ReportTrackerEntity with the new ID</returns>
        public static ReportTrackerEntity InsertReportTracker(ReportTrackerEntity r)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            SqlParameter paramID = new SqlParameter("@ReportTrackerID", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            parameters.Add(paramID);
            
            parameters.Add(new SqlParameter("@ReportID", r.ReportID));
            parameters.Add(new SqlParameter("@ServerName", r.ServerName));
            parameters.Add(new SqlParameter("@UserName", r.UserName));
            parameters.Add(new SqlParameter("@DisplayName", r.DisplayName));
            parameters.Add(new SqlParameter("@UserRole", r.UserRole));
            parameters.Add(new SqlParameter("@UserTerritories", r.UserTerritories));

            parameters = SQLUtility.SqlExecuteNonQueryReturnParameters("sp_InsertReportTracker", parameters);
            r.ReportTrackerID = Convert.ToInt32(paramID.Value.ToString());

            return r;
        }

    } //// end class
}