﻿// ------------------------------------------------------------------
// <copyright file="GroupEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Group Entity. </summary>  
    public class GroupEntity
    {
        /// <summary>This is the GroupID.</summary>
        private int intGroupID;

        /// <summary>This is the Group Name.</summary>
        private string strGroupName;

        /// <summary>Initializes a new instance of the UserRolesEntity class.</summary>
        public GroupEntity()
        {
            this.GroupName = String.Empty;
        }

        /// <summary>Gets or sets the GroupID.</summary>
        /// <value>The GroupID.</value>
        public int GroupID
        {
            get { return this.intGroupID; }
            set { this.intGroupID = value; }
        }

        /// <summary>Gets or sets the GroupName.</summary>
        /// <value>The GroupName.</value>
        public string GroupName
        {
            get { return this.strGroupName; }
            set { this.strGroupName = value; }
        }


        /// <summary>Receives a Group datarow and converts it to a GroupEntity.</summary>
        /// <param name="r">The Group DataRow.</param>
        /// <returns>GroupEntity</returns>
        protected static GroupEntity GetEntityFromDataRow(DataRow r)
        {
            GroupEntity gu = new GroupEntity();

            if ((r["GroupID"] != null) && (r["GroupID"] != DBNull.Value))
                gu.GroupID = Convert.ToInt32(r["GroupID"].ToString());

            gu.GroupName = r["GroupName"].ToString();

            return gu;
        }

        /// <summary>Gets all the GroupEntity List.</summary>
        /// <returns>The newly populated GroupEntity List</returns>
        public static List<GroupEntity> GetGroupAllGroups()
        {
            List<GroupEntity> groups = new List<GroupEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAllGroups", parameters);
            Debug.WriteLine("group Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                GroupEntity myGroup = GroupEntity.GetEntityFromDataRow(r);
                groups.Add(myGroup);
            }

            return groups;
        }

        /// <summary>Gets groups not assigned to a reportID the GroupEntity List.</summary>
        /// <param name="intReportId">the reportId</param>
        /// <returns>The newly populated GroupEntity List</returns>
        public static List<GroupEntity> GetGroupsNotInByReportID(int intReportId)
        {
            List<GroupEntity> groups = new List<GroupEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@reportId", intReportId));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetGroupsNotInByReportID", parameters);

            foreach (DataRow r in dt.Rows)
            {
                GroupEntity myGroup = GroupEntity.GetEntityFromDataRow(r);
                groups.Add(myGroup);
            }

            return groups;
        }

        /// <summary>Deletes the group by the groupId.</summary>
        /// <param name="intGroupdID">the groupId</param>
        /// <returns>the count of the records deleted</returns>
        public static int DeleteGroupByGroupId(int intGroupdID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@groupId", intGroupdID));

            int intRecordsDeleted = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteGroup", parameters);

            return intRecordsDeleted;
        }

        /// <summary>Insert Group record</summary>
        /// <param name="strGroupName">the Group Name</param>
        /// <returns>the count of the records Inserted</returns>
        public static int InsertGroup(string strGroupName)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@groupName", strGroupName));

            int intRecordsInserted = SQLUtility.SqlExecuteNonQueryCount("sp_InsertGroup", parameters);

            return intRecordsInserted;
        }


    
    }//// end class
}