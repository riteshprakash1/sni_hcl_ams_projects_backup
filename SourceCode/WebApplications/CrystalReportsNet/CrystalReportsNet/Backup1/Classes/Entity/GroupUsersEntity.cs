﻿// ------------------------------------------------------------------
// <copyright file="GroupUsersEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Group Users Entity. </summary>  
    public class GroupUsersEntity
    {
        /// <summary>This is the GroupUserID.</summary>
        private int intGroupUserID;

        /// <summary>This is the GroupID.</summary>
        private int intGroupID;

        /// <summary>This is the Group Name.</summary>
        private string strGroupName;

        /// <summary>This is the UserID.</summary>
        private int intUserID;

        /// <summary>This is the User Name.</summary>
        private string strUserName;

        /// <summary>This is the Display Name.</summary>
        private string strDisplayName;

        /// <summary>Initializes a new instance of the UserRolesEntity class.</summary>
        public GroupUsersEntity()
        {
            this.GroupName = String.Empty;
            this.DisplayName = String.Empty;
            this.UserName = String.Empty;
        }

        /// <summary>Gets or sets the GroupUserID.</summary>
        /// <value>The GroupUserID.</value>
        public int GroupUserID
        {
            get { return this.intGroupUserID; }
            set { this.intGroupUserID = value; }
        }

        /// <summary>Gets or sets the GroupID.</summary>
        /// <value>The GroupID.</value>
        public int GroupID
        {
            get { return this.intGroupID; }
            set { this.intGroupID = value; }
        }

        /// <summary>Gets or sets the GroupName.</summary>
        /// <value>The GroupName.</value>
        public string GroupName
        {
            get { return this.strGroupName; }
            set { this.strGroupName = value; }
        }

        /// <summary>Gets or sets the UserID.</summary>
        /// <value>The UserID.</value>
        public int UserID
        {
            get { return this.intUserID; }
            set { this.intUserID = value; }
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get { return this.strUserName; }
            set { this.strUserName = value; }
        }

        /// <summary>Gets or sets the Display Name.</summary>
        /// <value>The Display Name.</value>
        public string DisplayName
        {
            get { return this.strDisplayName; }
            set { this.strDisplayName = value; }
        }

        /// <summary>Receives a GroupUsers datarow and converts it to a GroupUsersEntity.</summary>
        /// <param name="r">The GroupUsers DataRow.</param>
        /// <returns>GroupUsersEntity</returns>
        protected static GroupUsersEntity GetEntityFromDataRow(DataRow r)
        {
            GroupUsersEntity gu = new GroupUsersEntity();

            if ((r["GroupUserID"] != null) && (r["GroupUserID"] != DBNull.Value))
                gu.GroupUserID = Convert.ToInt32(r["GroupUserID"].ToString());

            if ((r["GroupID"] != null) && (r["GroupID"] != DBNull.Value))
                gu.GroupID = Convert.ToInt32(r["GroupID"].ToString());

            if ((r["UserID"] != null) && (r["UserID"] != DBNull.Value))
                gu.UserID = Convert.ToInt32(r["UserID"].ToString());

            gu.GroupName = r["GroupName"].ToString();
            gu.UserName = r["UserName"].ToString();
            gu.DisplayName = r["DisplayName"].ToString();

            return gu;
        }

        /// <summary>Gets all the GroupUsersEntity List.</summary>
        /// <returns>The newly populated GroupUsersEntity List</returns>
        public static List<GroupUsersEntity> GetGroupUsers()
        {
            List<GroupUsersEntity> groupUsers = new List<GroupUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAllGroupUsers", parameters);
            Debug.WriteLine("groupUsers Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                GroupUsersEntity myGroup = GroupUsersEntity.GetEntityFromDataRow(r);
                groupUsers.Add(myGroup);
            }

            return groupUsers;
        }

        /// <summary>Gets all the GroupUsersEntity List.</summary>
        /// <param name="grpUsers">the GroupUsersEntity List</param>
        /// <param name="strUsername">the username</param>
        /// <returns>The newly populated GroupUsersEntity List</returns>
        public static List<GroupUsersEntity> GetGroupsByUsername(List<GroupUsersEntity> grpUsers, string strUsername)
        {
            List<GroupUsersEntity> userGroups = grpUsers.FindAll(x => x.UserName == strUsername);
            return userGroups;
        }


        /// <summary>Gets all the GroupUsersEntity List.</summary>
        /// <param name="grpUsers">the GroupUsersEntity List</param>
        /// <param name="strUsername">the username</param>
        /// <returns>The newly populated GroupUsersEntity List</returns>
        public static string GetGroupsByUsernameString(List<GroupUsersEntity> grpUsers, string strUsername)
        {
            List<GroupUsersEntity> userGroups = grpUsers.FindAll(x => x.UserName == strUsername);
            string strGroupsList = string.Empty;
            foreach (GroupUsersEntity gu in userGroups)
            {
                strGroupsList += gu.GroupName + "; ";
            }

            strGroupsList = strGroupsList.TrimEnd(';',' ');

            return strGroupsList;
        }

        /// <summary>Gets the GroupUsersEntity List by UserId.</summary>
        /// <param name="intUserID">the userId</param>
        /// <returns>The newly populated GroupUsersEntity List</returns>
        public static List<GroupUsersEntity> GetGroupUsersByUserID(int intUserId)
        {
            List<GroupUsersEntity> groups = new List<GroupUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userId", intUserId));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetGroupUsersByUserID", parameters);
            Debug.WriteLine("group Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                GroupUsersEntity myGroup = GroupUsersEntity.GetEntityFromDataRow(r);
                groups.Add(myGroup);
            }

            return groups;
        }

        /// <summary>Gets the GroupUsersEntity List by GroupId.</summary>
        /// <param name="intGroupID">the groupId</param>
        /// <returns>The newly populated GroupUsersEntity List</returns>
        public static List<GroupUsersEntity> GetGroupUsersByGroupID(int intGroupId)
        {
            List<GroupUsersEntity> groups = new List<GroupUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@groupId", intGroupId));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetGroupUsersByGroupID", parameters);
            Debug.WriteLine("group Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                GroupUsersEntity myGroup = GroupUsersEntity.GetEntityFromDataRow(r);
                groups.Add(myGroup);
            }

            return groups;
        }


        /// <summary>Adds a new GroupUser record</summary>
        /// <param name="intUserID">the userID</param>
        /// <param name="intGroupID">the groupID</param>
        /// <returns>records updated count</returns>
        public static int AddUserToGroup(int intUserID, int intGroupID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            myParameters.Add(new SqlParameter("@userID", intUserID));
            myParameters.Add(new SqlParameter("@groupID", intGroupID));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_InsertGroupUser", myParameters);

            return intRecordsUpdated;
        }

        /// <summary>Removes the GroupUser record by the GroupUserID</summary>
        /// <param name="intGroupUserID">the Group User ID</param>
        /// <returns>records updated count</returns>
        public static int RemoveGroupUser(int intGroupUserID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            myParameters.Add(new SqlParameter("@groupUserID", intGroupUserID));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteGroupUser", myParameters);

            return intRecordsUpdated;
        }


    }
}