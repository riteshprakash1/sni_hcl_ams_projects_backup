﻿// ------------------------------------------------------------------
// <copyright file="ParameterEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web.UI.WebControls;

    /// <summary>This is the Parameter Entity.</summary>
    public class ParameterEntity
    {

        /// <summary>This is the ParameterName.</summary>
        private string strParameterName;

        /// <summary>This is the ParameterType.</summary>
        private string strParameterType;

        /// <summary>This is the PromptText.</summary>
        private string strPromptText;

        /// <summary>This is the Discrete Or Range.</summary>
        private string strDiscreteOrRange;

        /// <summary>This is the Start Range.</summary>
        private string strStartRange;

        /// <summary>This is the End Range.</summary>
        private string strEndRange;

        ///// <summary>This is the Parameter Values List.</summary>
        //private ArrayList lstValues;

        ///// <summary>This is the Default Values List.</summary>
        //private ArrayList lstDefaultValues;

        /// <summary>This is the Default Values List.</summary>
        private List<DefaultEntryEntity> lstDefaultValues;

        ///// <summary>This is the Selected Values List.</summary>
        //private ArrayList lstSelectedValues;

        /// <summary>This is the Input Values List.</summary>
        private List<InputEntity> lstInputValues;

        /// <summary>Allow multiple values.</summary>
        private bool blnMultipleValues;

        /// <summary>Initializes a new instance of the ParameterEntity class.</summary>
        public ParameterEntity()
        {
            this.ParameterName = String.Empty;
            this.DiscreteOrRange = String.Empty;
            this.ParameterType = String.Empty;
            this.PromptText = String.Empty;
            //ArrayList lstValues = new ArrayList();
            //ArrayList lstDefaultValues = new ArrayList();

            Hashtable tblDefaultValues = new Hashtable();

            //ArrayList lstSelectedValues = new ArrayList();
           this.InputValueList = new List<InputEntity>();
        }

        /// <summary>Gets or sets the name of the Parameter.</summary>
        /// <value>The name of the Parameter.</value>
        public string ParameterName
        {
            get { return this.strParameterName; }

            set { this.strParameterName = value; }
        }

        /// <summary>Gets or sets the Type of the Parameter.</summary>
        /// <value>The Type of the Parameter.</value>
        public string ParameterType
        {
            get { return this.strParameterType; }

            set { this.strParameterType = value; }
        }

        /// <summary>Gets or sets the Prompt Text.</summary>
        /// <value>The Prompt Text.</value>
        public string PromptText
        {
            get { return this.strPromptText; }

            set { this.strPromptText = value; }
        }

        /// <summary>Gets or sets the Discrete Or Range indicator.</summary>
        /// <value>The Discrete Or Range indicator.</value>
        public string DiscreteOrRange
        {
            get { return this.strDiscreteOrRange; }

            set { this.strDiscreteOrRange = value; }
        }

        ///// <summary>Gets or sets the Values ArrayList.</summary>
        ///// <value>The Values ArrayList.</value>
        //public ArrayList Values
        //{
        //    get { return this.lstValues; }

        //    set { this.lstValues = value; }
        //}

        ///// <summary>Gets or sets the Selected Values ArrayList.</summary>
        ///// <value>The Selected Values ArrayList.</value>
        //public ArrayList SelectedValues
        //{
        //    get { return this.lstSelectedValues; }

        //    set { this.lstSelectedValues = value; }
        //}

        ///// <summary>Gets or sets the Default Values ArrayList.</summary>
        ///// <value>The Default Values ArrayList.</value>
        //public ArrayList DefaultValues
        //{
        //    get { return this.lstDefaultValues; }

        //    set { this.lstDefaultValues = value; }
        //}

        /// <summary>Gets or sets the Default Values List.</summary>
        /// <value>The Default Values List.</value>
        public List<DefaultEntryEntity> DefaultValues
        {
            get { return this.lstDefaultValues; }

            set { this.lstDefaultValues = value; }
        }

        

        /// <summary>Gets or sets the InputEntity List.</summary>
        /// <value>The InputEntity List.</value>
        public List<InputEntity> InputValueList
        {
            get { return this.lstInputValues; }

            set { this.lstInputValues = value; }
        }


        /// <summary>Gets or sets the Start Range.</summary>
        /// <value>The Start Range.</value>
        public string StartRange
        {
            get { return this.strStartRange; }

            set { this.strStartRange = value; }
        }

        /// <summary>Gets or sets the End Range.</summary>
        /// <value>The End Range.</value>
        public string EndRange
        {
            get { return this.strEndRange; }

            set { this.strEndRange = value; }
        }

        /// <summary>Gets or sets the Multiple Values.</summary>
        /// <value>The Multiple Values.</value>
        public bool MultipleValues
        {
            get { return this.blnMultipleValues; }

            set { this.blnMultipleValues = value; }
        }
        

    } //// end class
}