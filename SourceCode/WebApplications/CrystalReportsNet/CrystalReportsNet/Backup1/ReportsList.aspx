﻿<%@ Page Title="Reports List" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportsList.aspx.cs" Inherits="CrystalReportsNet.ReportsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:HiddenField ID="hdnReportName" runat="server" />
    <table style="width:100%; text-align:center">
        <tr>
            <td style="text-align:right">
                <asp:HyperLink ID="lnkUserGuide" NavigateUrl="~/CrystalReportsUserGuide.pdf" Target="_blank" runat="server">Need help?  Click to open the User Guide</asp:HyperLink>
            </td>
        </tr>
    </table>
    
    <table style="width:90%; text-align:center">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Size="10pt"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Table runat="server" Width="90%" ID="tableReports"></asp:Table>

</asp:Content>
