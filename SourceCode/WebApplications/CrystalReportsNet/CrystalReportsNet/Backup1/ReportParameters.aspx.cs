﻿// --------------------------------------------------------------
// <copyright file="ReportsViewer.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalDecisions.CrystalReports.Engine;
    using CrystalDecisions.Shared;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;

    /// <summary>This is the ReportParameters class for ReportsViewer page</summary>
    public partial class ReportParameters : System.Web.UI.Page
    {
        /// <summary>This is the AppUsersEntity for the application user.</summary>
        protected AppUsersEntity appUser;

        /// <summary>This indicates whether the user should have teeritories assigned and does not.</summary>
        protected bool blnSalesNoTerritories;

        /// <summary>Handles the Init event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblMessage.Text = string.Empty;
            Debug.WriteLine("Page_Load IsPostBack: " + Page.IsPostBack.ToString());
            AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));

            // Impersonate User; must be an admin
            if (Request.QueryString["impersonate"] != null && (myUser.RoleID == 1 || myUser.RoleID == 5))
            {
                appUser = AppUsersEntity.GetAppUserByUsername(Request.QueryString["impersonate"]);
                this.lblMessage.Text += "Impersonating: " + Request.QueryString["impersonate"];
            }
            else
            {
                appUser = myUser;
            }

            
            if (!Page.IsPostBack)
            {
                //if (Request.QueryString["ReportID"] == null)
                //    Response.Redirect("ReportParameters.aspx?ReportID=226", true);

                
                if (Request.QueryString["ReportID"] != null && appUser != null)
                {
                    if (appUser.UserStatus.ToLower() == "active" && !AppUsersEntity.SalesWithNoTerritories(appUser))
                    {
                        this.hdnReportID.Value = Request.QueryString["ReportID"];

                        ReportEntity rpt = ReportEntity.GetReportByReportID(Convert.ToInt32(this.hdnReportID.Value));
                        this.lblReportName.Text = rpt.PublishedName;
                        string reportPath = Server.MapPath("") + "\\reports\\" + rpt.ReportFileName;

                        ReportDocument report = new ReportDocument();
                        report.Load(reportPath);

                        Collection<ParameterEntity> rptParameters = this.LoadParameterEntityCollection(report);
                        this.BindDT(rptParameters);
                    }
                    else
                    {
                        Response.Redirect("~/ReportsList.aspx", true);
                    }
                }
                else
                {
                    Response.Redirect("~/ReportsList.aspx", true);
                }
            }

        } //// end Page_Load


        /// <summary>The pulls all the parameter data from the ReportDocument object and loads it into a ParameterEntity Collection</summary>
        /// <param name="report">The ReportDocument Object</param>
        /// <returns>The ParameterEntity Collection</returns>
        protected Collection<ParameterEntity> LoadParameterEntityCollection(ReportDocument report)
        {
            Collection<ParameterEntity> coll = new Collection<ParameterEntity>();

            ParameterFieldDefinitions parmFields = report.DataDefinition.ParameterFields;
            foreach (ParameterFieldDefinition def in parmFields)
            {
                ParameterEntity entity = new ParameterEntity();
                entity.MultipleValues = def.EnableAllowMultipleValue;
                Debug.WriteLine("ValueType: " + def.ValueType.ToString());
                entity.ParameterName = def.ParameterFieldName;
                entity.ParameterType = def.ParameterValueKind.ToString().ToLower().Replace("parameter", "");
                entity.DiscreteOrRange = def.DiscreteOrRangeKind.ToString();
                entity.PromptText = report.ParameterFields[entity.ParameterName].PromptText;

                Debug.WriteLine(def.ParameterFieldName + " - DiscreteOrRangeKind: " + def.DiscreteOrRangeKind.ToString());

                List<DefaultEntryEntity> entries = new List<DefaultEntryEntity>();
                foreach (ParameterValue v in def.DefaultValues)
                {
                    if (v != null)
                    {
                        ParameterDiscreteValue dValue = (ParameterDiscreteValue)v;

                        if (dValue.Description == null)
                            entries.Add(new DefaultEntryEntity(dValue.Value.ToString(), dValue.Value.ToString()));
                        else
                            entries.Add(new DefaultEntryEntity(dValue.Value.ToString(), dValue.Description.ToString()));

                        Debug.WriteLine(entity.ParameterName + " Added: " + dValue.Value.ToString());
                    }
                }

                entity.DefaultValues = entries;


                //ArrayList lst = new ArrayList();
                //foreach (ParameterValue v in def.DefaultValues)
                //{
                //    if (v != null)
                //    {
                //        ParameterDiscreteValue dValue = (ParameterDiscreteValue)v;
                //        lst.Add(dValue.Value.ToString());
                //        Debug.WriteLine(entity.ParameterName + " Added: " + dValue.Value.ToString());
                //     }
                //}

                //entity.DefaultValues = lst;

                coll.Add(entity);
            }

            report.Close();
            report.Dispose();

            return coll;
        }

        /// <summary>Handles the Start Calendar Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The ImageClickEventArgs instance containing the event data.</param>
        protected void imgStartCalendar_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;

            Calendar calStartDate = (Calendar)gvRow.FindControl("calStartDate");
            calStartDate.Visible = !calStartDate.Visible;

            Debug.WriteLine("calStartDate.Visible:" + calStartDate.Visible.ToString());

            if(calStartDate.Visible)
            {
 
                TextBox txtStartDate = (TextBox)gvRow.FindControl("txtStartDate");
                Debug.WriteLine("txtStartDate.Text != string.Empty: " + Convert.ToBoolean(txtStartDate.Text != string.Empty).ToString());
                if (txtStartDate.Text != string.Empty)
                {
                    Debug.WriteLine("txtStartDate.Text != string.Empty");
                    RegularExpressionValidator vldStartDate = (RegularExpressionValidator)gvRow.FindControl("vldStartDate");
                    vldStartDate.Validate();
                    if (vldStartDate.IsValid)
                    {
                        Debug.WriteLine("IsValid:" + txtStartDate.Text);
                        calStartDate.SelectedDate = this.GetDateTime(txtStartDate.Text);
                    }
                }
            }
        }

        /// <summary>Converts the string date to a DateTime</summary>
        /// <param name="strDate">The string datetime</param>
        /// <returns>The DateTime object</returns>
        protected DateTime GetDateTime(string strDate)
        {
            DateTime myDate = DateTime.Now;
            try
            {
                string strYr = strDate.Substring(6, 4);
                string strMo = strDate.Substring(0, 2);
                string strDay = strDate.Substring(3, 2);
                myDate = new DateTime(Convert.ToInt32(strYr), Convert.ToInt32(strMo), Convert.ToInt32(strDay));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Date Error: " + ex.Message);
            }

            return myDate;

        }

        /// <summary>Handles the Start Calendar SelectionChanged event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void calStartDate_SelectionChanged(object sender, EventArgs e)
        {
            Calendar lb = (Calendar)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;

            Calendar calStartDate = (Calendar)gvRow.FindControl("calStartDate");
            TextBox txtStartDate = (TextBox)gvRow.FindControl("txtStartDate");

            txtStartDate.Text = calStartDate.SelectedDate.ToShortDateString();
            calStartDate.Visible = false;
        }

        /// <summary>Handles the End Calendar Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The ImageClickEventArgs instance containing the event data.</param>
        protected void imgEndCalendar_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;

            Calendar calEndDate = (Calendar)gvRow.FindControl("calEndDate");
            calEndDate.Visible = !calEndDate.Visible;

            if (calEndDate.Visible)
            {
                calEndDate.SelectedDate = DateTime.Now;

                TextBox txtEndDate = (TextBox)gvRow.FindControl("txtEndDate");
                if (txtEndDate.Text != string.Empty)
                {
                    RegularExpressionValidator vldEndDate = (RegularExpressionValidator)gvRow.FindControl("vldEndDate");
                    vldEndDate.Validate();
                    if (vldEndDate.IsValid)
                    {
                        calEndDate.SelectedDate = this.GetDateTime(txtEndDate.Text);
                    }
                }
            }
        }

        /// <summary>Handles the End Calendar SelectionChanged event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void calEndDate_SelectionChanged(object sender, EventArgs e)
        {
            Calendar lb = (Calendar)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;

            Calendar calEndDate = (Calendar)gvRow.FindControl("calEndDate");
            TextBox txtEndDate = (TextBox)gvRow.FindControl("txtEndDate");

            txtEndDate.Text = calEndDate.SelectedDate.ToShortDateString();
            calEndDate.Visible = false;
        }

        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Debug.WriteLine("GV_RowDataBound");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Find all the controls in the GridView Row
                GridView gvInput = (GridView)e.Row.FindControl("gvInput");
                
                ListBox lstDefaultValues = (ListBox)e.Row.FindControl("lstDefaultValues");
                TextBox txtListValue = (TextBox)e.Row.FindControl("txtListValue");
                Label lblDiscrete = (Label)e.Row.FindControl("lblDiscrete");
                Button btnAddValue = (Button)e.Row.FindControl("btnAddValue");

                Literal ltlDefaultValueBreak = (Literal)e.Row.FindControl("ltlDefaultValueBreak");

                Literal ltlDateBreak1 = (Literal)e.Row.FindControl("ltlDateBreak1");
                Literal ltlDateBreak2 = (Literal)e.Row.FindControl("ltlDateBreak2");
                Literal ltlDateBreak3 = (Literal)e.Row.FindControl("ltlDateBreak3");
                Literal ltlDiscreteAndRangeBreak = (Literal)e.Row.FindControl("ltlDiscreteAndRangeBreak");
                
                Label lblStart = (Label)e.Row.FindControl("lblStart");
                Label lblEnd = (Label)e.Row.FindControl("lblEnd");
                Button btnAddRange = (Button)e.Row.FindControl("btnAddRange");

                Button btnAddDefault = (Button)e.Row.FindControl("btnAddDefault");
                RequiredFieldValidator vldRequiredDefaultSelect = (RequiredFieldValidator)e.Row.FindControl("vldRequiredDefaultSelect");

                RequiredFieldValidator vldRequiredListValue = (RequiredFieldValidator)e.Row.FindControl("vldRequiredListValue");

                TextBox txtStartDate = (TextBox)e.Row.FindControl("txtStartDate");
                ImageButton imgStartCalendar = (ImageButton)e.Row.FindControl("imgStartCalendar");
                Calendar calStartDate = (Calendar)e.Row.FindControl("calStartDate");
                RegularExpressionValidator vldStartDate = (RegularExpressionValidator)e.Row.FindControl("vldStartDate");
                RequiredFieldValidator vldRequiredStartDate = (RequiredFieldValidator)e.Row.FindControl("vldRequiredStartDate");

                TextBox txtEndDate = (TextBox)e.Row.FindControl("txtEndDate");
                ImageButton imgEndCalendar = (ImageButton)e.Row.FindControl("imgEndCalendar");
                Calendar calEndDate = (Calendar)e.Row.FindControl("calEndDate");
                RegularExpressionValidator vldEndDate = (RegularExpressionValidator)e.Row.FindControl("vldEndDate");
                RequiredFieldValidator vldRequiredEndDate = (RequiredFieldValidator)e.Row.FindControl("vldRequiredEndDate");
                RegularExpressionValidator vldDataLength10 = (RegularExpressionValidator)e.Row.FindControl("vldDataLength10");
                RegularExpressionValidator vldDataLength6 = (RegularExpressionValidator)e.Row.FindControl("vldDataLength6");

                
                //// Load the Default Value List
                //ArrayList lstDefault = (ArrayList)this.gvParameters.DataKeys[e.Row.RowIndex]["DefaultValues"];
                //for (int x = 0; x < lstDefault.Count; x++)
                //{
                //    lstDefaultValues.Items.Add(new ListItem(lstDefault[x].ToString()));
                //}

                // Load the Default Value List
                List<DefaultEntryEntity> entries = (List<DefaultEntryEntity>)this.gvParameters.DataKeys[e.Row.RowIndex]["DefaultValues"];
                foreach (DefaultEntryEntity entry in entries)
                {
                    lstDefaultValues.Items.Add(new ListItem(entry.DefaultDescription + " - " + entry.DefaultValue, entry.DefaultValue));
                   // Console.WriteLine("{0} -> {1}", dictionaryEntry.Key, dictionaryEntry.Value);
                }

                string strParameterName = this.gvParameters.DataKeys[e.Row.RowIndex]["ParameterName"].ToString().ToLower();
                if ((appUser.IsDistributor && strParameterName == "salesdistrict") || (strParameterName == "salesrep" && appUser.IsSalesRep))
                    e.Row.Visible = false;

                bool isTenDigitField = Convert.ToBoolean(strParameterName == "soldto" || strParameterName == "customerid" || strParameterName == "soldtocustomer" 
                                        || strParameterName == "salesrep");
                vldDataLength10.Visible = isTenDigitField;

                bool isSixDigitField = Convert.ToBoolean(strParameterName == "salesdistrict");
                vldDataLength6.Visible = isSixDigitField;

                string strDiscreteOrRange = this.gvParameters.DataKeys[e.Row.RowIndex]["DiscreteOrRange"].ToString();
                string strParameterType = this.gvParameters.DataKeys[e.Row.RowIndex]["ParameterType"].ToString();
                bool isRange = Convert.ToBoolean((strDiscreteOrRange.ToLower() == "rangevalue") || (strDiscreteOrRange.ToLower() == "discreteandrangevalue"));
                bool blnMultipleValues = Convert.ToBoolean(this.gvParameters.DataKeys[e.Row.RowIndex]["MultipleValues"].ToString());
                bool isDate = Convert.ToBoolean(strParameterType.ToLower() == "date");
                Debug.WriteLine("strParameterType: " + strParameterType);

                bool isDiscrete = Convert.ToBoolean((strDiscreteOrRange.ToLower() == "discretevalue") || (strDiscreteOrRange.ToLower() == "discreteandrangevalue"));

                bool showListDefaultValues = Convert.ToBoolean(lstDefaultValues.Items.Count > 1);
                lstDefaultValues.Visible = showListDefaultValues;
                btnAddDefault.Visible = lstDefaultValues.Visible;
                vldRequiredDefaultSelect.Visible = lstDefaultValues.Visible;
                ltlDefaultValueBreak.Visible = lstDefaultValues.Visible;

                // if only one default value add to txtListValue and hide lstDefaultValues
                if (entries.Count == 1)
                    txtListValue.Text = entries[0].DefaultValue;

                txtListValue.Visible = isDiscrete;
                vldRequiredListValue.Visible = txtListValue.Visible;
                btnAddValue.Visible = txtListValue.Visible;
                lblDiscrete.Visible = txtListValue.Visible;
                ltlDiscreteAndRangeBreak.Visible = Convert.ToBoolean(txtListValue.Visible && isRange);

                if(txtListValue.Visible)
                    EnterButton.TieButton(txtListValue, btnAddValue); 
                
                if (!Page.IsPostBack && txtListValue.Visible)
                {
                    EnterButton.TieButton(txtListValue, btnAddValue);
                }

                ltlDateBreak1.Visible = isRange;
                ltlDateBreak2.Visible = isRange;
                ltlDateBreak3.Visible = isRange;
                btnAddRange.Visible = isRange;

                txtStartDate.Visible = isRange;
                imgStartCalendar.Visible = isRange & isDate;
                calStartDate.Visible = false;
                vldStartDate.Visible = isRange & isDate;
                vldRequiredStartDate.Visible = isRange;

                // Pre-Populate the Start Date if it is visible and not a PostBack
                if (!Page.IsPostBack && txtStartDate.Visible && isDate)
                {
                    txtStartDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    EnterButton.TieButtonDoNothing(txtStartDate, this.btnRunReport);
                }


                txtEndDate.Visible = isRange;
                imgEndCalendar.Visible = isRange & isDate;
                calEndDate.Visible = false;
                vldEndDate.Visible = isRange & isDate;
                vldRequiredEndDate.Visible = isRange;

                // Pre-Populate the End Date if it is visible and not a PostBack
                if (!Page.IsPostBack && txtEndDate.Visible && isDate)
                {
                    txtEndDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    EnterButton.TieButtonDoNothing(txtEndDate, this.btnRunReport);
                }
                
                lblStart.Visible = isRange;
                lblEnd.Visible = isRange;

                List<InputEntity> inputList = this.LoadInputEntityListFromGridView(gvInput);
                this.BindInput(inputList, gvInput);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Cancel event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_AddDefault(object sender, GridViewCancelEditEventArgs e)
        {
            Debug.WriteLine("GV_AddDefault");

            RequiredFieldValidator vldRequiredDefaultSelect = (RequiredFieldValidator)this.gvParameters.Rows[e.RowIndex].FindControl("vldRequiredDefaultSelect");
            vldRequiredDefaultSelect.Validate();

            if (vldRequiredDefaultSelect.IsValid)
            {
                ListBox lstDefaultValues = (ListBox)this.gvParameters.Rows[e.RowIndex].FindControl("lstDefaultValues");
                string strParameterName = this.gvParameters.DataKeys[e.RowIndex]["ParameterName"].ToString().ToLower();
                string strDiscreteOrRange = this.gvParameters.DataKeys[e.RowIndex]["DiscreteOrRange"].ToString().ToLower();

                List<InputEntity> entries = new List<InputEntity>();

                bool blnMultipleValues = Convert.ToBoolean(this.gvParameters.DataKeys[e.RowIndex]["MultipleValues"].ToString());
                GridView gvInput = (GridView)this.gvParameters.Rows[e.RowIndex].FindControl("gvInput");

                if (blnMultipleValues)
                {
                    entries = LoadInputEntityListFromGridView(gvInput);
                }

                InputEntity newInput = new InputEntity();
                newInput.StartValue = lstDefaultValues.SelectedItem.Value;
                newInput.ParameterName = strParameterName;
                newInput.DiscreteOrRange = "discrete";
                entries.Add(newInput);
                this.BindInput(entries, gvInput);

                lstDefaultValues.SelectedIndex = -1;
            }
       
        }

        /// <summary>Handles the Edit event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewEditEventArgs e)
        {
            Debug.WriteLine("GV_Add");
            string strParameterName = this.gvParameters.DataKeys[e.NewEditIndex]["ParameterName"].ToString().ToLower();
            string strDiscreteOrRange = this.gvParameters.DataKeys[e.NewEditIndex]["DiscreteOrRange"].ToString().ToLower();

            RequiredFieldValidator vldRequiredListValue = (RequiredFieldValidator)this.gvParameters.Rows[e.NewEditIndex].FindControl("vldRequiredListValue");
            vldRequiredListValue.Validate();
            bool IsValidToAdd = vldRequiredListValue.IsValid;

            if (IsValidToAdd)
            {
                RegularExpressionValidator vldDataLength6 = (RegularExpressionValidator)this.gvParameters.Rows[e.NewEditIndex].FindControl("vldDataLength6");
                if (vldDataLength6.Visible)
                {
                    vldDataLength6.Validate();
                    IsValidToAdd = vldDataLength6.IsValid;
                }
                else
                {
                    RegularExpressionValidator vldDataLength10 = (RegularExpressionValidator)this.gvParameters.Rows[e.NewEditIndex].FindControl("vldDataLength10");
                    if (vldDataLength10.Visible)
                    {
                        vldDataLength10.Validate();
                        IsValidToAdd = vldDataLength10.IsValid;
                    }
                }
            }

            if (IsValidToAdd)
            {
                TextBox txtListValue = (TextBox)this.gvParameters.Rows[e.NewEditIndex].FindControl("txtListValue");
                List<InputEntity> entries = new List<InputEntity>();

                bool blnMultipleValues = Convert.ToBoolean(this.gvParameters.DataKeys[e.NewEditIndex]["MultipleValues"].ToString());
                GridView gvInput = (GridView)this.gvParameters.Rows[e.NewEditIndex].FindControl("gvInput");

                if (blnMultipleValues)
                {
                    entries = LoadInputEntityListFromGridView(gvInput);
                }

                InputEntity newInput = new InputEntity();
                newInput.StartValue = txtListValue.Text;
                newInput.ParameterName = strParameterName;
                newInput.DiscreteOrRange = "discrete";
                entries.Add(newInput);
                this.BindInput(entries, gvInput);

                txtListValue.Text = string.Empty;
            }
        }

        /// <summary>Handles the AddRange event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_AddRange(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            TextBox txtStartDate = (TextBox)this.gvParameters.Rows[e.RowIndex].FindControl("txtStartDate");
            TextBox txtEndDate = (TextBox)this.gvParameters.Rows[e.RowIndex].FindControl("txtEndDate");
            string strParameterName = this.gvParameters.DataKeys[e.RowIndex]["ParameterName"].ToString().ToLower();
            bool blnMultipleValues = Convert.ToBoolean(this.gvParameters.DataKeys[e.RowIndex]["MultipleValues"].ToString());
            string strDiscreteOrRange = this.gvParameters.DataKeys[e.RowIndex]["DiscreteOrRange"].ToString().ToLower();
            
            RequiredFieldValidator vldRequiredStartDate = (RequiredFieldValidator)this.gvParameters.Rows[e.RowIndex].FindControl("vldRequiredStartDate");
            vldRequiredStartDate.Validate();

            RequiredFieldValidator vldRequiredEndDate = (RequiredFieldValidator)this.gvParameters.Rows[e.RowIndex].FindControl("vldRequiredEndDate");
            vldRequiredEndDate.Validate();

            bool IsValidToAdd = Convert.ToBoolean(vldRequiredStartDate.IsValid && vldRequiredEndDate.IsValid);

            Debug.WriteLine("Validate vldRequiredStartDate: " + vldRequiredStartDate.IsValid.ToString());
            Debug.WriteLine("Validate vldRequiredEndDate: " + vldRequiredEndDate.IsValid.ToString()); 

            if (IsValidToAdd)
            {
                RegularExpressionValidator vldEndDate = (RegularExpressionValidator)this.gvParameters.Rows[e.RowIndex].FindControl("vldEndDate");
                RegularExpressionValidator vldStartDate = (RegularExpressionValidator)this.gvParameters.Rows[e.RowIndex].FindControl("vldStartDate");

                if (vldStartDate.Visible)
                {
                    vldEndDate.Validate();
                    vldStartDate.Validate();

                    IsValidToAdd = Convert.ToBoolean(vldEndDate.IsValid && vldStartDate.IsValid);
                }

                if (IsValidToAdd)
                {
                    List<InputEntity> entries = new List<InputEntity>();
                    GridView gvInput = (GridView)this.gvParameters.Rows[e.RowIndex].FindControl("gvInput");

                    if (blnMultipleValues)
                    {
                        entries = LoadInputEntityListFromGridView(gvInput);
                    }

                    InputEntity newInput = new InputEntity();
                    newInput.StartValue = txtStartDate.Text;
                    newInput.EndValue = txtEndDate.Text;
                    newInput.ParameterName = strParameterName;
                    newInput.DiscreteOrRange = "range";
                    entries.Add(newInput);
                    this.BindInput(entries, gvInput);

                    txtStartDate.Text = string.Empty;
                    txtEndDate.Text = string.Empty;
                }
            }

        }

        /// <summary>Handles the Remove event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_RemoveEntry(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            Debug.WriteLine("GV_RemoveEntry: " + sender.ToString());
            GridView gvInput = (GridView)sender;
            string strParameterName = gvInput.DataKeys[e.RowIndex]["ParameterName"].ToString();

            Debug.WriteLine("strParameterName: " + strParameterName);

            List<InputEntity> entries = this.LoadInputEntityListFromGridView(gvInput);
            entries.RemoveAt(e.RowIndex);
            this.BindInput(entries, gvInput);
        }


        /// <summary>Handles the btnRunReport Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void btnRunReport_Click(object sender, EventArgs e)
        {
            Page.Validate("RunReport");

            foreach (GridViewRow r in this.gvParameters.Rows)
            {
                if (r.Visible)
                {
                    CustomValidator vldRequiredInput = (CustomValidator)r.FindControl("vldRequiredInput");
                    GridView gvValidInput = (GridView)r.FindControl("gvInput");

                    vldRequiredInput.IsValid = gvValidInput.Rows.Count > 0;
                }
            }

            if (Page.IsValid)
            {
                Collection<ParameterEntity> coll = new Collection<ParameterEntity>();

                foreach (GridViewRow r in this.gvParameters.Rows)
                {
                    ParameterEntity p = new ParameterEntity();
                    p.ParameterName = this.gvParameters.DataKeys[r.RowIndex]["ParameterName"].ToString();
                    p.ParameterType = this.gvParameters.DataKeys[r.RowIndex]["ParameterType"].ToString();
                    p.DiscreteOrRange = this.gvParameters.DataKeys[r.RowIndex]["DiscreteOrRange"].ToString();
                    p.MultipleValues = Convert.ToBoolean(this.gvParameters.DataKeys[r.RowIndex]["MultipleValues"].ToString());

                    //bool isRange = Convert.ToBoolean(p.DiscreteOrRange.ToLower() == "rangevalue");
                    bool isDate = Convert.ToBoolean(p.ParameterType.ToLower() == "date");

                    Debug.WriteLine("Name: " + p.ParameterName + "; Type: " + p.ParameterType + "; Range: " + p.DiscreteOrRange + "; MultipleValues: " + p.MultipleValues.ToString());

                    GridView gvInput = (GridView)r.FindControl("gvInput");
                    p.InputValueList = this.LoadInputEntityListFromGridView(gvInput);

                    if ((appUser.IsDistributor && p.ParameterName.ToLower() == "salesdistrict") || (p.ParameterName.ToLower() == "salesrep" && appUser.IsSalesRep))
                    {
                        List<UserTerritoryEntity> territories = UserTerritoryEntity.GetUserTerritoriesByUserId(appUser.UserID);
                        Debug.WriteLine("btnRunReport_Click sp_GetUserTerritoriesByUserId UserTerritory Rows: " + territories.Count.ToString());
                        //p.Values = new ArrayList();
                        foreach (UserTerritoryEntity i in territories)
                        {
                            InputEntity newInput = new InputEntity();
                            newInput.StartValue = i.Territory;
                            newInput.ParameterName = p.ParameterName;
                            newInput.DiscreteOrRange = "discrete";
                            p.InputValueList.Add(newInput);
                        }
                    }

                    coll.Add(p);

                }  //// end foreach

                ReportTrackerEntity rt = this.TrackReport();

                Session["ParametersEntityCollection" + rt.ReportTrackerID.ToString()] = coll;
                //Response.Redirect("ReportsViewer.aspx?ReportName=" + this.lblReportName.Text, true);
                Response.Redirect("ReportsViewer.aspx?ReportTrackerID=" + rt.ReportTrackerID.ToString(), true);

            }  //// end Page.IsValid
            else
            {
                Debug.WriteLine("RunReport NOT Valid");
            }
        }

        /// <summary>Binds the parameters collection to the GridView.</summary>
        protected void BindDT(Collection<ParameterEntity> coll)
        {
            this.gvParameters.DataSource = coll;
            this.gvParameters.DataBind();
        }//// end BindDT


        /// <summary>Binds the parameters collection to the GridView.</summary>
        protected void BindInput(List<InputEntity> lst, GridView gv)
        {
            //if (gv.Rows.Count == 0)
            //    lst.Add(new InputEntity());

            gv.DataSource = lst;
            gv.DataBind();
        }//// end BindDT

        /// <summary>This loads the data from the GridView into InputEntity List.</summary>
        /// <returns>The InputEntity List</returns>
        protected List<InputEntity> LoadInputEntityListFromGridView(GridView gv)
        {
            List<InputEntity> entries = new List<InputEntity>();

            foreach (GridViewRow r in gv.Rows)
            {
                Label lblStart = (Label)gv.Rows[r.RowIndex].FindControl("lblStart");
                Label lblEnd = (Label)gv.Rows[r.RowIndex].FindControl("lblEnd");
                string strParameterName = gv.DataKeys[r.RowIndex]["ParameterName"].ToString();
                string strDiscreteOrRange = gv.DataKeys[r.RowIndex]["DiscreteOrRange"].ToString();

                InputEntity i = new InputEntity();
                i.StartValue = lblStart.Text;
                i.EndValue = lblEnd.Text;
                i.ParameterName = strParameterName;
                i.DiscreteOrRange = strDiscreteOrRange;

                entries.Add(i);
            }

            return entries;
        }

        /***********  Tracking Methods ********************************/

        /// <summary>Instantiates a new ReportTrackerEntity, loads the data and inserts the record into the DB and returns the ReportTrackerEntity</summary>
        /// <returns>The ReportTrackerEntity</returns>
        protected ReportTrackerEntity TrackReport()
        {
            ReportTrackerEntity r = new ReportTrackerEntity();
            r.ReportID = Convert.ToInt32(this.hdnReportID.Value);
            r.ServerName = Page.Request.Url.ToString();
            r.UserName = Utility.CurrentUser(Request);

            AppUsersEntity u = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));
            r.UserTerritories = u.Territories;
            r.UserRole = u.RoleName;
            r.DisplayName = u.DisplayName;
            r = ReportTrackerEntity.InsertReportTracker(r);

            return r;
        }

    }//// end class
}