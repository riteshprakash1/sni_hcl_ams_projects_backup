﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="CrystalReportsNet.Admin.UserList" MasterPageFile="~/Site.Master" Title="User List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="SearchPage">
        <table>
            <tr>
                <td style="width:30%;font-size: large;text-decoration: underline; font-weight:bold">User List</td>
                <td style="width:40%">Select User: <asp:DropDownList ID="ddlUsers" runat="server" 
                        onselectedindexchanged="ddlUsers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                <td style="text-align:right">
                    <asp:Button id="btnAddUser" runat="server" Text="Add New User" 
					    CausesValidation="false" onclick="btnAddUser_Click"></asp:Button>
                    <asp:HiddenField ID="hdnSortDirection" runat="server" Value="descending" />
                    <asp:HiddenField ID="hdnSortField" runat="server" Value="displayname" />
                </td>
            </tr>           
            <tr>
                <td colspan="3">
                    <asp:GridView ID="gvUser" runat="server" Width="800" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound" OnRowEditing="GV_Edit"
                    EmptyDataText="There are no users." OnRowDeleting="GV_Delete"  ShowFooter="false" AllowSorting="true" OnSorting="GV_Sorting"
                    PagerSettings-FirstPageText="First" PagerSettings-LastPageText="Last" PagerSettings-Position="TopAndBottom" AllowPaging="true" PagerSettings-Mode="Numeric" PageSize="100"  
                     CellPadding="2" DataKeyNames="userID, userName, roleID" GridLines="Both" OnRowCancelingEdit="GV_Add" OnPageIndexChanging="GV_PageIndexChanging">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="View" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 

                       <asp:TemplateField HeaderStyle-Width="15%" HeaderText="UserName" HeaderStyle-HorizontalAlign="Left" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" Font-Size="8pt" runat="server"  Text='<%# Eval("username")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddUserName" Width="150px" MaxLength="30" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredAddUserName" ValidationGroup="Add" ControlToValidate="txtAddUserName" runat="server"  ErrorMessage="User Name is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldUserExists" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name already exists.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldAddUserInAD" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name is not valid.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:BoundField DataField="displayname" SortExpression="displayname" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Display Name" />                       
                        <asp:BoundField DataField="userstatus" SortExpression="userstatus" HeaderStyle-Width="10%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Status" />                       
                        <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Role" SortExpression="roleName" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRole" Font-Size="8pt" runat="server"  Text='<%# Eval("roleName")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlRole" width="125px" DataTextField="RoleName" DataValueField="RoleID" DataSource='<%# GetRoles()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldRole" ControlToValidate="ddlRole" ValidationGroup="Add" ForeColor="#FF7300" runat="server" ErrorMessage="Role is required.">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField> 
                        <asp:BoundField DataField="groupnames" SortExpression="groupnames" HeaderStyle-Width="45%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Groups" />                       
                   </Columns>
                </asp:GridView> 
                
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="vldSummary1"  ValidationGroup="FindUser" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>

       </table>    
    </div>
</asp:Content>
