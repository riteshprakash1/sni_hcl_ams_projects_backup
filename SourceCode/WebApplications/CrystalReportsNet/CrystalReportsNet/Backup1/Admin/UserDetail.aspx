﻿<%@ Page Title="User Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserDetail.aspx.cs" Inherits="CrystalReportsNet.Admin.UserDetail" %>
<%@ Register TagPrefix="uc2" Namespace="CrystalReportsNet.Classes" Assembly="CrystalReportsNet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <table style="width:100%">
            <tr>
                <td style="width:768px">
                   <table style="width:100%">
                        <tr>
                            <td colspan="4" style="font-size: large;text-decoration: underline;"><strong>User Detail</strong>
                                <asp:hiddenfield ID="hdnDisplayName" runat="server"></asp:hiddenfield>
                            </td>
                        </tr> 
                        <tr>
                            <td style="width:17%; font-size:10pt; font-weight:bold">S&N Username: *</td>
                            <td style="width:25%; font-size:10pt"><asp:TextBox ID="txtUserName" Font-Size="10pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredUsername" ControlToValidate="txtUserName" ValidationGroup="ValidSubmit" runat="server" ForeColor="#FF7300" ErrorMessage="S&N Username is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldUsernameInValid" runat="server" ValidationGroup="ValidSubmit" ForeColor="#FF7300" ErrorMessage="The username is not valid.  It does not exist in Active Directory.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldUserExists" runat="server" ValidationGroup="ValidSubmit" ForeColor="#FF7300" ErrorMessage="A Crystal Profile account already exists with this user name.">*</asp:CustomValidator>
                            </td>
                            <td style="width:14%;"><asp:Label ID="LabelUserID"  Font-Bold="true" Font-Size="10pt" runat="server" Text="User ID:"></asp:Label></td>
                            <td style="font-size:10pt"><asp:Label ID="lblUserID" Font-Size="10pt" runat="server"></asp:Label></td>
                        </tr>          
                        <tr>
                            <td style="font-weight:bold; font-size:10pt">Role: *</td>
                            <td><asp:DropDownList ID="ddlRole" runat="server"  Font-Size="10pt" Width="150px"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vldRequiredRole" ValidationGroup="ValidSubmit" ControlToValidate="ddlRole" runat="server" ForeColor="#FF7300" ErrorMessage="Role is required.">*</asp:RequiredFieldValidator>
                            </td>
                            <td><asp:Label ID="LabelDisplayName"  Font-Bold="true" Font-Size="10pt" runat="server" Text="Display Name:"></asp:Label></td>
                            <td><asp:Label ID="lblDisplayName" Font-Size="10pt" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="font-weight:bold; font-size:10pt">Status:</td>
                            <td><asp:DropDownList ID="ddlStatus" runat="server" Font-Size="10pt" Width="150px">
                                <asp:ListItem>Active</asp:ListItem>
                                <asp:ListItem>Disabled</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td><asp:Label ID="LabelCreated"  Font-Bold="true" Font-Size="10pt" runat="server" Text="Created:"></asp:Label></td>
                            <td><asp:Label ID="lblCreatedBy" Font-Size="10pt" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblCreatedDate" Font-Size="10pt" runat="server"></asp:Label></td>
                        </tr>          
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td><asp:Label ID="LabelModified"  Font-Bold="true" Font-Size="10pt" runat="server" Text="Modified:"></asp:Label></td>
                            <td><asp:Label ID="lblModifiedBy" Font-Size="10pt" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblModifiedDate" Font-Size="10pt" runat="server"></asp:Label></td>
                         </tr>
          
                         <tr>
                            <td>&nbsp;</td>
                            <td style="text-align:left">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click"  Width="120px"/>
                            </td>
                            <td style="text-align:left">
                                <asp:Button ID="btnImpersonate" runat="server" Text="Impersonate" Width="120px" 
                                    onclick="btnImpersonate_Click"/>
                            </td>
                        </tr>
                       <tr>
                            <td colspan="4">
                                <asp:ValidationSummary ID="vldSummary1" ValidationGroup="ValidSubmit" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblMessage" ForeColor="#FF7300" Font-Size="10pt" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align:top; width:256px">
                    <table>
                         <tr>
                             <td style="vertical-align:top;"><asp:Label ID="LabelTerritories" Font-Bold="true" Font-Size="10pt" runat="server" Text="Territories:"></asp:Label></td>
                        </tr>
                         <tr>
                            <td>
                                <asp:GridView ID="gvTerritory" runat="server" Width="245px" AutoGenerateColumns="False" OnRowDataBound="GV_RowDataBoundTerritory"
                                    EmptyDataText="There are no territories." OnRowDeleting="GV_DeleteTerritory" ShowFooter="true" ShowHeader="false"
                                    OnRowCancelingEdit="GV_AddTerritory" CellPadding="2" DataKeyNames="UserTerritoryID" BorderStyle="None" GridLines="Both">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
									                ></asp:Button>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							                </FooterTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Territory ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTerritory" Font-Size="8pt" runat="server"  Text='<%# Eval("Territory")%>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtTerritory" MaxLength="20" Width="80px" Font-Size="8pt" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldRequiredTerritory" ValidationGroup="AddTerritory" ControlToValidate="txtTerritory" runat="server"  ErrorMessage="Territory is required.">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="vldSalesRepID" ValidationGroup="AddTerritory" ControlToValidate="txtTerritory" runat="server" ErrorMessage="Sales Rep Territory ID must be 10 digits long." ValidationExpression="\d{10}?">*</asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="vldDistributorID" ValidationGroup="AddTerritory" ControlToValidate="txtTerritory" runat="server" ErrorMessage="ASD/DE/ESD Territory ID must be 6 digits long." ValidationExpression="\d{6}?">*</asp:RegularExpressionValidator>
                                            </FooterTemplate>
                                        </asp:TemplateField>                            
                                    </Columns>
                                </asp:GridView>
                            </td>
                         </tr>                        
                        <tr>
                            <td><asp:ValidationSummary ID="vldTerritorySummary" ValidationGroup="AddTerritory" Font-Size="8pt" ForeColor="#FF7300" runat="server" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2"><asp:Literal ID="ltlLine1" runat="server" Text="<hr />"></asp:Literal></td>
            </tr>

        </table>
    <table style="width:100%">
        <tr>
            <td style="width:50%; vertical-align:top"">
                <table style="width:100%">
                    <tr>
                        <td colspan="3">
                        <asp:Label ID="LabelGroupMembership" Font-Bold="true" Font-Underline="true" Font-Size="12pt" runat="server" Text="Group Membership"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:12%; vertical-align:top"><asp:Label ID="LabelGroups" Font-Bold="true" Font-Size="10pt" runat="server" Text="Groups:"></asp:Label></td>
                        <td style="width:17%; font-size:10pt; font-weight:bold; vertical-align:top">
                            <asp:DropDownList ID="ddlGroups" runat="server"  Font-Size="10pt" Width="150px"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="vldRequiredGroup" ValidationGroup="AddGroup" ControlToValidate="ddlGroups" runat="server" ForeColor="#FF7300" ErrorMessage="Group is required.">*</asp:RequiredFieldValidator>
                        </td>
                        <td style="text-align:left; width:20%"><asp:Button ID="btnAddGroup" runat="server" 
                                Text="Add Group" Width="120px" onclick="btnAddGroup_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td  style="vertical-align:top"><asp:Label ID="LabelMembership" Font-Bold="true" Font-Size="10pt" runat="server" Text="Membership:"></asp:Label></td>
                        <td style="vertical-align:top"><asp:ListBox ID="lstGroupMembership" Rows="4" 
                                runat="server" Width="150px"></asp:ListBox>
                                <asp:RequiredFieldValidator ID="vldRequiredGroupMember" ValidationGroup="RemoveGroup" ControlToValidate="lstGroupMembership" runat="server" ForeColor="#FF7300" ErrorMessage="Select a group to remove.">*</asp:RequiredFieldValidator>

                        </td>
                        <td style="text-align:left;vertical-align:top"><asp:Button ID="btnRemoveGroup" 
                                runat="server" Text="Remove Group" Width="120px" 
                                onclick="btnRemoveGroup_Click"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                                <asp:ValidationSummary ID="vldAddGroupSummary" ValidationGroup="AddGroup" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                            <asp:ValidationSummary ID="vldRemoveGroupSummary" ValidationGroup="RemoveGroup" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:100%; vertical-align:top">
                <table style="width:100%; vertical-align:top">
                    <tr>
                        <td><asp:Label ID="LabelReports" Font-Bold="true" Font-Underline="true" Font-Size="12pt" runat="server" Text="Reports List"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;"><asp:ListBox ID="lstReports" runat="server" Rows="15" Width="300px" Font-Size="8pt"></asp:ListBox></td>
                    </tr>
                </table>
            </td>
        </tr>
            <tr>
                <td colspan="2"><asp:Literal ID="ltlLine2" runat="server" Text="<hr />"></asp:Literal></td>
            </tr>
    </table>
 
    <table style="width:100%;">
        <asp:HiddenField ID="hdnSortDirection" runat="server" Value="descending" />
            <asp:HiddenField ID="hdnSortField" runat="server" Value="displayname" />
        <tr>
            <td colspan="4"><asp:Label ID="LabelActivity" Font-Bold="true" Font-Underline="true" Font-Size="12pt" runat="server" Text="Activity"></asp:Label></td>
        </tr>
        <tr>
            <td style="width:20%"><asp:Label ID="LabelStartDate" runat="server" Text="Start Date:&nbsp;(mm/dd/yyyy):*"></asp:Label></td>
            <td style="width:20%"><asp:Label ID="LabelEndDate" runat="server" Text="End Date:&nbsp;(mm/dd/yyyy):*"></asp:Label></td>
            <td><asp:Label ID="LabelRecords" runat="server" Text="Top Records:"></asp:Label></td>
            <td>&nbsp;</td>
                
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtStartDate" MaxLength="10" runat="server" Width="125px" ></asp:TextBox>
                <asp:ImageButton ID="imgStartCalendar" ImageUrl="~/Images/calendar.gif" runat="server" CausesValidation="false" onclick="imgStartCalendar_Click" />
                <asp:RegularExpressionValidator ID="vldStartDate" runat="server" ControlToValidate="txtStartDate" ValidationGroup="UserStat" 
                    ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})" ErrorMessage="Start Date Format (mm/dd/yyyy)">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="vldRequiredStartDate" ValidationGroup="UserStat" runat="server" ControlToValidate="txtStartDate" ErrorMessage="Start Date is required">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtEndDate" MaxLength="10" Width="125px" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgEndCalendar" ImageUrl="~/Images/calendar.gif" runat="server" CausesValidation="false" onclick="imgEndCalendar_Click" />
                <asp:RegularExpressionValidator ID="vldEndDate" runat="server" ControlToValidate="txtEndDate" ValidationGroup="UserStat" 
                    ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})" ErrorMessage="End Date Format (mm/dd/yyyy)">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="vldRequiredEndDate" runat="server" ValidationGroup="UserStat" ControlToValidate="txtEndDate" ErrorMessage="End Date is required">*</asp:RequiredFieldValidator>
            </td>
            <td><asp:DropDownList ID="ddlRecords" runat="server">
                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                    <asp:ListItem Value="25" Text="25"></asp:ListItem>
                    <asp:ListItem Value="50" Text="50"></asp:ListItem>
                    <asp:ListItem Value="100" Text="100"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td><uc2:waitbutton ID="btnRunReport" runat="server" CausesValidation="false" ToolTip="Click here to run the report."
                    Text="Run Report" WaitText="Processing..." Width="120px" 
                    onclick="btnRunReport_Click">
            </uc2:waitbutton>         
            </td>
        </tr>
        <tr>
            <td colspan="2"> 
                <asp:Calendar ID="calStartDate" runat="server" onselectionchanged="calStartDate_SelectionChanged"></asp:Calendar>
                <asp:Calendar ID="calEndDate" runat="server" onselectionchanged="calEndDate_SelectionChanged"></asp:Calendar>
            </td>
            <td colspan="2"><asp:ValidationSummary ID="vldSummary" ValidationGroup="UserStat" runat="server" /></td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:GridView ID="gvStats" runat="server" Width="800px" AutoGenerateColumns="False"  OnRowDataBound="GVStats_RowDataBound" OnRowEditing="GVStats_Edit"
                EmptyDataText="No data found." ShowFooter="false" AllowSorting="true" OnSorting="GV_Sorting" DataKeyNames="ReportTrackerId"
                    CellPadding="2" GridLines="Both">
                <RowStyle HorizontalAlign="left" />
                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                <Columns>
                    <asp:BoundField DataField="reportname" SortExpression="reportname" HeaderStyle-Width="50%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Report Name" />                       
                    <asp:BoundField DataField="createDate" SortExpression="createDate" HeaderStyle-Width="40%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Run Date" />                       
                    <asp:TemplateField HeaderStyle-Width="58%" HeaderText="Parameters">
                        <ItemTemplate>
                            <asp:LinkButton id="btnEdit" Width="75px" Height="18" runat="server" Text="View" CommandName="Edit"
								    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:GridView ID="gvParameters" runat="server" Width="300px" AutoGenerateColumns="False"
                                EmptyDataText="No data found." ShowFooter="false" CellPadding="2" GridLines="Both">
                                <RowStyle HorizontalAlign="left" />
                                <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                                <Columns>
                                    <asp:BoundField DataField="ParameterName" HeaderStyle-Width="35%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Name" />                       
                                    <asp:BoundField DataField="ParameterValue" HeaderStyle-Width="65%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Value" />                       
                                </Columns>
                            </asp:GridView> 
                        </EditItemTemplate>
                    </asp:TemplateField>                            

                </Columns>
            </asp:GridView> 
            </td>
        </tr>
    </table>

</asp:Content>
