﻿// --------------------------------------------------------------
// <copyright file="ReportsList.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalDecisions.CrystalReports.Engine;
    using CrystalDecisions.Shared;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;

    public partial class ReportsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            this.lblMessage.Text = string.Empty;
            if (!Page.IsPostBack)
            {
                this.CheckTempCount();
                
                if (Request.QueryString["ReportTrackerID"] != null)
                {
                    string strReportTrackerID = Request.QueryString["ReportTrackerID"];

                    if (Session["myReport" + strReportTrackerID] != null)
                    {
                        //Response.Write("<br>Found Report in Session");
                        ReportDocument report = (ReportDocument)Session["myReport" + strReportTrackerID];
                        string strReportName = report.Name;
                        report.Close();
                        report.Dispose();
                        GC.Collect();
                        //Response.Write("<br>Disposed of Report: " + strReportName);
                    }
                }

                AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));

                // Impersonate User; must be an admin
                if (Request.QueryString["impersonate"] != null && (myUser.RoleID == 1 || myUser.RoleID == 5))
                {
                    myUser = AppUsersEntity.GetAppUserByUsername(Request.QueryString["impersonate"]);
                    this.lblMessage.Text = "Impersonating: " + Request.QueryString["impersonate"];
                }


                if (Request.QueryString["msg"] != null)
                {
                    if(Request.QueryString["msg"].ToLower() == "timeout")
                        this.lblMessage.Text += "Your web session timed out.";
                }

                if (myUser != null)
                {
                    if (myUser.UserStatus.ToLower() == "active")
                    {
                        //the user is in sales and has territoies assigned
                        if (!AppUsersEntity.SalesWithNoTerritories(myUser))
                        {
                            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                            myParamters.Add(new SqlParameter("@username", myUser.UserName));
                            DataTable dtReportsList = SQLUtility.SqlExecuteQuery("sp_GetReportsListByUserName", myParamters);
                            this.ListReports(dtReportsList);
                        }
                        else
                        {
                            this.lblMessage.Text += "<br>You have no sales territories assigned to your Crystal profile.  Contact the Administrator.";
                        }
                    }
                }
            }
       }

        /// <summary>This method is intended to check how high the temp file count is getting compared to the max limit.</summary>
        protected void CheckTempCount()
        {
            try
            {
                bool blnCheckTempCount = Convert.ToBoolean(ConfigurationManager.AppSettings["CheckTempCount"]);
                if (blnCheckTempCount == true)
                {
                    DirectoryInfo d = new DirectoryInfo(ConfigurationManager.AppSettings["TempFolder"]);
                    FileInfo[] rptTempFiles = d.GetFiles("*.rpt");
                    Debug.WriteLine("rptTempFiles Count: " + rptTempFiles.Length);

                    int intTempCountWarning = 250;
                    if (ConfigurationManager.AppSettings["TempCountWarning"] != null)
                    {
                        intTempCountWarning = Convert.ToInt32(ConfigurationManager.AppSettings["TempCountWarning"]);

                        if (rptTempFiles.Length > intTempCountWarning)
                        {
                            string strSubject = ConfigurationManager.AppSettings["MaxReportsSubject"] + rptTempFiles.Length.ToString();
                            string strTo = ConfigurationManager.AppSettings["EmailTempCountWarningTo"];
                            string strFrom = ConfigurationManager.AppSettings["logFromEmail"];
                            string strBody = ConfigurationManager.AppSettings["MaxReportsBody"];
                        //    strBody = "To clear the *.rpt files in the temp folder, you can RDP into the server, usman-smwspvw12, open IIS and restart the web site, AsdApps";
                            Debug.WriteLine("strBody: " + strBody);
                            Utility.SendEmail(strTo, strFrom, string.Empty, strSubject, strBody, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException.HandleException(ex, Request);
            }
        }

 
        protected ArrayList GetHeadings(DataTable dtHeadings)
        {
            ArrayList lstHeadings = new ArrayList();
            string strCurrentHeading = string.Empty;
            
            foreach(DataRow r in dtHeadings.Rows)
            {
                if (r["header"].ToString() != strCurrentHeading)
                {
                    strCurrentHeading = r["header"].ToString();
                    lstHeadings.Add(strCurrentHeading);
                }
            }

            return lstHeadings;
        }

        protected void ListReports(DataTable dt)
        {
            int intColumnTotal = 4;
            ArrayList lstHeadings = this.GetHeadings(dt);

            int columnWidth = 100 / intColumnTotal;

            TableRow thRow = null;

            for (int x = 0; x < lstHeadings.Count; x++)
            {
                if (x % intColumnTotal == 0)
                {
                    if(x != 0)
                        this.tableReports.Rows.Add(thRow);
                    
                    thRow = new TableRow();
                    thRow.CssClass = "reportsListRow";

                }
                TableCell thCell = new TableCell();
                thCell.CssClass = "thCell";

                System.Web.UI.WebControls.Table cellTable = new System.Web.UI.WebControls.Table();
                cellTable.CssClass = "cellTable";

                TableRow headerRow = new TableRow();
                TableCell headerCell = new TableCell();

                headerCell.Text = lstHeadings[x].ToString();
                headerCell.Font.Bold = true;
                headerCell.Font.Underline = true;
                headerCell.CssClass = "reportsListHeaderCell";
                headerRow.Cells.Add(headerCell);
                cellTable.Rows.Add(headerRow);

                bool showReportHeaderColumn = false;

                DataRow[] rows = dt.Select("header = '" + lstHeadings[x].ToString() + "'", "publishedName");
                foreach (DataRow r in rows)
                {
                    string strAppFolder = Server.MapPath("");
                    string strReportPath = strAppFolder + "\\Reports\\" + r["reportFileName"].ToString().Trim().Replace(".rpt", "") + ".rpt";
                    Debug.WriteLine("strReportPath: " + strReportPath);

                    if (File.Exists(strReportPath))
                    {
                        TableRow detailRow = new TableRow();
                        detailRow.CssClass = "reportsListDetailRow";

                        TableCell detailCell = new TableCell();
                        detailCell.CssClass = "reportsListDetailCell";

                        HyperLink lnkReport = new HyperLink();
                        //                    lnkReport.Text = report.SummaryInfo.ReportTitle;
                        //lnkReport.Text = r["reportFileName"].ToString().Trim().Replace(".rpt", "");
                        lnkReport.Text = r["publishedName"].ToString();

                        string strNavigateURLPage = "~/ReportsViewer.aspx";
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["ParameterPage"].ToString()))
                            strNavigateURLPage = "~/ReportParameters.aspx";

                        lnkReport.NavigateUrl = strNavigateURLPage + "?ReportID=" + r["reportID"].ToString();

                        // Impersonate User; must be an admin
                        AppUsersEntity appUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));
                        if (Request.QueryString["impersonate"] != null && (appUser.RoleID == 1 || appUser.RoleID == 5))
                        {
                            lnkReport.NavigateUrl += "&impersonate=" + Request.QueryString["impersonate"];
                        }

                        //lnkReport.NavigateUrl = strNavigateURLPage + "?ReportName=" + r["reportFileName"].ToString().Trim().Replace(".rpt", "") + ".rpt";
                        detailCell.Controls.Add(lnkReport);

                        detailRow.Cells.Add(detailCell);
                        cellTable.Rows.Add(detailRow);

                        showReportHeaderColumn = true;
                    }

                }

                thCell.Controls.Add(cellTable);

                if(showReportHeaderColumn)
                    thRow.Cells.Add(thCell);


            }
            this.tableReports.Rows.Add(thRow);

        }

        //protected void ListReports()
        //{
        //    Debug.WriteLine("Reports Folder: " + Server.MapPath("~\\reports\\"));
        //    DirectoryInfo dirReports = new DirectoryInfo(Server.MapPath("~\\reports\\"));
        //    FileInfo[] files = dirReports.GetFiles("*.rpt");

        //    TableRow thRow = new TableRow();
        //    TableCell thCell = new TableCell();
        //    thCell.Text = "Reports List";
        //    thCell.Font.Bold = true;
        //    thCell.Font.Underline = true;

        //    this.tableReports.Rows.Clear();
        //    thRow.Cells.Add(thCell);
        //    this.tableReports.Rows.Add(thRow);

        //    foreach (FileInfo f in files)
        //    {
        //        Debug.WriteLine("File: " + f.FullName);
        //        TableRow tRow = new TableRow();
        //        TableCell tCell = new TableCell();

        //        ReportDocument report = new ReportDocument();
        //        report.Load(f.FullName);

        //        HyperLink lnkReport = new HyperLink();
        //        lnkReport.Text = report.SummaryInfo.ReportTitle;
        //        lnkReport.NavigateUrl = "~/ReportParameters.aspx?ReportName=" + f.Name;

        //        tCell.Controls.Add(lnkReport);
        //        tRow.Cells.Add(tCell);
        //        this.tableReports.Rows.Add(tRow);
        //    }
        //}


    }//// end class
}