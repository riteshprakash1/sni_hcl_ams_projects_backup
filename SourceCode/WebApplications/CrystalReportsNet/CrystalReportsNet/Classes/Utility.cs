﻿// --------------------------------------------------------------
// <copyright file="Utility.cs" company="Smith and Nephew">
//     Copyright (c) 2013 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet.Classes
{
    using System;
    using System.Configuration;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Mail;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Messaging;
    using System.Web;

    /// <summary>This class has some useful methods.</summary>
    public class Utility
    {
        /// <summary>Prevents a default instance of the <see cref="Utility"/> class from being created.</summary>
        private Utility()
        {
        }

        /// <summary>Gets the Authenticated User.</summary>
        /// <param name="req">The web session request.</param>
        /// <returns>username string</returns>
        public static string CurrentUser(System.Web.HttpRequest req)
        {
            string[] arrayAuth = req.ServerVariables["AUTH_USER"].Split('\\'); ;

            if (arrayAuth.Length > 1)
            {
                return arrayAuth[1];
            }
            else
            {
                return String.Empty;
            }
        }

        public delegate string SendEmailDelegate(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml);

        public static void GetResultsOnCallback(IAsyncResult ar)
        {
            SendEmailDelegate del = (SendEmailDelegate)((AsyncResult)ar).AsyncDelegate;
            try
            {
                string result;
                result = del.EndInvoke(ar);
                Debug.WriteLine("\nOn CallBack: result is " + result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\nOn CallBack, problem occurred: " + ex.Message);
            }
        }

        public static string SendEmailAsync(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml)
        {
            SendEmailDelegate dc = new SendEmailDelegate(Utility.SendEmail);
            AsyncCallback cb = new AsyncCallback(Utility.GetResultsOnCallback);
            IAsyncResult ar = dc.BeginInvoke(strTo, strFrom, strCc, strSubj, strBody, isHtml, cb, null);

            return "ok";
        }

        /// <summary>Sends the email.</summary>
        /// <param name="strTo">The to address.</param>
        /// <param name="strFrom">The from address.</param>
        /// <param name="strSubj">The subjubject.</param>
        /// <param name="strBody">The body text.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        public static string SendEmail(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml)
        {
            MailMessage message = new MailMessage();           
            MailAddress from = new MailAddress(strFrom);
            message.From = from;

            // Pipe delimited list of email addesses
            string[] emails = strTo.Split(Convert.ToChar("|"));

            foreach (string sEmail in emails)
            {
                MailAddress to = new MailAddress(sEmail);
                message.To.Add(to);
            }

            //// Specify the message content.
            Debug.WriteLine("From: " + message.From.Address);

            if (strCc != String.Empty)
            {
                message.CC.Add(strCc);
            }

            if (ConfigurationManager.AppSettings["ccEmail"] != null)
            {
                if (ConfigurationManager.AppSettings["ccEmail"] != String.Empty)
                {
                    message.CC.Add(ConfigurationManager.AppSettings["ccEmail"]);
                }
            }

            message.IsBodyHtml = isHtml;
            message.Subject = strSubj;
            message.Body = strBody;

            Debug.WriteLine("Body: " + message.Body);

            SmtpClient client = new SmtpClient();
            Debug.WriteLine("client: " + client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Debug.WriteLine(excm.Message);
                LogException.HandleException(excm, strSubj);
            }

            return "sent";
        }

    }//// end class
}