﻿// ------------------------------------------------------------------
// <copyright file="ReportEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Reports Entity. </summary>  
    public class ReportEntity
    {
        /// <summary>This is the ReportID.</summary>
        private int intReportID;

        /// <summary>This is the Report File Name.</summary>
        private string strReportFileName;

        /// <summary>This is the Published Name.</summary>
        private string strPublishedName;

        /// <summary>This is the Header.</summary>
        private string strHeader;

        /// <summary>This is the ConnectionKey.</summary>
        private string strConnectionKey;

        /// <summary>Initializes a new instance of the ReportEntity class.</summary>
        public ReportEntity()
        {
            this.Header = String.Empty;
            this.PublishedName = String.Empty;
            this.ReportFileName = String.Empty;
            this.ConnectionKey = String.Empty;
        }

        /// <summary>Gets or sets the ReportID.</summary>
        /// <value>The ReportID.</value>
        public int ReportID
        {
            get { return this.intReportID; }
            set { this.intReportID = value; }
        }

        /// <summary>Gets or sets the ReportFileName.</summary>
        /// <value>The ReportFileName.</value>
        public string ReportFileName
        {
            get { return this.strReportFileName; }
            set { this.strReportFileName = value; }
        }

        /// <summary>Gets or sets the Published Name.</summary>
        /// <value>The Published Name.</value>
        public string PublishedName
        {
            get { return this.strPublishedName; }
            set { this.strPublishedName = value; }
        }

        /// <summary>Gets or sets the Header.</summary>
        /// <value>The Header.</value>
        public string Header
        {
            get { return this.strHeader; }
            set { this.strHeader = value; }
        }

        /// <summary>Gets or sets the Connection Key.</summary>
        /// <value>The Connection Key.</value>
        public string ConnectionKey
        {
            get { return this.strConnectionKey; }
            set { this.strConnectionKey = value; }
        }
        

        /// <summary>Receives a Reports datarow and converts it to a ReportEntity.</summary>
        /// <param name="r">The ReportEntity DataRow.</param>
        /// <returns>ReportEntity</returns>
        protected static ReportEntity GetEntityFromDataRow(DataRow r)
        {
            ReportEntity re = new ReportEntity();

            if ((r["ReportID"] != null) && (r["ReportID"] != DBNull.Value))
                re.ReportID = Convert.ToInt32(r["ReportID"].ToString());

            re.PublishedName = r["PublishedName"].ToString();
            re.Header = r["Header"].ToString();
            re.ReportFileName = r["ReportFileName"].ToString();
            re.ConnectionKey = r["ConnectionKey"].ToString();

            return re;
        }

        /// <summary>Gets all reports and return a ReportEntity List</summary>
        /// <returns>The newly populated ReportEntity List</returns>
        public static List<ReportEntity> GetAllReports()
        {
            List<ReportEntity> reports = new List<ReportEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAllReports", parameters);

            foreach (DataRow r in dt.Rows)
            {
                ReportEntity entity = ReportEntity.GetEntityFromDataRow(r);
                reports.Add(entity);
            }

            return reports;
        }

        /// <summary>Gets the list of header from the list of reports</summary>
        /// <returns>The newly populated string List</returns>
        public static List<string> GetAllHeaders(List<ReportEntity> rpts)
        {
            List<string> lstHeaders = new List<string>();

            string strLastHeader = string.Empty;
            foreach (ReportEntity r in rpts)
            {
                if (strLastHeader != r.Header)
                {
                    lstHeaders.Add(r.Header);
                    strLastHeader = r.Header;
                }
            }

            return lstHeaders;
        }

        /// <summary>Gets ReportEntity by ReportID.</summary>
        /// <param name="intReportID">The ReportID.</param>
        /// <returns>The newly populated ReportEntity</returns>
        public static ReportEntity GetReportByReportID(int intReportID)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@reportID", intReportID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetReportByReportID", parameters);

            ReportEntity entity = new ReportEntity();
            if(dt.Rows.Count > 0)
            {
                entity = ReportEntity.GetEntityFromDataRow(dt.Rows[0]);
            }

            return entity;
        }

        /// <summary>Gets ReportEntity List of reports by Username.</summary>
        /// <param name="strUsername">The Username.</param>
        /// <returns>The newly populated ReportEntity List</returns>
        public static List<ReportEntity> GetReportsListByUserName(string strUsername)
        {
            List<ReportEntity> reports = new List<ReportEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@UserName", strUsername));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetReportsListByUserName", parameters);

            foreach (DataRow r in dt.Rows)
            {
                ReportEntity entity = ReportEntity.GetEntityFromDataRow(r);
                reports.Add(entity);
            }

            return reports;
        }

        /// <summary>Gets ReportEntity List of reports by UserID.</summary>
        /// <param name="intUserID">The UserID.</param>
        /// <returns>The newly populated ReportEntity List</returns>
        public static List<ReportEntity> GetReportsListByUserID(int intUserID)
        {
            List<ReportEntity> reports = new List<ReportEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@userID", intUserID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetReportsListByUserID", parameters);

            foreach (DataRow r in dt.Rows)
            {
                ReportEntity entity = ReportEntity.GetEntityFromDataRow(r);
                reports.Add(entity);
            }

            return reports;
        }

        /// <summary>Gets ReportEntity List of reports not in a GroupID.</summary>
        /// <param name="intGroupID">The Group ID.</param>
        /// <returns>The newly populated ReportEntity List</returns>
        public static List<ReportEntity> GetReportsNotInByGroupID(int intGroupID)
        {
            List<ReportEntity> reports = new List<ReportEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@groupID", intGroupID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetReportsNotInByGroupID", parameters);

            foreach (DataRow r in dt.Rows)
            {
                ReportEntity entity = ReportEntity.GetEntityFromDataRow(r);
                reports.Add(entity);
            }

            return reports;
        }

        /// <summary>Insert Report record</summary>
        /// <param name="r">the ReportEntity object</param>
        /// <returns>the reportentity with the new ID</returns>
        public static ReportEntity InsertReport(ReportEntity r)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            SqlParameter paramID = new SqlParameter("@reportId", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            parameters.Add(paramID);
            
            parameters.Add(new SqlParameter("@header", r.Header));
            parameters.Add(new SqlParameter("@reportFileName", r.ReportFileName));
            parameters.Add(new SqlParameter("@publishedName", r.PublishedName));
            parameters.Add(new SqlParameter("@connectionKey", r.ConnectionKey));

            parameters = SQLUtility.SqlExecuteNonQueryReturnParameters("sp_InsertReport", parameters);
            r.ReportID = Convert.ToInt32(paramID.Value.ToString());

            return r;
        }

        /// <summary>Update Report record</summary>
        /// <param name="r">the ReportEntity object</param>
        /// <returns>the count of the records Inserted</returns>
        public static int UpdateReport(ReportEntity r)
        {
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            parameters.Add(new SqlParameter("@reportId", r.ReportID));
            parameters.Add(new SqlParameter("@header", r.Header));
            parameters.Add(new SqlParameter("@reportFileName", r.ReportFileName));
            parameters.Add(new SqlParameter("@publishedName", r.PublishedName));
            parameters.Add(new SqlParameter("@connectionKey", r.ConnectionKey));

            int recordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateReport", parameters);

            return recordsUpdated;
        }



    } //// end class
}