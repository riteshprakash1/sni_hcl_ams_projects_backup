﻿// ------------------------------------------------------------------
// <copyright file="GroupReportsEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Group Reports Entity. </summary>  
    public class GroupReportsEntity
    {

        /// <summary>This is the GroupReportID.</summary>
        private int intGroupReportID;

        /// <summary>This is the GroupID.</summary>
        private int intGroupID;

        /// <summary>This is the Group Name.</summary>
        private string strGroupName;

        /// <summary>This is the ReportID.</summary>
        private int intReportID;

        /// <summary>This is the Report File Name.</summary>
        private string strReportFileName;

        /// <summary>This is the Published Name.</summary>
        private string strPublishedName;

        /// <summary>This is the Header.</summary>
        private string strHeader;

        /// <summary>Initializes a new instance of the GroupReportsEntity class.</summary>
        public GroupReportsEntity()
        {
            this.GroupName = String.Empty;
            this.Header = String.Empty;
            this.PublishedName = String.Empty;
            this.ReportFileName = String.Empty;
        }

        /// <summary>Gets or sets the GroupReportID.</summary>
        /// <value>The GroupReportID.</value>
        public int GroupReportID
        {
            get { return this.intGroupReportID; }
            set { this.intGroupReportID = value; }
        }

        /// <summary>Gets or sets the GroupID.</summary>
        /// <value>The GroupID.</value>
        public int GroupID
        {
            get { return this.intGroupID; }
            set { this.intGroupID = value; }
        }

        /// <summary>Gets or sets the GroupName.</summary>
        /// <value>The GroupName.</value>
        public string GroupName
        {
            get { return this.strGroupName; }
            set { this.strGroupName = value; }
        }

        /// <summary>Gets or sets the ReportID.</summary>
        /// <value>The ReportID.</value>
        public int ReportID
        {
            get { return this.intReportID; }
            set { this.intReportID = value; }
        }

        /// <summary>Gets or sets the ReportFileName.</summary>
        /// <value>The ReportFileName.</value>
        public string ReportFileName
        {
            get { return this.strReportFileName; }
            set { this.strReportFileName = value; }
        }

        /// <summary>Gets or sets the Published Name.</summary>
        /// <value>The Published Name.</value>
        public string PublishedName
        {
            get { return this.strPublishedName; }
            set { this.strPublishedName = value; }
        }

        /// <summary>Gets or sets the Header.</summary>
        /// <value>The Header.</value>
        public string Header
        {
            get { return this.strHeader; }
            set { this.strHeader = value; }
        }

        /// <summary>Receives a GroupReports datarow and converts it to a GroupReportsEntity.</summary>
        /// <param name="r">The GroupReports DataRow.</param>
        /// <returns>GroupReportsEntity</returns>
        protected static GroupReportsEntity GetEntityFromDataRow(DataRow r)
        {
            GroupReportsEntity gu = new GroupReportsEntity();

            if ((r["groupReportID"] != null) && (r["groupReportID"] != DBNull.Value))
                gu.GroupReportID = Convert.ToInt32(r["groupReportID"].ToString());

            if ((r["GroupID"] != null) && (r["GroupID"] != DBNull.Value))
                gu.GroupID = Convert.ToInt32(r["GroupID"].ToString());

            if ((r["ReportID"] != null) && (r["ReportID"] != DBNull.Value))
                gu.ReportID = Convert.ToInt32(r["ReportID"].ToString());

            gu.GroupName = r["GroupName"].ToString();
            gu.PublishedName = r["PublishedName"].ToString();
            gu.Header = r["Header"].ToString();
            gu.ReportFileName = r["ReportFileName"].ToString();

            return gu;
        }

        /// <summary>Gets GroupReportsEntity List by GroupID.</summary>
        /// <param name="intGroupID">The GroupID.</param>
        /// <returns>The newly populated GroupReportsEntity List</returns>
        public static List<GroupReportsEntity> GetGroupReportsByGroupID(int intGroupID)
        {
            List<GroupReportsEntity> groupReports = new List<GroupReportsEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@groupID", intGroupID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetGroupReportsByGroupID", parameters);
            Debug.WriteLine("groupUsers Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                GroupReportsEntity entity = GroupReportsEntity.GetEntityFromDataRow(r);
                groupReports.Add(entity);
            }

            return groupReports;
        }

        /// <summary>Gets GroupReportsEntity List by ReportID.</summary>
        /// <param name="intReportID">The Report.</param>
        /// <returns>The newly populated GroupReportsEntity List</returns>
        public static List<GroupReportsEntity> GetGroupReportsByReportID(int intReport)
        {
            List<GroupReportsEntity> groupReports = new List<GroupReportsEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@reportID", intReport));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetGroupReportsByReportID", parameters);

            foreach (DataRow r in dt.Rows)
            {
                GroupReportsEntity entity = GroupReportsEntity.GetEntityFromDataRow(r);
                groupReports.Add(entity);
            }

            return groupReports;
        }

        /// <summary>Adds a new GroupReport record</summary>
        /// <param name="intReportID">the reportID</param>
        /// <param name="intGroupID">the groupID</param>
        /// <returns>records updated count</returns>
        public static int AddReportToGroup(int intReportID, int intGroupID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            myParameters.Add(new SqlParameter("@reportID", intReportID));
            myParameters.Add(new SqlParameter("@groupID", intGroupID));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_InsertGroupReport", myParameters);

            return intRecordsUpdated;
        }

        /// <summary>Removes the GroupReport record by the GroupReportID</summary>
        /// <param name="intGroupReportID">the Group Report ID</param>
        /// <returns>records updated count</returns>
        public static int RemoveGroupReport(int intGroupReportID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            myParameters.Add(new SqlParameter("@groupReportID", intGroupReportID));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteGroupReport", myParameters);

            return intRecordsUpdated;
        }



    }
}