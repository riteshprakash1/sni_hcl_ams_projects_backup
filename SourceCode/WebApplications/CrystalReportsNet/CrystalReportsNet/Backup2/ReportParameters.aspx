﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportParameters.aspx.cs" Title="Report Parameters" Inherits="CrystalReportsNet.ReportParameters" %>
<%@ Register TagPrefix="uc1" Namespace="CrystalReportsNet.Classes" Assembly="CrystalReportsNet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table>
        <tr>
            <td style="font-weight:bold; font-size:12pt">Report Name: <asp:Label runat="server" ID="lblReportName"></asp:Label>
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>

        </tr>
        <tr>
            <td style="text-align:center"><asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Size="10pt"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvParameters" runat="server" Width="1000px" AutoGenerateColumns="false"  OnRowEditing="GV_Add" 
                    EmptyDataText="There are no report parameters. Click Run Report." ShowFooter="false" ShowHeader="true" CellPadding="4" CellSpacing="4" GridLines="Both" OnRowDeleting="GV_AddRange" 
                    DataKeyNames="ParameterName, MultipleValues, DiscreteOrRange, ParameterType, DefaultValues" AllowPaging="false" OnRowCancelingEdit="GV_AddDefault"
                     OnRowDataBound="GV_RowDataBound">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                   <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="38%" HeaderText="Parameter" ItemStyle-VerticalAlign="Top" ItemStyle-Font-Bold="true" ItemStyle-Font-Size="10pt">
                            <ItemTemplate>
                                 <asp:Label runat="server" ID="lblPromptText" Font-Size="8pt" Text='<%# Eval("PromptText")%>'></asp:Label>
                           </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="32%" HeaderText="Input Fields" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="reportParameterList">
                            <ItemTemplate>
                                 <asp:Label runat="server" ID="lblStart" Text="Start: " Width="40px"></asp:Label>
                                <asp:TextBox ID="txtStartDate" TabIndex="1" Font-Size="8pt" runat="server" Width="75px" ></asp:TextBox>
                                <asp:ImageButton ID="imgStartCalendar" ImageUrl="~/Images/calendar.gif" 
                                    runat="server" CausesValidation="false" onclick="imgStartCalendar_Click" />
                                 <asp:Literal ID="ltlDateBreak1" runat="server" Text="<br />"></asp:Literal>
                                <asp:RegularExpressionValidator ID="vldStartDate" Font-Size="8pt" runat="server" ValidationGroup="AddRange"
                                    ControlToValidate="txtStartDate" ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})">Format (mm/dd/yyyy)</asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="vldRequiredStartDate" Font-Size="8pt" runat="server" ControlToValidate="txtStartDate"  ValidationGroup="AddRange">Required</asp:RequiredFieldValidator>

                                 <asp:Literal ID="ltlDateBreak2" runat="server" Text="<br />"></asp:Literal>
                                <asp:Label runat="server" ID="lblEnd" Text="End: " Width="40px"></asp:Label>
                                <asp:TextBox ID="txtEndDate" TabIndex="2" Font-Size="8pt" runat="server" Width="75px"></asp:TextBox>
                                <asp:ImageButton ID="imgEndCalendar" ImageUrl="~/Images/calendar.gif" 
                                    runat="server" CausesValidation="false" onclick="imgEndCalendar_Click" />
                                <asp:Button ID="btnAddRange" TabIndex="3" CausesValidation="false" Font-Size="8pt" ToolTip="Add Range" runat="server" Text="Add" Width="40px" CommandName="Delete" />
                                 <asp:Literal ID="ltlDateBreak3" runat="server" Text="<br />"></asp:Literal>
                                <asp:RegularExpressionValidator ID="vldEndDate" Font-Size="8pt" runat="server" ValidationGroup="AddRange"
                                    ControlToValidate="txtEndDate" ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})">Format (mm/dd/yyyy)</asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="vldRequiredEndDate" Font-Size="8pt" runat="server" ControlToValidate="txtEndDate" ValidationGroup="AddRange">Required</asp:RequiredFieldValidator>

                                <asp:Literal ID="ltlDiscreteAndRangeBreak" runat="server" Text="<br />"></asp:Literal>
                                <asp:Label runat="server" ID="lblDiscrete" Text="Discrete: " Width="40px"></asp:Label>
                               <asp:TextBox ID="txtListValue" TabIndex="4" Width="75px" runat="server" Font-Size="8pt"></asp:TextBox>
                                <asp:Button ID="btnAddValue" TabIndex="5" CausesValidation="false" Font-Size="8pt" ToolTip="Add Value" runat="server" Text="Add" Width="40px" CommandName="Edit" />
                                <asp:RequiredFieldValidator ID="vldRequiredListValue" runat="server" ControlToValidate="txtListValue" ValidationGroup="AddListValue"
                                    Text="<br>Input required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="vldDataLength10" Font-Size="8pt" runat="server" ValidationGroup="AddListValue"
                                    ControlToValidate="txtListValue" Text="Must be 10 digits or 'all'" 
                                    ValidationExpression="\d{10}|(all)"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="vldDataLength6" Font-Size="8pt" runat="server" ValidationGroup="AddListValue"
                                    ControlToValidate="txtListValue" Text="Must be 6 digits or 'all'" 
                                    ValidationExpression="\d{6}|(all)"></asp:RegularExpressionValidator>
                                <asp:Literal ID="ltlDefaultValueBreak" runat="server" Text="<br />"></asp:Literal>
                                 <asp:ListBox ID="lstDefaultValues" Width="153px" Rows="3" runat="server" Font-Size="8pt"></asp:ListBox>
                                <asp:Button ID="btnAddDefault" CausesValidation="false" Font-Size="8pt" ToolTip="Add Default Value" runat="server" Text=" > " Width="40px" CommandName="Cancel" />
                                <asp:RequiredFieldValidator ID="vldRequiredDefaultSelect" runat="server" ControlToValidate="lstDefaultValues" ValidationGroup="AddDefaultValue"
                                    ErrorMessage="Selection required">Selection required</asp:RequiredFieldValidator>
                           </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30%" HeaderText="Selection" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="reportParameterList">
                            <ItemTemplate>
                                <asp:GridView ID="gvInput" runat="server" AutoGenerateColumns="False" ShowFooter="false" CellPadding="2" 
                                      OnRowDeleting="GV_RemoveEntry" GridLines="Both" DataKeyNames="ParameterName, DiscreteOrRange">
                                    <RowStyle HorizontalAlign="left"/>
                                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                                    <Columns>
                                       <asp:TemplateField HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Button ID="btnRemoveInput" CausesValidation="false" Font-Size="8pt" ToolTip="Remove Entry" runat="server" Text="Remove" Width="60px" CommandName="Delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>                            
                                       <asp:TemplateField HeaderStyle-Width="40%" HeaderText="Value/Start" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStart" Font-Size="8pt" runat="server" Text='<%# Eval("StartValue")%>' ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                            
                                       <asp:TemplateField HeaderStyle-Width="40%" HeaderText="End" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEnd" Font-Size="8pt" runat="server" Text='<%# Eval("EndValue")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                   </Columns>
                                </asp:GridView> 
                                <asp:CustomValidator ID="vldRequiredInput" Font-Size="8pt" runat="server" ValidationGroup="RunReport">Input Required</asp:CustomValidator>
                                <asp:Calendar ID="calStartDate" runat="server" onselectionchanged="calStartDate_SelectionChanged"></asp:Calendar>
                                <asp:Calendar ID="calEndDate" runat="server" onselectionchanged="calEndDate_SelectionChanged"></asp:Calendar>
                          </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
        <tr>
            <td>
                <uc1:WaitButton ID="btnRunReport" runat="server" CausesValidation="false" ToolTip="Click here to run the report."
                    OnClick="btnRunReport_Click" Text="Run Report" WaitText="Processing..."  Width="120px">
                </uc1:WaitButton>         
           </td>
        </tr>
    </table>
</asp:Content>
