﻿// ------------------------------------------------------------------
// <copyright file="AppUsersEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Application User's Entity. </summary>    
    public class AppUsersEntity
    {
        /// <summary>This is the UserID.</summary>
        private int intUserID;

        /// <summary>This is the Username.</summary>
        private string strUserName;

        /// <summary>This is the Lastname.</summary>
        private string strDisplayName;

        /// <summary>This is the RoleID.</summary>
        private int intRoleID;

        /// <summary>This is the Role Name.</summary>
        private string strRoleName;

        /// <summary>This is the User Status.</summary>
        private string strUserStatus;

        /// <summary>This is the Created By.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>This is the Modified By.</summary>
        private string strModifiedBy;

        /// <summary>This is the ModifiedDate.</summary>
        private string strModifiedDate;

        /// <summary>This is the GroupNames.</summary>
        private string strGroupNames;

        /// <summary>This is the Territories.</summary>
        private string strTerritories;

        /// <summary>Initializes a new instance of the AppUsersEntity class.</summary>
        public AppUsersEntity()
        {
            this.UserID = 0;
            this.UserName = String.Empty;
            this.DisplayName = String.Empty;
            this.UserStatus = String.Empty;
            this.GroupNames = String.Empty;

            this.CreatedBy = String.Empty;
            this.CreateDate = String.Empty;
            this.ModifiedBy = String.Empty;
            this.ModifiedDate = String.Empty;
            this.Territories = String.Empty;
        }

        /// <summary>Gets or sets the User ID.</summary>
        /// <value>The User ID.</value>
        public int UserID
        {
            get{ return this.intUserID;}
            set{this.intUserID = value;}
        }

        /// <summary>Gets tru/false whther user role is sales rep.</summary>
        /// <value>true/false</value>
        public bool IsSalesRep
        {
            get { return Convert.ToBoolean(this.intRoleID == 3); }
        }

        /// <summary>Gets tru/false whther user role is distributor.</summary>
        /// <value>true/false</value>
        public bool IsDistributor
        {
            get { return Convert.ToBoolean(this.intRoleID == 2); }
        }

        /// <summary>Gets tru/false whther user role is Administrator.</summary>
        /// <value>true/false</value>
        public bool IsAdministrator
        {
            get { return Convert.ToBoolean(this.intRoleID == 1); }
        }

        /// <summary>Gets tru/false whther user role is User Admin.</summary>
        /// <value>true/false</value>
        public bool IsUserAdmin
        {
            get { return Convert.ToBoolean(this.intRoleID == 5); }
        }
        
        /// <summary>Gets or sets the RoleID.</summary>
        /// <value>The RoleID.</value>
        public int RoleID
        {
            get { return this.intRoleID; }
            set { this.intRoleID = value; }
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get{ return this.strUserName;}
            set{this.strUserName = value;}
        }

        /// <summary>Gets or sets the Display Name.</summary>
        /// <value>The Display Name.</value>
        public string DisplayName
        {
            get { return this.strDisplayName; }
            set { this.strDisplayName = value; }
        }

        /// <summary>Gets or sets the Role Name.</summary>
        /// <value>The Role Name.</value>
        public string RoleName
        {
            get { return this.strRoleName; }
            set { this.strRoleName = value; }
        }

        /// <summary>Gets or sets the User Status.</summary>
        /// <value>The User Status.</value>
        public string UserStatus
        {
            get { return this.strUserStatus; }
            set { this.strUserStatus = value; }
        }

        /// <summary>Gets or sets the GroupNames.</summary>
        /// <value>The GroupNames.</value>
        public string GroupNames
        {
            get { return this.strGroupNames; }
            set { this.strGroupNames = value; }
        }

        /// <summary>Gets or sets the Territories.</summary>
        /// <value>The Territories.</value>
        public string Territories
        {
            get { return this.strTerritories; }
            set { this.strTerritories = value; }
        }
 
        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.</value>
        public string CreatedBy
        {
            get { return this.strCreatedBy; }
            set { this.strCreatedBy = value; }
        }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.</value>
        public string CreateDate
        {
            get { return this.strCreateDate; }
            set { this.strCreateDate = value; }
        }

        /// <summary>Gets or sets the ModifiedBy.</summary>
        /// <value>The ModifiedBy.</value>
        public string ModifiedBy
        {
            get { return this.strModifiedBy; }
            set { this.strModifiedBy = value; }
        }

        /// <summary>Gets or sets the ModifiedDate.</summary>
        /// <value>The ModifiedDate.</value>
        public string ModifiedDate
        {
            get { return this.strModifiedDate; }
            set { this.strModifiedDate = value; }
        }

        /// <summary>Gets the AppUsersEntity List by the ProgramID by the username.</summary>
        /// <param name="strUsername">The strUsername</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static AppUsersEntity GetAppUserByUsername(string strUsername)
        {
            
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@username", strUsername));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUserByUsername", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            AppUsersEntity myUser = null;
            if(dt.Rows.Count > 0)
            {
                myUser = AppUsersEntity.GetEntityFromDataRow(dt.Rows[0]);
                myUser.Territories = AppUsersEntity.GetAppUserTerritories(myUser.UserID);
            }

            return myUser;
        }

        /// <summary>Gets the AppUsersEntity List by the ProgramID by the username.</summary>
        /// <param name="strUsername">The strUsername</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static AppUsersEntity GetAppUserByUserID(string strUserID)
        {

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@UserID", Convert.ToInt32(strUserID)));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAppUserByUserID", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            AppUsersEntity myUser = new AppUsersEntity();
            if (dt.Rows.Count > 0)
            {
                myUser = AppUsersEntity.GetEntityFromDataRow(dt.Rows[0]);
                myUser.Territories = AppUsersEntity.GetAppUserTerritories(myUser.UserID);
            }

            return myUser;
        }

        /// <summary>Gets the AppUsersEntity List by the last Synch Date.</summary>
        /// <param name="strEndDate">The Last Synch Date</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAllUsersSinceLastSynchDate(string strEndDate)
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@endDate", strEndDate));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAllUsersSinceLastSynchDate", parameters);

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }


        

        /// <summary>Gets the Territories string for the UserID</summary>
        /// <param name="intUserID">The strUsername</param>
        /// <returns>The newly populated Territories string</returns>
        public static string GetAppUserTerritories(int intUserID)
        {
            List<UserTerritoryEntity> userTerritories = UserTerritoryEntity.GetUserTerritoriesByUserId(intUserID);
            string strTerritories = string.Empty;
            foreach (UserTerritoryEntity t in userTerritories)
            {
                strTerritories += t.Territory + ";";
            }

            return strTerritories;
        }

        
        /// <summary>Gets all the AppUsersEntity Collection.</summary>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAllUsers()
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetAllUsers", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Gets the Users not assigned to the GroupID.</summary>
        /// <param name="intGroupID">The Group ID.</param>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetUsersNotInByGroupID(int intGroupID)
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@groupID", intGroupID));

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUsersNotInByGroupID", parameters);
            //Debug.WriteLine("App Users Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Gets all the AppUsersEntity Collection.</summary>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAllUsersWithGroups()
        {
            List<AppUsersEntity> users = AppUsersEntity.GetAllUsers();

            List<GroupUsersEntity> groupUsers = GroupUsersEntity.GetGroupUsers();

            foreach (AppUsersEntity user in users)
            {
                user.GroupNames = GroupUsersEntity.GetGroupsByUsernameString(groupUsers, user.UserName);
            }

            return users;
        }


        /// <summary>Returns true if user is in sales (Sales Rep or Distributor) but no territories are assigned.</summary>
        /// <param name="u">The AppUsersEntity object.</param>
        /// <returns>true/false</returns>
        public static bool SalesWithNoTerritories(AppUsersEntity u)
        {
            bool blnFlag = false;
            if (u.IsDistributor || u.IsSalesRep)
            {
                List<UserTerritoryEntity> territories = UserTerritoryEntity.GetUserTerritoriesByUserId(u.UserID);
                if (territories.Count == 0)
                    blnFlag = true;
            }

            return blnFlag;
        }


        /// <summary>Receives a AppUsers datarow and converts it to a AppUsersEntity.</summary>
        /// <param name="r">The App User DataRow.</param>
        /// <returns>AppUsersEntity</returns>
        public static AppUsersEntity GetEntityFromDataRow(DataRow r)
        {
            AppUsersEntity appUser = new AppUsersEntity();

            if ((r["UserID"] != null) && (r["UserID"] != DBNull.Value))
                appUser.UserID = Convert.ToInt32(r["UserID"].ToString());

            if ((r["RoleID"] != null) && (r["RoleID"] != DBNull.Value))
                appUser.RoleID = Convert.ToInt32(r["RoleID"].ToString());
 
            appUser.CreateDate = r["CreatedDate"].ToString();
            appUser.CreatedBy = r["CreatedBy"].ToString();
            appUser.DisplayName = r["DisplayName"].ToString();
            appUser.UserStatus = r["UserStatus"].ToString();
            appUser.ModifiedBy = r["ModifiedBy"].ToString();
            appUser.ModifiedDate = r["ModifiedDate"].ToString();

            appUser.RoleName = r["RoleName"].ToString();
            appUser.UserName = r["UserName"].ToString();

            return appUser;
        }

        /// <summary>Inserts The App User into the database</summary>
        /// <param name="u">AppUsersEntity object</param>
        /// <returns>The AppUsers Entity</returns>
        public static AppUsersEntity InsertAppUser(AppUsersEntity u)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            SqlParameter SQLID = new SqlParameter("@userID", u.UserID);
            SQLID.Direction = ParameterDirection.Output;
            myParameters.Add(SQLID);

            myParameters.Add(new SqlParameter("@roleID", u.RoleID));
            myParameters.Add(new SqlParameter("@ModifiedBy", u.ModifiedBy));
            myParameters.Add(new SqlParameter("@displayName", u.DisplayName));
            myParameters.Add(new SqlParameter("@UserStatus", u.UserStatus));
            myParameters.Add(new SqlParameter("@username", u.UserName));

            SqlConnection conn = SQLUtility.OpenSqlConnection();
            SqlTransaction trans = conn.BeginTransaction();

            SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertAppUser", myParameters, conn, trans);

            if (insertParameteres["@userID"] != null)
            {
                u.UserID = Convert.ToInt32(insertParameteres["@userID"].Value.ToString());
                Debug.WriteLine("Inserted UserID: " + u.UserID.ToString());
            }

            trans.Commit();

            SQLUtility.CloseSqlConnection(conn);
            return u;
        }

        /// <summary>Updates The App User record in the database</summary>
        /// <param name="u">AppUsersEntity object</param>
        /// <returns>records updated count</returns>
        public static int UpdateAppUser(AppUsersEntity u)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            myParameters.Add(new SqlParameter("@userID", u.UserID));
            myParameters.Add(new SqlParameter("@roleID", u.RoleID));
            myParameters.Add(new SqlParameter("@ModifiedBy", u.ModifiedBy));
            myParameters.Add(new SqlParameter("@displayName", u.DisplayName));
            myParameters.Add(new SqlParameter("@UserStatus", u.UserStatus));
            myParameters.Add(new SqlParameter("@username", u.UserName));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateAppUser", myParameters);

            return intRecordsUpdated;
        }

        /// <summary>Delete the App User record in the database</summary>
        /// <param name="intUserID">the UserID</param>
        /// <returns>records updated count</returns>
        public static int DeleteAppUser(int intUserID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@userID", intUserID));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_DeleteAppUser", myParameters);

            return intRecordsUpdated;
        }

    }//// end class
}