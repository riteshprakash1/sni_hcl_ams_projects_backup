﻿// --------------------------------------------------------------
// <copyright file="ReportDetail.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet.Admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;

    /// <summary> This is the ReportDetail class for the ReportDetail page. </summary>  
    public partial class ReportDetail : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));
                //// if not an admin redirect to Unauthorized Page
                if ((myUser == null) || (myUser.UserStatus.ToLower() != "active"))
                {
                    Response.Redirect("~/ReportsList.aspx", true);
                }
                else
                {
                    if ((myUser == null) || (!myUser.IsAdministrator))
                    {
                        Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
                    }
                }

                this.LoadReportList();
                this.ddlReport.SelectedIndex = 0;

                this.LoadReportData();
            }

            this.SetControls();
            this.lblMsg.Text = string.Empty;
        }

        /// <summary>
        /// Gets the Connection String table.
        /// </summary>
        /// <returns>Connection String datatable</returns>
        private DataTable GetConnectionStringTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("SettingName", typeof(string)));
            dt.Columns.Add(new DataColumn("ConnectionString", typeof(string)));

            Configuration configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            ConnectionStringSettingsCollection settings = configuration.ConnectionStrings.ConnectionStrings;

            foreach (ConnectionStringSettings set in settings)
            {
                DataRow r = dt.NewRow();
                r["SettingName"] = set.Name;
                r["ConnectionString"] = set.ConnectionString;
                dt.Rows.Add(r);
                Debug.WriteLine("Name: " + set.Name + "; Value: " + set.ConnectionString);
            }

            return dt;
        }



        /// <summary>Load the Reports drop-down list</summary>
        protected void LoadReportList()
        {
            List<ReportEntity> reports = ReportEntity.GetAllReports();

            this.ddlReport.Items.Clear();
            ddlReport.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (ReportEntity r in reports)
            {
                this.ddlReport.Items.Add(new ListItem(r.Header + "-" + r.PublishedName, r.ReportID.ToString()));
            }

            List<string> lstHeaders = ReportEntity.GetAllHeaders(reports);
            ddlHeader.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (string s in lstHeaders)
            {
                this.ddlHeader.Items.Add(new ListItem(s));
            }

            DataTable dt = this.GetConnectionStringTable();
            this.ddlConnection.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in dt.Rows)
            {
                this.ddlConnection.Items.Add(new ListItem(r["SettingName"].ToString(), r["SettingName"].ToString()));
            }

        }

        /// <summary>Displays or hides controls based on the selected group index</summary>
        protected void SetControls()
        {
        //    vldRequiredGroupRemove.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);
            ddlGroups.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);
            btnRemoveGroup.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);
            vldRequiredAddGroup.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);
            //LabelGroups.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);
            this.btnAddGroup.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);
            this.chkLstGroups.Visible = Convert.ToBoolean(this.ddlReport.SelectedIndex != 0);

        }

        /// <summary>Loads the Group Controls.</summary>
        protected void LoadReportData()
        {
            if (this.ddlReport.SelectedIndex != 0)
            {
                ReportEntity rpt = ReportEntity.GetReportByReportID(Convert.ToInt32(this.ddlReport.SelectedValue));

                this.txtPublishedName.Text = rpt.PublishedName;
                this.txtReportFileName.Text = rpt.ReportFileName;

                this.ddlHeader.ClearSelection();
                if (this.ddlHeader.Items.FindByValue(rpt.Header) != null)
                {
                    ddlHeader.Items.FindByValue(rpt.Header).Selected = true;
                }

                this.ddlConnection.ClearSelection();
                if (this.ddlConnection.Items.FindByValue(rpt.ConnectionKey) != null)
                {
                    ddlConnection.Items.FindByValue(rpt.ConnectionKey).Selected = true;
                }

                DirectoryInfo d = new DirectoryInfo(Server.MapPath(""));
                string strReportPath = d.Parent.FullName + "\\Reports\\" + rpt.ReportFileName;
                this.lblReportLoaded.Text = File.Exists(strReportPath).ToString();
                //Debug.WriteLine("strReportPath: " + strReportPath);

                this.btnSubmit.Text = "Update Report";

                this.LoadGroups();
            }
            else
            {
                this.txtPublishedName.Text = string.Empty;
                this.txtReportFileName.Text = string.Empty;
                this.ddlHeader.ClearSelection();

                this.btnSubmit.Text = "Create Report";
            }

            this.SetControls();
        }


        /// <summary>Loads the Group Controls.</summary>
        protected void LoadGroups()
        {
            List<GroupReportsEntity> groupReports = GroupReportsEntity.GetGroupReportsByReportID(Convert.ToInt32(this.ddlReport.SelectedValue));

            this.chkLstGroups.Items.Clear();
            foreach (GroupReportsEntity gr in groupReports)
            {
                this.chkLstGroups.Items.Add(new ListItem(gr.GroupName, gr.GroupReportID.ToString()));
            }

            List<GroupEntity> groups = GroupEntity.GetGroupsNotInByReportID(Convert.ToInt32(this.ddlReport.SelectedValue));

            this.ddlGroups.Items.Clear();
            ddlGroups.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (GroupEntity g in groups)
            {
                this.ddlGroups.Items.Add(new ListItem(g.GroupName, g.GroupID.ToString()));
            }

            this.SetControls();

        }

        /// <summary>Handles the btnRemoveGroup_Click event by adding the selected report to the group.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnRemoveGroup_Click(object sender, EventArgs e)
        {
            Validate("RemoveGroup");

            if (Page.IsValid)
            {
                foreach (ListItem i in this.chkLstGroups.Items)
                {
                    if (i.Selected == true)
                    {
                        GroupReportsEntity.RemoveGroupReport(Convert.ToInt32(i.Value));
                    }
                }
                this.LoadGroups();
            }
        }

        /// <summary>Handles the btnAddGroup_Click event by adding the selected report to the group.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnAddGroup_Click(object sender, EventArgs e)
        {
            Validate("AddGroup");

            if (Page.IsValid)
            {
                GroupReportsEntity.AddReportToGroup(Convert.ToInt32(this.ddlReport.SelectedValue), Convert.ToInt32(this.ddlGroups.SelectedValue));
                this.LoadGroups();
            }

        }

        /// <summary>Handles the Report Selected Index Change event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadReportData();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate("Submit");

            if (Page.IsValid)
            {
                ReportEntity r = new ReportEntity();
                r.ReportFileName = this.txtReportFileName.Text;
                r.PublishedName = this.txtPublishedName.Text;
                r.Header = this.ddlHeader.SelectedValue;
                r.ConnectionKey = this.ddlConnection.SelectedValue;

                if (this.ddlReport.SelectedIndex == 0)
                {
                    r = ReportEntity.InsertReport(r);
                    this.lblMsg.Text = "Report Created";
                }
                else
                {
                    r.ReportID = Convert.ToInt32(this.ddlReport.SelectedValue);
                    ReportEntity.UpdateReport(r);
                    this.lblMsg.Text = "Report Updated";
                }
                
                this.LoadReportList();

                if (this.ddlReport.Items.FindByValue(r.ReportID.ToString()) != null)
                {
                    ddlReport.Items.FindByValue(r.ReportID.ToString()).Selected = true;
                }

                this.LoadReportData();
            
            } //// end if valid
        }



    }//// end class
}