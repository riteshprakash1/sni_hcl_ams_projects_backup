﻿// --------------------------------------------------------------
// <copyright file="UserList.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet.Admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;

    /// <summary>This is the User List page</summary>
    public partial class UserList : System.Web.UI.Page
    {

        protected AppUsersEntity myUser;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("Start Page_Load");
            myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));

            //// if not an admin redirect to Unauthorized Page
            if ((myUser == null) || (myUser.UserStatus.ToLower() != "active"))
            {
                Response.Redirect("~/ReportsList.aspx", true);
            }
            else
            {
                if (!myUser.IsAdministrator && !myUser.IsUserAdmin)
                {
                    Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
                }
            }

            if (!Page.IsPostBack)
            {
                this.BindDT();
                List<AppUsersEntity> appUsers = Session["AppUsers"] as List<AppUsersEntity>;

                this.ddlUsers.Items.Clear();
                this.ddlUsers.Items.Add(new ListItem("-- Select --", ""));
                foreach (AppUsersEntity u in appUsers)
                {
                    this.ddlUsers.Items.Add(new ListItem(u.DisplayName, u.UserID.ToString()));
                }


            }
            Debug.WriteLine("End Page_Load");
        } //// end Page_Load

        ///// <summary>Handles the Sorting event of the grid view control.</summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void GV_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {

            if (e.SortExpression.ToLower() == this.hdnSortField.Value.ToLower())
            {
                if (this.hdnSortDirection.Value == "ascending")
                    this.hdnSortDirection.Value = "descending";
                else
                    this.hdnSortDirection.Value = "ascending";
            }
            else
            {
                this.hdnSortField.Value = e.SortExpression.ToLower();
                this.hdnSortDirection.Value = "ascending";
            }

            List<AppUsersEntity> appUsers = null;
            if (Session["AppUsers"] != null)
            {
                appUsers = Session["AppUsers"] as List<AppUsersEntity>;
            }
            else
            {
                appUsers = AppUsersEntity.GetAllUsersWithGroups();           
            }

            appUsers = this.SortGridView(appUsers);
            Session["AppUsers"] = appUsers;
            this.BindDT();
        }


        protected List<AppUsersEntity> SortGridView(List<AppUsersEntity> userList)
        {
            Func<AppUsersEntity, object> prop = null;
            switch (this.hdnSortField.Value)
            {
                case "displayname":
                    {
                        prop = p => p.DisplayName;
                        break;
                    }
                case "username":
                    {
                        prop = p => p.UserName;
                        break;
                    }
                case "userstatus":
                    {
                        prop = p => p.UserStatus;
                        break;
                    }
                case "rolename":
                    {
                        prop = p => p.RoleName;
                        break;
                    }
                case "groupnames":
                    {
                        prop = p => p.GroupNames;
                        break;
                    }
                default:
                    {
                        prop = p => p.DisplayName;
                        break;
                    }
            }

            
            Func<IEnumerable<AppUsersEntity>, Func<AppUsersEntity, object>, IEnumerable<AppUsersEntity>> func = null;
            Debug.WriteLine("SortDirection: " + this.hdnSortDirection.Value);

            switch (this.hdnSortDirection.Value)
            {
                case "ascending":
                    {
                        func = (c, p) => c.OrderBy(p);
                        break;
                    }
                case "descending":
                    {
                        func = (c, p) => c.OrderByDescending(p);
                        break;
                    }
            }

            IEnumerable<AppUsersEntity> users = userList as IEnumerable<AppUsersEntity>;

            users = func(users, prop);

            List<AppUsersEntity> sortedUsers = users.ToList<AppUsersEntity>();

            return sortedUsers;
        }

        /// <summary>Binds the users table to the gvUsers GridView.</summary>
        protected void BindDT()
        {
         //   DataTable dtUsers = SQLUtility.SqlExecuteQuery("sp_GetAdminUsers");
            List<AppUsersEntity> appUsers = null;

            if (Session["AppUsers"] == null)
            {
                appUsers = AppUsersEntity.GetAllUsersWithGroups();
                Session["AppUsers"] = appUsers;
            }
            else
            {
                appUsers = Session["AppUsers"] as List<AppUsersEntity>;
            }

            if (appUsers.Count == 0)
                appUsers.Add(new AppUsersEntity());

            this.gvUser.DataSource = appUsers;
            this.gvUser.DataBind();
        }

        /// <summary>Handles the PageIndexChanging event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.gvUser.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtUserName = (TextBox)this.gvUser.FooterRow.FindControl("txtAddUserName");
            DropDownList ddlRole = (DropDownList)this.gvUser.FooterRow.FindControl("ddlRole");
            CustomValidator vldUserExists = (CustomValidator)this.gvUser.FooterRow.FindControl("vldUserExists");
            CustomValidator vldAddUserInAD = (CustomValidator)this.gvUser.FooterRow.FindControl("vldAddUserInAD");

            AppUsersEntity testUser = AppUsersEntity.GetAppUserByUsername(txtUserName.Text);
            vldUserExists.IsValid = Convert.ToBoolean(testUser.UserID == 0);

            ADUserEntity myUser = null;

            if (txtUserName.Text != String.Empty)
            {
                myUser = ADUtility.GetADUserByUserName(txtUserName.Text);
                vldAddUserInAD.IsValid = Convert.ToBoolean(myUser != null);
            }

            if (Page.IsValid)
            {
                AppUsersEntity newUser = new AppUsersEntity();
                newUser.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                newUser.UserName = txtUserName.Text;
                newUser.DisplayName = myUser.DisplayName;
                newUser.UserStatus = myUser.FirstName;
                newUser.ModifiedBy = Utility.CurrentUser(Request);

                AppUsersEntity.InsertAppUser(newUser);

                this.gvUser.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid

        }  ////end GV_Add

        /// <summary>Handles the RowDataBound event of the gvUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType  == DataControlRowType.DataRow)
            {
                Label lblUserName = (Label)e.Row.FindControl("lblUserName");
                string strRoleID = this.gvUser.DataKeys[e.Row.RowIndex]["roleID"].ToString();
                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                if (lblUserName.Text == string.Empty)
                    e.Row.Visible = false;

                if (!myUser.IsAdministrator && strRoleID == "1")
                {
                    btnEdit.Enabled = false;
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddUserName = (TextBox)e.Row.FindControl("txtAddUserName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddUserName, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the gvUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intID = Int32.Parse(this.gvUser.DataKeys[e.RowIndex].Value.ToString()); 

            AppUsersEntity.DeleteAppUser(intID);

            this.gvUser.EditIndex = -1;
            this.BindDT();
        }


        /// <summary>Gets the UserRoles datatable.</summary>
        /// <returns>the UserRolesEntity collection</returns>
        public List<UserRolesEntity> GetRoles()
        {
            List<UserRolesEntity> roles = UserRolesEntity.GetUserRoles();
            return roles;
        }

        /// <summary>Handles the Edit event of the gvUser GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            string strUserID = this.gvUser.DataKeys[e.NewEditIndex]["userID"].ToString();
            Response.Redirect("UserDetail.aspx?userID=" + strUserID, true);
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserDetail.aspx", true);
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlUsers.SelectedIndex != 0)
            {
                string strUserID = this.ddlUsers.SelectedValue;
                Response.Redirect("UserDetail.aspx?userID=" + strUserID, true);
            }

        }

    } //// end class
}
