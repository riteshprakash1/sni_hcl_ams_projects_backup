﻿// --------------------------------------------------------------
// <copyright file="Stats.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;
    
    /// <summary>This is the User List page</summary>
    public partial class Stats : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                Session["Stats"] = null;
                this.calStartDate.Visible = false;
                this.calEndDate.Visible = false;

                txtEndDate.Text = DateTime.Now.ToShortDateString();
                txtStartDate.Text = DateTime.Now.ToShortDateString();

                List<AppUsersEntity> users = AppUsersEntity.GetAllUsers();

                EnterButton.TieButton(this.txtStartDate, this.btnRunReport);
                EnterButton.TieButton(this.txtEndDate, this.btnRunReport);

            }

        }//// end Page_Load

        /// <summary>Handles the Start Calendar Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The ImageClickEventArgs instance containing the event data.</param>
        protected void imgStartCalendar_Click(object sender, ImageClickEventArgs e)
        {
            Debug.WriteLine("Sender: " + sender.ToString());
            this.calStartDate.Visible = !calStartDate.Visible;
            if (this.calStartDate.Visible)
                this.calEndDate.Visible = false;
        }

        /// <summary>Handles the Start Calendar SelectionChanged event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void calStartDate_SelectionChanged(object sender, EventArgs e)
        {
            txtStartDate.Text = calStartDate.SelectedDate.ToShortDateString();
            calStartDate.Visible = false;
        }

        /// <summary>Handles the End Calendar Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The ImageClickEventArgs instance containing the event data.</param>
        protected void imgEndCalendar_Click(object sender, ImageClickEventArgs e)
        {
            Debug.WriteLine("Sender: " + sender.ToString());
            this.calEndDate.Visible = !calEndDate.Visible;
            if (this.calEndDate.Visible)
                this.calStartDate.Visible = false;
        }

        /// <summary>Handles the End Calendar SelectionChanged event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void calEndDate_SelectionChanged(object sender, EventArgs e)
        {
            txtEndDate.Text = calEndDate.SelectedDate.ToShortDateString();
            calEndDate.Visible = false;
        }

        /// <summary>Handles the Sorting event of the grid view control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void GV_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {

            if (e.SortExpression.ToLower() == this.hdnSortField.Value.ToLower())
            {
                if (this.hdnSortDirection.Value == "ascending")
                    this.hdnSortDirection.Value = "descending";
                else
                    this.hdnSortDirection.Value = "ascending";
            }
            else
            {
                this.hdnSortField.Value = e.SortExpression.ToLower();
                this.hdnSortDirection.Value = "ascending";
            }

            List<StatsEntity> stats = null;
            if (Session["Stats"] != null)
            {
                stats = Session["Stats"] as List<StatsEntity>;
            }
            else
            {
                stats = StatsEntity.GetStatistics(txtStartDate.Text, txtEndDate.Text, this.ddlReports.SelectedValue);
            }

            stats = this.SortGridView(stats);
            Session["Stats"] = stats;
            this.BindDT();
        }


        protected List<StatsEntity> SortGridView(List<StatsEntity> statsList)
        {
            Func<StatsEntity, object> prop = null;  //processings, max(CreateDate) as lastRun, DisplayName, header, publishedName
            switch (this.hdnSortField.Value)
            {
                case "displayname":
                    {
                        prop = p => p.DisplayName;
                        break;
                    }
                case "lastrun":
                    {
                        prop = p => p.LastRun;
                        break;
                    }
                case "header":
                    {
                        prop = p => p.Header;
                        break;
                    }
                case "publishedname":
                    {
                        prop = p => p.PublishedName;
                        break;
                    }
                case "processings":
                    {
                        prop = p => p.Processings;
                        break;
                    }
                default:
                    {
                        prop = p => p.Processings;
                        break;
                    }
            }


            Func<IEnumerable<StatsEntity>, Func<StatsEntity, object>, IEnumerable<StatsEntity>> func = null;
            Debug.WriteLine("SortDirection: " + this.hdnSortDirection.Value);

            switch (this.hdnSortDirection.Value)
            {
                case "ascending":
                    {
                        func = (c, p) => c.OrderBy(p);
                        break;
                    }
                case "descending":
                    {
                        func = (c, p) => c.OrderByDescending(p);
                        break;
                    }
            }

            IEnumerable<StatsEntity> stats = statsList as IEnumerable<StatsEntity>;

            stats = func(stats, prop);

            List<StatsEntity> sortedStats = stats.ToList<StatsEntity>();

            return sortedStats;
        }

        /// <summary>Binds the stats list to the gvStats GridView.</summary>
        protected void BindDT()
        {
            //   DataTable dtUsers = SQLUtility.SqlExecuteQuery("sp_GetAdminUsers");
            List<StatsEntity> stats = null;

            if (Session["Stats"] == null)
            {
                stats = StatsEntity.GetStatistics(txtStartDate.Text, txtEndDate.Text, this.ddlReports.SelectedValue);
                Session["Stats"] = stats;
            }
            else
            {
                stats = Session["Stats"] as List<StatsEntity>;
            }

            if (stats.Count == 0)
                stats.Add(new StatsEntity());

            this.gvStats.DataSource = stats;
            this.gvStats.DataBind();
        }

        /// <summary>Handles the btnRunReport Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnRunReport_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid)
            {
                Session["Stats"] = null;
                this.BindDT();
            }
        }

        ///// <summary>Handles the Edit event of the gvStats GridView control.</summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        //protected void GV_Edit(object sender, GridViewEditEventArgs e)
        //{
        //    //string strUserID = this.gvStats.DataKeys[e.NewEditIndex]["userID"].ToString();
        //    //Response.Redirect("UserDetail.aspx?userID=" + strUserID, true);
        //}

        ///// <summary>Handles the RowDataBound event of the gvStats GridView control.</summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        //protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        //{

        //} // end gv_RowDataBound


    } //// end class
}