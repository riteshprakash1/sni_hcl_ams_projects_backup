﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalReportsNet.Classes;
using CrystalReportsNet.Classes.Entity;

namespace CrystalReportsNet.Admin
{
    public partial class GroupDetail : System.Web.UI.Page
    {
 
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));
                //// if not an admin redirect to Unauthorized Page
                if ((myUser == null) || (myUser.UserStatus.ToLower() != "active"))
                {
                    Response.Redirect("~/ReportsList.aspx", true);
                }
                else
                {
                    if ((myUser == null) || (!myUser.IsAdministrator))
                    {
                        Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
                    }
                }

                // remove the session object in case up dates are made
                Session["AppUsers"] = null;

                List<GroupEntity> groups = GroupEntity.GetGroupAllGroups();

                this.ddlGroup.Items.Clear();
                ddlGroup.Items.Add(new ListItem("-- Select --", String.Empty));
                foreach (GroupEntity g in groups)
                {
                    this.ddlGroup.Items.Add(new ListItem(g.GroupName, g.GroupID.ToString()));
                }

            }

            this.SetControls();

        }

        /// <summary>Handles the Group Selected Index Change event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.ddlGroup.SelectedIndex != 0)
            {
                this.LoadUsers();

                this.LoadReports();
            }

            this.SetControls();
        }


        /// <summary>Displays or hides controls based on the selected group index</summary>
        protected void SetControls()
        {
            this.LabelUsers.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
    //        this.lstUsers.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.chkLstUser.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.LabelReports.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.chkLstReports.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.ddlReports.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.ddlUsers.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.btnAddReport.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.btnAddUser.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.btnRemoveReport.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.btnRemoveUser.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.vldRequiredAddReport.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.vldRequiredAddUser.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.LabelGroupUsers.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
            this.LabelGroupReports.Visible = Convert.ToBoolean(this.ddlGroup.SelectedIndex != 0);
        }

        /// <summary>Loads the User Controls.</summary>
        protected void LoadUsers()
        {
            List<GroupUsersEntity> groupUsers = GroupUsersEntity.GetGroupUsersByGroupID(Convert.ToInt32(this.ddlGroup.SelectedValue));

            this.chkLstUser.Items.Clear();
            foreach (GroupUsersEntity gu in groupUsers)
            {
             //   this.lstUsers.Items.Add(new ListItem(gu.DisplayName, gu.GroupUserID.ToString()));
                this.chkLstUser.Items.Add(new ListItem(gu.DisplayName, gu.GroupUserID.ToString()));
            }

            List<AppUsersEntity> users = AppUsersEntity.GetUsersNotInByGroupID(Convert.ToInt32(this.ddlGroup.SelectedValue));

            this.ddlUsers.Items.Clear();
            ddlUsers.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (AppUsersEntity u in users)
            {
                this.ddlUsers.Items.Add(new ListItem(u.DisplayName, u.UserID.ToString()));
            }

        }

        /// <summary>Loads the Report Controls.</summary>
        protected void LoadReports()
        {
            List<GroupReportsEntity> groupReports = GroupReportsEntity.GetGroupReportsByGroupID(Convert.ToInt32(this.ddlGroup.SelectedValue));

            this.chkLstReports.Items.Clear();
            foreach (GroupReportsEntity gr in groupReports)
            {
                this.chkLstReports.Items.Add(new ListItem(gr.Header + "-" + gr.PublishedName, gr.GroupReportID.ToString()));
            }

            List<ReportEntity> reports = ReportEntity.GetReportsNotInByGroupID(Convert.ToInt32(this.ddlGroup.SelectedValue));

            this.ddlReports.Items.Clear();
            ddlReports.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (ReportEntity r in reports)
            {
                this.ddlReports.Items.Add(new ListItem(r.Header + "-" + r.PublishedName, r.ReportID.ToString()));
            }

        }
        
        /// <summary>Handles the btnAddReport_Click event by adding the selected report to the group.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnAddReport_Click(object sender, EventArgs e)
        {
            Validate("AddReport");

            if (Page.IsValid)
            {
                GroupReportsEntity.AddReportToGroup(Convert.ToInt32(this.ddlReports.SelectedValue), Convert.ToInt32(this.ddlGroup.SelectedValue));
                this.LoadReports();
            }

        }

        /// <summary>Handles the btnRemoveReport_Click event by removing the selected report to the group.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnRemoveReport_Click(object sender, EventArgs e)
        {
            Validate("RemoveReport");

            if (Page.IsValid)
            {
                foreach (ListItem i in this.chkLstReports.Items)
                {
                    if (i.Selected == true)
                    {
                        GroupReportsEntity.RemoveGroupReport(Convert.ToInt32(i.Value));
                    }
                }
                this.LoadReports();
            }

        }

        /// <summary>Handles the btnAddUser_Click event by adding the selected user to the group.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            Validate("AddUser");

            if (Page.IsValid)
            {
                GroupUsersEntity.AddUserToGroup(Convert.ToInt32(this.ddlUsers.SelectedValue), Convert.ToInt32(this.ddlGroup.SelectedValue));
                this.LoadUsers();
            }
        }

        /// <summary>Handles the btnRemoveUser_Click event by removing the selected user from the group.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnRemoveUser_Click(object sender, EventArgs e)
        {
            Validate("RemoveUser");

            if (Page.IsValid)
            {
                foreach (ListItem i in this.chkLstUser.Items)
                {
                    if (i.Selected == true)
                    {
                        GroupUsersEntity.RemoveGroupUser(Convert.ToInt32(i.Value));
                    }
                }
                this.LoadUsers();
            }
        }

    } //// end class
}