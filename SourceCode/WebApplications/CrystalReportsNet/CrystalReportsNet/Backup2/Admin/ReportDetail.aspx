﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportDetail.aspx.cs" Inherits="CrystalReportsNet.Admin.ReportDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width:100%;">

        <tr>
            <td style="width:40%; vertical-align:top">
                <table style="width:100%;">
                    <tr>
                        <td style="width:25%; font-weight:bold">Select Report:</td>
                        <td><asp:DropDownList ID="ddlReport" runat="server"  Width="200px" Font-Size="8pt" 
                                 AutoPostBack="true"  onselectedindexchanged="ddlReport_SelectedIndexChanged"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold">Report File Name:</td>
                        <td><asp:TextBox ID="txtReportFileName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="vldRequiredReportFileName" ValidationGroup="Submit" ControlToValidate="txtReportFileName" runat="server" ForeColor="#FF7300" ErrorMessage="Required">Required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold">Published Name:</td>
                        <td><asp:TextBox ID="txtPublishedName" MaxLength="50" Font-Size="8pt" runat="server" Width="200px"></asp:TextBox>&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="vldRequiredPublishedName" ValidationGroup="Submit" ControlToValidate="txtPublishedName" runat="server" ForeColor="#FF7300" ErrorMessage="Required">Required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold">Header Name:</td>
                        <td><asp:DropDownList ID="ddlHeader" Font-Size="8pt" runat="server" Width="200px"></asp:DropDownList>&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="vldRequiredHeader" ValidationGroup="Submit" ControlToValidate="ddlHeader" runat="server" ForeColor="#FF7300" ErrorMessage="Required">Required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold">Connection:</td>
                        <td><asp:DropDownList ID="ddlConnection" Font-Size="8pt" runat="server" Width="200px"></asp:DropDownList>&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="vldRequiredConnection" ValidationGroup="Submit" ControlToValidate="ddlConnection" runat="server" ForeColor="#FF7300" ErrorMessage="Required">Required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold">Report Loaded:</td>
                        <td><asp:Label ID="lblReportLoaded" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align:center" colspan="2"><asp:Button ID="btnSubmit" Font-Size="8pt" runat="server" 
                                Text="Update Report" onclick="btnSubmit_Click" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:center" colspan="2"><asp:Label ID="lblMsg" ForeColor="#FF7300" runat="server"></asp:Label></td>
                    </tr>
           
                </table>
            </td>
            <td style="width:50%;">
                <table style="width:100%;">
                    <tr>
                        <td style="width:20%;vertical-align:top; font-weight:bold"><asp:Label ID="LabelGroups" runat="server" Text="Groups:"></asp:Label>
                            <br /><br /><br /><asp:Button ID="btnRemoveGroup" runat="server" 
                                Text="Remove Group" Font-Size="8pt" Width="110px" 
                                onclick="btnRemoveGroup_Click"/>
                        </td>
                        <td>
                            <div style="border:1px solid black; width:200px;height:180px;margin:0px;padding:0px;overflow:auto; vertical-align:top">
                                <asp:CheckBoxList ID="chkLstGroups" runat="server" CellPadding="0" CellSpacing="0" Width="190px" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">
                            <asp:Button ID="btnAddGroup" runat="server" Text="Add Group" 
                                 Width="110px" Font-Size="8pt" onclick="btnAddGroup_Click" /></td>
                        <td style="vertical-align:top"><asp:DropDownList ID="ddlGroups" runat="server" Width="200px" Font-Size="8pt"></asp:DropDownList>
                            <br /><asp:RequiredFieldValidator ID="vldRequiredAddGroup" ValidationGroup="AddGroup" ControlToValidate="ddlGroups" runat="server" ForeColor="#FF7300" ErrorMessage="Select Group">Select Group</asp:RequiredFieldValidator>
                        </td>            
                    </tr>
                </table>                
            </td>

        </tr>
    </table>
</asp:Content>
