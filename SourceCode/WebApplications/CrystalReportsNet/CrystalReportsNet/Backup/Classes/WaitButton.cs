﻿// --------------------------------------------------------------
// <copyright file="WaitButton.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet.Classes
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// This is a button that will set the text value to wait and disable itself when a form is being submitted.
    /// </summary>
    public class WaitButton : System.Web.UI.WebControls.Button
    {
        /// <summary>
        /// This is the text when our button is waiting...
        /// </summary>
        private string waitText = "Wait...";

        /// <summary>
        /// Initializes a new instance of the <see cref="WaitButton"/> class.
        /// </summary>
        public WaitButton()
        {
        }

        /// <summary>
        /// Gets or sets the text displayed by the control after it has been clicked.
        /// </summary>
        [DefaultValue("Please wait...")]
        [Description("The text displayed by the button after it has been clicked.")]
        [Category("Appearance")]
        public string WaitText
        {
            get
            {
                return this.waitText;
            }

            set
            {
                this.waitText = value;
            }
        }

        /// <summary>
        /// Determines whether the button has been clicked prior to rendering on the client.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //// sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //// sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
            sb.AppendFormat("this.value = '{0}';", this.waitText);
            sb.Append("this.disabled = true;");
            sb.Append("document.body.style.cursor='wait';");
            //// sb.Append(this.Page.GetPostBackEventReference(this));
            sb.Append(Page.ClientScript.GetPostBackEventReference(this, String.Empty));
            //// sb.Append(";");
            this.Attributes["onclick"] = sb.ToString();
            base.OnPreRender(e);
        }
    } //// END class WaitButton
}
