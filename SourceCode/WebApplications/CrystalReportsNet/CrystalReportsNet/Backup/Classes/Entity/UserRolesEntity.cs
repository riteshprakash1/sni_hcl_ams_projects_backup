﻿// ------------------------------------------------------------------
// <copyright file="UserRolesEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrystalReportsNet.Classes.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the User Roles Entity. </summary>    
    public class UserRolesEntity
    {

        /// <summary>This is the RoleID.</summary>
        private int intRoleID;

        /// <summary>This is the Role Name.</summary>
        private string strRoleName;

        /// <summary>Initializes a new instance of the UserRolesEntity class.</summary>
        public UserRolesEntity()
        {
            this.RoleName = String.Empty;
        }

        /// <summary>Gets or sets the RoleID.</summary>
        /// <value>The RoleID.</value>
        public int RoleID
        {
            get { return this.intRoleID; }
            set { this.intRoleID = value; }
        }

        /// <summary>Gets or sets the RoleName.</summary>
        /// <value>The RoleName.</value>
        public string RoleName
        {
            get { return this.strRoleName; }
            set { this.strRoleName = value; }
        }

        /// <summary>Receives a UserRole datarow and converts it to a UserRolesEntity.</summary>
        /// <param name="r">The UserRole DataRow.</param>
        /// <returns>UserRolesEntity</returns>
        protected static UserRolesEntity GetEntityFromDataRow(DataRow r)
        {
            UserRolesEntity role = new UserRolesEntity();

            if ((r["RoleID"] != null) && (r["RoleID"] != DBNull.Value))
                role.RoleID = Convert.ToInt32(r["RoleID"].ToString());

            role.RoleName = r["RoleName"].ToString();

            return role;
        }

        /// <summary>Gets all the UserRoles List.</summary>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<UserRolesEntity> GetUserRoles()
        {
            List<UserRolesEntity> roles = new List<UserRolesEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = SQLUtility.SqlExecuteQuery("sp_GetUserRoles", parameters);
            Debug.WriteLine("Role Rows: " + dt.Rows.Count.ToString());

            foreach (DataRow r in dt.Rows)
            {
                UserRolesEntity myRole = UserRolesEntity.GetEntityFromDataRow(r);
                roles.Add(myRole);
            }

            return roles;
        }

    }//// end class
}