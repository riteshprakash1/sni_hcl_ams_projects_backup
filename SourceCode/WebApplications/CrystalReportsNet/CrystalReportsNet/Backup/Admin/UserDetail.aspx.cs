﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalReportsNet.Classes;
using CrystalReportsNet.Classes.Entity;

namespace CrystalReportsNet.Admin
{
    public partial class UserDetail : System.Web.UI.Page
    {
        protected AppUsersEntity myUser;
         
        protected void Page_Load(object sender, EventArgs e)
        {
            myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));
            
            if (!Page.IsPostBack)
            {
                //// if not an admin redirect to Unauthorized Page
                if ((myUser == null) || (myUser.UserStatus.ToLower() != "active"))
                {
                    Response.Redirect("~/ReportsList.aspx", true);
                }
                else
                {
                    if ((myUser == null) || (!myUser.IsAdministrator && !myUser.IsUserAdmin))
                    {
                        Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
                    }
                }

                // remove the session object in case up dates are made
                Session["AppUsers"] = null;

                EnterButton.TieButton(txtUserName, btnSubmit);
                EnterButton.TieButton(txtStartDate, this.btnRunReport);
                EnterButton.TieButton(txtEndDate, btnRunReport);
                
                string strUserID = "0";
                this.LoadUserRoles();
                //this.LoadUGroupsDropDown();

                if (Request.QueryString["userID"] != null)
                {
                    strUserID = Request.QueryString["userID"];
                    this.btnSubmit.Text = "Update User";
                }
                else
                {
                    this.btnSubmit.Text = "Create User";
                }

                AppUsersEntity user = AppUsersEntity.GetAppUserByUserID(strUserID);

                if((user.RoleID == 1) && (!myUser.IsAdministrator))
                {
                    Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
                }
                else
                {
                    this.LoadUserData(user);
                    this.BindTerritory();
                }


                /******************** Report Usage **********************************/
                Session["UsageStats"] = null;
                this.calStartDate.Visible = false;
                this.calEndDate.Visible = false;

                txtEndDate.Text = DateTime.Now.ToShortDateString();
                txtStartDate.Text = DateTime.Now.AddMonths(-1).ToShortDateString();

                if (Request.QueryString["userID"] != null)
                {
                    // Load the user stat gridview
                    this.btnRunReport_Click(sender, e);
                }


            }//// end Is NOT PostBack

            this.SetControls();
        }

        /// <summary>Loads the user data to the page controls</summary>
        /// <param name="user">The AppUsersEntity.</param>
        protected void LoadUserData(AppUsersEntity user)
        {
            this.lblCreatedBy.Text = user.CreatedBy;
            this.lblCreatedDate.Text = user.CreateDate;
            this.lblDisplayName.Text = user.DisplayName;
            this.lblModifiedBy.Text = user.ModifiedBy;
            this.lblModifiedDate.Text = user.ModifiedDate;
            this.lblUserID.Text = user.UserID.ToString();
            this.txtUserName.Text = user.UserName;

            this.ddlRole.ClearSelection();
            if (this.ddlRole.Items.FindByValue(user.RoleID.ToString()) != null)
            {
                ddlRole.Items.FindByValue(user.RoleID.ToString()).Selected = true;
            }

            this.ddlStatus.ClearSelection();
            if (this.ddlStatus.Items.FindByValue(user.UserStatus) != null)
            {
                ddlStatus.Items.FindByValue(user.UserStatus).Selected = true;
            }

            Debug.WriteLine("LoadUserData user.UserID: " + user.UserID.ToString());
            this.LoadUserGroupMembership(user.UserID);
        }

        /// <summary>Sets the display of the Web Controls</summary>
        protected void SetControls()
        {
            bool showControl = Convert.ToBoolean(this.lblUserID.Text != "0");

            this.LabelCreated.Visible = showControl;
            this.LabelDisplayName.Visible = showControl;
            this.LabelModified.Visible = showControl;
            this.LabelUserID.Visible = showControl;
            this.lblUserID.Visible = showControl;
            this.LabelGroups.Visible = showControl;
            this.LabelGroupMembership.Visible = showControl;
            this.LabelMembership.Visible = showControl;
            this.ddlGroups.Visible = showControl;
            this.lstGroupMembership.Visible = showControl;
            this.btnAddGroup.Visible = showControl;
            this.btnRemoveGroup.Visible = showControl;
            this.vldRequiredGroupMember.Visible = showControl;
            this.vldRequiredGroup.Visible = showControl;
            this.ltlLine1.Visible = showControl;
            this.ltlLine2.Visible = showControl;

            this.lstReports.Visible = showControl;
            this.LabelReports.Visible = showControl;
            this.LabelTerritories.Visible = showControl;
            this.gvTerritory.Visible = showControl;
            this.btnImpersonate.Visible = showControl;

            this.LabelStartDate.Visible = showControl;
            this.LabelEndDate.Visible = showControl;
            this.LabelRecords.Visible = showControl;
            this.txtStartDate.Visible = showControl;
            this.imgStartCalendar.Visible = showControl;
            this.vldStartDate.Visible = showControl;
            this.vldRequiredStartDate.Visible = showControl;
            this.txtEndDate.Visible = showControl;
            this.imgEndCalendar.Visible = showControl;
            this.vldEndDate.Visible = showControl;
            this.vldRequiredEndDate.Visible = showControl;
            this.ddlRecords.Visible = showControl;
            this.btnRunReport.Visible = showControl;
            this.vldSummary.Visible = showControl;
            this.LabelActivity.Visible = showControl;


        }

        /// <summary>Loads the lstReports ListBox with the Users Reports controls</summary>
        /// <param name="intUserID">the userId</param>
        protected void LoadUserReports()
        {
            this.lstReports.Items.Clear();

            if (this.lblUserID.Text != "0")
            {
                List<ReportEntity> rpts = ReportEntity.GetReportsListByUserID(Convert.ToInt32(this.lblUserID.Text));

                foreach (ReportEntity r in rpts)
                {
                    this.lstReports.Items.Add(new ListItem(r.Header + " - " + r.PublishedName, r.ReportID.ToString()));
                }
            }
        }

        /// <summary>Loads the lstGroupMembership ListBox with the Users Groups controls</summary>
        /// <param name="intUserID">the userId</param>
        protected void LoadUserGroupMembership(int intUserID)
        {
            Debug.WriteLine("LoadUserGroupMembership user.UserID: " + intUserID.ToString());
            List<GroupUsersEntity> groups = GroupUsersEntity.GetGroupUsersByUserID(intUserID);

            this.lstGroupMembership.Items.Clear();
            foreach (GroupUsersEntity g in groups)
            {
                this.lstGroupMembership.Items.Add(new ListItem(g.GroupName, g.GroupUserID.ToString()));
            }

            // Populate the Group Drop Down without the MemberGroups
            List<GroupEntity> allGroups = GroupEntity.GetGroupAllGroups();

            this.ddlGroups.Items.Clear();
            this.ddlGroups.Items.Add(new ListItem("-- Select --", ""));
            foreach (GroupEntity g in allGroups)
            {
                List<GroupUsersEntity> selectedGroups = groups.FindAll(x => x.GroupID == g.GroupID);
                if(selectedGroups.Count == 0)
                    this.ddlGroups.Items.Add(new ListItem(g.GroupName, g.GroupID.ToString()));
            }

            this.LoadUserReports();
        }
        
        /// <summary>Loads the user roles drop down controls</summary>
        protected void LoadUserRoles()
        {
            List<UserRolesEntity> roles = UserRolesEntity.GetUserRoles();

            this.ddlRole.Items.Clear();
            this.ddlRole.Items.Add(new ListItem("-- Select --", ""));
            foreach(UserRolesEntity r in roles)
            {
                if (r.RoleID == 1)
                {
                    if (myUser.IsAdministrator)
                    {
                        this.ddlRole.Items.Add(new ListItem(r.RoleName, r.RoleID.ToString()));
                    }
                }
                else
                    this.ddlRole.Items.Add(new ListItem(r.RoleName, r.RoleID.ToString()));

            }
        }

        /// <summary>Handles the click event of the btnSubmit control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate("ValidSubmit");

            ADUserEntity userAD = null;

            if(this.txtUserName.Text != string.Empty)
            {
                userAD = ADUtility.GetADUserByUserName(this.txtUserName.Text);
                this.vldUsernameInValid.IsValid = Convert.ToBoolean(userAD != null);

                AppUsersEntity user = AppUsersEntity.GetAppUserByUsername(this.txtUserName.Text);

                if(this.lblUserID.Text == "0")
                    this.vldUserExists.IsValid = Convert.ToBoolean(user == null);
                else
                    this.vldUserExists.IsValid = Convert.ToBoolean(user.UserID.ToString() == this.lblUserID.Text);
            }

            if (Page.IsValid)
            {
                ADUserEntity creator = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

                AppUsersEntity u = new AppUsersEntity();
                AppUsersEntity updatedUser = new AppUsersEntity();

                u.ModifiedBy = creator.DisplayName;
                u.UserName = this.txtUserName.Text;
                u.DisplayName = userAD.DisplayName;
                u.UserStatus = this.ddlStatus.SelectedValue;
                u.RoleID = Convert.ToInt32(this.ddlRole.SelectedValue);
                u.UserID = Convert.ToInt32(this.lblUserID.Text);

                if (this.lblUserID.Text == "0")
                {
                    u.CreatedBy = creator.DisplayName;
                    u = AppUsersEntity.InsertAppUser(u);

                    //List<UserTerritoryEntity> tList = this.LoadTerritoryGrid();
                    //UserTerritoryEntity.UpdateUserTerritories(tList, u.UserID);
                    this.btnSubmit.Text = "Update User";
                }
                else
                {
                    AppUsersEntity.UpdateAppUser(u);
                }

                AppUsersEntity user = AppUsersEntity.GetAppUserByUserID(u.UserID.ToString());
                this.LoadUserData(user);

                this.BindTerritory();
                this.SetControls();
                
            } //// end Page.Isvalid

        }//// end btnSubmit_Click

        /// <summary>Handles the Edit event of the gvTerritory GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_AddTerritory(object sender, GridViewCancelEditEventArgs e)
        {
            RegularExpressionValidator vldSalesRepID = (RegularExpressionValidator)this.gvTerritory.FooterRow.FindControl("vldSalesRepID");
            RegularExpressionValidator vldDistributorID = (RegularExpressionValidator)this.gvTerritory.FooterRow.FindControl("vldDistributorID");

            vldSalesRepID.Visible = Convert.ToBoolean(this.ddlRole.SelectedValue == "3");
            vldDistributorID.Visible = Convert.ToBoolean(this.ddlRole.SelectedValue == "2");

            Page.Validate("AddTerritory");

            TextBox txtTerritory = (TextBox)this.gvTerritory.FooterRow.FindControl("txtTerritory");

            if (Page.IsValid)
            {
                ADUserEntity creator = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

                UserTerritoryEntity.InsertUserTerritory(Convert.ToInt32(this.lblUserID.Text), txtTerritory.Text, creator.DisplayName);

                this.gvTerritory.EditIndex = -1;
                this.BindTerritory();
            } //// Page.IsValid
        }

        /// <summary>Binds the Territory List to the gvTerritory GridView.</summary>
        protected void BindTerritory(List<UserTerritoryEntity> tList)
        {
            Debug.WriteLine("BindTerritory tList.Count: " + tList.Count.ToString());
            
            if (tList.Count == 0)
            {
                UserTerritoryEntity dummy = new UserTerritoryEntity();
                dummy.UserTerritoryID = -99;
                tList.Add(dummy);
                Debug.WriteLine("BindTerritory Added Dummy - " + dummy.UserTerritoryID.ToString());
            }

            this.gvTerritory.DataSource = tList;
            this.gvTerritory.DataBind();
        }//// end BindDT
        
        /// <summary>Binds the Territory List to the gvTerritory GridView.</summary>
        protected void BindTerritory()
        {
            List<UserTerritoryEntity> tList = UserTerritoryEntity.GetUserTerritoriesByUserId(Convert.ToInt32(this.lblUserID.Text));
            Debug.WriteLine("BindTerritory tList Count: " + tList.Count.ToString());
            if (tList.Count == 0)
            {
                UserTerritoryEntity dummy = new UserTerritoryEntity();
                dummy.UserTerritoryID = -99;
                tList.Add(dummy);
            }

            this.gvTerritory.DataSource = tList;
            this.gvTerritory.DataBind();
        }//// end BindDT

        /// <summary>Handles the RowDataBound event of the gvTerritory GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBoundTerritory(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Territory');");

                if (this.gvTerritory.DataKeys[e.Row.RowIndex]["UserTerritoryID"].ToString() != "-99")
                {
                    Debug.WriteLine("Territory Visble: " + this.gvTerritory.DataKeys[e.Row.RowIndex]["UserTerritoryID"].ToString());
                }
                else
                {
                    Debug.WriteLine("Territory Visble: False; Index:" + e.Row.RowIndex.ToString());

                }
                //// if the gvTerritory if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvTerritory.DataKeys[e.Row.RowIndex]["UserTerritoryID"].ToString() != "-99");
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtTerritory = (TextBox)e.Row.FindControl("txtTerritory");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtTerritory, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the gvTerritory GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_DeleteTerritory(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intUserTerritoryID = Int32.Parse(this.gvTerritory.DataKeys[e.RowIndex]["UserTerritoryID"].ToString());

            ADUserEntity creator = ADUtility.GetADUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
            UserTerritoryEntity.DeleteTerritoryByUserTerritoryId(intUserTerritoryID, creator.DisplayName);

            this.gvTerritory.EditIndex = -1;
            this.BindTerritory();
        }

        /// <summary>This loads all the data from the gvTerritory GridView into a UserTerritoryEntity List.</summary>
        /// <returns>UserTerritoryEntity List</returns>
        protected List<UserTerritoryEntity> LoadTerritoryGrid()
        {
            List<UserTerritoryEntity> tList = new List<UserTerritoryEntity>();

            foreach (GridViewRow r in this.gvTerritory.Rows)
            {
                if ((r.RowType == DataControlRowType.DataRow) && (r.Visible))
                {
                    UserTerritoryEntity t = new UserTerritoryEntity();
                    t.UserTerritoryID = r.RowIndex;

                    Label lblTerritory = (Label)r.FindControl("lblTerritory");
                    t.Territory = lblTerritory.Text;

                    t.UserID = Convert.ToInt32(this.lblUserID.Text);

                    Debug.WriteLine("LoadTerritoryGrid Territory: " + t.Territory + "; UserTerritoryID: " + t.UserTerritoryID.ToString());
                    tList.Add(t);
                }

            }

            return tList;
        }

        /// <summary>Handles the Add Group button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void btnAddGroup_Click(object sender, EventArgs e)
        {
            Page.Validate("AddGroup");

            if (Page.IsValid)
            {
                GroupUsersEntity.AddUserToGroup(Convert.ToInt32(this.lblUserID.Text), Convert.ToInt32(this.ddlGroups.SelectedValue));
                this.LoadUserGroupMembership(Convert.ToInt32(this.lblUserID.Text));
            }
        }

        /// <summary>Handles the Remove Group button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void btnRemoveGroup_Click(object sender, EventArgs e)
        {
            Page.Validate("RemoveGroup");

            if (Page.IsValid)
            {
                GroupUsersEntity.RemoveGroupUser(Convert.ToInt32(this.lstGroupMembership.SelectedValue));
                this.LoadUserGroupMembership(Convert.ToInt32(this.lblUserID.Text));
            }
        }

        /// <summary>Handles the Impersonate button click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void btnImpersonate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ReportsList.aspx?impersonate=" + this.txtUserName.Text.Trim(), true);
        }


        /***************************** Report Usage *********************************/
        /// <summary>Handles the Start Calendar Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The ImageClickEventArgs instance containing the event data.</param>
        protected void imgStartCalendar_Click(object sender, ImageClickEventArgs e)
        {
            Debug.WriteLine("Sender: " + sender.ToString());
            this.calStartDate.Visible = !calStartDate.Visible;
            if (this.calStartDate.Visible)
                this.calEndDate.Visible = false;
        }

        /// <summary>Handles the Start Calendar SelectionChanged event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void calStartDate_SelectionChanged(object sender, EventArgs e)
        {
            txtStartDate.Text = calStartDate.SelectedDate.ToShortDateString();
            calStartDate.Visible = false;
        }

        /// <summary>Handles the End Calendar Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The ImageClickEventArgs instance containing the event data.</param>
        protected void imgEndCalendar_Click(object sender, ImageClickEventArgs e)
        {
            Debug.WriteLine("Sender: " + sender.ToString());
            this.calEndDate.Visible = !calEndDate.Visible;
            if (this.calEndDate.Visible)
                this.calStartDate.Visible = false;
        }

        /// <summary>Handles the End Calendar SelectionChanged event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The EventArgs instance containing the event data.</param>
        protected void calEndDate_SelectionChanged(object sender, EventArgs e)
        {
            txtEndDate.Text = calEndDate.SelectedDate.ToShortDateString();
            calEndDate.Visible = false;
        }

        /// <summary>Handles the Sorting event of the grid view control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void GV_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {

            if (e.SortExpression.ToLower() == this.hdnSortField.Value.ToLower())
            {
                if (this.hdnSortDirection.Value == "ascending")
                    this.hdnSortDirection.Value = "descending";
                else
                    this.hdnSortDirection.Value = "ascending";
            }
            else
            {
                this.hdnSortField.Value = e.SortExpression.ToLower();
                this.hdnSortDirection.Value = "ascending";
            }

            List<UserStatsEntity> stats = null;
            if (Session["UsageStats"] != null)
            {
                stats = Session["UsageStats"] as List<UserStatsEntity>;
            }
            else
            {
                stats = UserStatsEntity.GetUserStatistics(txtStartDate.Text, txtEndDate.Text, this.ddlRecords.SelectedValue, this.txtUserName.Text);
            }

            stats = this.SortGridView(stats);
            Session["UsageStats"] = stats;
            this.BindDT();
        }


        protected List<UserStatsEntity> SortGridView(List<UserStatsEntity> statsList)
        {
            Func<UserStatsEntity, object> prop = null;  //processings, max(CreateDate) as lastRun, DisplayName, header, publishedName
            switch (this.hdnSortField.Value)
            {
                case "displayname":
                    {
                        prop = p => p.DisplayName;
                        break;
                    }
                case "createdate":
                    {
                        prop = p => p.CreateDate;
                        break;
                    }
                case "reportname":
                    {
                        prop = p => p.ReportName;
                        break;
                    }

                default:
                    {
                        prop = p => p.CreateDate;
                        break;
                    }
            }


            Func<IEnumerable<UserStatsEntity>, Func<UserStatsEntity, object>, IEnumerable<UserStatsEntity>> func = null;
            Debug.WriteLine("SortDirection: " + this.hdnSortDirection.Value);

            switch (this.hdnSortDirection.Value)
            {
                case "ascending":
                    {
                        func = (c, p) => c.OrderBy(p);
                        break;
                    }
                case "descending":
                    {
                        func = (c, p) => c.OrderByDescending(p);
                        break;
                    }
            }

            IEnumerable<UserStatsEntity> stats = statsList as IEnumerable<UserStatsEntity>;

            stats = func(stats, prop);

            List<UserStatsEntity> sortedStats = stats.ToList<UserStatsEntity>();

            return sortedStats;
        }

        /// <summary>Binds the stats list to the gvStats GridView.</summary>
        protected void BindDT()
        {
            List<UserStatsEntity> stats = null;

            if (Session["UsageStats"] == null)
            {
                stats = UserStatsEntity.GetUserStatistics(txtStartDate.Text, txtEndDate.Text, this.ddlRecords.SelectedValue, this.txtUserName.Text);
                Session["UsageStats"] = stats;
            }
            else
            {
                stats = Session["UsageStats"] as List<UserStatsEntity>;
            }

            this.gvStats.DataSource = stats;
            this.gvStats.DataBind();
        }

        /// <summary>Handles the btnRunReport Click event.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnRunReport_Click(object sender, EventArgs e)
        {
            Page.Validate("userStat");

            if (Page.IsValid)
            {
                Session["UsageStats"] = null;

                this.gvStats.EditIndex = -1;
                this.BindDT();
            }
        }

        /// <summary>Handles the Edit event of the gvStats GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GVStats_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvStats.EditIndex = e.NewEditIndex;

            string strReportTrackerId = this.gvStats.DataKeys[e.NewEditIndex]["ReportTrackerId"].ToString();
            List<ReportTrackerParameterEntity> rptParams = ReportTrackerParameterEntity.GetReportTrackerParametersByReportTrackerId(Convert.ToInt32(strReportTrackerId));

            this.BindDT();

            GridView gv = (GridView)this.gvStats.Rows[e.NewEditIndex].FindControl("gvParameters");
            gv.DataSource = rptParams;
            gv.DataBind();

            //Response.Redirect("UserDetail.aspx?userID=" + strUserID, true);
        }

        /// <summary>Handles the RowDataBound event of the gvStats GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GVStats_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        } // end gv_RowDataBound


    } //// end class
}