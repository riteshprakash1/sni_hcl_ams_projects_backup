﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="CrystalReportsNet.ErrorPage" MasterPageFile="~/Site.Master" Title="Error Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table style="width:600; text-align:center; border:1">
            <tr style="height:200px; vertical-align:middle">
                <td style="text-align:center">
                    <asp:Label ID="lblErrorHeading" ForeColor="#FF7300" Font-Size="14pt" runat="server" Text="Application Error"></asp:Label>
                     <br /><br />
                    <asp:Label ID="lblMsg" Font-Size="11pt" runat="server" Text="An application error occurred.  
                    An email has been sent to the application developer.  
                    You may be contacted by the application developer to troubleshoot the issue.  
                    Click on one of the links above to be redirected to the application." ></asp:Label>
                    <br /><br />
                    <asp:Label ID="lblErrorMsg" Font-Size="11pt" runat="server" Text="Application Error"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
