﻿// --------------------------------------------------------------
// <copyright file="Site.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;

    /// <summary>This is the Master page for the Web Application.</summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        
        /// <summary>Handles the OnInit event of the Page control.  This fixes Menu rendering in Chrome and Safari.</summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {    // For Chrome and Safari    
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                if (Request.Browser.Adapters.Count > 0)
                {
                    Request.Browser.Adapters.Clear();
                    Response.Redirect(Page.Request.Url.AbsoluteUri);
                }
            }
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/ShowHint.js"));
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/jquery-1.4.1.min.js"));
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/jquery-1.4.1.js"));
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/jquery-1.4.1-vsdoc.js"));

     //       this.lblMessage.Text = string.Empty;

            if (!Page.IsPostBack)
            {
                bool blnShowEnvironment = !Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"].ToString());

                if(blnShowEnvironment)
                    this.lblEnvironment.Text = ConfigurationManager.AppSettings["Environment"].ToString();

                this.ltlEnvBreak.Visible = blnShowEnvironment;
                this.lblEnvironment.Visible = blnShowEnvironment;

                Debug.WriteLine("Site Master !Page.IsPostBack");
                this.lblAppName.Text = ConfigurationManager.AppSettings["AppName"];
                
                if (Request.QueryString["ReportTrackerID"] == null)
                {
                    this.NavMenu.Items.Add(new MenuItem("Reports List", "ReportsList.aspx"));
                }
                else
                {
                    this.NavMenu.Items.Add(new MenuItem("Reports List", "ReportsList.aspx?ReportTrackerID=" + Request.QueryString["ReportTrackerID"]));
                }
                //this.NavMenu.Items.Add(new MenuItem("Administration", "", "", "~/Admin/UserList.aspx"));

                //AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));

                //if (AppUsersEntity.IsAdmin(myUser))
                //{
                //    this.NavMenu.Items.Add(new MenuItem("Administration", "", "", "~/Admin/Default.aspx"));
                //}


                try
                {
                    AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));
                    if (myUser != null)
                    {
                        this.lblUser.Text = myUser.DisplayName;
                        Debug.WriteLine("User: " + myUser.UserName + "; Role: " + myUser.RoleName);
                        if (myUser.UserStatus.ToLower() == "active")
                        {
                            if (myUser.IsAdministrator || myUser.IsUserAdmin)
                            {
                                MenuItem adminMenu = new MenuItem("Administration", "", "", "");
                                adminMenu.ChildItems.Add(new MenuItem("User List", "", "", "~/Admin/UserList.aspx"));
                                adminMenu.ChildItems.Add(new MenuItem("Reporting Stats", "", "", "~/Admin/Stats.aspx"));
                                if (myUser.IsAdministrator)
                                {
                                    adminMenu.ChildItems.Add(new MenuItem("Groups", "", "", "~/Admin/GroupDetail.aspx"));
                                    adminMenu.ChildItems.Add(new MenuItem("Reports", "", "", "~/Admin/ReportDetail.aspx"));
                                }
                                adminMenu.ChildItems.Add(new MenuItem("Administrator Guide", "", "", "~/CrystalReportsAdministratorGuide.pdf", "_blank"));
                                this.NavMenu.Items.Add(adminMenu);
                            }
                        }
                        else
                        {
                            this.lblMasterMessage.Text = "<br><br>";
                            if (ConfigurationManager.AppSettings["DisabledMessage"] != null)
                            {
                                 this.lblMasterMessage.Text += ConfigurationManager.AppSettings["DisabledMessage"].ToString() + "<br>";
                            }
                            this.lblMasterMessage.Text += "Your Crystal Reports profile is disabled.  Please contact the system administrator. ";

                            this.AuthorizationIssueEmail("Crystal Reports Access Denied - Disabled Account");
                        }
                    }
                    else
                    {
                        this.lblMasterMessage.Text = "<br><br>";
                        if (ConfigurationManager.AppSettings["NoProfileMessage"] != null)
                        {
                            this.lblMasterMessage.Text += ConfigurationManager.AppSettings["NoProfileMessage"].ToString() + "<br>";
                        }
                        this.lblMasterMessage.Text += "You do not have a Crystal Reports profile.  Please contact the system administrator. ";
                        
                        this.AuthorizationIssueEmail("Crystal Reports Access Denied - No Crystal Profile");
                    }
                }
                catch (Exception ex)
                {
                    LogException.HandleException(ex, Request);
                }

            }
        }

        private void AuthorizationIssueEmail(string strSubject)
        {
            ADUserEntity user = ADUtility.GetADUserByUserName(Utility.CurrentUser(Request));
            
            string strTo = ConfigurationManager.AppSettings["logEmailAddresses"].ToString();
            string strFrom = ConfigurationManager.AppSettings["logFromEmail"].ToString();
            string strBody = "<table>";
            strBody += "<tr><td>UserName:</td><td>"+user.UserName+"</td></tr>";
            strBody += "<tr><td>Display Name:</td><td>" + user.DisplayName + "</td></tr>";
            strBody += "<tr><td>Email:</td><td>" + user.Email + "</td></tr>";
            strBody += "<tr><td>Environment:</td><td>" + ConfigurationManager.AppSettings["Environment"].ToString() + "</td></tr>";
            strBody += "<tr><td>URL:</td><td>" + Request.Url.ToString() + "</td></tr>";
            strBody += "</table>";

            Debug.WriteLine("strBody: " + strBody);
            Utility.SendEmail(strTo, strFrom, String.Empty, strSubject, strBody, true);
        }

        /// <summary>Called when MenuItem is clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.MenuEventArgs"/> instance containing the event data.</param>
        public void OnClick(Object sender, MenuEventArgs e)
        {
            Debug.WriteLine("In OnClick");
            //MessageLabel.Text = "You selected " + e.Item.Text + ".";
            Debug.WriteLine("Value: " + e.Item.Value + "; Text: " + e.Item.Text);
            e.Item.Selected = true;
            string strPage = "~/" + e.Item.Value.Replace("Admin", "Admin/");
            Response.Redirect(strPage, true);
        }

    } //// end class
}