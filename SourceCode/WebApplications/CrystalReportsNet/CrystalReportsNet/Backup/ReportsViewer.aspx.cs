﻿// --------------------------------------------------------------
// <copyright file="ReportsViewer.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2014 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrystalReportsNet
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrystalDecisions.CrystalReports.Engine;
    using CrystalDecisions.Shared;
    using CrystalReportsNet.Classes;
    using CrystalReportsNet.Classes.Entity;

    /// <summary>This is the ReportsViewer class for ReportsViewer page</summary>
    public partial class ReportsViewer : System.Web.UI.Page
    {
        protected ReportDocument report;

        protected string strReportSessionName;

        /// <summary>Handles the Init event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Request.QueryString["ReportTrackerID"] != null)
            {
                strReportSessionName = "myReport" + Request.QueryString["ReportTrackerID"];
            }
            else
            {
                Response.Redirect("ReportsList.aspx", true);
            }

            if (!Page.IsPostBack)
            {
                AppUsersEntity myUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));

                // Session["myReport"] = null;

                if (Request.QueryString["ReportTrackerID"] == null || myUser == null)
                {
                    Response.Redirect("ReportsList.aspx", true);
                }
                else
                {
                    if (myUser.UserStatus.ToLower() == "active")
                    {
                        this.RunReport();
                    }
                    else
                    {
                        Response.Redirect("ReportsList.aspx", true);
                    }

                } //// end else

            } //// end if Not Postback
            else
            {
                //string strSessionReportName = "myReport";

                //if (Request.QueryString["ReportTrackerID"] != null)
                //{
                //    strSessionReportName = "myReport" + Request.QueryString["ReportTrackerID"];
                //}
                //else
                //{
                //    Response.Redirect("ReportsList.aspx", true);
                //}

                if (Session[strReportSessionName] != null)
                {
                    ReportDocument report = (ReportDocument)Session[strReportSessionName];
                    this.crViewer.ReportSource = report;
                }
                else
                {
                    Debug.WriteLine("Page INIT Callback Session Expired; Hopefully going to on navigate - Callback: " + Page.IsCallback.ToString() + "; Postback: " + Page.IsPostBack.ToString());
                    //        try
                    //        {
                    //            string reportPath = Server.MapPath("") + "\\reports\\" + "TimeOut.rpt";
                    //            ReportDocument report = new ReportDocument();
                    //            Debug.WriteLine("Instantient Report");
                    //            report.Load(reportPath);
                    //            Debug.WriteLine("Load Report");

                    //            this.crViewer.ReportSource = report;
                    //            Debug.WriteLine("Report as Source");
                    //            //FINALLY REFRESH REPORT
                    //    //        this.crViewer.RefreshReport();

                    ////            Response.Redirect("ReportsList.aspx?msg=timeout", true);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            //Page.IsCallback
                    //            Debug.WriteLine("Call Back Error: " + ex.Message);
                    //        }
                    //  this.RunReport();
                }

            } /// if Not Postback ELSE


        }

        protected void CheckSession()
        {
            if (Session[strReportSessionName] == null)
            {
                Response.Redirect("ReportsList.aspx?msg=timeout", false);
            }

        }

        protected void CRV_Search(object source, CrystalDecisions.Web.SearchEventArgs e)
        {
            Debug.WriteLine("I'm In CRV_Search: " + strReportSessionName + " : " + Convert.ToBoolean(Session[strReportSessionName] == null).ToString());
            this.CheckSession();
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("In Page_Load: " + strReportSessionName + " : " + Convert.ToBoolean(Session[strReportSessionName] == null).ToString() + "; Callback: " + Page.IsCallback.ToString() + "; Postback: " + Page.IsPostBack.ToString());
            if (Page.IsPostBack)
            {
                this.CheckSession();
                Debug.WriteLine("In Page_Load: After CheckSession");
            } //// end IsPostBack
        }

        protected void CRV_Navigate(object source, CrystalDecisions.Web.NavigateEventArgs e)
        {
            Debug.WriteLine("I'm In CRV_Navigate: " + strReportSessionName + " : " + Convert.ToBoolean(Session[strReportSessionName] == null).ToString() + "; Callback: " + Page.IsCallback.ToString() + "; Postback: " + Page.IsPostBack.ToString());
            this.CheckSession();
        }

        ///// <summary>Handles the Load event of the Page control.</summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    Debug.WriteLine("Page_Load");             
        //    if (Page.IsPostBack)
        //    {
        //        Debug.WriteLine("Page_Load: IsPostBack");             
                
        //        //Checks whther the Report Session has expired; if so redirects to Reports List.
        //        if (Session[strReportSessionName] == null)
        //        {
        //            Response.Redirect("ReportsList.aspx?msg=timeout", false);
                    
        //            Debug.WriteLine("Page_Load: After Redirect");
        //        }
        //    } //// end IsPostBack
        //}

        /// <summary>
        /// Creates an instance of the ConnectionInfo object sets the attributes 
        /// from the values in the ConnectionString section of the Web.Config 
        /// </summary>
        /// <param name="strConnectionName">The ConnectionString Key.</param>
        /// <returns>ConnectionInfo</returns>
        private ConnectionInfo GetConnectionInfo(string strConnectionName)
        {
            Debug.WriteLine("strConnection: " + strConnectionName);
            string strConnection = ConfigurationManager.ConnectionStrings[strConnectionName].ToString();
            string[] strProperties = strConnection.Split(';');
            string strDataSource = string.Empty;
            string strUserID = string.Empty;
            string strPassword = string.Empty;
            string strInitialCatalog = string.Empty;

            string strTimeout = "15";
            if (ConfigurationManager.AppSettings["RptConnTimeout"] != null)
                strTimeout = ConfigurationManager.AppSettings["RptConnTimeout"].ToString();

            Dictionary<string, string> connvalues = new Dictionary<string, string>();

            foreach (string str in strProperties)
            {
                string[] strKeyValue = str.Split('=');

                if (strKeyValue.Length == 2)
                {
                    // Key, Value
                    connvalues.Add(strKeyValue[0].Trim().ToLower(), strKeyValue[1].Trim());
                    //Debug.WriteLine("Key: " + strKeyValue[0].Trim() + "  Value: " + strKeyValue[1].Trim());
                }
            }

            string strTest = string.Empty;
            if (connvalues.TryGetValue("data source", out strTest))
                strDataSource = strTest;

            strTest = string.Empty;
            if (connvalues.TryGetValue("user id", out strTest))
                strUserID = strTest;

            strTest = string.Empty;
            if (connvalues.TryGetValue("password", out strTest))
                strPassword = strTest;

            strTest = string.Empty;
            if (connvalues.TryGetValue("initial catalog", out strTest))
                strInitialCatalog = strTest;

            //Debug.WriteLine("ServerName: " + strDataSource + ";  DatabaseName: " + strInitialCatalog + ";  UserID: " + strUserID + ";  Password: " + strPassword);

            DbConnectionAttributes dbAttributes;
            ConnectionInfo crConnectionInfo = null;

            //setup the attributes for the connection
            dbAttributes = new DbConnectionAttributes();
            dbAttributes.Collection.Set("Auto Translate", "-1");
            dbAttributes.Collection.Set("Connect Timeout", strTimeout);
            dbAttributes.Collection.Set("Data Source", strDataSource);
            dbAttributes.Collection.Set("General Timeout", "0");
            dbAttributes.Collection.Set("Initial Catalog", strInitialCatalog);
            dbAttributes.Collection.Set("Integrated Security", false);
            dbAttributes.Collection.Set("Locale Identifier", "5129");
            dbAttributes.Collection.Set("OLE DB Services", "-5");
            dbAttributes.Collection.Set("Provider", "SQLOLEDB");
            dbAttributes.Collection.Set("Tag with column collation when possible", "0");
            dbAttributes.Collection.Set("Use DSN Default Properties", false);
            dbAttributes.Collection.Set("Use Encryption for Data", "0");
            //setup the connection
            crConnectionInfo = new ConnectionInfo();
            crConnectionInfo.LogonProperties.Clear();
            crConnectionInfo.Attributes.Collection.Clear();
            crConnectionInfo.DatabaseName = strInitialCatalog;
            crConnectionInfo.ServerName = strDataSource;
            crConnectionInfo.UserID = strUserID;
            crConnectionInfo.Password = strPassword;
            crConnectionInfo.Attributes.Collection.Set("Database DLL", "crdb_ado.dll");
            crConnectionInfo.Attributes.Collection.Set("QE_DatabaseName", strInitialCatalog);
            crConnectionInfo.Attributes.Collection.Set("QE_DatabaseType", "OLE DB (ADO)");
            crConnectionInfo.Attributes.Collection.Set("QE_LogonProperties", dbAttributes);
            crConnectionInfo.Attributes.Collection.Set("QE_ServerDescription", strDataSource);
            crConnectionInfo.Attributes.Collection.Set("QE_SQLDB", true);
            crConnectionInfo.Attributes.Collection.Set("SSO Enabled", false);
            crConnectionInfo.LogonProperties = dbAttributes.Collection;

            //Debug.WriteLine("ServerName: " + crConnectionInfo.ServerName + ";  DatabaseName: " + crConnectionInfo.DatabaseName + ";  UserID: " + crConnectionInfo.UserID + ";  Password: " + crConnectionInfo.Password);

            return crConnectionInfo;

        }

        protected void ReportSessionTimeout()
        {
            this.crViewer.Dispose();
            Response.Write("<br> Session Timed Out!");
        }

        /// <summary>Generates the report, using the parameters in the ParametersEntityCollection</summary>
        protected void RunReport()
        {
            this.hdnReportTrackerID.Value = Request.QueryString["ReportTrackerID"];
            int intReportTrackerID = 0;
            try
            {
                intReportTrackerID = Convert.ToInt32(this.hdnReportTrackerID.Value);
            }
            catch
            {
                Response.Redirect("ReportList.aspx", true);
            }
            
            ReportTrackerEntity rt = ReportTrackerEntity.GetReportTrackerByReportTrackerId(intReportTrackerID);
            this.hdnReportID.Value = rt.ReportID.ToString();
            
            ReportEntity rpt = ReportEntity.GetReportByReportID(Convert.ToInt32(this.hdnReportID.Value));
            report = new ReportDocument();

            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();

            ConnectionInfo crConnectionInfo = this.GetConnectionInfo(rpt.ConnectionKey);
            Tables CrTables;
            //string strReporName = Request.QueryString["ReportName"];
            //string reportPath = Server.MapPath("") + "\\reports\\" + strReporName;

            string reportPath = Server.MapPath("") + "\\reports\\" + rpt.ReportFileName;

            report.Load(reportPath);

     //       this.TestStuff(report);

            //LOOP THROUGH EACH TABLE & PROVIDE LOGIN CREDENTIALS
            CrTables = report.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            this.crViewer.ReportSource = report;
            //FINALLY REFRESH REPORT
            this.crViewer.RefreshReport();

            if (Session["ParametersEntityCollection" + this.hdnReportTrackerID.Value] != null)
            {
                Collection<ParameterEntity> pColl = Session["ParametersEntityCollection" + this.hdnReportTrackerID.Value] as Collection<ParameterEntity>;
                this.LoadParameterValues(pColl, intReportTrackerID);
            }
            else
            {
                Response.Redirect("ReportsList.aspx?msg=timeout", true);
            }

            this.SetHiddenReportParameter();

          //  this.TrackReport();

            //string strSessionName = "myReport" + this.hdnReportTrackerID.Value;
            //Debug.WriteLine("RunReport Session Name: " + strSessionName);
            //Session[strSessionName] = report;

            Session[strReportSessionName] = report;
        }//// end RunReport

        /// <summary>Inserts the ReportTrackerEntity then loops thru the ParametersEntityCollection and sets the parameters</summary>
        /// <param name="pColl">The ParameterEntity collection.</param>
        /// <param name="intReportTrackerID">The Report Tracker ID.</param>
        protected void LoadParameterValues(Collection<ParameterEntity> pColl, int intReportTrackerID)
        {
      //      ReportTrackerEntity rt = TrackReport();
            
            foreach (ParameterEntity pe in pColl)
            {
                this.SetReportTrackerParameter(pe, intReportTrackerID);
            }//// end foreach ParameterEntity Collection
        }

        /// <summary>Sets the Report Tracker Parameter using the data in the ParameterEntity</summary>
        /// <param name="pe">The ParameterEntity </param>
        /// <param name="intReportTrackerID">The Report Tracker ID </param>
        protected void SetReportTrackerParameter(ParameterEntity pe, int intReportTrackerID)
        {
            foreach (CrystalDecisions.Shared.ParameterField pField in this.crViewer.ParameterFieldInfo)
            {
                if (pField.ParameterFieldName == pe.ParameterName)
                {
                    // clear the collection of current values in the Parameter
                    pField.CurrentValues.Clear();

                    foreach (InputEntity input in pe.InputValueList)
                    {
                        ReportTrackerParameterEntity p = new ReportTrackerParameterEntity();
                        p.ReportTrackerID = intReportTrackerID;
                        p.ParameterName = pField.ParameterFieldName;
                        p.ParameterType = pField.ParameterValueType.ToString();

                        //Debug.WriteLine("ParameterFieldName: " + input.ParameterName + "; DiscreteOrRange: " + input.DiscreteOrRange.ToLower());
                        
                        if (input.DiscreteOrRange.ToLower() == "range")
                        {
                            CrystalDecisions.Shared.ParameterRangeValue rangeValue = new CrystalDecisions.Shared.ParameterRangeValue();
                            rangeValue.LowerBoundType = CrystalDecisions.Shared.RangeBoundType.BoundInclusive;
                            rangeValue.UpperBoundType = CrystalDecisions.Shared.RangeBoundType.BoundInclusive;

                            if (pe.ParameterType.ToLower() == "date")
                            {
                                rangeValue.StartValue = DateTime.Parse(input.StartValue);
                                rangeValue.EndValue = DateTime.Parse(input.EndValue);
                            }
                            else
                            {
                                rangeValue.StartValue = input.StartValue;
                                rangeValue.EndValue = input.EndValue;
                            }
                            pField.CurrentValues.Add(rangeValue);

                            p.ParameterValue = input.StartValue + " - " + input.EndValue;
                            ReportTrackerParameterEntity.InsertReportTrackerParameter(p);

                            Debug.WriteLine("Range Start: " + rangeValue.StartValue.ToString() + "; End: " + rangeValue.EndValue);
                        }
                        else /// is discrete
                        {
                            // create new Parameter Discrete Value object
                            ParameterDiscreteValue discreteValue = new ParameterDiscreteValue();

                            // Set the value of the Discrete value object 
                            if (pe.ParameterType.ToLower() == "date")
                                discreteValue.Value = DateTime.Parse(input.StartValue);
                            else
                                discreteValue.Value = input.StartValue;

                            // Add the Discrete value object to the collection of current values
                            pField.CurrentValues.Add(discreteValue);

                            p.ParameterValue = input.StartValue;
                            ReportTrackerParameterEntity.InsertReportTrackerParameter(p);
                        }
                    }
                } //// in foreach Input Values
                //Debug.WriteLine("SET Parameter: " + pe.ParameterName);

            } //// if ParameterName

        } //// foreach ParameterFieldInfo


        /// <summary>Gets the AppUsersEntity object for the user, next it checks whether the user is a SalesRep or Distributor.  
        /// If true, the program sets the user territory values into the SalesRep or SalesDistrict parameter of the report. 
        /// </summary>
        protected void SetHiddenReportParameter()
        {
            AppUsersEntity appUser = AppUsersEntity.GetAppUserByUsername(Utility.CurrentUser(Request));

            if ((appUser.IsDistributor || appUser.IsSalesRep))
            {
                string strCrystalParameter = "salesdistrict";
                if (appUser.IsSalesRep)
                    strCrystalParameter = "salesrep";

                foreach (CrystalDecisions.Shared.ParameterField pField in this.crViewer.ParameterFieldInfo)
                {

                    if (pField.ParameterFieldName.ToLower() == strCrystalParameter)
                    {
                        //Debug.WriteLine("SetHiddenReportParameter: " + pField.ParameterFieldName.ToLower());
                        // clear the collection of current values
                        pField.CurrentValues.Clear();
                        
                        List<UserTerritoryEntity> territories = UserTerritoryEntity.GetUserTerritoriesByUserId(appUser.UserID);
                        Debug.WriteLine("SetHiddenReportParameter sp_GetUserTerritoriesByUserId UserTerritory Rows: " + territories.Count.ToString());

                        foreach (UserTerritoryEntity ut in territories)
                        {
                            // create new Parameter Discrete Value object
                            ParameterDiscreteValue discreteValue = new ParameterDiscreteValue();

                            // Set the value of the Discrete value object 
                            discreteValue.Value = ut.Territory;

                            // Add the Discrete value object to the collection of current values
                            pField.CurrentValues.Add(discreteValue);
                        } //// end foreach
                    } //// end if
                } //// end foreach
            } //// end if
        } //// end SetHiddenReportParameter
        
         /// <summary>Sets the value of the parameter(s) with a matching parameter name as the name in the strParamName.</summary>
        /// <param name="strParamName">Report Parameter Name</param>
        /// <param name="strParamValue">Report Parameter Value</param>
        /// <param name="rpt">the Report Document</param>
        private void SetParameterValue(string strParamName, string strParamValue, ReportDocument rpt)
        {
            //Console.WriteLine("Passed paramName: " + strParamName + "; Value: " + strParamValue);

            // get the parameter field definitions 
            ParameterFieldDefinitions fieldDefs = rpt.DataDefinition.ParameterFields;

            // for each parameter, get the value from the control 
            foreach (ParameterFieldDefinition def in fieldDefs)
            {
                // get parameter name 
                string paramName = def.ParameterFieldName;
                //Console.WriteLine("paramName: " + paramName);
                if (def.ParameterFieldName == strParamName)
                {
                    //ParameterFieldDefinition def = fieldDefs[[strParamName];
                    
                    // get the param type
                    ParameterValueKind kind = def.ParameterValueKind;

                    // create new Parameter Discrete Value object
                    ParameterDiscreteValue discreteValue = new ParameterDiscreteValue();

                    // Set the value of the Discrete value object 
                    discreteValue.Value = strParamValue;

                    // extract the collection of current values
                    ParameterValues currentValues = def.CurrentValues;

                    // Add the Discrete value object to the collection of current values
                    currentValues.Add(discreteValue);

                    // apply the modified current values to the param collection
                    def.ApplyCurrentValues(currentValues);

                    //Debug.WriteLine("Set Parameter: " + strParamName);
                }
            }
        }

    }//// end class
}