﻿<%@ Page Title="Groups" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GroupDetail.aspx.cs" Inherits="CrystalReportsNet.Admin.GroupDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table style="width:100%;">
        <tr>
            <td style="width:10%; font-weight:bold">Select Group:</td>
            <td><asp:DropDownList ID="ddlGroup" runat="server"  Width="200px" 
                    onselectedindexchanged="ddlGroup_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
            <td style="width:12%; font-weight:bold">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><asp:Label ID="LabelGroupUsers" Font-Bold="true" Font-Size="12pt"  runat="server" Text="Group Users"></asp:Label></td>
            <td>&nbsp;</td>
            <td><asp:Label ID="LabelGroupReports" Font-Bold="true" Font-Size="12pt"  runat="server" Text="Group Reports"></asp:Label></td>
        </tr>
        <tr>
            <td style="vertical-align:top; font-weight:bold"><asp:Label ID="LabelUsers" runat="server" Text="Users:"></asp:Label>
                <br /><br /><br /><asp:Button ID="btnRemoveUser" runat="server" 
                    Text="Remove User" Font-Size="8pt" Width="110px" 
                    onclick="btnRemoveUser_Click" />
            
            </td>
            <td>
                <div style="border:1px solid black;width:225px;height:400px;margin:0px;padding:0px;overflow:auto;">
                    <asp:CheckBoxList ID="chkLstUser" CellPadding="0" CellSpacing="0" runat="server" Width="200px" Font-Size="8pt" />
                </div>
            </td>
            <td style="vertical-align:top; font-weight:bold">
                <asp:Label ID="LabelReports" runat="server" Text="Reports:"></asp:Label>
                <br /><br /><br /><asp:Button ID="btnRemoveReport" runat="server" 
                    Text="Remove Report" onclick="btnRemoveReport_Click" Font-Size="8pt" Width="110px" />
            </td>
            <td>
                <div style="border:1px solid black;width:325px;height:400px;margin:0px;padding:0px;overflow:auto;">
                    <asp:CheckBoxList ID="chkLstReports" CellPadding="0" CellSpacing="0"  runat="server" Width="300px" Font-Size="8pt" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top">
                <asp:Button ID="btnAddUser" runat="server" Text="Add User" 
                     Width="110px" Font-Size="8pt" onclick="btnAddUser_Click" /></td>
            <td><asp:DropDownList ID="ddlUsers" runat="server" Width="200px" Font-Size="8pt"></asp:DropDownList>
                <br /><asp:RequiredFieldValidator ID="vldRequiredAddUser" ValidationGroup="AddUser" ControlToValidate="ddlUsers" runat="server" ForeColor="#FF7300" ErrorMessage="Select User">Select User</asp:RequiredFieldValidator>
            </td>            
            <td style="vertical-align:top"><asp:Button ID="btnAddReport" runat="server" Text="Add Report" 
                    onclick="btnAddReport_Click" Width="110px" Font-Size="8pt" /></td>
            <td><asp:DropDownList ID="ddlReports" runat="server" Width="200px" Font-Size="8pt"></asp:DropDownList>
                <br /><asp:RequiredFieldValidator ID="vldRequiredAddReport" ValidationGroup="AddReport" ControlToValidate="ddlReports" runat="server" ForeColor="#FF7300" ErrorMessage="Select Report">Select Report</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>

</asp:Content>
