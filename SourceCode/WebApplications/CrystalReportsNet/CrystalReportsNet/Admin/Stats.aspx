﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Stats.aspx.cs" Inherits="CrystalReportsNet.Admin.Stats" Title="Statistics" %>
<%@ Register TagPrefix="uc2" Namespace="CrystalReportsNet.Classes" Assembly="CrystalReportsNet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table style="width:100%;">
            <asp:HiddenField ID="hdnSortDirection" runat="server" Value="descending" />
               <asp:HiddenField ID="hdnSortField" runat="server" Value="displayname" />
            <tr>
                <td style="width:20%"><asp:Label ID="LabelStartDate" runat="server" Text="Start Date:&nbsp;(mm/dd/yyyy):*"></asp:Label></td>
                <td style="width:20%"><asp:Label ID="LabelEndDate" runat="server" Text="End Date:&nbsp;(mm/dd/yyyy):*"></asp:Label></td>
                <td><asp:Label ID="LabelReport" runat="server" Text="Report:"></asp:Label></td>
                <td>&nbsp;</td>
                
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtStartDate" MaxLength="10" runat="server" Width="125px" ></asp:TextBox>
                    <asp:ImageButton ID="imgStartCalendar" ImageUrl="~/Images/calendar.gif" runat="server" CausesValidation="false" onclick="imgStartCalendar_Click" />
                    <asp:RegularExpressionValidator ID="vldStartDate" runat="server" ControlToValidate="txtStartDate" 
                        ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})" ErrorMessage="Start Date Format (mm/dd/yyyy)">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="vldRequiredStartDate" runat="server" ControlToValidate="txtStartDate" ErrorMessage="Start Date is required">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:TextBox ID="txtEndDate" MaxLength="10" Width="125px" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="imgEndCalendar" ImageUrl="~/Images/calendar.gif" runat="server" CausesValidation="false" onclick="imgEndCalendar_Click" />
                    <asp:RegularExpressionValidator ID="vldEndDate" runat="server" ControlToValidate="txtEndDate" 
                        ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})" ErrorMessage="End Date Format (mm/dd/yyyy)">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="vldRequiredEndDate" runat="server" ControlToValidate="txtEndDate" ErrorMessage="End Date is required">*</asp:RequiredFieldValidator>
                </td>
                <td><asp:DropDownList ID="ddlReports" runat="server">
                        <asp:ListItem Value="1">Reports By Users Summary</asp:ListItem>
                        <asp:ListItem Value="2">Reports Summary</asp:ListItem>
                        <asp:ListItem Value="3">Users Summary</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td><asp:Button ID="btnRunReport" runat="server" CausesValidation="false" ToolTip="Click here to run the report."
                     Text="Run Report" Width="120px" onclick="btnRunReport_Click" />       
                </td>
            </tr>
            <tr>
                <td colspan="2"> 
                    <asp:Calendar ID="calStartDate" runat="server" onselectionchanged="calStartDate_SelectionChanged"></asp:Calendar>
                    <asp:Calendar ID="calEndDate" runat="server" onselectionchanged="calEndDate_SelectionChanged"></asp:Calendar>
                </td>
                <td colspan="2"><asp:ValidationSummary ID="vldSummary" runat="server" /></td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:GridView ID="gvStats" runat="server" Width="700px" AutoGenerateColumns="False"
                    EmptyDataText="No data found." ShowFooter="false" AllowSorting="true" OnSorting="GV_Sorting"
                     CellPadding="2" GridLines="Both">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                        <asp:BoundField DataField="displayname" SortExpression="displayname" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Display Name" />                       
                        <asp:BoundField DataField="processings" SortExpression="processings" HeaderStyle-Width="10%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Processings" />                       
                        <asp:BoundField DataField="header" SortExpression="header" HeaderStyle-Width="10%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Header" />                       
                        <asp:BoundField DataField="publishedName" SortExpression="publishedName" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Published Name" />                       
                        <asp:BoundField DataField="LastRun" SortExpression="LastRun" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Last Run" />                       
                   </Columns>
                </asp:GridView> 
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
