using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for WhyRegister.
	/// </summary>
	public class WhyRegister : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form2;
		protected System.Web.UI.WebControls.Button btnLogin;
		protected System.Web.UI.WebControls.Label lblDisclaimer;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!= null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("Login.aspx");

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
			Session["Utility"] = myUtility;
			Response.Redirect("Login.aspx");
		}
	}
}
