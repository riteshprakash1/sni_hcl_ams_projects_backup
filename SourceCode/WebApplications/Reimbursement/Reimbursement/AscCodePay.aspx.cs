using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for AscCodePay.
	/// </summary>
	public class AscCodePay : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Literal ltlCoding;
		protected System.Web.UI.WebControls.DataGrid dgCPT;
		protected System.Web.UI.WebControls.DataGrid dgMedicare;
		protected System.Web.UI.WebControls.Literal ltlPrivate;
		protected System.Web.UI.WebControls.DataGrid dgICD;
		protected DataTable dtCPT;
	
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Literal ltlICD;
		protected System.Web.UI.WebControls.Literal ltlAdditional;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.Literal ltlTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			this.dgCPT.Columns[3].HeaderText = myUtility.RateYear + " National Medicare<br>OPPS Fee Schedule";

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				myUtility.ProductsTable = myUtility.SetTable("sp_GetProduct", myUtility.ProductID);
				dtCPT = myUtility.SetTable("sp_GetAscByProduct", myUtility.ProductID);

				myUtility.SqlConn.Close();

				if(dtCPT.Rows.Count > 0)
					this.BindCPT();
				else
					dgCPT.Visible = false;

				if(myUtility.ProductsTable.Rows.Count>0)
				{
					DataRow aRow = myUtility.ProductsTable.Rows[0];

					ltlICD.Text = aRow["ascMedicare"].ToString();
					ltlPrivate.Text = aRow["ascPrivateIns"].ToString();
					ltlAdditional.Text = aRow["ascAdditional"].ToString();
					ltlCoding.Text = aRow["ascCoding"].ToString();
				
				}


			} // END isPostBack	
		
		} // Page_Load

		public void BindCPT()
		{
			dgCPT.DataSource = dtCPT;
			dgCPT.DataBind();
		} // END BindCPT


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
