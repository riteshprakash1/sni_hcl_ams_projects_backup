using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for HospitalCodePay.
	/// </summary>
	public class HospitalCodePay : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Literal ltlCoding;
		protected System.Web.UI.WebControls.DataGrid dgCPT;
		protected System.Web.UI.WebControls.Literal ltlMedicare;
		protected System.Web.UI.WebControls.DataGrid dgMedicare;
		protected System.Web.UI.WebControls.Literal ltlPrivate;
		protected System.Web.UI.WebControls.DataGrid dgICD;
		protected DataTable dtCPT;
		protected DataTable dtICD;
	
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Literal ltlICD;
		protected System.Web.UI.WebControls.Literal ltlAdditional;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			this.dgCPT.Columns[3].HeaderText = myUtility.RateYear + " National Medicare<br>OPPS Fee Schedule";

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				myUtility.ProductsTable = myUtility.SetTable("sp_GetProduct", myUtility.ProductID);
				dtCPT = myUtility.SetTable("sp_GetApcByProduct", myUtility.ProductID);
				dtICD = myUtility.SetTable("sp_GetHospitalIcdCodesByProductID", myUtility.ProductID);
				myUtility.CptIcdTable = myUtility.SetTable("sp_GetHospitalCptIcdByProduct", myUtility.ProductID);

				myUtility.SqlConn.Close();
				
				if(dtCPT.Rows.Count > 0)
					this.BindCPT();
				else
					dgCPT.Visible = false;

				if(dtICD.Rows.Count > 0)
					this.BindICD();
				else
					dgICD.Visible = false;

				if(myUtility.ProductsTable.Rows.Count>0)
				{
					DataRow aRow = myUtility.ProductsTable.Rows[0];

					ltlICD.Text = aRow["hospitalICD"].ToString();
					ltlPrivate.Text = aRow["hospitalPrivateIns"].ToString();
					ltlAdditional.Text = aRow["hospitalAdditional"].ToString();
					ltlCoding.Text = aRow["hospitalCoding"].ToString();
				
				}


			} // END isPostBack	
		
		} // Page_Load

		public void BindCPT()
		{
				dgCPT.DataSource = dtCPT;
				dgCPT.DataBind();
		} // END BindCPT

		public void BindICD()
		{
			dgICD.DataSource = dtICD;
			dgICD.DataBind();
		} // END BindCPT

		public string GetCrossReference(string id)
		{
			DataRow[] rows = myUtility.CptIcdTable.Select("productIcdCodeID = " + id);
			string strRef = "";
			string strDel = "";
			foreach(DataRow aRow in rows)
			{
				strRef += strDel + aRow["CPT_HCPCS"].ToString();
				strDel = ", ";
			}

			return strRef;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
