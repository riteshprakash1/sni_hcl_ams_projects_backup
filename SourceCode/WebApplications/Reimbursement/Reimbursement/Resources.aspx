<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="Resources.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.Resources" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table width="100%" style="BORDER-BOTTOM:#cccccc 1px dotted; BORDER-LEFT:#cccccc 1px dotted; PADDING-BOTTOM:5px; PADDING-LEFT:5px; PADDING-RIGHT:5px; BORDER-TOP:#cccccc 1px dotted; BORDER-RIGHT:#cccccc 1px dotted; PADDING-TOP:5px">
			<tr>
				<td>
					<table cellSpacing="1" cellPadding="1" width="100%" border="0">
						<tr>
							<td colspan="2"><b>Smith &amp; Nephew Advanced Surgical Devices Reimbursement Office:</b></td>
						</tr>
						<tr>
							<td vAlign="top">Dallas Office:</td>
							<td>
								<P>
									16650 Westgrove Drive, Suite 350<br>
									Addison, Texas 75001<br>
									Phone: (888) 711-9903<br>
									Main Fax: (901) 721-2475<br>
									Donna Robinson, Sr. Reimbursement Manager<br>
									Michelle Lowe, Reimbursement Website Manager
								</P>
								<p>&nbsp;</p>
							</td>
						</tr>
						<tr>
							<td vAlign="top">Andover Endoscopy Home Office:</td>
							<td>
								<p>150 Minuteman Road<br>
									Andover, MA 01810<br>
									Phone: (800) 544-2330<br>
								</p>
								<p>&nbsp;</p>
							</td>
						</tr>
						<tr>
							<td vAlign="top">Memphis Orthopaedics Home Office:</td>
							<td>
								<p>1450 Brooks Road<br>
									Memphis, TN 38116<br>
									Phone: (800) 821-5700<br>
								</p>
								<p>&nbsp;</p>
							</td>
						</tr>
						<tr height="40" valign="bottom">
							<td colspan="2">
								For further reimbursement questions or feedback on the website, please send an 
								email to:
								<asp:HyperLink id="lnkEmail" runat="server">HyperLink</asp:HyperLink>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
