using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for Links.
	/// </summary>
	public class Links : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Table tblLinks;
		protected Utility myUtility;
		protected DataTable dtLinks;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				myUtility.LinksTable = myUtility.SetTable("sp_GetLinkListByCategory", myUtility.ProductCategoryID);
				myUtility.SqlConn.Close();
				Session["Utility"] = myUtility;

				Debug.WriteLine("Link Rows="+myUtility.LinksTable.Rows.Count.ToString());

				foreach(DataRow aRow in myUtility.LinksTable.Rows)
				{
					TableRow linkRow = new TableRow();
					TableCell linkCell = new TableCell();
					HyperLink lnk = new HyperLink();

					lnk.NavigateUrl = aRow["linkUrl"].ToString();
					lnk.Text = "<img src=\"images/icn_orange_arrow.jpg\" alt=\"\" border=\"0\" />&nbsp;" + aRow["linkName"].ToString();
					lnk.Attributes.Add("onclick", "return confirm_leaving();");
					lnk.CssClass = "breadcrumbNav";
					
					linkCell.Controls.Add(lnk);
					linkRow.Controls.Add(linkCell);
					tblLinks.Controls.Add(linkRow);

				}

			}
			
		} // END Page_Load

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
