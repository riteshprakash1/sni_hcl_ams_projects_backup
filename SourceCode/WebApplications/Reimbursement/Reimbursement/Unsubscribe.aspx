<%@ Page language="c#" Codebehind="Unsubscribe.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.Unsubscribe" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>

<uc1:Header id="Header1" runat="server"></uc1:Header>

					<form id="Form1" method="post" runat="server">
						<div id="region-content">
							<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
							<table width="100%" style="BORDER-RIGHT:#cccccc 1px dotted; PADDING-RIGHT:5px; BORDER-TOP:#cccccc 1px dotted; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:#cccccc 1px dotted; PADDING-TOP:5px; BORDER-BOTTOM:#cccccc 1px dotted">
								<tr>
									<td>
										<TABLE cellSpacing="1" cellPadding="1" width="100%" border="0">
											<tr>
												<td colspan="3"><b> Smith &amp; Nephew Endoscopy Reimbursement Unsubscribe</b></td>
											</tr>
											<tr>
												<td width="25%">Unsubscribe Email Address:</td>
												<TD><asp:textbox id="txtEmail" runat="server" Width="250px"></asp:textbox>
													<asp:requiredfieldvalidator id="RequiredFieldValidator5" ForeColor="#FF7300" runat="server" Width="8px" ControlToValidate="txtEmail"
														ErrorMessage="The email address is required.">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator ForeColor="#FF7300" id="RegularExpressionValidator1" runat="server" Width="8px"
														ControlToValidate="txtEmail" ErrorMessage="Enter a valid email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:regularexpressionvalidator></TD>
												<td>
													<asp:button id="btnUnsubscribe" runat="server" Font-Size="10pt" Width="100px" Text="Unsubscribe"
														Height="21" BackColor="#79BDE9" CausesValidation="False" ForeColor="White" Font-Names="Arial ,Helvetica,sans-serif"
														BorderWidth="0px"></asp:button>
												</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;
													<asp:ValidationSummary id="ValidationSummary1" runat="server"></asp:ValidationSummary>
												</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;
													<asp:Label id="lblMsg" ForeColor="#FF7300" runat="server">Label</asp:Label>
												</td>
											</tr>
										</TABLE>
									</td>
								</tr>
							</table>
						</div>
					</form>

<uc1:Footer id="Footer1" runat="server"></uc1:Footer>