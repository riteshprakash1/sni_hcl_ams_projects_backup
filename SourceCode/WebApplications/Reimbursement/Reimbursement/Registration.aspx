<%@ Page language="c#" Codebehind="Registration.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.New_Subscription" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table width="100%" style="BORDER-BOTTOM:#cccccc 1px dotted; BORDER-LEFT:#cccccc 1px dotted; PADDING-BOTTOM:5px; PADDING-LEFT:5px; PADDING-RIGHT:5px; BORDER-TOP:#cccccc 1px dotted; BORDER-RIGHT:#cccccc 1px dotted; PADDING-TOP:5px">
			<tr>
				<td>
					<TABLE cellSpacing="3" cellPadding="2" width="100%" border="0">
						<tr>
							<td colspan="3" align="center">
								<asp:Label id="lblOccupationSpecialty" ForeColor="red" runat="server"></asp:Label></td>
						</tr>
						<TR>
							<TD align="right"><asp:label id="lblocc" runat="server" Width="80px" Font-Bold="True">Occupation:</asp:label></TD>
							<TD><asp:dropdownlist id="ddbocu" runat="server" Width="250px"></asp:dropdownlist>
								<asp:comparevalidator id="vldOccupation" ForeColor="#FF7300" runat="server" ErrorMessage="Occupation must be selected."
									ControlToValidate="ddbocu" ValueToCompare="0" Operator="NotEqual">*</asp:comparevalidator>
							</TD>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD width="30%" align="right"><asp:label id="lblspe" runat="server" Width="80px" Font-Bold="True">Specialty:</asp:label></TD>
							<TD width="38%"><asp:dropdownlist id="ddbspe" runat="server" Width="250px"></asp:dropdownlist>
								<asp:comparevalidator id="vldSpecialty" ForeColor="#FF7300" runat="server" ErrorMessage="Specialty must be selected."
									ControlToValidate="ddbspe" ValueToCompare="0" Operator="NotEqual">*</asp:comparevalidator>
							</TD>
							<TD width="32%">&nbsp;</TD>
						</TR>
						<TR>
							<TD align="right">
								<A class="hintanchor" onmouseover="showhint('Enter 99999 if outside of the US', this, event, '200px', 0, 30)"
									href="#">[?]</A>&nbsp;
								<asp:label id="lblzip" runat="server" Width="72px" Font-Bold="True">Zip Code:</asp:label></TD>
							<TD><asp:textbox id="txtzip" runat="server" Width="250px"></asp:textbox>
								<asp:requiredfieldvalidator ForeColor="#FF7300" id="RequiredFieldValidator4" runat="server" Width="8px" ControlToValidate="txtzip"
									ErrorMessage="Enter Zip Code">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator ForeColor="#FF7300" id="RegularExpressionValidator2" runat="server" Width="8px"
									ControlToValidate="txtzip" ErrorMessage="Enter a valid zip code" ValidationExpression="\d{5}(-\d{4})?">*</asp:regularexpressionvalidator>
							</TD>
							<td>&nbsp;</td>
						</TR>
						<TR>
							<TD align="right"><asp:label id="lblemail" runat="server" Width="160px" Font-Bold="True">Email Address:</asp:label></TD>
							<TD><asp:textbox id="txtemail" runat="server" Width="250px"></asp:textbox>
								<asp:requiredfieldvalidator id="RequiredFieldValidator5" ForeColor="#FF7300" runat="server" Width="8px" ControlToValidate="txtemail"
									ErrorMessage="Enter Email">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator ForeColor="#FF7300" id="RegularExpressionValidator1" runat="server" Width="8px"
									ControlToValidate="txtemail" ErrorMessage="Enter a valid email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:regularexpressionvalidator>
								<asp:CustomValidator id="vldExistEmail" ForeColor="#FF7300" runat="server" ErrorMessage="The email address already exists."
									ControlToValidate="txtemail">*</asp:CustomValidator></TD>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD align="right"><asp:label id="lblConfirmEmail" runat="server" Width="160px" Font-Bold="True">Confirm Email:</asp:label></TD>
							<TD><asp:textbox id="txtConfirmEmail" runat="server" Width="250px"></asp:textbox>
								<asp:requiredfieldvalidator id="vldRequiredConfirmEmail" ForeColor="#FF7300" runat="server" Width="8px" ControlToValidate="txtConfirmEmail"
									ErrorMessage="Enter Confirm Email">*</asp:requiredfieldvalidator>
								<asp:CustomValidator id="Customvalidator1" ForeColor="#FF7300" runat="server" ErrorMessage="Email Address mismatch."
									ControlToValidate="txtConfirmEmail">*</asp:CustomValidator></TD>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD align="right"><asp:label id="lblpassword" runat="server" Width="88px" Font-Bold="True">Password:</asp:label></TD>
							<TD><asp:textbox id="txtpass" runat="server" Width="250px" TextMode="Password"></asp:textbox>
								<asp:requiredfieldvalidator id="RequiredFieldValidator1" ForeColor="#FF7300" runat="server" Width="8px" ControlToValidate="txtpass"
									ErrorMessage="Enter Password">*</asp:requiredfieldvalidator>
								<asp:CustomValidator id="vldPassWordLen" ForeColor="#FF7300" runat="server" ErrorMessage="The length of the password must be between 4 and 10 characters long. "
									ControlToValidate="txtpass">*</asp:CustomValidator></TD>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD align="right"><asp:label id="lblconfirm" runat="server" Width="144px" Font-Bold="True">Confirm Password:</asp:label></TD>
							<TD><asp:textbox id="txtconfirm" runat="server" Width="250px" TextMode="Password"></asp:textbox>
								<asp:requiredfieldvalidator id="RequiredFieldValidator2" ForeColor="#FF7300" runat="server" ControlToValidate="txtconfirm"
									ErrorMessage="Enter Confirm Password">*</asp:requiredfieldvalidator><asp:comparevalidator id="CompareValidator1" ForeColor="#FF7300" runat="server" Width="8px" ControlToValidate="txtconfirm"
									ErrorMessage="Password Mismatch" ControlToCompare="txtpass">*</asp:comparevalidator></TD>
							<TD>&nbsp;</TD>
						</TR>
						<tr>
							<td>&nbsp;</td>
							<td><asp:checkbox id="chkRemeberMe" runat="server" Font-Bold="True" Text="Remember Me on this computer"></asp:checkbox></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="2">
								<asp:checkbox id="chkSubscribe" runat="server" TextAlign="Right" Font-Bold="True" Text="Subscribe to email updates"></asp:checkbox>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr vAlign="top" height="40">
							<td>&nbsp;</td>
							<td colSpan="3"><STRONG>If you would like to subscribe to email updates from the 
									"What's New" postings, please click the checkbox above:</STRONG></td>
						</tr>
						<TR>
							<TD colSpan="2" align="right">
								<asp:button id="btnSubmit" runat="server" Font-Size="10pt" Width="70px" Height="22" BackColor="#79BDE9"
									CausesValidation="False" ForeColor="White" Font-Names="Arial ,Helvetica,sans-serif" BorderWidth="0px"
									Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</TD>
						</TR>
						<tr>
							<TD>&nbsp;</TD>
							<TD><asp:label id="lblmsg" runat="server" Font-Bold="True"></asp:label></TD>
							<TD>&nbsp;</TD>
						</tr>
						<TR>
							<TD vAlign="top" colspan="2" align="left"><asp:validationsummary id="ValidationSummary1" ForeColor="#FF7300" runat="server"></asp:validationsummary></TD>
							<TD>&nbsp;</TD>
						</TR>
					</TABLE>
				</td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
