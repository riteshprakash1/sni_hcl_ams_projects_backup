using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for WhatsNew.
	/// </summary>
	public class ReferenceGuide : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.DataGrid dgFiles;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected DataTable dtWhatsNew;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
		protected System.Web.UI.WebControls.Label lblNote;
		protected System.Web.UI.WebControls.Literal Literal2;
		protected System.Web.UI.WebControls.Literal Literal1;

		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("Login.aspx");


			if(!Page.IsPostBack)
			{
				if(myUtility.ProductID != 0)
				{
					if(myUtility.SqlConn.State != ConnectionState.Open)
						myUtility.SqlConn.Open();
					
					// DEC 11/8/07		dtWhatsNew = myUtility.SetTable("sp_GetDocumentDisplay");
					myUtility.DocumentFilesTable = myUtility.SetTable("sp_GetReferenceFile",myUtility.ProductID);
					myUtility.SqlConn.Close();
					Session["Utility"] = myUtility;
                    if(myUtility.DocumentFilesTable.Rows.Count>0)
			             {
						this.BindDT();	
					}
					else
					{
						lblmsg.Text="No Records Available for this product";
					}
							
				}
			} // END !Page.IsPostBack
		} // END Page_Load

		public string GetFileLink(string strFile)
		{
			string strLink = "<img src=\"images/icn_orange_arrow.jpg\" alt=\"\" border=\"0\"/>&nbsp;" + strFile;
			return strLink;
		}

			
		public void BindDT()
		{
			dgFiles.DataSource = myUtility.DocumentFilesTable;
			dgFiles.DataBind();

			bool isVisible = Convert.ToBoolean(myUtility.DocumentFilesTable.Rows.Count != 0);
			dgFiles.Visible = isVisible;
				
		}


		protected void btnView_Click(object sender, DataGridCommandEventArgs e)
		{
			try
			{
				int ID = Int32.Parse(dgFiles.DataKeys[e.Item.ItemIndex].ToString());

				DataTable dt = myUtility.SetTable("sp_GetDocumentFiles",ID);
					
				if(dt.Rows.Count >0)
					this.ShowTheFile(dt.Rows[0]);
			}
			catch(Exception exc)
			{
				Debug.WriteLine(exc.Message);
			}	
		} // END btnView_Click


			
		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			int ID = Int32.Parse(dgFiles.DataKeys[e.Item.ItemIndex].ToString());
				
			DataTable dt = myUtility.SetTable("sp_GetDocumentFiles", ID);
			if(dt.Rows.Count >0)
				this.ShowTheFile(dt.Rows[0]);
		} // END btnEdit_Click



		public string GetImage(string fileName)
		{
			int intP = fileName.LastIndexOf('.');
			string ext = fileName.Substring(intP+1, 3).ToLower();
			Debug.WriteLine("FileName: " + fileName + "; Ext: " + ext);
			switch(ext)       
			{         
				case "doc":   
					return "images\\word16.gif";
				case "xls":            
					return "images\\excel16.gif";
				case "txt":            
					return "images\\text16.gif";
				case "pdf":            
					return "images\\acrobat16.gif";
				case "ppt":            
					return "images\\mspowerpoint16.gif";
				default:            
					return "images\\text16.gif";
			}

		}

		private void ShowTheFile(DataRow aRow)
		{
			string strFolder = "DocumentLibrary\\";
			string strFile = strFolder + (string)aRow["NameFile"];
			FileInfo myFile = new FileInfo(strFile);
			if(myFile.Exists)
				myFile.Delete();

			byte[] myData = (byte[])aRow["FileData"];
			Debug.WriteLine("Bytes = " + myData.Length.ToString());

			this.WriteToFile(strFile,(byte[])aRow["FileData"]);
			Response.Redirect(strFile);

//			string strUrl = ConfigurationSettings.AppSettings["reimbursementSite"].ToString()+ "DocumentLibrary/"+aRow["NameFile"].ToString();
//			Debug.WriteLine("File: " + strFile + "; URL: " + strUrl);
//			Literal1.Text = "<script type='text/javascript'>Start('" + strUrl + "',document.forms[0]);</script>";
		} // END ShowTheFile

		private void WriteToFile(string strFileName, byte[] Buffer)
		{
			Debug.WriteLine("Impersonator ID : " + System.Security.Principal.WindowsIdentity.GetCurrent().Name);
			string myPath = Request.ServerVariables["APPL_PHYSICAL_PATH"];
			string strPath = myPath + strFileName;
			// Create a file
			Debug.WriteLine("File = " + strPath);
			FileStream newFile = new FileStream(strPath, FileMode.Create);

			// Write data to the file
			newFile.Write(Buffer, 0, Buffer.Length);

			// Close file
			newFile.Close();
		} // END WriteToFile

		private void LinkButton1_Click(object sender, DataGridCommandEventArgs e)
		{
			int ID = Int32.Parse(dgFiles.DataKeys[e.Item.ItemIndex].ToString());
				
			DataTable dt = myUtility.SetTable("sp_GetDocumentFile", ID);
			if(dt.Rows.Count >0)
				this.ShowTheFile(dt.Rows[0]);
		} // END btnView_Click
		//	


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
