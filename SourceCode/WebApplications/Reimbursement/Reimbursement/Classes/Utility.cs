using System;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Collections.Specialized;

namespace Reimbursement.Classes
{
	/// <summary>
	/// Summary description for Utility.
	/// </summary>
	public class Utility
	{

		protected string strLoginCookieName;
		protected string strRateYear;
		protected int intProductCategoryID;
		protected int intSpecialty_ID;
		protected int intOccupation_ID;
		protected int intDocID;
		protected int intProductID;
		protected int  intEmail_ID ;
		protected int intDocumentID;
		protected int intWhatsNewID;
		protected int intLinkRows;
		protected SqlConnection sqlConnection1;
		protected DataTable dtFacility;
		protected DataTable dtOccupation;
		protected DataTable dtPayer;
		protected DataTable dtProvider;
		protected DataTable dtStates;
		protected DataTable dtCities;
		protected DataTable dtWageIndex;
		protected DataTable dtPayment;
		protected DataTable dtProductCategory;
        protected DataTable dtDocumentFiles;
		protected DataTable dtSpecialty;
		protected DataTable dtProducts;
		protected DataTable dtProduct;
		protected DataTable dtProductCodes;
		protected DataTable dtICD;
		protected DataTable dtCptIcd;
		protected DataTable dtContentType;
		protected DataTable dtPages;
		protected bool blnAdmin;
		protected DataTable dtItems;
		protected bool blnIsDraftReader;
		protected bool blnIsAcceptedDisclaimer;
		protected bool blnDisclaimerViewed;
		protected DataTable dtParentMenu;
		protected DataTable dtSubMenu;
		protected DataTable dtGlobalVariables;
		protected DataTable dtProductInfo;
		protected int intFaqID;
		protected DataTable dtprodretrive;
		protected int intTermID;
		protected DataTable dtFaqs;
		protected DataTable dtTerms;
		protected DataTable dtLinks;
		protected DataTable dtWhatsNew;
		protected DataTable dtWhatsNewFiles;
		protected DataTable dtUsers;
		protected DataTable dtPublication;
		protected DataTable dtUserProfile;
		protected DataTable dtSurvey;
		private string strPassKey = "Pass";
		private string strUserKey = "User";
		protected bool blnSurveyChecked;
		protected int intSurveyID;

		public Utility()
		{
			this.RateYear = ConfigurationSettings.AppSettings["rateYear"].ToString();

			string strConn = ConfigurationSettings.AppSettings["sqlConn"].ToString();
			this.SqlConn = new SqlConnection();
			this.SqlConn.ConnectionString = strConn;

			this.LoginCookieName = "SNCookie";
			this.WhatsNewID = 0;
			this.DocumentID = 0;
			this.EmailID = 0;
			this.DocID = 0;
			this.TermID = 0;
			this.FaqID = 0;
			this.SurveyID = 0;
			this.ProductCategoryID = 0;
			blnAdmin = false;
			this.IsDraftReader = false;
			this.LoadTables();
				
		} // END  constructor

		/********************* Getters and Setters ***************************/
		
		public SqlConnection SqlConn
		{
			get {return sqlConnection1;}
			set {sqlConnection1 = value;}
		}
	
		public string RateYear
		{
			get {return strRateYear;}
			set {strRateYear = value;}
		}
		
		public string LoginCookieName
		{
			get {return strLoginCookieName;}
			set {strLoginCookieName = value;}
		}

		public string PassKey
		{
			get {return strPassKey;}
			set {strPassKey = value;}
		}
		
		public string UserKey
		{
			get {return strUserKey;}
			set {strUserKey = value;}
		}

		public bool DisclaimerViewed
		{
			get {return blnDisclaimerViewed;}
			set {blnDisclaimerViewed = value;}
		}

		public int DocID
		{
			get {return intDocID;}
			set {intDocID = value;}

		}
		public DataTable ProductRetriveTable
		{
			get {return dtprodretrive;}
			set {dtprodretrive = value;}
		}
		
		public DataTable UsersTable
		{
			get {return dtUsers;}
			set {dtUsers = value;}
		}

		public bool IsDraftReader
		{
			get {return blnIsDraftReader;}
			set {blnIsDraftReader = value;}
		}

	
		public DataTable DocumentFilesTable
		{
			get {return dtDocumentFiles;}
			set {dtDocumentFiles = value;}
		}

		public int DocumentID
		{
			get {return intDocumentID;}
			set {intDocumentID = value;}
		}

		public bool IsAcceptedDisclaimer
		{
			get {return blnIsAcceptedDisclaimer;}
			set {blnIsAcceptedDisclaimer = value;}
		}
		
		public DataTable LinksTable
		{
			get {return dtLinks;}
			set {dtLinks = value;}
		}

		public DataTable WhatsNewTable
		{
			get {return dtWhatsNew;}
			set {dtWhatsNew = value;}
		}
		public DataTable WhatsNewFilesTable
		{
			get {return dtWhatsNewFiles;}
			set {dtWhatsNewFiles = value;}
		}

		public int WhatsNewID
		{
			get {return intWhatsNewID;}
			set {intWhatsNewID = value;}
		}

		public int FaqID
		{
			get {return intFaqID;}
			set {intFaqID = value;}
		}

		public int TermID
		{
			get {return intTermID;}
			set {intTermID = value;}
		}

		public int LinkRows
		{
			get {return intLinkRows;}
			set {intLinkRows = value;}
		}

		public bool IsAdmin
		{
			get {return blnAdmin;}
			set {blnAdmin = value;}
		}

		public DataTable FaqsTable
		{
			get {return dtFaqs;}
			set {dtFaqs = value;}
		}

		public DataTable TermsTable
		{
			get {return dtTerms;}
			set {dtTerms = value;}
		}

		public DataTable ProductInfoTable
		{
			get {return dtProductInfo;}
			set {dtProductInfo = value;}
		}

		public DataTable ParentMenuTable
		{
			get {return dtParentMenu;}
			set {dtParentMenu = value;}
		}

		public DataTable SubMenuTable
		{
			get {return dtSubMenu;}
			set {dtSubMenu = value;}
		}

		public DataTable GlobalVariablesTable
		{
			get {return dtGlobalVariables;}
			set {dtGlobalVariables = value;}
		}


		public DataTable PagesTable
		{
			get {return dtPages;}
			set {dtPages = value;}
		}

		public DataTable CptIcdTable
		{
			get {return dtCptIcd;}
			set {dtCptIcd = value;}
		}

		public DataTable ContentTypeTable
		{
			get {return dtContentType;}
			set {dtContentType = value;}
		}

		public DataTable ProductCodesTable
		{
			get {return dtProductCodes;}
			set {dtProductCodes = value;}
		}

		public DataTable ICDTable
		{
			get {return dtICD;}
			set {dtICD = value;}
		}
		
		public DataTable FacilityTable
		{
			get {return dtFacility;}
			set {dtFacility = value;}
		}

		public DataTable PayerTable
		{
			get {return dtPayer;}
			set {dtPayer = value;}
		}

		public DataTable ProviderTable
		{
			get {return dtProvider;}
			set {dtProvider = value;}
		}
		
		public DataTable StatesTable
		{
			get {return dtStates;}
			set {dtStates = value;}
		}

		public DataTable WageIndexTable
		{
			get {return dtWageIndex;}
			set {dtWageIndex = value;}
		}

		public DataTable PaymentTable
		{
			get {return dtPayment;}
			set {dtPayment = value;}
		}

		public DataTable CitiesTable
		{
			get {return dtCities;}
			set {dtCities = value;}
		}

		public DataTable ProductCategoryTable
		{
			get {return dtProductCategory;}
			set {dtProductCategory = value;}
		}

		public DataTable OccupationTable
		{
			get {return dtOccupation;}
			set {dtOccupation = value;}
		}
		

		public DataTable ProductsTable
		{
			get {return dtProducts;}
			set {dtProducts = value;}
		}
		public DataTable ItemsTable
		{
			get {return dtItems;}
			set {dtItems = value;}
		}

		public DataTable ProductTable
		{
			get {return dtProduct;}
			set {dtProduct = value;}
		}
		public DataTable SpecialtyTable
		{
			get {return dtSpecialty;}
			set {dtSpecialty = value;}
		}

		public DataTable UserProfileTable
		{
			get {return dtUserProfile;}
			set {dtUserProfile = value;}
		}
		
		public DataTable SurveyTable
		{
			get {return dtSurvey;}
			set {dtSurvey = value;}
		}

		public int ProductCategoryID
		{
			get {return intProductCategoryID;}
			set {intProductCategoryID = value;}
		}
		public int Occupation_ID
		{
			get {return intOccupation_ID;}
			set {intOccupation_ID = value;}
		}
		public int Specialty_ID
		{
			get {return intSpecialty_ID;}
			set {intSpecialty_ID = value;}
		}
		public int EmailID
		{
			get {return intEmail_ID;}
			set {intEmail_ID = value;}
		}

		public int ProductID
		{
			get {return intProductID;}
			set {intProductID = value;}
		}

		public DataTable PublicationTable
		{
			get {return dtPublication;}
			set {dtPublication = value;}
		}	
	
		public bool SurveyChecked
		{
			get {return blnSurveyChecked;}
			set {blnSurveyChecked = value;}
		}	
		
		public int SurveyID
		{
			get {return intSurveyID;}
			set {intSurveyID = value;}
		}

		/********************* END Getters and Setters ***************************/


		public void LoadTables()
		{
			if(this.SqlConn.State != ConnectionState.Open)
				this.SqlConn.Open();

			dtFacility = this.SetTable("sp_GetFacilities");
			dtStates = this.SetTable("sp_GetStates");
			dtContentType = this.SetTable("sp_GetContentTypes");
			dtPages = this.SetTable("sp_GetApplicationPages");
			Debug.WriteLine("dtPages: " + dtPages.Rows.Count.ToString());
			this.ProductInfoTable = this.SetTable("sp_GetProducts");
			this.ProductCategoryTable = this.SetTable("sp_GetProductCategories");
			Debug.WriteLine("Category Rows: " + this.ProductCategoryTable.Rows.Count.ToString());

            this.OccupationTable=this.SetTable("sp_GetOccupation");
			this.SpecialtyTable=this.SetTable("sp_GetSpecialty");
//			this.EmailTable=this.Email("sp_getEmailAddress");
			this.UsersTable = this.SetTable("sp_GetReimbursementUsers");
//			this.SurveyTable = this.SetTable("sp_GetSurveysInProgressByUserID", this.EmailID);
			this.SqlConn.Close();
		} // END LoadTables


		public void ExcludeDraftProductCategories()
		{
			/* Exclude Draft Product Categories */
			DataRow[] rows = this.ProductCategoryTable.Select();
			this.ProductCategoryTable = this.ProductCategoryTable.Clone();
			
			foreach(DataRow aRow in rows)
			{
				Debug.WriteLine("EXCLUDER productCategoryID="+aRow["productCategoryID"].ToString());

				string strTest = "productCategoryID="+aRow["productCategoryID"].ToString();
				strTest += " AND productStatus<>'Draft' ";
				DataRow[] testRow = dtProductInfo.Select(strTest);
				if(testRow.Length!=0)
					this.ProductCategoryTable.ImportRow(aRow);
			}
		}

		public DataTable SqlExecuteQuery(SqlCommand command)
		{
			DataTable myDataTable = new DataTable();

			command.Connection = this.SqlConn;
			command.CommandType = CommandType.StoredProcedure;

			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(myDataTable);
			da.Dispose();
			command.Connection.Close();

			return myDataTable;
		}

		public DataTable SetTable(string sp)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		public DataTable SetTable(string sp, int id)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@ID", id));

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
				//	Debug.WriteLine("myDT Rows = " + myDT.Rows.Count.ToString());
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		public DataTable SetTable(string sp, string st)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@st", st));

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
				Debug.WriteLine("myDT Rows = " + myDT.Rows.Count.ToString());
				Debug.WriteLine("sp: " + sp + "; st: " + st);
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()


		
		public DataTable SetTable(string sp, string st, string st1)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@st", st));
			cmdSetTable.Parameters.Add(new SqlParameter("@st1", st1));

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
				//	Debug.WriteLine("myDT Rows = " + myDT.Rows.Count.ToString());
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		public DataTable Email(string EmailID)

		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = "sp_getEmailAddress";
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection  = this.SqlConn;
       
			DataTable myDT = new DataTable();
			 
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
				
			}
			catch(Exception exc)
			{
				Debug.WriteLine(" SetTable ERROR" +  exc.Message);
			}	
			//	
						return(myDT);
		} 
		// END SetTable()	

		public void SetIsAdmin(string strEmail)
		{
			string strCrit = "emailAddress='"+strEmail+"' AND userRole='Admin'";
			DataRow[] myRows = this.UsersTable.Select(strCrit);
			this.IsDraftReader = Convert.ToBoolean(myRows.Length>0);

		} // END
		
		
		public void SetIsDraftReader(string strEmail)
		{
			string strCrit = "emailAddress='"+strEmail+"'";
			DataRow[] myRows = this.UsersTable.Select(strCrit);
			this.IsDraftReader = Convert.ToBoolean(myRows.Length>0);
		} // END

		public void SetFocus(Control control)
		{
			StringBuilder sb = new StringBuilder();
 
			sb.Append("\r\n<script language='JavaScript'>\r\n");
			sb.Append("<!--\r\n"); 
			sb.Append("function SetFocus()\r\n"); 
			sb.Append("{\r\n"); 
			sb.Append("\tdocument.");
 
			Control p = control.Parent;
			while (!(p is System.Web.UI.HtmlControls.HtmlForm)) p = p.Parent; 
 
			sb.Append(p.ClientID);
			sb.Append("['"); 
			sb.Append(control.UniqueID); 
			sb.Append("'].focus();\r\n"); 
			sb.Append("}\r\n"); 
			sb.Append("window.onload = SetFocus;\r\n"); 
			sb.Append("// -->\r\n"); 
			sb.Append("</script>");
 
			control.Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
		} // END SetFocus


		public bool SetCookie(System.Web.HttpResponse res, string strUser, string strPass ,int iDaysToExpire)
		{
			try
			{
				HttpCookie objCookie = new HttpCookie(this.LoginCookieName);
				res.Cookies.Clear();
				res.Cookies.Add(objCookie);

				string strCrypPass = CryptoUtil.Encrypt(strPass);
				string strCrypUser = CryptoUtil.Encrypt(strUser);
				string strCrypPassKey = CryptoUtil.Encrypt(this.PassKey);
				string strCrypUserKey = CryptoUtil.Encrypt(this.UserKey);
				
				NameValueCollection coll = new NameValueCollection();
				coll.Add(strCrypUserKey, strCrypUser);
				coll.Add(strCrypPassKey, strCrypPass);

				objCookie.Values.Add(coll);
				DateTime dtExpiry = DateTime.Now.AddDays(iDaysToExpire);
				res.Cookies[this.LoginCookieName].Expires =dtExpiry;
			}
			catch( Exception e)
			{
				Debug.WriteLine("Error: " + e.Message);
				return false;
			}
			return true;
		}

		public bool SetExpireCookie(System.Web.HttpResponse res)
		{
			try
			{
				HttpCookie objCookie = new HttpCookie(this.LoginCookieName);
				res.Cookies.Clear();
				res.Cookies.Add(objCookie);
				res.Cookies[this.LoginCookieName].Expires = DateTime.Now.AddYears(-30);
			}
			catch( Exception e)
			{
				Debug.WriteLine("Error: " + e.Message);
				return false;
			}
			return true;
		}

		public int GetPageGroupID(System.Web.HttpRequest req)
		{
			string strMyPage = this.GetPageFileName(req).ToLower();
			string strWhere = "PageFile = '" + strMyPage + "'";
			DataRow[] rows = this.PagesTable.Select(strWhere);

			int intID=0;
			if(rows.Length > 0)
			{
				intID = Convert.ToInt32(rows[0]["pageLevel"].ToString());
			}

			return intID;
			
		} // end GetPageGroupID

		
		public string GetPageFileName(System.Web.HttpRequest req)
		{
			string myPage = "";
			string pathInfo = req.ServerVariables["PATH_INFO"];
			string[] strPages = pathInfo.Split('/');
			for(int x=0;x<strPages.Length;x++)
			{
				myPage = strPages[x];
			}
			

			return myPage;
		} // END GetPage


		public string GetReferPage(System.Web.HttpRequest req)
		{
			string myPage = "";
			string pathInfo = req.ServerVariables["HTTP_REFERER"];
			if(pathInfo != null)
			{
				string[] strPages = pathInfo.Split('/');
				for(int x=0;x<strPages.Length;x++)
				{
					myPage = strPages[x];
				}
			}

			return myPage;
		} // END GetPage
		
		public int GetPageID(string strFileName)
		{
			int pageID = 0;
			DataRow[] rows = this.PagesTable.Select("pageFile = '" + strFileName + "'");
			if(rows.Length > 0)
				pageID = Int32.Parse(rows[0]["applicationPageID"].ToString());

			return pageID;
		} // END GetPageID

		public string GetPageName(System.Web.HttpRequest req)
		{
			string strPageFileName = this.GetPageFileName(req);
			string strPageTitleName = string.Empty;

			DataRow[] rows = this.PagesTable.Select("pageFile = '" + strPageFileName + "'");
			
			if(rows.Length > 0)
				strPageTitleName = rows[0]["pageName"].ToString();

			return strPageTitleName;
		} // END GetPageName

		public bool HasOccupationSpecialty()
		{
			bool hasOccupationSpecialty = false;
			if(this.UserProfileTable != null)
			{
				if(this.UserProfileTable.Rows.Count > 0)
				{
					DataRow r = this.UserProfileTable.Rows[0];
					bool hasSpecialty = Convert.ToBoolean(string.Empty != r["SpecialtyID"].ToString());
					bool hasOccupation = Convert.ToBoolean(string.Empty != r["OccupationID"].ToString());
					hasOccupationSpecialty = Convert.ToBoolean(hasSpecialty && hasOccupation);
				}
			}

			return hasOccupationSpecialty;
		} //// HasOccupationSpecialty

		public void LogHit(string strEmail, string strMsg)
		{
			Debug.WriteLine("LogHit");

			// log login
			SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
			cmdInsert.CommandType = CommandType.StoredProcedure;
			cmdInsert.Connection = this.SqlConn;
			cmdInsert.CommandText = "sp_InsertUserHit";

			cmdInsert.Parameters.Add(new SqlParameter("@EmailAddress", strEmail));
			cmdInsert.Parameters.Add(new SqlParameter("@LogHit", strMsg));
						
			try
			{   
				Debug.WriteLine("Before Exec LogHit");
				int rowsUpdated = cmdInsert.ExecuteNonQuery();		
				Debug.WriteLine("After Exec LogHit");
			}
			catch(Exception exc)
			{
				Debug.WriteLine(cmdInsert.CommandText + " Error - " + exc.Message);
				this.SqlConn.Close();
			}	
		
		} //END LogHit

		public DataTable GetUserInfo(string strEmail, string strPass)
		{
			SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Connection = this.SqlConn;
			cmd.CommandText = "sp_GetUserValid";
				
			cmd.Parameters.Add(new SqlParameter("@EmailAddress", strEmail));
			cmd.Parameters.Add(new SqlParameter("@Password", strPass));
			DataTable myDT = new DataTable();

			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(myDT);
			}
			catch(Exception exc) 
			{
				Debug.WriteLine("sp_GetUserValid SetTable ERROR" +  exc.Message);
				this.SqlConn.Close();
			}
		
			return myDT;
		}

		/// <summary>Executes a query with SqlParameters</summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameters">An ArrayList of SqlParameters.</param>
		/// <returns>A datatable of the results.</returns>
		public DataTable SqlExecuteQueryWithParameters(string storedProcedureName, ArrayList parameters)
		{
			DataTable myDataTable = new DataTable();
			SqlConnection SqlConn = this.SqlConn;

			SqlCommand command = new SqlCommand();
			command.Connection = this.SqlConn;
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = storedProcedureName;

			for(int x=0; x<parameters.Count; x++)
			{
				SqlParameter parameter = parameters[x] as SqlParameter;
				command.Parameters.Add(parameter);
			}

			if(this.SqlConn.State != ConnectionState.Open)
				this.SqlConn.Open();

			SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
			myDataAdapter.Fill(myDataTable);
			myDataAdapter.Dispose();

			this.SqlConn.Close();

			return myDataTable;
		}

		/// <summary>Detremines whether to issue a survey request and returns True/False</summary>
		/// <param name="dtSurvey">DataTable with Survey Records</param>
		/// <returns>returns zerofor no survey otherwise it returns the SurveyID</returns>
		protected int IssueSurvey(DataTable dtSurvey)
		{
			if(this.SurveyTable.Rows.Count > 0)
			{
				DataRow r = this.SurveyTable.Rows[0];
				int intSurveyID = Convert.ToInt32(r["SurveyID"].ToString());
				string strSurveyResponseID = r["SurveyResponseID"].ToString();
				string strUserResponse = r["UserResponse"].ToString();
				string strResponseDate = r["ResponseDate"].ToString();
				string strResponseTime = r["ResponseTime"].ToString();

				if(strSurveyResponseID == string.Empty)
					return intSurveyID;
				else
				{
					if(strUserResponse == "Later")
					{
						string strMonth = strResponseDate.Substring(0,2);
						string strDay = strResponseDate.Substring(3,2);
						string strYear = strResponseDate.Substring(6,4);
						
						string strHour = strResponseTime.Substring(0,2);
						string strMin = strResponseTime.Substring(3,2);
						string strSec = strResponseTime.Substring(6,2);
						
						DateTime dtResponse = new DateTime(Convert.ToInt32(strYear), Convert.ToInt32(strMonth), Convert.ToInt32(strDay), Convert.ToInt32(strHour),Convert.ToInt32(strMin), Convert.ToInt32(strSec)); 
						TimeSpan s = DateTime.Now.Subtract(dtResponse);

						int intSurveyDelayHours = Convert.ToInt32(ConfigurationSettings.AppSettings["SurveyDelayHours"].ToString());

						if(s.TotalHours > intSurveyDelayHours)
							return intSurveyID;
						else
							return 0;
					}
					else
						return 0;
				}

			}
			
			return 0;
		}


		/// <summary>Checks whether the user should be prompted to take a survey. If yes, it returns the SurveyID else it returns zero.</summary>
		/// <param name="intProductID">The product ID</param>
		/// <returns>zero for no survey otherwise the SurveyID for the survey to prompt</returns>
		public int RequestProductSurvey(int intProductID)
		{
			ArrayList parameters = new ArrayList();
			parameters.Add(new SqlParameter("@UserID", this.EmailID));
			parameters.Add(new SqlParameter("@ProductID", intProductID));

			this.SurveyTable = this.SqlExecuteQueryWithParameters("sp_GetSurveysInProgressByUserID", parameters);
			return this.IssueSurvey(this.SurveyTable);

		} // END



		
	} // END class
}
