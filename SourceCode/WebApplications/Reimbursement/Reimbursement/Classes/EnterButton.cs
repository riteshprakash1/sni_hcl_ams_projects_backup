using System;

namespace Reimbursement.Classes
{
	/// <summary>
	/// Summary description for EnterButton.
	/// </summary>
	public class EnterButton
	{
		/// <summary>
		///     This ties a textbox to a button. 
		/// </summary>
		/// <param name="TextBoxToTie">
		///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
		///     and the html control should accept an 'onkeydown' attribute.
		/// </param>
		/// <param name="ButtonToTie">
		///     This is the button to tie to. All we need from this is it's ClientID. The Html tag it renders should support click()
		/// </param>
		/// 

		public static void TieButton(System.Web.UI.Control TextBoxToTie, System.Web.UI.Control ButtonToTie)
		{
			string formName;
			try
			{
				int i=0;
				System.Web.UI.Control c = ButtonToTie.Parent;
				// Step up the control hierarchy until either:
				// 1) We find an HtmlForm control
				// 2) We find a Page control - not what we want, but we should stop searching because we a Page will be higher than the HtmlForm.
				// 3) We complete 500 iterations. Obviously we are in a loop, and should stop.
				while(! (c is System.Web.UI.HtmlControls.HtmlForm) &! (c is System.Web.UI.Page) && i<500)
				{
					c=c.Parent;
					i++;
				}
				// If we have found an HtmlForm, we use it's ClientID for the formName.
				// If not, we use the first form on the page ("forms[0]").
				if (c is System.Web.UI.HtmlControls.HtmlForm)
					formName = c.ClientID;
				else
					formName = "forms[0]";
			}
			catch
			{
				//If we catch an exception, we should use the first form on the page ("forms[0]").
				formName = "forms[0]";
			}
			// Tie the button.
			TieButton(TextBoxToTie, ButtonToTie, formName);
		}

		/// <summary>
		///     This ties a textbox to a button. 
		/// </summary>
		/// <param name="TextBoxToTie">
		///     This is the textbox to tie to. It doesn't have to be a TextBox control, but must be derived from either HtmlControl or WebControl,
		///     and the html control should accept an 'onkeydown' attribute.
		/// </param>
		/// <param name="ButtonToTie">
		///     This is the button to tie to. All we need from this is it's ClientID. The Html tag it renders should support click()
		/// </param>
		/// <param name="formName">
		///     This is the ClientID of the form that the button resides in.
		/// </param>
		public static void TieButton(System.Web.UI.Control TextBoxToTie, System.Web.UI.Control ButtonToTie, string formName)
		{
			// This is our javascript - we fire the client-side click event of the button if the enter key is pressed.
			string jsString = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {document."+formName+".elements['"+ButtonToTie.UniqueID+"'].click();return false;} else return true; ";
			// We attach this to the onkeydown attribute - we have to cater for HtmlControl or WebControl.
			if (TextBoxToTie is System.Web.UI.HtmlControls.HtmlControl)
				((System.Web.UI.HtmlControls.HtmlControl)TextBoxToTie).Attributes.Add("onkeydown",jsString);
			else if (TextBoxToTie is System.Web.UI.WebControls.WebControl)
				((System.Web.UI.WebControls.WebControl)TextBoxToTie).Attributes.Add("onkeydown",jsString);
			else
			{
				// We throw an exception if TextBoxToTie is not of type HtmlControl or WebControl.
				throw new ArgumentException("Control TextBoxToTie should be derived from either System.Web.UI.HtmlControls.HtmlControl or System.Web.UI.WebControls.WebControl", "TextBoxToTie");
			}
		}

		public EnterButton()
		{
			//
			// TODO: Add constructor logic here
			//
		}


	} // End Class EnterButton
} // END namespace
