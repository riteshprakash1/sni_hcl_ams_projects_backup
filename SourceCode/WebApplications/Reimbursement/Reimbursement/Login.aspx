<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.Login" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<META content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:titlenavigation id="TitleNavigation2" runat="server"></uc1:titlenavigation>
		<TABLE width="100%" border="0">
			<tr>
				<td>
					<table style="BORDER-BOTTOM: #cccccc thin solid; BORDER-LEFT: #cccccc thin solid; BORDER-TOP: #cccccc thin solid; BORDER-RIGHT: #cccccc thin solid"
						cellSpacing="0" cellPadding="0" width="390">
						<tr>
							<td>
								<table width="100%" bgColor="whitesmoke">
									<tr height="30">
										<td align="center" colSpan="2"><asp:label id="Label1" runat="server" Font-Size="12pt" Font-Bold="True">Please Sign in to Continue</asp:label></td>
									</tr>
									<tr>
										<td colSpan="2">
											<hr SIZE="1">
										</td>
									</tr>
									<tr>
										<td align="right" width="85%"><asp:label id="lblEmail" runat="server" Font-Bold="True">Email Address:&nbsp;</asp:label><asp:textbox id="txtEmail" runat="server" Font-Size="10pt" MaxLength="50" Width="200"></asp:textbox></td>
										<td><asp:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" Width="8px" ErrorMessage="Enter E-mail"
												ControlToValidate="txtEmail">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" Width="8px" ErrorMessage="Enter a valid email"
												ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:regularexpressionvalidator></td>
									</tr>
									<TR>
										<TD align="right"><asp:label id="lblPassword" runat="server" Font-Bold="True">Password:&nbsp;</asp:label><asp:textbox id="txtPass" runat="server" Font-Size="10pt" MaxLength="50" Width="200" TextMode="Password"></asp:textbox>
										<td><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Width="8px" ErrorMessage="Enter Password"
												ControlToValidate="txtPass">*</asp:requiredfieldvalidator></td>
									</TR>
									<tr>
										<td align="right"><asp:checkbox id="chkRemeberMe" runat="server" Font-Bold="True" Text="Remember Me on this computer"></asp:checkbox></td>
										<td>&nbsp;</td>
									</tr>
									<TR height="30">
										<td align="right">
											<asp:button id="btnLogin" runat="server" Font-Size="10pt" Width="70px" Text="Login" Height="21"
												BackColor="#79BDE9" CausesValidation="False" ForeColor="White" Font-Names="Arial ,Helvetica,sans-serif"
												BorderWidth="0px"></asp:button></td>
										<td><asp:customvalidator id="vldBadLogin" runat="server" ErrorMessage="The login and password you entered are incorrect. Please try &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;again. <br><br>If you have not already registered, please do so before logging &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;in by clicking on the 'I want to register' link below."
												ControlToValidate="txtEmail">*</asp:customvalidator></td>
									</TR>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td vAlign="middle" align="left" width="280"><asp:validationsummary id="ValidationSummary1" runat="server" ForeColor="#FF7300" ShowMessageBox="True"
						HeaderText="Validation Error(s) noted below:"></asp:validationsummary></td>
			</tr>
			<tr height="25">
				<td>
					<asp:linkbutton id="btnWhyRegister" runat="server" Height="22" BorderColor="white" BackColor="White"
						CausesValidation="False" ForeColor="#FF7300">
<asp:Image id="imgRegister" runat="server" ImageUrl="images/icn_orange_arrow.jpg"></asp:Image>&nbsp;Why do I need to Register?</asp:linkbutton></td>
			</tr>
			<tr vAlign="middle" height="25">
				<td><asp:linkbutton id="btnForgotPassword" runat="server" Height="22" BorderColor="white" BackColor="White"
						CausesValidation="False" ForeColor="#FF7300">
<asp:Image id="Image1" runat="server" ImageUrl="images/icn_orange_arrow.jpg"></asp:Image>&nbsp;Forgot Password?</asp:linkbutton></td>
			</tr>
		</TABLE>
		<table>
			<tr height="1">
				<td style="BORDER-BOTTOM: #cccccc thick double" colSpan="2">&nbsp;</td>
			</tr>
			<tr height="25">
				<td><asp:label id="lblHaveRegister" runat="server">Haven't Registered?</asp:label></td>
			</tr>
			<tr height="25">
				<td><asp:label id="lblRegistering" runat="server">Registering is Quick and Easy.</asp:label></td>
			</tr>
			<TR height="30">
				<td>
					<table>
						<tr>
							<TD align="right" width="50%"><asp:linkbutton id="lnkRegister" runat="server" Height="22" BorderColor="white" BackColor="White"
									CausesValidation="False" ForeColor="#FF7300">
<asp:Image id="Image2" runat="server" ImageUrl="images/icn_orange_arrow.jpg"></asp:Image>&nbsp;I want to Register</asp:linkbutton></TD>
						</tr>
					</table>
				</td>
			</TR>
			<tr vAlign="bottom" height="30">
				<td>For further reimbursement questions or feedback on the website, please send an 
					email to:
					<asp:hyperlink id="lnkEmail" runat="server">HyperLink</asp:hyperlink></td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
