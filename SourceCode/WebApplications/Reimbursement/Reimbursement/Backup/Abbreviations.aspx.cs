using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for Abbreviations.
	/// </summary>
	public class Abbreviations : System.Web.UI.Page
	{
	//	protected System.Web.UI.WebControls.Literal ltlContent;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Table tblTerms;
		protected string strSection = "";
		protected System.Web.UI.WebControls.Button termsBottom;
		protected System.Web.UI.WebControls.Button btnCalculate;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("Page_Load controls = " + tblTerms.Controls.Count.ToString());

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();
				myUtility.TermsTable = myUtility.SetTable("sp_GetTerms");
				myUtility.SqlConn.Close();
				Session["Utility"] = myUtility;

			} // END !Page.IsPostBack

			this.DisplayList();
		
		} // END Page_Load

		protected void DisplayList()
		{
			DataRow[] rows = myUtility.TermsTable.Select();

			// Add row of instructions
			if(rows.Length > 0)
			{
				string strInstr = "To view the terminology definition in one of the category areas, click below on the area of choice and then on the orange terminology link.<br><br>";
					TableRow rInstr = new TableRow();
				TableCell cInstr = new TableCell();
				Label lblInstr = new Label();
				lblInstr.Text = strInstr;
				lblInstr.Font.Size = FontUnit.Point(10);
				cInstr.Wrap = false;
				cInstr.ColumnSpan = 2;

				cInstr.Controls.Add(lblInstr);
				rInstr.Controls.Add(cInstr);
				tblTerms.Controls.Add(rInstr);

//				// Add breadcrumb
//				Literal ltlBread = new Literal();
//				string strTermSection = "";
//				foreach(DataRow aRow in rows)
//				{
//					if(strTermSection != aRow["termSection"].ToString())
//					{
//						strTermSection = aRow["termSection"].ToString();
//
//						if(ltlBread.Text != "")
//							ltlBread.Text += " | <a href=#" + strTermSection.Replace(" ", "") + " Class=\"abbrevLinks\">" + strTermSection+ "</a>";
//						else
//							ltlBread.Text += "<a href=#" + strTermSection.Replace(" ", "") + " Class=\"abbrevLinks\">" + strTermSection+ "</a>";
//					}
//				}
//					
//				TableRow rBread = new TableRow();
//				TableCell cBread = new TableCell();
//				cBread.ColumnSpan = 2;
//				rBread.Height = Unit.Pixel(30);
//
//				cBread.Controls.Add(ltlBread);
//				rBread.Controls.Add(cBread);
//				tblTerms.Controls.Add(rBread);


				// Add breadcrumb
				Literal ltlBread = new Literal();
				string strTermSection = "";
				foreach(DataRow aRow in rows)
				{
					if(strTermSection != aRow["termSection"].ToString())
					{
						Debug.WriteLine("Section: " + aRow["termSection"].ToString());
						strTermSection = aRow["termSection"].ToString();
						TableRow rBread = new TableRow();
						TableCell cBread = new TableCell();
						cBread.ColumnSpan = 2;
					//	rBread.Height = Unit.Pixel(30);
						ltlBread.Text = "<img src=\"images/icn_orange_arrow.jpg\" alt=\"\" border=\"0\" />&nbsp;<a href=#" + strTermSection.Replace(" ", "") + " Class=\"abbrevLinks\">" + strTermSection+ "</a>";
//						cBread.Controls.Add(ltlBread);
						cBread.Text = "<img src=\"images/icn_orange_arrow.jpg\" alt=\"\" border=\"0\" />&nbsp;<a href=#" + strTermSection.Replace(" ", "") + " Class=\"abbrevLinks\">" + strTermSection+ "</a>";
						rBread.Controls.Add(cBread);
						tblTerms.Controls.Add(rBread);
					}
				}
			}


			foreach(DataRow aRow in rows)
			{
				if(strSection != aRow["termSection"].ToString())
				{
					strSection = aRow["termSection"].ToString();

					Literal ltlSection = new Literal();
					ltlSection.Text = "<a name=" + strSection.Replace(" ", "");
					ltlSection.Text += " style=\"FONT-SIZE: 11pt; COLOR: #666666; FONT-WEIGHT: bold; FONT-FAMILY: Arial; TEXT-DECORATION: underline\">";
					ltlSection.Text += strSection + "</a>";

					TableRow rSection = new TableRow();
					TableCell c1Section = new TableCell();
//					Label lblSection = new Label();
//					lblSection.Text = strSection;
//					lblSection.Font.Size = FontUnit.Point(10);
//					lblSection.Font.Bold = true;
					c1Section.Width = Unit.Percentage(5.0);
					c1Section.Wrap = false;
					c1Section.ColumnSpan = 2;

					c1Section.Controls.Add(ltlSection);
					rSection.Controls.Add(c1Section);
					tblTerms.Controls.Add(rSection);

				}

				TableRow rTerm = new TableRow();
				TableCell c1 = new TableCell();
				TableCell c2 = new TableCell();

//				System.Web.UI.WebControls.Image img =  new System.Web.UI.WebControls.Image();
//				img.ImageUrl = "images/icn_orange_arrow.jpg";
//				img.BorderStyle = BorderStyle.None;

				Button btn = new Button();
				if(aRow["termAbbreviation"].ToString() != "")
					btn.Text = aRow["term"].ToString() + "(" + aRow["termAbbreviation"].ToString() + ")";
				else
					btn.Text = aRow["term"].ToString();

				btn.CommandArgument = aRow["termID"].ToString();
				btn.Click += new System.EventHandler(this.LinkButton1_Click);
				btn.CssClass = "abbrevButtons"; 
				btn.Attributes.Add("onMouseover", "javascript:underlineIt(this);");
				btn.Attributes.Add("onMouseout", "javascript:underlineOff(this);");

				Literal ltl = new Literal();
				ltl.Text = "&nbsp;";

				c1.Width = Unit.Percentage(5.0);
				c1.Wrap = false;
				c2.Width = Unit.Percentage(95.0);

				c1.Controls.Add(ltl);
//				c2.Controls.Add(img);
//				c2.Controls.Add(ltl);
				c2.Controls.Add(btn);
				rTerm.Controls.Add(c1);
				rTerm.Controls.Add(c2);
				tblTerms.Controls.Add(rTerm);

				TableRow rLit = new TableRow();
				TableCell cellLit1 = new TableCell();
				TableCell cellLit2 = new TableCell();

				Literal ltlDefinition = new Literal();
				ltlDefinition.Text = aRow["termDefinition"].ToString();
				
				ltlDefinition.Visible = Convert.ToBoolean(aRow["termID"].ToString() == myUtility.TermID.ToString());

				cellLit2.Controls.Add(ltlDefinition);
				rLit.Controls.Add(cellLit1);
				rLit.Controls.Add(cellLit2);
				tblTerms.Controls.Add(rLit);

				TableRow rSpace = new TableRow();
				TableCell cellSpace = new TableCell();
				cellSpace.ColumnSpan = 2;

				Literal ltlSpace = new Literal();
				ltlSpace.Text = "&nbsp;";

				ltlSpace.Visible = Convert.ToBoolean(aRow["termID"].ToString() == myUtility.TermID.ToString());


					cellSpace.Controls.Add(ltlSpace);
					rSpace.Controls.Add(cellSpace);
					tblTerms.Controls.Add(rSpace);

			} // END foreach

			string strFocusCtl = "";
			int cntFocus = 0;

			for(int x=0; x < tblTerms.Controls.Count; x++) // 1st genertaion
			{
				Control c1 = tblTerms.Controls[x];
				for(int y=0; y < c1.Controls.Count; y++) // 2st genertaion
				{
					Control c2 = c1.Controls[y];
					for(int z=0; z < c2.Controls.Count; z++) // 3rd genertaion
					{
						Control c3 = c2.Controls[z];
						if(c3.GetType().ToString() == "System.Web.UI.WebControls.Button")
						{
							Button butt = c3 as Button;
							if(strFocusCtl != "" && cntFocus<10)
							{
								cntFocus++;
								strFocusCtl = butt.ClientID;
							}

							if(Int32.Parse(butt.CommandArgument) == myUtility.TermID)
							{
								Debug.WriteLine("FOUND IT; ID=" + butt.ID + "; ClientID="+butt.ClientID);
								strFocusCtl = butt.ClientID;
							}
						} // END if Type Button
					}//END Loop 3rd genertaion
				}//END Loop 2nd genertaion
			}//END Loop 1st genertaion

			if(strFocusCtl != "")
			{
				if(cntFocus < 5)
					this.SetFocus("termsBottom");
				else
					this.SetFocus(strFocusCtl);
			}

			myUtility.TermID = 0;
			Session["Utility"] = myUtility;


		} // END DisplayList

		private void SetFocus(string strCtl)
		{
			StringBuilder sb = new StringBuilder();
 
			sb.Append("\r\n<script language='JavaScript'>\r\n");
			sb.Append("<!--\r\n"); 
			sb.Append("function SetFocus()\r\n"); 
			sb.Append("{\r\n"); 
//			sb.Append("\tdocument.Form.");
			sb.Append("\tdocument.Form1.");
			sb.Append(strCtl);
			sb.Append(".focus();\r\n"); 
			sb.Append("}\r\n"); 
			sb.Append("window.onload = SetFocus;\r\n"); 
			sb.Append("// -->\r\n"); 
			sb.Append("</script>");
 
			Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
		}





		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.termsBottom.Click += new System.EventHandler(this.termsBottom_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LinkButton1_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("LinkButton1_Click controls = " + tblTerms.Controls.Count.ToString());
			Button btn = sender as Button;
			Debug.WriteLine("Sender value = " + btn.CommandArgument + "; Sender name = " + btn.Text);
			myUtility.TermID = Int32.Parse(btn.CommandArgument);
			Session["Utility"] = myUtility;

			Response.Redirect("Abbreviations.aspx");
//			this.DisplayList();

		}

		private void termsBottom_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Abbreviations.aspx");
		}
	
	} // class
}
