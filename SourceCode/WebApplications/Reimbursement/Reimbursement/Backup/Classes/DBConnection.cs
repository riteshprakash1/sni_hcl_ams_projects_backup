using System;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Reimbursement.Classes
{
	/// <summary>
	/// Summary description for DBConnection.
	/// </summary>
	public class DBConnection
	{
		protected System.Data.SqlClient.SqlConnection sqlConnection1;
		protected string strConnString;
		
		public DBConnection()
		{
			try
			{
				System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();
				strConnString = ((string)(configurationAppSettings.GetValue("sqlConn", typeof(string))));
			}
			catch(Exception ex)
			{
				Debug.WriteLine("ERROR: " + ex.Message);
			}
			//		strConnString ="workstation id=ANDSRV12;packet size=4096;user id=sqlprog;data source=andsrv37;persist security info=True;initial catalog=Crystal;password=sqlprog";
			//		strConnString = "packet size=4096;user id=sqlprog;data source=andsrv37;pers" +
			//			"ist security info=True;initial catalog=Crystal;password=sqlprog";
		}


		public string getConnString()
		{
			return strConnString;
		}

		public void setConnString(string connString)
		{
			strConnString = connString;
		}

		public SqlConnection getConnection()
		{
			sqlConnection1 = new System.Data.SqlClient.SqlConnection();
			sqlConnection1.ConnectionString = strConnString;
			return sqlConnection1;
		}

	}	// END Class
}		// END Namespace
