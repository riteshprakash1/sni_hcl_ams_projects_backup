<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="HospitalAscCalculator.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.HospitalAscCalculator" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent"><uc1:titlenavigation id="TitleNavigation2" runat="server"></uc1:titlenavigation>
			<TABLE cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD width="100%" height="25"></TD>
				</TR>
				<TR>
					<TD width="100%">
						<table align="center" border="0">
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td width="8%"><b>Step 1</b></td>
								<td width="20%"><b>State:</b></td>
								<td noWrap width="50%"><asp:dropdownlist id="ddlState" runat="server" AutoPostBack="true" Width="250px"></asp:dropdownlist><asp:comparevalidator id="vldState" runat="server" Operator="NotEqual" ValueToCompare="0" ErrorMessage="You must select a state."
										ControlToValidate="ddlState">*</asp:comparevalidator></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><b>Step 2</b></td>
								<td><b>City/County:</b></td>
								<td noWrap><asp:dropdownlist id="ddlCity" runat="server" Width="250px"></asp:dropdownlist><asp:comparevalidator id="vldCity" runat="server" Operator="NotEqual" ValueToCompare="0" ErrorMessage="You must select a City/ County."
										ControlToValidate="ddlCity">*</asp:comparevalidator></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" colSpan="3"><asp:button id="btnCalculate" runat="server" Width="70px" Font-Size="10pt" CausesValidation="False"
										BackColor="#79BDE9" ForeColor="White" Text="Calculate" Height="21" Font-Names="Arial ,Helvetica,sans-serif" BorderWidth="0px"></asp:button>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:button id="btnReset" runat="server" Width="70px" Font-Size="10pt" CausesValidation="False"
										BackColor="#A6C535" ForeColor="White" Text="Reset" Height="21" Font-Names="Arial ,Helvetica,sans-serif"
										BorderWidth="0px"></asp:button></td>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr vAlign="bottom" height="30">
								<td colSpan="4"><asp:label id="LabelRates" runat="server" Font-Bold="True"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="4"><asp:datagrid id="dgCPT" runat="server" Width="740" CellSpacing="0" CellPadding="5" ShowHeader="True"
										AutoGenerateColumns="False" ShowFooter="False">
										<HeaderStyle BackColor="#cccccc" Font-Bold="True"></HeaderStyle>
										<ItemStyle VerticalAlign="Top" Wrap="False"></ItemStyle>
										<Columns>
											<asp:BoundColumn DataField="CPT_HCPCS" HeaderStyle-Width="10%" ReadOnly="True" HeaderText="CPT Code"></asp:BoundColumn>
											<asp:BoundColumn DataField="CPTDescription" HeaderStyle-Width="30%" ReadOnly="True" HeaderText="CPT Description"></asp:BoundColumn>
											<asp:TemplateColumn headertext="Rural ASC" HeaderStyle-Width="15%">
												<ItemTemplate>
													<asp:Label id="lblRuralASC" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "AscRuralPymt")) %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Urban ASC" HeaderStyle-Width="15%">
												<ItemTemplate>
													<asp:Label id="lblUrbanASC" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "AscUrbanPymt")) %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Rural Hospital" HeaderStyle-Width="15%">
												<ItemTemplate>
													<asp:Label id="lblRuralHospital" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "ApcRuralPymt")) %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Urban Hospital" HeaderStyle-Width="15%">
												<ItemTemplate>
													<asp:Label id="lblUrbanHospital" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "ApcUrbanPymt")) %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td class="smallDisclaimer" colSpan="4"><BR>
									<BR>
									<FONT size="1"><STRONG>Note:</STRONG> &nbsp; Calculations are based on CMS formulas 
										factoring in Wage Index, GPCI&nbsp;and RVUs. CMS may make adjustments to any or 
										all of the data inputs from time to time. All CPT codes are copyrights of the 
										American Medical Association.<BR>
										<BR>
									</FONT>
									<asp:label id="lblDisclaimer" runat="server" CssClass="smallDisclaimer">
										<FONT size="1"><B>Disclaimer</B><BR>
											The information provided on this website is general reimbursement information, 
											provided for the providers' reference and convenience.&nbsp;&nbsp;This 
											information is not legal advice, nor is it advice about how to code, complete 
											or submit any particular claim for payment.&nbsp;&nbsp;Although we supply this 
											information based on our current knowledge, it is always the provider's 
											responsibility to determine and submit appropriate codes, charges, modifiers, 
											and bills for the services that were rendered.&nbsp;&nbsp;The coding and 
											reimbursement information is subject to change without 
											notice.&nbsp;&nbsp;Payers or their local branches may have their own coding and 
											reimbursement requirements and policies.&nbsp;&nbsp;Before filing any claims, 
											providers should verify current requirements and policies to the payer.<BR>
										</FONT>
									</asp:label></td>
							</tr>
							<tr>
								<td colSpan="3"><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD width="100%" height="25"></TD>
				</TR>
			</TABLE>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
