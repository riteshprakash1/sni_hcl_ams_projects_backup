<%@ Page language="c#" Codebehind="WhatsNew.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.WhatsNew" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table width="100%">
			<tr>
				<td>
					<asp:datagrid id="dgWhatsNew" width="540" runat="server" ShowHeader="false" PagerStyle-Visible="False"
						ShowFooter="False" AutoGenerateColumns="False" CellPadding="5" CellSpacing="0" DataKeyField="whatsNewID"
						BorderWidth="0" BorderStyle="None" OnEditCommand="btnEdit_Click">
						<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
						<Columns>
							<asp:TemplateColumn ItemStyle-Width="90%">
								<ItemTemplate>
														<img src="images/icn_orange_arrow.jpg" alt="" border="0" />&nbsp;
<asp:LinkButton Runat="server" CssClass="newstitle" Text='<%# DataBinder.Eval(Container.DataItem, "whatsNewHeading").ToString() %>' CommandName="Edit" Font-Size="10pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" ID="Linkbutton1">
									</asp:LinkButton>
														<br>
														<asp:Literal ID="ltlSummary" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "whatsNewSummary").ToString() %>'>
									</asp:Literal>
													</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:datagrid>
				</td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
