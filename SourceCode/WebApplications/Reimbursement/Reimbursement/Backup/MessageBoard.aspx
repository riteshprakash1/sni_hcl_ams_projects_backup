<%@ Page language="c#" Codebehind="MessageBoard.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.MessageBoard" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="Header1_pageTitle">Message Board</title>
		<META content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
		<meta content="Reimbursement, Managed Care, Fee Schedules, Medicare, CMS, Physician Coding, &#13;&#10;&#9;&#9;Hospital Coding, ASC Coding, APC, Payor, Payer, Resource Center, Policies, Published Literature, Modifiers, CPT, HCPCS, &#13;&#10;&#9;&#9;C-Codes, ICD9, Diagnosis, Calculations, Reference Guides, Sample Letters, RVU, Non-Facility, Private Insurers, IDET, &#13;&#10;&#9;&#9;Discography, TriVex, TCI, Arthroscopy, Insurance Coverage, &#13;&#10;&#9;&#9;Stimulation, SpineCath, Intradiscal, CDS, Controlled Disc, RF, Pulsed Radiofrequency, Denervation Lumbar,&#13;&#10;&#9;&#9;Cervical, Transilluminated, TCI, Knee, Shoulder, Hip, Small Joint, Operative Hysteroscopy, Billing, Medical Device, Smith &amp; Nephew,&#13;&#10;&#9;&#9;C1754, C1713, 58558, 58561, 0062T, 0063T, 64999, 64622, 64623, &#13;&#10;&#9;&#9;64626, 64627, 72285, 72295, 62290, 62291, 37765, 37766, 37785, 37799, 37700, 37718, 37722, 37735, S2300, 29805, 29806, &#13;&#10;&#9;&#9;29807, 29819, 29820, 29821, 29822, 29823, 29824, 29825, 29826, 29827, 29860, 29861, 29862, 29863, 29866, 29867, 29868, &#13;&#10;&#9;&#9;29870, 29871, 29873, 29874, 29875, 29876, 29877, 29879, 29880, 29881, 29882, 29883, 29884, 29885, 29886, 29887, 29888, &#13;&#10;&#9;&#9;29889, G0289, 29830, 29834, 29835, 29836, 29837&#13;&#10;&#9;&#9;&#9;29838, 29840, 29843, 29844, 29845, 29846, 29847, &#13;&#10;&#9;&#9;29848, 29891, 29892, 29893, 29894, 29895, 29897, 29898, 29899, 29900, 29901, 29902, 29999"
			name="keywords">
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/global.css" type="text/css" rel="stylesheet">
		<LINK href="css/styles.css" type="text/css" rel="stylesheet">
		<LINK href="css/StyleNew.css" type="text/css" rel="stylesheet">
		<LINK href="css/print.css" type="text/css" rel="stylesheet" media="print">
		<LINK href="css/ScreenStyle.css" type="text/css" rel="stylesheet" media="screen">
		<script src="scripts/showhint.js" type="text/javascript"></script>
		<script src="scripts/JavaScr.js" type="text/javascript"></script>
		<script src="scripts/navigation.js" type="text/javascript"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<div class="d1_details" id="shell">
			<div id="region-body-and-footer">
				<div id="masthead">
					<div class="floatLeft width330">
						<a href="http://www.smith-nephew.com/us/professional/" border="0"><img src="images/logo.gif" alt="Smith &amp; Nephew" border="0" class="floatLeft marginTop10"></a>
						<div class="clear paddingTop10 strapline"><a style=" FONT-SIZE:1em;FONT-WEIGHT:normal;COLOR:#85898b" href="http://www.smith-nephew.com/us/professional/resources/reimbursement/">US 
								Professional</a></div>
					</div>
				</div>
				<div id="horizontalNav"></div>
			</div>
			<table cellSpacing="0" cellPadding="0" width="850" border="0" align="center">
				<tr>
					<td style="FONT-SIZE: 12pt;COLOR: #f19205"><br>
						<br>
						The Reimbursement Web Application is down for maintenance. It should be 
						available again in a few hours.
						<br>
						We apologize for any inconvience.
					</td>
				</tr>
			</table>
		</div>
	</body>
</HTML>
