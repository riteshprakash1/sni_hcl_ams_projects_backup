using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for Unsubscribe.
	/// </summary>
	public class Unsubscribe : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnUnsubscribe;
		protected System.Web.UI.WebControls.HyperLink lnkEmail;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label lblMsg;
		protected Utility myUtility;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			lblMsg.Text = "";

			if(!Page.IsPostBack)
			{  
				EnterButton.TieButton(this.txtEmail, btnUnsubscribe);
			}
		} // end Page_Load

		private void btnUnsubscribe_Click(object sender, System.EventArgs e)
		{
			
			Page.Validate();


			if(Page.IsValid)
			{
				int rowsUpdated = 0;
				
				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "sp_UpdateUserUnsubscribe";
				cmd.Connection = myUtility.SqlConn;
				cmd.Parameters.Add(new SqlParameter("@EmailAddress", txtEmail.Text.ToLower()));

				try
				{   
					if(myUtility.SqlConn.State != ConnectionState.Open)
						myUtility.SqlConn.Open();

					rowsUpdated = cmd.ExecuteNonQuery();
					myUtility.SqlConn.Close();
				}
				catch(Exception exc)
				{
					Debug.WriteLine(cmd.CommandText + " Error - " + exc.Message);
					myUtility.SqlConn.Close();
				}

				if(rowsUpdated == 0)
					lblMsg.Text="The email address was not found.  Unsubscribe failed.";
				else
					lblMsg.Text="You have successfully unsubscribed from the emails sent by the Smith & Nephew Reimbursement Web Site.";

				Session["Utility"]=myUtility;
			} // end  Page.IsValid

		} // end btnUnsubscribe_Click



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnUnsubscribe.Click += new System.EventHandler(this.btnUnsubscribe_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


	} // class
}
