using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for Crosswalk.
	/// </summary>
	public class Crosswalk : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgCrosswalk;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.Literal ltlCrosswalk;
		protected System.Web.UI.WebControls.Table tblCrosswalkHeader;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.WebControls.Label lblSortOrder;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.Literal Literal1;
		protected System.Web.UI.WebControls.DropDownList ddlPart;
		protected System.Web.UI.WebControls.TextBox txtCatalog;
		protected System.Web.UI.WebControls.TextBox txtDescription;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DropDownList ddlCPT;
		protected System.Web.UI.WebControls.Label lblNoRecords;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("In  Page_Load");
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				EnterButton.TieButton(this.txtDescription, this.btnSearch);
				EnterButton.TieButton(this.txtCatalog, this.btnSearch);

				this.LoadDropDowns();
				this.btnSearch_Click(sender, e);
			}
		} // end page_load

		private void LoadDropDowns()
		{
			DataTable dtParts = myUtility.SetTable("sp_GetCrosswalkParts");
			DataTable dtCPT = myUtility.SetTable("sp_GetCrosswalkCPT");
			
			this.ddlPart.Items.Clear();
			ddlPart.Items.Add(new ListItem("-- Select --", "%"));
			foreach (DataRow r in dtParts.Rows)
			{
				this.ddlPart.Items.Add(new ListItem(r["Part"].ToString(), r["Part"].ToString()));
			}

			this.ddlCPT.Items.Clear();
			ddlCPT.Items.Add(new ListItem("-- Select --", "%"));
			foreach (DataRow r in dtCPT.Rows)
			{
				this.ddlCPT.Items.Add(new ListItem(r["CPT_HCPCS"].ToString(), r["CPT_HCPCS"].ToString()));
			}
		}

		public void BindCrosswalk(DataTable dt)
		{
			bool hasData = Convert.ToBoolean(dt.Rows.Count > 0);
			
			dgCrosswalk.DataSource = dt;
			dgCrosswalk.DataBind();

			this.dgCrosswalk.Visible = hasData;
			this.btnExport.Visible = hasData;
			this.lblNoRecords.Visible = !hasData;
		} // END BindCrosswalk

		protected void sortHandler(object sender, DataGridSortCommandEventArgs e)
		{
			this.lblSortOrder.Text = e.SortExpression;
			DataTable dt = this.SortTable(this.lblSortOrder.Text);
				
			this.BindCrosswalk(dt);

		} // END sortHandler

		protected DataTable SortTable(string strSortBy)
		{
			DataTable dtCrosswalk = GetSearchTable();

			DataRow[] rows = dtCrosswalk.Select(null, strSortBy);

			DataTable dt = dtCrosswalk.Clone();
			foreach (DataRow row in rows) 
			{
				dt.ImportRow(row);
			}
				
			return dt;

		} // END sortTable


		public DataTable GetSearchTable()
		{
			if(Session["dtCrosswalk"]!=null)
			{
				return (DataTable)Session["dtCrosswalk"];
			}
			else
			{
			
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				SqlCommand cmdSetTable = new SqlCommand();
				cmdSetTable.CommandText = "sp_GetCrosswalkSearch";
				cmdSetTable.CommandType = CommandType.StoredProcedure;
				cmdSetTable.Connection = myUtility.SqlConn;
				cmdSetTable.Parameters.Add(new SqlParameter("@Part", this.ddlPart.SelectedValue));
				cmdSetTable.Parameters.Add(new SqlParameter("@CatalogNum", "%"+this.txtCatalog.Text+"%"));
				cmdSetTable.Parameters.Add(new SqlParameter("@ProductDescription", "%"+this.txtDescription.Text+"%"));
				cmdSetTable.Parameters.Add(new SqlParameter("@CPT_HCPCS", this.ddlCPT.SelectedValue));

				DataTable myDT = new DataTable();
				Debug.WriteLine("myDT rows: " + myDT.Rows.Count.ToString());

				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);

				myUtility.SqlConn.Close();

				Session["dtCrosswalk"] = myDT;
	
				return myDT;
			}
		} // END SetTable()

		protected void Page_Changed(object sender, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			DataTable dtCrosswalk = GetSearchTable();

			this.dgCrosswalk.CurrentPageIndex = e.NewPageIndex;	
			try
			{
				this.BindCrosswalk(dtCrosswalk);
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Page_Changed Error: " + ex.Message);
			}
		}

		public void btnExport_Click(object sender, EventArgs e)
		{		
			DataTable dtCrosswalk = GetSearchTable();
			this.dgCrosswalk.AllowPaging=false;
			this.BindCrosswalk(dtCrosswalk);
			this.ExportGrid();

			
//			Response.Clear();
//			Response.Buffer= true;
//			Response.ContentType = "application/vnd.ms-excel";
//			Response.Charset = "";
//	
//			StringBuilder SB = new StringBuilder();    
//			StringWriter SW = new StringWriter(SB);
//			HtmlTextWriter htmlTW = new HtmlTextWriter(SW);
//			
//			DataTable dtCrosswalk = GetSearchTable();
//			int tempPageIndex = this.dgCrosswalk.CurrentPageIndex;
//			this.dgCrosswalk.CurrentPageIndex = 0;
//			this.dgCrosswalk.AllowPaging = false;
//			this.BindCrosswalk(dtCrosswalk);
//			Debug.WriteLine("dgCrosswalk All count: " +  this.dgCrosswalk.Items.Count.ToString());
//			
//			this.dgCrosswalk.RenderControl(htmlTW);
//
//			this.dgCrosswalk.CurrentPageIndex = tempPageIndex;
//			this.dgCrosswalk.AllowPaging = true;
//			this.BindCrosswalk(dtCrosswalk);
//			Debug.WriteLine("dgCrosswalk After count: " +  this.dgCrosswalk.Items.Count.ToString());
//		
//			Response.Write(SW.ToString());	
//			Response.End();
		}

		private void ExportGrid()
		{
			DataTable dtCrosswalk = GetSearchTable();
			
			//turn off paging
			this.dgCrosswalk.AllowPaging=false;
			this.BindCrosswalk(dtCrosswalk);

			Response.Clear();
			Response.Buffer = true;
			Response.Charset = "";
			Response.AddHeader("content-disposition", "attachment; filename=ReimbursementReport.xls");
			Response.ContentType = "application/vnd.ms-excel";
			StringWriter sw = new System.IO.StringWriter();
			HtmlTextWriter htw = new HtmlTextWriter(sw);
	//		DataTable dt = (DataTable)DataGrid1.DataSource; 
			DataTable dt = (DataTable)this.dgCrosswalk.DataSource; 
			
			// here, we loop through each row and then each cell and check the data type.  We then add the appropriate class              	 	 	
			foreach (DataGridItem itm in this.dgCrosswalk.Items)              	 	
			{                              		 		
				for (int i = 0; i < itm.Cells.Count; i++)                 		 		
				{                      			 			
					TableCell tc = itm.Cells[i];                      			 			
					switch (dt.Columns[i].DataType.ToString().ToLower())                     			 			
					{                          				 				
						case "system.string":                              					 					
							tc.Attributes.Add("class", "text");                              					 					
							break;       				 				
						case "system.datetime":                              					 					
							tc.Attributes.Add("class", "dt");                             					 					
							break;                           				 				
						case "system.decimal":                              					 					
							tc.Attributes.Add("class", "num");                              					 					
							break;                          				 				
						default:                              					 					
							break;                     			 			
					}                 		 		
				}            	 	
			}              	
			
			this.dgCrosswalk.RenderControl(htw);              	 	
			StringBuilder sb = new StringBuilder();             	 	
			sb.Append(@"<style> ");              	 	
			sb.Append(@" .text { mso-number-format:\@; } ");              	 	
			sb.Append(@" .num { mso-number-format:0\.00; } ");              	 	
			sb.Append(@" .dt { mso-number-format: �Short Date�; } ");              	 	
			sb.Append(@"</style>");                 	
			Response.Write(sb.ToString());  
			// write the styles to the Response           	 	
			Response.Write(sw.ToString());             	 	
			Response.Flush();              	 	
			Response.End(); 
         

		}


		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			Session["dtCrosswalk"] = null;
			DataTable dtCrosswalk = GetSearchTable();
			this.lblSortOrder.Text = "CatalogNum";

			this.dgCrosswalk.CurrentPageIndex = 0;	
			this.BindCrosswalk(dtCrosswalk);
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
