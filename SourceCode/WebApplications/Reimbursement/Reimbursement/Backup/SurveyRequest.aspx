<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Page language="c#" Codebehind="SurveyRequest.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.SurveyRequest" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table width="100%" style="BORDER-BOTTOM:#cccccc 1px dotted; BORDER-LEFT:#cccccc 1px dotted; PADDING-BOTTOM:5px; PADDING-LEFT:5px; PADDING-RIGHT:5px; BORDER-TOP:#cccccc 1px dotted; BORDER-RIGHT:#cccccc 1px dotted; PADDING-TOP:5px">
			<tr>
				<td>
					<asp:Label id="lblSurveyMessage" runat="server"></asp:Label>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<asp:Label id="lblProductID" runat="server" Visible="False"></asp:Label>
					<asp:Label id="lblSurveyURL" runat="server" Visible="False"></asp:Label>
					<asp:button style="Z-INDEX: 0" id="btnTakeSurvey" runat="server" Text="Take Survey" Font-Size="10pt"
						Height="22" BackColor="#79BDE9" CausesValidation="False" Font-Names="Arial ,Helvetica,sans-serif"
						BorderWidth="0px" Width="120px" ForeColor="White"></asp:button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:button style="Z-INDEX: 0" id="btnDeclineSurvey" runat="server" Text="Decline Survey" Font-Size="10pt"
						Height="22" BackColor="#79BDE9" CausesValidation="False" Font-Names="Arial ,Helvetica,sans-serif"
						BorderWidth="0px" Width="120px" ForeColor="White"></asp:button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:button style="Z-INDEX: 0" id="btnAskLater" runat="server" Text="Ask Me Later" Font-Size="10pt"
						Height="22" BackColor="#79BDE9" CausesValidation="False" Font-Names="Arial ,Helvetica,sans-serif"
						BorderWidth="0px" Width="120px" ForeColor="White"></asp:button>&nbsp;
					<asp:button style="Z-INDEX: 0" id="btnBack" runat="server" ForeColor="White" Width="240px" BorderWidth="0px"
						Font-Names="Arial ,Helvetica,sans-serif" CausesValidation="False" BackColor="#79BDE9" Height="22"
						Font-Size="10pt" Text="Return to Reimbursement Site"></asp:button>
				</td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
