<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="PhysicianCodePay.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.PhysicianCodePay" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent">
			<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
			<asp:literal id="ltlCoding" runat="server"></asp:literal>
			<asp:datagrid id="dgCPT" runat="server" ShowFooter="False" AutoGenerateColumns="False" ShowHeader="True"
				CellPadding="5" CellSpacing="0">
				<HeaderStyle BackColor="#cccccc" Font-Bold="True"></HeaderStyle>
				<ItemStyle VerticalAlign="Top" Wrap="False"></ItemStyle>
				<Columns>
					<asp:BoundColumn DataField="CPT_HCPCS" HeaderStyle-Width="15%" ReadOnly="True" HeaderText="CPT Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="Descr" HeaderStyle-Width="85%" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			<asp:literal id="ltlMedicare" runat="server"></asp:literal>
			<asp:Table Width="100%" CellPadding="0" CellSpacing="0" BackColor="#cccccc" BorderColor="Gray"
				BorderStyle="Solid" BorderWidth="1" Runat="server" ID="tblMedicareTblHeader" Font-Names="Arial"
				Font-Size="11pt">
				<asp:TableRow BackColor="#CCCCCC" Height="30px">
					<asp:TableHeaderCell BackColor="#CCCCCC" HorizontalAlign="Center" Text="National Medicare Physician Fee Schedule"
						CssClass="spine" ID="cellFeeSchedule"></asp:TableHeaderCell>
				</asp:TableRow>
			</asp:Table>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<asp:datagrid id="dgMedicare" runat="server" ShowFooter="False" AutoGenerateColumns="False" ShowHeader="True"
							CellPadding="5" CellSpacing="0">
							<HeaderStyle BackColor="#cccccc" HorizontalAlign="Center" Font-Bold="True"></HeaderStyle>
							<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" Wrap="False"></ItemStyle>
							<Columns>
								<asp:BoundColumn DataField="CptMod" HeaderStyle-Width="12%" ReadOnly="True" HeaderText="CPT Code"></asp:BoundColumn>
								<asp:BoundColumn DataField="Descr" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
									HeaderStyle-Width="58%" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
								<asp:TemplateColumn headertext="Non-Facility" HeaderStyle-Width="16%">
									<ItemTemplate>
										<asp:Label id="lblNonPayment" runat="server" Text='<%#GetPhysicianPayment(String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "NonFacPayment")))%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn headertext="Facility" HeaderStyle-Width="14%">
									<ItemTemplate>
										<asp:Label id="lblFacPayment" runat="server" Text='<%#GetPhysicianPayment(String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "FacPayment")))%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
			<asp:literal id="ltlAdditional" runat="server"></asp:literal><br>
			<br>
			<asp:literal id="ltlPrivate" runat="server"></asp:literal>
			<asp:datagrid id="dgICD" runat="server" ShowFooter="False" AutoGenerateColumns="False" ShowHeader="True"
				CellPadding="5" CellSpacing="0">
				<HeaderStyle BackColor="#cccccc" Font-Bold="True" VerticalAlign="Bottom"></HeaderStyle>
				<ItemStyle VerticalAlign="Top"></ItemStyle>
				<Columns>
					<asp:BoundColumn DataField="icdCode" HeaderStyle-Width="10%" ReadOnly="True" ItemStyle-Wrap="False"
						HeaderText="ICD-10"></asp:BoundColumn>
					<asp:BoundColumn DataField="Descr" HeaderStyle-Width="50%" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
					<asp:TemplateColumn ItemStyle-Wrap="True" HeaderText="CPT Cross Reference" HeaderStyle-Width="40%">
						<ItemTemplate>
							<asp:Label id="lblCPTCross" Text='<%# GetCrossReference(DataBinder.Eval(Container.DataItem, "productIcdCodeID").ToString()) %>' runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
