<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="Links.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.Links" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<P>To be directed to these websites, please click on the hyperlink below.<BR>
			<BR>
		</P>
		<asp:Table id="tblLinks" runat="server"></asp:Table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
