using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for PhysicianCodePay.
	/// </summary>
	public class PhysicianCodePay : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgCPT;
		protected System.Web.UI.WebControls.Literal ltlMedicare;
		protected System.Web.UI.WebControls.DataGrid dgMedicare;
		protected System.Web.UI.WebControls.Literal ltlPrivate;
		protected System.Web.UI.WebControls.DataGrid dgICD;
		protected System.Web.UI.WebControls.Literal ltlAdditional;
		protected System.Web.UI.WebControls.Literal ltlCoding;
		protected DataTable dtCPT;
		protected DataTable dtICD;
		protected DataTable dtMedicare;
	
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Table tblMedicareTblHeader;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("In  Page_Load");
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			tblMedicareTblHeader.Rows[0].Cells[0].Text = myUtility.RateYear + " National Medicare Physician Fee Schedule";

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				myUtility.ProductsTable = myUtility.SetTable("sp_GetProduct", myUtility.ProductID);
				dtCPT = myUtility.SetTable("sp_GetPhysicianCPTByProduct", myUtility.ProductID);
				dtMedicare = myUtility.SetTable("sp_GetPhysicianRvuByProduct", myUtility.ProductID);
				myUtility.CptIcdTable = myUtility.SetTable("sp_GetPhysicianCptIcdByProduct", myUtility.ProductID);

				dtICD = myUtility.SetTable("sp_GetPhysicianIcdCodesByProductID", myUtility.ProductID);

				myUtility.SqlConn.Close();

//				if(dtCPT.Rows.Count > 0)
//					this.BindCPT();
//				else
					dgCPT.Visible = false;

				if(dtICD.Rows.Count > 0)
					this.BindICD();
				else
					dgICD.Visible = false;

				AppSettingsReader configAppSet = new AppSettingsReader();
				double dblRvuAdj = ((double)(configAppSet.GetValue("rvuAdjustor", typeof(double))));

				Debug.WriteLine("dblNon = ((dblRvuWork * dblRvuAdj)+(dblNonPE)+(dblFacMP))* dblConvFactor");
				Debug.WriteLine("dblFac = ((dblRvuWork * dblRvuAdj)+(dblFacPE)+(dblFacMP))* dblConvFactor");

				for(int x=0; x<dtMedicare.Rows.Count; x++)
				{
					double dblRvuWork = this.GetAmount(dtMedicare.Rows[x]["RvuWork"].ToString());
					double dblFacMP = this.GetAmount(dtMedicare.Rows[x]["FacMP"].ToString());
					double dblNonPE = this.GetAmount(dtMedicare.Rows[x]["NonPE"].ToString());
					double dblFacPE = this.GetAmount(dtMedicare.Rows[x]["FacPE"].ToString());
					double dblConvFactor = this.GetAmount(dtMedicare.Rows[x]["ConvFactor"].ToString());

//					double dblNon = ((dblRvuWork)+(dblNonPE)+(dblFacMP))* dblConvFactor;
//					double dblFac = ((dblRvuWork)+(dblFacPE)+(dblFacMP))* dblConvFactor;

					double dblNon = ((dblRvuWork * dblRvuAdj)+(dblNonPE)+(dblFacMP))* dblConvFactor;
					double dblFac = ((dblRvuWork * dblRvuAdj)+(dblFacPE)+(dblFacMP))* dblConvFactor;

					dtMedicare.Rows[x]["NonFacPayment"] = dblNon;
					dtMedicare.Rows[x]["FacPayment"] = dblFac;

					Debug.WriteLine("Non CPT: " + dtMedicare.Rows[x]["CPT_HCPCS"].ToString() + " :: " + dblNon.ToString() + " = ((" + dblRvuWork.ToString() + " * " + dblRvuAdj.ToString() + "))+(" + dblNonPE.ToString() + ")+(" + dblFacMP + ")) * " + dblConvFactor.ToString());
					Debug.WriteLine("Fac CPT: " + dtMedicare.Rows[x]["CPT_HCPCS"].ToString() + " :: " + dblNon.ToString() + " = ((" + dblRvuWork.ToString() + " * " + dblRvuAdj.ToString() + "))+(" + dblFacPE.ToString() + ")+(" + dblFacMP + ")) * " + dblConvFactor.ToString());
				}

				if(dtMedicare.Rows.Count > 0)
					this.BindMed();
				else
					dgMedicare.Visible = false;


				if(myUtility.ProductsTable.Rows.Count>0)
				{
					DataRow aRow = myUtility.ProductsTable.Rows[0];

					ltlMedicare.Text = aRow["physicianMedicare"].ToString();
					ltlPrivate.Text = aRow["physicianPrivateIns"].ToString();
					ltlAdditional.Text = aRow["physicianAdditional"].ToString();
					ltlCoding.Text = aRow["physicianCoding"].ToString();
				
				}


			} // END isPostBack	
		
		} // Page_Load

		public string GetPhysicianPayment(string strPay)
		{
			string strCleanPay = strPay.Replace("$", "").Replace(",","");
			double pay = Double.Parse(strCleanPay);
			if(pay == 0.00)
				return "* See Below";
			else
				return strPay;
		//	String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "FacPayment"))

		}

		private double GetAmount(string strAmount)
		{
			double dblAmt = 0.00;
			if(strAmount != "")
			{
				try
				{
					dblAmt = Double.Parse(strAmount);
				}
				catch(Exception exc)
				{
					Debug.WriteLine("GetAmount Error: " + exc.Message);
					Debug.WriteLine("Could not Double.Parse: " + strAmount);
				}

			}
			return dblAmt;
		} // END GetAmount

		public string GetCrossReference(string id)
		{
			DataRow[] rows = myUtility.CptIcdTable.Select("productIcdCodeID = " + id);
			string strRef = "";
			string strDel = "";
			foreach(DataRow aRow in rows)
			{
				strRef += strDel + aRow["CPT_HCPCS"].ToString();
				strDel = ", ";
			}

			return strRef;
		}

		public void BindCPT()
		{
			dgCPT.DataSource = dtCPT;
			dgCPT.DataBind();
		} // END BindCPT

		public void BindICD()
		{
			dgICD.DataSource = dtICD;
			dgICD.DataBind();
		} // END BindCPT

		public void BindMed()
		{
			dgMedicare.DataSource = dtMedicare;
			dgMedicare.DataBind();
		} // END BindCPT


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
