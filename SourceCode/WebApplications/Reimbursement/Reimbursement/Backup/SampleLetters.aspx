<%@ Page language="c#" Codebehind="SampleLetters.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.SampleLetters" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent">
			<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
			<table width="100%">
				<tr>
					<td colSpan="3"><asp:datagrid id="dgFiles" runat="server" AllowPaging="false" PagerStyle-Visible="False" AutoGenerateColumns="False"
							CellPadding="5" DataKeyField="DocumentID" OnDeleteCommand="btnView_Click">
							<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
							<HeaderStyle Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Summary" HeaderStyle-Width="40%">
									<ItemTemplate>
										<asp:LinkButton id="Linkbutton1" Height="21" runat="server" Text='<%#GetFileLink(DataBinder.Eval(Container.DataItem, "DocSummary").ToString())%>' CommandName="Delete" Font-Size="10pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC">
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Type">
									<HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Image Runat=server ImageUrl='<%# GetImage(DataBinder.Eval(Container.DataItem, "NameFile").ToString()) %>'>
										</asp:Image>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				<TR>
					<TD colSpan="3"><asp:Label ID="lblNote" Runat="server" Font-Size="8pt" Text='* Note - turn off your browser pop-up blocker to view publications.'></asp:Label>
						<asp:Literal id="Literal2" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<asp:label id="lblmsg" runat="server" Font-Bold="True" Width="392px" ForeColor="#FF8000"></asp:label>
						<asp:Literal id="Literal1" runat="server"></asp:Literal>
					</TD>
				</TR>
			</table>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
