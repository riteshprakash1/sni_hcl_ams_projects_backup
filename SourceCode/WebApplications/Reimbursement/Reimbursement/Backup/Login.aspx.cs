using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Specialized;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for Login.
	/// </summary>
	public class Login : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.Button btnLogin;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label lblPassword;
		protected System.Web.UI.WebControls.TextBox txtPass;
		protected System.Web.UI.WebControls.CheckBox chkRemeberMe;
		protected System.Web.UI.WebControls.CustomValidator vldBadLogin;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.LinkButton btnWhyRegister;
		protected System.Web.UI.WebControls.LinkButton btnForgotPassword;
		protected System.Web.UI.WebControls.Label lblHaveRegister;
		protected System.Web.UI.WebControls.Label lblRegistering;
		protected System.Web.UI.WebControls.HyperLink lnkEmail;
		protected System.Web.UI.WebControls.Button btnEnter;
		protected System.Web.UI.HtmlControls.HtmlForm Form2;
		protected System.Web.UI.WebControls.LinkButton lnkRegister;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.Image imgRegister;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Convert.ToBoolean(ConfigurationSettings.AppSettings["ShowMessageBoard"].ToString()))
				Response.Redirect("MessageBoard.aspx", true);

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			EnterButton.TieButton(this.txtPass, btnLogin);
			EnterButton.TieButton(this.txtEmail, btnLogin);

//			Debug.WriteLine("Title: " + pageTitle.InnerText);
//			myUtility.PageTitle = pageTitle.InnerText;

			AppSettingsReader config = new AppSettingsReader();
			string strEmail = ((string)(config.GetValue("EmailFrom", typeof(string))));
			lnkEmail.Text = strEmail;
			lnkEmail.NavigateUrl = "mailto:"+strEmail;

			System.Web.HttpCookieCollection cookColl = Request.Cookies;
			for(int x=0; x<cookColl.Count; x++)
			{
				HttpCookie c = cookColl[x];
				Debug.WriteLine(x.ToString() + "- " + c.Name);
				Debug.WriteLine(myUtility.LoginCookieName + "- " + c.Name + "; " + Convert.ToString(myUtility.LoginCookieName == c.Name));
			}
			HttpCookie cookie = Request.Cookies[myUtility.LoginCookieName];

			Debug.WriteLine("Cookie is Null: " + Convert.ToString(cookie==null));
			if(cookie != null)
			{
				this.CookieLogin(cookie);
			}
		
		} // END Page_Load

		private void CookieLogin(HttpCookie cook)
		{
			string strUser = "";
			string strPass = "";
			
			try
			{
				strUser = cook.Values[0].TrimStart('=');
				strPass = cook.Values[1].TrimStart('=');
				strUser = CryptoUtil.Decrypt(strUser);
				strPass = CryptoUtil.Decrypt(strPass);
			}
			catch(Exception ex)
			{
				Debug.WriteLine("CookieLogin Error: " + ex.Message);
			}

			Debug.WriteLine("CookieLogin - strUser: " + strUser + "; strPass: " + strPass);
			if(myUtility.SqlConn.State != ConnectionState.Open)
				myUtility.SqlConn.Open();
				
			myUtility.UserProfileTable = myUtility.GetUserInfo(strUser, strPass);

			if(myUtility.UserProfileTable.Rows.Count>0)
				myUtility.LogHit(strUser, "Cookie Success");
			else
				myUtility.LogHit(strUser, "Cookie Fail");
		
			myUtility.SqlConn.Close();

			if(myUtility.UserProfileTable.Rows.Count>0)
				this.SetMyUtility(myUtility.UserProfileTable);

		} // END CookieLogin

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			this.btnWhyRegister.Click += new System.EventHandler(this.btnWhyRegister_Click);
			this.btnForgotPassword.Click += new System.EventHandler(this.btnForgotPassword_Click);
			this.lnkRegister.Click += new System.EventHandler(this.lnkRegister_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion



		private void btnLogin_Click(object sender, System.EventArgs e)
		{
			Page.Validate();

			if(Page.IsValid)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();
				
				myUtility.UserProfileTable = myUtility.GetUserInfo(this.txtEmail.Text, this.txtPass.Text);

				if(myUtility.UserProfileTable.Rows.Count>0)
					myUtility.LogHit(this.txtEmail.Text, "Manual Success");
				else
					myUtility.LogHit(this.txtEmail.Text, "Manual Fail");
		
				myUtility.SqlConn.Close();

				if(myUtility.UserProfileTable.Rows.Count>0)
				{
					if(this.chkRemeberMe.Checked)
					{
						Debug.WriteLine("Is Checked");
						myUtility.SetCookie(Response,this.txtEmail.Text, this.txtPass.Text, 360);
					}
					this.SetMyUtility(myUtility.UserProfileTable);
				}
				else
				{
					System.Web.UI.ValidatorCollection vals = Page.Validators;

					foreach(IValidator val in vals)
					{
						if(val.ErrorMessage.StartsWith("The login and password "))
						{
							val.IsValid = false;
						}
					} // END foreach				
				}
			} // END isValid		
		} // END btnLogin

		protected void SetMyUtility(DataTable dt)
		{
			myUtility.EmailID = Int32.Parse(dt.Rows[0]["emailID"].ToString());
			myUtility.SetIsDraftReader(dt.Rows[0]["emailAddress"].ToString());
			myUtility.SetIsAdmin(dt.Rows[0]["emailAddress"].ToString());
			if(!(myUtility.IsAdmin || myUtility.IsDraftReader))
				myUtility.ExcludeDraftProductCategories();
			Session["Utility"] = myUtility;
			Response.Redirect("Home.aspx");
		}

		private void btnWhyRegister_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("WhyRegister.aspx");
		}

		private void btnForgotPassword_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("Fire btnForgotPassword_Click");
			Session["Utility"] = myUtility;
			Response.Redirect("ForgotPassword.aspx");
		}

		private void lnkRegister_Click(object sender, System.EventArgs e)
		{
			Session["Utility"] = myUtility;
			Response.Redirect("NewRegistration.aspx");
		}

	} // END class
}
