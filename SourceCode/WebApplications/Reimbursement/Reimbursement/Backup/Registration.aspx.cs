using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for New_Subscription.
	/// </summary>

	public class New_Subscription : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected SqlConnection sqlConn;
		protected DataTable dtPerson;
		protected System.Web.UI.WebControls.TextBox txtpass;
		protected System.Web.UI.WebControls.TextBox txtemail;
		protected System.Web.UI.WebControls.Label lblemail;
		protected System.Web.UI.WebControls.Label lblzip;
		protected System.Web.UI.WebControls.DropDownList ddbspe;
		protected System.Web.UI.WebControls.Label lblspe;
		protected System.Web.UI.WebControls.DropDownList ddbocu;
		protected System.Web.UI.WebControls.Label lblocc;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.TextBox txtzip;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator1;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.Label lblpassword;
		protected System.Web.UI.WebControls.CheckBoxList chbupdate;
		protected System.Web.UI.WebControls.CheckBox chkRemeberMe;
		protected System.Web.UI.WebControls.CustomValidator vldExistEmail;
		protected System.Web.UI.WebControls.CustomValidator vldPassWordLen;
		protected System.Web.UI.WebControls.Label lblConfirmEmail;
		protected System.Web.UI.WebControls.TextBox txtConfirmEmail;
		protected System.Web.UI.WebControls.RequiredFieldValidator vldRequiredConfirmEmail;
		protected System.Web.UI.WebControls.CustomValidator Customvalidator1;
		protected System.Web.UI.WebControls.Label lblconfirm;
		protected System.Web.UI.WebControls.TextBox txtconfirm;
		protected System.Web.UI.WebControls.CompareValidator vldOccupation;
		protected System.Web.UI.WebControls.CompareValidator vldSpecialty;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.CheckBox chkSubscribe;
		protected System.Web.UI.WebControls.Label lblOccupationSpecialty;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");
		   
			if(!Page.IsPostBack)
			{  
				EnterButton.TieButton(this.txtemail, btnSubmit);
				EnterButton.TieButton(this.txtpass, btnSubmit);
				EnterButton.TieButton(this.txtconfirm, btnSubmit);
				EnterButton.TieButton(this.txtzip, btnSubmit);

//				this.BindDT();
			
				myUtility.ProductCategoryID = 0;

				ddbocu.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.OccupationTable.Rows)
				{
					ddbocu.Items.Add(new ListItem(aRow["Occupation_Name"].ToString(),aRow["Occupation_ID"].ToString() ));
				}

				ddbspe.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.SpecialtyTable.Rows)
				{
					ddbspe.Items.Add(new ListItem(aRow["Specialty_Name"].ToString(),aRow["Specialty_ID"].ToString() ));
				}

				if(myUtility.EmailID != 0)
				{
					if(myUtility.SqlConn.State != ConnectionState.Open)
						myUtility.SqlConn.Open();

					myUtility.UserProfileTable = myUtility.SetTable("sp_GetUserProfileByEmailID", myUtility.EmailID);
					myUtility.ProductRetriveTable= myUtility.SetTable("sp_GetUserProfileByEmailID",myUtility.EmailID);
					myUtility.SqlConn.Close();

					Session["Utility"] = myUtility;
					
					this.LoadData();

					this.SetOccupationSpecialtyMessage();
				}
				
			} // END isPostBack
		
			this.SetControls();
        
		}  // END Page_Load 

		/// <summary>
		/// Sets a message if the user does not have a selected Occupation or Specialty
		/// Otherwise the message is blank.
		/// </summary>
		protected void SetOccupationSpecialtyMessage()
		{
			if(myUtility.HasOccupationSpecialty() == false)
			{
				this.lblOccupationSpecialty.Text = "Please update your Specialty and/or Occuption information.";
			}
			else
			{
				this.lblOccupationSpecialty.Text = string.Empty;
			}
		}
		
		protected void SetControls()
		{
			if(myUtility.EmailID == 0)
				this.btnSubmit.Text = "Submit";
			else
				this.btnSubmit.Text = "Update";
		} // END SetControls

		protected void LoadData()
		{
			if(myUtility.UserProfileTable.Rows.Count > 0)
			{
				DataRow aRow = myUtility.UserProfileTable.Rows[0];
				if(this.ddbocu.Items.FindByValue(aRow["OccupationID"].ToString()) != null)
					this.ddbocu.Items.FindByValue(aRow["OccupationID"].ToString()).Selected=true;

				if(this.ddbspe.Items.FindByValue(aRow["SpecialtyID"].ToString()) != null)
					this.ddbspe.Items.FindByValue(aRow["SpecialtyID"].ToString()).Selected=true;

				this.txtzip.Text = aRow["ZipCode"].ToString();
				this.txtemail.Text = aRow["EmailAddress"].ToString();
				this.chkSubscribe.Checked = Convert.ToBoolean(aRow["subscribe"].ToString().ToLower());

				HttpCookie cookie = Request.Cookies[myUtility.LoginCookieName];
				this.chkRemeberMe.Checked = Convert.ToBoolean(cookie != null);
			}

		} // END LoadData

		public void clear()
		{
			ddbocu.SelectedValue=null;
			ddbspe.SelectedValue=null;
			this.chkSubscribe.Checked = false;
			txtzip.Text="";
			txtemail.Text="";
			txtpass.Text="";
			txtconfirm.Text="";
		}// END clear
		

		private bool IsValidPassword(string strPass)
		{
			if(strPass.Length < 4 || strPass.Length > 10)
				return false;
			else
				return true;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("Validating");
			Page.Validate();
			
			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("The length of the password"))
				{
					val.IsValid = this.IsValidPassword(txtpass.Text);
				}

				if(val.ErrorMessage.StartsWith("Email Address mismatch"))
				{
					val.IsValid = Convert.ToBoolean(this.txtConfirmEmail.Text==this.txtemail.Text);
				}
			} // END foreach

			if(Page.IsValid)
			{
				bool isGoodEmail = true;
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				if(myUtility.UserProfileTable.Rows.Count > 0)
				{
					string email = myUtility.UserProfileTable.Rows[0]["EmailAddress"].ToString().ToLower();
					Debug.WriteLine("email: " + email + "; New Email: " + this.txtemail.Text.ToLower());
					Debug.WriteLine("Same email: " + Convert.ToBoolean(email==this.txtemail.Text.ToLower()).ToString());
					if(email != this.txtemail.Text.ToLower())
					{
						DataTable dt = myUtility.SetTable("sp_GetUserByEmail",txtemail.Text);
						isGoodEmail = Convert.ToBoolean(dt.Rows.Count == 0);
					}
				}
				else
					isGoodEmail = false;
				
				if(isGoodEmail)
				{
					SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
					cmdInsert.CommandType = CommandType.StoredProcedure;
					cmdInsert.CommandText = "sp_UpdateUserProfile";
					cmdInsert.Connection = myUtility.SqlConn;
					cmdInsert.Parameters.Add(new SqlParameter("@OccupationID",(ddbocu.SelectedValue)));
					cmdInsert.Parameters.Add(new SqlParameter("@SpecialtyID", (ddbspe.SelectedValue)));
					cmdInsert.Parameters.Add(new SqlParameter("@ZipCode", txtzip.Text));
					cmdInsert.Parameters.Add(new SqlParameter("@EmailAddress", txtemail.Text.ToLower()));
					cmdInsert.Parameters.Add(new SqlParameter("@Password", txtpass.Text.ToLower()));
					cmdInsert.Parameters.Add(new SqlParameter("@EmailID", myUtility.EmailID));
					cmdInsert.Parameters.Add(new SqlParameter("@Subscribe", this.chkSubscribe.Checked.ToString()));

					try
					{   
						int rowsUpdated = cmdInsert.ExecuteNonQuery();
					}
					catch(Exception exc)
					{
						Debug.WriteLine(cmdInsert.CommandText + " Error - " + exc.Message);
						myUtility.SqlConn.Close();
					}

					//Delete old items
					SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Connection = myUtility.SqlConn;
					cmd.CommandText = "sp_DeleteUserProduct";
					cmd.Parameters.Add(new SqlParameter("@EmailID",myUtility.EmailID));
				
					try
					{   
						int rowsUpdated = cmd.ExecuteNonQuery();
					}
					catch(Exception exc)
					{
						Debug.WriteLine(cmd.CommandText + " Error - " + exc.Message);
						myUtility.SqlConn.Close();
					}	

					myUtility.UserProfileTable = myUtility.SetTable("sp_GetUserProfileByEmailID", myUtility.EmailID);
					this.SetOccupationSpecialtyMessage();

					lblmsg.Text="You have successfully updated your registration.";

					Session["Utility"]=myUtility;

					if(this.chkRemeberMe.Checked)
						myUtility.SetCookie(Response, this.txtemail.Text, this.txtpass.Text, 360);
					else
						myUtility.SetExpireCookie(Response);
				}
				else
				{
					foreach(IValidator val in Page.Validators)
					{
						if(val.ErrorMessage == "The email address already exists.")
						{
							val.IsValid = false;
						}
					} // END foreach
				}

				myUtility.SqlConn.Close();
      		
			} // END Page.IsValid
		 
		} //END btnSubmit_Click
		
	} // END class
}




		








	

