<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="InPatientCodePay.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.InPatientCodePay" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent">
			<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
			<asp:Literal id="ltlCoding" runat="server"></asp:Literal>
			<asp:Literal id="ltlAdditional" runat="server"></asp:Literal>
			<asp:Literal id="ltlICD" runat="server"></asp:Literal>
			<asp:datagrid id="dgICD" runat="server" CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False"
				ShowFooter="False" Width="100%">
				<HeaderStyle BackColor="#cccccc" Font-Bold="True" VerticalAlign="Bottom"></HeaderStyle>
				<ItemStyle VerticalAlign="Top"></ItemStyle>
				<Columns>
					<asp:BoundColumn DataField="icdCode" HeaderStyle-Width="12%" ReadOnly="True" HeaderText="ICD-10-PCS"></asp:BoundColumn>
					<asp:BoundColumn DataField="Descr" HeaderStyle-Width="35%" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
					<asp:TemplateColumn ItemStyle-Wrap="True" HeaderText="CPT Cross Reference" HeaderStyle-Width="28%">
						<ItemTemplate>
							<asp:Label id="lblCPTCross" Text='<%# GetCrossReference(DataBinder.Eval(Container.DataItem, "productIcdCodeID").ToString()) %>' runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn ItemStyle-Wrap="True" HeaderText="DRG Cross Reference" HeaderStyle-Width="25%">
						<ItemTemplate>
							<asp:Label id="lblDrgCross" Text='<%# GetDRGCrossReference(DataBinder.Eval(Container.DataItem, "IcdCode").ToString()) %>' runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid><br>
			<br>
			<asp:datagrid id="dgDRG" runat="server" CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False"
				ShowFooter="False" Width="100%">
				<HeaderStyle BackColor="#cccccc" Font-Bold="True" VerticalAlign="Bottom"></HeaderStyle>
				<ItemStyle VerticalAlign="Top"></ItemStyle>
				<Columns>
					<asp:BoundColumn DataField="drgCode" HeaderStyle-Width="12%" ReadOnly="True" HeaderText="DRG Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="drgDesc" HeaderStyle-Width="88%" ReadOnly="True" HeaderText="DRG Description"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			<asp:Literal id="ltlPrivate" runat="server"></asp:Literal>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
