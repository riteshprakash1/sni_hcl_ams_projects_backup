using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for ForGotPassword.
	/// </summary>
	public class ForgotPassword : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnSubmit;
	    protected Utility myUtility;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.RequiredFieldValidator vldRequiredEmail;
		protected System.Web.UI.WebControls.Label lblZip;
		protected System.Web.UI.WebControls.TextBox txtZip;
		protected System.Web.UI.WebControls.RequiredFieldValidator vldRequiredZip;
		protected System.Web.UI.WebControls.ValidationSummary vldSummary;
		protected System.Web.UI.WebControls.Label lblMsg;
		protected AppSettingsReader config;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlForm Form2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("In Page Load ForgoPassword");
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			EnterButton.TieButton(this.txtEmail, this.btnSubmit);
			EnterButton.TieButton(this.txtZip, this.btnSubmit);
		} // END Page_Load

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			this.lblMsg.Text = "";
			Page.Validate();

			if(Page.IsValid)
			{
				
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				DataTable dt = myUtility.SetTable("sp_GetUserPassword",txtEmail.Text, txtZip.Text); 
				myUtility.SqlConn.Close();

				if(dt.Rows.Count>0)
				{
					DataRow aRow = dt.Rows[0];
					config = new AppSettingsReader();
					string strReimburseSite = ((string)(config.GetValue("reimbursementSite", typeof(string))));
					string strFrom = ((string)(config.GetValue("EmailFrom", typeof(string))));
					string strFromName = ((string)(config.GetValue("EmailFromName", typeof(string))));
					string strSrv = ((string)(config.GetValue("SmtpServer", typeof(string))));
					string strFromEmail = strFromName + "<" + strFrom + ">";

					string[] strCreateDate = aRow["createDate"].ToString().Split(' ');
					string[] strLastLogon = aRow["lastLogon"].ToString().Split(' ');
					
					MailMessage objEmail= new MailMessage();
					objEmail.To   =txtEmail.Text;
					objEmail.From = strFromEmail;
			//		objEmail.Bcc = ((string)(config.GetValue("EmailBcc", typeof(string))));
					objEmail.Subject  = "Smith & Nephew Reimbursements Web Site";
					objEmail.BodyFormat = MailFormat.Html;
					string strBody = "<table width=500>";
					strBody += "<tr><td>";
					strBody += "Here is the S&N account information you requested.<br><br>";
					strBody += "Your UserID is: " + aRow["EmailAddress"].ToString()+ "<br>";
					strBody += "Your Password is: " + aRow["password"].ToString()+ "<br>";
					strBody += "Date your account was created: " + strCreateDate[0];
					strBody += "<br>Date of your last visit: " + strLastLogon[0];
					strBody += "<br><br>We hope you continue to find ";
					strBody += "<a href='" + strReimburseSite + "'><font color=#FF7300>Smith & Nephew</font></a>";
					strBody += " Reimbursement Website useful and enjoyable.&nbsp;&nbsp;Please ";
					strBody += "let us know if you have any further questions or problems. ";
					strBody += "<br><br>Note: If you did not request this information, please contact us by replying to this email. ";
					strBody += "</td></tr>";
					strBody += "</table>";					
					objEmail.Body = strBody;						
					SmtpMail.SmtpServer = strSrv;
					SmtpMail.Send(objEmail);
					lblMsg.Visible = true;
					lblMsg.Text="Your password has been sent to your email address.";
					txtEmail.Text = "";
					txtZip.Text = "";
				}	
				else
				{
					lblMsg.Text="* We could not find an account with that email address and zip code.";
				}
			} // Page.IsValid
		
		} // END btnSubmit_Click

	} // END class
}
