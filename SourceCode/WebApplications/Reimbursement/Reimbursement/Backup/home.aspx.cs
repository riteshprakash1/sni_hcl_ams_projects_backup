using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for home.
	/// </summary>
	public class home : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.WebControls.HyperLink lnkEmail;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected skmMenu.Menu Menu1;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			if(myUtility.HasOccupationSpecialty() == false)
			{
				Response.Redirect("Registration.aspx", false);
			}

			AppSettingsReader config = new AppSettingsReader();
			string strEmail = ((string)(config.GetValue("EmailFrom", typeof(string))));

			lnkEmail.Text = strEmail;
			lnkEmail.NavigateUrl = "mailto:"+strEmail;

			if(!Page.IsPostBack)
			{
				Debug.WriteLine("INITIAL LOAD");
				Menu1.DataSource = this.CreateMenuXMLDocument();
				Menu1.DataBind();

				myUtility.ProductCategoryID = 0;

				// Check whether to Survey User
				myUtility.SurveyID = myUtility.RequestProductSurvey(0);
				Session["Utility"] = myUtility;

				if(myUtility.SurveyID != 0)
					Response.Redirect("SurveyRequest.aspx", false);

			} // END isPostBack
			else
				Debug.WriteLine("DID POSTBACK");

		}  // END Page_Load

		/// <summary> 
		/// Writes the Product Category/Product Menu to and XMLDocument. 
		/// </summary> 
		/// <returns>XmlDocument</returns> 
		private XmlDocument CreateMenuXMLDocument()
		{
			// Create the xml document containe
			XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
			XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "utf-8", null);
			doc.AppendChild(dec);// Create the root element
			XmlElement root = doc.CreateElement("menu");
			doc.AppendChild(root);

			foreach(DataRow r in myUtility.ProductCategoryTable.Rows)
			{
				XmlElement menuItem = doc.CreateElement("menuItem");
				XmlElement text = doc.CreateElement("text");
				text.InnerText = " " + r["productCategoryName"].ToString();
				menuItem.AppendChild(text);

				XmlElement leftImage = doc.CreateElement("leftimage");
				leftImage.InnerText = "images/icn_orange_arrow.jpg";
				menuItem.AppendChild(leftImage);
				
				string strWhere = "productCategoryID=" + r["productCategoryID"].ToString();
				DataRow[] rows = myUtility.ProductInfoTable.Select(strWhere, "productName");

				if(rows.Length > 0)
				{
					XmlElement subMenu = doc.CreateElement("subMenu");

					foreach(DataRow prodRow in rows)
					{
						bool skipProduct = Convert.ToBoolean(!myUtility.IsAdmin && prodRow["productStatus"].ToString().ToLower() != "production"); 

						if(!skipProduct)
						{

							XmlElement subMenuItem = doc.CreateElement("menuItem");
							XmlElement subMenuText = doc.CreateElement("text");
							subMenuText.InnerText = prodRow["productName"].ToString();
							subMenuItem.AppendChild(subMenuText);

							XmlElement subMenuCommandName = doc.CreateElement("commandname");
							subMenuCommandName.InnerText = prodRow["productID"].ToString();
							subMenuItem.AppendChild(subMenuCommandName);
					
							subMenu.AppendChild(subMenuItem);
						}//// end skipProduct
					}
					menuItem.AppendChild(subMenu);
				}

				root.AppendChild(menuItem);
			}
			
			return doc;
		}

		private void Menu1_MenuItemClick(object sender, skmMenu.MenuItemClickEventArgs e)
		{
			Debug.WriteLine("In Menu1_MenuItemClick");
			myUtility.ProductID = Int32.Parse(e.CommandName);
			
			string strWhere = "productID=" + myUtility.ProductID.ToString();
			DataRow[] rows = myUtility.ProductInfoTable.Select(strWhere, null);
			if(rows.Length > 0)
				myUtility.ProductCategoryID = Int32.Parse(rows[0]["productCategoryID"].ToString());

			Session["Utility"] = myUtility;	
			Debug.WriteLine("Before Redirect");
			Response.Redirect("ProcedureDisclaimer.aspx");
		}

		protected void btnView_Click(object sender, System.EventArgs e)
		{
			LinkButton but = sender as LinkButton;
			myUtility.ProductCategoryID = Int32.Parse(but.CommandArgument);
			myUtility.ProductID = 0;
			Session["Utility"] = myUtility;	
			Response.Redirect("ProductCategory.aspx");
		} // END btnView_Click

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Menu1.MenuItemClick += new skmMenu.MenuItemClickedEventHandler(this.Menu1_MenuItemClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


	} // END class
}
