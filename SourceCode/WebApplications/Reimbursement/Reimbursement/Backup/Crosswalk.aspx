<%@ Page language="c#" Codebehind="Crosswalk.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.Crosswalk" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent"><uc1:titlenavigation id="TitleNavigation2" runat="server"></uc1:titlenavigation>
			<table>
				<tr>
					<td>We have provided a list of the Smith &amp; Nephew products that can be 
						crosswalked to an existing HCPCS code. Please be advised not all HCPCS codes 
						will receive additional payment. Most HCPCS codes are used for reporting 
						purposes only under the Medicare Outpatient Payment System (OPPS).
						<br>
						<br>
						We have given you different ways to perform your search. You can narrow your 
						search by catalog number, part, description of product and/or HCPCS Code and 
						also export your search. Or you can leave all options open and export entire 
						spreadsheet to your computer.
					</td>
				</tr>
				<tr>
					<td>
						<br>
						<span style="BACKGROUND-COLOR: #f09028;COLOR: white;FONT-SIZE: 10pt">Smith &amp; 
												Nephew Departments</span><br>
						SNE = ENDOSCOPY<br>
						SNT =
						  ORTHOPAEDICS - TRAUMA<br>
						SNR = ORTHOPAEDICS - RECON<br>
						SNW = WOUND<br>
					</td>
				</tr>
			</table>
			<table width="100%" align="center">
				<tr>
					<td width="12%">Part:&nbsp;</td>
					<td width="28%"><asp:dropdownlist id="ddlPart" runat="server" Font-Size="8pt" Width="150px"></asp:dropdownlist></td>
					<td width="15%">Description:&nbsp;</td>
					<td width="25%"><asp:textbox id="txtDescription" runat="server" Font-Size="8pt" Width="150px"></asp:textbox></td>
					<td width="20%"><asp:button id="btnSearch" runat="server" Font-Size="10pt" Width="120px" Font-Names="Arial ,Helvetica,sans-serif"
							BorderWidth="0px" BackColor="#79BDE9" Text="Search" Height="21" ForeColor="White" CausesValidation="False"></asp:button></td>
				</tr>
				<tr>
					<td>Catalog #:&nbsp;</td>
					<td><asp:textbox id="txtCatalog" runat="server" Font-Size="8pt" Width="150px"></asp:textbox></td>
					<td>HCPCS Code:&nbsp;</td>
					<td><asp:dropdownlist id="ddlCPT" runat="server" Font-Size="8pt" Width="150px"></asp:dropdownlist></td>
					<td width="20%"><asp:button id="btnExport" runat="server" Font-Size="10pt" Width="120px" Font-Names="Arial ,Helvetica,sans-serif"
							BorderWidth="0px" BackColor="#79BDE9" Text="Export to Excel" Height="21" ForeColor="White" CausesValidation="False"></asp:button></td>
				</tr>
			</table>
			<asp:table id="tblCrosswalkHeader" Font-Size="11pt" Width="100%" Font-Names="Arial" BorderWidth="1"
				BackColor="#cccccc" CellSpacing="0" CellPadding="0" Runat="server" BorderStyle="Solid"
				BorderColor="Gray">
				<asp:TableRow BackColor="#CCCCCC" Height="30px">
					<asp:TableHeaderCell BackColor="#CCCCCC" HorizontalAlign="Center" Text="Crosswalk Table" CssClass="spine" 
 ID="cellFeeSchedule"></asp:TableHeaderCell>
				</asp:TableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td><asp:label id="lblNoRecords" runat="server" text="No records found for the search parameters."></asp:label><asp:datagrid id="dgCrosswalk" runat="server" Width="100%" CellSpacing="0" CellPadding="5" AllowSorting="False"
							OnSortCommand="sortHandler" OnPageIndexChanged="Page_Changed" PagerStyle-Visible="True" AllowPaging="True" AllowCustomPaging="False" PageSize="100" ShowFooter="False" AutoGenerateColumns="False" ShowHeader="True">
							<ItemStyle VerticalAlign="Top" Wrap="False"></ItemStyle>
							<HeaderStyle Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
							<Columns>
								<asp:BoundColumn HeaderStyle-Width="8%" DataField="SNDEPT" SortExpression="SNDEPT" ReadOnly="True" 
 HeaderText="SN Dept" ItemStyle-Font-Size="8pt"></asp:BoundColumn>
								<asp:BoundColumn HeaderStyle-Width="10%" DataField="Part" SortExpression="Part" ReadOnly="True" HeaderText="Part" 
 ItemStyle-Font-Size="8pt"></asp:BoundColumn>
								<asp:BoundColumn HeaderStyle-Width="12%" DataField="CatalogNum" SortExpression="CatalogNum" ReadOnly="True" 
 HeaderText="Catalog #" ItemStyle-Font-Size="8pt"></asp:BoundColumn>
								<asp:BoundColumn HeaderStyle-Width="55%" DataField="ProductDescription" SortExpression="ProductDescription" 
 ReadOnly="True" HeaderText="Product Description" ItemStyle-Font-Size="8pt"></asp:BoundColumn>
								<asp:BoundColumn HeaderStyle-Width="15%" DataField="CPT_HCPCS" SortExpression="CPT_HCPCS" ReadOnly="True" 
 HeaderText="HCPCS Code" ItemStyle-Font-Size="8pt"></asp:BoundColumn>
							</Columns>
							<PagerStyle Position="TopAndBottom" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
		</div>
		<asp:label id="lblSortOrder" runat="server" Visible="False"></asp:label><asp:literal id="Literal1" runat="server"></asp:literal></div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
