namespace Reimbursement.Controls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Diagnostics;
	using System.Data.SqlClient;
	using Reimbursement.Classes;

	/// <summary>
	///		Summary description for Header.
	/// </summary>
	public class Header : System.Web.UI.UserControl
	{
		protected Utility myUtility;
		protected string myPage;
		protected System.Web.UI.WebControls.Literal ltlLeftNavContent;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"] == null)
				myUtility = new Utility();
			else
				myUtility = (Utility)Session["Utility"];

			myPage = myUtility.GetPageFileName(Request).ToLower();
			int intPageGroupID = myUtility.GetPageGroupID(Request);
			pageTitle.InnerText = myUtility.GetPageName(Request);

			Debug.WriteLine("Left Nav Page: " + myPage);
			this.SetLeftNav(intPageGroupID);
			
			if(intPageGroupID !=4 && myUtility.EmailID==0)
			{
				Response.Redirect("Login.aspx");
			}

			if(!Page.IsPostBack)
			{
				this.InsertUseTrack(myPage);
			}
		}

		private string isSelected(string strPage)
		{
			string s = "<li>";
			
			if(myPage==strPage)
				s = "<li class=\"selected\">";
			
			return s;
		}

		private string isChildSelected(string strPage)
		{
			string s = "<li>";
			
			if(myPage==strPage)
				s = "<li class=\"childselected\">";
			
			return s;
		}

		private void SetLeftNav(int iPageGroupID)
		{
			Debug.WriteLine("PageGroupID: " + iPageGroupID.ToString());
			string strList= "";

			switch (iPageGroupID)
			{
				case 1:
					strList += "<ul>";
					strList += this.isSelected("home.aspx") + "<a href=\"home.aspx\" class=\"selected\">Reimbursement Home</a></li>";
//					if(myUtility.ProductCategoryID!=0)
//						strList += this.isSelected("faq.aspx") + "<a href=\"faq.aspx\">FAQ</a></li>";
					
					strList += this.isSelected("whatsnew.aspx") + "<a href=\"WhatsNew.aspx\">What's New</a></li>";
					strList += this.isSelected("abbreviations.aspx") + "<a href=\"Abbreviations.aspx\">Abbreviations &amp; Definitions</a></li>";
					strList += this.isSelected("links.aspx") + "<a href=\"Links.aspx\">Useful Links</a></li>";
					strList += "</ul>";
					break;

				case 2:
					strList += "<ul>";
					strList += this.isSelected("home.aspx") + "<a href=\"home.aspx\">Reimbursement Home</a></li>";
//					strList += this.isSelected("faq.aspx") + "<a href=\"faq.aspx\">FAQ</a></li>";
					strList += this.isSelected("registration.aspx") + "<a href=\"Registration.aspx\">Update Registration</a></li>";
					strList += this.isSelected("resources.aspx") + "<a href=\"Resources.aspx\">Smith &amp; Nephew Resources</a></li>";
					strList += this.isSelected("crosswalk.aspx") + "<a href=\"Crosswalk.aspx\">Crosswalk</a></li>";
					strList += this.isSelected("whatsnew.aspx") + "<a href=\"WhatsNew.aspx\">What's New</a></li>";
					strList += this.isSelected("abbreviations.aspx") + "<a href=\"Abbreviations.aspx\">Abbreviations &amp; Definitions</a></li>";
					strList += this.isSelected("links.aspx") + "<a href=\"Links.aspx\">Useful Links</a></li>";
					strList += "</ul>";
					break;
				case 3:
					strList += "<ul>";
					strList += this.isSelected("home.aspx") + "<a href=\"home.aspx\">Reimbursement Home</a></li>";
					strList += this.isSelected("proceduredisclaimer.aspx") + "<a href=\"proceduredisclaimer.aspx\">Procedure &amp; Disclaimer</a></li>";
					strList += this.isSelected("reimburseguidelines.aspx") + "<a href=\"reimburseguidelines.aspx\">Reimbursement Guidelines</a></li>";
					strList += this.isSelected("physiciancodepay.aspx") + "<a href=\"physiciancodepay.aspx\">Physician Coding and Payment</a></li>";
					strList += this.isSelected("hospitalcodepay.aspx") + "<a href=\"hospitalcodepay.aspx\">Hospital OutPatient Coding and Payment</a></li>";
					strList += this.isSelected("inpatientcodepay.aspx") + "<a href=\"inpatientcodepay.aspx\">Hospital InPatient Coding and Payment</a></li>";
					strList += this.isSelected("asccodepay.aspx") + "<a href=\"asccodepay.aspx\">ASC Coding and Payment</a></li>";
					strList += this.isSelected("referenceguide.aspx") + "<a href=\"referenceguide.aspx\">Reference Guide</a></li>";
					strList += this.isSelected("referencearticle.aspx") + "<a href=\"referencearticle.aspx\">Supporting Material</a></li>";
					strList += this.isSelected("sampleletters.aspx") + "<a href=\"sampleletters.aspx\">Sample Letters</a></li>";
					strList += this.isSelected("faq.aspx") + "<a href=\"faq.aspx\">FAQ</a></li>";
					strList += "<li class=\"selected\">Reimbursement Calculations<ul class=\"child_left_nav\">";
					strList += this.isChildSelected("hospitalasccalculator.aspx") + "<a href=\"hospitalasccalculator.aspx\">Hospital/ASC</a></li>";
					strList += this.isChildSelected("physiciancalculator.aspx") + "<a href=\"physiciancalculator.aspx\">Physician</a></li>";
					strList += "</ul>";
					break;
				case 4:
					//					strList += "<table height=\"1\"><tr><td>&nbsp;</td></tr></table>";
//					strList += "<p>&nbsp;</p>";
//					strList += "";
					break;

				default:
					break;
			} // end SWITCH

			
			string strLeftNavContent = "";
			if(strList == "")
				strLeftNavContent = "<div id=\"region-left-navigation-empty\"><p>&nbsp;</p></div>";
			else
				strLeftNavContent = "<div id=\"region-left-navigation\">" + strList + "</div>";

			
			this.ltlLeftNavContent.Text = strLeftNavContent;
		} // end SetLeftNav


		private void InsertUseTrack(string strPage)
		{
			SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "sp_InsertUseTracker";
			cmd.Connection = myUtility.SqlConn;

			cmd.Parameters.Add(new SqlParameter("@emailID", myUtility.EmailID));
			cmd.Parameters.Add(new SqlParameter("@session", Session.SessionID));
			cmd.Parameters.Add(new SqlParameter("@productID", myUtility.ProductID));
			cmd.Parameters.Add(new SqlParameter("@applicationPageID", myUtility.GetPageID(strPage)));
			cmd.Parameters.Add(new SqlParameter("@ProductCategoryID", myUtility.ProductCategoryID));
			cmd.Parameters.Add(new SqlParameter("@referringPageID", myUtility.GetPageID(myUtility.GetPageFileName(Request).ToLower())));

			try
			{   
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				int rowsUpdated = cmd.ExecuteNonQuery();
				myUtility.SqlConn.Close();
			}
			catch(Exception exc)
			{
				Debug.WriteLine(cmd.CommandText + " Error: " + exc.Message);
				myUtility.SqlConn.Close();
			}

		} // END InsertUseTrack


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
