<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="PhysicianCalculator.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.PhysicianCalculator" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent">
			<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
			<table>
				<tr>
					<td width="20%"><b>Zip Code:</b></td>
					<td width="50%"><asp:textbox id="txtZip" runat="server" MaxLength="5"></asp:textbox>
						<asp:RequiredFieldValidator id="vldZIpRequired" runat="server" ErrorMessage="You must enter a zip code." ControlToValidate="txtZip">*</asp:RequiredFieldValidator>
						<asp:RegularExpressionValidator id="vldZip" runat="server" ErrorMessage="You must enter a valid zip code." ControlToValidate="txtZip"
							ValidationExpression="\d{5}(-\d{4})?">*</asp:RegularExpressionValidator>
						<asp:CustomValidator id="vldZipNotFound" runat="server" ControlToValidate="txtZip" ErrorMessage="The zip code was not found in the calculation tables.">*</asp:CustomValidator></td>
					<td width="25%" align="center">
						<asp:button id="btnCalculate" runat="server" Width="70px" Font-Size="10pt" CausesValidation="False"
							BackColor="#79BDE9" ForeColor="White" Text="Calculate" Height="21" Font-Names="Arial ,Helvetica,sans-serif"
							BorderWidth="0px"></asp:button>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr height="30" valign="bottom">
					<td colspan="4"><asp:Label id="LabelRates" runat="server" Font-Bold="True"></asp:Label></td>
				</tr>
				<tr>
					<td colSpan="4">
						<asp:datagrid id="dgCPT" Width="740" runat="server" ShowFooter="False" AutoGenerateColumns="False"
							ShowHeader="True" CellPadding="5" CellSpacing="0">
							<HeaderStyle BackColor="#cccccc" Font-Bold="True"></HeaderStyle>
							<ItemStyle VerticalAlign="Top" Wrap="False"></ItemStyle>
							<Columns>
								<asp:BoundColumn DataField="CptMod" HeaderStyle-Width="10%" ReadOnly="True" HeaderText="CPT Code"></asp:BoundColumn>
								<asp:BoundColumn DataField="Descr" HeaderStyle-Width="60%" ReadOnly="True" HeaderText="CPT Code Description"></asp:BoundColumn>
								<asp:TemplateColumn headertext="Non-Facility" HeaderStyle-Width="15%">
									<ItemTemplate>
										<asp:Label id="lblNonPayment" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "NonFacPayment")) %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn headertext="Facility" HeaderStyle-Width="15%">
									<ItemTemplate>
										<asp:Label id="lblFacPayment" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "FacPayment")) %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
				<tr>
					<td colSpan="4" class="smallDisclaimer"><BR>
						<BR>
						<FONT size="1"><STRONG>Note:</STRONG> &nbsp; Calculations are based on CMS formulas 
							factoring in Wage Index, GPCI&nbsp;and RVUs. CMS may make adjustments to any or 
							all of the data inputs from time to time. All CPT codes are copyrights of the 
							American Medical Association.<BR>
						</FONT>
						<BR>
						<asp:Label id="lblDisclaimer" runat="server" CssClass="smallDisclaimer">
							<FONT size="1"><B>Disclaimer</B><BR>
								The information provided on this website is general reimbursement information, 
								provided for the providers' reference and convenience.&nbsp;&nbsp;This 
								information is not legal advice, nor is it advice about how to code, complete 
								or submit any particular claim for payment.&nbsp;&nbsp;Although we supply this 
								information based on our current knowledge, it is always the provider's 
								responsibility to determine and submit appropriate codes, charges, modifiers, 
								and bills for the services that were rendered.&nbsp;&nbsp;The coding and 
								reimbursement information is subject to change without 
								notice.&nbsp;&nbsp;Payers or their local branches may have their own coding and 
								reimbursement requirements and policies.&nbsp;&nbsp;Before filing any claims, 
								providers should verify current requirements and policies to the payer.<BR>
							</FONT>
						</asp:Label>
					</td>
				</tr>
				<tr>
					<td colSpan="4">
						<asp:ValidationSummary id="ValidationSummary1" runat="server"></asp:ValidationSummary></td>
				</tr>
			</table>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
