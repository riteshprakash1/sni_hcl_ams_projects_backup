<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="Abbreviations.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.Abbreviations" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<asp:Table ID="tblTerms" Runat="server"></asp:Table>
		<asp:Button ForeColor="white" BackColor="white" BorderStyle="None" id="termsBottom" runat="server"
			Text="B"></asp:Button>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
