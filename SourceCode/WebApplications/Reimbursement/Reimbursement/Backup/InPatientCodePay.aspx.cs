using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for HospitalCodePay.
	/// </summary>
	public class InPatientCodePay : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Literal ltlCoding;
		protected System.Web.UI.WebControls.Literal ltlPrivate;
		protected System.Web.UI.WebControls.DataGrid dgICD;
		protected DataTable dtICD;
		protected DataTable dtCPT;
		protected DataTable dtIcdDrg;
	
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Literal ltlICD;
		protected System.Web.UI.WebControls.Literal ltlAdditional;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.DataGrid dgDRG;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				myUtility.ProductsTable = myUtility.SetTable("sp_GetProduct", myUtility.ProductID);
				myUtility.CptIcdTable = myUtility.SetTable("sp_GetInpatientCptIcdByProduct", myUtility.ProductID);
				dtIcdDrg = myUtility.SetTable("sp_GetIcdDrgCodesByProductID", myUtility.ProductID);

				dtICD = myUtility.SetTable("sp_GetInPatientIcdCodesByProductID", myUtility.ProductID);

				Debug.WriteLine("dtICD rows: " + dtICD.Rows.Count.ToString());
				myUtility.SqlConn.Close();

				this.BindDRG();
				
				
				if(dtICD.Rows.Count > 0)
					this.BindICD();
				else
				{
					dgICD.Visible = false;
				}

				Debug.WriteLine("dgICD.Visible: " + dgICD.Visible.ToString());


				if(myUtility.ProductsTable.Rows.Count>0)
				{
					DataRow aRow = myUtility.ProductsTable.Rows[0];

					ltlICD.Text = aRow["inPatientICD"].ToString();
					ltlPrivate.Text = aRow["inPatientPrivateIns"].ToString();
					ltlAdditional.Text = aRow["inPatientAdditional"].ToString();
					ltlCoding.Text = aRow["inPatientCoding"].ToString();
				
				}

			} // END isPostBack	
		
		} // Page_Load

		public void BindDRG()
		{
			string strDrgCode = string.Empty;
			DataRow[] rows = this.dtIcdDrg.Select(null, "drgCode");
			DataTable dtDistinctDRG  = this.dtIcdDrg.Clone();
			foreach (DataRow row in rows) 
			{
				if(strDrgCode != row["drgCode"].ToString())
				{
					dtDistinctDRG.ImportRow(row);
					strDrgCode = row["drgCode"].ToString();
				}
			}
	
			if(dtIcdDrg.Rows.Count > 0)
			{
				this.dgDRG.DataSource = dtDistinctDRG;
				this.dgDRG.DataBind();
			}
			else
			{
				this.dgDRG.Visible = false;
			}
		}

		public void BindICD()
		{
			dgICD.DataSource = dtICD;
			dgICD.DataBind();
		} // END BindCPT

		public string GetDRGCrossReference(string id)
		{
			DataRow[] rows = this.dtIcdDrg.Select("IcdCode = '" + id + "'", "drgCode");
			string strRef = "";
			string strDel = "";
			foreach(DataRow aRow in rows)
			{
				strRef += strDel + aRow["drgCode"].ToString();
				strDel = ", ";
			}

			return strRef;
		}

		public string GetCrossReference(string id)
		{
			string strWhere = "productIcdCodeID = " + id;
			DataRow[] rows = myUtility.CptIcdTable.Select(strWhere);
			string strRef = "";
			string strDel = "";
			foreach(DataRow aRow in rows)
			{
				strRef += strDel + aRow["CPT_HCPCS"].ToString();
				strDel = ", ";
			}

			return strRef;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
