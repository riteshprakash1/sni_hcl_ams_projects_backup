<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="WhyRegister.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.WhyRegister" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form2" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent">
			<uc1:titlenavigation id="Titlenavigation1" runat="server"></uc1:titlenavigation>
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr vAlign="middle" height="40">
					<td width="40%"><b>Why Register?</b></td>
					<td><asp:button id="btnLogin" runat="server" Visible="False" CausesValidation="False" Text="Return to Login Page"
							ForeColor="#FF7300" BackColor="White" BorderColor="#CCCCCC" Height="21" Font-Size="10pt"></asp:button></td>
				</tr>
			</TABLE>
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colSpan="2">
						<asp:label id="lblDisclaimer" runat="server">
							<p>Smith &amp; Nephew is committed to protecting the security of your user 
								information.&nbsp;&nbsp;We use a variety of security technologies and 
								procedures to help protect your user information from unauthorized access, use, 
								or disclosure.&nbsp;&nbsp;For example, we store the user information you 
								provide on computer systems with limited access, which are located in 
								controlled facilities.
							</p>
							<p><br>
							</p>
							<p><b>Disclaimer</b></p>
							<p>The information provided with this notice is general reimbursement information 
								only; it is not legal advice, nor is it advice about how to code, complete or 
								submit any particular claim for payment.&nbsp;&nbsp;Although we supply this 
								information to the best of our current knowledge, it is always the provider's 
								responsibility to determine and submit appropriate codes, charges, modifiers 
								and bills for services that were rendered.&nbsp;&nbsp;The coding and 
								reimbursement information is subject to change without 
								notice.&nbsp;&nbsp;Payers or their local branches may have their own coding and 
								reimbursement requirements and policies.&nbsp;&nbsp;Before filing any claims, 
								providers should verify current requirements and policies with the payer.
							</p>
							<p><br>
							</p>
							<p><b>Terms of Use Disclaimer</b></p>
							<p>The information provided on this website is general reimbursement information, 
								intended for the providers' reference and convenience.&nbsp;&nbsp;This 
								information is not legal advice, nor is it advice about how to code, complete 
								or submit any particular claim for payment.&nbsp;&nbsp;It is always the 
								provider's responsibility to determine and submit appropriate codes, charges, 
								modifiers, and bills for the services that were rendered.&nbsp;&nbsp;Coding and 
								reimbursement information is subject to change without notice.&nbsp;&nbsp; 
								Payers or their local branches may have their own coding and reimbursement 
								requirements and policies.&nbsp;&nbsp;Before filing any claims, providers 
								should verify current requirements and policies to the payer.
							</p>
							<p><br>
							</p>
							<p>Smith &amp; Nephew will collect and use your information to operate and improve 
								its sites and deliver the services or carry out the transactions you have 
								requested.&nbsp;&nbsp; These uses may include providing you with more effective 
								customer service; making the sites or services easier to use by eliminating the 
								need for you to repeatedly enter the same information, and performing research 
								and analysis aimed at improving our products, services and technologies.
							</p>
							<p><br>
							</p>
							<p><b>Accessing User Information</b></p>
							<p>You may have the ability to view or edit your user information 
								online.&nbsp;&nbsp;In order to help prevent your user information from being 
								viewed by others, you will be required to sign in with your credentials (e-mail 
								address and password).&nbsp;&nbsp;The appropriate method(s) for accessing your 
								user information will depend on which sites or services you have used.<br>
								<br>
							</p>
							<p>If a password is used to help protect your accounts and user information, it is 
								your responsibility to keep your password confidential.&nbsp;&nbsp;Do not share 
								this information with anyone.&nbsp;&nbsp;If you are sharing a computer with 
								anyone you should always choose to log out before leaving a site or service to 
								protect access to your information from subsequent users.<br>
								<br>
							</p>
							<p>Smith &amp; Nephew sites use "cookies" to enable you to sign in to our services 
								and to help personalize your online experience.&nbsp;&nbsp;A cookie is a small 
								text file that is placed on your hard disk by a Web page 
								server.&nbsp;&nbsp;Cookies contain information that can later be read by a web 
								server in the domain that issued the cookie to you.&nbsp;&nbsp; Cookies cannot 
								be used to run programs or deliver viruses to your computer.&nbsp;&nbsp;The 
								cookie functionality is only used at your request by clicking on the "Remember 
								Me" checkbox found on the login page and Update Registration page.<br>
								<br>
							</p>
						</asp:label>
					</td>
				</tr>
			</TABLE>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
