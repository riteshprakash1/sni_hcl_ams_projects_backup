<%@ Register TagPrefix="cc1" Namespace="skmMenu" Assembly="skmMenu" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="home.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.home" %>
<META content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
				<td vAlign="top" align="left">
					<p><strong>Reimbursement Resource Center - Advanced Surgical Devices Division</strong><br>
						Welcome&nbsp;to the Smith &amp; Nephew Reimbursement Resource Center. This site 
						provides detailed information on reimbursement guidelines, codes, payment 
						methodologies and a reimbursement calculation tool.<br>
						<br>
						<strong>Reimbursement Product Areas</strong>
					</p>
					<cc1:menu id="Menu1" runat="server" ForeColor="#FF7300" SubmenuCSSClass="subMenu" DefaultCssClass-Length="0"
						ItemPadding="2" ItemSpacing="0" Cursor="Pointer" BackColor="Transparent" Font-Size="11pt"
						Font-Names="Arial,Helvetica,sans-serif">
						<SelectedMenuItemStyle BorderColor="Transparent" BackColor="#E0E0E0"></SelectedMenuItemStyle>
					</cc1:menu></td>
			</tr>
			<tr height="60" vAlign="bottom">
				<td>For further reimbursement questions or feedback on the website, please send an 
					email to:
					<asp:hyperlink id="lnkEmail" runat="server">HyperLink</asp:hyperlink></td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
