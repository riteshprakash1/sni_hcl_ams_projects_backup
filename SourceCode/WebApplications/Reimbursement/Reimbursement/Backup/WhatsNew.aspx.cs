using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for WhatsNew.
	/// </summary>
	public class WhatsNew : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.DataGrid dgWhatsNew;
		protected DataTable dtWhatsNew;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				myUtility.WhatsNewTable = myUtility.SetTable("sp_GetWhatsNewListByCategory",myUtility.ProductCategoryID);
				myUtility.SqlConn.Close();

				Session["Utility"] = myUtility;
				   this.BindDT();	
				
			} // END !Page.IsPostBack
         
//			this.BindDT();
		} // END Page_Load

		
		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			myUtility.WhatsNewID = Int32.Parse(dgWhatsNew.DataKeys[e.Item.ItemIndex].ToString());
			Session["Utility"] = myUtility;
			Response.Redirect("WhatsNewDetail.aspx");
		} // END btnEdit_Click

		public void BindDT()
		{
			dgWhatsNew.DataSource = myUtility.WhatsNewTable;
			dgWhatsNew.DataBind();
		}



//		protected void DisplayList()
//		{
//			foreach(DataRow aRow in dtWhatsNew.Rows)
//			{
//
//				TableRow rbutton = new TableRow();
//				TableCell cbutton = new TableCell();
//
//				Button btn = new Button();
//				btn.Text = aRow["whatsNewHeading"].ToString();
//
//				btn.CommandArgument = aRow["whatsNewID"].ToString();
//				btn.Click += new System.EventHandler(this.LinkButton1_Click);
//				btn.CssClass = "abbrevButtons"; 
//
//				cbutton.Controls.Add(btn);
//				rbutton.Controls.Add(cbutton);
//				tblWhatsNew.Controls.Add(rbutton);
//
//				TableRow rSummary = new TableRow();
//				TableCell cSummary = new TableCell();
//
//				Literal ltlSummary = new Literal();
//				ltlSummary.Text = aRow["whatsNewSummary"].ToString();
//				
//				cSummary.Controls.Add(ltlSummary);
//				rSummary.Controls.Add(cSummary);
//				tblWhatsNew.Controls.Add(rSummary);
//
//				TableRow rSpace = new TableRow();
//				TableCell cellSpace = new TableCell();
//
//				Literal ltlSpace = new Literal();
//				ltlSpace.Text = "&nbsp;";
//
//				cellSpace.Controls.Add(ltlSpace);
//				rSpace.Controls.Add(cellSpace);
//				tblWhatsNew.Controls.Add(rSpace);
//
//			} // END foreach
//		} // END DisplayList

		private void LinkButton1_Click(object sender, System.EventArgs e)
		{
			Button btn = sender as Button;
			Debug.WriteLine("Sender value = " + btn.CommandArgument + "; Sender name = " + btn.Text);
			myUtility.WhatsNewID = Int32.Parse(btn.CommandArgument);
			Session["Utility"] = myUtility;

			Response.Redirect("WhatsNewDetail.aspx");
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
