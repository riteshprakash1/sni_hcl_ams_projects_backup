<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="HospitalCodePay.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.HospitalCodePay" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<div id="pageContent">
			<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
			<asp:Literal id="ltlCoding" runat="server"></asp:Literal>
			<asp:datagrid id="dgCPT" runat="server" CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False"
				ShowFooter="False">
				<HeaderStyle BackColor="#cccccc" Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
				<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" Wrap="False"></ItemStyle>
				<Columns>
					<asp:BoundColumn DataField="CPT_HCPCS" HeaderStyle-Width="12%" ReadOnly="True" HeaderText="CPT Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="Descr" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-Width="55%" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
					<asp:BoundColumn DataField="Grp" HeaderStyle-Width="10%" ReadOnly="True" HeaderText="APC"></asp:BoundColumn>
					<asp:TemplateColumn headertext="National Medicare<br>OPPS Fee Schedule" HeaderStyle-Width="23%">
						<ItemTemplate>
							<asp:Label id="lblPayment" runat="server" Text='<%# String.Format("{0:c}", DataBinder.Eval(Container.DataItem, "Payment")) %>'>
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
			<asp:Literal id="ltlAdditional" runat="server"></asp:Literal>
			<asp:Literal id="ltlICD" runat="server"></asp:Literal>
			<asp:datagrid id="dgICD" runat="server" CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False"
				ShowFooter="False" Width="100%">
				<HeaderStyle BackColor="#cccccc" Font-Bold="True" VerticalAlign="Bottom"></HeaderStyle>
				<ItemStyle VerticalAlign="Top"></ItemStyle>
				<Columns>
					<asp:BoundColumn DataField="icdCode" HeaderStyle-Width="12%" ReadOnly="True" HeaderText="ICD-10-PCS"></asp:BoundColumn>
					<asp:BoundColumn DataField="Descr" HeaderStyle-Width="45%" ReadOnly="True" HeaderText="Description"></asp:BoundColumn>
					<asp:TemplateColumn ItemStyle-Wrap="True" HeaderText="CPT Cross Reference" HeaderStyle-Width="45%">
						<ItemTemplate>
							<asp:Label id="lblCPTCross" Text='<%# GetCrossReference(DataBinder.Eval(Container.DataItem, "productIcdCodeID").ToString()) %>' runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
			<asp:Literal id="ltlPrivate" runat="server"></asp:Literal>
		</div>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
