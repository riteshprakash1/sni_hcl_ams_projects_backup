<%@ Page language="c#" Codebehind="WhatsNewDetail.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.WhatsNewDetail" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table width="100%">
			<TR>
				<TD><B>Heading:</B></TD>
				<TD colSpan="2"><asp:label id="lblHeading" runat="server"></asp:label></TD>
			</TR>
			<tr>
				<td vAlign="top"><b>Summary:</b></td>
				<TD colSpan="2"><asp:label id="lblSummary" runat="server"></asp:label></TD>
			</tr>
			<tr>
				<td colSpan="3"><asp:literal id="ltlDetail" Runat="server"></asp:literal></td>
			</tr>
			<tr>
				<td colSpan="3"><asp:literal id="Literal1" Runat="server" Text='<hr color="#666666">'></asp:literal></td>
			</tr>
			<tr>
				<td colSpan="3"><asp:label id="lblFiles" Runat="server" Font-Bold="True">Files</asp:label></td>
			</tr>
			<tr>
				<td colSpan="3"><asp:datagrid id="dgFiles" runat="server" OnDeleteCommand="btnView_Click" DataKeyField="whatsNewFileID"
						CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False" ShowFooter="false" PagerStyle-Visible="False"
						AllowPaging="True" AllowCustomPaging="False">
						<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
						<HeaderStyle Font-Bold="True" BorderStyle="Solid" BorderWidth="5px" BorderColor="black" ForeColor="#333333"
							BackColor="#cccccc"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="File Name">
								<ItemTemplate>
									<asp:LinkButton id="btnView" Height="21" runat="server" Text='<%#GetFileLink(DataBinder.Eval(Container.DataItem, "nameFile").ToString())%>' CommandName="Delete" Font-Size="10pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC">
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Type" ItemStyle-HorizontalAlign="Center">
								<HeaderStyle Width="4%" HorizontalAlign="Center"></HeaderStyle>
								<ItemTemplate>
									<asp:Image Runat=server ImageUrl='<%# GetImage(DataBinder.Eval(Container.DataItem, "nameFile").ToString()) %>' ID="Image1">
									</asp:Image>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle Visible="False"></PagerStyle>
					</asp:datagrid>
				</td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
