using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for Faq.
	/// </summary>
	public class Faq : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Table tblFaq;
		protected DataTable dtFaq;
		protected int cnt = 0;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				dtFaq = myUtility.SetTable("sp_GetFaqByCategory", myUtility.ProductCategoryID);
				myUtility.SqlConn.Close();
			
				foreach(DataRow aRow in dtFaq.Rows)
				{
					cnt++;
					// Load Question into table
					TableRow qRow = new TableRow();
					TableCell qCell = new TableCell();
					Label lblQ = new Label();
					string strQuestion = Server.HtmlEncode(aRow["faqQuestion"].ToString());
					 
					strQuestion = strQuestion.Replace("\r\n", "<br>");
					lblQ.Text = cnt.ToString() + " - " + strQuestion;

//					lblQ.Text = cnt.ToString() + " - " + aRow["faqQuestion"].ToString();
					lblQ.Font.Bold = true;
					 
					qCell.Controls.Add(lblQ);
					qRow.Controls.Add(qCell);
					tblFaq.Controls.Add(qRow);

					// Load Answer into table
					TableRow anRow = new TableRow();
					TableCell anCell = new TableCell();
					Label lblAn = new Label();
					string strAnswer = Server.HtmlEncode(aRow["faqAnswer"].ToString());
//					char[] chars = strAnswer.ToCharArray();
//					for(int x=0; x<chars.Length; x++)
//						Debug.Write(chars[x].ToString());
					 
					strAnswer = strAnswer.Replace("\r\n", "<br>");
					lblAn.Text = strAnswer;
					 
					anCell.Controls.Add(lblAn);
					anRow.Controls.Add(anCell);
					tblFaq.Controls.Add(anRow);
					
					// add empty row for separation between questions
					TableRow blankRow = new TableRow();
					TableCell blankCell = new TableCell();
					Label blankLbl = new Label();
					blankLbl.Text = "&nbsp;";
					blankCell.Controls.Add(blankLbl);
					blankRow.Controls.Add(blankCell);
					tblFaq.Controls.Add(blankRow);

				} // END foreach loop
			} // END !Page.IsPostBack
		} // END Page_Load


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	} // END class
}
