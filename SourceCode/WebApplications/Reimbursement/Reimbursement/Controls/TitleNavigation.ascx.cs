namespace Reimbursement
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Diagnostics;
	using Reimbursement.Classes;

	/// <summary>
	///		Summary description for TitleNavigation.
	/// </summary>
	public class TitleNavigation : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.WebControls.HyperLink myScroller;
		protected System.Web.UI.WebControls.ImageButton btnPrinter;
		protected System.Web.UI.WebControls.LinkButton lnkPrint;
		protected System.Web.UI.WebControls.Table tblTop;
		protected System.Web.UI.WebControls.Label LabelProductArea;
		protected System.Web.UI.WebControls.Label lblProductArea;
		protected System.Web.UI.WebControls.Label LabelProduct;
		protected System.Web.UI.WebControls.Label lblProduct;
		protected System.Web.UI.WebControls.Table tblBottom;

		protected Utility myUtility;
		protected System.Web.UI.WebControls.ImageButton btnScroller;
		protected System.Web.UI.WebControls.Literal ltlScroller;
		protected int cnt = 0;


		private void Page_Load(object sender, System.EventArgs e)
		{

			myUtility = (Utility)Session["Utility"];
			cnt = myUtility.LinkRows;
			this.SetProductInfo();

			this.ltlTitle.Text = myUtility.GetPageName(Request);

			// PageGroupID set on the LeftNavHome Page
			if(myUtility.GetPageGroupID(Request) == 3)
			{
				btnPrinter.Attributes.Add("onclick", "javascript:runPrint(document.all.pageContent);");
				lnkPrint.Attributes.Add("onclick", "javascript:runPrint(document.all.pageContent);");
				ltlScroller.Text = "<A href=\"javascript:scroller(pageContent);\">Scroll&nbsp;<img src=\"Images/scroller.gif\" alt=\"\" border=\"0\"></A>";
			}
			else
			{
				btnPrinter.Attributes.Add("onclick", "window.print();");
				this.lnkPrint.Attributes.Add("onclick", "window.print();");
				ltlScroller.Text = "";
			}
		} // end Page_Load


		private void SetProductInfo()
		{
			bool isProductPage = false;
			if(myUtility.PagesTable.Rows.Count!=0)
			{
				string myPage = myUtility.GetPageFileName(Request);
				string strCrit = "pageFile = '" + myPage + "'";
				Debug.WriteLine("Title Nav myPage: " + myPage);
				DataRow[] rows = myUtility.PagesTable.Select(strCrit);
				myUtility.ProductCategoryID = 0;
				if(rows.Length > 0)
				{
					int intPageLevel = Convert.ToInt32(rows[0]["pageLevel"].ToString());
					if(intPageLevel == 3)
					{
						isProductPage = true;
						strCrit = "productID="+myUtility.ProductID.ToString();
						rows = myUtility.ProductInfoTable.Select(strCrit);
						Debug.WriteLine("Rows: " + rows.Length.ToString() + "; cnt: " + cnt.ToString() + "; strCrit: " + strCrit);
						myUtility.ProductCategoryID = Convert.ToInt32(rows[0]["productCategoryID"].ToString());
						if(rows.Length > 0)
						{
							lblProduct.Text = rows[0]["productName"].ToString();
							this.lblProductArea.Text = rows[0]["productCategoryName"].ToString();
						}
						else
						{
							lblProduct.Text = string.Empty;
							this.lblProductArea.Text = string.Empty;
						}
					}
				}

				LabelProduct.Visible = isProductPage;
				lblProduct.Visible = isProductPage;
				this.tblBottom.Visible = isProductPage;
				this.tblTop.Visible = isProductPage;
				LabelProductArea.Visible= isProductPage;
				lblProductArea.Visible = isProductPage;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}// end class
}
