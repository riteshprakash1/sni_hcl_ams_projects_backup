<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Header.ascx.cs" Inherits="Reimbursement.Controls.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title runat="server" id=pageTitle>Header</title>
		<meta content="Reimbursement, Managed Care, Fee Schedules, Medicare, CMS, Physician Coding, &#13;&#10;&#9;&#9;Hospital Coding, ASC Coding, APC, Payor, Payer, Resource Center, Policies, Published Literature, Modifiers, CPT, HCPCS, &#13;&#10;&#9;&#9;C-Codes, ICD9, Diagnosis, Calculations, Reference Guides, Sample Letters, RVU, Non-Facility, Private Insurers, IDET, &#13;&#10;&#9;&#9;Discography, TriVex, TCI, Arthroscopy, Insurance Coverage, &#13;&#10;&#9;&#9;Stimulation, SpineCath, Intradiscal, CDS, Controlled Disc, RF, Pulsed Radiofrequency, Denervation Lumbar,&#13;&#10;&#9;&#9;Cervical, Transilluminated, TCI, Knee, Shoulder, Hip, Small Joint, Operative Hysteroscopy, Billing, Medical Device, Smith &amp; Nephew,&#13;&#10;&#9;&#9;C1754, C1713, 58558, 58561, 0062T, 0063T, 64999, 64622, 64623, &#13;&#10;&#9;&#9;64626, 64627, 72285, 72295, 62290, 62291, 37765, 37766, 37785, 37799, 37700, 37718, 37722, 37735, S2300, 29805, 29806, &#13;&#10;&#9;&#9;29807, 29819, 29820, 29821, 29822, 29823, 29824, 29825, 29826, 29827, 29860, 29861, 29862, 29863, 29866, 29867, 29868, &#13;&#10;&#9;&#9;29870, 29871, 29873, 29874, 29875, 29876, 29877, 29879, 29880, 29881, 29882, 29883, 29884, 29885, 29886, 29887, 29888, &#13;&#10;&#9;&#9;29889, G0289, 29830, 29834, 29835, 29836, 29837&#13;&#10;&#9;&#9;&#9;29838, 29840, 29843, 29844, 29845, 29846, 29847, &#13;&#10;&#9;&#9;29848, 29891, 29892, 29893, 29894, 29895, 29897, 29898, 29899, 29900, 29901, 29902, 29999"
			name="keywords">
		<meta content="True" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/global.css" type="text/css" rel="stylesheet">
		<LINK href="css/styles.css" type="text/css" rel="stylesheet">
		<LINK href="css/StyleNew.css" type="text/css" rel="stylesheet">
		<LINK href="css/print.css" type="text/css" rel="stylesheet" media="print">
		<LINK href="css/ScreenStyle.css" type="text/css" rel="stylesheet" media="screen">
		<script src="scripts/showhint.js" type="text/javascript"></script>
		<script src="scripts/JavaScr.js" type="text/javascript"></script>
		<script src="scripts/navigation.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script type="text/javascript">
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-93837333-1', 'auto');
              ga('send', 'pageview');

        </script>

        <script type="text/javascript">
            // Added to get the file download clicks to register in Google Analytics 

            if (typeof jQuery != 'undefined') {
              jQuery(document).ready(function($) {
                var filetypes = /\.(zip|exe|dmg|pdf|doc.*|xls.*|ppt.*|mp3|txt|rar|wma|mov|avi|wmv|flv|wav)$/i;
                var baseHref = '';
                if (jQuery('base').attr('href') != undefined) baseHref = jQuery('base').attr('href');
 
                jQuery('a').on('click', function(event) {
                    var el = jQuery(this);
                  var track = true;
                  var href = (typeof(el.attr('href')) != 'undefined' ) ? el.attr('href') :"";
                  var isThisDomain = href.match(document.domain.split('.').reverse()[1] + '.' + document.domain.split('.').reverse()[0]);
                  if (!href.match(/^javascript:/i)) {
                    var elEv = []; elEv.value=0, elEv.non_i=false;
                    if (href.match(/^mailto\:/i)) {
                      elEv.category = "email";
                      elEv.action = "click";
                      elEv.label = href.replace(/^mailto\:/i, '');
                      elEv.loc = href;
                    }
                    else if (href.match(filetypes)) {
                      var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
                      elEv.category = "download";
                      elEv.action = "click-" + extension[0];
                      elEv.label = href.replace(/ /g,"-");
                      elEv.loc = baseHref + href;
                    }
                    else if (href.match(/^https?\:/i) && !isThisDomain) {
                      elEv.category = "external";
                       elEv.action = "click";
                      elEv.label = href.replace(/^https?\:\/\//i, '');
                      elEv.non_i = true;
                      elEv.loc = href;
                    }
                    else if (href.match(/^tel\:/i)) {
                      elEv.category = "telephone";
                      elEv.action = "click";
                      elEv.label = href.replace(/^tel\:/i, '');
                      elEv.loc = href;
                    }
                    else track = false;
 
                    if (track) {
                      _gaq.push(['_trackEvent', elEv.category.toLowerCase(), elEv.action.toLowerCase(), elEv.label.toLowerCase(), elEv.value, elEv.non_i]);
                      if ( el.attr('target') == undefined || el.attr('target').toLowerCase() != '_blank') {
                        setTimeout(function() { location.href = elEv.loc; }, 400);
                        return false;
                  }
                }
                  }
                });
              });
            }
            </script>
    </HEAD>
	<body MS_POSITIONING="GridLayout">
		<div class="d1_details" id="shell">
			<div id="region-body-and-footer">
				<div id="masthead">
					<div class="floatLeft width330">
						<a href="http://www.smith-nephew.com/us/professional/" border="0"><img src="images/logo.gif" alt="Smith &amp; Nephew" border="0" class="floatLeft marginTop10"></a>
						<div class="clear paddingTop10 strapline"><a style=" font-size:1.0em;color:#85898b;font-weight:normal;" href="http://www.smith-nephew.com/us/professional/resources/reimbursement/">US Professional</a></div>
					</div>
					<div class="floatRight width620 marginLeft10">
						<div id="masthead-links">&nbsp;</div>
						<div class="clear"></div>
						<div class="floatRight">&nbsp;</div>
					</div>
					<div class="clear"></div>
				</div>

				<div id="horizontalNav">&nbsp;</div>  

				<div id="breadcrumbs"><ul></ul></div>
				<div class="clear"></div>
				<div id="region-body">
					
					<!--div id="region-left-navigation"-->
						<asp:Literal id="ltlLeftNavContent" runat="server"></asp:Literal>