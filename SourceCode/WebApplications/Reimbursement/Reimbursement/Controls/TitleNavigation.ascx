<%@ Control Language="c#" AutoEventWireup="false" Codebehind="TitleNavigation.ascx.cs" Inherits="Reimbursement.TitleNavigation" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table border="0" cellSpacing="0" cellPadding="0" width="100%">
	<tr>
		<td width="600">
			<div id="orangeTitle"><asp:literal id="ltlTitle" runat="server"></asp:literal></div>
		</td>
		<td align="right">
			<div class="print-and-email">
				<div class="floatLeft"><asp:linkbutton id="lnkPrint" runat="server" CausesValidation="False">Print</asp:linkbutton></div>
				<div class="floatLeft marginLeft3"><asp:imagebutton id="btnPrinter" runat="server" CausesValidation="False" ImageUrl="../Images/icn_print.gif"></asp:imagebutton></div>
				<div class="floatLeft marginLeft5"><asp:literal id="ltlScroller" runat="server"></asp:literal></div>
			</div>
		</td>
	</tr>
</table>
<asp:table id="tblTop" Runat="server" Width="100%" DESIGNTIMEDRAGDROP="45">
	<asp:TableRow Height="5">
		<asp:TableCell></asp:TableCell>
	</asp:TableRow>
</asp:table>
<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
	<tr vAlign="middle">
		<td>
			<table width="100%">
				<tr>
					<td width="35%"><asp:label id="LabelProductArea" runat="server"><b>Product 
								Area:&nbsp;&nbsp;</b></asp:label><asp:label id="lblProductArea" runat="server"></asp:label></td>
					<td>
					<td><asp:label id="LabelProduct" runat="server"><b>Product:&nbsp;&nbsp;</b></asp:label><asp:label id="lblProduct" runat="server"></asp:label></td>
				</tr>
			</table>
		</td>
	</tr>
</TABLE>
<asp:table id="tblBottom" Runat="server" Width="100%">
	<asp:TableRow Height="5">
		<asp:TableCell></asp:TableCell>
	</asp:TableRow>
</asp:table>
