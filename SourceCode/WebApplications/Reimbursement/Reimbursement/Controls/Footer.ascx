<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Footer.ascx.cs" Inherits="Reimbursement.Controls.Footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
				<div id="region-footer">
					<ul id="footer2">
						<li><sup>&#x25CA;</sup> Trademark of Smith & Nephew  | This information intended for United States customers only</li>
					</ul>
					<ul id="footer1">
						<li></li>
						<li><a href="http://www.smith-nephew.com/professional/accessibility/">Accessibility</a></li>
						<li><a href="http://www.smith-nephew.com/professional/copyright/">Copyright & Disclaimer</a></li>
						<li></li>
						<li></li>
					</ul>
				</div>


			</div>
		</div>
	</body>
</HTML>
