using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for PhysicianCalculator.
	/// </summary>
	public class PhysicianCalculator : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtZip;
		protected DataTable dtGpci;
		protected DataTable dtCPT;
	
		protected Utility myUtility;
		protected System.Web.UI.WebControls.DataGrid dgCPT;
		protected System.Web.UI.WebControls.RequiredFieldValidator vldZIpRequired;
		protected System.Web.UI.WebControls.RegularExpressionValidator vldZip;
		protected System.Web.UI.WebControls.Button btnCalculate;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.CustomValidator vldZipNotFound;
		protected System.Web.UI.WebControls.Label lblDisclaimer;
		protected System.Web.UI.WebControls.Label LabelRates;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");


			if(!Page.IsPostBack)
			{
				EnterButton.TieButton(txtZip, btnCalculate);
				this.LabelRates.Text = myUtility.RateYear + " Rate Calculations";
				this.LabelRates.Visible=false;

			} // END isPostBack	
		
		} // Page_Load

		public void BindCPT()
		{
			dgCPT.DataSource = dtCPT;
			dgCPT.DataBind();
		} // END BindCPT


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private double GetAmount(string strAmount)
		{
			double dblAmt = 0.00;
			if(strAmount != "")
			{
				try
				{
					dblAmt = Double.Parse(strAmount);
				}
				catch(Exception exc)
				{
					Debug.WriteLine("GetAmount Error: " + exc.Message);
					Debug.WriteLine("Could not Double.Parse: " + strAmount);
				}

			}
			return dblAmt;
		} // END GetAmount
		
		private void btnCalculate_Click(object sender, System.EventArgs e)
		{
			Page.Validate();

			if(Page.IsValid)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				dtGpci = myUtility.SetTable("sp_GetGpciByZip", txtZip.Text);
				dtCPT = myUtility.SetTable("sp_GetPhysicianRvuByProduct", myUtility.ProductID);

				myUtility.SqlConn.Close();

				if(dtGpci.Rows.Count == 0)
				{
					foreach(IValidator val in Page.Validators)
					{
						if(val.ErrorMessage == "The zip code was not found in the calculation tables.")
							val.IsValid = false;
						else
							val.IsValid = true;
					} // END foreach
				}
				else
				{
					AppSettingsReader configAppSet = new AppSettingsReader();
					double dblRvuAdj = ((double)(configAppSet.GetValue("rvuAdjustor", typeof(double))));
					
					double dblGpciWork = this.GetAmount(dtGpci.Rows[0]["GpciWork"].ToString());
					double dblGpciPE = this.GetAmount(dtGpci.Rows[0]["GpciPE"].ToString());
					double dblGpciMP = this.GetAmount(dtGpci.Rows[0]["GpciMP"].ToString());

					Debug.WriteLine(" dblNon = (((dblRvuWork*dblRvuAdj) *dblGpciWork)+(dblNonPE*dblGpciPE)+(dblFacMP*dblGpciMP))* dblConvFactor");
					Debug.WriteLine(" blFac = (((dblRvuWork*dblRvuAdj) *dblGpciWork)+(dblFacPE*dblGpciPE)+(dblFacMP*dblGpciMP))* dblConvFactor");

					for(int x=0; x<dtCPT.Rows.Count; x++)
					{
						double dblRvuWork = this.GetAmount(dtCPT.Rows[x]["RvuWork"].ToString());
						double dblFacMP = this.GetAmount(dtCPT.Rows[x]["FacMP"].ToString());
						double dblNonPE = this.GetAmount(dtCPT.Rows[x]["NonPE"].ToString());
						double dblFacPE = this.GetAmount(dtCPT.Rows[x]["FacPE"].ToString());
						double dblConvFactor = this.GetAmount(dtCPT.Rows[x]["ConvFactor"].ToString());

//						double dblNon = ((dblRvuWork*dblGpciWork)+(dblNonPE*dblGpciPE)+(dblFacMP*dblGpciMP))* dblConvFactor;
//						double dblFac = ((dblRvuWork*dblGpciWork)+(dblFacPE*dblGpciPE)+(dblFacMP*dblGpciMP))* dblConvFactor;

						double dblNon = (((dblRvuWork*dblRvuAdj) *dblGpciWork)+(dblNonPE*dblGpciPE)+(dblFacMP*dblGpciMP))* dblConvFactor;
						double dblFac = (((dblRvuWork*dblRvuAdj) *dblGpciWork)+(dblFacPE*dblGpciPE)+(dblFacMP*dblGpciMP))* dblConvFactor;

						dtCPT.Rows[x]["NonFacPayment"] = dblNon;
						dtCPT.Rows[x]["FacPayment"] = dblFac;
						
						
						string strRvuWork = dblRvuWork.ToString();
						string strFacMP = dblFacMP.ToString();
						string strNonPE = dblNonPE.ToString();
						string strFacPE = dblFacPE.ToString();
						string ConvFactor = dblConvFactor.ToString(); 
						string strRvuAdj = dblRvuAdj.ToString();
						
						Debug.WriteLine("Non CPT: " + dtCPT.Rows[x]["CPT_HCPCS"].ToString() + " :: = ((( " + strRvuWork + " * " + strRvuAdj + " ) * " + dblGpciWork.ToString() + " ) + ( " + strNonPE + " * " + dblGpciPE.ToString() + " ) + ( " + strFacMP + " * " + dblGpciMP.ToString() + " )) * " +  ConvFactor);
						Debug.WriteLine("Fac CPT: " + dtCPT.Rows[x]["CPT_HCPCS"].ToString() + " :: = ((( " + strRvuWork + " * " + strRvuAdj + " ) * " + dblGpciWork.ToString() + " ) + ( " + strFacPE + " * " + dblGpciPE.ToString() + " ) + ( " + strFacMP + " * " + dblGpciMP.ToString() + " )) * " +  ConvFactor);
						//		Debug.WriteLine("intNonFacPay=" + dtCPT.Rows[x]["NonFacPayment"].ToString());
					}

					if(dtCPT.Rows.Count > 0)
					{
						this.BindCPT();
						this.LabelRates.Visible=true;
					}
					else
					{
						dgCPT.Visible = false;
						this.LabelRates.Visible=false;
					}
				} // END rows == 0
			} // isValid
		}
	} // END class
}
