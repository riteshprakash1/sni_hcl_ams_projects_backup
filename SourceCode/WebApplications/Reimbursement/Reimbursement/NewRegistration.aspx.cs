using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using System.Web.Mail;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for NewRegistration.
	/// </summary>
	public class NewRegistration : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblocc;
		protected System.Web.UI.WebControls.DropDownList ddbocu;
		protected System.Web.UI.WebControls.Label lblspe;
		protected System.Web.UI.WebControls.DropDownList ddbspe;
		protected System.Web.UI.WebControls.Label lblzip;
		protected System.Web.UI.WebControls.TextBox txtzip;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
		protected System.Web.UI.WebControls.Label lblemail;
		protected System.Web.UI.WebControls.TextBox txtemail;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.Label lblpassword;
		protected System.Web.UI.WebControls.TextBox txtpass;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.Label lblconfirm;
		protected System.Web.UI.WebControls.TextBox txtconfirm;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator1;
		protected System.Web.UI.WebControls.CheckBoxList chbupdate;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;

		protected Utility myUtility;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnRegister;
		protected System.Web.UI.WebControls.RadioButtonList rbDisclaimer;
		protected System.Web.UI.WebControls.RequiredFieldValidator vldDiscalimer;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Label lblDisclaimer;
		protected System.Web.UI.WebControls.CompareValidator vldRejectDisclaimer;
		protected DataTable dtProfile;
		protected System.Web.UI.WebControls.CustomValidator vldPassWordLen;
		protected System.Web.UI.WebControls.CustomValidator vldExistEmail;
		protected System.Web.UI.WebControls.Label lblConfirmEmail;
		protected System.Web.UI.WebControls.TextBox txtConfirmEmail;
		protected System.Web.UI.WebControls.RequiredFieldValidator vldRequiredConfirmEmail;
		protected System.Web.UI.WebControls.CustomValidator Customvalidator1;
		protected System.Web.UI.WebControls.CompareValidator vldOccupation;
		protected System.Web.UI.WebControls.CompareValidator vldSpecialty;
		protected AppSettingsReader config;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.CheckBox chkSubscribe;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("Page_Load");

			if(Session["Utility"]!= null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("Login.aspx");
		   
			if(!Page.IsPostBack)
			{  
				EnterButton.TieButton(this.txtemail, btnRegister);
				EnterButton.TieButton(this.txtpass, btnRegister);
				EnterButton.TieButton(this.txtconfirm, btnRegister);
				EnterButton.TieButton(this.txtzip, btnRegister);
				
				this.BindDT();
			
				myUtility.ProductCategoryID = 0;
				ddbocu.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.OccupationTable.Rows)
				{
					ddbocu.Items.Add(new ListItem(aRow["Occupation_Name"].ToString(),aRow["Occupation_ID"].ToString() ));
				}

				ddbspe.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.SpecialtyTable.Rows)
				{
					ddbspe.Items.Add(new ListItem(aRow["Specialty_Name"].ToString(),aRow["Specialty_ID"].ToString() ));
				}
				
			} // END isPostBack
		
        
		}  // END Page_Load 

		public void BindDT()
		{
			if(myUtility.EmailID == 0)
				myUtility.ExcludeDraftProductCategories();
//			chbupdate.DataSource = myUtility.ProductCategoryTable;
//			chbupdate.DataTextField = "productCategoryName";
//			chbupdate.DataValueField = "productCategoryID";
//			chbupdate.DataBind();
		}

		private bool IsValidPassword(string strPass)
		{
			if(strPass.Length < 4 || strPass.Length > 10)
				return false;
			else
				return true;
		}

		private void btnRegister_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("btnRegister_Click");
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("The length of the password"))
				{
					val.IsValid = this.IsValidPassword(txtpass.Text);
				}

				if(val.ErrorMessage.StartsWith("Email Address mismatch"))
				{
					val.IsValid = Convert.ToBoolean(this.txtConfirmEmail.Text==this.txtemail.Text);
				}

			} // END foreach

			if(Page.IsValid)
			{
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				DataTable dt = myUtility.SetTable("sp_GetUserByEmail",txtemail.Text);
				
				Debug.WriteLine("sp_GetUserByEmail");

				if(dt.Rows.Count == 0)
				{
					SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
					cmdInsert.CommandType = CommandType.StoredProcedure;
					cmdInsert.CommandText = "sp_InsertUserProfile";
					cmdInsert.Connection = myUtility.SqlConn;
					SqlParameter paramID = new SqlParameter("@EmailID", SqlDbType.Int);
					cmdInsert.Parameters.Add(paramID);
					paramID.Direction = ParameterDirection.Output;

					cmdInsert.Parameters.Add(new SqlParameter("@OccupationID",(ddbocu.SelectedValue)));
					cmdInsert.Parameters.Add(new SqlParameter("@SpecialtyID", (ddbspe.SelectedValue)));
					cmdInsert.Parameters.Add(new SqlParameter("@ZipCode", txtzip.Text));
					cmdInsert.Parameters.Add(new SqlParameter("@EmailAddress", txtemail.Text.ToLower()));
					cmdInsert.Parameters.Add(new SqlParameter("@Password", txtpass.Text.ToLower()));
					cmdInsert.Parameters.Add(new SqlParameter("@disclaimer", this.lblDisclaimer.Text));
					cmdInsert.Parameters.Add(new SqlParameter("@Subscribe", this.chkSubscribe.Checked.ToString()));

					try
					{   
						int rowsUpdated = cmdInsert.ExecuteNonQuery();
						myUtility.EmailID = Int32.Parse(paramID.Value.ToString());
					}
					catch(Exception exc)
					{
						Debug.WriteLine(cmdInsert.CommandText + " Error - " + exc.Message);
						myUtility.SqlConn.Close();
					}

//					// Enter checkbox items
//					for(int x=0; x<this.chbupdate.Items.Count; x++)
//					{
//						if(this.chbupdate.Items[x].Selected)
//						{
//							SqlCommand cmdProdInsert = new System.Data.SqlClient.SqlCommand();
//							cmdProdInsert.CommandType = CommandType.StoredProcedure;
//							cmdProdInsert.Connection = myUtility.SqlConn;
//							cmdProdInsert.CommandText = "sp_InsertUserProduct";
//							cmdProdInsert.Parameters.Add(new SqlParameter("@EmailID",myUtility.EmailID));
//							cmdProdInsert.Parameters.Add(new SqlParameter("@ProductCategoryID", Int32.Parse(chbupdate.Items[x].Value)));
//				
//							try
//							{   
//								int rowsUpdated = cmdProdInsert.ExecuteNonQuery();
//							}
//							catch(Exception exc)
//							{
//								Debug.WriteLine(cmdProdInsert.CommandText + " Error - " + exc.Message);
//								myUtility.SqlConn.Close();
//							}	
//						} //END selected item
//					}// END  for loop
					
					myUtility.UserProfileTable = myUtility.GetUserInfo(txtemail.Text, this.txtpass.Text);

					if(myUtility.UserProfileTable.Rows.Count>0)
					{
						Debug.WriteLine("SendEmail");
						this.SendEmail(txtemail.Text, this.txtpass.Text);
						Session["Utility"] = myUtility;
						myUtility.LogHit(txtemail.Text, "New Registration");
						Response.Redirect("Home.aspx");
					}

				} // 
				else
				{
					Debug.WriteLine("Text: " + this.txtemail + "; " + dt.Rows[0]["emailAddress"].ToString());
					foreach(IValidator val in Page.Validators)
					{
						if(val.ErrorMessage == "The email address already exists.")
						{
							val.IsValid = false;
						}
					} // END foreach
				}

				myUtility.SqlConn.Close();
      		
			} // END Page.IsValid
		}


		private void SendEmail(string strEmail, string strPass)
		{
					
			config = new AppSettingsReader();
			string strReimburseSite = ((string)(config.GetValue("reimbursementSite", typeof(string))));
			string strFrom = ((string)(config.GetValue("EmailFrom", typeof(string))));
			string strFromName = ((string)(config.GetValue("EmailFromName", typeof(string))));
			string strSrv = ((string)(config.GetValue("SmtpServer", typeof(string))));
			string strFromEmail = strFromName + "<" + strFrom + ">";

			MailMessage objEmail= new MailMessage();
			objEmail.To   = strEmail;
			objEmail.From = strFromEmail;
	//		objEmail.Bcc = ((string)(config.GetValue("EmailBcc", typeof(string))));
			objEmail.Subject  = "Smith & Nephew Reimbursements Web Site - New Registration";
			objEmail.BodyFormat = MailFormat.Html;
			string strBody = "<table width=500>";
			strBody += "<tr><td>";
			strBody += "Thank you for registering with the ";
			strBody += "<a href='" + strReimburseSite + "'><font color=#FF7300>Smith & Nephew</font></a>";
			strBody += " Endoscopy Reimbursement Website. This email is confirmation of your registration. ";
			strBody += "<br><br>Your UserID is: " + strEmail + "<br>";
			strBody += "Your Password is: " + strPass + "<br>";
			strBody += "<br><br>The user ID and password are unique to you and will ";
			strBody += "allow you full access to the ";
			strBody += "<a href='" + strReimburseSite + "'><font color=#FF7300>Smith & Nephew</font></a>";
			strBody += " Reimbursement website.";
			strBody += "</td></tr>";
			strBody += "</table>";					
			objEmail.Body = strBody;						
			SmtpMail.SmtpServer = strSrv;
			SmtpMail.Send(objEmail);
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion



	} // END class
}
