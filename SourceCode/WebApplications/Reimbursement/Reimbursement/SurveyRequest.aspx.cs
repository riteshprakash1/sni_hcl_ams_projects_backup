using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for SurveyRequest.
	/// </summary>
	public class SurveyRequest : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnDeclineSurvey;
		protected System.Web.UI.WebControls.Button btnAskLater;
		protected System.Web.UI.WebControls.Button btnTakeSurvey;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Label lblProductID;
		protected System.Web.UI.WebControls.Button btnBack;
		protected System.Web.UI.WebControls.Label lblSurveyMessage;
		protected System.Web.UI.WebControls.Label lblSurveyURL;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"] != null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				DataTable dt = myUtility.SetTable("sp_GetSurveyBySurveyID", myUtility.SurveyID);
				if(dt.Rows.Count > 0)
				{
					this.lblProductID.Text = dt.Rows[0]["ProductID"].ToString();
					this.lblSurveyURL.Text = dt.Rows[0]["SurveyURL"].ToString();
					this.lblSurveyMessage.Text = dt.Rows[0]["SurveyMessage"].ToString().Replace("\r", "<br>");;
					this.SetControls(true);
				}
				else
				{
					Response.Redirect("Home.aspx", true);
				}
			}
		}


		private void SetControls(bool promptUser)
		{
			this.btnAskLater.Visible = promptUser;
			this.btnDeclineSurvey.Visible = promptUser;
			this.btnTakeSurvey.Visible = promptUser;

			this.btnBack.Visible = !promptUser;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnTakeSurvey.Click += new System.EventHandler(this.btnTakeSurvey_Click);
			this.btnDeclineSurvey.Click += new System.EventHandler(this.btnDeclineSurvey_Click);
			this.btnAskLater.Click += new System.EventHandler(this.btnAskLater_Click);
			this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnTakeSurvey_Click(object sender, System.EventArgs e)
		{
			this.RecordResponse("Take Survey");
			this.SetControls(false);

			myUtility.SurveyID = 0;
			Session["Utility"] = myUtility;	
			
			string strSurveyURL = this.lblSurveyURL.Text + "?c=" + myUtility.EmailID.ToString();

			Debug.WriteLine("strSurveyURL: " + strSurveyURL);

			string strWindowLocation = "window.location = '" + strSurveyURL + "'"; 
			Response.Write("<script>");
			Response.Write(strWindowLocation);
			Response.Write("</script>");

			//// Pop-Up Window
//			string strWindowOpen = "window.open('" + strSurveyURL + "','_blank')";
//			Response.Write("<script>");
//			Response.Write(strWindowOpen);
//			Response.Write("</script>");
		}

		private void btnDeclineSurvey_Click(object sender, System.EventArgs e)
		{
			this.RecordResponse("Decline Survey");

			myUtility.SurveyID = 0;
			Session["Utility"] = myUtility;	
			
			if(this.lblProductID.Text == "0")
				Response.Redirect("Home.aspx", false);
			else
				Response.Redirect("ProcedureDisclaimer.aspx", false);
		}

		private void btnAskLater_Click(object sender, System.EventArgs e)
		{
			this.RecordResponse("Later");

			myUtility.SurveyID = 0;
			Session["Utility"] = myUtility;	
			
			if(this.lblProductID.Text == "0")
				Response.Redirect("Home.aspx", false);
			else
				Response.Redirect("ProcedureDisclaimer.aspx", false);
		}

		private void RecordResponse(string strUserResponse)
		{
			SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
			cmdInsert.CommandType = CommandType.StoredProcedure;
			cmdInsert.Connection = myUtility.SqlConn;

			cmdInsert.Parameters.Add(new SqlParameter("@UserID", myUtility.EmailID));
			cmdInsert.Parameters.Add(new SqlParameter("@SurveyID", myUtility.SurveyID));
			cmdInsert.Parameters.Add(new SqlParameter("@UserResponse", strUserResponse));
				
			SqlParameter paramID = new SqlParameter("@SurveyResponseID", SqlDbType.Int);
			cmdInsert.Parameters.Add(paramID);

			cmdInsert.CommandText = "sp_InsertSurveyResponse";
			paramID.Direction = ParameterDirection.Output;

			if(myUtility.SqlConn.State != ConnectionState.Open)
				myUtility.SqlConn.Open();

			int rowsUpdated = cmdInsert.ExecuteNonQuery();
			myUtility.SqlConn.Close();
		}

		private void btnBack_Click(object sender, System.EventArgs e)
		{
			if(this.lblProductID.Text == "0")
				Response.Redirect("Home.aspx", false);
			else
				Response.Redirect("ProcedureDisclaimer.aspx", false);

		}


	}//// end class
}
