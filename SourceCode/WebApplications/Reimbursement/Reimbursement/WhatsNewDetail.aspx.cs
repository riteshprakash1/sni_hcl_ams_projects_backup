using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for WhatsNewDetail.
	/// </summary>
	public class WhatsNewDetail : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Label lblHeading;
		protected System.Web.UI.WebControls.Label lblSummary;
		protected System.Web.UI.WebControls.Literal Literal1;
		protected System.Web.UI.WebControls.TextBox txtDetail;
		protected System.Web.UI.WebControls.DataGrid dgFiles;
		protected System.Web.UI.WebControls.Label lblFiles;
		protected System.Web.UI.WebControls.Literal ltlDetail;
		protected DataTable dtWhatsNew;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				if(myUtility.WhatsNewID != 0)
				{
				
					if(myUtility.SqlConn.State != ConnectionState.Open)
						myUtility.SqlConn.Open();

					dtWhatsNew = myUtility.SetTable("sp_GetWhatsNew", myUtility.WhatsNewID);
					myUtility.WhatsNewFilesTable = myUtility.SetTable("sp_GetWhatsNewFiles", myUtility.WhatsNewID);
					myUtility.SqlConn.Close();

					Session["Utility"] = myUtility;

					Debug.WriteLine("Rows = " + dtWhatsNew.Rows.Count.ToString());
					if(dtWhatsNew.Rows.Count > 0)
						this.LoadData(dtWhatsNew.Rows[0]);

					this.BindDT();
				}
			} // END !Page.IsPostBack
			this.SetControls();
		} // END Page_Load


		public string GetFileLink(string strFile)
		{
			string strLink = "<img src=\"images/icn_orange_arrow.jpg\" alt=\"\" border=\"0\"/>&nbsp;" + strFile;
			return strLink;
		}


		public void BindDT()
		{
			dgFiles.DataSource = myUtility.WhatsNewFilesTable;
			dgFiles.DataBind();

			bool isVisible = Convert.ToBoolean(myUtility.WhatsNewFilesTable.Rows.Count != 0);
			dgFiles.Visible = isVisible;
			lblFiles.Visible = isVisible;
			Literal1.Visible = isVisible;
		}

		protected void SetControls()
		{
			if(myUtility.WhatsNewID == 0)
			{
				this.dgFiles.Visible = false;
				this.lblFiles.Visible = false;
			}
			else
			{
				this.dgFiles.Visible = true;
				this.lblFiles.Visible = true;
			}
		}

		protected void LoadData(DataRow aRow)
		{
			string strDetail = aRow["whatsNewDetail"].ToString();
			strDetail = strDetail.Replace("\r\n", "<br>");
			strDetail = strDetail.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			ltlDetail.Text = "<br><br>" + strDetail;

	//		this.txtDetail.Text = aRow["whatsNewDetail"].ToString();
			this.lblSummary.Text = aRow["whatsNewSummary"].ToString();
			this.lblHeading.Text = aRow["whatsNewHeading"].ToString();


		} // END LoadData

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		public string GetImage(string fileName)
		{
			int intP = fileName.LastIndexOf('.');
			string ext = fileName.Substring(intP+1, 3).ToLower();
			Debug.WriteLine("Ext = " + ext);
			switch(ext)       
			{         
				case "doc":   
					return "images\\word16.gif";
				case "xls":            
					return "images\\excel16.gif";
				case "txt":            
					return "images\\text16.gif";
				case "pdf":            
					return "images\\acrobat16.gif";
				case "ppt":            
					return "images\\mspowerpoint16.gif";
				default:            
					return "images\\text16.gif";
			}

		}

		protected void btnView_Click(object sender, DataGridCommandEventArgs e)
		{
			int ID = Int32.Parse(dgFiles.DataKeys[e.Item.ItemIndex].ToString());

			DataTable dt = myUtility.SetTable("sp_GetWhatsNewFile", ID);
			if(dt.Rows.Count >0)
					this.ShowTheFile(dt.Rows[0]);
		} // END btnView_Click


		private void ShowTheFile(DataRow aRow)
		{
				string strFolder = "DocumentLibrary\\";
				string strFile = strFolder + (string)aRow["nameFile"];
				FileInfo myFile = new FileInfo(strFile);
				if(myFile.Exists)
					myFile.Delete();

				byte[] myData = (byte[])aRow["FileData"];
				Debug.WriteLine("Bytes = " + myData.Length.ToString());

				this.WriteToFile(strFile,(byte[])aRow["FileData"]);
				Response.Redirect(strFile);

		} // END ShowTheFile

		// Writes file to current folder
		private void WriteToFile(string strFileName, byte[] Buffer)
		{
			Debug.WriteLine("Impersonator ID : " + System.Security.Principal.WindowsIdentity.GetCurrent().Name);
			string myPath = Request.ServerVariables["APPL_PHYSICAL_PATH"];
			string strPath = myPath + strFileName;
			// Create a file
			Debug.WriteLine("File = " + strPath);
			FileStream newFile = new FileStream(strPath, FileMode.Create);

			// Write data to the file
			newFile.Write(Buffer, 0, Buffer.Length);

			// Close file
			newFile.Close();
		} // END WriteToFile

	} // END class
}
