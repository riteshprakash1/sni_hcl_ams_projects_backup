using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using Reimbursement.Classes;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for HospitalAscCalculator.
	/// </summary>
	public class HospitalAscCalculator : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlState;
		protected System.Web.UI.WebControls.CompareValidator vldState;
		protected System.Web.UI.WebControls.DropDownList ddlCity;
		protected System.Web.UI.WebControls.CompareValidator vldCity;
		protected System.Web.UI.WebControls.Button btnCalculate;
		protected System.Web.UI.WebControls.Button btnReset;
		protected System.Web.UI.WebControls.DataGrid dgCPT;
		protected System.Web.UI.WebControls.ValidationSummary vldSummary;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Label lblDisclaimer;
		protected System.Web.UI.WebControls.Label LabelRates;
		protected DataTable dtCPT;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("Login.aspx");

			if(!Page.IsPostBack)
			{
				
				this.SetControls(false);
				this.LabelRates.Text = myUtility.RateYear + " Rate Calculations";
				this.LabelRates.Visible=false;

				ddlState.Items.Clear();
				ddlState.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.StatesTable.Rows)
				{
					ddlState.Items.Add(new ListItem(aRow["state"].ToString(),aRow["st_code"].ToString() ));
				}
			} // END !Page.IsPostBack
		} // END Page_Load

		private void SetControls(bool blnShow)
		{

		} // END SetControls


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlState.SelectedIndexChanged += new System.EventHandler(this.ddlState_SelectedIndexChanged);
			this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ddlState_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			myUtility.CitiesTable = myUtility.SetTable("sp_GetUrbanCities", ddlState.SelectedValue);
			ddlCity.Items.Clear();
			ddlCity.Items.Add(new ListItem("Select","0"));
			foreach(DataRow aRow in myUtility.CitiesTable.Rows)
			{
				ddlCity.Items.Add(new ListItem(aRow["County"].ToString(),aRow["UrbanID"].ToString() ));
			}

			ddlCity.SelectedIndex = 0;
	
		} // END ddlState_SelectedIndexChanged


		private void btnCalculate_Click(object sender, System.EventArgs e)
		{
				Page.Validate();

				if(Page.IsValid)
				{
					if(myUtility.SqlConn.State != ConnectionState.Open)
						myUtility.SqlConn.Open();

					/****************************** Set Wage Index *****************************/
					DataTable dtUrbanWI = myUtility.SetTable("sp_GetUrbanIndex", Int32.Parse(ddlCity.SelectedValue));
					DataTable dtRuralWI = myUtility.SetTable("sp_GetRuralIndex", ddlState.SelectedValue);

					double dblUrbanWI = 0;
					double dblRuralWI = 0;
					double dblUrbanAscWI = 0;
					double dblRuralAscWI = 0;
					
					if(dtUrbanWI.Rows.Count > 0)
					{
						DataRow aRow = dtUrbanWI.Rows[0];
						dblUrbanWI = Double.Parse(aRow["WageIndex"].ToString());
						dblUrbanAscWI = Double.Parse(aRow["AscWI"].ToString());
					}

					if(dtRuralWI.Rows.Count > 0)
					{
						DataRow aRow = dtRuralWI.Rows[0];
						dblRuralWI = Double.Parse(aRow["WageIndex"].ToString());
						dblRuralAscWI = Double.Parse(aRow["AscWI"].ToString());
					}

			
					/****************************** END Set Wage Index *****************************/
					/****************************** Set Payment *****************************/
					AppSettingsReader configAppSet = new AppSettingsReader();
					double dblAscFactor = ((double)(configAppSet.GetValue("AscWIFactor", typeof(double))));
					double dblApcFactor = ((double)(configAppSet.GetValue("ApcWIFactor", typeof(double))));
				
					dtCPT = myUtility.SetTable("sp_GetAscApcPaymentsbyProduct", myUtility.ProductID);

					for(int x=0; x<dtCPT.Rows.Count; x++)
					{
						double dblApcRuralPymt = this.GetAmount(dtCPT.Rows[x]["ApcRuralPymt"].ToString());
						double dblApcUrbanPymt = this.GetAmount(dtCPT.Rows[x]["ApcUrbanPymt"].ToString());
						double dblAscRuralPymt = this.GetAmount(dtCPT.Rows[x]["AscRuralPymt"].ToString());
						double dblAscUrbanPymt = this.GetAmount(dtCPT.Rows[x]["AscUrbanPymt"].ToString());

						// the if else clause will set the value to zero if there is not a wage index for the urban or rural are
						if(dblRuralWI==0)
							dtCPT.Rows[x]["ApcRuralPymt"]= 0.00;
						else
							dtCPT.Rows[x]["ApcRuralPymt"]= ((dblApcRuralPymt  * dblApcFactor * dblRuralWI) + (dblApcRuralPymt * (1-dblApcFactor)));
						
						
						if(dblUrbanWI==0)
							dtCPT.Rows[x]["ApcUrbanPymt"]= 0.00;
						else
							dtCPT.Rows[x]["ApcUrbanPymt"]= ((dblApcUrbanPymt  * dblApcFactor * dblUrbanWI) + (dblApcUrbanPymt * (1-dblApcFactor)));

						
						if(dblRuralAscWI==0)
							dtCPT.Rows[x]["AscRuralPymt"]= 0.00;
						else
							dtCPT.Rows[x]["AscRuralPymt"]= ((dblAscRuralPymt  * dblAscFactor * dblRuralAscWI) + (dblAscRuralPymt * (1-dblAscFactor)));
						
						Debug.WriteLine("CPT_HCPCS: " + dtCPT.Rows[x]["CPT_HCPCS"].ToString() + "; dblAscRuralPymt: " + dblAscRuralPymt.ToString());
						Debug.WriteLine("dblAscFactor: " + dblAscFactor.ToString() + "; dblRuralAscWI: " + dblRuralAscWI.ToString());
						Debug.WriteLine("1-dblAscFactor: " + Convert.ToString(1-dblAscFactor) + "; AscRuralPymt: " + dtCPT.Rows[x]["AscRuralPymt"].ToString());
						
						if(dblUrbanAscWI==0)
							dtCPT.Rows[x]["AscUrbanPymt"]= 0.00;
						else
							dtCPT.Rows[x]["AscUrbanPymt"]= ((dblAscUrbanPymt  * dblAscFactor * dblUrbanAscWI) + (dblAscUrbanPymt * (1-dblAscFactor)));

						
						
						// Replaced with the code immediately above
//						dtCPT.Rows[x]["ApcRuralPymt"]= ((dblApcRuralPymt  * dblApcFactor * dblRuralWI) + (dblApcRuralPymt * (1-dblApcFactor)));
//						dtCPT.Rows[x]["ApcUrbanPymt"]= ((dblApcUrbanPymt  * dblApcFactor * dblUrbanWI) + (dblApcUrbanPymt * (1-dblApcFactor)));
//						dtCPT.Rows[x]["AscRuralPymt"]= ((dblAscRuralPymt  * dblAscFactor * dblRuralAscWI) + (dblAscRuralPymt * (1-dblAscFactor)));
//						dtCPT.Rows[x]["AscUrbanPymt"]= ((dblAscUrbanPymt  * dblAscFactor * dblUrbanAscWI) + (dblAscUrbanPymt * (1-dblAscFactor)));
					}

					/****************************** END Set Payment *****************************/
					Session["Utility"] = 	myUtility;
					myUtility.SqlConn.Close();


				} // END (Page.IsValid)

				this.BindCPT();

				if(Page.IsValid)
					this.SetControls(true);
		
			} // END btnCalculate_Click

		private double GetAmount(string strAmount)
		{
			double dblAmt = 0.00;
			if(strAmount != "")
			{
				try
				{
					dblAmt = Double.Parse(strAmount);
				}
				catch(Exception exc)
				{
					Debug.WriteLine("GetAmount Error: " + exc.Message);
					Debug.WriteLine("Could not Double.Parse: " + strAmount);
				}

			}
			return dblAmt;
		} // END GetAmount

		public void BindCPT()
		{
			dgCPT.DataSource = dtCPT;
			dgCPT.DataBind();

			this.LabelRates.Visible = Convert.ToBoolean(this.dgCPT.Items.Count > 0);
		} // END BindCPT


		private void btnReset_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("HospitalAscCalculator.aspx");
		}
	} // END class
}
