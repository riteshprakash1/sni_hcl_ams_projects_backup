<%@ Page language="c#" Codebehind="ForgotPassword.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.ForgotPassword" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>

<uc1:Header id="Header1" runat="server"></uc1:Header>

				<form id="Form2" method="post" runat="server">
					<div id="region-content">
						<uc1:titlenavigation id="Titlenavigation1" runat="server"></uc1:titlenavigation>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="20%"><asp:label id="lblEmail" runat="server" Font-Bold="True">Email Address:</asp:label></td>
								<td><asp:textbox id="txtEmail" runat="server" MaxLength="50" Width="250"></asp:textbox>
									<asp:RequiredFieldValidator id="vldRequiredEmail" runat="server" ErrorMessage="Email Address is required." ControlToValidate="txtEmail">*</asp:RequiredFieldValidator>
								</td>
								<td width="30%">&nbsp;</td>
							</tr>
							<tr>
								<TD><asp:label id="lblZip" runat="server" Font-Bold="True">Zip Code:</asp:label></TD>
								<td><asp:textbox id="txtZip" MaxLength="10" Width="250" runat="server"></asp:textbox>
									<asp:RequiredFieldValidator id="vldRequiredZip" runat="server" ErrorMessage="Zip Code is required." ControlToValidate="txtZip">*</asp:RequiredFieldValidator>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr height="40" valign="middle">
								<td colspan="2" align="center">
									<asp:button id="btnSubmit" runat="server" CausesValidation="False" Width="70px" Text="Submit"
										Height="21" ForeColor="White" BackColor="#79BDE9" Font-Size="10pt" BorderWidth="0px" Font-Names="Arial ,Helvetica,sans-serif"></asp:button>
								</td>
							</tr>
							<TR>
								<TD colspan="3">&nbsp;</TD>
							</TR>
							<TR>
								<TD colspan="3">
									<asp:Label id="lblMsg" runat="server" ForeColor="#FF7300"></asp:Label>
									<asp:ValidationSummary id="vldSummary" runat="server"></asp:ValidationSummary></TD>
							</TR>
						</TABLE>
					</div>
				</form>

<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
