<%@ Register TagPrefix="uc1" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleNavigation" Src="Controls/TitleNavigation.ascx" %>
<%@ Page language="c#" Codebehind="NewRegistration.aspx.cs" AutoEventWireup="false" Inherits="Reimbursement.NewRegistration" %>

<META content="text/html; charset=iso-8859-1" http-equiv=Content-Type>
<uc1:Header id="Header1" runat="server"></uc1:Header>
<form id="Form1" method="post" runat="server">
	<div id="region-content">
		<uc1:TitleNavigation id="TitleNavigation2" runat="server"></uc1:TitleNavigation>
		<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
			<tr vAlign="middle" height="60">
				<td colSpan="4"><asp:label id="lblTitle" runat="server" Font-Bold="True">
									Smith &amp; Nephew Reimbursement Web Site Registration</asp:label></td>
			</tr>
			<tr>
				<td width="13%"><asp:label id="lblocc" runat="server" Font-Bold="True">Occupation:</asp:label></td>
				<td width="28%"><asp:dropdownlist id="ddbocu" runat="server" Width="150" tabIndex="1"></asp:dropdownlist>
					<asp:comparevalidator id="vldOccupation" ForeColor="#FF7300" runat="server" ControlToValidate="ddbocu"
						ErrorMessage="Occupation must be selected." ValueToCompare="0" Operator="NotEqual">*</asp:comparevalidator></td>
				<td width="21%"><asp:label id="lblemail" runat="server" Font-Bold="True">Email Address:</asp:label></td>
				<td width="38%"><asp:textbox id="txtemail" runat="server" MaxLength="50" Width="250px" tabIndex="4"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator5" ForeColor="#FF7300" runat="server" Width="8px" ErrorMessage="Enter E-mail"
						ControlToValidate="txtemail">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator ForeColor="#FF7300" id="RegularExpressionValidator1" runat="server" Width="8px"
						ErrorMessage="Enter a valid email" ControlToValidate="txtemail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:regularexpressionvalidator>
					<asp:CustomValidator id="vldExistEmail" ForeColor="#FF7300" runat="server" ControlToValidate="txtemail"
						ErrorMessage="The email address already exists.">*</asp:CustomValidator></td>
			</tr>
			<tr>
				<td><asp:label id="lblzip" runat="server" Font-Bold="True">Zip Code:</asp:label>&nbsp;
					<A class="hintanchor" onmouseover="showhint('Enter 99999 if outside of the US', this, event, '200px', 0, 30)"
						href="#">[?]</A>
				</td>
				<td><asp:textbox id="txtzip" runat="server" Width="150" tabIndex="2"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator4" ForeColor="#FF7300" runat="server" Width="8px" ErrorMessage="Enter Zip Code"
						ControlToValidate="txtzip">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator2" ForeColor="#FF7300" runat="server" Width="8px"
						ErrorMessage="Enter a valid zip code" ControlToValidate="txtzip" ValidationExpression="\d{5}(-\d{4})?">*</asp:regularexpressionvalidator></td>
				<td><asp:label id="lblConfirmEmail" runat="server" Font-Bold="True">Confirm Email Address:</asp:label></td>
				<td><asp:textbox id="txtConfirmEmail" runat="server" Width="250px" tabIndex="5"></asp:textbox>
					<asp:requiredfieldvalidator id="vldRequiredConfirmEmail" ForeColor="#FF7300" runat="server" Width="8px" ControlToValidate="txtConfirmEmail"
						ErrorMessage="Enter Confirm Email">*</asp:requiredfieldvalidator>
					<asp:CustomValidator id="Customvalidator1" runat="server" ErrorMessage="Email Address mismatch." ControlToValidate="txtConfirmEmail">*</asp:CustomValidator></td>
			</tr>
			<tr>
				<td><asp:label id="lblspe" runat="server" Font-Bold="True">Specialty:</asp:label></td>
				<td><asp:dropdownlist id="ddbspe" runat="server" Width="150" tabIndex="3"></asp:dropdownlist>
					<asp:comparevalidator id="vldSpecialty" ForeColor="#FF7300" runat="server" ControlToValidate="ddbspe"
						ErrorMessage="Specialty must be selected." ValueToCompare="0" Operator="NotEqual">*</asp:comparevalidator></td>
				<td><asp:label id="lblpassword" runat="server" Font-Bold="True">Password:</asp:label></td>
				<td><asp:textbox id="txtpass" runat="server" MaxLength="50" Width="150" TextMode="Password" tabIndex="6"></asp:textbox>
					<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ForeColor="#FF7300" Width="8px" ErrorMessage="Enter Password"
						ControlToValidate="txtpass">*</asp:requiredfieldvalidator>
					<asp:CustomValidator id="vldPassWordLen" runat="server" ForeColor="#FF7300" ControlToValidate="txtpass"
						ErrorMessage="The length of the password must be between 4 and 10 characters long. ">*</asp:CustomValidator></td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:checkbox id="chkSubscribe" runat="server" Font-Bold="True" Text="Subscribe to email updates"></asp:checkbox></td>
				<TD>
					<asp:label id="lblconfirm" runat="server" Font-Bold="True">Confirm Password:</asp:label></TD>
				<TD>
					<asp:textbox id="txtconfirm" tabIndex="7" runat="server" Width="150" TextMode="Password"></asp:textbox>
					<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Confirm Password"
						ControlToValidate="txtconfirm" ForeColor="#FF7300">*</asp:requiredfieldvalidator>
					<asp:comparevalidator id="CompareValidator1" runat="server" Width="8px" ErrorMessage="Password Mismatch"
						ControlToValidate="txtconfirm" ForeColor="#FF7300" ControlToCompare="txtpass">*</asp:comparevalidator></TD>
			</tr>
			<tr vAlign="bottom" height="40">
				<td colSpan="4"><STRONG>If you would like to subscribe to email updates of the<br>
				"What's New" posting, please click the checkbox above:</STRONG></td>
			</tr>
			<TR>
				<td colSpan="4">&nbsp;</td>
			</TR>
			<tr height="30">
				<td align="right" colSpan="2" valign="top" width="394">
					<asp:button id="btnRegister" runat="server" Font-Size="10pt" Width="70px" Height="22" BackColor="#79BDE9"
						CausesValidation="False" ForeColor="White" Font-Names="Arial ,Helvetica,sans-serif" BorderWidth="0px"
						Text="Register"></asp:button></td>
				<td colspan="2" vAlign="top" align="left"><asp:validationsummary id="ValidationSummary1" runat="server" ForeColor="#FF7300"></asp:validationsummary></td>
			</tr>
			<tr>
				<td colspan="4">
					<asp:label id="lblmsg" runat="server" Font-Bold="True" Width="392px"></asp:label>
				</td>
			</tr>
		</table>
		<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="1">
			<tr>
				<td>
					<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
						<tr height="50" valign="bottom">
							<td colSpan="4"><asp:radiobuttonlist id="rbDisclaimer" runat="server" RepeatDirection="Vertical">
									<asp:ListItem Value="1">I &lt;b&gt;accept&lt;/b&gt; the conditions set forth in the disclaimer below.</asp:ListItem>
									<asp:ListItem Value="0">I &lt;b&gt;reject&lt;/b&gt; the conditions set forth in the disclaimer below.</asp:ListItem>
								</asp:radiobuttonlist><asp:requiredfieldvalidator id="vldDiscalimer" runat="server" ErrorMessage="You must select accept or reject."
									ControlToValidate="rbDisclaimer">*</asp:requiredfieldvalidator>
								<asp:CompareValidator id="vldRejectDisclaimer" runat="server" ControlToValidate="rbDisclaimer" ErrorMessage="If you do not accept the Terms of Use Disclaimer, you will not be registered."
									Operator="NotEqual" ValueToCompare="0">*</asp:CompareValidator></td>
						</tr>
						<tr>
							<td colSpan="4" width="95%">
								<asp:label id="lblDisclaimer" runat="server" Font-Name="Arial">
									<p>Smith &amp; Nephew is committed to protecting the security of your user 
										information.&nbsp;&nbsp;We use a variety of security technologies and 
										procedures to help protect your user information from unauthorized access, use, 
										or disclosure.&nbsp;&nbsp;For example, we store the user information you 
										provide on computer systems with limited access, which are located in 
										controlled facilities.
									</p>
									<p><br>
									</p>
									<p><b>Disclaimer</b></p>
									<p>The information provided with this notice is general reimbursement information 
										only; it is not legal advice, nor is it advice about how to code, complete or 
										submit any particular claim for payment.&nbsp;&nbsp;Although we supply this 
										information to the best of our current knowledge, it is always the provider's 
										responsibility to determine and submit appropriate codes, charges, modifiers 
										and bills for services that were rendered.&nbsp;&nbsp;The coding and 
										reimbursement information is subject to change without 
										notice.&nbsp;&nbsp;Payers or their local branches may have their own coding and 
										reimbursement requirements and policies.&nbsp;&nbsp;Before filing any claims, 
										providers should verify current requirements and policies with the payer.
									</p>
									<p><br>
									</p>
									<p><b>Terms of Use Disclaimer</b></p>
									<p>The information provided on this website is general reimbursement information, 
										intended for the providers' reference and convenience.&nbsp;&nbsp;This 
										information is not legal advice, nor is it advice about how to code, complete 
										or submit any particular claim for payment.&nbsp;&nbsp;It is always the 
										provider's responsibility to determine and submit appropriate codes, charges, 
										modifiers, and bills for the services that were rendered.&nbsp;&nbsp;Coding and 
										reimbursement information is subject to change without notice.&nbsp;&nbsp; 
										Payers or their local branches may have their own coding and reimbursement 
										requirements and policies.&nbsp;&nbsp;Before filing any claims, providers 
										should verify current requirements and policies to the payer.
									</p>
									<p><br>
									</p>
									<p>Smith &amp; Nephew will collect and use your information to operate and improve 
										its sites and deliver the services or carry out the transactions you have 
										requested.&nbsp;&nbsp; These uses may include providing you with more effective 
										customer service; making the sites or services easier to use by eliminating the 
										need for you to repeatedly enter the same information, and performing research 
										and analysis aimed at improving our products, services and technologies.
									</p>
									<p><br>
									</p>
									<p><b>Accessing User Information</b></p>
									<p>You may have the ability to view or edit your user information 
										online.&nbsp;&nbsp;In order to help prevent your user information from being 
										viewed by others, you will be required to sign in with your credentials (e-mail 
										address and password).&nbsp;&nbsp;The appropriate method(s) for accessing your 
										user information will depend on which sites or services you have used.<br>
										<br>
									</p>
									<p>If a password is used to help protect your accounts and user information, it is 
										your responsibility to keep your password confidential.&nbsp;&nbsp;Do not share 
										this information with anyone.&nbsp;&nbsp;If you are sharing a computer with 
										anyone you should always choose to log out before leaving a site or service to 
										protect access to your information from subsequent users.<br>
										<br>
									</p>
									<p>Smith &amp; Nephew sites use "cookies" to enable you to sign in to our services 
										and to help personalize your online experience.&nbsp;&nbsp;A cookie is a small 
										text file that is placed on your hard disk by a Web page 
										server.&nbsp;&nbsp;Cookies contain information that can later be read by a web 
										server in the domain that issued the cookie to you.&nbsp;&nbsp; Cookies cannot 
										be used to run programs or deliver viruses to your computer.&nbsp;&nbsp;The 
										cookie functionality is only used at your request by clicking on the "Remember 
										Me" checkbox found on the login page and Update Registration page.<br>
										<br>
									</p>
								</asp:label>
							</td>
						</tr>
					</TABLE>
				</td>
			</tr>
		</table>
	</div>
</form>
<uc1:Footer id="Footer1" runat="server"></uc1:Footer>