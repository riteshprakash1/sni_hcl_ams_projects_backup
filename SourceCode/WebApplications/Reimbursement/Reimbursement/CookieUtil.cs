using System;
using System.Web;
using System.Diagnostics;

namespace Reimbursement
{
	/// <summary>
	/// Summary description for CookieUtil.
	/// </summary>
	public class CookieUtil
	{
		public CookieUtil()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void SetTripleDESEncryptedCookie(string key, string value) 
		{ 
			key = CryptoUtil.EncryptTripleDES(key); 
			value = CryptoUtil.EncryptTripleDES(value); 
			SetCookie(key, value); 
		} 

		public static void SetTripleDESEncryptedCookie(string key, string value, System.DateTime expires) 
		{ 
			key = CryptoUtil.EncryptTripleDES(key); 
			value = CryptoUtil.EncryptTripleDES(value); 
			SetCookie(key, value, expires); 
		} 

		public static void SetEncryptedCookie(string key, string value) 
		{ 
			key = CryptoUtil.Encrypt(key); 
			value = CryptoUtil.Encrypt(value); 
			SetCookie(key, value); 
		} 

		public static void SetEncryptedCookie(string key, string value, System.DateTime expires) 
		{ 
			key = CryptoUtil.Encrypt(key); 
			value = CryptoUtil.Encrypt(value); 
			SetCookie(key, value, expires); 
		} 

		public static void SetCookie(string key, string value) 
		{ 
			key = HttpContext.Current.Server.UrlEncode(key); 
			value = HttpContext.Current.Server.UrlEncode(value); 
			HttpCookie cookie; 
			cookie = new HttpCookie(key, value); 
			SetCookie(cookie); 
		} 

		public static void SetCookie(string key, string value, System.DateTime expires) 
		{ 
			key = HttpContext.Current.Server.UrlEncode(key); 
			value = HttpContext.Current.Server.UrlEncode(value); 
			HttpCookie cookie; 
			cookie = new HttpCookie(key, value); 
			cookie.Expires = expires; 
			SetCookie(cookie); 
		} 

		public static void SetCookie(HttpCookie cookie) 
		{ 
			HttpContext.Current.Response.Cookies.Set(cookie); 
		} 

		public static string GetTripleDESEncryptedCookieValue(string key) 
		{ 
			key = CryptoUtil.EncryptTripleDES(key); 
			string value; 
			value = GetCookieValue(key); 
			value = CryptoUtil.DecryptTripleDES(value); 
			return value; 
		} 

		public static string GetEncryptedCookieValue(string key) 
		{ 
			key = CryptoUtil.Encrypt(key); 
			string value; 
			value = GetCookieValue(key); 
			value = CryptoUtil.Decrypt(value); 
			return value; 
		} 

		public static HttpCookie GetCookie(string key) 
		{ 
			key = HttpContext.Current.Server.UrlEncode(key); 
			return HttpContext.Current.Request.Cookies.Get(key); 
		} 

		public static string GetCookieValue(string key) 
		{ 
			string strVal = ""; 
			try 
			{ 
				strVal = GetCookie(key).Value; 
				strVal = HttpContext.Current.Server.UrlDecode(strVal); 
			} 
			catch(Exception ex)
			{ 
				Debug.WriteLine("GetCookieValue error: " + ex.Message);
			} 
			return strVal; 

		}  // END GetCookieValue



	} // END class
}
