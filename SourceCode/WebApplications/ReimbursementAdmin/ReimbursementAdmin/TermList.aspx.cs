using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for TermList.
	/// </summary>
	public partial class TermList : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				myUtility.TermsTable = myUtility.SetTable("sp_GetTerms");
				sqlConnection1.Close();

				Debug.WriteLine("Rows = " + myUtility.TermsTable.Rows.Count.ToString());
				if(myUtility.TermsTable.Rows.Count > 0)
					this.BindDT();

				this.SetControls();
			}
		
		}


		private void SetControls()
		{
			bool blnVisible = Convert.ToBoolean(myUtility.TermsTable.Rows.Count > 0);
			Debug.WriteLine("bool = " + blnVisible.ToString());
			dgTerm.Visible = blnVisible;
			btnFirst.Visible = blnVisible;
			btnPrev.Visible = blnVisible;
			ddlPage.Visible = blnVisible;
			lblPageCnt.Visible = blnVisible;
			btnNext.Visible = blnVisible;
			btnLast.Visible = blnVisible;
			lblRecordCount.Visible = blnVisible;
		
		} // END SetControls


		public void BindDT()
		{
			try
			{
				dgTerm.DataSource = myUtility.TermsTable;
				dgTerm.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Exception: " + ex.Message);
				dgTerm.CurrentPageIndex = 0;
			}

			if(dgTerm.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgTerm.CurrentPageIndex != (dgTerm.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}


			lblRecordCount.Text = "<b><font color=red>" + Convert.ToString(myUtility.TermsTable.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgTerm.PageCount.ToString();

			ddlPage.Items.Clear();
			for(int x=1;x<dgTerm.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			
			int myPage=dgTerm.CurrentPageIndex+1;
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

		} // END BindDT

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgTerm.CurrentPageIndex = 0;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgTerm.CurrentPageIndex >= 1)
				dgTerm.CurrentPageIndex = dgTerm.CurrentPageIndex - 1;
			else
				dgTerm.CurrentPageIndex = 0;

			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgTerm.CurrentPageIndex != (dgTerm.PageCount-1))
				dgTerm.CurrentPageIndex = dgTerm.CurrentPageIndex + 1;
			else
				dgTerm.CurrentPageIndex = (dgTerm.PageCount-1);

			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgTerm.CurrentPageIndex = (dgTerm.PageCount-1);
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgTerm.CurrentPageIndex = ddlPage.SelectedIndex;
			this.BindDT();
		}

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{

			myUtility.TermID = Int32.Parse(dgTerm.DataKeys[e.Item.ItemIndex].ToString());
			Session["Utility"] = myUtility;
			Response.Redirect("updateTerm.aspx");
		} // END btnEdit_Click


		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteTerm";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@termID", Int32.Parse(dgTerm.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.TermsTable = myUtility.SetTable("sp_GetTerms");
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnNewTerm_Click(object sender, System.EventArgs e)
		{
			myUtility.TermID = 0;
			Session["Utility"] = myUtility;
			Response.Redirect("UpdateTerm.aspx");
		} // END btnNewTerm_Click
	} // END class
}
