using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for WhatsNewList.
	/// </summary>
	public partial class WhatsNewList : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Image Image1;
		protected DataTable dtWhatsNew;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				dtWhatsNew = myUtility.SetTable("sp_GetWhatsNewList");
				sqlConnection1.Close();

				Debug.WriteLine("Rows = " + dtWhatsNew.Rows.Count.ToString());
				if(dtWhatsNew.Rows.Count > 0)
					this.BindDT();

			}		
		} // END Page_Load

		public void BindDT()
		{
			dgWhatsNew.DataSource = dtWhatsNew;
			dgWhatsNew.DataBind();
		}

		public string GetImage(string fileName)
		{
			int intP = fileName.LastIndexOf('.');
			string ext = fileName.Substring(intP+1, 3).ToLower();
			Debug.WriteLine("Ext = " + ext);
			switch(ext)       
			{         
				case "doc":   
					return "images\\word16.gif";
				case "xls":            
					return "images\\excel16.gif";
				case "txt":            
					return "images\\text16.gif";
				case "pdf":            
					return "images\\acrobat16.gif";
				case "ppt":            
					return "images\\mspowerpoint16.gif";
				default:            
					return "images\\text16.gif";
			}

		}

		protected void btnNew_Click(object sender, System.EventArgs e)
		{
			myUtility.WhatsNewID = 0;
			Session["Utility"] = myUtility;
			Response.Redirect("WhatsNewDetail.aspx");
		} // END btnNew_Click

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			myUtility.WhatsNewID = Int32.Parse(dgWhatsNew.DataKeys[e.Item.ItemIndex].ToString());
			Session["Utility"] = myUtility;
			Response.Redirect("WhatsNewDetail.aspx");
		} // END btnEdit_Click


		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteWhatsNew";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@whatsNewID", Int32.Parse(dgWhatsNew.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			dtWhatsNew = myUtility.SetTable("sp_GetWhatsNewList");
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


	}
}
