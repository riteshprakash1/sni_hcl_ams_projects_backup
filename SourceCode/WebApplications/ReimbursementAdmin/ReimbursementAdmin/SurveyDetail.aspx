<%@ Page language="c#" Codebehind="SurveyDetail.aspx.cs" AutoEventWireup="True" validateRequest="false" Inherits="ReimbursementAdmin.SurveyDetail" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Survey Detail</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="global.css">
	</HEAD>
	<BODY>
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td width="760">
						<table width="760" align="center">
							<tr>
								<td width="12%"><asp:label id="LabelSurveyID" runat="server" text="Survey ID" Font-Bold="True"></asp:label></td>
								<td><asp:label id="lblSurveyID" runat="server"></asp:label></td>
							</tr>
							<tr>
								<td><b>Status:</b></td>
								<td><asp:dropdownlist id="ddlStatus" runat="server" Width="150px">
										<asp:ListItem Value="Not Started">Not Started</asp:ListItem>
										<asp:ListItem Value="In Progress">In Progress</asp:ListItem>
										<asp:ListItem Value="Completed">Completed</asp:ListItem>
									</asp:dropdownlist>
									<asp:CustomValidator id="vldStatus" runat="server" ControlToValidate="ddlStatus" ErrorMessage="Only one survey by product can be set to 'In Progress'.">*</asp:CustomValidator></td>
							</tr>
							<tr>
								<td><b>Name:&nbsp; *</b></td>
								<td><asp:textbox id="txtSurveyName" runat="server" Width="600px" MaxLength="75"></asp:textbox><asp:requiredfieldvalidator id="vldSurveyName" runat="server" ControlToValidate="txtSurveyName" ErrorMessage="Survey Name is a required field.">*</asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td><b>Survey URL: *</b></td>
								<td><asp:textbox id="txtSurveyURL" runat="server" Width="600px" MaxLength="100"></asp:textbox><asp:requiredfieldvalidator id="vldRequiredSurveyURL" runat="server" ControlToValidate="txtSurveyURL" ErrorMessage="Survey URL is a required field.">*</asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td vAlign="top"><b>Message: *</b></td>
								<td><asp:textbox id="txtSurveyMessage" runat="server" Width="600px" TextMode="MultiLine" Rows="5"></asp:textbox><asp:requiredfieldvalidator id="vldRequiredSurveyMessage" runat="server" ControlToValidate="txtSurveyMessage"
										ErrorMessage="Survey Message is a required field.">*</asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td vAlign="top"><b>Description:</b></td>
								<td><asp:textbox id="txtSurveyDescription" runat="server" Width="600px" TextMode="MultiLine" Rows="5"></asp:textbox></td>
							</tr>
							<tr>
								<td><b>Product:&nbsp;&nbsp;</b></td>
								<td><asp:dropdownlist id="ddlProduct" runat="server" Width="200px"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td vAlign="top"><b>Start Date:</b></td>
								<td><asp:textbox id="txtStartDate" runat="server" Width="120px"></asp:textbox><asp:requiredfieldvalidator id="vldRequiredStartDate" runat="server" ControlToValidate="txtStartDate" ErrorMessage="Start Date is a required field.">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="vldStartDate" runat="server" ControlToValidate="txtStartDate" ErrorMessage="Start Date must be a valid date (mm/dd/yyyy)."
										ValidationExpression="(1[0-2]|0?[1-9])[\-\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})">*</asp:regularexpressionvalidator></td>
							</tr>
							<tr>
								<td vAlign="top"><b>End Date:</b></td>
								<td><asp:textbox id="txtEndDate" runat="server" Width="120px"></asp:textbox><asp:requiredfieldvalidator id="vldRequiredEndDate" runat="server" ControlToValidate="txtEndDate" ErrorMessage="End Date is a required field.">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="vldEndDate" runat="server" ControlToValidate="txtEndDate" ErrorMessage="End Date must be a valid date (mm/dd/yyyy)."
										ValidationExpression="(1[0-2]|0?[1-9])[\-\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})">*</asp:regularexpressionvalidator></td>
							</tr>
							<tr>
								<td colSpan="2">* - indicates required field</td>
							</tr>
							<tr>
								<td colSpan="2" align="center"><asp:button id="btnSubmit" runat="server" Width="100px" Height="18" Text="Submit" Font-Size="8pt"
										ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnSubmit_Click"></asp:button></td>
							</tr>
							<tr>
								<td colSpan="2" align="center"><asp:label id="lblStatusMessage" runat="server" ForeColor="#FF7300"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="2"><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td colSpan="3">
									<hr color="#666666">
								</td>
							</tr>
							<tr>
								<td colSpan="3"><asp:label id="lblDataGridMessage" runat="server" ForeColor="#FF7300"></asp:label><asp:datagrid id="dgSurveyResponses" runat="server" PagerStyle-Visible="False" AllowPaging="false"
										AutoGenerateColumns="False" Width="750px" CellPadding="5" DataKeyField="SurveyResponseID">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="UserID" HeaderStyle-Width="7%" HeaderText="UserID"></asp:BoundColumn>
											<asp:BoundColumn DataField="EmailAddress" HeaderStyle-Width="29%" HeaderText="User Email Address"></asp:BoundColumn>
											<asp:BoundColumn DataField="UserResponse" HeaderStyle-Width="11%" HeaderText="Response"></asp:BoundColumn>
											<asp:BoundColumn DataField="ResponseDate" HeaderStyle-Width="18%" HeaderText="Response Date"></asp:BoundColumn>
											<asp:BoundColumn DataField="Specialty_Name" HeaderStyle-Width="12%" HeaderText="Specialty"></asp:BoundColumn>
											<asp:BoundColumn DataField="Occupation_Name" HeaderStyle-Width="17%" HeaderText="Occupation"></asp:BoundColumn>
											<asp:BoundColumn DataField="ZipCode" HeaderStyle-Width="6%" HeaderText="Zip Code"></asp:BoundColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
