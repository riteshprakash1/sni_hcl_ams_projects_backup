using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace ReimbursementAdmin
{
	/// <summary>Summary description for SurveyList.</summary>
	public partial class SurveyList : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected System.Data.DataTable tblSurveys;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			this.lblMessage.Text = string.Empty;
			
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				this.tblSurveys = myUtility.SetTable("sp_GetSurveys");
				sqlConnection1.Close();

				this.BindDT();
			}		
		} // END Page_Load


		public void BindDT()
		{
			dgSurveys.DataSource = this.tblSurveys;
			dgSurveys.DataBind();

			bool isVisible = Convert.ToBoolean(this.tblSurveys.Rows.Count != 0);
			dgSurveys.Visible = isVisible;

			if(!isVisible)
				this.lblMessage.Text = "There are no surveys in the database table.";
		}

		public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			Button btn;
			if(e.Item.ItemType==ListItemType.Item || e.Item.ItemType==ListItemType.AlternatingItem)
			{
				btn = (Button)e.Item.FindControl("btnDelete");
				btn.Attributes.Add("onclick", "return confirm_delete();");
			}
		} // END DG_ItemDataBound

		protected void btnNew_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("SurveyDetail.aspx?SurveyID=0", true);
		} // END btnNew_Click

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			string strSurveyID = dgSurveys.DataKeys[e.Item.ItemIndex].ToString();
			Response.Redirect("SurveyDetail.aspx?SurveyID="+strSurveyID, true);
		} // END btnEdit_Click


		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteSurveyBySurveyID";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@SurveyID", Int32.Parse(dgSurveys.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			this.tblSurveys = myUtility.SetTable("sp_GetSurveys");
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
