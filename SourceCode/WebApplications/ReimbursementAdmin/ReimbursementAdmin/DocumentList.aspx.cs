using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for DocumentList.
	/// </summary>
	public partial class DocumentList : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				myUtility.DocumentFilesTable = myUtility.SetTable("sp_GetNewDocumentList");
				sqlConnection1.Close();
				if(	myUtility.DocumentFilesTable.Rows.Count > 0)
					this.BindDT();
			}		
		} // END Page_Load


		public void BindDT()
		{
			dgDocuments.DataSource = myUtility.DocumentFilesTable;
		    dgDocuments.DataBind();

			bool isVisible = Convert.ToBoolean(myUtility.DocumentFilesTable.Rows.Count != 0);
			dgDocuments.Visible = isVisible;
		}

		public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			Button btn;
			if(e.Item.ItemType==ListItemType.Item || e.Item.ItemType==ListItemType.AlternatingItem)
			{
				btn = (Button)e.Item.FindControl("btnDelete");
				btn.Attributes.Add("onclick", "return confirm_delete();");
			}
		} // END DG_ItemDataBound


		public string GetImage(string fileName)
		{
			int intP =fileName.LastIndexOf('.');
			string ext = fileName.Substring(intP+1, 3).ToLower();
			Debug.WriteLine("Ext = " + ext);
			switch(ext)       
			{         
				case "doc":   
					return "images\\word16.gif";
				case "xls":            
					return "images\\excel16.gif";
				case "txt":            
					return "images\\text16.gif";
				case "pdf":            
					return "images\\acrobat16.gif";
				case "ppt":            
					return "images\\mspowerpoint16.gif";
				default:            
					return "images\\text16.gif";
			}

		}

		private void ShowTheFile(DataRow aRow)
		{
			string strFolder = "DocumentLibrary\\";
			string strFile = strFolder + (string)aRow["FileType"];
			FileInfo myFile = new FileInfo(strFile);
			if(myFile.Exists)
				myFile.Delete();

			byte[] myData = (byte[])aRow["FileData"];
			Debug.WriteLine("Bytes = " + myData.Length.ToString());

			this.WriteToFile(strFile,(byte[])aRow["FileData"]);
			Response.Redirect(strFile);

		} // END ShowTheFile

		private void WriteToFile(string strFileName, byte[] Buffer)
		{
			Debug.WriteLine("Impersonator ID : " + System.Security.Principal.WindowsIdentity.GetCurrent().Name);
			string myPath = Request.ServerVariables["APPL_PHYSICAL_PATH"];
			string strPath = myPath + strFileName;
			// Create a file
			Debug.WriteLine("File = " + strPath);
			FileStream newFile = new FileStream(strPath, FileMode.Create);

			// Write data to the file
			newFile.Write(Buffer, 0, Buffer.Length);

			// Close file
			newFile.Close();
		} // END WriteToFile


		protected void btnNew_Click(object sender, System.EventArgs e)
		{
			myUtility.DocumentID = 0;
			Session["Utility"] = myUtility;
			Response.Redirect("ReimbursementDocuments.aspx");
		} // END btnNew_Click

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			myUtility.DocumentID = Int32.Parse(dgDocuments.DataKeys[e.Item.ItemIndex].ToString());
			Session["Utility"] = myUtility;
			Response.Redirect("ReimbursementDocuments.aspx");
		} // END btnEdit_Click


		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteNewDocument";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@DocumentID", Int32.Parse(dgDocuments.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.DocumentFilesTable = myUtility.SetTable("sp_GetNewDocumentList");
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

	} // END btnDelete_Click
			
			
					#region Web Form Designer generated code
					override protected void OnInit(EventArgs e)
					{
						//
						// CODEGEN: This call is required by the ASP.NET Web Form Designer.
						//
						InitializeComponent();
						base.OnInit(e);
		
	}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


	} // END class
}
