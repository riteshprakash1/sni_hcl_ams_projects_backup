<html>
<body>
<%
	sub dbgprint( msg )
		response.write(msg)
	end sub

	function showvar( var_label, var_value)
		response.write("<tr><th>" & var_label & "<td>")
		On Error Resume Next
		temp = var_value

		Y = UBound(temp,1)
		if Err.Number <> 0 Then
			if var_label = "PLAN_CART" OR var_label = "CREDIT_CHECK" OR var_label = "SILKOBJ_DICT" Then
				response.write("<table border=1>")
				For Each obj In Session(var_label)
					Response.Write("<TR><TD>" & obj & "</TD><TD>" & Session(var_label).Item(obj) & "</TD></TR>")
				Next
				Response.Write("</TABLE>")
			else
				response.write(temp)
			End If
			exit function
		else
'			Response.Write("var_label = " & var_label)
			if StrComp(var_label,"Categories",1) = 0 Then
				response.write("<table border=1>")
				For Each obj In Application(var_label)
'					Response.Write("ObjName = " & TypeName(obj))
					Response.Write("<TR><TD>Name</TD><TD>" & obj.Name & "</TD></TR>")
					Response.Write("<TR><TD>Description</TD><TD>" & obj.Description & "</TD></TR>")
					Response.Write("<TR><TD>NumForums</TD><TD>" & obj.NumForums & "</TD></TR>")
				Next
				Response.Write("</TABLE>")
			end if
		end if

		response.write("<table border=1>")

		Y = UBound(temp,2)
		if Err.Number <> 0 Then
			for Y = 0 to UBound(temp,1)
				response.write("<tr><td>" & temp(Y) )
			next 'Y
		else
			for X = 0 to UBound( temp,1)
			response.write("<tr>")
			for Y = 0 to UBound( temp,2)
				response.write("<td>" & temp(X,Y) )
			next 'Y
			next 'X

		end if
		response.write("</table>")


	end function
%>
<h1>Session Variables</h1>
<table border=1 cellpadding=5>
<%
	function ShowSessionVar( var_label )
		ShowVar var_label, session(var_label)
	end function

        for each i in Session.Contents
           ShowSessionVar (i)
        next 'i
%>
</table>
<h1>Application Variables</h1>
<table border=1 cellpadding=5>
<%

	function ShowAppVar( var_label )
		ShowVar var_label, application(var_label)
	end function

        for each i in Application.Contents
           ShowAppVar (i)
        next 'i
%>
</table>
<h1>Request Variables</h1>
<%
	function ShowRequestVar( var_label )
		ShowVar var_label, request(var_label)
	end function
%>
<h3>ServerVariables</h3>
<table border=1 cellpadding=5>
<%
        for each i in Request.ServerVariables
           ShowRequestVar(i)
        next 'i
%>
</table>
<h3>Form</h3>
<table border=1 cellpadding=5>
<%
        for each i in Request.Form
           ShowRequestVar(i)
        next 'i
%>
</table>
<h3>QueryString</h3>
<table border=1 cellpadding=5>
<%
        for each i in Request.QueryString
           ShowRequestVar(i)
        next 'i
%>
</table>
<h3>Cookies</h3>
<table border=1 cellpadding=5>
<%
        for each i in Request.Cookies
           ShowRequestVar(i)
        next 'i
%>
</table>
</body>
</html>
