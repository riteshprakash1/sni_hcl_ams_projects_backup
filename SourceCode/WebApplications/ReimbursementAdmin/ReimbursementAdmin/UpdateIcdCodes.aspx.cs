using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateIcdCodes.
	/// </summary>
	public partial class UpdateIcdCodes : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("UpdateIcdCodes Page_Load");
			
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				EnterButton.TieButton(txtFindIcd, btnFind);

				sqlConnection1.Open();
				myUtility.ICDTable = myUtility.SetTable("sp_GetIcdCodes");
				sqlConnection1.Close();
				Session["Utility"] = myUtility;
				this.BindDT();

			}
			
		} // END Page_Load


		private void SetControls()
		{
			if(myUtility.ICDTable.Rows.Count == 0)
			{
				dgICD.Visible=false;
				btnFirst.Visible=false;
				btnPrev.Visible=false;
				ddlPage.Visible=false;
				lblPageCnt.Visible=false;;
				btnNext.Visible=false;
				btnLast.Visible=false;
				lblRecordCount.Visible=false;
			}
			else
			{
				dgICD.Visible=true;
				btnFirst.Visible=true;
				btnPrev.Visible=true;
				ddlPage.Visible=true;
				lblPageCnt.Visible=true;;
				btnNext.Visible=true;
				btnLast.Visible=true;
				lblRecordCount.Visible=true;
			}
		
		} // END SetControls


		public void BindDT()
		{
			try
			{
				dgICD.DataSource = myUtility.ICDTable;
				dgICD.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Exception: " + ex.Message);
				dgICD.CurrentPageIndex = 0;
			}

			if(dgICD.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgICD.CurrentPageIndex != (dgICD.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}


			lblRecordCount.Text = "<b><font color=red>" + Convert.ToString(myUtility.ICDTable.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgICD.PageCount.ToString();
			ddlPage.Items.Clear();
			for(int x=1;x<dgICD.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			int myPage=dgICD.CurrentPageIndex+1;
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

		} // END BindDT

		public int GetIcdTypeIndex(string strType)
		{
			if(strType == "Diagnosis")
				return 0;
			else
				return 1;
		} // END GetIcdTypeIndex

		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteIcdCode";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@ID", Int32.Parse(dgICD.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.ICDTable = myUtility.SetTable("sp_GetIcdCodes");
			sqlConnection1.Close();

			dgICD.EditItemIndex = -1;
			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click

		protected void btnUpdate_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("Add - "))
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				Label lblIcd = (Label)e.Item.FindControl("lblIcd");
				DropDownList ddlIcdType = (DropDownList)e.Item.FindControl("ddlIcdType");
				TextBox txtIcdDesc = (TextBox)e.Item.FindControl("txtIcdDesc");
				
				sqlConnection1.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_UpdateIcdCode";
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@icdCode", lblIcd.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@Descr", txtIcdDesc.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@icdType", ddlIcdType.SelectedValue));
				cmdInsert.Parameters.Add(new SqlParameter("@icdCodeID", Int32.Parse(dgICD.DataKeys[e.Item.ItemIndex].ToString())));

				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				myUtility.ICDTable = myUtility.SetTable("sp_GetIcdCodes");
				sqlConnection1.Close();

				dgICD.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid) 
		} // END btnUpdate_Click

		protected void btnAdd_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(!val.ErrorMessage.StartsWith("Add - "))
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				TextBox txtAddIcd = (TextBox)e.Item.FindControl("txtAddIcd");
				DropDownList ddlAddIcdType = (DropDownList)e.Item.FindControl("ddlAddIcdType");
				TextBox txtAddIcdDesc = (TextBox)e.Item.FindControl("txtAddIcdDesc");
				
				sqlConnection1.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertIcdCode";
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@icdCode", txtAddIcd.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@Descr", txtAddIcdDesc.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@icdType", ddlAddIcdType.SelectedValue));

				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				myUtility.ICDTable = myUtility.SetTable("sp_GetIcdCodes");
				sqlConnection1.Close();

				dgICD.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid) 
		} // END btnAdd_Click

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			Debug.WriteLine("ItemIndex = " + e.Item.ItemIndex.ToString());
			dgICD.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindDT();
		} // END btnEdit_Click

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgICD.CurrentPageIndex = 0;
			dgICD.EditItemIndex = -1;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgICD.CurrentPageIndex >= 1)
				dgICD.CurrentPageIndex = dgICD.CurrentPageIndex - 1;
			else
				dgICD.CurrentPageIndex = 0;

			dgICD.EditItemIndex = -1;
			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgICD.CurrentPageIndex != (dgICD.PageCount-1))
				dgICD.CurrentPageIndex = dgICD.CurrentPageIndex + 1;
			else
				dgICD.CurrentPageIndex = (dgICD.PageCount-1);

			dgICD.EditItemIndex = -1;
			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgICD.CurrentPageIndex = (dgICD.PageCount-1);
			dgICD.EditItemIndex = -1;
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgICD.CurrentPageIndex = ddlPage.SelectedIndex;
			dgICD.EditItemIndex = -1;
			this.BindDT();
		}

		protected void btnFind_Click(object sender, System.EventArgs e)
		{
			int intPageIndex = this.FindRecordPage(txtFindIcd.Text);
			Debug.WriteLine("btnFind_Click intPageIndex = " + intPageIndex.ToString());
			if(intPageIndex != 0)
			{
				Debug.WriteLine("In intPageIndex != 0 ");
				dgICD.CurrentPageIndex = intPageIndex;
				this.BindDT();
			}
			else
			{
				Page.Validate();

				foreach(IValidator val in Page.Validators)
				{
					if(val.ErrorMessage == "ICD Code for this product was not found.")
						val.IsValid = false;
					else
						val.IsValid = true;
				} // END foreach
			}
		} // END btnFind_Click()

		private int FindRecordPage(string strIcdCode)
		{
			int intRow = 0;
			for(int x=0; x<myUtility.ICDTable.Rows.Count; x++)
			{
				if(myUtility.ICDTable.Rows[x]["IcdCode"].ToString().ToUpper() == strIcdCode.ToUpper())
				{
					intRow = x;
					x = myUtility.ICDTable.Rows.Count;
				}
			}

			if(intRow != 0)
				intRow = intRow / dgICD.PageSize;

			Debug.WriteLine("Return Row = " + intRow.ToString());
			return intRow;
		} // END FindRecordPage


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
