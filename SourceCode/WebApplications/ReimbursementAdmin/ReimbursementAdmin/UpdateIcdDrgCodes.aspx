<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Page language="c#" Codebehind="UpdateIcdDrgCodes.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateIcdDrgCodes" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update ICD DRG Codes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="global.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top" width="760">
						<table width="100%">
							<tr>
								<td width="18%">
									<asp:Label id="LabelIcd" Font-Bold="True" runat="server" ToolTip="Enter the ICD Code and click the Submit button.  This will retrieve any assigned DRG codes for the ICD code you enter."
										Text="Enter the ICD Code:"></asp:Label></td>
								<td width="10%">
                                    <asp:textbox id="txtICDCode" runat="server" ToolTip="Enter the ICD Code and click the Submit button.  This will retrieve any assigned DRG codes for the ICD code you enter."
										Columns="10" MaxLength="10" Font-Size="8pt"></asp:textbox></td>
								<td width="12%"><asp:Button id="btnDrg" BorderColor="#CCCCCC" CausesValidation="False" BackColor="White" ForeColor="#FF7300"
										Font-Size="8pt" Text="Submit" runat="server" Height="18" Width="70" onclick="btnDrg_Click"></asp:Button></td>
								<td><asp:label id="lblICDDesc" runat="server"></asp:label><asp:requiredfieldvalidator id="vldRequiredICD" runat="server" ErrorMessage="The ICD Code is required." ForeColor="#FF7300"
										ControlToValidate="txtICDCode">*</asp:requiredfieldvalidator></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td><asp:datagrid id="dgDRG" runat="server" ShowFooter="true" AutoGenerateColumns="False" ShowHeader="True"
										CellPadding="5" CellSpacing="0" Width="760" OnItemDataBound="DG_ItemDataBound" OnEditCommand="btnAddClick"
										OnDeleteCommand="btnDeleteClick" DataKeyField="drgCode">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn ItemStyle-Width="10%">
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="40" Height="18" runat="server" Text="Add" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DRG Code" HeaderStyle-Width="15%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblDrgCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "drgCode") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Textbox id="txtDrgCode" runat="server" Font-Size="8pt" MaxLength="10" Columns="5"></asp:Textbox>
													<asp:RequiredFieldValidator id="vldRequireDrgCode" ControlToValidate="txtDrgCode" runat="server" ErrorMessage="The DRG Code is required."
														ForeColor="#FF7300">*</asp:RequiredFieldValidator>
													<asp:CustomValidator id="vldDRGCodeExists" runat="server" ErrorMessage="The DRG Code is already assigned to this ICD Code.">*</asp:CustomValidator>
													<asp:CustomValidator id="vldValidDRGCode" runat="server" ErrorMessage="The DRG Code is invalid.">*</asp:CustomValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DRG Description" HeaderStyle-Width="75%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblDrgDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "drgDesc") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
							<tr>
								<td><asp:validationsummary id="vldSummary1" runat="server" ForeColor="#FF7300"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
