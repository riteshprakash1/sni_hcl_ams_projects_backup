<%@ Page language="c#" Codebehind="UpdateDrgCodes.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateDrgCodes" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update DRG Codes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="global.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top" width="760">
						<table width="760" align="center">
							<tr>
								<td><asp:validationsummary id="vldSummaryTop" runat="server" ForeColor="#FF7300"></asp:validationsummary></td>
							</tr>
							<tr>
								<td><asp:datagrid id="dgDRG" runat="server" ShowFooter="true" AutoGenerateColumns="False" ShowHeader="True"
										AllowPaging="True" PageSize="50" CellPadding="5" CellSpacing="0" Width="760" OnItemDataBound="DG_ItemDataBound"
										DataKeyField="drgCode" OnCancelCommand="btnAddClick" OnEditCommand="btnEditClick" OnDeleteCommand="btnDeleteClick"
										OnUpdateCommand="btnUpdateClick" OnPageIndexChanged="GridPageIndexChanged" PagerStyle-Mode="NumericPages"
										PagerStyle-Position="TopAndBottom">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="6%">
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="50" Height="18" runat="server" Text="Update" CommandName="Update"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="40" Height="18" runat="server" Text="Add" CommandName="Cancel"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DRG Code" HeaderStyle-Width="12%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblDrgCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "drgCode") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Textbox id="txtDrgCode" runat="server" Font-Size="8pt" MaxLength="10" Columns="5"></asp:Textbox>
													<asp:RequiredFieldValidator id="vldRequireDrgCode" ControlToValidate="txtDrgCode" runat="server" ErrorMessage="The DRG Code is required."
														ForeColor="#FF7300">*</asp:RequiredFieldValidator>
													<asp:CustomValidator id="vldDRGCodeExists" runat="server" ErrorMessage="The DRG Code is already exists in the table.">*</asp:CustomValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="DRG Description" HeaderStyle-Width="78%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblDrgDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "drgDesc") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Textbox id="txtDrgDesc" runat="server" Font-Size="8pt" MaxLength="75" Width="500" Text='<%# DataBinder.Eval(Container.DataItem, "drgDesc") %>' >
													</asp:Textbox>
													<asp:RequiredFieldValidator id="vldRequiredDrgDesc" ControlToValidate="txtDrgDesc" runat="server" ErrorMessage="Update - DRG Description is required."
														ForeColor="#FF7300">*</asp:RequiredFieldValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:Textbox id="txtAddDrgDesc" runat="server" Font-Size="8pt" MaxLength="75" Width="500"></asp:Textbox>
													<asp:RequiredFieldValidator id="vldAddDrgDesc" ControlToValidate="txtAddDrgDesc" runat="server" ErrorMessage="Add - DRG Description is required."
														ForeColor="#FF7300">*</asp:RequiredFieldValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn ItemStyle-Width="10%">
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
							<tr>
								<td><asp:validationsummary id="vldSummary1" runat="server" ForeColor="#FF7300"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
