<%@ Page language="c#" Codebehind="UpdatePublishedLit.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdatePublishedLit" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Published Literature</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<script>
		function confirm_delete()
		{
			var msg
			msg =  "Are you sure you want to delete this published literature item? Click 'OK' to\n\r"
			msg += "continue with the delete or click 'Cancel' to abort the delete."
			if (confirm(msg)==true)
				return true;
			else
				return false;
		}
		</script>
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%">
							<tr>
								<td width="40%"><asp:imagebutton id="btnFirst" runat="server" CausesValidation="False" ImageUrl="images/first.gif" onclick="btnFirst_Click"></asp:imagebutton><asp:imagebutton id="btnPrev" runat="server" CausesValidation="False" ImageUrl="images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" Font-Size="8pt" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblPageCnt" runat="server" Font-Size="8pt">Label</asp:label>&nbsp;&nbsp;<asp:imagebutton id="btnNext" runat="server" CausesValidation="False" ImageUrl="images/next.gif" onclick="btnNext_Click"></asp:imagebutton>
									<asp:imagebutton id="btnLast" runat="server" CausesValidation="False" ImageUrl="images/last.gif"
										ImageAlign="Bottom" onclick="btnLast_Click"></asp:imagebutton>
								</td>
								<td width="30%">&nbsp;</td>
								<td align="right"><asp:label id="lblRecordCount" runat="server"></asp:label></td>
							</tr>
						</table>
						<table width="100%" align="center">
							<tr>
								<td colSpan="5"><asp:datagrid id="dgPubLit" runat="server" Width="760" DataKeyField="publishedLitID" OnDeleteCommand="btnDelete_Click"
										CellSpacing="0" CellPadding="5" ShowFooter="True" ShowHeader="True" AutoGenerateColumns="False" PagerStyle-Visible="False"
										AllowPaging="True" AllowCustomPaging="False" PageSize="10" OnCancelCommand="btnAdd_Click" OnEditCommand="btnEdit_Click"
										OnUpdateCommand="btnUpdate_Click" OnItemDataBound="DG_ItemDataBound">
										<FooterStyle VerticalAlign="Top"></FooterStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<ItemStyle Wrap="False" Width="6%" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="50" Height="18" runat="server" Text="Add" CommandName="Cancel"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="50" Height="18" runat="server" Text="Update" CommandName="Update"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Literature Information">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblNote" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "note") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtNote" runat="server" Columns=60 Font-Size="8pt" Rows="3" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container.DataItem, "note") %>'>
													</asp:textbox>
													<asp:requiredfieldvalidator id="vldRequiredNote" runat="server" ControlToValidate="txtNote" ErrorMessage="Update - You must enter Literature Information.">*</asp:requiredfieldvalidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddNote" runat="server" Columns="60" Font-Size="8pt" Rows="3" TextMode="MultiLine"></asp:textbox>
													<asp:requiredfieldvalidator id="vldAddRequiredNote" runat="server" ControlToValidate="txtAddNote" ErrorMessage="Add - You must enter Literature Information.">*</asp:requiredfieldvalidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Product">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblProduct" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "productname") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:DropDownList Font-Size="8pt" id="ddlProduct" runat="server" width=100 DataTextField="ProductName" DataValueField="ProductID" DataSource="<%#GetProductList()%>" SelectedIndex='<%#GetProductIndex(DataBinder.Eval(Container.DataItem, "ProductID").ToString())%>' >
													</asp:DropDownList>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:DropDownList Font-Size="8pt" id="ddlAddProduct" runat="server" width=100 DataTextField="ProductName" DataValueField="ProductID" DataSource="<%#GetProductList()%>">
													</asp:DropDownList>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Year">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblYear" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "publishedYear") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddYear" runat="server" Font-Size="8pt" Columns="4" MaxLength="4"></asp:textbox>
													<asp:requiredfieldvalidator id="vldAddRequiredYear" runat="server" ControlToValidate="txtAddYear" ErrorMessage="Add - You must enter a year.">*</asp:requiredfieldvalidator>
													<asp:RangeValidator id="vldAddRangeYear" runat="server" ErrorMessage="Add - You must enter a year between 1980 and  2015."
										ControlToValidate="txtAddYear" MaximumValue="2015" MinimumValue="1980" Type="Integer">*</asp:RangeValidator>
												</FooterTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtYear" runat="server" Font-Size="8pt" Columns="4" MaxLength=4 Text='<%# DataBinder.Eval(Container.DataItem, "publishedYear") %>'>
													</asp:textbox>
													<asp:requiredfieldvalidator id="vldRequiredYear" runat="server" ControlToValidate="txtYear" ErrorMessage="Update - You must enter a year.">*</asp:requiredfieldvalidator>
													<asp:RangeValidator id="vldRangeYear" runat="server" ErrorMessage="Update - You must enter a year between 1980 and  2015."
										ControlToValidate="txtYear" MaximumValue="2015" MinimumValue="1980" Type="Integer">*</asp:RangeValidator>												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
												<ItemTemplate>
													<asp:Button id="btnDelete" CausesValidation="False" Font-Size="8pt" Width="50" Height="18" runat="server"
														Text="Delete" CommandName="Delete" ForeColor="#FF7300" BackColor="White" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Button id="btnEditDelete" CausesValidation="False" Font-Size="8pt" Width="50" Height="18"
														runat="server" Text="Delete" CommandName="Delete" ForeColor="#FF7300" BackColor="White"
														BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid>
									
								</td>
							</tr>
						</table>
						<table width="100%" align="center">
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
