<%@ Page language="c#" Codebehind="ErrorPage.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.ErrorPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Error Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="global.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
        <table cellspacing="0" cellpadding="5" width="980px" align="center" border="0">
	        <tr>
		        <td width="52%">
			        <IMG height="66" alt="" hspace="3" src="images/main-logo.gif" width="221"
				        border="0">
		        </td>
		        <td width="48%">
			        <div align="right"></div>
		        </td>
	        </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="980px" align="center" border="0">

	        <tr>
		        <td colspan="5"><hr style="color:#666666"/></td>
	        </tr>
        </table>
			<table width="980px" align="center">
				<tr>
					<td width="140px" valign="top">
                        <asp:Button id="btnHome" Width="120" Height="18" runat="server" Text="Home" Font-Size="8pt" ForeColor="#FF7300" 
				            BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnHome_Click"></asp:Button>
                    </td>
					<td>
						<table width="100%" border="0">
							<tr style="height:200" valign="middle">
								<td style="TEXT-ALIGN:center; vertical-align:top">
									<asp:Label ID="lblErrorHeading" ForeColor="#FF7300" Font-Size="14pt" runat="server" Text="Application Error"></asp:Label>
									<br>
									<br>
									<asp:Label ID="lblMsg" Font-Size="11pt" runat="server" Text="An application error occurred.  &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;An email has been sent to the application developer.  &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;You may be contacted by the application developer to troubleshoot the issue.  &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;Click the home button to be redirected to the application."></asp:Label>
									<br>
									<br>
									<asp:Label ID="lblErrorMsg" Font-Size="11pt" runat="server" Text="Application Error"></asp:Label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
