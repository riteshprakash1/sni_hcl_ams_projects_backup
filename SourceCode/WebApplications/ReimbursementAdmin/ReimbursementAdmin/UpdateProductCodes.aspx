<%@ Page language="c#" Codebehind="UpdateProductCodes.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateProductCodes" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Product Codes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frmUpdateProductCodes" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760" valign="top">
						<table width="100%">
							<tr valign="top">
								<td width="12%"><b>Categories:</b></td>
								<td><asp:dropdownlist id="ddlCategories" runat="server" AutoPostBack="True" Width="200px" onselectedindexchanged="ddlCategories_SelectedIndexChanged"></asp:dropdownlist><asp:comparevalidator id="vldCategories" runat="server" ErrorMessage="You must select a category." ControlToValidate="ddlCategories"
										Operator="NotEqual" ValueToCompare="0">*</asp:comparevalidator></td>
								<td width="12%"><b>Products:</b></td>
								<td><asp:dropdownlist id="ddlProducts" runat="server" AutoPostBack="True" Width="200px" onselectedindexchanged="ddlProducts_SelectedIndexChanged"></asp:dropdownlist><asp:comparevalidator id="Comparevalidator1" runat="server" ErrorMessage="You must select a product."
										ControlToValidate="ddlProducts" Operator="NotEqual" ValueToCompare="0">*</asp:comparevalidator></td>
							</tr>
						</table>
						<table width="760" align="center" border="0">
							<tr>
								<td colSpan="3"><asp:datagrid id="dgCPT" runat="server" OnCancelCommand="btnAddCPT_Click" ShowFooter="True" AutoGenerateColumns="False"
										ShowHeader="True" CellPadding="5" CellSpacing="0" DataKeyField="productCodeID" OnDeleteCommand="btnDeleteCPT_Click">
										<HeaderStyle BackColor="#cccccc" Font-Bold="True" Font-Size="10px"></HeaderStyle>
										<ItemStyle VerticalAlign="Top" Wrap="False"></ItemStyle>
										<Columns>
											<asp:TemplateColumn ItemStyle-Width="6%">
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="40" Height="18" runat="server" Text="Add" CommandName="Cancel"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="CPT Code" ItemStyle-Width="12%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblCPT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CPT_HCPCS") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddCPT" Font-Size="8pt" runat="server" Columns="5" MaxLength="5"></asp:textbox>
													<asp:RequiredFieldValidator id="vldAddCpt" runat="server" ControlToValidate="txtAddCPT" ErrorMessage="Add CPT - You must enter a CPT Code.">*</asp:RequiredFieldValidator>
													<asp:CustomValidator id="vldAddCptCode" runat="server" ControlToValidate="txtAddCPT" ErrorMessage="Add CPT - The CPT Code is not valid.">*</asp:CustomValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="CPT Description" ItemStyle-Width="50%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblCPTDescr" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Descr") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Applies To" ItemStyle-Width="32%">
												<ItemTemplate>
													<asp:CheckBox id="ckbPhysician" Enabled="False" runat="server" Text="Physician" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "PhysianPage")) %>'>
													</asp:CheckBox>
													<asp:CheckBox id="ckbAsc" Enabled="False" runat="server" Text="ASC" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "AscPage")) %>'>
													</asp:CheckBox>&nbsp;
													<asp:CheckBox id="ckbHospital" Enabled="False" runat="server" Text="Hospital" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "HospitalPage")) %>'>
													</asp:CheckBox>
													<asp:CheckBox id="ckbInPatient" Enabled="False" runat="server" Text="Hospital InPatient" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "InPatientPage")) %>'>
													</asp:CheckBox>
												</ItemTemplate>
												<FooterTemplate>
													<asp:CheckBox id="ckbAddPhysician" runat="server" Text="Physician" Checked="True"></asp:CheckBox>
													<asp:CheckBox id="ckbAddAsc" runat="server" Text="ASC" Checked="True"></asp:CheckBox>&nbsp;
													<asp:CheckBox id="ckbAddHospital" runat="server" Text="Hospital" Checked="True"></asp:CheckBox>
													<asp:CheckBox id="ckbAddInPatient" runat="server" Text="Hosptital InPatient" Checked="True"></asp:CheckBox>
												</FooterTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td>&nbsp;
								</td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
