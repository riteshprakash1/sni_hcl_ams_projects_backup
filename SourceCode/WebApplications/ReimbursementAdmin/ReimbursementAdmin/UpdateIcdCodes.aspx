<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Page language="c#" Codebehind="UpdateIcdCodes.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateIcdCodes" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UpdateIcdCodes</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%">
							<tr>
								<td width="40%"><asp:imagebutton id="btnFirst" runat="server" CausesValidation="False" ImageUrl="images/first.gif" onclick="btnFirst_Click"></asp:imagebutton><asp:imagebutton id="btnPrev" runat="server" CausesValidation="False" ImageUrl="images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" Font-Size="8pt" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblPageCnt" runat="server" Font-Size="8pt">Label</asp:label>&nbsp;&nbsp;<asp:imagebutton id="btnNext" runat="server" CausesValidation="False" ImageUrl="images/next.gif" onclick="btnNext_Click"></asp:imagebutton>
									<asp:imagebutton id="btnLast" runat="server" CausesValidation="False" ImageUrl="images/last.gif"
										ImageAlign="Bottom" onclick="btnLast_Click"></asp:imagebutton>
								</td>
								<td width="30%">
									<asp:button id="btnFind" runat="server" Width="90px" Height="18" Text="Find ICD Code" CommandName="Cancel"
										Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnFind_Click"></asp:button>
									<asp:TextBox id="txtFindIcd" runat="server" Columns="6" MaxLength="6" Font-Size="8pt"></asp:TextBox>
									<asp:CustomValidator id="vldIcdCodeFind" runat="server" ErrorMessage="ICD Code for this product was not found.">*</asp:CustomValidator>
								</td>
								<td align="right"><asp:label id="lblRecordCount" runat="server"></asp:label></td>
							</tr>
						</table>
						<table width="100%" align="center">
							<tr>
								<td colSpan="5"><asp:datagrid id="dgICD" runat="server" Width="760" DataKeyField="icdCodeID" OnDeleteCommand="btnDelete_Click"
										CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False" PagerStyle-Visible="False"
										AllowPaging="True" AllowCustomPaging="False" PageSize="10" OnCancelCommand="btnAdd_Click" OnEditCommand="btnEdit_Click"
										OnUpdateCommand="btnUpdate_Click">
										<FooterStyle VerticalAlign="Top"></FooterStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<ItemStyle Wrap="False" Width="6%" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="50" Height="18" runat="server" Text="Add" CommandName="Cancel"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="50" Height="18" runat="server" Text="Update" CommandName="Update"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="46px"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnDeleteIcdD" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="ICD Code">
												<HeaderStyle Width="91px"></HeaderStyle>
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblIcd" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "icdCode") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddIcd" runat="server" Font-Size="8pt" MaxLength="5" Columns="5"></asp:textbox>
													<asp:requiredfieldvalidator id="vldRequiredIcd" runat="server" ControlToValidate="txtAddIcd" ErrorMessage="Add - You must enter an ICD Code.">*</asp:requiredfieldvalidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="ICD Type">
												<HeaderStyle Width="114px"></HeaderStyle>
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblIcdType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "icdType") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:dropdownlist id="ddlAddIcdType" runat="server">
														<asp:ListItem Value="0">Select</asp:ListItem>
														<asp:ListItem Value="Diagnosis">Diagnosis</asp:ListItem>
														<asp:ListItem Value="Procedure">Procedure</asp:ListItem>
													</asp:dropdownlist>
													<asp:CompareValidator id="vldAddIcdType" runat="server" ErrorMessage="Add - You must select an ICD Type."
														ControlToValidate="ddlAddIcdType" ValueToCompare="0" Operator="NotEqual">*</asp:CompareValidator>
												</FooterTemplate>
												<EditItemTemplate>
													<asp:dropdownlist id="ddlIcdType" runat="server" SelectedIndex='<%#GetIcdTypeIndex(DataBinder.Eval(Container.DataItem, "icdType").ToString())%>' >
														<asp:ListItem Value="Diagnosis">Diagnosis</asp:ListItem>
														<asp:ListItem Value="Procedure">Procedure</asp:ListItem>
													</asp:dropdownlist>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="ICD Description">
												<HeaderStyle Width="410px"></HeaderStyle>
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblIcdDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Descr") %>'>
													</asp:Label>
												</ItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddIcdDesc" runat="server" Font-Size="8pt" Columns="50" TextMode="MultiLine"
														Rows="3"></asp:textbox>
													<asp:requiredfieldvalidator id="vldAddRequiredIcdDesc" runat="server" ControlToValidate="txtAddIcdDesc" ErrorMessage="Add - You must enter an ICD Description.">*</asp:requiredfieldvalidator>
												</FooterTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtIcdDesc" runat="server" Font-Size="8pt" Columns="50" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container.DataItem, "Descr") %>' Rows="3">
													</asp:textbox>
													<asp:requiredfieldvalidator id="vldRequiredIcdDesc" runat="server" ControlToValidate="txtIcdDesc" ErrorMessage="You must enter an ICD Description.">*</asp:requiredfieldvalidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<table width="100%" align="center">
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
