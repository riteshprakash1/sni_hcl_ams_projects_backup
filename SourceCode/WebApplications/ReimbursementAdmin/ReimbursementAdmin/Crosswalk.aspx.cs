using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for Crosswalk.
	/// </summary>
	public partial class Crosswalk : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected System.Web.UI.WebControls.Literal ltlTitle;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pageTitle;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("In  Page_Load");
			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			if(!Page.IsPostBack)
			{

				EnterButton.TieButton(this.txtDescription, this.btnSearch);
				EnterButton.TieButton(this.txtCatalog, this.btnSearch);

				this.LoadDropDowns();

				this.lblSortOrder.Text = "CatalogNum";
				this.BindCrosswalk();

			}
		} // end page_load

		protected void btnUpdate_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("Add"))
				{
					val.IsValid = true;
				}
			} // END foreach
			
			if(Page.IsValid)
			{
				TextBox txtSNDept = (TextBox)e.Item.FindControl("txtSNDept");
				TextBox txtPart = (TextBox)e.Item.FindControl("txtPart");
				TextBox txtCatalogNum = (TextBox)e.Item.FindControl("txtCatalogNum");
				TextBox txtProductDescription = (TextBox)e.Item.FindControl("txtProductDescription");
				TextBox txtCPT = (TextBox)e.Item.FindControl("txtCPT");
				
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				SqlCommand cmdUpdate = new System.Data.SqlClient.SqlCommand();
				cmdUpdate.CommandType = CommandType.StoredProcedure;
				cmdUpdate.CommandText = "sp_UpdateCrosswalk";
				cmdUpdate.Connection = myUtility.SqlConn;

				cmdUpdate.Parameters.Add(new SqlParameter("@SNDept", txtSNDept.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@CPT_HCPCS", txtCPT.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@Part", txtPart.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@CatalogNum", txtCatalogNum.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@ProductDescription", txtProductDescription.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@CrosswalkID", Int32.Parse(this.dgCrosswalk.DataKeys[e.Item.ItemIndex].ToString())));

				int rowsUpdated = cmdUpdate.ExecuteNonQuery();

                this.dgCrosswalk.EditItemIndex = -1;
                this.BindCrosswalk();
				myUtility.SqlConn.Close();
			} // END (Page.IsValid)
		} // END btnUpdate_Click

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			this.dgCrosswalk.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindCrosswalk();
		} // END btnEdit_Click

        public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            Button btn;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                btn = (Button)e.Item.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete();");
            }
        } // END DG_ItemDataBound


		protected void btnAdd_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(!val.ErrorMessage.StartsWith("Add"))
				{
					val.IsValid = true;
				}
			} // END foreach
			
			if(Page.IsValid)
			{
				TextBox txtAddSNDept = (TextBox)e.Item.FindControl("txtAddSNDept");
				TextBox txtAddPart = (TextBox)e.Item.FindControl("txtAddPart");
				TextBox txtAddCatalogNum = (TextBox)e.Item.FindControl("txtAddCatalogNum");
				TextBox txtAddProductDescription = (TextBox)e.Item.FindControl("txtAddProductDescription");
				TextBox txtAddCPT = (TextBox)e.Item.FindControl("txtAddCPT");
				
				if(myUtility.SqlConn.State != ConnectionState.Open)
					myUtility.SqlConn.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertCrosswalk";
				cmdInsert.Connection = myUtility.SqlConn;

				cmdInsert.Parameters.Add(new SqlParameter("@SNDept", txtAddSNDept.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@CPT_HCPCS", txtAddCPT.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@Part", txtAddPart.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@CatalogNum", txtAddCatalogNum.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@ProductDescription", txtAddProductDescription.Text));

				int rowsUpdated = cmdInsert.ExecuteNonQuery();
                
                this.dgCrosswalk.EditItemIndex = -1;
				this.BindCrosswalk();
				myUtility.SqlConn.Close();
			} // END (Page.IsValid)
		} // END btnAdd_Click

		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			if(myUtility.SqlConn.State != ConnectionState.Open)
				myUtility.SqlConn.Open();
			
			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteCrosswalkByID";
			cmdDelete.Connection = myUtility.SqlConn;

			cmdDelete.Parameters.Add(new SqlParameter("@CrosswalkID", Int32.Parse(this.dgCrosswalk.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

            this.dgCrosswalk.EditItemIndex = -1;
            this.BindCrosswalk();
			myUtility.SqlConn.Close();

		} // END btnDelete_Click

		public void BindCrosswalk()
		{
			DataTable dtCrosswalk = this.GetSearchTable();

			dgCrosswalk.DataSource = dtCrosswalk;
			dgCrosswalk.DataBind();
		} // END BindCrosswalk

		protected void sortHandler(object sender, DataGridSortCommandEventArgs e)
		{
			this.lblSortOrder.Text = e.SortExpression;
			this.BindCrosswalk();

		} // END sortHandler


		protected void Page_Changed(object sender, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.dgCrosswalk.CurrentPageIndex = e.NewPageIndex;	
			this.BindCrosswalk();
		}

		private void LoadDropDowns()
		{
			DataTable dtParts = myUtility.SetTable("sp_GetCrosswalkParts");
			DataTable dtCPT = myUtility.SetTable("sp_GetCrosswalkCPT");
			
			this.ddlPart.Items.Clear();
			ddlPart.Items.Add(new ListItem("-- Select --", "%"));
			foreach (DataRow r in dtParts.Rows)
			{
				this.ddlPart.Items.Add(new ListItem(r["Part"].ToString(), r["Part"].ToString()));
			}

			this.ddlCPT.Items.Clear();
			ddlCPT.Items.Add(new ListItem("-- Select --", "%"));
			foreach (DataRow r in dtCPT.Rows)
			{
				this.ddlCPT.Items.Add(new ListItem(r["CPT_HCPCS"].ToString(), r["CPT_HCPCS"].ToString()));
			}
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			this.lblSortOrder.Text = "CatalogNum";
			this.dgCrosswalk.CurrentPageIndex = 0;	
			this.BindCrosswalk();
		}

		public DataTable GetSearchTable()
		{
			if(myUtility.SqlConn.State != ConnectionState.Open)
				myUtility.SqlConn.Open();

			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = "sp_GetCrosswalkSearch";
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = myUtility.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@Part", this.ddlPart.SelectedValue));
			cmdSetTable.Parameters.Add(new SqlParameter("@CatalogNum", "%"+this.txtCatalog.Text+"%"));
			cmdSetTable.Parameters.Add(new SqlParameter("@ProductDescription", "%"+this.txtDescription.Text+"%"));
			cmdSetTable.Parameters.Add(new SqlParameter("@CPT_HCPCS", this.ddlCPT.SelectedValue));

			DataTable dtSearch = new DataTable();
			Debug.WriteLine("dtSearch rows: " + dtSearch.Rows.Count.ToString());

			SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
			da.Fill(dtSearch);

			myUtility.SqlConn.Close();

			DataRow[] rows = dtSearch.Select(null, this.lblSortOrder.Text);

			DataTable dtCrosswalk = dtSearch.Clone();
			foreach (DataRow row in rows) 
			{
				dtCrosswalk.ImportRow(row);
			}
				
			return dtCrosswalk;
		} // END GetSearchTable()

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


	}
}
