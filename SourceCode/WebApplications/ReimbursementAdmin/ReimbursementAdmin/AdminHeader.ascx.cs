namespace ReimbursementAdmin
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Data.SqlClient;
	using System.Diagnostics;


	/// <summary>
	///		Summary description for AdminHeader.
	/// </summary>
	public partial class AdminHeader : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Button btnProductContent;
		protected System.Web.UI.WebControls.Button btnProductCPTCodes;
		protected System.Web.UI.WebControls.Button btnICDCodes;
		protected System.Web.UI.WebControls.Button btnProductICDCodes;
		protected SqlConnection sqlConnection1;
		protected System.Web.UI.WebControls.Button btnFaq;
		protected Utility myUtility;

		protected void Page_Load(object sender, System.EventArgs e)
		{ 

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");
			
			if(!myUtility.IsAdmin)
			{
				myUtility.SetIsAdmin(Request);
				Session["Utility"] = myUtility;
			}

			if(!myUtility.IsAdmin)
				Response.Redirect("Unauthorized.aspx");


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion



	}
}
