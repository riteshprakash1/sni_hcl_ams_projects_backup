<%@ Page language="c#" Codebehind="UpdateProductIcdCodes.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateProductIcdCodes" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Product ICD Codes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frmUpdateProductCodes" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%">
							<tr>
								<td width="12%"><b>Categories:</b></td>
								<td><asp:dropdownlist id="ddlCategories" runat="server" Width="200px" AutoPostBack="True" onselectedindexchanged="ddlCategories_SelectedIndexChanged"></asp:dropdownlist><asp:comparevalidator id="vldCategories" runat="server" ValueToCompare="0" Operator="NotEqual" ControlToValidate="ddlCategories"
										ErrorMessage="You must select a category.">*</asp:comparevalidator></td>
								<td width="12%"><b>Products:</b></td>
								<td><asp:dropdownlist id="ddlProducts" runat="server" Width="200px" AutoPostBack="True" onselectedindexchanged="ddlProducts_SelectedIndexChanged"></asp:dropdownlist><asp:comparevalidator id="Comparevalidator1" runat="server" ValueToCompare="0" Operator="NotEqual" ControlToValidate="ddlProducts"
										ErrorMessage="You must select a product.">*</asp:comparevalidator></td>
							</tr>
						</table>
						<table width="760" align="center" border="1" CellSpacing="0" CellPadding="5">
							<tr vAlign="top">
								<td width="6%">
									<asp:button id="btnAddIcdD" runat="server" Width="40" BorderColor="#CCCCCC" CausesValidation="False"
										BackColor="White" ForeColor="#FF7300" Font-Size="8pt" CommandName="Cancel" Text="Add" Height="18" onclick="btnAddIcdD_Click"></asp:button></td>
								<td width="10%">
									<asp:textbox id="txtAddIcd" runat="server" Font-Size="8pt" MaxLength="10" 
                                        Columns="10" AutoPostBack="True"></asp:textbox>
									<asp:requiredfieldvalidator id="vldRequiredIcd" runat="server" ControlToValidate="txtAddIcd" ErrorMessage="Add - You must enter an ICD Code.">*</asp:requiredfieldvalidator>
									<asp:CustomValidator id="vldIcdCode" runat="server" ErrorMessage="Add - The ICD Code does not exist."
										ControlToValidate="txtAddIcd">*</asp:CustomValidator></td>
								<td width="13%">
									<asp:Label id="lblAddIcdType" runat="server">&nbsp;</asp:Label></td>
								<td width="18%">
									<asp:listbox id="lstCpt" runat="server" SelectionMode="Multiple" Rows="3"></asp:listbox></td>
								<td width="53%">
									<asp:textbox id="txtAddIcdDesc" runat="server" Font-Size="8pt" Columns="50" TextMode="MultiLine"
										Rows="3" ReadOnly="True"></asp:textbox></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td width="25%">&nbsp;</td>
								<td><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
						<table width="760" align="center" border="0">
							<tr>
								<td width="40%"><asp:imagebutton id="btnFirst" runat="server" CausesValidation="False" ImageUrl="images/first.gif" onclick="btnFirst_Click"></asp:imagebutton><asp:imagebutton id="btnPrev" runat="server" CausesValidation="False" ImageUrl="images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" Font-Size="8pt" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblPageCnt" runat="server" Font-Size="8pt">Label</asp:label>&nbsp;&nbsp;<asp:imagebutton id="btnNext" runat="server" CausesValidation="False" ImageUrl="images/next.gif" onclick="btnNext_Click"></asp:imagebutton>
									<asp:imagebutton id="btnLast" runat="server" CausesValidation="False" ImageUrl="images/last.gif"
										ImageAlign="Bottom" onclick="btnLast_Click"></asp:imagebutton>
								</td>
								<td width="30%">
									<asp:button id="btnFind" runat="server" Width="90px" Height="18" Text="Find ICD Code" CommandName="Cancel"
										Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnFind_Click"></asp:button>
									<asp:TextBox id="txtFindIcd" runat="server" Columns="6" MaxLength="6" Font-Size="8pt"></asp:TextBox>
									<asp:CustomValidator id="vldIcdCodeFind" runat="server" ErrorMessage="ICD Code for this product was not found.">*</asp:CustomValidator></td>
								<td align="right"><asp:label id="lblRecordCount" runat="server">Label</asp:label></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td colSpan="5"><asp:datagrid id="dgICD" runat="server" Width="760" OnDeleteCommand="btnDeleteIcd_Click" DataKeyField="productIcdCodeID"
										CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False" ShowFooter="False" PagerStyle-Visible="False"
										AllowPaging="True" AllowCustomPaging="False" PageSize="10" OnItemDataBound="DG_ItemDataBound" OnEditCommand="btnEdit_Click"
										OnUpdateCommand="btnUpdate_Click">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<ItemStyle Width="6%"></ItemStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="40" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="40" Height="18" runat="server" Text="Update" CommandName="Update"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemStyle Width="6%"></ItemStyle>
												<ItemTemplate>
													<asp:Button id="btnDeleteIcdD" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="ICD Code" HeaderStyle-Width="10%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblIcd" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "icdCode") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="ICD Type" HeaderStyle-Width="13%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblIcdType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "icdType") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn ItemStyle-Wrap="True" HeaderText="CPT Cross&lt;br&gt;Reference" HeaderStyle-Width="18%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblCPTCross" Text='<%# GetCrossReference(DataBinder.Eval(Container.DataItem, "productIcdCodeID").ToString()) %>' runat="server">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:listbox id="lstUpdateCpt" runat="server" SelectionMode="Multiple" Rows="3"></asp:listbox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="ICD Description" HeaderStyle-Width="53%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblIcdDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Descr") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
