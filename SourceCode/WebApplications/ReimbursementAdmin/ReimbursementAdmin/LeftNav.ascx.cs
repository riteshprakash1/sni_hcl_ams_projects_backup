namespace ReimbursementAdmin
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Configuration;

	/// <summary>
	///		Summary description for LeftNav.
	/// </summary>
	public partial class LeftNav : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Button btnPublisheLit;
		protected Utility myUtility;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx", true);

		} // END Page_Load

		protected void btnProductContent_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateProductContent.aspx", true);
		}

		protected void btnProductCPTCodes_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateProductCodes.aspx", true);
		}

		protected void btnProductICDCodes_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateProductIcdCodes.aspx", true);
		}

		protected void btnICDCodes_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateIcdCodes.aspx", true);
		}

		protected void btnFaq_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("faqList.aspx", true);
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void btnLinks_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateLinks.aspx", true);
		}

		protected void btnTerms_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("TermList.aspx", true);
		}

		protected void btnWhatsNew_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("WhatsNewList.aspx", true);
		
		}

		protected void btndocuments_Click(object sender, System.EventArgs e)
		{
		     Response.Redirect("DocumentList.aspx", true);
		}

//		private void btnPublisheLit_Click(object sender, System.EventArgs e)
//		{
//			Response.Redirect("UpdatePublishedLit.aspx", true);
//		}

		protected void btnActitviy_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UserActivity.aspx", true);
		}

		protected void btnQuery_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("QuerySet.aspx", true);
		
		}

		protected void btnUpdateCptCodes_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateCptCode.aspx", true);
		
		}

		protected void btnCrosswalk_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Crosswalk.aspx", true);
		}

		protected void btnIcdDrg_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateIcdDrgCodes.aspx", true);
		}

		protected void btnDrg_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("UpdateDrgCodes.aspx", true);
		}

		protected void btnSurvey_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("SurveyList.aspx", true);
		}

	
		

	} // END class
}
