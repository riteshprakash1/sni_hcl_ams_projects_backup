using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateProductCodes.
	/// </summary>
	public partial class UpdateProductCodes : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected DataTable dtCPT;
		protected System.Web.UI.WebControls.DropDownList ddlIcdType;
		protected System.Web.UI.WebControls.ListBox lstCpt;
		protected DataTable dtICD;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				ddlCategories.Items.Clear();
				ddlCategories.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.ProductCategoryTable.Rows)
				{
					ddlCategories.Items.Add(new ListItem(aRow["productCategoryName"].ToString(),aRow["productCategoryID"].ToString() ));
				}
			} // END isPostBack	

			this.SetControls();

		} // END Page_Load

		private void SetControls()
		{ 
 
		} // END SetControls



		protected void btnAddCPT_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			TextBox txtAddCPT = (TextBox)e.Item.FindControl("txtAddCPT");
			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("Add ICD"))
				{
					val.IsValid = true;
				}

				if(val.ErrorMessage == "Add CPT - The CPT Code is not valid.")
				{
					sqlConnection1.Open();
					DataTable dtCod = myUtility.SetTable("sp_GetCptCode", txtAddCPT.Text);
					sqlConnection1.Close();

					val.IsValid = Convert.ToBoolean(dtCod.Rows.Count>0);
				}


			} // END foreach
			
			if(Page.IsValid)
			{
				CheckBox ckbAddPhysician = (CheckBox)e.Item.FindControl("ckbAddPhysician");
				CheckBox ckbAddAsc = (CheckBox)e.Item.FindControl("ckbAddAsc");
				CheckBox ckbAddHospital = (CheckBox)e.Item.FindControl("ckbAddHospital");
				CheckBox ckbAddInPatient = (CheckBox)e.Item.FindControl("ckbAddInPatient");
				
				sqlConnection1.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertProductCptCode";
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@productID", Int32.Parse(ddlProducts.SelectedValue)));
				cmdInsert.Parameters.Add(new SqlParameter("@CPT_HCPCS", txtAddCPT.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@PhysianPage", ckbAddPhysician.Checked.ToString()));
				cmdInsert.Parameters.Add(new SqlParameter("@AscPage", ckbAddAsc.Checked.ToString()));
				cmdInsert.Parameters.Add(new SqlParameter("@HospitalPage", ckbAddHospital.Checked.ToString()));
				cmdInsert.Parameters.Add(new SqlParameter("@InPatientPage", ckbAddInPatient.Checked.ToString()));

				SqlParameter paramID = new SqlParameter("@productCodeID", SqlDbType.Int);
				paramID.Direction = ParameterDirection.Output;
				cmdInsert.Parameters.Add(paramID);

				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				dtCPT = myUtility.SetTable("sp_GetProductCptCodes", Int32.Parse(ddlProducts.SelectedValue));
				sqlConnection1.Close();

				this.BindCPT();

			} // END (Page.IsValid)
		} // END btnAdd_Click

		protected void btnDeleteCPT_Click(object sender, DataGridCommandEventArgs e)
		{
				sqlConnection1.Open();

				SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
				cmdDelete.CommandType = CommandType.StoredProcedure;
				cmdDelete.CommandText = "sp_DeleteProductCptCode";
				cmdDelete.Connection = sqlConnection1;

				cmdDelete.Parameters.Add(new SqlParameter("@productCodeID", Int32.Parse(dgCPT.DataKeys[e.Item.ItemIndex].ToString())));

				int rowsUpdated = cmdDelete.ExecuteNonQuery();

				dtCPT = myUtility.SetTable("sp_GetProductCptCodes", Int32.Parse(ddlProducts.SelectedValue));
				sqlConnection1.Close();

				this.BindCPT();

		} // END btnDelete_Click

		public void BindCPT()
		{
			dgCPT.DataSource = dtCPT;
			dgCPT.DataBind();
		} // END BindCPT




		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlCategories_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage != "You must select a category.")
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				this.GetProductList();
			}
			ddlProducts.SelectedIndex = 0;

			this.SetControls();
		}

		protected void ddlProducts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage != "You must select a product.")
				{
					val.IsValid = true;
				}
			} // END foreach

			
			if(Page.IsValid)
			{
				sqlConnection1.Open();
				dtCPT = myUtility.SetTable("sp_GetProductCptCodes", Int32.Parse(ddlProducts.SelectedValue));
				sqlConnection1.Close();

				this.BindCPT();

			} // END Page.IsValid

		} // END ddlCategories_SelectedIndexChanged

		private void GetProductList()
		{
			sqlConnection1.Open();

	//		myUtility.ProductsTable = myUtility.SetTable("sp_GetProducts", Int32.Parse(ddlCategories.SelectedValue));
			DataRow[] rows = myUtility.ProductInfoTable.Select("productCategoryID="+ddlCategories.SelectedValue);
		Debug.WriteLine("ProductInfoTable Rows: " + rows.Length.ToString());
			ddlProducts.Items.Clear();
			ddlProducts.Items.Add(new ListItem("Select","0"));
			foreach(DataRow aRow in rows)
			{
				ddlProducts.Items.Add(new ListItem(aRow["productName"].ToString(),aRow["productID"].ToString() ));
			}

			sqlConnection1.Close();
		} // END GetProductList

	} // END class
}
