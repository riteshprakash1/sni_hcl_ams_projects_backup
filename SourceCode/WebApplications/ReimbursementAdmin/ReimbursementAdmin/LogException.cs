using System;
using System.Web;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
//using System.Web.Mail;
using System.Configuration;
using System.Net.Mail;

namespace ReimbursementAdmin
{	 
	public class LogException  
	{
		public LogException( ) //ctor
		{ 
		}
	
		public void HandleException(Exception ex)
		{
			HttpContext ctx = HttpContext.Current;	
			string strData=String.Empty;

			bool logIt = Convert.ToBoolean(ConfigurationManager.AppSettings["logErrors"]);
			if(logIt)
			{
				string referer=String.Empty;
				if(ctx.Request.ServerVariables["HTTP_REFERER"]!=null)
					referer = ctx.Request.ServerVariables["HTTP_REFERER"].ToString();

				string strUser = String.Empty;
				if(ctx.Request.ServerVariables["AUTH_USER"]!=null)
					strUser = ctx.Request.ServerVariables["AUTH_USER"].ToString();

				string sForm = (ctx.Request.Form !=null)?ctx.Request.Form.ToString():String.Empty;
      
				string logDateTime =DateTime.Now.ToString();
				string sQuery = (ctx.Request.QueryString !=null)?ctx.Request.QueryString.ToString():String.Empty;
				strData =  "\nSOURCE: " + ex.Source + "\nLogDateTime: " +logDateTime;
				strData += "\nMESSAGE: " +ex.Message + "\nFORM: " + sForm;
				strData += "\nQUERYSTRING: " + sQuery +	"\nTARGETSITE: " + ex.TargetSite;
				strData += "\nSTACKTRACE: " + ex.StackTrace + "\nREFERER: " +referer;
				strData += "\nUSER: " + strUser + "\n";

			} // END if(logIt)
	
			// Send email notification
			// Email receiptient list should be delimited by |
            string strEmails = ConfigurationManager.AppSettings["logEmailAddresses"].ToString();
			if (strEmails.Length >0) 
			{

                MailMessage message = new MailMessage();
                MailAddress from = new MailAddress(ConfigurationManager.AppSettings["logFromEmail"]);
                message.From = from;

                // Pipe delimited list of email addesses
                string[] emails = strEmails.Split(Convert.ToChar("|"));
                foreach (string sEmail in emails)
                {
                    MailAddress to = new MailAddress(sEmail);
                    message.To.Add(to);
                }

                message.IsBodyHtml = false;

                message.Subject = "Web application error!";
                message.Body = strData;
                //SmtpMail.SmtpServer = ConfigurationManager.AppSettings["SmtpServer"].ToString();

                SmtpClient client = new SmtpClient();

				try
				{
                    client.Send(message);
                }
				catch (Exception excm )
				{
					Debug.WriteLine(excm.Message);
//					throw;
				}
			}
		} // END HandleException(Exception ex)

	} // END class
} // END namespace
