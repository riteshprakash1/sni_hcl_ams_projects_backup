using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateProductIcdCodes.
	/// </summary>
	public partial class UpdateProductIcdCodes : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected DataTable dtCPT;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				EnterButton.TieButton(txtFindIcd, btnFind);
				EnterButton.TieButton(txtAddIcd, btnAddIcdD);

				ddlCategories.Items.Clear();
				ddlCategories.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.ProductCategoryTable.Rows)
				{
					ddlCategories.Items.Add(new ListItem(aRow["productCategoryName"].ToString(),aRow["productCategoryID"].ToString() ));
				}


			} // END isPostBack	

			this.SetControls();

		} // END Page_Load


		private void SetControls()
		{
			bool blnVisible = Convert.ToBoolean(!(ddlProducts.SelectedIndex == 0 || ddlProducts.Items.Count == 0));
			Debug.WriteLine("bool = " + blnVisible.ToString());
			dgICD.Visible = blnVisible;
			btnFirst.Visible = blnVisible;
			btnPrev.Visible = blnVisible;
			ddlPage.Visible = blnVisible;
			lblPageCnt.Visible = blnVisible;
			btnNext.Visible = blnVisible;
			btnLast.Visible = blnVisible;
			lblRecordCount.Visible = blnVisible;
			btnFind.Visible = blnVisible;
			txtFindIcd.Visible = blnVisible;
		
		} // END SetControls


		public void BindDT()
		{
			try
			{
				dgICD.DataSource = myUtility.ICDTable;
				dgICD.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Exception: " + ex.Message);
				dgICD.CurrentPageIndex = 0;
			}

			if(dgICD.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgICD.CurrentPageIndex != (dgICD.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}


			lblRecordCount.Text = "<b><font color=red>" + Convert.ToString(myUtility.ICDTable.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgICD.PageCount.ToString();
			ddlPage.Items.Clear();
			for(int x=1;x<dgICD.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			int myPage=dgICD.CurrentPageIndex+1;
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

		} // END BindDT

		protected void btnDeleteIcd_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteProductIcd";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@ID", Int32.Parse(dgICD.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.ICDTable = myUtility.SetTable("sp_GetProductIcdCodes", Int32.Parse(ddlProducts.SelectedValue));
			myUtility.CptIcdTable = myUtility.SetTable("sp_GetCptIcdByProduct", Int32.Parse(ddlProducts.SelectedValue));
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;
			this.ClearAddControls();

		} // END btnDeleteIcdD_Click


		protected void ddlCategories_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage != "You must select a category.")
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				this.GetProductList();
			}
			ddlProducts.SelectedIndex = 0;

			this.SetControls();
			this.ClearAddControls();
		}

		protected void ddlProducts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage != "You must select a product.")
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				sqlConnection1.Open();
				myUtility.ICDTable = myUtility.SetTable("sp_GetProductIcdCodes", Int32.Parse(ddlProducts.SelectedValue));
				myUtility.CptIcdTable = myUtility.SetTable("sp_GetCptIcdByProduct", Int32.Parse(ddlProducts.SelectedValue));
				myUtility.CptTable = myUtility.SetTable("sp_GetProductCptCodes", Int32.Parse(ddlProducts.SelectedValue));
		//		dtCPT = myUtility.SetTable("sp_GetProductCptCodes", Int32.Parse(ddlProducts.SelectedValue));
				sqlConnection1.Close();
				Debug.WriteLine("ICD Rows = " + myUtility.ICDTable.Rows.Count.ToString());
				Debug.WriteLine("CPT Rows = " + myUtility.CptTable.Rows.Count.ToString());
				Debug.WriteLine("CptIcd Rows = " + myUtility.CptIcdTable.Rows.Count.ToString());
				
				dgICD.CurrentPageIndex = 0;
				this.BindDT();
				Session["Utility"] = myUtility;

				lstCpt.Items.Clear();
				foreach(DataRow aRow in myUtility.CptTable.Rows)
				{
					lstCpt.Items.Add(new ListItem(aRow["CPT_HCPCS"].ToString(),aRow["CPT_HCPCS"].ToString() ));
				}

				Session["Utility"] = myUtility;
			
			} // END Page.IsValid

			this.SetControls();
			this.ClearAddControls();

		} // END ddlCategories_SelectedIndexChanged


		private void GetProductList()
		{
			sqlConnection1.Open();

			DataRow[] rows = myUtility.ProductInfoTable.Select("productCategoryID="+ddlCategories.SelectedValue);
			Debug.WriteLine("product rows = " + myUtility.ProductInfoTable);	
			ddlProducts.Items.Clear();
			ddlProducts.Items.Add(new ListItem("Select","0"));
			foreach(DataRow aRow in rows)
			{
				ddlProducts.Items.Add(new ListItem(aRow["productName"].ToString(),aRow["productID"].ToString() ));
			}

			sqlConnection1.Close();
		} // END GetProductList



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgICD.CurrentPageIndex = 0;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgICD.CurrentPageIndex >= 1)
				dgICD.CurrentPageIndex = dgICD.CurrentPageIndex - 1;
			else
				dgICD.CurrentPageIndex = 0;

			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgICD.CurrentPageIndex != (dgICD.PageCount-1))
				dgICD.CurrentPageIndex = dgICD.CurrentPageIndex + 1;
			else
				dgICD.CurrentPageIndex = (dgICD.PageCount-1);

			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgICD.CurrentPageIndex = (dgICD.PageCount-1);
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgICD.CurrentPageIndex = ddlPage.SelectedIndex;
			this.BindDT();
		}

		public string GetCrossReference(string id)
		{
			DataRow[] rows = myUtility.CptIcdTable.Select("productIcdCodeID = " + id);
			string strRef = "";
			string strDel = "";
			foreach(DataRow aRow in rows)
			{
				strRef += strDel + aRow["CPT_HCPCS"].ToString();
				strDel = ", ";
			}

			return strRef;
		}

		protected void btnAddIcdD_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("In btnAddIcdD_Click");
			Page.Validate();

			
			if(Page.IsValid)
			{
				
				sqlConnection1.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertProductIcdCode";
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@productID", Int32.Parse(ddlProducts.SelectedValue)));
				cmdInsert.Parameters.Add(new SqlParameter("@icdCode", txtAddIcd.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@icdType", lblAddIcdType.Text));

				SqlParameter paramID = new SqlParameter("@productIcdCodeID", SqlDbType.Int);
				paramID.Direction = ParameterDirection.Output;
				cmdInsert.Parameters.Add(paramID);

				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				int intProductIcdCodeID = Int32.Parse(paramID.Value.ToString());

				foreach(ListItem i in lstCpt.Items)
				{
					if(i.Selected)
					{
						SqlCommand cmdInsertCode = new System.Data.SqlClient.SqlCommand();
						cmdInsertCode.CommandType = CommandType.StoredProcedure;
						cmdInsertCode.CommandText = "sp_InsertCptIcdCode";
						cmdInsertCode.Connection = sqlConnection1;

						cmdInsertCode.Parameters.Add(new SqlParameter("@productIcdCodeID", intProductIcdCodeID));
						cmdInsertCode.Parameters.Add(new SqlParameter("@icdCode", txtAddIcd.Text));
						cmdInsertCode.Parameters.Add(new SqlParameter("@CPT_HCPCS", i.Value));

						int rowsInsert = cmdInsertCode.ExecuteNonQuery();
					}
				} // END foreach
				
				myUtility.ICDTable = myUtility.SetTable("sp_GetProductIcdCodes", Int32.Parse(ddlProducts.SelectedValue));
				myUtility.CptIcdTable = myUtility.SetTable("sp_GetCptIcdByProduct", Int32.Parse(ddlProducts.SelectedValue));
				
				sqlConnection1.Close();

				int intRow = 0;
				for(int x=0; x<myUtility.ICDTable.Rows.Count; x++)
				{
					if(Int32.Parse(myUtility.ICDTable.Rows[x]["productIcdCodeID"].ToString()) == intProductIcdCodeID)
					{
						intRow = x;
						x = myUtility.ICDTable.Rows.Count;
					}
				}

				int intPageIndex = intRow / dgICD.PageSize;

				dgICD.CurrentPageIndex = intPageIndex;
				this.BindDT();
				Session["Utility"] = myUtility;
				this.ClearAddControls();

			} // END (Page.IsValid)
		
		}

		private int FindRecordPage(string strIcdCode)
		{
			int intRow = 0;
			for(int x=0; x<myUtility.ICDTable.Rows.Count; x++)
			{
				if(myUtility.ICDTable.Rows[x]["IcdCode"].ToString().ToUpper() == strIcdCode.ToUpper())
				{
					intRow = x;
					x = myUtility.ICDTable.Rows.Count;
				}
			}

			if(intRow != 0)
				intRow = intRow / dgICD.PageSize;

			Debug.WriteLine("Return Row = " + intRow.ToString());
			return intRow;

		} // END FindRecordPage

		private void ClearAddControls()
		{
			txtAddIcd.Text = "";
			lblAddIcdType.Text = "";
			txtAddIcdDesc.Text = "";		
			foreach(ListItem i in lstCpt.Items)
				i.Selected = false;

			txtFindIcd.Text = "";
		} // END ClearAddControls


		private void txtAddIcd_TextChanged(object sender, System.EventArgs e)
		{
			if(txtAddIcd.Text!="")
			{
				sqlConnection1.Open();
				DataTable dt = myUtility.SetTable("sp_GetIcdCode", txtAddIcd.Text);
				sqlConnection1.Close();
			
				if(dt.Rows.Count != 0)
				{
					lblAddIcdType.Text = dt.Rows[0]["icdType"].ToString();
					txtAddIcdDesc.Text = dt.Rows[0]["descr"].ToString();
				}
				else
				{
					Page.Validate();

					foreach(IValidator val in Page.Validators)
					{
						if(val.ErrorMessage == "Add - The ICD Code does not exist.")
							val.IsValid = false;
						else
							val.IsValid = true;
					} // END foreach
				}
			}
		} // END txtAddIcd_TextChanged


		protected void btnFind_Click(object sender, System.EventArgs e)
		{
			int intPageIndex = this.FindRecordPage(txtFindIcd.Text);
			Debug.WriteLine("btnFind_Click intPageIndex = " + intPageIndex.ToString());
			if(intPageIndex != 0)
			{
				Debug.WriteLine("In intPageIndex != 0 ");
				dgICD.CurrentPageIndex = intPageIndex;
				this.BindDT();
			}
			else
			{
				Page.Validate();

				foreach(IValidator val in Page.Validators)
				{
					if(val.ErrorMessage == "ICD Code for this product was not found.")
						val.IsValid = false;
					else
						val.IsValid = true;
				} // END foreach
			}
		
		} // END btnFind_Click()

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			Debug.WriteLine("ItemIndex = " + e.Item.ItemIndex.ToString());
			dgICD.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindDT();
		} // END btnEdit_Click

		protected void btnUpdate_Click(object sender, DataGridCommandEventArgs e)
		{

			int intProductIcdCodeID = Int32.Parse(dgICD.DataKeys[e.Item.ItemIndex].ToString());
			Label lblIcd = (Label)e.Item.FindControl("lblIcd");
			ListBox lstUpdateCpt = (ListBox)e.Item.FindControl("lstUpdateCpt");

			sqlConnection1.Open();
		// Delete old list od CPT codes
			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteProductIcdCpt";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@ID", intProductIcdCodeID));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

		// Insert new list of CPT codes
			foreach(ListItem i in lstUpdateCpt.Items)
			{
				if(i.Selected)
				{
					SqlCommand cmdInsertCode = new System.Data.SqlClient.SqlCommand();
					cmdInsertCode.CommandType = CommandType.StoredProcedure;
					cmdInsertCode.CommandText = "sp_InsertCptIcdCode";
					cmdInsertCode.Connection = sqlConnection1;

					cmdInsertCode.Parameters.Add(new SqlParameter("@productIcdCodeID", intProductIcdCodeID));
					cmdInsertCode.Parameters.Add(new SqlParameter("@icdCode", lblIcd.Text));
					cmdInsertCode.Parameters.Add(new SqlParameter("@CPT_HCPCS", i.Value));

					int rowsInsert = cmdInsertCode.ExecuteNonQuery();
				}
			} // END foreach
				
			myUtility.ICDTable = myUtility.SetTable("sp_GetProductIcdCodes", Int32.Parse(ddlProducts.SelectedValue));
			myUtility.CptIcdTable = myUtility.SetTable("sp_GetCptIcdByProduct", Int32.Parse(ddlProducts.SelectedValue));
			sqlConnection1.Close();

			dgICD.EditItemIndex = -1;
			this.BindDT();
			Session["Utility"] = myUtility;
 
		} // END btnUpdate_Click


		public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.EditItem)
			{
				ListBox lstUpdateCpt = (ListBox)e.Item.FindControl("lstUpdateCpt");
				lstUpdateCpt.Items.Clear();
				int intID = Int32.Parse(dgICD.DataKeys[e.Item.ItemIndex].ToString());
				DataRow[] rows = myUtility.CptIcdTable.Select("productIcdCodeID = " + intID);
			
				foreach(DataRow aRow in myUtility.CptTable.Rows)
				{
					ListItem i = new ListItem(aRow["CPT_HCPCS"].ToString(), aRow["CPT_HCPCS"].ToString());
					foreach(DataRow bRow in rows)
					{
						if(bRow["CPT_HCPCS"].ToString() == aRow["CPT_HCPCS"].ToString())
							i.Selected = true;
					}
				
					lstUpdateCpt.Items.Add(i);
				}			
			
			}
		} // END DG_ItemDataBound

 

	} // END class
}
