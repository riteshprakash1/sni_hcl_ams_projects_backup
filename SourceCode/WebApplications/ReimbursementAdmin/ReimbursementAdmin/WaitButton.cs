using System;
using System.ComponentModel;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for WaitButton.
	/// </summary>
	public class WaitButton : System.Web.UI.WebControls.Button
	{
		public WaitButton()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private string waitText = "Processing...";

		/// <summary>
		/// Gets or sets the text displayed by the control after it has been clicked.
		/// </summary>
		[DefaultValue("Processing...")]
		[Description("The text displayed by the button after it has been clicked.")]
		[Category("Appearance")]
		public string WaitText
		{
			get
			{
				return this.waitText;
			}
			set
			{
				this.waitText = value;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
		//	sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
		//	sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
			sb.AppendFormat("this.value = '{0}';", this.waitText);
			sb.Append("this.disabled = true;");
			sb.Append("document.body.style.cursor='wait';"); 
			sb.Append(this.Page.ClientScript.GetPostBackEventReference(this, string.Empty));
		//	sb.Append(";");
			this.Attributes["onclick"] = sb.ToString();	

			base.OnPreRender (e);
		}

	} // END class WaitButton
} // end namespace
