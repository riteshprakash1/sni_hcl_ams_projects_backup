<%@ Page language="c#" Codebehind="Crosswalk.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.Crosswalk" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Crosswalk</title>
		<META content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
		<script>
	    function confirm_delete() {
	        var msg
	        msg = "Are you sure you want to delete this item? Click 'OK' to\n\r"
	        msg += "continue with the delete or click 'Cancel' to abort the delete."
	        if (confirm(msg) == true)
	            return true;
	        else
	            return false;
	    }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760" valign="top">
						<table width="100%" align="center">
							<tr>
								<td width="12%">Part:&nbsp;</td>
								<td width="28%"><asp:dropdownlist id="ddlPart" runat="server" Width="150px" Font-Size="8pt"></asp:dropdownlist></td>
								<td width="15%">Description:&nbsp;</td>
								<td width="25%"><asp:textbox id="txtDescription" runat="server" Width="150px" Font-Size="8pt"></asp:textbox></td>
								<td width="20%"><asp:button id="btnSearch" runat="server" CausesValidation="False" ForeColor="White" Height="21"
										Text="Search" Width="120px" BackColor="#79BDE9" BorderWidth="0px" Font-Names="Arial ,Helvetica,sans-serif"
										Font-Size="10pt" onclick="btnSearch_Click"></asp:button></td>
							</tr>
							<tr>
								<td>Catalog #:&nbsp;</td>
								<td><asp:textbox id="txtCatalog" runat="server" Width="150px" Font-Size="8pt"></asp:textbox></td>
								<td>HCPCS Code:&nbsp;</td>
								<td><asp:dropdownlist id="ddlCPT" runat="server" Width="150px" Font-Size="8pt"></asp:dropdownlist></td>
								<td width="20%">&nbsp;</td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="Validationsummary2" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td>
									<asp:datagrid id="dgCrosswalk" runat="server" CellSpacing="0" CellPadding="5" ShowHeader="True"
										Width="100%" AutoGenerateColumns="False" ShowFooter="True" PageSize="40" AllowCustomPaging="False"
										AllowPaging="True" PagerStyle-Visible="True" OnPageIndexChanged="Page_Changed" OnSortCommand="sortHandler"
										AllowSorting="True" OnEditCommand="btnEdit_Click" OnUpdateCommand="btnUpdate_Click" OnDeleteCommand="btnDelete_Click"
										OnCancelCommand="btnAdd_Click" DataKeyField="CrosswalkID" OnItemDataBound="DG_ItemDataBound">
										<ItemStyle VerticalAlign="Top" Wrap="False"></ItemStyle>
										<HeaderStyle Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="6%">
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="50" Height="18" runat="server" Text="Update" CommandName="Update"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="40" Height="18" runat="server" Text="Add" CommandName="Cancel"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="SN Dept" SortExpression="SNDept" ItemStyle-Width="10%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblSNDept" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SNDept") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtSNDept" Font-Size="8pt" runat="server" Columns="5" MaxLength="5" Text='<%# DataBinder.Eval(Container.DataItem, "SNDept") %>'>
													</asp:textbox>
													<asp:RequiredFieldValidator id="vldSNDept" runat="server" ControlToValidate="txtSNDept" ErrorMessage="You must enter a SN Dept.">*</asp:RequiredFieldValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddSNDept" Font-Size="8pt" runat="server" Columns="5" MaxLength="5"></asp:textbox>
													<asp:RequiredFieldValidator id="vldAddSNDept" runat="server" ControlToValidate="txtAddSNDept" ErrorMessage="Add - You must enter a SN Dept.">*</asp:RequiredFieldValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Part" SortExpression="Part" ItemStyle-Width="10%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblPart" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Part") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtPart" Font-Size="8pt" runat="server" Columns="10" MaxLength="25" Text='<%# DataBinder.Eval(Container.DataItem, "Part") %>'>
													</asp:textbox>
													<asp:RequiredFieldValidator id="vldPart" runat="server" ControlToValidate="txtPart" ErrorMessage="You must enter a Part.">*</asp:RequiredFieldValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddPart" Font-Size="8pt" runat="server" Columns="10" MaxLength="25"></asp:textbox>
													<asp:RequiredFieldValidator id="vldAddPart" runat="server" ControlToValidate="txtAddPart" ErrorMessage="Add - You must enter a Part.">*</asp:RequiredFieldValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Catalog #" SortExpression="CatalogNum" ItemStyle-Width="10%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblCatalogNum" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CatalogNum") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtCatalogNum" Font-Size="8pt" runat="server" Columns="10" MaxLength="18" Text='<%# DataBinder.Eval(Container.DataItem, "CatalogNum") %>'>
													</asp:textbox>
													<asp:RequiredFieldValidator id="vldCatalogNum" runat="server" ControlToValidate="txtCatalogNum" ErrorMessage="You must enter a Catalog #.">*</asp:RequiredFieldValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddCatalogNum" Font-Size="8pt" runat="server" Columns="10" MaxLength="18"></asp:textbox>
													<asp:RequiredFieldValidator id="vldAddCatalogNum" runat="server" ControlToValidate="txtAddCatalogNum" ErrorMessage="Add - You must enter a Catalog #.">*</asp:RequiredFieldValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="Product Description" SortExpression="ProductDescription" ItemStyle-Width="45%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblProductDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProductDescription") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtProductDescription" Font-Size="8pt" Columns="70" runat="server" MaxLength="250" Text='<%# DataBinder.Eval(Container.DataItem, "ProductDescription") %>'>
													</asp:textbox>
													<asp:RequiredFieldValidator id="vldProductDescription" runat="server" ControlToValidate="txtProductDescription"
														ErrorMessage="You must enter a Product Description.">*</asp:RequiredFieldValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddProductDescription" Font-Size="8pt" Columns="70" runat="server" MaxLength="250"></asp:textbox>
													<asp:RequiredFieldValidator id="vldAddProductDescription" runat="server" ControlToValidate="txtAddProductDescription"
														ErrorMessage="Add - You must enter a Product Description.">*</asp:RequiredFieldValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn headertext="HCPCS Code" SortExpression="CPT_HCPCS" ItemStyle-Width="13%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CPT_HCPCS") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtCPT" Font-Size="8pt" runat="server" Columns="12" MaxLength="25" Text='<%# DataBinder.Eval(Container.DataItem, "CPT_HCPCS") %>'>
													</asp:textbox>
													<asp:RequiredFieldValidator id="vldCPT" runat="server" ControlToValidate="txtCPT" ErrorMessage="You must enter a HCPCS Code.">*</asp:RequiredFieldValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddCPT" Font-Size="8pt" runat="server" Columns="12" MaxLength="25"></asp:textbox>
													<asp:RequiredFieldValidator id="vldAddCPT" runat="server" ControlToValidate="txtAddCPT" ErrorMessage="Add - You must enter a HCPCS Code.">*</asp:RequiredFieldValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn ItemStyle-Width="6%">
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
								</td>
							</tr>
							<tr>
								<td><asp:Label Font-Size="8pt" id="lblSortOrder" runat="server" Visible="False"></asp:Label></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
