<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Page language="c#" Codebehind="UpdateCPTCode.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateCPTCode" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update CPT Codes</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%">
							<tr>
								<td width="40%"><asp:imagebutton id="btnFirst" runat="server" CausesValidation="False" ImageUrl="images/first.gif" onclick="btnFirst_Click"></asp:imagebutton><asp:imagebutton id="btnPrev" runat="server" CausesValidation="False" ImageUrl="images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" Font-Size="8pt" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblPageCnt" runat="server" Font-Size="8pt">Label</asp:label>&nbsp;&nbsp;<asp:imagebutton id="btnNext" runat="server" CausesValidation="False" ImageUrl="images/next.gif" onclick="btnNext_Click"></asp:imagebutton>
									<asp:imagebutton id="btnLast" runat="server" CausesValidation="False" ImageUrl="images/last.gif"
										ImageAlign="Bottom" onclick="btnLast_Click"></asp:imagebutton>
								</td>
								<td width="30%">
									<asp:button id="btnFind" runat="server" Width="90px" Height="18" Text="Find CPT Code" CommandName="Cancel"
										Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnFind_Click"></asp:button>
									<asp:TextBox id="txtFindCpt" runat="server" Columns="6" MaxLength="6" Font-Size="8pt"></asp:TextBox>
									<asp:CustomValidator id="vldIcdCodeFind" runat="server" ErrorMessage="ICD Code for this product was not found.">*</asp:CustomValidator>
								</td>
								<td align="right"><asp:label id="lblRecordCount" runat="server"></asp:label></td>
							</tr>
						</table>
						<table width="100%" align="center">
							<tr>
								<td colSpan="5"><asp:datagrid id="dgCpt" runat="server" Width="760" DataKeyField="CPT_HCPCS" 
										CellSpacing="0" CellPadding="5" ShowHeader="True" ShowFooter="True" AutoGenerateColumns="False" PagerStyle-Visible="False"
										AllowPaging="True" AllowCustomPaging="False" PageSize="20" OnEditCommand="btnEdit_Click"
										OnUpdateCommand="btnUpdate_Click" OnCancelCommand="btnAdd_Click">
										<FooterStyle VerticalAlign="Top"></FooterStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="6%">
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="50" Height="18" runat="server" Text="Update" CommandName="Update"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="40" Height="18" runat="server" Text="Add" CommandName="Cancel"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="CPT Code" HeaderStyle-Width="8%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblCPTCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CPT_HCPCS") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label Font-Size="8pt" id="lblUpdateCPTCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CPT_HCPCS") %>'>
													</asp:Label>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddCPTCode" runat="server" Font-Size="8pt" MaxLength="10" Columns="10">
													</asp:textbox>
													<asp:requiredfieldvalidator id="vldAddCPTCode" runat="server" ControlToValidate="txtAddCPTCode" ErrorMessage="Add - You must enter an CPT Code.">*</asp:requiredfieldvalidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="CPT Description" HeaderStyle-Width="86%">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblCptDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Descr") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:textbox id="txtCptDesc" runat="server" Font-Size="8pt" width=600 TextMode="MultiLine" Text='<%# DataBinder.Eval(Container.DataItem, "Descr") %>' Rows="3">
													</asp:textbox>
													<asp:requiredfieldvalidator id="vldRequiredCptDesc" runat="server" ControlToValidate="txtCptDesc" ErrorMessage="You must enter an CPT Description.">*</asp:requiredfieldvalidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddCptDesc" runat="server" Font-Size="8pt" width=600 TextMode="MultiLine" Rows="3">
													</asp:textbox>
													<asp:requiredfieldvalidator id="vldAddRequiredCptDesc" runat="server" ControlToValidate="txtAddCptDesc" ErrorMessage="Add - You must enter an CPT Description.">*</asp:requiredfieldvalidator>
												</FooterTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<table width="100%" align="center">
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
