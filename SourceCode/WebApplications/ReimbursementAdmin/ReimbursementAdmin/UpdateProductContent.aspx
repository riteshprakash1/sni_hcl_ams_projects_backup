<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Page language="c#" validateRequest="false" Codebehind="UpdateProductContent.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateProductContent" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Product Content</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%">
							<tr>
								<td width="12%"><b>Categories:</b></td>
								<td><asp:dropdownlist id="ddlCategories" runat="server" AutoPostBack="True" Width="200px" onselectedindexchanged="ddlCategories_SelectedIndexChanged"></asp:dropdownlist><asp:comparevalidator id="vldCategories" runat="server" ErrorMessage="You must select a category." ControlToValidate="ddlCategories"
										Operator="NotEqual" ValueToCompare="0">*</asp:comparevalidator></td>
								<td width="12%"><b><asp:label id="LabelProdName" runat="server">Product Name:</asp:label></b></td>
								<td><asp:textbox id="txtProdName" runat="server" MaxLength="50" Columns="25"></asp:textbox>
									<asp:dropdownlist id="ddlStatus" runat="server" Width="100px">
										<asp:ListItem Value="Draft">Draft</asp:ListItem>
										<asp:ListItem Value="Production">Production</asp:ListItem>
									</asp:dropdownlist>
									<asp:requiredfieldvalidator id="vldProdName" runat="server" ErrorMessage="You must enter a product name." ControlToValidate="txtProdName">*</asp:requiredfieldvalidator>
								</td>
								<td align="center"><asp:button id="btnAddProduct" runat="server" Width="80px" Height="18" ForeColor="#FF7300" BackColor="White"
										BorderColor="#CCCCCC" CausesValidation="False" Font-Size="8pt" Text="Add Product" onclick="btnAddProduct_Click"></asp:button></td>
							</tr>
							<tr>
								<td width="12%"><b>Products:</b></td>
								<td><asp:dropdownlist id="ddlProducts" runat="server" AutoPostBack="True" Width="200px" onselectedindexchanged="ddlProducts_SelectedIndexChanged"></asp:dropdownlist><asp:comparevalidator id="vldProduct" runat="server" ErrorMessage="You must select a product." ControlToValidate="ddlProducts"
										Operator="NotEqual" ValueToCompare="0">*</asp:comparevalidator></td>
								<td><b><asp:label id="LabelContent" runat="server">Content:</asp:label></b></td>
								<td><asp:dropdownlist id="ddlContent" runat="server" AutoPostBack="True" Width="200px" onselectedindexchanged="ddlContent_SelectedIndexChanged"></asp:dropdownlist></td>
								<td align="center"><asp:button id="btnUpdate" runat="server" Width="80px" Height="18" ForeColor="#FF7300" BackColor="White"
										BorderColor="#CCCCCC" CausesValidation="False" Font-Size="8pt" Text="Update" onclick="btnUpdate_Click"></asp:button></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td colSpan="2"><asp:textbox id="txtContent" runat="server" Columns="85" Rows="12" TextMode="MultiLine"></asp:textbox></td>
							</tr>
							<tr>
								<td align="center" colSpan="2">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" colSpan="2"><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
							</tr>
							<tr>
								<td width="520" colSpan="2"><SPAN class="MsoNormal"><asp:literal id="ltlContent" runat="server"></asp:literal></SPAN></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
