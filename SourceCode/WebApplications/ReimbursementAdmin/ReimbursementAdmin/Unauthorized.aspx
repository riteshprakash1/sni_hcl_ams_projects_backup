<%@ Page language="c#" Codebehind="Unauthorized.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.Unauthorized" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Unauthorized</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="800" align="center">
				<tr>
					<td>You are not authorized to view the requested page.</td>
				</tr>
			</table>
		</form>
	</body>
</html>
