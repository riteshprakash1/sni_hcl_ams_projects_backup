using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateTerm.
	/// </summary>
	public partial class UpdateTerm : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{

				ddlSection.Items.Clear();
				foreach(DataRow aRow in myUtility.TermSectionTable.Rows)
				{
					ddlSection.Items.Add(new ListItem(aRow["termSection"].ToString(),aRow["termSectionID"].ToString() ));
				}

				if(myUtility.TermID != 0)
				{
					this.LoadTerm();
					btnAdd.Text = "Update";
				}
				else
				{
					btnAdd.Text = "Add";
				}
			} // END isPostBack	
		
		} // END Page_Load


		private void LoadTerm()
		{
			sqlConnection1.Open();
			DataTable dtTerm = myUtility.SetTable("sp_GetTerm", myUtility.TermID);
			sqlConnection1.Close();	
	
			if(dtTerm.Rows.Count != 0)
			{
				txtTerm.Text = dtTerm.Rows[0]["term"].ToString();
				txtDefinition.Text = dtTerm.Rows[0]["termDefinition"].ToString();
				Literal1.Text = dtTerm.Rows[0]["termDefinition"].ToString();
				txtAbbreviation.Text = dtTerm.Rows[0]["termAbbreviation"].ToString();
				ddlSection.Items.FindByValue(dtTerm.Rows[0]["termSectionID"].ToString()).Selected=true;
			}
		} // END LoadTerm

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			Page.Validate();


			if(Page.IsValid)
			{
				sqlConnection1.Open();

				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = sqlConnection1;

				cmd.Parameters.Add(new SqlParameter("@term", txtTerm.Text));
				cmd.Parameters.Add(new SqlParameter("@termDefinition", txtDefinition.Text));
				cmd.Parameters.Add(new SqlParameter("@termAbbreviation", txtAbbreviation.Text));
				cmd.Parameters.Add(new SqlParameter("@termSectionID", Int32.Parse(ddlSection.SelectedValue)));
				SqlParameter paramID = new SqlParameter("@termID", SqlDbType.Int);
				cmd.Parameters.Add(paramID);

				Button btn = sender as Button;
				if(btn.Text == "Add")
				{
					cmd.CommandText = "sp_InsertTerm";
					paramID.Direction = ParameterDirection.Output;
				}
				else
				{
					cmd.CommandText = "sp_UpdateTerm";
					paramID.Value = myUtility.TermID;
				}

				int rowsUpdated = cmd.ExecuteNonQuery();

				if(btn.Text == "Add")
					myUtility.TermID = Int32.Parse(paramID.Value.ToString());

				Response.Redirect("updateTerm.Aspx");

			} // END Page.IsValid

		} // END btnAdd_Click


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
