<%@ Page language="c#" Codebehind="TermList.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.TermList" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Term List</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%">
							<tr>
								<td width="40%"><asp:imagebutton id="btnFirst" runat="server" CausesValidation="False" ImageUrl="images/first.gif" onclick="btnFirst_Click"></asp:imagebutton><asp:imagebutton id="btnPrev" runat="server" CausesValidation="False" ImageUrl="images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblPageCnt" runat="server">Label</asp:label>&nbsp;&nbsp;<asp:imagebutton id="btnNext" runat="server" CausesValidation="False" ImageUrl="images/next.gif" onclick="btnNext_Click"></asp:imagebutton>
									<asp:imagebutton id="btnLast" runat="server" CausesValidation="False" ImageUrl="images/last.gif"
										ImageAlign="Bottom" onclick="btnLast_Click"></asp:imagebutton>
								</td>
								<td width="30%">
									<asp:Button id="btnNewTerm" Width="60" Height="18" runat="server" Text="New Term" Font-Size="8pt"
										ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnNewTerm_Click"></asp:Button>
								</td>
								<td align="right"><asp:label id="lblRecordCount" runat="server">Label</asp:label></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td colSpan="5"><asp:datagrid id="dgTerm" runat="server" Width="760" OnEditCommand="btnEdit_Click" OnDeleteCommand="btnDelete_Click"
										DataKeyField="termID" CellSpacing="0" CellPadding="5" ShowHeader="True" AutoGenerateColumns="False" ShowFooter="False"
										PagerStyle-Visible="False" AllowPaging="True" AllowCustomPaging="False" PageSize="15">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="40" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Term">
												<HeaderStyle Width="68%"></HeaderStyle>
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblTerm" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "term").ToString() %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Term Section">
												<HeaderStyle Width="20%"></HeaderStyle>
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblTermSection" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "termSection") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
