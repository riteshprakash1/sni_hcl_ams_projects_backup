<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Page language="c#" Codebehind="SurveyList.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.SurveyList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Survey List</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="global.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<script>
		function confirm_delete()
		{
			var msg
			msg =  "Are you sure you want to delete this Survey? Click 'OK' to\n\r"
			msg += "continue with the delete or click 'Cancel' to abort the delete."
			if (confirm(msg)==true)
				return true;
			else
				return false;
		}
		</script>
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top" width="760">
						<table width="100%">
							<tr>
								<td>
									<asp:button id="btnNew" runat="server" Width="152px" Height="18" Text="Add New Survey" Font-Size="8pt"
										ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnNew_Click"></asp:button>
									<asp:datagrid id="dgSurveys" runat="server" OnEditCommand="btnEdit_Click" OnDeleteCommand="btnDelete_Click"
										DataKeyField="SurveyID" CellPadding="5" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-Visible="False"
										OnItemDataBound="DG_ItemDataBound">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="40" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="SurveyName" HeaderStyle-Width="35%" HeaderText="Survey Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="SurveyStatus" HeaderStyle-Width="15%" HeaderText="Status"></asp:BoundColumn>
											<asp:BoundColumn DataField="ProductName" HeaderStyle-Width="18%" HeaderText="Product"></asp:BoundColumn>
											<asp:BoundColumn DataField="StartDate" HeaderStyle-Width="10%" HeaderText="Start Date"></asp:BoundColumn>
											<asp:BoundColumn DataField="EndDate" HeaderStyle-Width="10%" HeaderText="End Date"></asp:BoundColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td><asp:Label id="lblMessage" ForeColor="#FF7300" runat="server"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
