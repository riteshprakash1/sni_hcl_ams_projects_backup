<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Page language="c#" validateRequest="false" Codebehind="UpdateFaq.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateFaq" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Faq</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%" border=1>
				<tr>
					<td width="40%"><b>Product Area:&nbsp;&nbsp;</b><asp:DropDownList id="ddlCategory" runat="server"></asp:DropDownList></td>
					<td><asp:button id="btnAdd" runat="server" Width="60" BorderColor="#CCCCCC" CausesValidation="False"
							BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Text="Add" Height="18" onclick="btnAdd_Click"></asp:button></td>
				</tr>
				<tr>
					<td colspan="2"><b>Question:</b>
						<asp:RequiredFieldValidator id="vldRequiredQuestion" runat="server" ErrorMessage="Question is a required field."
							ControlToValidate="txtQuestion">*</asp:RequiredFieldValidator>
						<br>
						<asp:TextBox id="txtQuestion" runat="server" TextMode="MultiLine" Rows="3" Columns="90"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td colspan="2"><b>Answer:</b>
						<asp:RequiredFieldValidator id="vldRequiredAnswer" runat="server" ErrorMessage="Answer is a required field."
							ControlToValidate="txtAnswer">*</asp:RequiredFieldValidator>
						<br>
						<asp:TextBox id="txtAnswer" runat="server" TextMode="MultiLine" Rows="5" Columns="90"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td colspan="2"><asp:ValidationSummary id="vldSummary" runat="server"></asp:ValidationSummary></td>
				</tr>
			</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
