using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for QuerySet.
	/// </summary>
	public partial class QuerySet : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("Query Hello");
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			Debug.WriteLine("IsPostback: " + Page.IsPostBack);
			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				myUtility.QueryParametersTable = myUtility.SetTable("sp_GetQueryParameters");
				myUtility.QueryJoinsTable = myUtility.SetTable("sp_GetQueryJoins");
				sqlConnection1.Close();	

				foreach(DataRow aRow in myUtility.QueryParametersTable.Rows)
				{
					this.lstFields.Items.Add(new ListItem(aRow["fieldHeaderName"].ToString(), aRow["fieldName"].ToString()));
				} // end foreach

				this.lblRecordsFound.Text = "";
				this.txtFromDate.Text=DateTime.Now.ToShortDateString();
				this.txtToDate.Text=DateTime.Now.ToShortDateString();

			} // end IsPostBack

		} // end Page_Load


		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			foreach(ListItem i in this.lstFields.Items)
			{
				if(i.Selected)
				{
					this.lstFieldsSelected.Items.Add(new ListItem(i.Text, i.Value));
					i.Selected=false;
				}
			}
		}

		protected void btnRemove_Click(object sender, System.EventArgs e)
		{
			ArrayList a = new ArrayList();
			foreach(ListItem i in this.lstFieldsSelected.Items)
			{
				if(i.Selected)
					a.Add(i);
			}

			for(int x=0; x<a.Count; x++)
			{
				ListItem i = (ListItem)a[x];
				this.lstFieldsSelected.Items.Remove(i);
			}
		}

		private void btnResults_Click(object sender, System.EventArgs e)
		{
			string strQuery = "SELECT DISTINCT ";
			for(int x=0; x<lstFieldsSelected.Items.Count; x++)
			{
				DataRow r = this.GetFieldNameRow(lstFieldsSelected.Items[x].Value);
				if(r!=null)
				{
					if(x == 0)
						strQuery += r["tableName"].ToString()+"."+r["fieldName"].ToString() + " as [" + r["fieldHeaderName"].ToString()+"]";
					else
						strQuery += ", " + r["tableName"].ToString()+"."+r["fieldName"].ToString() + " as [" + r["fieldHeaderName"].ToString()+"]";
				}
			}

			strQuery += this.GetJoin();
			strQuery += this.GetWhere();
			strQuery += this.GetSortOrder();
			Debug.WriteLine("Query: " + strQuery);
			this.ExecuteSearch(strQuery);
			this.BindDT();

			Session["Utility"] = myUtility;


		} //end btnResults_Click

		private string GetWhere()
		{
			string strW = "";
			if(this.txtFromDate.Text!="")
				strW = " WHERE entryDate >= '" + this.txtFromDate.Text + " 00:00:00' ";

			if(this.txtToDate.Text!="")
			{
				if(strW!="")
					strW += " AND entryDate <= '" + this.txtToDate.Text + " 23:59:00' ";
				else
					strW = " WHERE entryDate <= '" + this.txtToDate.Text + " 23:59:00' ";
			}

			return strW;
		}

		private string GetJoin()
		{
			// Get the list of tables
			ArrayList lst = new ArrayList();
			string strTable="";
			string strQuery="";
			foreach(DataRow r in myUtility.QueryParametersTable.Rows)
			{
				if(this.txtFromDate.Text!="" ||this.txtFromDate.Text!="")
				{
					Debug.WriteLine("QueryParametersTable Field: " + r["fieldName"].ToString() + "; table: " + r["tableName"].ToString());
					if(lstFieldsSelected.Items.FindByValue(r["fieldName"].ToString())!=null || r["fieldName"].ToString().ToLower()=="entrydate")
					{
						if(strTable != r["tableName"].ToString())
						{
							lst.Add(r);
							strTable = r["tableName"].ToString();
						}
					}
				}
				else
				{
					Debug.WriteLine("ELSE QueryParametersTable Field: " + r["fieldName"].ToString() + "; table: " + r["tableName"].ToString());
					if(lstFieldsSelected.Items.FindByValue(r["fieldName"].ToString())!=null)
					{
						if(strTable != r["tableName"].ToString())
						{
							lst.Add(r);
							strTable = r["tableName"].ToString();
						}
					}
				}
			}
			
			DataRow a = (DataRow)lst[0];
			string strFirstTable=a["tableName"].ToString();
			Debug.WriteLine("strFirstTable: " + strFirstTable);

			string strJoin = "";

			for(int x=0; x<lst.Count; x++)
			{
				DataRow row = (DataRow)lst[x];
		//		strTable  = row["serverName"].ToString() + "." + row["dbName"].ToString()+ ".dbo."; 
				strTable  = " ";
				strTable += row["tableName"].ToString()+ " " + row["tableName"].ToString();
				if(x==0)
				{
					strQuery += " FROM " + strTable;
				}
				else
				{
					strQuery += " JOIN " + strTable;
					
					string strCrit= "leftTableName='"+strFirstTable+"' AND rightTableName='" + row["tableName"].ToString() + "'";
					Debug.WriteLine("strCrit: " + strCrit);
					DataRow[] rows = myUtility.QueryJoinsTable.Select(strCrit);

					if(rows.Length>0)
					{
						strJoin = " ON " + rows[0]["leftTableName"].ToString()+"."+rows[0]["leftTableField1"].ToString();
						strJoin += " = " + rows[0]["rightTableName"].ToString()+"."+rows[0]["rightTableField1"].ToString();
						if(rows[0]["leftTableField2"].ToString()!="")
						{
							strJoin += " AND " + rows[0]["leftTableName"].ToString()+"."+rows[0]["leftTableField2"].ToString();
							strJoin += " = " + rows[0]["rightTableName"].ToString()+"."+rows[0]["rightTableField2"].ToString();
							if(rows[0]["leftTableField3"].ToString()!="")
							{
								strJoin += " AND " + rows[0]["leftTableName"].ToString()+"."+rows[0]["leftTableField3"].ToString();
								strJoin += " = " + rows[0]["rightTableName"].ToString()+"."+rows[0]["rightTableField3"].ToString();
							}
						}
					}

					strQuery += strJoin;
				} // end else
			} //end for loop
			Debug.WriteLine("return strQuery: " + strQuery);
			return strQuery;
		}// end GetJoin

		private string GetSortOrder()
		{
			if(lstSortOrder.Items.Count>0)
			{
				string strSort=" ORDER BY ";
				int cnt=0;
				foreach(ListItem i in this.lstSortOrder.Items)
				{
					DataRow r = GetFieldNameRow(i.Value);
					if(r!=null)
					{
						if(cnt>0)
							strSort+=", " + r["tableName"].ToString()+"."+r["fieldName"].ToString();
						else
							strSort+=r["tableName"].ToString()+"."+r["fieldName"].ToString();

						cnt++;
					}
				}// end foreach
				return strSort;
			}
			else
				return String.Empty;
		} // end GetSortOrder

		private DataRow GetFieldNameRow(string strFieldName)
		{
			string strCrit= "fieldName='"+strFieldName+"'";
			Debug.WriteLine("strCrit: " + strCrit);
			DataRow[] rows = myUtility.QueryParametersTable.Select(strCrit);
			if(rows.Length>0)
				return rows[0];
			else
			{
				DataRow r = null;
				return r;
			}

		}

		private void SetControls()
		{
			if(this.dgQuery != null && myUtility.SearchTable.Rows.Count>0)
			{
				for(int x=0; x<dgQuery.Columns.Count; x++)
				{
					DataRow r = GetFieldNameRow(dgQuery.Columns[x].HeaderText);
					if(r!=null)
						dgQuery.Columns[x].HeaderText=r["fieldHeaderName"].ToString();
				} //end for loop
			} // end if
		}//end SetControls

		private void ExecuteSearch(string strSQL)
		{
			myUtility.SearchTable = new DataTable();
			try
			{   
				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandText = strSQL;
				cmd.CommandTimeout = 300;

				sqlConnection1.Open();
				cmd.Connection = sqlConnection1;

				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(myUtility.SearchTable);

				sqlConnection1.Close();
			}
			catch(Exception exc)
			{
				Debug.WriteLine("FILL ERROR", exc.Message);
				sqlConnection1.Close();
				throw;
			}
		}

		public void BindDT()
		{
			Debug.WriteLine("Query Rows = " + myUtility.SearchTable.Rows.Count.ToString());
			this.dgQuery.DataSource = myUtility.SearchTable;
			this.dgQuery.DataBind();
			this.lblRecordsFound.Text = myUtility.SearchTable.Rows.Count.ToString() + " records found.";
			this.SetControls();
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnMoveUp_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("SelectedIndex: " + lstFieldsSelected.SelectedIndex.ToString());
			int intSelectedIndex = lstFieldsSelected.SelectedIndex;
			if(intSelectedIndex != 0)
			{
				//load the items to an array
				ArrayList itemsArray = new ArrayList();
				foreach(ListItem item in this.lstFieldsSelected.Items)
				{
					itemsArray.Add(item);
				}

				//clear the current list
				this.lstFieldsSelected.Items.Clear();

				//reload the list in the new order
				for(int i=0;i<itemsArray.Count;i++)
				{
					if(intSelectedIndex-1 == i)
					{
						this.lstFieldsSelected.Items.Add((ListItem)itemsArray[intSelectedIndex]);
						this.lstFieldsSelected.Items.Add((ListItem)itemsArray[(intSelectedIndex-1)]);
						i = intSelectedIndex;
					}
					else
					{
						this.lstFieldsSelected.Items.Add((ListItem)itemsArray[i]);
					}

				} // end for loop
			} // end if(intSelectedIndex != 0)
		}

		protected void btnAddSort_Click(object sender, System.EventArgs e)
		{
			foreach(ListItem i in this.lstFieldsSelected.Items)
			{
				if(i.Selected)
				{
					this.lstSortOrder.Items.Add(new ListItem(i.Text, i.Value));
					i.Selected=false;
				}
			}
		}

		protected void btnRemoveSort_Click(object sender, System.EventArgs e)
		{
			ArrayList a = new ArrayList();
			foreach(ListItem i in this.lstSortOrder.Items)
			{
				if(i.Selected)
					a.Add(i);
			}

			for(int x=0; x<a.Count; x++)
			{
				ListItem i = (ListItem)a[x];
				this.lstSortOrder.Items.Remove(i);
			}
		}

		protected void btnMoveUpSort_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("SelectedIndex: " + lstFieldsSelected.SelectedIndex.ToString());
			int intSelectedIndex = this.lstSortOrder.SelectedIndex;
			if(intSelectedIndex != 0)
			{
				//load the items to an array
				ArrayList itemsArray = new ArrayList();
				foreach(ListItem item in this.lstSortOrder.Items)
				{
					itemsArray.Add(item);
				}

				//clear the current list
				this.lstSortOrder.Items.Clear();

				//reload the list in the new order
				for(int i=0;i<itemsArray.Count;i++)
				{
					if(intSelectedIndex-1 == i)
					{
						this.lstSortOrder.Items.Add((ListItem)itemsArray[intSelectedIndex]);
						this.lstSortOrder.Items.Add((ListItem)itemsArray[(intSelectedIndex-1)]);
						i = intSelectedIndex;
					}
					else
					{
						this.lstSortOrder.Items.Add((ListItem)itemsArray[i]);
					}

				} // end for loop
			} // end if(intSelectedIndex != 0)
		}

		protected void btnReset_Click(object sender, System.EventArgs e)
		{
			this.lblRecordsFound.Text = "";
			this.lstFieldsSelected.Items.Clear();
			this.lstSortOrder.Items.Clear();
			foreach(ListItem i in this.lstFieldsSelected.Items)
			{
				if(i.Selected)
					i.Selected=false;
			}
		}//end btnMoveUpSort_Click

		protected void btnQuery_Click(object sender, System.EventArgs e)
		{
			string strQuery = "SELECT DISTINCT ";
			for(int x=0; x<lstFieldsSelected.Items.Count; x++)
			{
				DataRow r = this.GetFieldNameRow(lstFieldsSelected.Items[x].Value);
				if(r!=null)
				{
					if(x == 0)
						strQuery += r["tableName"].ToString()+"."+r["fieldName"].ToString() + " as [" + r["fieldHeaderName"].ToString()+"]";
					else
						strQuery += ", " + r["tableName"].ToString()+"."+r["fieldName"].ToString() + " as [" + r["fieldHeaderName"].ToString()+"]";
				}
			}
			//			} // end foreach

			strQuery += this.GetJoin();
			strQuery += this.GetWhere();
			strQuery += this.GetSortOrder();
			Debug.WriteLine("Query: " + strQuery);
			this.ExecuteSearch(strQuery);
			this.BindDT();

			Session["Utility"] = myUtility;		
		} //end btnQuery_Click

	
	
	} // end class
}
