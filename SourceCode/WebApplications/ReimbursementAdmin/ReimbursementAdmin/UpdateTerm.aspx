<%@ Page language="c#" Codebehind="UpdateTerm.aspx.cs" validateRequest="false"  AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateTerm" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Term</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:AdminHeader id="AdminHeader1" runat="server"></uc1:AdminHeader>
			<table width="900" align="center">
				<tr>
					<td width="140" valign="top" class="LeftNav">
						<uc1:LeftNav id="LeftNav1" runat="server"></uc1:LeftNav></td>
					<td width="760">
						<table width="100%" border="0">
							<tr>
								<td width="15%"><b>Term Section:&nbsp;&nbsp;</b></td>
								<td width="25%"><asp:DropDownList id="ddlSection" runat="server"></asp:DropDownList></td>
								<td><asp:button id="btnAdd" runat="server" Width="60" BorderColor="#CCCCCC" CausesValidation="False"
										BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Text="Add" Height="18" onclick="btnAdd_Click"></asp:button></td>
							</tr>
							<tr>
								<td><b>Abbreviation:</b></td>
								<td><asp:TextBox Font-Size="8pt" id="txtAbbreviation" runat="server" MaxLength="15" Columns="15"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td colspan="3"><b>Term:</b>
									<asp:RequiredFieldValidator id="vldRequiredTerm" runat="server" ErrorMessage="Term is a required field." ControlToValidate="txtTerm">*</asp:RequiredFieldValidator>
									<br>
									<asp:TextBox Font-Size="8pt" id="txtTerm" runat="server" MaxLength="200" Columns="90"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td colspan="3"><b>Definition:</b>
									<asp:RequiredFieldValidator id="vldRequiredDefinition" runat="server" ErrorMessage="Definition is a required field."
										ControlToValidate="txtDefinition">*</asp:RequiredFieldValidator>
									<br>
									<asp:TextBox id="txtDefinition" Font-Size="8pt" runat="server" TextMode="MultiLine" Rows="12"
										Columns="90"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td colspan="3"><asp:ValidationSummary id="vldSummary" runat="server"></asp:ValidationSummary></td>
							</tr>
							<tr>
								<td colspan="3">
									<asp:Literal id="Literal1" runat="server"></asp:Literal></td>
							</tr>
						</table>
				</tr>
			</table>
			</TD></TR></TABLE>
		</form>
	</body>
</HTML>
