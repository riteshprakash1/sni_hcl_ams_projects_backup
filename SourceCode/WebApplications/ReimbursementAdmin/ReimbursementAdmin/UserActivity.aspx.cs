using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UserActivity.
	/// </summary>
	public partial class UserActivity : System.Web.UI.Page
	{
		protected Utility myUtility;
		protected SqlConnection sqlConnection1;
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");		
		} // END Page_Load

		public void BindDT()
		{
			if(myUtility.ActivityTable.Rows.Count > 0)
			{
				this.lblRecords.Text = "Login Attempt Count: " + myUtility.ActivityTable.Rows.Count.ToString(); 
				dgActivity.Visible = true;
				dgActivity.DataSource = myUtility.ActivityTable;
				dgActivity.DataBind();

				dgActivityDates.Visible = true;
				dgActivityDates.DataSource = myUtility.ActivityDatesTable;
				dgActivityDates.DataBind();

				dgActivityEmail.Visible = true;
				dgActivityEmail.DataSource = myUtility.ActivityEmailTable;
				dgActivityEmail.DataBind();
			}
			else
			{
				dgActivityEmail.Visible = false;
				dgActivityDates.Visible = false;
				dgActivity.Visible = false;
				this.lblRecords.Text = "No Records found."; 
			}
		}

		public void sortHandler(object sender, DataGridSortCommandEventArgs e)
		{
			string sortBY = e.SortExpression;

			if(myUtility.ActivityTable != null)
			{
				DataRow[] rows = myUtility.ActivityTable.Select(null, sortBY);

				myUtility.ActivityTable = myUtility.ActivityTable.Clone();
				foreach (DataRow row in rows) 
				{
					myUtility.ActivityTable.ImportRow(row);
				}
				this.BindDT();

				Session["Utility"] = myUtility;
			}

		} // END sortHandler

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnActivity_Click(object sender, System.EventArgs e)
		{
			this.lblRecords.Text = "";
			Page.Validate();

			if(Page.IsValid)
			{
		
				sqlConnection1.Open();
				string strFromDate = this.txtFromDate.Text + " 00:00:01";
				string strToDate = this.txtToDate.Text + " 23:59:59"; 
				myUtility.ActivityTable = myUtility.SetTable("sp_GetHitsByDateRange", strFromDate.Trim(), strToDate.Trim());
				myUtility.ActivityDatesTable = myUtility.SetTable("sp_GetSumHitsByDate", strFromDate.Trim(), strToDate.Trim());
				myUtility.ActivityEmailTable = myUtility.SetTable("sp_GetSumHitsByEmail", strFromDate.Trim(), strToDate.Trim());

				sqlConnection1.Close();

				this.BindDT();
				Session["Utility"] = myUtility;
			}
		}// END btnActivity_Click
	
	
	} // END class
}
