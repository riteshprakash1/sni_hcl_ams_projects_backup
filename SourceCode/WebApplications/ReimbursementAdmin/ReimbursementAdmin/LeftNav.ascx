<%@ Control Language="c#" AutoEventWireup="True" Codebehind="LeftNav.ascx.cs" Inherits="ReimbursementAdmin.LeftNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table width="100%">
	<tr>
		<td>
			<asp:Button id="btnProductContent" Width="120" Height="18" runat="server" Text="Product Content"
				Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnProductContent_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnProductCPTCodes" Width="120" Height="18" runat="server" Text="Product CPT Codes"
				Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnProductCPTCodes_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnProductICDCodes" Width="120" Height="18" runat="server" Text="Product ICD Codes"
				Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnProductICDCodes_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnICDCodes" Width="120" Height="18" runat="server" Text="ICD Codes" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnICDCodes_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnUpdateCptCodes" Width="120" Height="18" runat="server" Text="CPT Codes" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnUpdateCptCodes_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnCrosswalk" Width="120" Height="18" runat="server" Text="Crosswalk" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnCrosswalk_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnFaq" Width="120" Height="18" runat="server" Text="FAQ" Font-Size="8pt" ForeColor="#FF7300"
				BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnFaq_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnLinks" Width="120" Height="18" runat="server" Text="Links" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnLinks_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnTerms" Width="120" Height="18" runat="server" Text="Terms" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnTerms_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnWhatsNew" Width="120" Height="18" runat="server" Text="What's New" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnWhatsNew_Click"></asp:Button>
		</td>
	</tr>
	<TR>
		<TD>
			<asp:Button id="btndocuments" BorderColor="#CCCCCC" CausesValidation="False" BackColor="White"
				ForeColor="#FF7300" Font-Size="8pt" Text="Documents" runat="server" Height="18" Width="120" onclick="btndocuments_Click"></asp:Button></TD>
	</TR>
	<tr>
		<td>
			<asp:Button id="btnActitviy" Width="120" Height="18" runat="server" Text="Login Activity" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnActitviy_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnQuery" Width="120" Height="18" runat="server" Text="Query" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnQuery_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnIcdDrg" Width="120" Height="18" runat="server" Text="ICD - DRG Codes" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnIcdDrg_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnDrg" Width="120" Height="18" runat="server" Text="DRG Codes" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnDrg_Click"></asp:Button>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Button id="btnSurvey" Width="120" Height="18" runat="server" Text="Surveys" Font-Size="8pt"
				ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnSurvey_Click"></asp:Button>
		</td>
	</tr>
</table>
