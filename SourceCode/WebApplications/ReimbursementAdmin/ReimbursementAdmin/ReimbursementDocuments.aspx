<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Page language="c#" Codebehind="ReimbursementDocuments.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.ReimbursementDocuments" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReimbursementDocuments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td width="760">
						<table width="760" align="center">
							<tr>
								<TD width="64"><STRONG>Products:</STRONG></TD>
								<TD><asp:dropdownlist id="ddlProducts" Font-Size=8pt runat="server" AutoPostBack="True" Width="300px"></asp:dropdownlist>
									<asp:CompareValidator id="vldProduct" runat="server" ControlToValidate="ddlProducts" ErrorMessage="You must select a product."
										ValueToCompare="0" Operator="NotEqual">*</asp:CompareValidator></TD>
								<td><asp:button id="btnAdd" CausesValidation=false runat="server" BorderColor="#CCCCCC" BackColor="White" ForeColor="#FF7300"
										Font-Size="8pt" Text="Add" Height="18" Width="100" onclick="btnAdd_Click"></asp:button></td>
							</tr>
							<TR>
								<TD><STRONG>Page:</STRONG></TD>
								<TD><asp:dropdownlist id="ddlpage" Font-Size=8pt runat="server" Width="300px" AutoPostBack="True">
										<asp:ListItem Value="1">Reference Guide</asp:ListItem>
										<asp:ListItem Value="2">Supporting Material</asp:ListItem>
										<asp:ListItem Value="3">Sample Letters</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD></TD>
							</TR>
							<tr>
								<td style="WIDTH: 64px" vAlign="top"><b>Summary:</b></td>
								<td colSpan="2"><asp:textbox id="txtSummary" runat="server" Font-Size=8pt Columns="80" MaxLength="1000" Rows="5"></asp:textbox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Width="80px" ErrorMessage="Summary is required."
										ControlToValidate="txtSummary">*</asp:RequiredFieldValidator></td>
							</tr>
							<TR>
								<TD style="WIDTH: 64px" vAlign="top"><STRONG>File:</STRONG></TD>
								<TD colSpan="2"><INPUT id="txtFile"  type="file" size="40" name="txtFile" runat="server" language="C#">
									<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="File does not exist." ControlToValidate="txtFile">*</asp:RequiredFieldValidator></TD>
							</TR>
						</table>
						<table width="760" align="center">
							<tr>
								<td colSpan="3">
								</td>
							</tr>
							<tr>
								<td colSpan="3" style="HEIGHT: 13px"></td>
							</tr>
							<tr>
								<td colSpan="3"><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary>
								</td>
							</tr>
							<tr>
								<td colSpan="3"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
