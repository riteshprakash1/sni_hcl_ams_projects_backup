using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateIcdDrgCodes.
	/// </summary>
	public partial class UpdateIcdDrgCodes : System.Web.UI.Page
	{
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			if(!Page.IsPostBack)
			{
				EnterButton.TieButton(this.txtICDCode, this.btnDrg);
			}
		}

		public void BindDT(DataTable dt)
		{
			this.dgDRG.DataSource = dt;
			this.dgDRG.DataBind();
		}

		public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.Item)
			{
				Label lblDrgCode = e.Item.FindControl("lblDrgCode") as Label;
				e.Item.Visible = Convert.ToBoolean(lblDrgCode.Text != string.Empty);
			}

			if(e.Item.ItemType==ListItemType.Footer)
			{
				Button btnAdd = e.Item.FindControl("btnAdd") as Button;
				TextBox txtDrgCode = e.Item.FindControl("txtDrgCode") as TextBox;

				EnterButton.TieButton(txtDrgCode, btnAdd); 
			}
		}


		protected void btnAddClick(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			TextBox txtDrgCode = e.Item.FindControl("txtDrgCode") as TextBox;
			CustomValidator vldValidDRGCode = e.Item.FindControl("vldValidDRGCode") as CustomValidator;
			CustomValidator vldDRGCodeExists = e.Item.FindControl("vldDRGCodeExists") as CustomValidator;

			SqlCommand cmd = new SqlCommand("sp_GetDrgCodeByID");
			cmd.Parameters.Add(new SqlParameter("@drgCode", txtDrgCode.Text));
			DataTable dt = myUtility.SqlExecuteQuery(cmd);
			vldValidDRGCode.IsValid = Convert.ToBoolean(dt.Rows.Count > 0);

			foreach(DataGridItem i in this.dgDRG.Items)
			{
				Debug.WriteLine("Index: " + i.ItemIndex.ToString() + "; Type: " + i.ItemType.ToString());
				if(i.ItemType == ListItemType.Item)
				{
					Label lblDrgCode = i.FindControl("lblDrgCode") as Label;
					if(lblDrgCode.Text.ToLower() == txtDrgCode.Text.ToLower())
					{
						vldDRGCodeExists.IsValid = false;
						break;
					}

				}
			}

			if(Page.IsValid)
			{
				Debug.WriteLine("3");
				SqlCommand cmdInsert = new SqlCommand("sp_InsertDrgCodeIcdCode");
				cmdInsert.Parameters.Add(new SqlParameter("@icdCode", this.txtICDCode.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@drgCode", txtDrgCode.Text));
			
				int rowCnt = myUtility.ExecuteNonQuery(cmdInsert);
				Debug.WriteLine("4");

				DataTable tbl = this.GetIcdDrgTable();
				Debug.WriteLine("Row Count: "+ tbl.Rows.Count.ToString());
				this.BindDT(tbl);
			}
		} // END btnEdit_Click


		protected void btnDeleteClick(object sender, DataGridCommandEventArgs e)
		{
			SqlCommand cmd = new SqlCommand("sp_DeleteDrgCodeIcdCode");
			cmd.Parameters.Add(new SqlParameter("@icdCode", this.txtICDCode.Text));
			cmd.Parameters.Add(new SqlParameter("@drgCode", this.dgDRG.DataKeys[e.Item.ItemIndex].ToString()));
			
			int rowCnt = myUtility.ExecuteNonQuery(cmd);

			this.BindDT(this.GetIcdDrgTable());
			
		} // END btnDeleteClick

		private DataTable GetIcdDrgTable()
		{
			SqlCommand cmd = new SqlCommand("sp_GetDrgCodesByIcdCode");
			cmd.Parameters.Add(new SqlParameter("@icdCode", this.txtICDCode.Text));
			return myUtility.SqlExecuteQuery(cmd);
		}

		protected void btnDrg_Click(object sender, System.EventArgs e)
		{
			DataTable dtIcd = myUtility.SetTable("sp_GetIcdCode", this.txtICDCode.Text);

			if(dtIcd.Rows.Count > 0)
			{
				DataTable tblIcdDrg = this.GetIcdDrgTable();
				this.lblICDDesc.Text = dtIcd.Rows[0]["Descr"].ToString();
				this.BindDT(tblIcdDrg);
			}
			else
			{
				this.lblICDDesc.Text = this.txtICDCode.Text + " is not a valid code.";
				this.txtICDCode.Text = "";
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


	} //// end class
}
