<%@ Page language="c#" Codebehind="DocumentList.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.DocumentList" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Document List</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body MS_POSITIONING="GridLayout">
	<script>
		function confirm_delete()
		{
			var msg
			msg =  "Are you sure you want to delete this Document item? Click 'OK' to\n\r"
			msg += "continue with the delete or click 'Cancel' to abort the delete."
			if (confirm(msg)==true)
				return true;
			else
				return false;
		}
	</script>
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top" width="760">
						<table width="100%">
							<TR>
								<TD>
									<asp:button id="btnNew" runat="server" Width="152px" Height="18" Text="Add New Document" Font-Size="8pt"
										ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnNew_Click"></asp:button>
										<asp:datagrid id="dgDocuments"  runat="server" OnEditCommand="btnEdit_Click"
										OnDeleteCommand="btnDelete_Click" DataKeyField="DocumentID" CellPadding="5" 
										AutoGenerateColumns="False" AllowPaging=false PagerStyle-Visible="False" OnItemDataBound="DG_ItemDataBound">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="40" Height="18" runat="server" Text="Edit" CommandName="Edit" 
 Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="DocPage" HeaderStyle-Width="15%" HeaderText="Page"></asp:BoundColumn>
											<asp:BoundColumn DataField="DocSummary" HeaderText="Summary"></asp:BoundColumn>
											<asp:BoundColumn DataField="ProductName" HeaderStyle-Width="15%" HeaderText="Products"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Type">
												<HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:Image Runat=server ImageUrl='<%# GetImage(DataBinder.Eval(Container.DataItem, "NameFile").ToString()) %>' ID="Image1">
													</asp:Image>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete" 
 Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
							</TR>
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
