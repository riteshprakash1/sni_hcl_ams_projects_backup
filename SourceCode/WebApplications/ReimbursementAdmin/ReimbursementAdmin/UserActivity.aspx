<%@ Page language="c#" Codebehind="UserActivity.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UserActivity" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>User Activity</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top" width="760">
						<table width="100%">
							<tr>
								<td vAlign="top" width="25%"><b>From Date:&nbsp;</b>
									<asp:textbox id="txtFromDate" runat="server" Font-Size="8pt" MaxLength="10" Columns="10"></asp:textbox><asp:requiredfieldvalidator id="vldFromDateRequiredEmpty" runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date cannot be blank.">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="vldFromDate" runat="server" Width="8px" ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})"
										ControlToValidate="txtFromDate" ErrorMessage="From Date must be a valid date (mm/dd/yyyy).">*</asp:regularexpressionvalidator></td>
								<td vAlign="top" width="25%"><b>To Date:&nbsp;</b>
									<asp:textbox id="txtToDate" runat="server" Font-Size="8pt" MaxLength="10" Columns="10"></asp:textbox><asp:requiredfieldvalidator id="vldToDateRequiredEmpty" runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date cannot be blank.">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="vldToDate" runat="server" Width="8px" ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})"
										ControlToValidate="txtToDate" ErrorMessage="To Date must be a valid date (mm/dd/yyyy).">*</asp:regularexpressionvalidator></td>
								<td vAlign="top" width="14%"><asp:button id="btnActivity" runat="server" Font-Size="8pt" BackColor="white" Text="Get Activity"
										CausesValidation="False" BorderColor="#cccccc" ForeColor="#ff7300" Height="18" Width="80" onclick="btnActivity_Click"></asp:button></td>
								<td valign="top"><asp:Label Font-Size="8pt" ForeColor="#ff7300" id="lblRecords" runat="server"></asp:Label><asp:ValidationSummary id="vldSummary1" runat="server"></asp:ValidationSummary></td>
							</tr>
							<tr>
								<td valign=top>
									<asp:datagrid id="dgActivityDates" runat="server" ShowFooter="False" AutoGenerateColumns="False"
										ShowHeader="True" CellPadding="5" CellSpacing="0" AllowSorting="True" OnSortCommand="sortHandler"
										PagerStyle-Visible="False">
										<HeaderStyle BackColor="#cccccc" Font-Bold="True" Font-Size="8pt"></HeaderStyle>
										<ItemStyle Font-Size="7pt" VerticalAlign="Top" Wrap="False"></ItemStyle>
										<Columns>
											<asp:BoundColumn DataField="HitDate" HeaderText="Date"></asp:BoundColumn>
											<asp:BoundColumn DataField="HitCount" HeaderText="Hits"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
								</td>
								<td colspan=3 valign=top>
									<asp:datagrid id="dgActivityEmail" runat="server" ShowFooter="False" AutoGenerateColumns="False"
										ShowHeader="True" CellPadding="5" CellSpacing="0" AllowSorting="True" OnSortCommand="sortHandler"
										PagerStyle-Visible="False">
										<HeaderStyle BackColor="#cccccc" Font-Bold="True" Font-Size="8pt"></HeaderStyle>
										<ItemStyle Font-Size="7pt" VerticalAlign="Top" Wrap="False"></ItemStyle>
										<Columns>
											<asp:BoundColumn DataField="EmailAddress" HeaderText="Email Address"></asp:BoundColumn>
											<asp:BoundColumn DataField="HitCount" HeaderText="Hits"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<asp:datagrid id="dgActivity" runat="server" ShowFooter="False" AutoGenerateColumns="False" ShowHeader="True"
										CellPadding="5" CellSpacing="0" AllowSorting="True" OnSortCommand="sortHandler" PagerStyle-Visible="False">
										<HeaderStyle BackColor="#cccccc" Font-Bold="True" Font-Size="8pt"></HeaderStyle>
										<ItemStyle Font-Size="7pt" VerticalAlign="Top" Wrap="False"></ItemStyle>
										<Columns>
											<asp:BoundColumn DataField="EmailAddress" SortExpression="EmailAddress" HeaderText="Email Address"></asp:BoundColumn>
											<asp:BoundColumn DataField="HitDate" SortExpression="HitDate" HeaderText="Login Date"></asp:BoundColumn>
											<asp:BoundColumn DataField="lastLogon" SortExpression="lastLogon" HeaderText="Last Logon"></asp:BoundColumn>
											<asp:BoundColumn DataField="LogHit" SortExpression="LogHit" HeaderText="Status"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
