using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for ErrorPage.
	/// </summary>
	public partial class ErrorPage : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            Debug.WriteLine("ErrorPage Page_Load");

            if (Request.QueryString["ErrorMsg"] != null)
			{
				this.lblErrorMsg.Text = "<b>Error Message:</b> " + Request.QueryString["ErrorMsg"];
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("UpdateProductContent.aspx", true);
        }
	}
}
