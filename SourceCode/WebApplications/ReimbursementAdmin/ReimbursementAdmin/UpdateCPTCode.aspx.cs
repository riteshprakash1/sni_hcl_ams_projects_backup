using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Data.SqlClient;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateCPTCode.
	/// </summary>
	public partial class UpdateCPTCode : System.Web.UI.Page
	{
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				EnterButton.TieButton(txtFindCpt, btnFind);

				myUtility.SqlConn.Open();
				myUtility.CptTable = myUtility.SetTable("sp_GetCptCodes");
				myUtility.SqlConn.Close();
				
				this.BindDT();
				Session["Utility"] = myUtility;

			}
			
		} // END Page_Load


		private void SetControls()
		{
			if(myUtility.CptTable.Rows.Count == 0)
			{
				dgCpt.Visible=false;
				btnFirst.Visible=false;
				btnPrev.Visible=false;
				ddlPage.Visible=false;
				lblPageCnt.Visible=false;;
				btnNext.Visible=false;
				btnLast.Visible=false;
				lblRecordCount.Visible=false;
			}
			else
			{
				dgCpt.Visible=true;
				btnFirst.Visible=true;
				btnPrev.Visible=true;
				ddlPage.Visible=true;
				lblPageCnt.Visible=true;;
				btnNext.Visible=true;
				btnLast.Visible=true;
				lblRecordCount.Visible=true;
			}
		
		} // END SetControls


		public void BindDT()
		{
			try
			{
				this.dgCpt.DataSource = myUtility.CptTable;
				dgCpt.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Exception: " + ex.Message);
				dgCpt.CurrentPageIndex = 0;
			}

			if(dgCpt.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgCpt.CurrentPageIndex != (dgCpt.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}


			lblRecordCount.Text = "<b><font color=red>" + Convert.ToString(myUtility.CptTable.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgCpt.PageCount.ToString();
			ddlPage.Items.Clear();
			for(int x=1;x<dgCpt.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			int myPage=dgCpt.CurrentPageIndex+1;
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

		} // END BindDT

		protected void btnAdd_Click(object sender, DataGridCommandEventArgs e)
		{
			Debug.WriteLine("In btnAdd_Click");
			
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(!val.ErrorMessage.StartsWith("Add"))
				{
					val.IsValid = true;
				}
			} // END foreach
			
			if(Page.IsValid)
			{
				TextBox txtCptDesc = (TextBox)e.Item.FindControl("txtAddCptDesc");
				TextBox txtCPTCode = (TextBox)e.Item.FindControl("txtAddCPTCode");
				
				myUtility.SqlConn.Open();

				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "sp_InsertCptCode";
				cmd.Connection = myUtility.SqlConn;

				cmd.Parameters.Add(new SqlParameter("@CPT_HCPCS", txtCPTCode.Text));
				cmd.Parameters.Add(new SqlParameter("@Descr", txtCptDesc.Text));

				int rowsUpdated = cmd.ExecuteNonQuery();

				myUtility.CptTable = myUtility.SetTable("sp_GetCptCodes");
				myUtility.SqlConn.Close();

				this.dgCpt.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;
			} // END (Page.IsValid) 
		
		} // END btnAdd_Click


		
		protected void btnUpdate_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("Add - "))
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				string strCpt = this.dgCpt.DataKeys[e.Item.ItemIndex].ToString();
				TextBox txtCptDesc = (TextBox)e.Item.FindControl("txtCptDesc");
				
				myUtility.SqlConn.Open();

				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "sp_UpdateCptCode";
				cmd.Connection = myUtility.SqlConn;

				cmd.Parameters.Add(new SqlParameter("@CPT_HCPCS", strCpt));
				cmd.Parameters.Add(new SqlParameter("@Descr", txtCptDesc.Text));

				int rowsUpdated = cmd.ExecuteNonQuery();

				myUtility.CptTable = myUtility.SetTable("sp_GetCptCodes");
				myUtility.SqlConn.Close();

				this.dgCpt.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid) 
		} // END btnUpdate_Click



		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			Debug.WriteLine("ItemIndex = " + e.Item.ItemIndex.ToString());
			dgCpt.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindDT();
		} // END btnEdit_Click

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgCpt.CurrentPageIndex = 0;
			dgCpt.EditItemIndex = -1;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgCpt.CurrentPageIndex >= 1)
				dgCpt.CurrentPageIndex = dgCpt.CurrentPageIndex - 1;
			else
				dgCpt.CurrentPageIndex = 0;

			dgCpt.EditItemIndex = -1;
			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgCpt.CurrentPageIndex != (dgCpt.PageCount-1))
				dgCpt.CurrentPageIndex = dgCpt.CurrentPageIndex + 1;
			else
				dgCpt.CurrentPageIndex = (dgCpt.PageCount-1);

			dgCpt.EditItemIndex = -1;
			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgCpt.CurrentPageIndex = (dgCpt.PageCount-1);
			dgCpt.EditItemIndex = -1;
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgCpt.CurrentPageIndex = ddlPage.SelectedIndex;
			dgCpt.EditItemIndex = -1;
			this.BindDT();
		}

		protected void btnFind_Click(object sender, System.EventArgs e)
		{
			int intPageIndex = this.FindRecordPage(this.txtFindCpt.Text);
			Debug.WriteLine("btnFind_Click intPageIndex = " + intPageIndex.ToString());
			if(intPageIndex != 0)
			{
				Debug.WriteLine("In intPageIndex != 0 ");
				dgCpt.CurrentPageIndex = intPageIndex;
				this.BindDT();
			}
			else
			{
				Page.Validate();

				foreach(IValidator val in Page.Validators)
				{
					if(val.ErrorMessage == "CPT Code for this product was not found.")
						val.IsValid = false;
					else
						val.IsValid = true;
				} // END foreach
			}
		} // END btnFind_Click()

		private int FindRecordPage(string strCode)
		{
			int intRow = 0;
			for(int x=0; x<myUtility.CptTable.Rows.Count; x++)
			{
				if(myUtility.CptTable.Rows[x]["CPT_HCPCS"].ToString().ToUpper() == strCode.ToUpper())
				{
					intRow = x;
					x = myUtility.CptTable.Rows.Count;
				}
			}

			if(intRow != 0)
				intRow = intRow / this.dgCpt.PageSize;

			Debug.WriteLine("Return Row = " + intRow.ToString());
			return intRow;
		} // END FindRecordPage

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
