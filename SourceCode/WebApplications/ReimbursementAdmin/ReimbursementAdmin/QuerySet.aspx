<%@ Register TagPrefix="uc2" Namespace="ReimbursementAdmin" Assembly="ReimbursementAdmin" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Page language="c#" Codebehind="QuerySet.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.QuerySet" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Query</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="100%" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top">
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%" align="center">
										<tr>
											<td width="9%"><b>From Date:</b></td>
											<td width="15%"><asp:TextBox id="txtFromDate" Columns="10" MaxLength="10" Font-Size="8pt" runat="server"></asp:TextBox>
												<asp:regularexpressionvalidator id="vldFromDate" runat="server" Width="8px" ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})"
													ControlToValidate="txtFromDate" ErrorMessage="From Date must be a valid date (mm/dd/yyyy).">*</asp:regularexpressionvalidator></td>
											<td width="9%"><b>To Date:</b></td>
											<td width="15%"><asp:TextBox id="txtToDate" MaxLength="10" Columns="10" Font-Size="8pt" runat="server"></asp:TextBox>
												<asp:regularexpressionvalidator id="vldToDate" runat="server" Width="8px" ValidationExpression="(1[0-2]|0?[1-9])[\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\/]((19|20)\d{2})"
													ControlToValidate="txtToDate" ErrorMessage="To Date must be a valid date (mm/dd/yyyy).">*</asp:regularexpressionvalidator>
											</td>
											<td><uc2:waitbutton id="btnQuery" runat="server" Width="110" Text="Process Report" BorderColor="#CCCCCC"
													CausesValidation="false" BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" WaitText="Processing..." onclick="btnQuery_Click"></uc2:waitbutton>
											</td>
										</tr>
										<tr>
											<td colspan="5">
												<asp:ValidationSummary id="ValidationSummary1" runat="server"></asp:ValidationSummary>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table style=" BORDER-RIGHT: #cccccc thin solid; BORDER-TOP: #cccccc thin solid; BORDER-LEFT: #cccccc thin solid; BORDER-BOTTOM: #cccccc thin solid"
										width="600" border="0">
										<tr>
											<td width="130"><b><u>Available Fields</u></b></td>
											<td width="110">&nbsp;</td>
											<td width="130"><b><u>Selected Fields</u></b></td>
											<td width="110">&nbsp;</td>
											<td width="130"><b><u>Sort Orde</u>r</b></td>
										</tr>
										<tr>
											<td vAlign="top"><asp:listbox id="lstFields" runat="server" SelectionMode="Multiple" Rows="8" Width="120"></asp:listbox></td>
											<td style="BORDER-RIGHT: #cccccc thin solid; BORDER-TOP: #cccccc thin solid; BORDER-LEFT: #cccccc thin solid"
												align="center">
												<table>
													<tr height="25">
														<td><asp:button id="btnAdd" runat="server" Width="90" Text="Add" BorderColor="#CCCCCC" CausesValidation="False"
																BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnAdd_Click"></asp:button></td>
													</tr>
													<tr height="25">
														<td align="center"><asp:button id="btnRemove" runat="server" Width="90" Text="Remove" BorderColor="#CCCCCC" CausesValidation="False"
																BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnRemove_Click"></asp:button></td>
													</tr>
													<tr height="25">
														<td align="center"><asp:button id="btnMoveUp" runat="server" Width="90" Text="Move Up" BorderColor="#CCCCCC" CausesValidation="False"
																BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnMoveUp_Click"></asp:button></td>
													</tr>
													<tr height="25">
														<td align="center"><asp:button id="btnReset" runat="server" Width="90" Text="Reset" BorderColor="#CCCCCC" CausesValidation="False"
																BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnReset_Click"></asp:button></td>
													</tr>
													<tr height="25">
														<td align="center"></td>
													</tr>
												</table>
											</td>
											<td vAlign="top"><asp:listbox id="lstFieldsSelected" runat="server" SelectionMode="Single" Rows="8" Width="120"></asp:listbox></td>
											<td style="BORDER-RIGHT: #cccccc thin solid; BORDER-TOP: #cccccc thin solid; BORDER-LEFT: #cccccc thin solid"
												vAlign="top" align="center">
												<table>
													<tr height="25">
														<td><asp:button id="btnAddSort" runat="server" Width="90" Text="Add" BorderColor="#CCCCCC" CausesValidation="False"
																BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnAddSort_Click"></asp:button></td>
													</tr>
													<tr height="25">
														<td><asp:button id="btnRemoveSort" runat="server" Width="90" Text="Remove" BorderColor="#CCCCCC"
																CausesValidation="False" BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnRemoveSort_Click"></asp:button></td>
													</tr>
													<tr height="25">
														<td><asp:button id="btnMoveUpSort" runat="server" Width="90" Text="Move Up" BorderColor="#CCCCCC"
																CausesValidation="False" BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Height="18" onclick="btnMoveUpSort_Click"></asp:button></td>
													</tr>
												</table>
											</td>
											<td vAlign="top"><asp:listbox id="lstSortOrder" runat="server" SelectionMode="Single" Rows="8" Width="120"></asp:listbox></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="bottom" colSpan="5" height="30"><asp:label id="lblRecordsFound" runat="server" ForeColor="#FF7300" Font-Size="10pt" Font-Bold="True">Label</asp:label></td>
							</tr>
							<tr>
								<td colSpan="5"><asp:datagrid id="dgQuery" runat="server" ShowHeader="True" AutoGenerateColumns="True" ShowFooter="False"
										PagerStyle-Visible="False" AllowPaging="False" AllowCustomPaging="False" PageSize="15">
										<ItemStyle Wrap="False" VerticalAlign="Top" Font-Size="8pt"></ItemStyle>
										<HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
