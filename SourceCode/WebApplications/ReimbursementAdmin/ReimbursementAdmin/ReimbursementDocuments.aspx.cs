using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for ReimbursementDocuments.
	/// </summary>
	public partial class ReimbursementDocuments : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlCategory;
		protected DataTable dtWhatsNew;
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				myUtility.ProductTable = myUtility.SetTable("sp_GetProducts");
				
				ddlProducts.Items.Clear();
				ddlProducts.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.ProductTable.Rows)
				{
					ddlProducts.Items.Add(new ListItem(aRow["productName"].ToString(),aRow["productID"].ToString() ));
				}	
              
				if(myUtility.DocumentID != 0)
				{
					dtWhatsNew = myUtility.SetTable("sp_GetDocument", myUtility.DocumentID);
					sqlConnection1.Close();
					Session["Utility"] = myUtility;
					if(dtWhatsNew.Rows.Count > 0)
						this.LoadData(dtWhatsNew.Rows[0]);

				} //end if
				sqlConnection1.Close();
			} // END !Page.IsPostBack
			
			this.SetControls();
		} // END Page_Load
		
	
		protected void SetControls()
		{
			if(myUtility.DocumentID == 0)
				this.btnAdd.Text = "Add";
			else
				this.btnAdd.Text = "Update";
		}

		protected void LoadData(DataRow aRow)

		{ 
			this.txtSummary.Text = aRow["DocSummary"].ToString();
			this.ddlpage.Items.FindByText(aRow["DocPage"].ToString()).Selected=true;

			if(this.ddlProducts.Items.FindByValue(aRow["productID"].ToString()) != null)
				this.ddlProducts.Items.FindByValue(aRow["productID"].ToString()).Selected=true;

		
		} // END LoadData

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		private void InsertDocumet()
		{
			Page.Validate();

			System.Web.UI.ValidatorCollection vals = Page.Validators;
			foreach(IValidator val in vals)
			{
				if(val.ErrorMessage.StartsWith("File"))
					val.IsValid = true;

				if(val.ErrorMessage == "File does not exist.")
				{
					if(txtFile == null)
						val.IsValid = false;
					else
						val.IsValid = (txtFile.PostedFile.ContentLength>0);
				}

			} // END foreach

			if(Page.IsValid)
			{
				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.Connection = sqlConnection1;
				cmdInsert.CommandText = "sp_InsertDocuments";

				SqlParameter paramID = new SqlParameter("@DocumentID", SqlDbType.Int);
				paramID.Direction = ParameterDirection.Output;
				cmdInsert.Parameters.Add(paramID);

				byte[] bData = this.GetByteFile(txtFile.PostedFile);
				string strName = txtFile.PostedFile.FileName;
				strName = strName.Substring(strName.LastIndexOf("\\")+1);
				strName = strName.Replace("%","").Replace("#","").Replace("@","").Replace("$","").Replace("^","");
				strName = strName.Replace("!","").Replace("*","").Replace("&","");
				
				cmdInsert.Parameters.Add(new SqlParameter("@fileData", bData));
				cmdInsert.Parameters.Add(new SqlParameter("@nameFile", strName));
				cmdInsert.Parameters.Add(new SqlParameter("@fileType", txtFile.PostedFile.ContentType));
				cmdInsert.Parameters.Add(new SqlParameter("@fileSize", txtFile.PostedFile.ContentLength));
				cmdInsert.Parameters.Add(new SqlParameter("@DocPage", ddlpage.SelectedItem.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@DocSummary", txtSummary.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@ProductID", Int32.Parse(ddlProducts.SelectedValue)));
				cmdInsert.Parameters.Add(new SqlParameter("@DocID",Int32.Parse( ddlpage.SelectedValue)));
				
				sqlConnection1.Open();
				int rowsUpdated = cmdInsert.ExecuteNonQuery();
				myUtility.DocumentID = Int32.Parse(paramID.Value.ToString());
				sqlConnection1.Close();

				Session["Utility"] = myUtility;			
			} // END Page.IsValid
		} // END InsertDocument
		
		
		private void UpdateDocument()
		{
			Debug.WriteLine("UpdateDocument");

			Page.Validate();

			System.Web.UI.ValidatorCollection vals = Page.Validators;
			foreach(IValidator val in vals)
			{
				if(val.ErrorMessage == "File does not exist.")
				{
					Debug.WriteLine("File Value: " + txtFile.Value);
					Debug.WriteLine("File Null: " + Convert.ToBoolean(txtFile.Value==null).ToString());
					Debug.WriteLine("File Empty: " + Convert.ToBoolean(txtFile.Value=="").ToString());
					if(txtFile.Value != "")
						val.IsValid = (txtFile.PostedFile.ContentLength>0);
					else
						val.IsValid = true;
				}

			} // END foreach

			if(Page.IsValid)
			{
				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = sqlConnection1;
				
				cmd.Parameters.Add(new SqlParameter("@DocumentID", myUtility.DocumentID));
				cmd.Parameters.Add(new SqlParameter("@DocPage", ddlpage.SelectedItem.Text));
				cmd.Parameters.Add(new SqlParameter("@DocSummary", txtSummary.Text));
				cmd.Parameters.Add(new SqlParameter("@ProductID", Int32.Parse(ddlProducts.SelectedValue)));
				cmd.Parameters.Add(new SqlParameter("@DocID",Int32.Parse( ddlpage.SelectedValue)));
				
				if(txtFile.Value == "")
				{
					cmd.CommandText = "sp_UpdateDocumentHeader";
				}
				else
				{
					cmd.CommandText = "sp_UpdateDocument";
				
					byte[] bData = this.GetByteFile(txtFile.PostedFile);
					string strName = txtFile.PostedFile.FileName;
					strName = strName.Substring(strName.LastIndexOf("\\")+1);
					strName = strName.Replace("%","").Replace("#","").Replace("@","").Replace("$","").Replace("^","");
					strName = strName.Replace("!","").Replace("*","").Replace("&","");

					cmd.Parameters.Add(new SqlParameter("@fileData", bData));
					cmd.Parameters.Add(new SqlParameter("@nameFile", strName));
					cmd.Parameters.Add(new SqlParameter("@fileType", txtFile.PostedFile.ContentType));
					cmd.Parameters.Add(new SqlParameter("@fileSize", txtFile.PostedFile.ContentLength));
				}

				sqlConnection1.Open();
				int rowsUpdated = cmd.ExecuteNonQuery();
				sqlConnection1.Close();

				Session["Utility"] = myUtility;			
			} // END Page.IsValid

		} //END UpdateDocument

		
		
		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			Button btn = sender as Button;
			if(btn.Text == "Add")
				this.InsertDocumet();
			else
				this.UpdateDocument();

		} // END btnAdd_Click


		protected byte[] GetByteFile(HttpPostedFile myFile)
		{
			// Get size of uploaded file
			int nFileLen = myFile.ContentLength; 
			byte[] myData = null;

			// make sure the size of the file is > 0
			if( nFileLen > 0 )
			{
				// Allocate a buffer for reading of the file
				myData = new byte[nFileLen];

				// Read uploaded file from the Stream
				myFile.InputStream.Read(myData, 0, nFileLen);

			} //END 

			return myData;
		} // END GetByteFile


		public string GetImage(string fileName)
		{
			int intP = fileName.LastIndexOf('.');
			string ext = fileName.Substring(intP+1, 3).ToLower();
	//		Debug.WriteLine("Ext = " + ext);
			switch(ext)       
			{         
				case "doc":   
					return "images\\word16.gif";
				case "xls":            
					return "images\\excel16.gif";
				case "txt":            
					return "images\\text16.gif";
				case "pdf":            
					return "images\\acrobat16.gif";
				case "ppt":            
					return "images\\mspowerpoint16.gif";
				default:            
					return "images\\text16.gif";
			}

		} // END GetImage

	} // END class
}

