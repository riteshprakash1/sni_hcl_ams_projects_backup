<%@ Page language="c#" Codebehind="WhatsNewDetail.aspx.cs" validateRequest="false" AutoEventWireup="True" Inherits="ReimbursementAdmin.WhatsNewDetail" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>What's New Detail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
		<script>
		function confirm_send()
		{
			var msg
			msg =  "Are you sure you want to send the email notification? Click 'OK' to\n\r"
			msg += "continue or click 'Cancel' to abort."
			if (confirm(msg)==true)
				return true;
			else
				return false;
		}
		</script>
	</HEAD>
	<BODY>
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td width="760">
						<table width="760" align="center">
							<tr>
								<td><b>Product Area:&nbsp;&nbsp;</b></td>
								<td><asp:dropdownlist id="ddlCategory" runat="server"></asp:dropdownlist></td>
								<td><asp:button id="btnAdd" runat="server" BorderColor="#CCCCCC" CausesValidation="False" BackColor="White"
										ForeColor="#FF7300" Font-Size="8pt" Text="Add" Height="18" Width="100" onclick="btnAdd_Click"></asp:button></td>
							</tr>
							<TR>
								<TD><STRONG>Status:</STRONG></TD>
								<TD><asp:dropdownlist id="ddbstatus" runat="server" Width="80px" AutoPostBack="True">
										<asp:ListItem Value="Enable">Enable</asp:ListItem>
										<asp:ListItem Value="Disable">Disable</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD><asp:button id="btnSendEmail" runat="server" BorderColor="#CCCCCC" CausesValidation="False"
										BackColor="White" ForeColor="#FF7300" Font-Size="8pt" Text="Send Email" Height="18" Width="100" onclick="btnSendEmail_Click"></asp:button></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 14px"><STRONG>Sort Order:</STRONG></TD>
								<TD style="HEIGHT: 14px"><asp:textbox id="txtsort" runat="server" Width="72px"></asp:textbox></TD>
								<TD style="HEIGHT: 14px"></TD>
							</TR>
							<TR>
								<TD><B>Heading:</B></TD>
								<TD colSpan="2"><asp:textbox id="txtHeading" runat="server" Columns="100" MaxLength="100"></asp:textbox><asp:requiredfieldvalidator id="vldHeading" runat="server" ErrorMessage="Heading is a required field." ControlToValidate="txtHeading">*</asp:requiredfieldvalidator></TD>
							</TR>
							<tr>
								<td vAlign="top"><b>Summary:</b></td>
								<td colSpan="2"><asp:textbox id="txtSummary" runat="server" Columns="80" MaxLength="1000" Rows="5" TextMode="MultiLine"></asp:textbox></td>
							</tr>
							<tr>
								<td vAlign="top"><b>Detail:</b></td>
								<td colSpan="2"><asp:textbox id="txtDetail" runat="server" Columns="80" Rows="10" TextMode="MultiLine"></asp:textbox></td>
							</tr>
						</table>
						<table width="760" align="center">
							<tr>
								<td colSpan="3">
									<hr color="#666666">
								</td>
							</tr>
							<tr>
								<td colSpan="3"><asp:label id="lblAttach" Font-Size="10pt" Runat="server" Font-Bold="True">Attachments</asp:label></td>
							</tr>
							<tr>
								<td colSpan="3"><asp:datagrid id="dgFiles" runat="server" Width="760" PageSize="10" AllowCustomPaging="False"
										AllowPaging="True" PagerStyle-Visible="False" ShowFooter="false" AutoGenerateColumns="False" ShowHeader="True"
										CellPadding="5" CellSpacing="0" DataKeyField="whatsNewFileID" OnDeleteCommand="btnDelete_Click">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="8%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="File">
												<ItemTemplate>
													<asp:Label ID="lblNameFile" Runat="server" Font-Size="8pt" Text='<%# DataBinder.Eval(Container.DataItem, "nameFile").ToString() %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Type" ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="4%" HorizontalAlign="Center"></HeaderStyle>
												<ItemTemplate>
													<asp:Image Runat=server ImageUrl='<%# GetImage(DataBinder.Eval(Container.DataItem, "nameFile").ToString()) %>' >
													</asp:Image>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td colSpan="3">
									<table cellSpacing="4" cellPadding="4">
										<tr>
											<td width="6%"><asp:button id="btnAddFile" runat="server" BorderColor="#CCCCCC" CausesValidation="False" BackColor="White"
													ForeColor="#FF7300" Font-Size="8pt" Text="Add" Height="18" Width="40" CommandName="Cancel" onclick="btnAddFile_Click"></asp:button></td>
											<td><input id="txtFile" type="file" size="40" name="txtFile" runat="server">
												<asp:customvalidator id="vldFileExist" runat="server" ErrorMessage="File does not exist." ControlToValidate="txtFile">*</asp:customvalidator></td>
											<td width="4%">&nbsp;</td>
										</tr>
									</table>
									<asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
							<tr>
								<td colSpan="3"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
