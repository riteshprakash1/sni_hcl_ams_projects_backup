using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateFaq.
	/// </summary>
	public partial class UpdateFaq : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{

				ddlCategory.Items.Clear();
				ddlCategory.Items.Add(new ListItem("All","99"));
//				ddlCategory.Items.Add(new ListItem("Front Page","0"));
				foreach(DataRow aRow in myUtility.ProductCategoryTable.Rows)
				{
					ddlCategory.Items.Add(new ListItem(aRow["productCategoryName"].ToString(),aRow["productCategoryID"].ToString() ));
				}

				if(myUtility.FaqID != 0)
				{
					this.LoadFaq();
					btnAdd.Text = "Update";
				}
				else
				{
					btnAdd.Text = "Add";
				}
			} // END isPostBack	

		} // END Page_Load


		private void LoadFaq()
		{
			sqlConnection1.Open();
			DataTable dtFaq = myUtility.SetTable("sp_GetFaq", myUtility.FaqID);
			sqlConnection1.Close();	
	
			if(dtFaq.Rows.Count != 0)
			{
				txtAnswer.Text = dtFaq.Rows[0]["faqAnswer"].ToString();
				Debug.WriteLine("DB Answer string = " +dtFaq.Rows[0]["faqAnswer"].ToString());
				Debug.WriteLine("DB Question string = " +dtFaq.Rows[0]["faqQuestion"].ToString());
				txtQuestion.Text = dtFaq.Rows[0]["faqQuestion"].ToString();
				if(ddlCategory.Items.FindByValue(dtFaq.Rows[0]["categoryID"].ToString())!=null)
					ddlCategory.Items.FindByValue(dtFaq.Rows[0]["categoryID"].ToString()).Selected=true;
			}
		} // END LoadFaq


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			Page.Validate();


			if(Page.IsValid)
			{
				sqlConnection1.Open();

				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = sqlConnection1;

				cmd.Parameters.Add(new SqlParameter("@faqQuestion", txtQuestion.Text));
				cmd.Parameters.Add(new SqlParameter("@faqAnswer", txtAnswer.Text));
				cmd.Parameters.Add(new SqlParameter("@categoryID", Int32.Parse(ddlCategory.SelectedValue)));
				SqlParameter paramID = new SqlParameter("@faqID", SqlDbType.Int);
				cmd.Parameters.Add(paramID);

				Button btn = sender as Button;
				if(btn.Text == "Add")
				{
					cmd.CommandText = "sp_InsertFaq";
					paramID.Direction = ParameterDirection.Output;
				}
				else
				{
					cmd.CommandText = "sp_UpdateFaq";
					paramID.Value = myUtility.FaqID;
				}
				
				int rowsUpdated = cmd.ExecuteNonQuery();

				if(btn.Text == "Add")
					myUtility.FaqID = Int32.Parse(paramID.Value.ToString());

				Response.Redirect("updateFaq.Aspx");

			} // END Page.IsValid

		} // END btnAdd_Click

	} // END class
}
