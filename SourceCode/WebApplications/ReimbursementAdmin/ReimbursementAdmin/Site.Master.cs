﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReimbursementAdmin
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProductContent_Click1(object sender, EventArgs e)
        {
             Response.Redirect("UpdateProductContent.aspx", true);
        }

        protected void btnProductCPTCodes_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateProductCodes.aspx", true);
        }

        protected void btnProductICDCodes_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateProductIcdCodes.aspx", true);

        }

        protected void btnICDCodes_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateIcdCodes.aspx", true);
        }

        protected void btnUpdateCptCodes_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateCptCode.aspx", true);
        }

        protected void btnCrosswalk_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Crosswalk.aspx", true);
        }

        protected void btnFaq_Click1(object sender, EventArgs e)
        {
            Response.Redirect("faqList.aspx", true);
        }

        protected void btnLinks_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateLinks.aspx", true);
        }

        protected void btnTerms_Click1(object sender, EventArgs e)
        {
            Response.Redirect("TermList.aspx", true);
        }

        protected void btnWhatsNew_Click1(object sender, EventArgs e)
        {
            Response.Redirect("WhatsNewList.aspx", true);
        }

        protected void btndocuments_Click1(object sender, EventArgs e)
        {
            Response.Redirect("DocumentList.aspx", true);
        }

        protected void btnActitviy_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UserActivity.aspx", true);
        }

        protected void btnQuery_Click1(object sender, EventArgs e)
        {
            Response.Redirect("QuerySet.aspx", true);
        }

        protected void btnIcdDrg_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateIcdDrgCodes.aspx", true);
        }

        protected void btnDrg_Click1(object sender, EventArgs e)
        {
            Response.Redirect("UpdateDrgCodes.aspx", true);
        }

        protected void btnSurvey_Click1(object sender, EventArgs e)
        {
            Response.Redirect("SurveyList.aspx", true);
        }



    }
}