using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Data.SqlClient;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateDrgCodes.
	/// </summary>
	public partial class UpdateDrgCodes : System.Web.UI.Page
	{
		protected Utility myUtility;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			if(!Page.IsPostBack)
			{
				this.BindDT();
			}
		}

		public void BindDT()
		{
			this.dgDRG.DataSource =this.GetDrgTable();
			this.dgDRG.DataBind();
		}

		public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.EditItem)
			{
				Button btnUpdate = e.Item.FindControl("btnUpdate") as Button;
				TextBox txtDrgDesc = e.Item.FindControl("txtDrgDesc") as TextBox;

				EnterButton.TieButton(txtDrgDesc, btnUpdate); 
			}

			if(e.Item.ItemType==ListItemType.Footer)
			{
				Button btnAdd = e.Item.FindControl("btnAdd") as Button;
				TextBox txtDrgCode = e.Item.FindControl("txtDrgCode") as TextBox;
				TextBox txtAddDrgDesc = e.Item.FindControl("txtAddDrgDesc") as TextBox;

				EnterButton.TieButton(txtDrgCode, btnAdd); 
				EnterButton.TieButton(txtAddDrgDesc, btnAdd); 
			}
		}
		
		protected void btnAddClick(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			TextBox txtDrgCode = e.Item.FindControl("txtDrgCode") as TextBox;
			TextBox txtAddDrgDesc = e.Item.FindControl("txtAddDrgDesc") as TextBox;
			CustomValidator vldDRGCodeExists = e.Item.FindControl("vldDRGCodeExists") as CustomValidator;

			SqlCommand cmd = new SqlCommand("sp_GetDrgCodeByID");
			cmd.Parameters.Add(new SqlParameter("@drgCode", txtDrgCode.Text));
			DataTable dt = myUtility.SqlExecuteQuery(cmd);
			vldDRGCodeExists.IsValid = Convert.ToBoolean(dt.Rows.Count == 0);

			if(Page.IsValid)
			{
				Debug.WriteLine("3");
				SqlCommand cmdInsert = new SqlCommand("sp_InsertDrgCode");
				cmdInsert.Parameters.Add(new SqlParameter("@drgDesc", txtAddDrgDesc.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@drgCode", txtDrgCode.Text));
			
				int rowCnt = myUtility.ExecuteNonQuery(cmdInsert);

				this.BindDT();
			}
		} // END btnAddClick

		protected void btnDeleteClick(object sender, DataGridCommandEventArgs e)
		{
			SqlCommand cmd = new SqlCommand("sp_DeleteDrgCode");
			cmd.Parameters.Add(new SqlParameter("@drgCode", this.dgDRG.DataKeys[e.Item.ItemIndex].ToString()));
			
			int rowCnt = myUtility.ExecuteNonQuery(cmd);

			this.BindDT();
			
		} // END btnDeleteClick

		private DataTable GetDrgTable()
		{
			SqlCommand cmd = new SqlCommand("sp_GetDrgCodes");
			return myUtility.SqlExecuteQuery(cmd);
		}

		protected void btnUpdateClick(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(!val.ErrorMessage.StartsWith("Update - "))
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				string strCpt = this.dgDRG.DataKeys[e.Item.ItemIndex].ToString();
				TextBox txtDrgDesc = (TextBox)e.Item.FindControl("txtDrgDesc");

				SqlCommand cmdUpdate = new SqlCommand("sp_UpdateDrgCode");
				cmdUpdate.Parameters.Add(new SqlParameter("@drgDesc", txtDrgDesc.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@drgCode", this.dgDRG.DataKeys[e.Item.ItemIndex].ToString()));
			
				int rowCnt = myUtility.ExecuteNonQuery(cmdUpdate);

				this.dgDRG.EditItemIndex = -1;
				this.BindDT();

			} // END (Page.IsValid) 
		} // END btnUpdate_Click

		protected void btnEditClick(object sender, DataGridCommandEventArgs e)
		{
			this.dgDRG.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindDT();
		} // END btnEdit_Click

		//Implement the EventHandler
		protected void GridPageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.dgDRG.CurrentPageIndex = e.NewPageIndex;
			//Bind the DataGrid again with the Data Source 
			this.BindDT();
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
