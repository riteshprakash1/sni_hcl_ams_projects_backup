using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateLinks.
	/// </summary>
	public partial class UpdateLinks : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected DataTable dtCategories;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{

			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			this.CreateTable();

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				myUtility.LinksTable = myUtility.SetTable("sp_GetLinkList");
				sqlConnection1.Close();

				this.BindDT();
			} // END !Page.IsPostBack

		}// END Page_Load


		private void CreateTable()
		{
			dtCategories = new DataTable();

			// Define the columns of the table.
			dtCategories.Columns.Add(new DataColumn("CatName", typeof(String)));
			dtCategories.Columns.Add(new DataColumn("CatValue", typeof(String)));

			DataRow bRow = dtCategories.NewRow();
			bRow["CatName"] = "All";
			bRow["CatValue"] = "99";
			dtCategories.Rows.Add(bRow);

			DataRow cRow = dtCategories.NewRow();
			cRow["CatName"] = "Front Page";
			cRow["CatValue"] = "0";
			dtCategories.Rows.Add(cRow);

			foreach(DataRow aRow in myUtility.ProductCategoryTable.Rows)
			{
				DataRow newRow = dtCategories.NewRow();
				newRow["CatName"] = aRow["productCategoryName"].ToString();
				newRow["CatValue"] = aRow["productCategoryID"].ToString();
				dtCategories.Rows.Add(newRow);
			}



		} // END CreateTable


		public DataTable GetCategoryTable()
		{
			return dtCategories;
		}


		public string GetCategoryName(string strCatValue)
		{
			string strCrit = "CatValue=" + strCatValue;
			Debug.WriteLine("Crit="+strCrit);
			DataRow[] catRows = dtCategories.Select(strCrit);
			if(catRows.Length != 0)
				return catRows[0]["CatName"].ToString();
			else
				return "";
		}
		
		public int GetCategoryIndex(string strCategoryID)
		{
			for(int x=0; x<dtCategories.Rows.Count; x++)
			{
				if(Int32.Parse(dtCategories.Rows[x]["CatValue"].ToString()) == Int32.Parse(strCategoryID))
					return x;
			}
			return 0;
		}


		public void BindDT()
		{
			dgLinks.DataSource = myUtility.LinksTable;
			dgLinks.DataBind();
		} // END 


		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteLink";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@linkID", Int32.Parse(dgLinks.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.LinksTable = myUtility.SetTable("sp_GetLinkList");
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click



		protected void btnAdd_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			ValidatorCollection vals = Page.Validators;
			foreach(IValidator val in vals)																																
			{
				if(!val.ErrorMessage.StartsWith("Add"))
				{
					val.IsValid = true;
				}
			} // END Foreach

			if(Page.IsValid)			
			{
				
				TextBox txtAddUrl = (TextBox)e.Item.FindControl("txtAddUrl");
				TextBox txtAddLinkName = (TextBox)e.Item.FindControl("txtAddLinkName");
				DropDownList ddlAddCategory = (DropDownList)e.Item.FindControl("ddlAddCategory");

				sqlConnection1.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertLink";
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@linkUrl", txtAddUrl.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@linkName", txtAddLinkName.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@categoryID", Int32.Parse(ddlAddCategory.SelectedValue)));

				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				myUtility.LinksTable = myUtility.SetTable("sp_GetLinkList");
				sqlConnection1.Close();
				dgLinks.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid)
		
		} // END btnAdd_Click



		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			dgLinks.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindDT();
		} // END btnEdit_Click

		protected void btnUpdate_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			ValidatorCollection vals = Page.Validators;
			foreach(IValidator val in vals)																																
			{
				if(!val.ErrorMessage.StartsWith("Update"))
				{
					val.IsValid = true;
				}
			} // END Foreach

			if(Page.IsValid)			
			{
				TextBox txtUrl = (TextBox)e.Item.FindControl("txtUrl");
				TextBox txtLinkName = (TextBox)e.Item.FindControl("txtLinkName");
				DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
				int linkID = Int32.Parse(dgLinks.DataKeys[e.Item.ItemIndex].ToString());

				sqlConnection1.Open();

				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "sp_UpdateLink";
				cmd.Connection = sqlConnection1;

				cmd.Parameters.Add(new SqlParameter("@linkUrl", txtUrl.Text));
				cmd.Parameters.Add(new SqlParameter("@linkName", txtLinkName.Text));
				cmd.Parameters.Add(new SqlParameter("@linkID", linkID));
				cmd.Parameters.Add(new SqlParameter("@categoryID", Int32.Parse(ddlCategory.SelectedValue)));

				int rowsUpdated = cmd.ExecuteNonQuery();

				myUtility.LinksTable = myUtility.SetTable("sp_GetLinkList");
				sqlConnection1.Close();
				dgLinks.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid)
 
		} // END btnUpdate_Click




		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
