﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Configuration;
using System.Net.Mail;


namespace ReimbursementAdmin
{
    public partial class EmailTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.txtEmailTo.Text = ConfigurationManager.AppSettings["logEmailAddresses"];
                this.txtSubject.Text = "TEST Email from Smith & Nephew Reimbursement Web Site";
                this.txtBody.Text = "This is a test";
            }

            this.lblMessage.Text = string.Empty;
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            this.Validate();
            if (Page.IsValid)
            {
                this.sendMail(this.txtEmailTo.Text, this.txtBody.Text, this.txtSubject.Text);
            }

        }

        private void sendMail(string strTo, string strBody, string strSubject)
        {
            try
            {
                MailAddress from = new MailAddress(ConfigurationManager.AppSettings["SiteEmailFromAddress"]);
                MailAddress to = new MailAddress(strTo);
                MailMessage message = new MailMessage(from, to);

                message.Subject = strSubject;
                message.IsBodyHtml = false;

                message.Body = strBody;

                SmtpClient client = new SmtpClient();
                client.Send(message);

                this.lblMessage.Text = "Email sent successfully";
            }
            catch (Exception exc)
            {
                this.lblMessage.Text = "Email send FAILED!";
                Debug.WriteLine("sendMail Error: " + exc.Message);
                throw;
            }
        }
    }
}