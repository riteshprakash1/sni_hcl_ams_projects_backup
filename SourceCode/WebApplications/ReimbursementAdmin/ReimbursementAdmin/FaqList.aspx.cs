using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for FaqList.
	/// </summary>
	public partial class FaqList : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();
				myUtility.FaqsTable = myUtility.SetTable("sp_GetFaqs");
				sqlConnection1.Close();

				Debug.WriteLine("Rows = " + myUtility.FaqsTable.Rows.Count.ToString());
				if(myUtility.FaqsTable.Rows.Count > 0)
					this.BindDT();

				this.SetControls();
			}
		
		}


		private void SetControls()
		{
			bool blnVisible = Convert.ToBoolean(myUtility.FaqsTable.Rows.Count > 0);
			Debug.WriteLine("bool = " + blnVisible.ToString());
			dgFaq.Visible = blnVisible;
			btnFirst.Visible = blnVisible;
			btnPrev.Visible = blnVisible;
			ddlPage.Visible = blnVisible;
			lblPageCnt.Visible = blnVisible;
			btnNext.Visible = blnVisible;
			btnLast.Visible = blnVisible;
			lblRecordCount.Visible = blnVisible;
		
		} // END SetControls


		public void BindDT()
		{
			try
			{
				dgFaq.DataSource = myUtility.FaqsTable;
				dgFaq.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Exception: " + ex.Message);
				dgFaq.CurrentPageIndex = 0;
			}

			if(dgFaq.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgFaq.CurrentPageIndex != (dgFaq.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}


			lblRecordCount.Text = "<b><font color=red>" + Convert.ToString(myUtility.FaqsTable.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgFaq.PageCount.ToString();
			Debug.WriteLine("PageCount = " + dgFaq.PageCount.ToString());
			ddlPage.Items.Clear();
			for(int x=1;x<dgFaq.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			
			int myPage=dgFaq.CurrentPageIndex+1;
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

		} // END BindDT

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgFaq.CurrentPageIndex = 0;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgFaq.CurrentPageIndex >= 1)
				dgFaq.CurrentPageIndex = dgFaq.CurrentPageIndex - 1;
			else
				dgFaq.CurrentPageIndex = 0;

			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgFaq.CurrentPageIndex != (dgFaq.PageCount-1))
				dgFaq.CurrentPageIndex = dgFaq.CurrentPageIndex + 1;
			else
				dgFaq.CurrentPageIndex = (dgFaq.PageCount-1);

			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgFaq.CurrentPageIndex = (dgFaq.PageCount-1);
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgFaq.CurrentPageIndex = ddlPage.SelectedIndex;
			this.BindDT();
		}

		public string GetQuestion(string q)
		{
			string strQuestion = Server.HtmlEncode(q);
			strQuestion = strQuestion.Replace("\r\n", "<br>");
			return strQuestion;
		}

		public string GetProductArea(string p)
		{
			Debug.WriteLine("p="+p);
			string filter = "productCategoryID = " + p;
			DataRow[] rows = myUtility.ProductCategoryTable.Select(filter);
			if(rows.Length==0)
			{
				return "All";
			}
			else
				return rows[0]["productCategoryName"].ToString();

		}

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{

			myUtility.FaqID = Int32.Parse(dgFaq.DataKeys[e.Item.ItemIndex].ToString());
			Session["Utility"] = myUtility;
			Response.Redirect("updateFaq.aspx");
		} // END btnEdit_Click


		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteFaq";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@faqID", Int32.Parse(dgFaq.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.FaqsTable = myUtility.SetTable("sp_GetFaqs");
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnNewFaq_Click(object sender, System.EventArgs e)
		{
			myUtility.FaqID = 0;
			Session["Utility"] = myUtility;
			Response.Redirect("UpdateFaq.aspx");
		}
	} // END class
}
