using System;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Configuration;
using System.IO;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for Utility.
	/// </summary>
	public class Utility
	{
		protected bool blnAdmin;
		protected DataTable dtProductCategory;
		protected int intWhatsNewID;
        protected int intDocumentID;
		protected int intDocID;
		protected int intFaqID;
		protected int intTermID;
		protected int intProductCategoryID;
		protected DataTable dtFaqs;
		protected SqlConnection sqlConnection1;
		protected DataTable dtICD;
		protected DataTable dtProductInfo;
		protected DataTable dtContentType;
		protected DataTable dtProduct;
		protected DataTable dtCptIcd;
		protected DataTable dtCpt;
		protected DataTable dtLinks;
		protected DataTable dtTerms;
		protected DataTable dtTermSection;
		protected DataTable dtWhatsNewFiles;
		protected DataTable dtDocumentFiles;
		protected DataTable dtPublishedLit;
		protected DataTable dtActivity;
		protected DataTable dtActivityDates;
		protected DataTable dtActivityEmail;
		protected DataTable dtQueryParameters;
		protected DataTable dtSearch;
		protected DataTable dtQueryJoins;
		protected DataTable dtPublication;
		protected string strPublicationFolder;
		protected string strADAccount;
		protected string strUserEmail;

		public Utility()
		{
			this.WhatsNewID = 0;
			this.DocumentID = 0;
			this.DocID = 0;
			this.TermID = 0;
			this.FaqID = 0;
			this.ProductCategoryID = 0;
			blnAdmin = false;
			this.LoadTables();

			string strConn = ConfigurationManager.AppSettings["sqlConn"].ToString();
			this.SqlConn = new SqlConnection();
			this.SqlConn.ConnectionString = strConn;
			
		} // END  constructor

		/********************* Getters and Setters ***************************/
		
		public string ADAccount
		{
			get {return strADAccount;}
			set {strADAccount = value;}
		}

		public string UserEmail
		{
			get {return strUserEmail;}
			set {strUserEmail = value;}
		}
		
		public SqlConnection SqlConn
		{
			get {return sqlConnection1;}
			set {sqlConnection1 = value;}
		}
		
		public int WhatsNewID
		{
			get {return intWhatsNewID;}
			set {intWhatsNewID = value;}
		}
		public int DocumentID
		{
			get {return intDocumentID;}
			set {intDocumentID = value;}
		}

		public int DocID
		{
			get {return intDocID;}
			set {intDocID = value;}
		}
		
		public DataTable ActivityTable
		{
			get {return dtActivity;}
			set {dtActivity = value;}
		}

		public DataTable QueryParametersTable
		{
			get {return dtQueryParameters;}
			set {dtQueryParameters = value;}
		}

		public DataTable QueryJoinsTable
		{
			get {return dtQueryJoins;}
			set {dtQueryJoins = value;}
		}

		public DataTable SearchTable
		{
			get {return dtSearch;}
			set {dtSearch = value;}
		}

		public DataTable ActivityDatesTable
		{
			get {return dtActivityDates;}
			set {dtActivityDates = value;}
		}

		public DataTable ActivityEmailTable
		{
			get {return dtActivityEmail;}
			set {dtActivityEmail = value;}
		}

		public DataTable WhatsNewFilesTable
		{
			get {return dtWhatsNewFiles;}
			set {dtWhatsNewFiles = value;}
		}
		public DataTable DocumentFilesTable
		{
			get {return dtDocumentFiles;}
			set {dtDocumentFiles = value;}
		}

		public DataTable PublishedLitTable
		{
			get {return dtPublishedLit;}
			set {dtPublishedLit = value;}
		}

		public DataTable TermSectionTable
		{
			get {return dtTermSection;}
			set {dtTermSection = value;}
		}

		public DataTable TermsTable
		{
			get {return dtTerms;}
			set {dtTerms = value;}
		}

		public DataTable LinksTable
		{
			get {return dtLinks;}
			set {dtLinks = value;}
		}

		public DataTable CptTable
		{
			get {return dtCpt;}
			set {dtCpt = value;}
		}

		public DataTable CptIcdTable
		{
			get {return dtCptIcd;}
			set {dtCptIcd = value;}
		}
		
		public DataTable ContentTypeTable
		{
			get {return dtContentType;}
			set {dtContentType = value;}
		}

		public DataTable ProductTable
		{
			get {return dtProduct;}
			set {dtProduct = value;}
		}

		public DataTable ProductInfoTable
		{
			get {return dtProductInfo;}
			set {dtProductInfo = value;}
		}

		public DataTable PublicationTable
		{
			get {return dtPublication;}
			set {dtPublication = value;}
		}		

		public DataTable ICDTable
		{
			get {return dtICD;}
			set {dtICD = value;}
		}

		public int ProductCategoryID
		{
			get {return intProductCategoryID;}
			set {intProductCategoryID = value;}
		}

		public int TermID
		{
			get {return intTermID;}
			set {intTermID = value;}
		}
	

		public int FaqID
		{
			get {return intFaqID;}
			set {intFaqID = value;}
		}
		
		public DataTable FaqsTable
		{
			get {return dtFaqs;}
			set {dtFaqs = value;}
		}

		public bool IsAdmin
		{
			get {return blnAdmin;}
			set {blnAdmin = value;}
		}

		public DataTable ProductCategoryTable
		{
			get {return dtProductCategory;}
			set {dtProductCategory = value;}
		}

		
		/********************* END  Getters and Setters ***************************/

		public void LoadTables()
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();
			sqlConnection1.Open();

			dtContentType = this.SetTable("sp_GetContentTypes");
			this.ProductCategoryTable = this.SetTable("sp_GetProductCategories");
			this.ProductInfoTable = this.SetTable("sp_GetProducts");
			this.TermSectionTable = this.SetTable("sp_GetTermSections");

			sqlConnection1.Close();
		} // END LoadTables


		public void SetIsAdmin(System.Web.HttpRequest req)
		{

			string authUser = req.ServerVariables["AUTH_USER"];
			string[] arrayAuth = authUser.Split('\\');

			if(arrayAuth.Length>1)
			{
				this.ADAccount = arrayAuth[1];

				DBConnection conn = new DBConnection();
				sqlConnection1 = conn.getConnection();
				sqlConnection1.Open();

				SqlCommand cmdSetTable = new SqlCommand();
				cmdSetTable.CommandText = "sp_GetReimbursementUser";
				cmdSetTable.CommandType = CommandType.StoredProcedure;
				cmdSetTable.Connection = sqlConnection1;

				cmdSetTable.Parameters.Add(new SqlParameter("@st", this.ADAccount));

				DataTable myDT = new DataTable();
			
				try
				{   
					SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
					da.Fill(myDT);
				}
				catch(Exception exc)
				{
					Debug.WriteLine("sp_GetReimbursementUser ERROR" +  exc.Message);
				}	
	
				sqlConnection1.Close();

				if(myDT.Rows.Count != 0)
				{
					this.IsAdmin = Convert.ToBoolean(myDT.Rows[0]["userRole"].ToString()=="Admin");
					this.UserEmail = myDT.Rows[0]["emailAddress"].ToString();
				}
				else
					this.IsAdmin = false;

			}  // if(arrayAuth.Length>1)
			else 
				this.IsAdmin =  false;
		} // END


		public DataTable SetTable(string sp)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = sqlConnection1;

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		public DataTable SetTable(string sp, int id)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = sqlConnection1;
			cmdSetTable.Parameters.Add(new SqlParameter("@ID", id));

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
				//	Debug.WriteLine("myDT Rows = " + myDT.Rows.Count.ToString());
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		public DataTable SetTable(string sp, string st)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = sqlConnection1;
			cmdSetTable.Parameters.Add(new SqlParameter("@st", st));

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
				//	Debug.WriteLine("myDT Rows = " + myDT.Rows.Count.ToString());
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		public DataTable SetTable(string sp, string st1, string st2)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = sqlConnection1;
			cmdSetTable.Parameters.Add(new SqlParameter("@st1", st1));
			cmdSetTable.Parameters.Add(new SqlParameter("@st2", st2));

			DataTable myDT = new DataTable();
			
			try
			{   
				SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
				da.Fill(myDT);
			}
			catch(Exception exc)
			{
				Debug.WriteLine(sp + " SetTable ERROR" +  exc.Message);
			}	
	
			return myDT;
		} // END SetTable()

		private SqlConnection GetOpenSqlConn()
		{
            string strConnString = ConfigurationManager.AppSettings["sqlConn"];
			SqlConnection sqlConn = new SqlConnection(strConnString);
			sqlConn.Open();
			return sqlConn;
		}

		public DataTable SqlExecuteQuery(SqlCommand command)
        {
            DataTable myDataTable = new DataTable();

            command.Connection = this.GetOpenSqlConn();
            command.CommandType = CommandType.StoredProcedure;

			SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(myDataTable);
            da.Dispose();
            command.Connection.Close();

            return myDataTable;
        }

		public int ExecuteNonQuery(SqlCommand cmd)
		{
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Connection = this.GetOpenSqlConn();

			int rowsUpdated = cmd.ExecuteNonQuery();

			cmd.Connection.Close();

			return rowsUpdated;
		} // END ExecuteNonQuery

		/// <summary>Executes a query with SqlParameters</summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameters">An ArrayList of SqlParameters.</param>
		/// <returns>A datatable of the results.</returns>
		public DataTable SqlExecuteQueryWithParameters(string storedProcedureName, ArrayList parameters)
		{
			DataTable myDataTable = new DataTable();
			SqlConnection SqlConn = this.SqlConn;

			SqlCommand command = new SqlCommand();
			command.Connection = this.SqlConn;
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = storedProcedureName;

			for(int x=0; x<parameters.Count; x++)
			{
				SqlParameter parameter = parameters[x] as SqlParameter;
				command.Parameters.Add(parameter);
			}

			if(this.SqlConn.State != ConnectionState.Open)
				this.SqlConn.Open();

			SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
			myDataAdapter.Fill(myDataTable);
			myDataAdapter.Dispose();

			this.SqlConn.Close();

			return myDataTable;
		}

	} // END class
}
