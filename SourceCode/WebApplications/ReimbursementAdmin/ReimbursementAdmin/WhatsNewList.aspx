<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<%@ Page language="c#" Codebehind="WhatsNewList.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.WhatsNewList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>What's New List</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td vAlign="top" width="760">
						<table width="760" align="center">
							<tr>
								<td><asp:button id="btnNew" runat="server" BorderColor="#CCCCCC" CausesValidation="False" BackColor="White"
										ForeColor="#FF7300" Font-Size="8pt" Text="Add What's New" Height="18" Width="100" onclick="btnNew_Click"></asp:button></td>
							</tr>
							<tr>
								<td><asp:datagrid id="dgWhatsNew" runat="server" Width="760px" AllowSorting="False" PagerStyle-Visible="False"
										AutoGenerateColumns="False" CellPadding="5" DataKeyField="whatsNewID"
										OnDeleteCommand="btnDelete_Click" OnEditCommand="btnEdit_Click">
										<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="40" Height="18" runat="server" Text="Edit" CommandName="Edit"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="WhatsNewStatus" HeaderText="Status"></asp:BoundColumn>
											<asp:BoundColumn DataField="CategoryName" HeaderText="Cat"></asp:BoundColumn>
											<asp:BoundColumn DataField="whatsNewSort" HeaderText="Sort"></asp:BoundColumn>
											<asp:BoundColumn DataField="whatsNewHeading" HeaderText="Heading"></asp:BoundColumn>
											<asp:BoundColumn DataField="whatsNewSummary" HeaderText="Summary"></asp:BoundColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="6%"></HeaderStyle>
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False"></PagerStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
