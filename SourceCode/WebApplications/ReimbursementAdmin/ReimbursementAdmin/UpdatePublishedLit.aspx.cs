using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdatePublishedLit.
	/// </summary>
	public partial class UpdatePublishedLit : System.Web.UI.Page
	{
	
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Debug.WriteLine("UpdatePublishedLit Page_Load");
			
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{

				sqlConnection1.Open();
				myUtility.PublishedLitTable = myUtility.SetTable("sp_GetPublishedLit");
				sqlConnection1.Close();
				Session["Utility"] = myUtility;
				this.BindDT();

			}
			
		} // END Page_Load


		private void SetControls()
		{
			if(myUtility.PublishedLitTable.Rows.Count == 0)
			{
				dgPubLit.Visible=false;
				btnFirst.Visible=false;
				btnPrev.Visible=false;
				ddlPage.Visible=false;
				lblPageCnt.Visible=false;;
				btnNext.Visible=false;
				btnLast.Visible=false;
				lblRecordCount.Visible=false;
			}
			else
			{
				dgPubLit.Visible=true;
				btnFirst.Visible=true;
				btnPrev.Visible=true;
				ddlPage.Visible=true;
				lblPageCnt.Visible=true;;
				btnNext.Visible=true;
				btnLast.Visible=true;
				lblRecordCount.Visible=true;
			}
		
		} // END SetControls


		public void BindDT()
		{
			try
			{
				dgPubLit.DataSource = myUtility.PublishedLitTable;
				dgPubLit.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Exception: " + ex.Message);
				dgPubLit.CurrentPageIndex = 0;
			}

			if(dgPubLit.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgPubLit.CurrentPageIndex != (dgPubLit.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}


			lblRecordCount.Text = "<b><font color=red>" + Convert.ToString(myUtility.PublishedLitTable.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgPubLit.PageCount.ToString();
			ddlPage.Items.Clear();
			for(int x=1;x<dgPubLit.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			int myPage=dgPubLit.CurrentPageIndex+1;
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

		} // END BindDT

		public DataTable GetProductList()
		{ 
			return myUtility.ProductInfoTable;
		}

		public int GetProductIndex(string strProductID)
		{
			int x=0;
			foreach(DataRow aRow in myUtility.ProductInfoTable.Rows)
			{
				if(strProductID == aRow["productID"].ToString())
					return x;

				x++;
			}

			return x;

		} // END GetProductIndex

		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeletePublishedLit";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@publishedLitID", Int32.Parse(dgPubLit.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.PublishedLitTable = myUtility.SetTable("sp_GetPublishedLit");
			sqlConnection1.Close();

			dgPubLit.EditItemIndex = -1;
			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click

		public void DG_ItemDataBound(object sender, DataGridItemEventArgs e)
		{

			Button btn;
			if(e.Item.ItemType==ListItemType.Item || e.Item.ItemType==ListItemType.AlternatingItem)
			{
				btn = (Button)e.Item.FindControl("btnDelete");
				btn.Attributes.Add("onclick", "return confirm_delete();");
			}
			if(e.Item.ItemType==ListItemType.EditItem)
			{
				btn = (Button)e.Item.FindControl("btnEditDelete");
				btn.Attributes.Add("onclick", "return confirm_delete();");
			}

		} // END DG_ItemDataBound

		protected void btnUpdate_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage.StartsWith("Add - "))
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				TextBox txtNote = (TextBox)e.Item.FindControl("txtNote");
				DropDownList ddlProduct = (DropDownList)e.Item.FindControl("ddlProduct");
				TextBox txtYear = (TextBox)e.Item.FindControl("txtYear");
				
				sqlConnection1.Open();

				SqlCommand cmdUpdate = new System.Data.SqlClient.SqlCommand();
				cmdUpdate.CommandType = CommandType.StoredProcedure;
				cmdUpdate.CommandText = "sp_UpdatePublishedLit";
				cmdUpdate.Connection = sqlConnection1;

				cmdUpdate.Parameters.Add(new SqlParameter("@publishedYear", Int32.Parse(txtYear.Text)));
				cmdUpdate.Parameters.Add(new SqlParameter("@note", txtNote.Text));
				cmdUpdate.Parameters.Add(new SqlParameter("@productID", Int32.Parse(ddlProduct.SelectedValue)));
				cmdUpdate.Parameters.Add(new SqlParameter("@publishedLitID", Int32.Parse(dgPubLit.DataKeys[e.Item.ItemIndex].ToString())));

				int rowsUpdated = cmdUpdate.ExecuteNonQuery();

				myUtility.PublishedLitTable = myUtility.SetTable("sp_GetPublishedLit");
				sqlConnection1.Close();

				dgPubLit.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid)  
		} // END btnUpdate_Click

		protected void btnAdd_Click(object sender, DataGridCommandEventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(!val.ErrorMessage.StartsWith("Add - "))
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				TextBox txtAddNote = (TextBox)e.Item.FindControl("txtAddNote");
				DropDownList ddlAddProduct = (DropDownList)e.Item.FindControl("ddlAddProduct");
				TextBox txtAddYear = (TextBox)e.Item.FindControl("txtAddYear");
				
				sqlConnection1.Open();

				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertPublishedLit";
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@publishedYear", Int32.Parse(txtAddYear.Text)));
				cmdInsert.Parameters.Add(new SqlParameter("@note", txtAddNote.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@productID", Int32.Parse(ddlAddProduct.SelectedValue)));

				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				myUtility.PublishedLitTable = myUtility.SetTable("sp_GetPublishedLit");
				sqlConnection1.Close();

				dgPubLit.EditItemIndex = -1;
				this.BindDT();
				Session["Utility"] = myUtility;

			} // END (Page.IsValid) 
		} // END btnAdd_Click

		protected void btnEdit_Click(object sender, DataGridCommandEventArgs e)
		{
			dgPubLit.EditItemIndex = Int32.Parse(e.Item.ItemIndex.ToString());
			this.BindDT();
		} // END btnEdit_Click

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgPubLit.CurrentPageIndex = 0;
			dgPubLit.EditItemIndex = -1;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgPubLit.CurrentPageIndex >= 1)
				dgPubLit.CurrentPageIndex = dgPubLit.CurrentPageIndex - 1;
			else
				dgPubLit.CurrentPageIndex = 0;

			dgPubLit.EditItemIndex = -1;
			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgPubLit.CurrentPageIndex != (dgPubLit.PageCount-1))
				dgPubLit.CurrentPageIndex = dgPubLit.CurrentPageIndex + 1;
			else
				dgPubLit.CurrentPageIndex = (dgPubLit.PageCount-1);

			dgPubLit.EditItemIndex = -1;
			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgPubLit.CurrentPageIndex = (dgPubLit.PageCount-1);
			dgPubLit.EditItemIndex = -1;
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgPubLit.CurrentPageIndex = ddlPage.SelectedIndex;
			dgPubLit.EditItemIndex = -1;
			this.BindDT();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
