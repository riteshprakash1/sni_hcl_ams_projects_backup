<%@ Page language="c#" Codebehind="UpdateLinks.aspx.cs" AutoEventWireup="True" Inherits="ReimbursementAdmin.UpdateLinks" %>
<%@ Register TagPrefix="uc1" TagName="AdminHeader" Src="AdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeftNav" Src="LeftNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Links</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="global.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<uc1:adminheader id="AdminHeader1" runat="server"></uc1:adminheader>
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" vAlign="top" width="140"><uc1:leftnav id="LeftNav1" runat="server"></uc1:leftnav></td>
					<td width="760">
						<table width="100%" align="center">
							<tr>
								<td><asp:datagrid id="dgLinks" runat="server" Width="760" OnUpdateCommand="btnUpdate_Click" OnEditCommand="btnEdit_Click"
										OnCancelCommand="btnAdd_Click" PagerStyle-Visible="False" AutoGenerateColumns="False" ShowHeader="True"
										CellPadding="5" CellSpacing="0" OnDeleteCommand="btnDelete_Click" DataKeyField="linkID" ShowFooter="true">
										<FooterStyle VerticalAlign="Top"></FooterStyle>
										<ItemStyle Wrap="False" VerticalAlign="Top" Font-Size="8pt"></ItemStyle>
										<HeaderStyle Font-Size="10px" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<ItemStyle Wrap="False" Width="6%" VerticalAlign="Top" BackColor="white" ForeColor="#ff7300"
													BorderColor="#cccccc"></ItemStyle>
												<ItemTemplate>
													<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
														BackColor="white" Font-Size="8pt" ForeColor="#ff7300" BorderColor="#cccccc" CausesValidation="False"></asp:Button>
												</ItemTemplate>
												<FooterTemplate>
													<asp:Button id="btnAdd" Width="50" Height="18" runat="server" Text="Add" CommandName="Cancel"
														BackColor="white" Font-Size="8pt" ForeColor="#ff7300" BorderColor="#cccccc" CausesValidation="False"></asp:Button>
												</FooterTemplate>
												<EditItemTemplate>
													<asp:Button id="btnUpdate" Width="50" Height="18" runat="server" Text="Update" CommandName="Update"
														BackColor="white" Font-Size="8pt" ForeColor="#ff7300" BorderColor="#cccccc" CausesValidation="False"></asp:Button>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemStyle Wrap="False" Width="6%"></ItemStyle>
												<ItemTemplate>
													<asp:Button id="btnDelete" Width="40" Height="18" runat="server" Text="Delete" CommandName="Delete"
														BackColor="white" Font-Size="8pt" ForeColor="#ff7300" BorderColor="#cccccc" CausesValidation="False"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="URL">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblUrl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "linkUrl") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox ID=txtUrl Font-Size=8pt Runat=server MaxLength="200" Columns="50" Text='<%# DataBinder.Eval(Container.DataItem, "linkUrl") %>'>
													</asp:TextBox>
													<asp:requiredfieldvalidator id="vldRequiredUrl" runat="server" ControlToValidate="txtUrl" ErrorMessage="Update - You must enter the URL.">*</asp:requiredfieldvalidator>
													<asp:RegularExpressionValidator id="vldUrl" ControlToValidate="txtUrl" runat="server" ErrorMessage="Update - The URL format is incorrect."
							ValidationExpression="http://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?">*</asp:RegularExpressionValidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddUrl" runat="server" Font-Size="8pt" MaxLength="200" Columns="50"></asp:textbox>
													<asp:requiredfieldvalidator id="vldAddRequiredUrl" runat="server" ControlToValidate="txtAddUrl" ErrorMessage="Add - You must enter the URL.">*</asp:requiredfieldvalidator>
													<asp:RegularExpressionValidator id="vldAddUrlReg" ControlToValidate="txtAddUrl" runat="server" ErrorMessage="Add - The URL format is incorrect."
							ValidationExpression="http://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?">*</asp:RegularExpressionValidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="URL Description">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblLinkName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "linkName") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox ID="txtLinkName" Font-Size=8pt Runat=server MaxLength="200" Columns="50" Text='<%# DataBinder.Eval(Container.DataItem, "linkName") %>'>
													</asp:TextBox>
													<asp:requiredfieldvalidator id="vldLinkName" runat="server" ControlToValidate="txtLinkName" ErrorMessage="Update - You must enter the URL Description.">*</asp:requiredfieldvalidator>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:textbox id="txtAddLinkName" runat="server" Font-Size="8pt" MaxLength="200" Columns="50"></asp:textbox>
													<asp:requiredfieldvalidator id="vldAddLinkName" runat="server" ControlToValidate="txtAddLinkName" ErrorMessage="Add - You must enter the URL Description.">*</asp:requiredfieldvalidator>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Product Area">
												<ItemTemplate>
													<asp:Label Font-Size="8pt" id="lblProductCategory" runat="server" Text='<%# GetCategoryName(DataBinder.Eval(Container.DataItem, "categoryID").ToString())%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:DropDownList id=ddlCategory Font-Size="8pt" datavaluefield="CatValue" datatextfield="CatName" DataSource="<%#GetCategoryTable()%>" SelectedIndex = '<%#GetCategoryIndex(DataBinder.Eval(Container.DataItem, "CategoryID").ToString())%>' runat="server"></asp:DropDownList>
												</EditItemTemplate>
												<FooterTemplate>
													<asp:DropDownList id="ddlAddCategory" Font-Size="8pt" runat="server" datavaluefield="CatValue" datatextfield="CatName" DataSource="<%#GetCategoryTable()%>"></asp:DropDownList>
												</FooterTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td><asp:validationsummary id="vldSummary" runat="server"></asp:validationsummary></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
