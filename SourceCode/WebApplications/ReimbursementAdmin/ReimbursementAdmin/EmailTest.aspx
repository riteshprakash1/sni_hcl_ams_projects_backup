﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailTest.aspx.cs" Inherits="ReimbursementAdmin.EmailTest" %>
<%@ Register src="AdminHeader.ascx" tagname="AdminHeader" tagprefix="uc1" %>
<%@ Register src="LeftNav.ascx" tagname="LeftNav" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Test</title>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:AdminHeader ID="AdminHeader1" runat="server" />
    
			<table width="900" align="center">
				<tr>
					<td class="LeftNav" valign="top" width="140"><uc2:LeftNav ID="LeftNav1" runat="server" /></td>
					<td  style="width:760px; text-align:center; vertical-align:top">
						<table style="width:755px; text-align:left; vertical-align:top">
							<tr>
 								<td style="width:12%"><b>Email To:&nbsp;&nbsp;</b></td>
                                <td><asp:TextBox ID="txtEmailTo" Width="500px"  runat="server"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="vldRequiredTo" ControlToValidate="txtEmailTo" runat="server" ErrorMessage="Email To is required"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="vldEmailTo" ControlToValidate="txtEmailTo" 
                                        runat="server" ErrorMessage="Email address is not valid" 
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                               </td>
							</tr>
							<tr>
 								<td><b>Subject:&nbsp;&nbsp;</b></td>
                                <td><asp:TextBox ID="txtSubject" Width="500px" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredSubject" ControlToValidate="txtSubject" runat="server" ErrorMessage="Subject is required"></asp:RequiredFieldValidator>
                                </td>
							</tr>
							<tr>
 								<td><b>Body:&nbsp;&nbsp;</b></td>
                                <td><asp:TextBox ID="txtBody" Width="500px"  TextMode="MultiLine" Rows="5" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredBody" ControlToValidate="txtBody" runat="server" ErrorMessage="Body is required"></asp:RequiredFieldValidator>
                                </td>
							</tr>
                            <tr>
                                <td colspan="2"><asp:button id="btnSend" runat="server" BorderColor="#CCCCCC" 
                                        CausesValidation="False" BackColor="White"
										ForeColor="#FF7300" Font-Size="8pt" Text="Send" Height="18" Width="100" onclick="btnSend_Click"></asp:button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><asp:Label ID="lblMessage" runat="server" ForeColor="#FF7300" Font-Size="8pt"></asp:Label></td>
                            </tr>

						</table>
                    </td>
                </tr>
            </table>           
    </form>
</body>
</html>
