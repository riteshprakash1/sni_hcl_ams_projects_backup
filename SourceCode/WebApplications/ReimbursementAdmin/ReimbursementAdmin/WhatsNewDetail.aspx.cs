using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
//using System.Web.Mail;
using System.Diagnostics;
using System.Configuration;
using System.Net.Mail;


namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for WhatsNewDetail.
	/// </summary>
	public partial class WhatsNewDetail : System.Web.UI.Page
	{
		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected DataTable dtWhatsNew;
		protected AppSettingsReader config;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
				Response.Redirect("UpdateProductContent.aspx");

			if(!Page.IsPostBack)
			{
				this.btnSendEmail.Attributes.Add("onclick", "return confirm_send();");

				ddlCategory.Items.Clear();
				ddlCategory.Items.Add(new ListItem("All","99"));
				ddlCategory.Items.Add(new ListItem("Front Page","0"));
				foreach(DataRow aRow in myUtility.ProductCategoryTable.Rows)
				{
					ddlCategory.Items.Add(new ListItem(aRow["productCategoryName"].ToString(),aRow["productCategoryID"].ToString() ));
				}
					
				if(myUtility.WhatsNewID != 0)
				{
				
					sqlConnection1.Open();
					dtWhatsNew = myUtility.SetTable("sp_GetWhatsNew", myUtility.WhatsNewID);
					myUtility.WhatsNewFilesTable = myUtility.SetTable("sp_GetWhatsNewFiles", myUtility.WhatsNewID);
					sqlConnection1.Close();

					Session["Utility"] = myUtility;

					Debug.WriteLine("Rows = " + dtWhatsNew.Rows.Count.ToString());
					if(dtWhatsNew.Rows.Count > 0)
						this.LoadData(dtWhatsNew.Rows[0]);

					this.BindDT();
				}
			} // END !Page.IsPostBack
			this.SetControls();
		} // END Page_Load


		public void BindDT()
		{
			dgFiles.DataSource = myUtility.WhatsNewFilesTable;
			dgFiles.DataBind();
		}

		protected void SetControls()
		{
			bool isActiveID = myUtility.WhatsNewID != 0;
			this.btnSendEmail.Enabled = isActiveID;
			this.dgFiles.Visible = isActiveID;
			this.lblAttach.Visible = isActiveID;
			this.btnAddFile.Visible = isActiveID;
			this.txtFile.Visible = isActiveID;
			
			if(myUtility.WhatsNewID == 0)
			{
				this.btnAdd.Text = "Add";
			}
			else
			{
				this.btnAdd.Text = "Update";
			}
		}


		protected void LoadData(DataRow aRow)
		{
			this.txtDetail.Text = aRow["whatsNewDetail"].ToString();
			this.txtSummary.Text = aRow["whatsNewSummary"].ToString();
			this.txtHeading.Text = aRow["whatsNewHeading"].ToString();
			this.txtsort.Text = aRow["whatsNewSort"].ToString();

			if(this.ddlCategory.Items.FindByValue(aRow["categoryID"].ToString()) != null)
				this.ddlCategory.Items.FindByValue(aRow["categoryID"].ToString()).Selected=true;

			if(this.ddbstatus.Items.FindByText(aRow["whatsNewStatus"].ToString()) != null)
				this.ddbstatus.Items.FindByText(aRow["whatsNewStatus"].ToString()).Selected=true;

		} // END LoadData

		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		
		private void sendMail(string strBody, string strTo, string strSrv, string strFrom)
		{
			try
			{
                MailAddress from = new MailAddress(strFrom);
                MailAddress to = new MailAddress(strTo);
                MailMessage message = new MailMessage(from, to);

                message.Subject = "Smith & Nephew Reimbursement Web Site -  What's New";
                message.IsBodyHtml = true;

                message.Body = strBody;

                SmtpClient client = new SmtpClient();
                client.Send(message);
			}
			catch(Exception exc)
			{
				Debug.WriteLine("sendMail Error: " + exc.Message);
				throw;
			}
		}
		
		private void SendToEmailList()
		{
			Debug.WriteLine("In SendToEmailList");

			config = new AppSettingsReader();
			string strSite = ((string)(config.GetValue("reimbursementSite", typeof(string))));
			string strFrom = ((string)(config.GetValue("SiteEmailFromAddress", typeof(string))));
			string strFromName = ((string)(config.GetValue("SiteEmailFromName", typeof(string))));
			string strSrv = ((string)(config.GetValue("SmtpServer", typeof(string))));
			string strFromEmail = strFromName + "<" + strFrom + ">";
			string strUnsubscribe = strSite+ "/unsubscribe.aspx";

			string strBody = "<table width=800>";
			strBody += "<tr><td>We recently added the following \"Whats's New\" item to our Reimbursement Web Site:";
			strBody += "<br><br><b>Heading:</b> " + this.txtHeading.Text;
			strBody += "<br><b>Summary:</b> " + this.txtSummary.Text;
			strBody += "<br>Detail: " + this.txtDetail.Text;
			strBody += "<br><br>For more information, please go to the ";
			strBody += "Smith & Nephew Reimbursement Web Site, ";
			strBody += "<a href='" + strSite + "'>" + strSite + "</a> .";
			strBody += "<br><br>If you prefer not to receive further News, please click here to ";
			strBody += "<a href=\"" + strUnsubscribe + "\">Unsubscribe</a>."; 
			strBody += "</td></tr>";
			strBody += "</table>";					

			sqlConnection1.Open();
			DataTable dt = myUtility.SetTable("sp_GetPublicationEmailAddresses");
			sqlConnection1.Close();

			//// if it NOT isProduction send email to test user
			if(!Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"].ToString()))
			{
				strBody += "<table width=800><tr><td><b>Recipient List</b>&nbsp;(not displayed  in production email)</td></tr>";
				foreach(DataRow aRow in dt.Rows)
				{
					strBody += "<tr><td>"+aRow["EmailAddress"].ToString()+"</td></tr>";
				}
				strBody += "</table>";					
				Debug.WriteLine("In test user email: " + myUtility.UserEmail);
				this.sendMail(strBody,myUtility.UserEmail, strSrv, strFromEmail);
			}
			else
			{
				foreach(DataRow aRow in dt.Rows)
				{
                    Debug.WriteLine("In Production Email: " + aRow["EmailAddress"].ToString() + "; isProduction: " + Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"].ToString()).ToString());
					this.sendMail(strBody, aRow["EmailAddress"].ToString(), strSrv, strFromEmail);
				}
			}

		} // END sendEmails
		
		protected void btnAdd_Click(object sender, System.EventArgs e)
		{

			Button btn = sender as Button;

			Page.Validate();

			System.Web.UI.ValidatorCollection vals = Page.Validators;

			foreach(IValidator val in vals)
			{
				if(val.ErrorMessage.StartsWith("File"))
				{
					val.IsValid = true;
				}

			} // END foreach

			if(Page.IsValid)
			{
				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.Connection = sqlConnection1;

				SqlParameter paramID = new SqlParameter("@whatsNewID", SqlDbType.Int);
				cmdInsert.Parameters.Add(paramID);
                cmdInsert.Parameters.Add(new SqlParameter("@whatsNewStatus", ddbstatus.SelectedItem.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@whatsNewSort", txtsort.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@whatsNewHeading", txtHeading.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@whatsNewSummary", txtSummary.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@whatsNewDetail", txtDetail.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@categoryID", Int32.Parse(ddlCategory.SelectedValue)));
				
				if(btn.Text == "Add")
				{
					cmdInsert.CommandText = "sp_InsertWhatsNew";
					paramID.Direction = ParameterDirection.Output;
				}
				else
				{
					cmdInsert.CommandText = "sp_UpdateWhatsNew";
					paramID.Direction = ParameterDirection.Input;
					paramID.Value = myUtility.WhatsNewID;
				}

				sqlConnection1.Open();
					int rowsUpdated = cmdInsert.ExecuteNonQuery();
					sqlConnection1.Close();

				if(btn.Text == "Add")
				{
					myUtility.WhatsNewID = Int32.Parse(paramID.Value.ToString());
//					Debug.WriteLine("Before SendToEmailList");
//					this.SendToEmailList();
				}
					
				Response.Redirect("WhatsNewDetail.aspx");

				Session["Utility"] = myUtility;		
			} // END Page.IsValid


		} // END btnAdd_Click


		protected byte[] GetByteFile(HttpPostedFile myFile)
		{
			// Get size of uploaded file
			int nFileLen = myFile.ContentLength; 
			byte[] myData = null;

			// make sure the size of the file is > 0
			if( nFileLen > 0 )
			{
				// Allocate a buffer for reading of the file
				myData = new byte[nFileLen];

				// Read uploaded file from the Stream
				myFile.InputStream.Read(myData, 0, nFileLen);

			} //END 

			return myData;
		} // END GetByteFile


		public string GetImage(string fileName)
		{
			int intP = fileName.LastIndexOf('.');
			string ext = fileName.Substring(intP+1, 3).ToLower();
			Debug.WriteLine("Ext = " + ext);
			switch(ext)       
			{         
				case "doc":   
					return "images\\word16.gif";
				case "xls":            
					return "images\\excel16.gif";
				case "txt":            
					return "images\\text16.gif";
				case "pdf":            
					return "images\\acrobat16.gif";
				case "ppt":            
					return "images\\mspowerpoint16.gif";
				default:            
					return "images\\text16.gif";
			}

		}

		protected void btnDelete_Click(object sender, DataGridCommandEventArgs e)
		{
			sqlConnection1.Open();

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.CommandText = "sp_DeleteWhatsNewFile";
			cmdDelete.Connection = sqlConnection1;

			cmdDelete.Parameters.Add(new SqlParameter("@whatsNewFileID", Int32.Parse(this.dgFiles.DataKeys[e.Item.ItemIndex].ToString())));

			int rowsUpdated = cmdDelete.ExecuteNonQuery();

			myUtility.WhatsNewFilesTable = myUtility.SetTable("sp_GetWhatsNewFiles", myUtility.WhatsNewID);
			sqlConnection1.Close();

			this.BindDT();
			Session["Utility"] = myUtility;

		} // END btnDelete_Click



		protected void btnAddFile_Click(object sender, System.EventArgs e)
		{
			Page.Validate();

			System.Web.UI.ValidatorCollection vals = Page.Validators;

			foreach(IValidator val in vals)
			{
				if(!val.ErrorMessage.StartsWith("File"))
				{
					val.IsValid = true;
				}

				if(val.ErrorMessage == "File does not exist.")
				{
					if(txtFile == null)
					{
						val.IsValid = false;
					}
					else
					{
						val.IsValid = (txtFile.PostedFile.ContentLength>0);
					}
				}


			} // END foreach

			if(Page.IsValid)
			{
				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.CommandText = "sp_InsertWhatsNewFile";
				cmdInsert.Connection = sqlConnection1;

				byte[] bData = this.GetByteFile(txtFile.PostedFile);
				string strName = txtFile.PostedFile.FileName;
				strName = strName.Substring(strName.LastIndexOf("\\")+1);
				cmdInsert.Parameters.Add(new SqlParameter("@fileData", bData));
				cmdInsert.Parameters.Add(new SqlParameter("@nameFile", strName));
				cmdInsert.Parameters.Add(new SqlParameter("@fileType", txtFile.PostedFile.ContentType));
				cmdInsert.Parameters.Add(new SqlParameter("@fileSize", txtFile.PostedFile.ContentLength));
				cmdInsert.Parameters.Add(new SqlParameter("@whatsNewID", myUtility.WhatsNewID));

				sqlConnection1.Open();
				int rowsUpdated = cmdInsert.ExecuteNonQuery();

				myUtility.WhatsNewFilesTable = myUtility.SetTable("sp_GetWhatsNewFiles", myUtility.WhatsNewID);
				sqlConnection1.Close();

				this.BindDT();
				Session["Utility"] = myUtility;

			} // END Page.IsValid

		}

		protected void btnSendEmail_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("Before SendToEmailList");
			this.SendToEmailList();
		}

	} // END class
}
