using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;


namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for UpdateProductContent.
	/// </summary>
	public partial class UpdateProductContent : System.Web.UI.Page
	{
		protected string strUpdateSP;
		protected string strColumnName;
		protected Utility myUtility;
		protected SqlConnection sqlConnection1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			if(Session["Utility"]!=null)
			{
				myUtility = (Utility)Session["Utility"];
			}
			else
			{
				myUtility = new Utility();
				Session["Utility"] = myUtility;
			}

			if(!Page.IsPostBack)
			{
				ddlCategories.Items.Clear();
				ddlCategories.Items.Add(new ListItem("Select","0"));
				foreach(DataRow aRow in myUtility.ProductCategoryTable.Rows)
				{
					ddlCategories.Items.Add(new ListItem(aRow["productCategoryName"].ToString(),aRow["productCategoryID"].ToString() ));
				}
			} // END isPostBack	

			this.SetControls();

		} // Page_Load

		private void SetControls()
		{ 
			
			Debug.WriteLine("ddlProducts.SelectedValue = " +ddlProducts.SelectedValue);
			switch (ddlProducts.SelectedValue)
			{
			case "99999": // Add Product Selected
				LabelProdName.Visible = true;
				LabelProdName.Text = "Product Name:";
				ddlStatus.Visible = false;
				txtProdName.Visible = true;
				btnAddProduct.Visible = true;
				btnAddProduct.Text = "Add Product";
				btnUpdate.Visible = false;
				LabelContent.Visible = false;
				ddlContent.Visible = false;
				txtContent.Visible = false;
				ltlContent.Visible = false;
				break;
				case "": // Product List is empty
					goto case "0";
				case "0": // 'Select' is selected
				Debug.WriteLine("0 value");
				LabelProdName.Visible = false;
				txtProdName.Visible = false;
				ddlStatus.Visible = false;
				btnAddProduct.Visible = false;
				btnUpdate.Visible = true;
				LabelContent.Visible = true;
				ddlContent.Visible = true;
				txtContent.Visible = true;
				ltlContent.Visible = true;
				break;
				default: // Product Selected
				LabelProdName.Visible = true;
				LabelProdName.Text = "Product Status:";
				ddlStatus.Visible = true;
				txtProdName.Visible = false;
				btnAddProduct.Visible = true;
				btnAddProduct.Text = "Update Status";
				btnUpdate.Visible = true;
				LabelContent.Visible = true;
				ddlContent.Visible = true;
				txtContent.Visible = true;
				ltlContent.Visible = true;
				break;
			}

		} // END SetControls

		private void LoadContentType()
		{
			DataRow[] rows = null;
			if(ddlProducts.SelectedIndex == 0 || ddlProducts.Items.Count == 0)
				rows = myUtility.ContentTypeTable.Select("contentLevel = 'Category'");
			else
				rows = myUtility.ContentTypeTable.Select("contentLevel = 'Product'");

			ddlContent.Items.Clear();
			foreach(DataRow aRow in rows)
			{
				ddlContent.Items.Add(new ListItem(aRow["contentName"].ToString(),aRow["contentTypeID"].ToString() ));
			}		
		} // END LoadContentType


		protected void ddlProducts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage != "You must select a product.")
				{
					val.IsValid = true;
				}
			} // END foreach

			
			if(Page.IsValid)
			{
				if(ddlProducts.SelectedValue != "99999") // Add Product
				{
					this.LoadContentType();
					ddlContent.SelectedIndex = 0;

					sqlConnection1.Open();
					this.GetContent("Product");
					sqlConnection1.Close();
				}
				else
				{
					txtProdName.Text = "";
				}
			} // END Page.IsValid
		} // END ddlProducts_SelectedIndexChanged

		private void GetContent(string strType)
		{
			DataRow[] rows = myUtility.ContentTypeTable.Select("contentTypeID = " + ddlContent.SelectedValue);
			if(rows != null)
			{
				strColumnName = rows[0]["contentField"].ToString();
			
				if(strType == "Product")
					myUtility.ProductTable = myUtility.SetTable("sp_GetProduct", Int32.Parse(ddlProducts.SelectedValue));
				else
					myUtility.ProductTable = myUtility.SetTable("sp_GetProductCategory", Int32.Parse(ddlCategories.SelectedValue));
			
				DataRow aRow = myUtility.ProductTable.Rows[0];
				txtContent.Text = aRow[strColumnName].ToString();
				ltlContent.Text = aRow[strColumnName].ToString();
				Debug.WriteLine("ddlStatus.SelectedValue = " + ddlStatus.SelectedValue);
				if(strType == "Product")
				{
					foreach(ListItem i in ddlStatus.Items)
					{
						i.Selected = Convert.ToBoolean(i.Value == aRow["productStatus"].ToString());
					}
				}
			}

		} // END GetContent

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(ddlProducts.SelectedIndex==0 && ddlContent.SelectedIndex==0)
				{
					if(val.ErrorMessage == "You must select a product.")
					{
						val.IsValid = true;
					}
				}

				if(val.ErrorMessage == "You must enter a product name.")
				{
					val.IsValid = true;
				}
			} // END foreach
			
			if(Page.IsValid)
			{
				DataRow[] rows = myUtility.ContentTypeTable.Select("contentTypeID = " + ddlContent.SelectedValue);
				if(rows != null)
				{
					string strLevel = rows[0]["contentLevel"].ToString();
					string strSP = rows[0]["contentSP"].ToString();

					sqlConnection1.Open();
					SqlCommand cmdUpdate = new System.Data.SqlClient.SqlCommand();
					cmdUpdate.CommandType = CommandType.StoredProcedure;
					cmdUpdate.CommandText = strSP;
					cmdUpdate.Connection = sqlConnection1;

					if(strLevel == "Product")
						cmdUpdate.Parameters.Add(new SqlParameter("@ID", Int32.Parse(ddlProducts.SelectedValue)));
					else
						cmdUpdate.Parameters.Add(new SqlParameter("@ID", Int32.Parse(ddlCategories.SelectedValue)));
				
					cmdUpdate.Parameters.Add(new SqlParameter("@content", txtContent.Text));

					int rowsUpdated = cmdUpdate.ExecuteNonQuery();
					Debug.WriteLine("rowsUpdated = " + rowsUpdated.ToString());
			
					this.GetContent(strLevel);

					sqlConnection1.Close();
				}
			}

		} // END btnUpdate_Click


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlContent_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				if(val.ErrorMessage == "You must enter a product name.")
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				DataRow[] rows = myUtility.ContentTypeTable.Select("contentTypeID = " + ddlContent.SelectedValue);
				if(rows != null)
				{
					string strLevel = rows[0]["contentLevel"].ToString();
					sqlConnection1.Open();
					this.GetContent(strLevel);
					sqlConnection1.Close();
				}
			}
		} // END ddlContent_SelectedIndexChanged

		protected void ddlCategories_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Page.Validate();

			foreach(IValidator val in Page.Validators)
			{
				Debug.WriteLine(val.ErrorMessage + " = " + val.IsValid.ToString());
				if(val.ErrorMessage != "You must select a category.")
				{
					val.IsValid = true;
				}
			} // END foreach

			if(Page.IsValid)
			{
				this.GetProductList();
				this.LoadContentType();
				ddlContent.SelectedIndex = 0;
				this.GetContent("Category");
			}
			ddlProducts.SelectedIndex = 0;
			txtProdName.Text = "";

			this.SetControls();
		}

		/// <summary>
		/// Populates the product drop-down list based on the selected Category
		/// </summary>
		private void GetProductList()
		{
			sqlConnection1.Open();

			DataRow[] rows = myUtility.ProductInfoTable.Select("productCategoryID="+ddlCategories.SelectedValue);
				
			ddlProducts.Items.Clear();
			ddlProducts.Items.Add(new ListItem("Select","0"));
			ddlProducts.Items.Add(new ListItem("Add Product","99999"));
			foreach(DataRow aRow in rows)
			{
				ddlProducts.Items.Add(new ListItem(aRow["productName"].ToString(),aRow["productID"].ToString() ));
			}

			sqlConnection1.Close();
		} // END GetProductList

		protected void btnAddProduct_Click(object sender, System.EventArgs e)
		{
			Button btn = (Button)sender;
			Debug.WriteLine("Button Text = " + btn.Text);
			if(btn.Text == "Update Status")
			{
				sqlConnection1.Open();

				SqlCommand cmdUpdate = new System.Data.SqlClient.SqlCommand();
				cmdUpdate.CommandType = CommandType.StoredProcedure;
				cmdUpdate.CommandText = "sp_UpdateProductStatus";
				cmdUpdate.Connection = sqlConnection1;

				cmdUpdate.Parameters.Add(new SqlParameter("@productStatus", ddlStatus.SelectedValue));
				cmdUpdate.Parameters.Add(new SqlParameter("@ID", Int32.Parse(ddlProducts.SelectedValue)));

				int rowsUpdated = cmdUpdate.ExecuteNonQuery();
				
				myUtility.ProductInfoTable = myUtility.SetTable("sp_GetProducts");
				sqlConnection1.Close();
			}
			else
			{
				Page.Validate();

				foreach(IValidator val in Page.Validators)
				{
					Debug.WriteLine(val.ErrorMessage + " = " + val.IsValid.ToString());
					if(val.ErrorMessage != "You must enter a product name.")
					{
						val.IsValid = true;
					}
				} // END foreach

				if(Page.IsValid)
				{
					sqlConnection1.Open();

					SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
					cmdInsert.CommandType = CommandType.StoredProcedure;
					cmdInsert.CommandText = "sp_InsertProduct";
					cmdInsert.Connection = sqlConnection1;

					cmdInsert.Parameters.Add(new SqlParameter("@productCategoryID", Int32.Parse(ddlCategories.SelectedValue)));
					cmdInsert.Parameters.Add(new SqlParameter("@productName", txtProdName.Text));
					
					SqlParameter paramID = new SqlParameter("@ID", SqlDbType.Int);
					paramID.Direction = ParameterDirection.Output;
					cmdInsert.Parameters.Add(paramID);
					
					int rowsUpdated = cmdInsert.ExecuteNonQuery();
				
					myUtility.ProductInfoTable = myUtility.SetTable("sp_GetProducts");
					
					sqlConnection1.Close();

					this.GetProductList();
					if(ddlProducts.Items.FindByValue(paramID.Value.ToString())!=null)
						ddlProducts.Items.FindByValue(paramID.Value.ToString()).Selected=true;
					
					this.LoadContentType();
					this.GetContent("Product");
				
					this.SetControls();

					Session["Utility"]=myUtility;
				} // END IsValid
			}
		} // END btnAddProduct_Click


	} // END class
}
