using System;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for DBConnection.
	/// </summary>
	public class DBConnection
	{
		protected System.Data.SqlClient.SqlConnection sqlConnection1;
		protected string strConnString;
		
		public DBConnection()
		{
			System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();
			strConnString = ((string)(configurationAppSettings.GetValue("sqlConn", typeof(string))));
		}

		public string getConnString()
		{
			return strConnString;
		}

		public void setConnString(string connString)
		{
			strConnString = connString;
		}

		public SqlConnection getConnection()
		{
			sqlConnection1 = new System.Data.SqlClient.SqlConnection();
			sqlConnection1.ConnectionString = strConnString;
			return sqlConnection1;
		}

	}	// END Class
}		// END Namespace
