using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace ReimbursementAdmin
{
	/// <summary>
	/// Summary description for SurveyDetail.
	/// </summary>
	public partial class SurveyDetail : System.Web.UI.Page
	{
		protected System.Data.DataTable tblSurveys;
		protected System.Data.DataTable tblSurveyResponses;

		protected SqlConnection sqlConnection1;
		protected Utility myUtility;
		protected DataTable dtSurveyResponses;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			DBConnection conn = new DBConnection();
			sqlConnection1 = conn.getConnection();

			this.lblStatusMessage.Text = string.Empty;

			if(Session["Utility"]!=null)
				myUtility = (Utility)Session["Utility"];
			else
				Response.Redirect("UpdateProductContent.aspx");

			this.lblDataGridMessage.Text = string.Empty;

			if(!Page.IsPostBack)
			{
				sqlConnection1.Open();

				if(Request.QueryString["SurveyID"] != null)
					this.lblSurveyID.Text = this.SetSurveyID(Request.QueryString["SurveyID"]);
				else
					this.lblSurveyID.Text = "0";

				myUtility.ProductTable = myUtility.SetTable("sp_GetProducts");
				DataRow[] productRows = myUtility.ProductTable.Select(null, "productName");

				ddlProduct.Items.Clear();
				ddlProduct.Items.Add(new ListItem("-- Select --","0"));
				
				foreach(DataRow aRow in productRows)
				{
					ddlProduct.Items.Add(new ListItem(aRow["productName"].ToString(),aRow["productID"].ToString() ));
				}
					
				if(this.lblSurveyID.Text != "0")
				{
					tblSurveys = myUtility.SetTable("sp_GetSurveyBySurveyID", Convert.ToInt32(this.lblSurveyID.Text));

					Session["Utility"] = myUtility;

					Debug.WriteLine("Rows = " + tblSurveys.Rows.Count.ToString());
					if(tblSurveys.Rows.Count > 0)
						this.LoadData(tblSurveys.Rows[0]);

					this.tblSurveyResponses = myUtility.SetTable("sp_GetSurveyResponsesBySurveyID", Convert.ToInt32(this.lblSurveyID.Text));
					this.BindDT(this.tblSurveyResponses);

					sqlConnection1.Close();
				}
			} // END !Page.IsPostBack

			this.LabelSurveyID.Visible = Convert.ToBoolean(this.lblSurveyID.Text != "0");
			this.lblSurveyID.Visible = Convert.ToBoolean(this.lblSurveyID.Text != "0");
		} // END Page_Load


		public void BindDT(DataTable dt)
		{
			this.dgSurveyResponses.DataSource = dt;
			this.dgSurveyResponses.DataBind();

			bool isVisible = Convert.ToBoolean(dt.Rows.Count != 0);
			this.dgSurveyResponses.Visible = isVisible;

			if(!isVisible)
				this.lblDataGridMessage.Text = "There are no Survey Responses in the database table.";
		}

		protected void LoadData(DataRow aRow)
		{
			this.txtEndDate.Text = aRow["EndDate"].ToString();
			this.txtStartDate.Text = aRow["StartDate"].ToString();
			this.txtSurveyDescription.Text = aRow["SurveyDescription"].ToString();
			this.txtSurveyMessage.Text = aRow["SurveyMessage"].ToString();
			this.txtSurveyName.Text = aRow["SurveyName"].ToString();
			this.txtSurveyURL.Text = aRow["SurveyURL"].ToString();

			if(this.ddlProduct.Items.FindByValue(aRow["productID"].ToString()) != null)
				this.ddlProduct.Items.FindByValue(aRow["productID"].ToString()).Selected=true;

			if(this.ddlStatus.Items.FindByText(aRow["SurveyStatus"].ToString()) != null)
				this.ddlStatus.Items.FindByText(aRow["SurveyStatus"].ToString()).Selected=true;

		} // END LoadData


		private string SetSurveyID(string strID)
		{
			int intSurveyID = 0;
			
			try
			{
				intSurveyID = Convert.ToInt32(strID);
			}
			catch(Exception ex)
			{
				Debug.WriteLine("GetSurveyID not int error: " + ex.Message);
			}

			return intSurveyID.ToString();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
			Page.Validate();

			ArrayList parameters = new ArrayList();
			parameters.Add(new SqlParameter("@SurveyID", Convert.ToInt32(this.lblSurveyID.Text)));
			parameters.Add(new SqlParameter("@ProductID", Int32.Parse(this.ddlProduct.SelectedValue)));

			DataTable tblSurveys = myUtility.SqlExecuteQueryWithParameters("sp_GetSurveysInProgressByProductNotEqSurveyID", parameters);

			this.vldStatus.IsValid = Convert.ToBoolean(tblSurveys.Rows.Count == 0);

			if(Page.IsValid)
			{
				SqlCommand cmdInsert = new System.Data.SqlClient.SqlCommand();
				cmdInsert.CommandType = CommandType.StoredProcedure;
				cmdInsert.Connection = sqlConnection1;

				cmdInsert.Parameters.Add(new SqlParameter("@SurveyName", this.txtSurveyName.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@SurveyDescription", this.txtSurveyDescription.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@SurveyMessage", this.txtSurveyMessage.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@SurveyURL", this.txtSurveyURL.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@StartDate", this.txtStartDate.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@EndDate", this.txtEndDate.Text));
				cmdInsert.Parameters.Add(new SqlParameter("@SurveyStatus", this.ddlStatus.SelectedValue));
				cmdInsert.Parameters.Add(new SqlParameter("@ProductID", Int32.Parse(this.ddlProduct.SelectedValue)));
				
				SqlParameter paramID = new SqlParameter("@SurveyID", SqlDbType.Int);
				cmdInsert.Parameters.Add(paramID);

				if(this.lblSurveyID.Text == "0")
				{
					cmdInsert.CommandText = "sp_InsertSurvey";
					paramID.Direction = ParameterDirection.Output;
					this.lblStatusMessage.Text = "New Survey added successfully";
				}
				else
				{
					cmdInsert.CommandText = "sp_UpdateSurvey";
					paramID.Direction = ParameterDirection.Input;
					paramID.Value = Convert.ToInt32(this.lblSurveyID.Text);
					this.lblStatusMessage.Text = "Survey updated successfully";
				}

				sqlConnection1.Open();

				int rowsUpdated = cmdInsert.ExecuteNonQuery();
				sqlConnection1.Close();

			}
		} //// end  btnSubmit_Click
	} //// end class
}
