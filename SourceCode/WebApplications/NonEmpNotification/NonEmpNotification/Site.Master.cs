﻿// --------------------------------------------------------------
// <copyright file="Site.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmpNotification
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmpNotification.Classes;
    using System.Web.UI.HtmlControls;

    /// <summary>This is the Master page for the Web Application.</summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        /// <summary>Handles the OnInit event of the Page control.  This fixes Menu rendering in Chrome and Safari.</summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {    // For Chrome and Safari    
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                if (Request.Browser.Adapters.Count > 0)
                {
                    Request.Browser.Adapters.Clear();
                    Response.Redirect(Page.Request.Url.AbsoluteUri);
                }

            }
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/ShowHint.js"));

            if (!Page.IsPostBack)
            {
                this.btnAppName.Text = ConfigurationManager.AppSettings["AppName"];
                this.NavMenu.Items.Add(new MenuItem("Home", "Default.aspx"));

            }
        }

        /// <summary>Gets or sets the Shell.</summary>
        /// <value>The Shell.<value> 
        public System.Web.UI.HtmlControls.HtmlGenericControl Shell { get { return this.shell; } set { this.shell = value; } }


        /// <summary>Called when MenuItem is clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.MenuEventArgs"/> instance containing the event data.</param>
        public void OnClick(Object sender, MenuEventArgs e)
        {
            Debug.WriteLine("In OnClick");
            //MessageLabel.Text = "You selected " + e.Item.Text + ".";
            Debug.WriteLine("Value: " + e.Item.Value + "; Text: " + e.Item.Text);
            e.Item.Selected = true;
            string strPage = "~/" + e.Item.Value.Replace("Admin", "Admin/");
            Response.Redirect(strPage, true);
        }

        /// <summary>Handles the Click event of the btnAppName control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAppName_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx", true);
        }

        /// <summary>Gets or sets the BusinessJustification.</summary>
        public HtmlGenericControl Body
        {
            get
            {
                return myBody;
            }
        } 

    } //// end class
}