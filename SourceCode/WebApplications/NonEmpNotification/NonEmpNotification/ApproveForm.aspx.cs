﻿// ------------------------------------------------------------------
// <copyright file="ApproveForm.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2018 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace NonEmpNotification
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using NonEmpNotification.Classes;
    using System.Data.SqlClient;
    using System.Collections.ObjectModel;

    public partial class ApproveForm : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
	        string strCompany, strLocation, strToEmail;
            string strCCSupervisor =  string.Empty;
            string strEmailSubject =  string.Empty;
            string strBody =  string.Empty;

	        string strFromEmail, strEmployee, strCompanyID, strNonEmployeeComments;
	        string strReqID = Request.QueryString["ID"];
            string strApprove = string.Empty;

            string sQuery = "select Approve from NonEmpRequest where RequestID=" + strReqID;
            DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);
            if(dt.Rows.Count > 0)
            {
                strApprove = dt.Rows[0]["Approve"].ToString().ToUpper();
            }

            if (strApprove == "N") 
            {
               Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
                myParameters.Add(new SqlParameter("@RequestID", Int32.Parse(strReqID)));
                int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmpRequestToApproved", myParameters);

                sQuery = "select * from NonEmployee, NonEmpRequest, Company where (NonEmployee.BadgeID = NonEmpRequest.BadgeID and NonEmpRequest.RequestID=" + strReqID + "and Company.CompanyID = NonEmployee.CompanyID)";
                DataTable dtRead = SQLUtility.SqlExecuteDynamicQuery(sQuery);

                if (dtRead.Rows.Count > 0)
                {
                    DataRow rdrEmployee = dtRead.Rows[0];
                    strCompany = rdrEmployee["CompanyName"].ToString();
			        strCompanyID = rdrEmployee["CompanyID"].ToString();
			        strLocation = rdrEmployee["Location"].ToString();
			        if ((strLocation == "Null")) 
				        strLocation = "";

			        if ((Convert.ToString(rdrEmployee["COMMENTS"]) == "Null")) 
				        strNonEmployeeComments = "";
                    else 
				        strNonEmployeeComments = Convert.ToString(rdrEmployee["COMMENTS"]);

			        strEmployee = rdrEmployee["LName"].ToString().Trim().ToUpper() + ", " + rdrEmployee["FName"].ToString().Trim().ToUpper();
                    strEmailSubject = "Temp/Contractor " + rdrEmployee["RequestType"].ToString() + " Notification: " + strEmployee;

			        strBody = "<font face='Smith&NephewLF, Arial, sans-serif' size='-1'><b>Temporary Worker/Contractor Notification Submitted By:</b> " + rdrEmployee["Rep"].ToString() + "</font>";
			        strBody += "<br><table border='1' style='border-color:#999999;empty-cells:show;font-size:14px;font-family:Smith&NephewLF, Arial, sans-serif' cellpadding='5' cellspacing='0'>";
			        strBody +=  "<tr>";
			        strBody +=  "<td><b>Request Type: </b></td><td>" + rdrEmployee["RequestType"].ToString() + "</td>";
			        strBody += "<td><b>Employee Name: </b></td><td>" + strEmployee + "</td>";
			        strBody += "</tr><tr>";
			        strBody += "<td><b>Badge ID: </b></td><td>" + rdrEmployee["BadgeID"].ToString() + "</td>";
			        strBody += "<td><b>Effective Date: </b></td><td>" + rdrEmployee["EFFECTIVEDATE"].ToString() + "</td>";
			        strBody += "</tr><tr>";
			        strBody += "<td><b>Cost Center: </b></td><td>" + rdrEmployee["CostCenter"].ToString() + "</td>";
			        strBody += "<td><b>Job Title: </b></td><td>" + rdrEmployee["JobTitle"].ToString() + "</td>";
			        strBody += "</tr><tr>";
			        strBody += "<td><b>Company: </b></td><td>" + strCompany + "</td>";
			        strBody += "<td><b>Location: </b></td><td>" + strLocation + "</td>";
			        strBody += "</tr><tr>";
			        strBody += "<td><b>Supervisor: </b></td><td>" + rdrEmployee["SUPNAME"].ToString() + "</td>";
			        strBody += "<td><b>Supervisor Email: </b></td><td>" + rdrEmployee["SUPEMAIL"].ToString() + "</td>";
			        strBody += "</tr><tr>";
			        strBody += "<td valign=\"top\"><b>Temp/Contractor Comments: </b></td><td colspan=\"3\" valign=\"top\">" + strNonEmployeeComments.Replace("\r", "<br>") + "</td>";
			        strBody += "</tr>";
			        strBody += "</table>";

			        strEmployee += " (" + rdrEmployee["EFFECTIVEDATE"].ToString() + ")";
			        strCCSupervisor = rdrEmployee["SupEmail"].ToString();
			        if ((strCCSupervisor == "Null")) 
				        strCCSupervisor = "";
			    
		        } //// end while

		        //Send Notification'
		        strToEmail = ConfigurationManager.AppSettings["HrEmail"];
		        strFromEmail = ConfigurationManager.AppSettings["ContractApprovalEmail"];

		        Utility.SendEmail(strToEmail, strFromEmail, strCCSupervisor, strEmailSubject, strBody, true, ADUtility.GetUserNameOfAppUser(Request));
                //    SendMail(strToEmail, strCCSupervisor, strBCCEmail, strFromEmail, strEmailSubject, datagridHTML);

		        lblMessage.Text = "Thank you. New Contractor Notification has been approved and distributed.";
            } 
            else 
            {
		        lblMessage.Text = "This Contractor Notification has already been approved.";
            } //// end if

        }
    }
}