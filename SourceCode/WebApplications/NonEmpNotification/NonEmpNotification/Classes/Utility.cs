﻿// --------------------------------------------------------------
// <copyright file="Utility.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmpNotification.Classes
{
    using System;
    using System.Configuration;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Mail;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Messaging;
    using System.Web;

    /// <summary>This class has some useful methods.</summary>
    public class Utility
    {
        /// <summary>Prevents a default instance of the <see cref="Utility"/> class from being created.</summary>
        private Utility()
        {
        }

        /// <summary>Gets the Authenticated User.</summary>
        /// <param name="req">The web session request.</param>
        /// <returns>username string</returns>
        public static string CurrentUser(System.Web.HttpRequest req)
        {
            string[] arrayAuth = req.ServerVariables["AUTH_USER"].Split('\\'); ;

            if (arrayAuth.Length > 1)
            {
                return arrayAuth[1];
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>Gets the object number.</summary>
        /// <param name="o">The object.</param>
        /// <returns>the integer or zero if not a number</returns>
        public static int GetInt(object o)
        {
            try
            {
                return Convert.ToInt32(o);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetInt Error: " + ex.Message);
                return 0;
            }

        }

        public delegate string SendEmailDelegate(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml, string strUser);

        public static void GetResultsOnCallback(IAsyncResult ar)
        {
            SendEmailDelegate del = (SendEmailDelegate)((AsyncResult)ar).AsyncDelegate;
            try
            {
                string result;
                result = del.EndInvoke(ar);
                Debug.WriteLine("\nOn CallBack: result is " + result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\nOn CallBack, problem occurred: " + ex.Message);
            }
        }

        public static string SendEmailAsync(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml, string strUser)
        {
            SendEmailDelegate dc = new SendEmailDelegate(Utility.SendEmail);
            AsyncCallback cb = new AsyncCallback(Utility.GetResultsOnCallback);
            IAsyncResult ar = dc.BeginInvoke(strTo, strFrom, strCc, strSubj, strBody, isHtml, strUser, cb, null);

            return "ok";
        }

        /// <summary>Sends the email.</summary>
        /// <param name="strTo">The to address.</param>
        /// <param name="strFrom">The from address.</param>
        /// <param name="strSubj">The subjubject.</param>
        /// <param name="strBody">The body text.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        /// <param name="strUser">The user name</param>
        public static string SendEmail(string strTo, string strFrom, string strCc, string strSubj, string strBody, bool isHtml, string strUser)
        {
            MailMessage objEmail = new MailMessage();
            objEmail.From = new MailAddress(strFrom);

            //// Specify the message content.
            Debug.WriteLine("From: " + objEmail.From.Address);

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"]))
            {
                objEmail.To.Add(new MailAddress(strTo));

                if (strCc != String.Empty)
                {
                    objEmail.CC.Add(new MailAddress(strCc));
                }
            }
            else
            {
                ADUserEntity myUser = ADUtility.GetADUserByUserName(strUser);
                objEmail.To.Add(new MailAddress(myUser.Email));

                strBody += "<table><tr>";
                strBody += "<td>To: " + strTo + "</td>";
                strBody += "<td>CC: " + strCc + "</td>";
                strBody += "</tr></table>";
            }

            if (ConfigurationManager.AppSettings["ccEmail"] != null)
            {
                if (ConfigurationManager.AppSettings["ccEmail"] != String.Empty)
                {
                    objEmail.CC.Add(ConfigurationManager.AppSettings["ccEmail"]);
                }
            }

            objEmail.IsBodyHtml = isHtml;
            objEmail.Subject = strSubj;
            objEmail.Body = strBody;

            SmtpClient client = new SmtpClient();
         //   Debug.WriteLine("client: " + client.Host);
            Debug.WriteLine("strBody: " + strBody);

            try
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["sendNotification"]))
                {
                    client.Send(objEmail);
                }
            }
            catch (Exception excm)
            {
                Debug.WriteLine(excm.Message);
                LogException.HandleException(excm, strSubj);
            }

            return "sent";
        }

    }//// end class
}