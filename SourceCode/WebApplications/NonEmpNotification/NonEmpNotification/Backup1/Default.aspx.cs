﻿// ------------------------------------------------------------------
// <copyright file="Default.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------


namespace NonEmpNotification
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmpNotification.Classes;

    using System.Data.OleDb;
    using System.Data.Common;
    using System.IO;
    using System.Net.Mail;

    /// <summary>This is the Default page.</summary> 
    public partial class Default : System.Web.UI.Page
    {
        OleDbDataAdapter rdaCompany;
        OleDbConnection oOraConn = new System.Data.OleDb.OleDbConnection(ConfigurationManager.ConnectionStrings["OracleConnString"].ToString());
        OleDbCommand cmd = null;
        OleDbDataReader rdrEmployee = null;
        string strRead;
        string strEmployee;
        string strCCSupervisor;
        string strRequestType;
        string strEmpNum;
        //string intCompID;
        string strLocation;
        string strReq;
        string strSupEmail;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            string strNTUserID = ADUtility.GetUserNameOfAppUser(Request).ToUpper();
            lblUserID.Text = strNTUserID;

            //Show Current Date as Request Date'
            lblDate.Text = DateTime.Now.ToShortDateString();

            //If Page is Not PostBack'
            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtSearch, this.btnSearch);
                EnterButton.TieButton(this.txtSearchFName, this.btnFindEmpNum);
                EnterButton.TieButton(this.txtSearchLName, this.btnFindEmpNum);


                this.btnClear_Click(sender, e);
                this.ddlType_Change(sender, e);

                bool blnReportingEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["ReportingEnabled"]);
                this.btnReportTemp.Visible = blnReportingEnabled;
                this.btnReportContract.Visible = blnReportingEnabled;
            }

        }

        protected void ddlType_Change(object sender, EventArgs e)
        {

            this.lblDateType.Text = "Start";

            //tblFindEmpNum.Visible = false
            this.txtEmpNum.Enabled = false;
            txtEmpNum.Text = "Generated when form is submitted";

            this.txtSearch.Text = "";

            this.trowSearchResult.Visible = false;
            //If Request Type <> Select Notification Type'
            if (ddlType.SelectedIndex != 0)
            {
                //If Request Type = New Temp/Contractor'
                if (ddlType.SelectedIndex == 1)
                {
                    ddlCompany.SelectedIndex = 0;
                    txtEmpNum.Text = "Generated when form is submitted";

                    tblDetails.Visible = true;
                    trowSearch.Visible = false;
                    this.trowSearchResult.Visible = false;
                }
                else
                {
                    //IF Request Type <> New Temp/Contractor'	
                    trowSearch.Visible = true;
                    btnHelpFind.Visible = true;
                    tblDetails.Visible = false;
                }

                lblSearchResult.Text = "";
                lblSearchResult.Visible = false;

            }
            else
            {
                //If Select Request Type is Selected'

                tblDetails.Visible = false;
                trowSearch.Visible = false;
            }

            //Hide Request Information used in Revision option'

            trowRequest.Visible = false;
            //lblReqType.Visible = false;

            //Clear Information'
            btnClear_Click(sender, e);
            //Open DB Connection'
            //oOraConn.Open()							'
            //Add ddlCompany default value'

            rdaCompany = new OleDbDataAdapter("select * from pwdreset.Company order by CompanyName", oOraConn);
            DataSet rdsCompany = new DataSet();
            rdaCompany.Fill(rdsCompany, "Company");
            ddlCompany.DataSource = rdsCompany;
            ddlCompany.DataTextField = rdsCompany.Tables[0].Columns["CompanyName"].ColumnName.ToString();
            ddlCompany.DataValueField = rdsCompany.Tables[0].Columns["CompanyId"].ColumnName.ToString();
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, "Select Company");
            trowCalendar.Visible = false;
            tblFindEmpNum.Visible = false;
            trowSearchResult.Visible = false;
            //Close DB Connection'
            //oOraConn.Close()			'	

        }

        private bool IsNumeric(string strValue)
        {
            try
            {
                int result = Int32.Parse(strValue);
                return true;
            }
            catch
            {
                Debug.WriteLine("Not a valid integer");
                return false;
            }

        }

        //----------------------btnSubmit_Click (FORM IS SUBMITTED - VERIFY INFORMATION)---------------'
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //If Other Selected - require company name'
            if ((IsOtherCompany(ddlCompany.SelectedValue) & (txtOtherComp.Text == "")))
            {
                lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                lblSearchResult.Visible = true;
                //Check for other stuff'
            }
            else
            {
                //If Request Type <> Terminate'	
                if (((ddlType.SelectedIndex != 3) & (ddlType.SelectedIndex != 4)))
                {
                    //If one of the required fields is not filled out'
                    if (((txtFName.Text == "") | (txtLName.Text == "") | (txtDate.Text == "") | (txtSupName.Text == "") | (txtJob.Text == "") | (txtCC.Text == "") | (!(IsNumeric(txtCC.Text))) | (txtCC.Text.Length != 7) | (ddlCompany.SelectedIndex == 0)))
                    {
                        lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                        lblSearchResult.Visible = true;
                        tblDetails.Visible = true;
                        //If all required fields are filled out'
                    }
                    else
                    {
                        tblDetails.Visible = true;
                        lblSearchResult.Visible = false;
                        trowRequest.Visible = false;
                        //lblReqType.Visible = false;
                        SubmitForm(sender, e);
                        //Clear Form'
                        btnClear_Click(sender, e);
                    }

                    //If Request Type = Terminate'
                }
                else if ((ddlType.SelectedIndex == 3))  //Terminate
                {
                    //If one of the required fields is not filled out'
                    if (((txtLName.Text == "") | (txtFName.Text == "") | (txtDate.Text == "") | (txtSupName.Text == "") | (txtJob.Text == "") | (txtCC.Text == "") | (!(IsNumeric(txtCC.Text))) | (txtCC.Text.Length != 7) | (ddlCompany.SelectedIndex == 0) | (ddlReturn.SelectedIndex == 0)))
                    {
                        lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                        lblSearchResult.Visible = true;
                        tblDetails.Visible = true;
                        //If all required fields are filled out'
                    }
                    else
                    {
                        tblDetails.Visible = true;
                        lblSearchResult.Visible = false;
                        SubmitForm(sender, e);
                        //Clear Form'
                        btnClear_Click(sender, e);
                    }

                    //Request Type = Revision'
                }
                else
                {
                    //If one of the required fields is not filled out'
                    if (((txtLName.Text == "") | (txtFName.Text == "") | (txtDate.Text == "") | (txtSupName.Text == "") | (txtJob.Text == "") | (txtCC.Text == "") | (!(IsNumeric(txtCC.Text))) | (txtCC.Text.Length != 7) | (ddlCompany.SelectedIndex == 0)))
                    {
                        lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                        lblSearchResult.Visible = true;
                        tblDetails.Visible = true;
                        //If all required fields are filled out'
                    }
                    else
                    {
                        tblDetails.Visible = true;
                        lblSearchResult.Visible = false;
                        //Save Request and Employee Information Revision'
                        SaveRevision(sender, e);

                        //Clear Form'
                        btnClear_Click(sender, e);
                    }
                }
            }
        }

        private int GetRequestID(string strReqID)
        {
            try
            {
                return Convert.ToInt32(strReqID);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetRequestID: " + ex.Message);
                return 0;
            }

        }

        //-----------------------------SaveRevision(SAVE REQUEST AND EMPLOYEE INFO UPDATE)----------'
        protected void SaveRevision(object sender, EventArgs e)
        {
            Debug.WriteLine("SaveRevision");

            object intCompID;
            oOraConn.Open();
            FixDate(sender, e);
            //Generate Company ID'
            if (IsOtherCompany(ddlCompany.SelectedValue))
            {
                //Insert Company'
                strRead = "Insert Into pwdreset.Company Values (select max(CompanyID) from pwdreset.company + 1), txtOtherComp.Text";
                cmd = new OleDbCommand(strRead, oOraConn);
                object sqlInsert = cmd.ExecuteScalar();
                //Retrive CompanyID'
                strRead = "select CompanyID from pwdreset.Company where CompanyName ='" + txtOtherComp.Text + "'";
                cmd = new OleDbCommand(strRead, oOraConn);
                intCompID = cmd.ExecuteScalar();
            }
            else
            {
                intCompID = ddlCompany.SelectedValue;
            }

            //Update Request Info'
            strRead = "Update pwdreset.NonEmpRequest Set EFFECTIVEDATE=:NewDate, REP=:Rep  WHERE REQUESTID=" + lblReqID.Text;
            cmd = new OleDbCommand(strRead, oOraConn);
            cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
            cmd.Parameters.Add(new OleDbParameter("Rep", lblUserID.Text));
            object sqlUpdate = cmd.ExecuteScalar();

            //For Term - update eligible to return'
            if ((lblReqType.Text == "Terminate"))
            {
                strRead = "Update pwdreset.NonEmpRequest Set ELIGIBLERTN=:RTN, EFFECTIVEDATE=:NewDate WHERE REQUESTID=" + lblReqID.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("RTN", ddlReturn.SelectedValue));
                cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
                sqlUpdate = cmd.ExecuteScalar();
            }

            Debug.WriteLine("New: " + Convert.ToBoolean(lblReqType.Text == "New").ToString());
            Debug.WriteLine("lblReqType: " + lblReqType.Text);

            //For NEW HIRE'
          //  if ((lblReqType.Text == "New"))

            // Parameters need to be in the same order as they are listed in the Update Query

            if ((lblReqType.Text == "Add"))
            {
                Debug.WriteLine("In lblReqType.Text == 'New'");
                strRead = "Update pwdreset.NonEmployee Set FNAME =:FNAME, LNAME = :LNAME, JOBTITLE = :JOBTITLE, COMPANYID = :COMPANY, LOCATION=:LOCATION, SUPNAME = :SUPNAME, SUPEMAIL = :SUPEMAIL, COMMENTS=:COMMENTS, COSTCENTER=:COSTCENTERNUM, STARTDATE=:NEWDATE WHERE BADGEID =" + txtEmpNum.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("FNAME", txtFName.Text.Trim().ToUpper()));
                cmd.Parameters.Add(new OleDbParameter("LNAME", txtLName.Text.Trim().ToUpper()));
                cmd.Parameters.Add(new OleDbParameter("JOBTITLE", txtJob.Text));
                cmd.Parameters.Add(new OleDbParameter("COMPANY", intCompID));
                if ((txtLocation.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("LOCATION", txtLocation.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("LOCATION", "Null"));
                }
                cmd.Parameters.Add(new OleDbParameter("SUPNAME", txtSupName.Text));
                if ((txtSupEmail.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("SUPEMAIL", txtSupEmail.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("SUPEMAIL", "Null"));
                }
                if ((txtNonEmployeeComments.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("COMMENTS", txtNonEmployeeComments.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("COMMENTS", "Null"));
                }
                cmd.Parameters.Add(new OleDbParameter("COSTCENTERNUM", txtCC.Text));
                cmd.Parameters.Add(new OleDbParameter("NEWDATE", txtDate.Text));
                sqlUpdate = cmd.ExecuteScalar();

                //Update Request'
                strRead = "Update pwdreset.NonEmpRequest Set EFFECTIVEDATE=:NewDate WHERE REQUESTID=" + lblReqID.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
                sqlUpdate = cmd.ExecuteScalar();

                SendNotification(this.GetRequestID(lblReqID.Text));

                //Transfer'
            }
            else if ((lblReqType.Text == "Transfer"))
            {
                Debug.WriteLine("In lblReqType.Text == 'Transfer'");
                strRead = "Update pwdreset.NonEmployee Set FNAME =:FNAME, LNAME = :LNAME, JOBTITLE = :JOBTITLE, COMPANYID = :COMPANY, LOCATION=:LOCATION, SUPNAME = :SUPNAME, SUPEMAIL = :SUPEMAIL, COMMENTS=:COMMENTS, COSTCENTER=:COSTCENTERNUM WHERE BADGEID =" + txtEmpNum.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("FNAME", txtFName.Text.Trim().ToUpper()));
                cmd.Parameters.Add(new OleDbParameter("LNAME", txtLName.Text.Trim().ToUpper()));
                cmd.Parameters.Add(new OleDbParameter("JOBTITLE", txtJob.Text));
                cmd.Parameters.Add(new OleDbParameter("COMPANY", intCompID));
                if ((txtLocation.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("LOCATION", txtLocation.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("LOCATION", "Null"));
                }
                cmd.Parameters.Add(new OleDbParameter("SUPNAME", txtSupName.Text));
                if ((txtSupEmail.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("SUPEMAIL", txtSupEmail.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("SUPEMAIL", "Null"));
                }
                if ((txtNonEmployeeComments.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("COMMENTS", txtNonEmployeeComments.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("COMMENTS", "Null"));
                }
                cmd.Parameters.Add(new OleDbParameter("COSTCENTERNUM", txtCC.Text));
                sqlUpdate = cmd.ExecuteScalar();

                //Update Request'
                strRead = "Update pwdreset.NonEmpRequest Set EFFECTIVEDATE=:NewDate WHERE REQUESTID=" + lblReqID.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
                sqlUpdate = cmd.ExecuteScalar();

                SendNotification(this.GetRequestID(lblReqID.Text));

                //Terminate'
            }
            else
            {
                Debug.WriteLine("In lblReqType.Text == 'Terminate'");
                strRead = "Update pwdreset.NonEmployee Set FNAME =:FNAME, LNAME = :LNAME, JOBTITLE = :JOBTITLE, COMPANYID = :COMPANY, LOCATION=:LOCATION, SUPNAME = :SUPNAME, SUPEMAIL = :SUPEMAIL, COMMENTS=:COMMENTS, COSTCENTER=:COSTCENTERNUM WHERE BADGEID =" + txtEmpNum.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("FNAME", txtFName.Text.Trim().ToUpper()));
                cmd.Parameters.Add(new OleDbParameter("LNAME", txtLName.Text.Trim().ToUpper()));
                cmd.Parameters.Add(new OleDbParameter("JOBTITLE", txtJob.Text));
                cmd.Parameters.Add(new OleDbParameter("COMPANY", intCompID));
                if ((txtLocation.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("LOCATION", txtLocation.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("LOCATION", "Null"));
                }
                cmd.Parameters.Add(new OleDbParameter("SUPNAME", txtSupName.Text));
                if ((txtSupEmail.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("SUPEMAIL", txtSupEmail.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("SUPEMAIL", "Null"));
                }
                if ((txtNonEmployeeComments.Text != ""))
                {
                    cmd.Parameters.Add(new OleDbParameter("COMMENTS", txtNonEmployeeComments.Text));
                }
                else
                {
                    cmd.Parameters.Add(new OleDbParameter("COMMENTS", "Null"));
                }
                cmd.Parameters.Add(new OleDbParameter("COSTCENTERNUM", txtCC.Text));
                
                sqlUpdate = cmd.ExecuteScalar();

                //Update Request'
                strRead = "Update pwdreset.NonEmpRequest Set EFFECTIVEDATE=:NewDate WHERE REQUESTID=" + lblReqID.Text;
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
                sqlUpdate = cmd.ExecuteScalar();

                SendNotification(this.GetRequestID(lblReqID.Text));
            }

            //Set Request Type'
            ddlType.SelectedIndex = 0;
            ddlType_Change(sender, e);
            oOraConn.Close();
            //END IF'
        }


        //-------------------------SubmitForm (INFORMATION IS SAVED TO THE DB)-----------------------------'
        protected void SubmitForm(object sender, EventArgs e)
        {
            //Declare Variables'	
            object objReqID, sqlInsert;
            int intReqID, intBadgeID, intCompID;

            oOraConn.Open();
            //Generate Request ID'
            strRead = "select max(RequestID) from pwdreset.NonEmpRequest";
            cmd = new OleDbCommand(strRead, oOraConn);
            objReqID = cmd.ExecuteScalar();
            if ((object.ReferenceEquals(objReqID, DBNull.Value)))
            {
                intReqID = 1;
            }
            else
            {
                intReqID = Convert.ToInt32(objReqID) + 1;
            }

            FixDate(sender, e);

            //If company is set to Other - Generate Company ID and insert company into DB'
            if (IsOtherCompany(ddlCompany.SelectedValue))
            {
                //Insert Company'
                strRead = "Insert Into pwdreset.Company Values((select max(CompanyID) from pwdreset.company) + 1,'" + txtOtherComp.Text + "')";
                cmd = new OleDbCommand(strRead, oOraConn);
                sqlInsert = cmd.ExecuteScalar();

                //Retrive CompanyID'
                strRead = "select CompanyID from pwdreset.Company where CompanyName ='" + txtOtherComp.Text + "'";
                cmd = new OleDbCommand(strRead, oOraConn);
                object objCompID = cmd.ExecuteScalar();
                try
                {
                    intCompID = Convert.ToInt32(objCompID);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("intCompID Convert Error: " + ex.Message);
                    intCompID = 0;
                }
            }
            else
            {
                intCompID = Convert.ToInt32(ddlCompany.SelectedValue);
            }

            //If Request Type = NEW Temp/Contractor'
            if ((ddlType.SelectedIndex == 1))
            {
                strRead = "select max(BadgeID) from pwdreset.NonEmployee";
                cmd = new OleDbCommand(strRead, oOraConn);
                intBadgeID = Convert.ToInt32(cmd.ExecuteScalar()) + 1;

                //INSERT NEW REQUEST INFO'
                //If Peoplemark - approve column is empty'
                if (IsTempAgency(ddlCompany.SelectedValue))
                {
 //                   strRead = "Insert into pwdreset.NonEmpRequest(REQUESTID, REQUESTDATE, REQUESTTYPE, BADGEID, Rep, EFFECTIVEDATE) Values (:intReqID, :REQUESTDATE, :REQUESTTYPE, :BADGEID, :Rep, :EffectiveDate)";

                    strRead = "Insert into pwdreset.NonEmpRequest(REQUESTID, REQUESTDATE, REQUESTTYPE, BADGEID, Rep, EFFECTIVEDATE) Values ";
                    strRead += "(" + intReqID.ToString() + ", '" + lblDate.Text + "', '" + ddlType.SelectedValue + "', " + intBadgeID.ToString() + ", '" + lblUserID.Text + "', '" + txtDate.Text + "')";
                    Debug.WriteLine("strRead: " + strRead);

                    //Not Peoplemark - approval = No'
                }
                else
                {
//                    strRead = "Insert into pwdreset.NonEmpRequest(REQUESTID, REQUESTDATE, REQUESTTYPE, BADGEID, Rep, EFFECTIVEDATE, APPROVE) Values (:intReqID, :REQUESTDATE, :REQUESTTYPE, :BADGEID, :Rep, :EffectiveDate, 'N')";

                    strRead = "Insert into pwdreset.NonEmpRequest(REQUESTID, REQUESTDATE, REQUESTTYPE, BADGEID, Rep, EFFECTIVEDATE, APPROVE) Values ";
                    strRead += "(" + intReqID.ToString() + ", '" + lblDate.Text + "', '" + ddlType.SelectedValue + "', " + intBadgeID.ToString() + ", '" + lblUserID.Text + "', '" + txtDate.Text + "', 'N')";
                    Debug.WriteLine("strRead: " + strRead);
                }
                cmd = new OleDbCommand(strRead, oOraConn);
                //cmd.Parameters.Add("intReqID", intReqID);
                //cmd.Parameters.Add("REQUESTDATE", lblDate.Text);
                //cmd.Parameters.Add("REQUESTTYPE", ddlType.SelectedValue);
                //cmd.Parameters.Add("BADGEID", intBadgeID);
                //cmd.Parameters.Add("Rep", lblUserID.Text);
                //cmd.Parameters.Add("EffectiveDate", txtDate.Text);
                sqlInsert = cmd.ExecuteScalar();

                //INSERT NEW EMPLOYEE INFO'			
                //If Peoplemark - ACTIVE = Y'
                if (IsTempAgency(ddlCompany.SelectedValue))
                {
//                    strRead = "Insert into pwdreset.NonEmployee (BADGEID, FNAME, LNAME, STARTDATE, JOBTITLE, LOCATION, SUPNAME, SUPEMAIL, COMMENTS, COSTCENTER, COMPANYID, ACTIVE) Values (:BADGEID, :FNAME, :LNAME, :STARTDATE, :JOBTITLE, :LOCATION, :SUPNAME, :SUPEMAIL, :COMMENTS, :COSTCENTER, :COMPANYID, 'Y')";

                    strRead = "Insert into pwdreset.NonEmployee (BADGEID, FNAME, LNAME, STARTDATE, JOBTITLE, LOCATION, SUPNAME, SUPEMAIL, COMMENTS, COSTCENTER, COMPANYID, ACTIVE) Values (";
                    strRead += intBadgeID.ToString() + ", '" + txtFName.Text.Trim().ToUpper() + "', '" + txtLName.Text.Trim().ToUpper() + "', '" + txtDate.Text + "', '";
                    strRead += txtJob.Text + "', '" + txtLocation.Text + "', '" + txtSupName.Text + "', '" + txtSupEmail.Text + "', '" + txtNonEmployeeComments.Text + "', '";
                    strRead += txtCC.Text + "', " + intCompID.ToString() + ", 'Y')";
                    //Not Peoplemark - ACTIVE = N'
                }
                else
                {
                    //strRead = "Insert into pwdreset.NonEmployee (BADGEID, FNAME, LNAME, STARTDATE, JOBTITLE, LOCATION, SUPNAME, SUPEMAIL, COMMENTS, COSTCENTER, COMPANYID, ACTIVE) Values (:BADGEID, :FNAME, :LNAME, :STARTDATE, :JOBTITLE, :LOCATION, :SUPNAME, :SUPEMAIL, :COMMENTS, :COSTCENTER, :COMPANYID, 'N')";

                    strRead = "Insert into pwdreset.NonEmployee (BADGEID, FNAME, LNAME, STARTDATE, JOBTITLE, LOCATION, SUPNAME, SUPEMAIL, COMMENTS, COSTCENTER, COMPANYID, ACTIVE) Values (";
                    strRead += intBadgeID.ToString() + ", '" + txtFName.Text.Trim().ToUpper() + "', '" + txtLName.Text.Trim().ToUpper() + "', '" + txtDate.Text + "', '";
                    strRead += txtJob.Text + "', '" + txtLocation.Text + "', '" + txtSupName.Text + "', '" + txtSupEmail.Text + "', '" + txtNonEmployeeComments.Text + "', '";
                    strRead += txtCC.Text + "', " + intCompID.ToString() + ", 'N')";
                }
                cmd = new OleDbCommand(strRead, oOraConn);
                //cmd.Parameters.Add("BADGEID", intBadgeID);
                //cmd.Parameters.Add("FNAME", txtFName.Text.Trim().ToUpper());
                //cmd.Parameters.Add("LNAME", txtLName.Text.Trim().ToUpper());
                //cmd.Parameters.Add("STARTDATE", txtDate.Text);
                //cmd.Parameters.Add("JOBTITLE", txtJob.Text);
                //cmd.Parameters.Add("SUPNAME", txtSupName.Text);
                //if ((txtNonEmployeeComments.Text != ""))
                //{
                //    cmd.Parameters.Add("COMMENTS", txtNonEmployeeComments.Text);
                //}
                //else
                //{
                //    cmd.Parameters.Add("COMMENTS", "Null");
                //}
                //cmd.Parameters.Add("COSTCENTER", txtCC.Text);
                //cmd.Parameters.Add("COMPANYID", intCompID);
                //if ((txtSupEmail.Text != ""))
                //{
                //    cmd.Parameters.Add("SUPEMAIL", txtSupEmail.Text);
                //}
                //else
                //{
                //    cmd.Parameters.Add("SUPEMAIL", "Null");
                //}
                //if ((txtLocation.Text != ""))
                //{
                //    cmd.Parameters.Add("LOCATION", txtLocation.Text);
                //}
                //else
                //{
                //    cmd.Parameters.Add("LOCATION", "Null");
                //}

                Debug.WriteLine("Insert Query: " + cmd.CommandText);
                sqlInsert = cmd.ExecuteScalar();
                SendNotification(intReqID);

                //If Request Type = De-activate'
            }
            else if ((ddlType.SelectedIndex == 3))
            {
                //INSERT NEW REQUEST INFO'
                strRead = "Insert into pwdreset.NonEmpRequest(REQUESTID, REQUESTDATE, REQUESTTYPE, BADGEID, Rep, EFFECTIVEDATE, ELIGIBLERTN) Values ";
                strRead += "(" + intReqID.ToString()+ ", '" +  lblDate.Text + "', '" + ddlType.SelectedValue + "', " + txtEmpNum.Text + ", '" + lblUserID.Text + "', '" + txtDate.Text + "', '" + ddlReturn.SelectedValue + "')";
                Debug.WriteLine("strRead: " + strRead);

                cmd = new OleDbCommand(strRead, oOraConn);
                sqlInsert = cmd.ExecuteScalar();

                //UPDATE EMPLOYEE INFO'		
                strRead = "Update pwdreset.NonEmployee Set ";
                strRead += "FNAME = '" + txtFName.Text.Trim().ToUpper() + "', ";

                strRead += "LNAME = '" + txtLName.Text.Trim().ToUpper() + "', ";
                strRead += "JOBTITLE = '" + txtJob.Text + "', ";
                strRead += "LOCATION = '" + txtLocation.Text + "', ";
                strRead += "SUPNAME = '" + txtSupName.Text + "', ";
                strRead += "SUPEMAIL = '" + txtSupEmail.Text + "', ";
                strRead += "COMMENTS = '" + txtNonEmployeeComments.Text + "', ";
                strRead += "COSTCENTER = '" + txtCC.Text + "', ";
                strRead += "COMPANYID = " + intCompID.ToString() + ", ";
                strRead += "ACTIVE = 'N' ";
                strRead += "WHERE BADGEID =" + txtEmpNum.Text;

                Debug.WriteLine("strRead: " + strRead);
                
                cmd = new OleDbCommand(strRead, oOraConn);

                object sqlUpdate = cmd.ExecuteScalar();
                SendNotification(intReqID);

                //If Request Type = Update/Activate'
            }
            else
            {
                //Insert Request Info'
                strRead = "Insert into pwdreset.NonEmpRequest(REQUESTID, REQUESTDATE, REQUESTTYPE, BADGEID, Rep, EFFECTIVEDATE) Values ";
                strRead += "(" + intReqID.ToString() + ", '" + lblDate.Text + "', '" + ddlType.SelectedValue + "', " + txtEmpNum.Text + ", '";
                strRead += lblUserID.Text + "', '" + txtDate.Text + "')";
                Debug.WriteLine("strRead: " + strRead);

                cmd = new OleDbCommand(strRead, oOraConn);
                sqlInsert = cmd.ExecuteScalar();
                
                //UPDATE EMPLOYEE INFO'		
                strRead = "Update pwdreset.NonEmployee Set ";
                strRead += "FNAME = '" + txtFName.Text.Trim().ToUpper() + "', ";
                strRead += "LNAME = '" + txtLName.Text.Trim().ToUpper() + "', ";
                strRead += "JOBTITLE = '" + txtJob.Text + "', ";
                strRead += "LOCATION = '" + txtLocation.Text + "', ";
                strRead += "SUPNAME = '" + txtSupName.Text + "', ";
                strRead += "SUPEMAIL = '" + txtSupEmail.Text + "', ";
                strRead += "COMMENTS = '" + txtNonEmployeeComments.Text + "', ";
                strRead += "COSTCENTER = '" + txtCC.Text + "', ";
                strRead += "COMPANYID = " + intCompID.ToString() + ", ";
                strRead += "ACTIVE = 'Y' ";
                strRead += "WHERE BADGEID =" + txtEmpNum.Text;
                cmd = new OleDbCommand(strRead, oOraConn);

                object sqlUpdate = cmd.ExecuteScalar();
                SendNotification(intReqID);
            }

            lblSearchResult.Text = "Thank you. Notification has been distributed/submitted for approval.";
            txtSearch.Text = "";
            lblSearchResult.Visible = true;
            tblDetails.Visible = false;

            //Set Request Type'
            ddlType.SelectedIndex = 0;
            ddlType_Change(sender, e);
            //Close DB Connection'
            oOraConn.Close();

        } //// end SubmitForm



        //'---------------------btnSearch_Click (SEARCH BY EMPLOYEE NUMBER)-------------------------'
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("In btnSearch_Click");

            if (txtSearch.Text != "")
            {
                lblSearchResult.Text = "";
                lblSearchResult.Visible = false;
                btnHelpFind.Visible = true;
                tblFindEmpNum.Visible = false;
                //Populate Form'	
                oOraConn.Open();
                if (ddlType.SelectedIndex != 4)
                {
                    Debug.WriteLine("In ddlType.SelectedIndex != 4");
                    //Search and pre-populate Update/De-activate Info'
                    trowSearchResult.Visible = false;
                    if (ddlType.SelectedIndex == 3)
                    {
                        lblDateType.Text = "End";
                    }
                    else
                    {
                        if (ddlType.SelectedIndex == 2)
                            lblDateType.Text = "Transfer";
                    }

                    this.SearchTransTerm(sender, e);
                }
                else
                {
                    //If Revision'
                    strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (NonEmployee.BadgeID = " + txtSearch.Text + " and pwdreset.NonEmpRequest.BadgeID=pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID = pwdreset.Company.CompanyID) Order By pwdreset.NonEmpRequest.RequestDate DESC, pwdreset.NonEmpRequest.RequestType";
                    cmd = new OleDbCommand(strRead, oOraConn);
                    rdrEmployee = cmd.ExecuteReader();
                    if (rdrEmployee.HasRows)
                    {
                        dgSearchRequest.DataSource = rdrEmployee;
                        dgSearchRequest.DataBind();
                        tblFindEmpNum.Visible = true;
                        trowSearchResult.Visible = true;
                        dgSearchRequest.Visible = true;
                        btnSearchSelectedRequest.Visible = true;
                        tblDetails.Visible = false;

                        trowRequest.Visible = false;
                        //lblReqType.Visible = false;
                        dgSearchResults.Visible = false;
                        lblSearchResult.Visible = false;
                        btnSearchSelected.Visible = false;
                    }
                    else
                    {
                        ShowError(sender, e);
                        trowSearchResult.Visible = false;
                        trowRequest.Visible = false;
                        //lblReqType.Visible = false;
                        btnClear_Click(sender, e);
                    }
                    rdrEmployee.Close();
                }
                oOraConn.Close();
            }
            else
            {
                lblSearchResult.Text = "Please enter a Badge ID or click Help Find Badge ID button.";
                lblSearchResult.Visible = true;
            }
        }

        //'----------------------------SearchTransTerm(Search for Transfer/Term Information)-------------------'
        protected void SearchTransTerm(object sender, EventArgs e)
        {
            Debug.WriteLine("In SearchTransTerm");
            //Hide Request Information used in Revision purpose'
            trowRequest.Visible = false;
            //lblReqType.Visible = false;

            strRead = "select * from pwdreset.NonEmployee where BadgeID=" + txtSearch.Text;
            cmd = new OleDbCommand(strRead, oOraConn);
            rdrEmployee = cmd.ExecuteReader();
            if (rdrEmployee.HasRows)
            {
                tblDetails.Visible = true;
                lblSearchResult.Visible = false;
                while (rdrEmployee.Read())
                {
                    txtFName.Text = Convert.ToString(rdrEmployee["FNAME"]);
                    txtLName.Text = Convert.ToString(rdrEmployee["LNAME"]);
                    txtEmpNum.Text = Convert.ToString(rdrEmployee["BadgeID"]);
                    txtCC.Text = Convert.ToString(rdrEmployee["CostCenter"]);
                    txtJob.Text = Convert.ToString(rdrEmployee["JOBTITLE"]);
                    ddlCompany.SelectedValue = Convert.ToString(rdrEmployee["COMPANYID"]);
                    txtSupName.Text = Convert.ToString(rdrEmployee["SUPNAME"]);
                    if (Convert.ToString(rdrEmployee["SUPEmail"]) == "Null")
                        txtSupEmail.Text = "";
                    else
                        txtSupEmail.Text = Convert.ToString(rdrEmployee["SUPEmail"]);

                    if (Convert.ToString(rdrEmployee["Location"]) == "Null")
                        txtLocation.Text = "";
                    else
                        txtLocation.Text = Convert.ToString(rdrEmployee["LOCATION"]);

                    if (Convert.ToString(rdrEmployee["COMMENTS"]) == "Null")
                        txtNonEmployeeComments.Text = "";
                    else
                        txtNonEmployeeComments.Text = Convert.ToString(rdrEmployee["COMMENTS"]);

                }
                rdrEmployee.Close();

                if (ddlType.SelectedIndex == 3)
                    tblReturn.Visible = true;
                else
                    tblReturn.Visible = false;

                tblDetails.Visible = true;

            }
            else
            {
                //Record with this employee id does not exist'
                ShowError(sender, e);
                btnClear_Click(sender, e);
                txtEmpNum.Text = txtSearch.Text;
            }
        }


        //------------------------btnFindEmpNum_Click (SEARCH FOR EMP.# BY First Name, Last Name)---------'
        protected void btnFindEmpNum_Click(object sender, EventArgs e)
        {
            oOraConn.Open();
            object strLName, strFName;
            strLName = txtSearchLName.Text.ToUpper();
            strFName = txtSearchFName.Text.ToUpper();
            strFName = "%" + strFName + "%";

            //If NOT a revision'
            if ((ddlType.SelectedValue != "Revision"))
            {
                strRead = "select * from pwdreset.NonEmployee where (LName = :LNAME and FNAME LIKE :FNAME)";
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("LNAME", strLName));
                cmd.Parameters.Add(new OleDbParameter("FNAME", strFName));
                rdrEmployee = cmd.ExecuteReader();
                if ((rdrEmployee.HasRows))
                {
                    dgSearchResults.DataSource = rdrEmployee;
                    dgSearchResults.DataBind();
                    trowSearchResult.Visible = true;
                    dgSearchResults.Visible = true;
                    btnSearchSelected.Visible = true;
                    dgSearchRequest.Visible = false;
                    lblSearchResult.Visible = false;
                    btnSearchSelectedRequest.Visible = false;
                }
                else
                {
                    btnClear_Click(sender, e);
                    txtEmpNum.Text = txtSearch.Text;
                }
            }
            else
            {
                //If Revision'
                strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (pwdreset.NonEmployee.LNAME=:LName and pwdreset.NonEmployee.FNAME LIKE :FNAME and pwdreset.NonEmployee.BadgeID=pwdreset.NonEmpRequest.BadgeID AND pwdreset.Company.CompanyID=pwdreset.NonEmployee.CompanyID)";
                cmd = new OleDbCommand(strRead, oOraConn);
                cmd.Parameters.Add(new OleDbParameter("LNAME", strLName));
                cmd.Parameters.Add(new OleDbParameter("FNAME", strFName));
                rdrEmployee = cmd.ExecuteReader();
                if ((rdrEmployee.HasRows))
                {
                    dgSearchRequest.DataSource = rdrEmployee;
                    dgSearchRequest.DataBind();
                    tblDetails.Visible = true;
                    trowSearchResult.Visible = true;
                    dgSearchRequest.Visible = true;
                    btnSearchSelectedRequest.Visible = true;
                    dgSearchResults.Visible = false;
                    lblSearchResult.Visible = false;
                    btnSearchSelected.Visible = false;
                }
                else
                {
                    ShowError(sender, e);
                    trowSearchResult.Visible = false;
                    btnClear_Click(sender, e);
                }
                tblDetails.Visible = false;
            }
            rdrEmployee.Close();
            oOraConn.Close();
            //tblDetails.Visible = false	'

            //Clear Search Fields'
            txtSearchFName.Text = "";
            txtSearchLName.Text = "";
            txtSearch.Text = "";
        }

        //------------------------btnSearchSelected_Click (SEARCH BY CHECKED EMPLOYEE NUMBER)---------------'
        protected void btnSearchSelected_Click(object sender, EventArgs e)
        {
            //Make sure that at least one box is checked'	  
            foreach (DataGridItem dgItem in dgSearchResults.Items)
            {
                //Find controls of type Checkbox'
                CheckBox chkSelection = dgItem.FindControl("chkSelection") as CheckBox;
                if (chkSelection.Checked)
                {
                    //Find Employee ID in that row'
                    txtSearch.Text = ((Label)dgItem.FindControl("BADGEID")).Text;
                    tblFindEmpNum.Visible = false;
                    trowSearchResult.Visible = false;
                }
            }
            if ((txtSearch.Text != ""))
            {
                lblSearchResult.Visible = false;
                btnSearch_Click(sender, e);
            }
            else
            {
                lblSearchResult.Text = "Select a record and try again.";
                lblSearchResult.Visible = true;
            }
        }



        //------------------------btnSearchSelectedRequest_Click (SEARCH BY CHECKED R)---------------'
        protected void btnSearchSelectedRequest_Click(object sender, EventArgs e)
        {
            tblOtherCompany.Visible = false;
            //Make sure that at least one box is checked'	  
            object strRequestID;

            tblDetails.Visible = true;
            trowRequest.Visible = true;
            //lblReqType.Visible = false;
            lblSearchResult.Visible = false;

            oOraConn.Open();
            foreach (DataGridItem dgItem in dgSearchRequest.Items)
            {
                //Find controls of type Checkbox'
                CheckBox chkSelection = dgItem.FindControl("chkSelection") as CheckBox;

                if (chkSelection.Checked)
                {
                    //Find REQUESTID in that row; Retrieve Info from HRRequest Table'
                    strRequestID = ((Label)dgItem.FindControl("RequestID")).Text;
                    strRead = "select * from pwdreset.NonEmpRequest where NonEmpRequest.RequestID=" + strRequestID;
                    cmd = new OleDbCommand(strRead, oOraConn);
                    rdrEmployee = cmd.ExecuteReader();
                    while ((rdrEmployee.Read()))
                    {
                        lblReqDate.Text = String.Format("{0:d}", rdrEmployee["RequestDate"]);
                        lblReqType.Text = Convert.ToString(rdrEmployee["RequestType"]);
                        lblReqID.Text = Convert.ToString(rdrEmployee["RequestID"]);
                        if ((rdrEmployee["RequestType"].ToString() == "Transfer"))
                        {
                            txtDate.Text = String.Format("{0:d}", rdrEmployee["EFFECTIVEDATE"]);
                        }
                        else if ((rdrEmployee["RequestType"].ToString() == "Terminate"))
                        {
                            txtDate.Text = String.Format("{0:d}", rdrEmployee["EFFECTIVEDATE"]);
                            ddlReturn.SelectedValue = Convert.ToString(rdrEmployee["EligibleRtn"]);
                        }
                    }
                    if ((lblReqType.Text == "Terminate"))
                    {
                        tblReturn.Visible = true;
                        lblDateType.Text = "Termination";
                    }
                    else if ((lblReqType.Text == "Add"))
                    {
                        lblDateType.Text = "Start";
                        tblReturn.Visible = false;
                    }
                    else
                    {
                        lblDateType.Text = "Transfer";
                        tblReturn.Visible = false;
                    }
                    //---FIND EMP NUM in that row; Retrieve Info from Employee Table----'
                    strEmpNum = ((Label)dgItem.FindControl("BADGEID")).Text;
                    strRead = "select * from pwdreset.NonEmployee where BADGEID='" + strEmpNum + "'";
                    cmd = new OleDbCommand(strRead, oOraConn);
                    rdrEmployee = cmd.ExecuteReader();
                    while ((rdrEmployee.Read()))
                    {
                        if ((lblReqType.Text == "New"))
                        {
                            txtDate.Text = Convert.ToString(rdrEmployee["StartDate"]);
                        }
                        txtFName.Text = Convert.ToString(rdrEmployee["FNAME"]);
                        txtLName.Text = Convert.ToString(rdrEmployee["LNAME"]);
                        txtEmpNum.Text = Convert.ToString(rdrEmployee["BADGEID"]);
                        ddlCompany.SelectedValue = Convert.ToString(rdrEmployee["CompanyID"]);
                        txtCC.Text = Convert.ToString(rdrEmployee["CostCenter"]);
                        txtJob.Text = Convert.ToString(rdrEmployee["JOBTITLE"]);
                        if ((Convert.ToString(rdrEmployee["Location"]) == "Null"))
                        {
                            txtLocation.Text = "";
                        }
                        else
                        {
                            txtLocation.Text = Convert.ToString(rdrEmployee["LOCATION"]);
                        }

                        if ((Convert.ToString(rdrEmployee["SUPEmail"]) == "Null"))
                        {
                            txtSupEmail.Text = "";
                        }
                        else
                        {
                            txtSupEmail.Text = Convert.ToString(rdrEmployee["SUPEmail"]);
                        }
                        if ((Convert.ToString(rdrEmployee["COMMENTS"]) == "Null"))
                        {
                            txtNonEmployeeComments.Text = "";
                        }
                        else
                        {
                            txtNonEmployeeComments.Text = Convert.ToString(rdrEmployee["COMMENTS"]);
                        }
                        txtSupName.Text = Convert.ToString(rdrEmployee["SUPNAME"]);
                    }
                }
                else
                {
                    //No Record Selected'
                }
            }
            oOraConn.Close();


            Debug.WriteLine("lblReqType: " + lblReqType.Text);
        }

        //'--------------------------btnHelpFind_Click (LOOK FOR EMP.NUMBER, SHOW SEARCH BAR)--------'
        protected void btnHelpFind_Click(object sender, EventArgs e)
        {
            tblFindEmpNum.Visible = true;
            trowSearchResult.Visible = false;
            btnHelpFind.Visible = false;
            lblSearchResult.Visible = false;
        }

        //'-------------------------btnCalendar_Click (SHOW CALENDAR)---------------------------------'
        protected void btnCalendar_Click(object sender, EventArgs e)
        {
            trowCalendar.Visible = true;
            tblDetails.Visible = true;
        }

        //'-------------------------UpdateMonth (UPDATE MONTH IN CALENDAR)---------------------------------'
        protected void UpdateMonth(object sender, MonthChangedEventArgs e)
        {
            tblDetails.Visible = true;
            trowCalendar.Visible = true;
        }

        //'-------------------------UpdateDate (INSERT SELECTED DATE IN FORM)---------------------------------'
        protected void UpdateDate(object sender, EventArgs e)
        {
            txtDate.Text = cldDate.SelectedDate.ToShortDateString();
            tblDetails.Visible = true;
            trowCalendar.Visible = false;
        }

        //'-------------------------btnReportTemp_Click(REDIRECT USER TO REPORTS PAGE)--------------------------'
        protected void btnReportTemp_Click(object sender, EventArgs e)
        {
            string strUrl = "ReportsTemp.aspx";
            Response.Redirect(strUrl);
        }

        //'-------------------------btnReportContract_Click(REDIRECT USER TO REPORTS PAGE)--------------------------'
        protected void btnReportContract_Click(object sender, EventArgs e)
        {
            string strURL = "ReportsContract.aspx";
            Response.Redirect(strURL);
        }

        //'-------------------------dgSearchResults_Data(HIDE NULL VALUES IN dgSearchResults)----------------'
        protected void dgSearchResults_Data(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
                if (strLocation == "Null")
                    e.Item.Cells[7].Text = "";

                if (strSupEmail == "Null")
                    e.Item.Cells[10].Text = "";
            }
        }


        //'-------------------------dgSearchRequest_Data(HIDE NULL VALUES IN dgSearchRequest)----------------'
        protected void dgSearchRequest_Data(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                strReq = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ReqNum"));
                strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
                if (strLocation == "Null")
                    e.Item.Cells[8].Text = "";

                if (strReq == "Null")
                    e.Item.Cells[9].Text = "";

                if (strSupEmail == "Null")
                    e.Item.Cells[12].Text = "";
            }
        }

        //'-------------------------dgNHTerm_Data(HIDE NULL VALUES IN dgNHTerm)-----------------------'
        protected void dgNHTerm_Data(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strRequestType = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RequestType"));
                strReq = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ReqNum"));
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                if (strRequestType == "New Hire")
                    e.Item.Cells[6].Text = "";
                else
                    e.Item.Cells[5].Text = "";

                if (strReq == "Null")
                    e.Item.Cells[9].Text = "";

                if (strLocation == "Null")
                    e.Item.Cells[10].Text = "";
            }
        }




        //'-------------------------dgTrans_Data(HIDE NULL VALUES IN dgTrans)--------------------------'
        protected void dgTrans_Data(object sender, DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                strReq = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ReqNum"));
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                if (strReq == null)
                {
                    e.Item.Cells[7].Text = "";
                }
                if (strLocation == "Null")
                {
                    e.Item.Cells[8].Text = "";
                }
            }
        }

        //'---------------------FixDate (Required to performed Oracle date operations)----'
        private void FixDate(object sender, EventArgs e)
        {
            string fixDate = "ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/YYYY'";
            cmd = new OleDbCommand(fixDate, oOraConn);
            cmd.ExecuteScalar();
        }

        //'-----------------------------btnClear_Click (CLEAR FORM CONTENTS)-----------------'
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtDate.Text = "";
            txtCC.Text = "0000000";
            txtJob.Text = "";
            ddlReturn.SelectedIndex = 0;
            txtLocation.Text = "";
            txtSupName.Text = "";
            txtSupEmail.Text = "";
            txtEmailComments.Text = "";
            txtNonEmployeeComments.Text = "";
            tblReturn.Visible = false;
            txtOtherComp.Text = "";
            ddlCompany.SelectedIndex = 0;
        }


        //'-----------------------------SendNotification(Send Notification Upon Form Submission)-----'
        private void SendNotification(int intReqID)
        {
            string strLocation;
            string strReturn;
            string strCompany = string.Empty;
            string strNonEmployeeComments;
            string strComments = "<br><b>Comments: </b>" + txtEmailComments.Text;
            string strCompanyID = "0";
            string dataGridHTML = string.Empty;

            //oOraConn.Open()'
            // if NOT Terminate
            if (ddlType.SelectedIndex != 3)
            {
                strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (pwdreset.NonEmployee.BadgeID = pwdreset.NonEmpRequest.BadgeID and pwdreset.NonEmpRequest.RequestID=" + intReqID.ToString() + " and pwdreset.Company.CompanyID = pwdreset.NonEmployee.CompanyID)";
                cmd = new OleDbCommand(strRead, oOraConn);
                rdrEmployee = cmd.ExecuteReader();
                while (rdrEmployee.Read())
                {
                    strCompany = rdrEmployee["CompanyName"].ToString();
                    strCompanyID = rdrEmployee["CompanyID"].ToString();
                    strLocation = rdrEmployee["Location"].ToString();
                    if (strLocation == "Null")
                        strLocation = "";

                    if (Convert.ToString(rdrEmployee["COMMENTS"]) == "Null")
                        strNonEmployeeComments = "";
                    else
                        strNonEmployeeComments = Convert.ToString(rdrEmployee["COMMENTS"]);


                    strReturn = rdrEmployee["EligibleRtn"].ToString();

                    strEmployee = rdrEmployee["LName"].ToString().Trim().ToUpper() + ", " + rdrEmployee["FName"].ToString().Trim().ToUpper();

                    dataGridHTML = "<tr>";
                    dataGridHTML += "<td><b>Request Type: </b></td><td>" + rdrEmployee["RequestType"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Employee Name: </b></td><td>" + strEmployee + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Badge ID: </b></td><td>" + rdrEmployee["BadgeID"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Effective Date: </b></td><td>" + String.Format("{0:d}", rdrEmployee["EFFECTIVEDATE"]) + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Cost Center: </b></td><td>" + rdrEmployee["CostCenter"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Job Title: </b></td><td>" + rdrEmployee["JobTitle"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Company: </b></td><td>" + strCompany + "</td>";
                    dataGridHTML += "<td><b>Location: </b></td><td>" + strLocation + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Supervisor: </b></td><td>" + rdrEmployee["SUPNAME"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Supervisor Email: </b></td><td>" + rdrEmployee["SUPEMAIL"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td valign='top'><b>Temp/Contractor Comments: </b></td><td colspan='3' valign='top'>" + strNonEmployeeComments.Replace("\r", "<br>") + "</td>";
                    dataGridHTML += "</tr>";
                    dataGridHTML += "</table>" + strComments;

                    strEmployee += " (" + String.Format("{0:d}", rdrEmployee["EFFECTIVEDATE"]) + ")";
                    strCCSupervisor = rdrEmployee["SupEmail"].ToString();
                    if (strCCSupervisor == "Null")
                        strCCSupervisor = "";

                }
                rdrEmployee.Close();
            }
            else
            {
                //        Else 'IF Terminate
                strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (pwdreset.NonEmployee.BadgeID = pwdreset.NonEmpRequest.BadgeID and pwdreset.NonEmpRequest.RequestID=" + intReqID.ToString() + " and pwdreset.Company.CompanyID = pwdreset.NonEmployee.CompanyID)";
                cmd = new OleDbCommand(strRead, oOraConn);
                rdrEmployee = cmd.ExecuteReader();
                while (rdrEmployee.Read())
                {
                    strCompany = rdrEmployee["CompanyName"].ToString();
                    strCompanyID = rdrEmployee["CompanyID"].ToString();
                    strLocation = rdrEmployee["Location"].ToString();
                    if (strLocation == "Null")
                        strLocation = "";

                    strReturn = rdrEmployee["EligibleRtn"].ToString();
                    if (strReturn == "Y")
                        strReturn = "Yes";
                    else
                        strReturn = "No";

                    if (rdrEmployee["COMMENTS"].ToString() == "Null")
                        strNonEmployeeComments = "";
                    else
                        strNonEmployeeComments = rdrEmployee["COMMENTS"].ToString();

                    strEmployee = rdrEmployee["LName"].ToString().Trim().ToUpper() + ", " + rdrEmployee["FName"].ToString().Trim().ToUpper();

                    dataGridHTML = "<tr>";
                    dataGridHTML += "<td><b>Request Type: </b></td><td>" + rdrEmployee["RequestType"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Employee Name: </b></td><td>" + strEmployee + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Badge ID: </b></td><td>" + rdrEmployee["BadgeID"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Effective Date: </b></td><td>" + String.Format("{0:d}", rdrEmployee["EFFECTIVEDATE"]) + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Cost Center: </b></td><td>" + rdrEmployee["CostCenter"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Job Title: </b></td><td>" + rdrEmployee["JobTitle"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Company: </b></td><td>" + strCompany + "</td>";
                    dataGridHTML += "<td><b>Location: </b></td><td>" + strLocation + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Supervisor: </b></td><td>" + rdrEmployee["SUPNAME"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Supervisor Email: </b></td><td>" + rdrEmployee["SUPEMAIL"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Eligible to Return:</b></td><td>" + strReturn + "</td>";
                    dataGridHTML += "<td>&nbsp;</td><td>&nbsp;</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td valign='top'><b>Temp/Contractor Comments: </b></td><td colspan='3' valign='top'>" + strNonEmployeeComments.Replace("\r", "<br>") + "</td>";
                    dataGridHTML += "</tr>";
                    dataGridHTML += "</table>" + strComments;

                    strEmployee += " (" + String.Format("{0:d}", rdrEmployee["EFFECTIVEDATE"]) + ")";
                    strCCSupervisor = rdrEmployee["SupEmail"].ToString();

                } //// end while
                rdrEmployee.Close();
            }////        End If	
            rdrEmployee.Close();
            //oOraConn.Close()'

            SendEmail(ddlType.SelectedValue, intReqID.ToString(), strEmployee, strCompany, strCompanyID, strCCSupervisor, dataGridHTML, lblUserID.Text);
        } //// end sub


        //'-----------------SendMail (Form and Send an Email--------------------------'
        //private void SendMail(string strTo, string strCc, string strBcc, string strFrom, string strSubject, string strBody)
        //{
        //    try
        //    {
        //        MailMessage objEmail = new MailMessage();

        //        objEmail.IsBodyHtml = true;
        //        objEmail.From = new MailAddress(strFrom);

        //        if (Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"]))
        //        {
        //            objEmail.To.Add(new MailAddress(strTo));
        //        }
        //        else
        //        {
        //            string strUser = ADUtility.GetUserNameOfAppUser(Request);
        //            ADUserEntity myUser = ADUtility.GetADUserByUserName(strUser);
        //            objEmail.To.Add(new MailAddress(myUser.Email));
        //            Response.Write("<br>To: " + strTo + "; Subject: " + strSubject);
        //        }

        //        objEmail.Subject = strSubject;
        //        if (strCc != "")
        //        {
        //            objEmail.CC.Add(new MailAddress(strCc));
        //        }

        //        if (strBcc != "")
        //        {
        //            objEmail.Bcc.Add(new MailAddress(strBcc));
        //        }

        //        objEmail.Body = strBody;

        //        if (Convert.ToBoolean(ConfigurationManager.AppSettings["sendNotification"]))
        //        {
        //            SmtpClient client = new SmtpClient();
        //            client.Send(objEmail);
        //        }
        //        else
        //        {
        //            Response.Write("<br>To: " + strTo + "; Subject: " + strSubject);
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        OpenLogFile(exc);
        //        try
        //        {
        //            MailMessage objEmail = new MailMessage();

        //            objEmail.IsBodyHtml = true;
        //            objEmail.From = new MailAddress(ConfigurationManager.AppSettings["StaffEmail"]);
        //            objEmail.To.Add(new MailAddress(ConfigurationManager.AppSettings["ErrorEmail"]));
        //            objEmail.Subject = "Temp/Contractor Error";
        //            objEmail.Body = "<table><tr><td>" + exc.Message + "</td></tr></table><br><br>" + strBody;

        //            if (Convert.ToBoolean(ConfigurationManager.AppSettings["sendNotification"]))
        //            {
        //                SmtpClient client = new SmtpClient();
        //                client.Send(objEmail);
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            OpenLogFile(ex);
        //        }
        //    }
        //}

        //private void OpenLogFile(Exception ex)
        //{
        //    try
        //    {
        //        string strLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
        //        FileInfo fileLog = new FileInfo(strLogFile);
        //        System.IO.StreamWriter str = fileLog.AppendText();

        //        string strTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": ";
        //        str.WriteLine(strTime + "SOURCE: " + Convert.ToString(ex.Source));
        //        str.WriteLine("MESSAGE: " + Convert.ToString(ex.Message));
        //        str.WriteLine("STACKTRACE: " + Convert.ToString(ex.StackTrace));

        //        str.Flush();
        //        // close log file
        //        str.Close();

        //    }
        //    catch (Exception exc)
        //    {
        //        Debug.WriteLine("OpenLogFile Error: " + exc.Message);
        //    }
        //}


        //'-----------------SendEmail (Form and Send an Email--------------------------'
        private void SendEmail(string strRequestType, string intReqID, string strEmployee, string strCompany, string strCompanyID, string strCCSupervisor, string strContents, string strHRRep)
        {
            MailMessage myMail = new MailMessage();

            //Sub SendEmail(strRequestType As String, intReqID As String, strEmployee As String, strCompany As String, strCompanyID As String, strCCSupervisor As String, strContents As String, strHRRep As String)	
            //    Dim myMail, cdoMessage
            //    Dim strEmailSubject, 
            string strToEmail;
            string strBCCEmail = string.Empty;
            string strTermNote;
            string strFromEmail;
            string strEmailBody;
            string strEmailMgrSubject;
            string strSiteURL;
            string strPeoplemarkEmail = ConfigurationManager.AppSettings["TempAgencyEmail"] + "; ";

            string strEmailSubject = strCompany + "- Temp/Contractor " + strRequestType + " Notification: " + strEmployee;

            string strHeader = "<font face='Smith&NephewLF, Arial, sans-serif' size='-1'><b>Temporary Worker/Contractor Notification Submitted By:</b> " + strHRRep + "</font>";
            strContents = "<br><table border='1' style='border-color:#999999;empty-cells:show;font-size:14px;font-family:Smith&NephewLF, Arial, sans-serif' cellpadding='5' cellspacing='0'>" + strContents;
            string strPeoplemarkTerm = "";

            //If Company is Temp Agency'
            if (IsTempAgency(strCompanyID))
            {
                strCCSupervisor = strPeoplemarkEmail + strCCSupervisor;
                strPeoplemarkTerm = ConfigurationManager.AppSettings["TempAgencyEmail"];
            }

            // If Request Type = Terminate'
            if (strRequestType == "Terminate")
            {
                //        'Send Email To Dist. List'
                strToEmail = ConfigurationManager.AppSettings["HrEmail"];
                strFromEmail = ConfigurationManager.AppSettings["StaffEmail"];
                strEmailBody = strHeader + strContents;
                //SendMail(strToEmail, strPeoplemarkTerm, strBCCEmail, strFromEmail, strEmailSubject, strEmailBody);
                Utility.SendEmail(strToEmail, strFromEmail, strPeoplemarkTerm, strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));

                //Send Email to Manager'
                strTermNote = "<font face='Smith&NephewLF, Arial, sans-serif' size='-1'>The following termination request has been processed:<br><br>" + strContents + "</font></table>";
                strTermNote += "<br><font color='#DB6415' size='3'><u><b>As a supervisor, you are responsible for returning temporary worker/contractor's equipment and/or tools <b>within 30 days </b> to avoid replacement equipment charges to your cost center.</u></b></font>";
                strTermNote += "<br><br><strong>Please use the list below to complete the termination process:</strong>";
                strTermNote += "<ol><li>Return <strong>Hardware, Office Phone, FOB</strong> to <strong>eIS Desktop Support</strong> (Remote employees must mail their equipment to eIS Desktop Support)</li>";
                strTermNote += "<li>Return <strong>ID Badge and/or Keys</strong> to <strong>Security</strong></li>";
                strTermNote += "<li>Return <strong>Corporate Credit Card</strong> to <strong>Michelle Smith</strong></li>";
                strTermNote += "</ol></font>";

                strEmailBody = strTermNote;
                strEmailMgrSubject = "Termination Procedures for " + strEmployee;
               // SendMail(strCCSupervisor, "", strBCCEmail, strFromEmail, strEmailMgrSubject, strEmailBody);
                Utility.SendEmail(strCCSupervisor, strFromEmail, "", strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));
            }
            else
            {
                //    'All Transfer or New Hire/Revision/Transfer for TempAgency - No Approval'
                //    ELSEIF ((strRequestType = "Transfer") or (IsTempAgency(strCompanyID))) THEN	
                if ((strRequestType == "Transfer") || (IsTempAgency(strCompanyID)))
                {

                    strToEmail = ConfigurationManager.AppSettings["HrEmail"];
                    strFromEmail = ConfigurationManager.AppSettings["StaffEmail"];
                    strEmailBody = strHeader + strContents;
                   // SendMail(strToEmail, strCCSupervisor, strBCCEmail, strFromEmail, strEmailSubject, strEmailBody);
                    Utility.SendEmail(strToEmail, strFromEmail, strCCSupervisor, strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));

                    //Non-Peoplemark New Hire/Revision - Then Request Approval'
                }
                else
                {

                    strToEmail = ConfigurationManager.AppSettings["ContractApprovalEmail"];
                    strFromEmail = ConfigurationManager.AppSettings["NewContractEmail"];
                    strSiteURL = ConfigurationManager.AppSettings["SiteURL"];
                    strEmailBody = strHeader + strContents + "<br><a href='" + strSiteURL + "/ApproveForm.aspx?ID=" + intReqID + "'><b>Approve Request</b></a>";
               //     SendMail(strToEmail, strCCSupervisor, strBCCEmail, strFromEmail, strEmailSubject, strEmailBody);
                    Utility.SendEmail(strToEmail, strFromEmail, strCCSupervisor, strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));

                    //    'If Request Type = Termination'	
                }

                //Show Message on the form'
                lblSearchResult.Text = "Your notification has been sent to the distribution list.";
                lblSearchResult.Visible = true;
                //tcellSearchResult.Visible = true;
                lblSearchResult.Visible = true;
            }
        }

        private void ShowError(object sender, EventArgs e)
        {
            lblSearchResult.Text = "No records found. Please try again.";
            lblSearchResult.Visible = true;
            tblDetails.Visible = false;
        }

        private bool IsTempAgency(string strCompanyID)
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["TempAgencyID"] == strCompanyID);
        }

        private bool IsOtherCompany(string strCompanyID)
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["OtherCompanyID"] == strCompanyID);
        }

        protected void ddlCompany_Change(object sender, EventArgs e)
        {
            if (IsOtherCompany(this.ddlCompany.SelectedValue))
                tblOtherCompany.Visible = true;
            else
                tblOtherCompany.Visible = false;

        }

    }//// end class
}