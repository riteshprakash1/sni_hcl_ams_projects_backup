﻿<%@ Page Title="Temporary Worker Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportsTemp.aspx.cs" Inherits="NonEmpNotification.ReportsTemp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<table ID="Table1" style="width:100%">
		<tr>
			<td style="width:1%">&nbsp;</td>
			<td>
					<br/>
					<table ID="Table2" style="width:100%" runat="server" BorderWidth="1" BackColor="#CCCCCC" BorderStyle="Inset">
						<tr>
							<td colspan="4" style="text-align:Center">
								<strong>TEMPORARY WORKER REPORT MANAGER</strong><hr>								
							</td>
						</tr>
						<tr style="text-align:left">
							<td colspan="4"><strong>Report Type:</strong>
								<br/><asp:RadioButton id="radStatus" Checked="true" AutoPostBack="true" OnCheckedChanged="radStatus_Click" Text="Temporary Worker Termination Information" runat="server"/>
								<br/><asp:RadioButton id="radOnSite" AutoPostBack="true" OnCheckedChanged="radOnSite_Click" Text="Currently On-Site Temporary Workers" runat="server"/>
								<br/><asp:RadioButton id="radPeoplemark" AutoPostBack="true" OnCheckedChanged="radPeoplemark_Click" Text="Currently On-Site and Recently Terminated Temporary Workers" runat="server"/>
								<br/><asp:RadioButton id="radNewHire" AutoPostBack="true" OnCheckedChanged="radNewHire_Click" Text="New Temporary Worker Notifications" runat="server"/>
								<br/><asp:RadioButton id="radTransfer" AutoPostBack="true" OnCheckedChanged="radTransfer_Click" Text="Temporary Worker Transfer Notifications" runat="server"/>
								<br/><asp:RadioButton id="radTerm" AutoPostBack="true" Text="Temporary Worker Termination Notifications" OnCheckedChanged="radTerm_Click" Checked="true" runat="server"/>
								<br/><br/>
							</td>
						</tr>						
						<tr>
						<td colspan="4">
							<table ID="tblFindEmpNum" style="width:100%" runat="server">								
								<tr>
									<td style="width:20%">
										<asp:label ForeColor="#E64A00" Font-Bold="true" id="lblSearchResult" runat="server"/>										
										<br/><b>Enter Search Criteria:</b>
										Badge ID: 
										<asp:TextBox id="txtBadge" runat="server"/>&nbsp;
										<strong>OR </strong>
										Last Name: 
										<asp:TextBox id="txtSearchLName" runat="server"/>&nbsp;
										First Name: <asp:TextBox id="txtSearchFName" runat="server"/>		
										<br/>
										<asp:Button id="btnFindEmpNum" OnClick="btnFindEmpNum_Click" Text="Search for Termination Notification" runat="server"/>					
									</td>	
							  	</tr>
								<tr id="trowSearchResult" runat="server">
									<td colspan="2">
										<asp:DataGrid id="dgSearchResults" OnItemDataBound="dgSearchResults_Data" autogeneratecolumns="False" Font-Size="10" HeaderStyle-Font-Bold="true" runat="server">
											<columns>
												<asp:TemplateColumn HeaderText="Badge ID">
													<ItemTemplate>
														<asp:Label ID="BADGEID" Text='<%# DataBinder.Eval(Container.DataItem,"BADGEID") %>' runat="server" />
													</ItemTemplate>
												</asp:TemplateColumn>										
													<asp:BoundColumn HeaderText="Request Date" DataField="RequestDate" DataFormatString="{0:d}"/>	
													<asp:BoundColumn HeaderText="Last Name" DataField="LNAME"/>	
													<asp:BoundColumn HeaderText="First Name" DataField="FNAME"/>													
													<asp:BoundColumn HeaderText="Termination Date" DataField="EFFECTIVEDATE" DataFormatString="{0:d}"/>	
													<asp:BoundColumn HeaderText="Job Title" DataField="JOBTITLE"/>	
													<asp:BoundColumn HeaderText="Company" DataField="CompanyName"/>	
													<asp:BoundColumn HeaderText="Location" DataField="LOCATION"/>	
													<asp:BoundColumn HeaderText="Cost Center" DataField="COSTCENTER"/>
													<asp:BoundColumn HeaderText="Supervisor Name" DataField="SUPNAME"/>	
													<asp:BoundColumn HeaderText="Supervisor Email" DataField="SUPEMAIL"/>	
													<asp:BoundColumn HeaderText="Return Eligibility" DataField="EligibleRtn"/>	
												</columns>
											</asp:DataGrid>
										</td>
									</tr>
						  </table>
					  </td>
				  </tr>				  
				  <tr>
						<td colspan="4">
							<table ID="tblPeoplemark" Width="100%" runat="server">								
								<tr>
									<td Width="20%">
										<asp:label ForeColor="#E64A00" Font-Bold="true" id="lblSearchResult2" runat="server"/>										
										<br/><b>Temporary Workers: Currently On-site and Terminated within</b>
										<asp:DropDownList id="ddlTime" runat="server">
											<asp:ListItem Value="1" Text="Last Week"/>
											<asp:ListItem Value="2" Text="2 Weeks Ago"/>
											<asp:ListItem Value="3" Text="3 Weeks Ago"/>
										</asp:DropDownList>										
									</td>	
							  	</tr>
							</table>
				 		</td>
					</tr> 
				  <tr style="vertical-align:top">
							<td ID="Tablecell1" runat="server">								
								<table id="tblDate" runat="server">
									<tr>
										<td>Requests from </td>
										<td><asp:TextBox id="txtFrom" Text="mm/dd/yyyy" Width="80" runat="server"/></td>
										<td><asp:ImageButton ID="btnFromCalendar" OnClick="btnFromCalendar_Click" ImageUrl="images/calendar.gif" ToolTip="View Calendar" runat="server"/></td>
										<td>To <asp:TextBox id="txtTo" Text="mm/dd/yyyy" Width="80" runat="server"/></td>
										<td><asp:ImageButton ID="btnToCalendar" OnClick="btnToCalendar_Click" ImageUrl="images/calendar.gif" ToolTip="View Calendar" runat="server"/></td>
										<td></td>
									</tr>
									<tr id="trowCalendar" runat="server">
										<td></td>
										<td colspan="3"><asp:Calendar id="cldFromDate" OnVisibleMonthChanged="UpdateMonth" OnSelectionChanged="UpdateFromDate" DayHeaderStyle-BackColor="#999999" TodayDayStyle-BackColor="#FF9933" ToolTip="Click on the date to close the calendar" runat="server"/></td>
										<td colspan="2"><asp:Calendar id="cldToDate" OnVisibleMonthChanged="UpdateMonth" OnSelectionChanged="UpdateToDate" DayHeaderStyle-BackColor="#999999" TodayDayStyle-BackColor="#FF9933" ToolTip="Click on the date to close the calendar" runat="server"/></td>
									</tr>
								</table>								 
							</td>
							<td>	
							</td>														
						</tr>
						<tr>
							<td colspan="4" >
								<asp:button id="btnSubmit" OnClick="btnSubmit_Click" Text="Run Report" runat="server"/>
								<asp:button id="btnExport" OnClick="btnExport_Click" Text="View Report in Excel" runat="server"/>
								<asp:button id="btnReturn" OnClick="btnReturn_Click" Text="Return to Notification Form" runat="server"/>
								<asp:button id="btnReportContract" Text="Run Reports on Contractors" OnClick="btnReportContract_Click" runat="server"/>
							</td>
						</tr>
						<tr>
							<td colspan="4" ID="tcellPeoplemarkResults" runat="server">
								<hr>
								<asp:DataGrid id="dgPeoplemarkReport" OnItemDataBound="dgPeoplemarkReport_Data" Font-Size="10" HeaderStyle-Font-Bold="true" autogeneratecolumns="False" runat="server">							
									<columns>
										<asp:BoundColumn HeaderText="Active" DataField="Active"/>	
										<asp:BoundColumn HeaderText="Badge ID " DataField="BadgeID"/>	
										<asp:BoundColumn HeaderText="Last Name" DataField="LName"/>	
										<asp:BoundColumn HeaderText="First Name" DataField="FNAME"/>	
										<asp:BoundColumn HeaderText="Effective Date <br/>(Active Worker-Start Date<br/>Inactive Worker-Term Date)" DataField="EFFECTIVEDATE" DataFormatString="{0:d}"/>	
										<asp:BoundColumn HeaderText="Division" DataField="DIVISION"/>	
										<asp:BoundColumn HeaderText="Job Title" DataField="JOBTITLE"/>	
										<asp:BoundColumn HeaderText="Location" DataField="LOCATION"/>	
										<asp:BoundColumn HeaderText="Cost Center" DataField="COSTCENTER"/>
										<asp:BoundColumn HeaderText="Supervisor Name" DataField="SUPNAME"/>	
										<asp:BoundColumn HeaderText="Supervisor Email" DataField="SUPEMAIL"/>	
										<asp:BoundColumn HeaderText="Submitted By:" DataField="Rep"/>	
									</columns>
								</asp:DataGrid>
								<br/>Total Number of Records: <asp:label id="lblPeoplemarkCount" runat="server"/>
							</td>
						</tr>
						<tr>
							<td colspan="4" ID="tcellResults" runat="server">
								<hr>
								<asp:DataGrid id="dgReport" OnItemDataBound="dgReport_Data" Font-Size="10" HeaderStyle-Font-Bold="true" autogeneratecolumns="False" runat="server">							
									<columns>
										<asp:BoundColumn HeaderText="Request Date" DataField="REQUESTDATE" DataFormatString="{0:d}"/>	
										<asp:BoundColumn HeaderText="Badge ID " DataField="BadgeID"/>	
										<asp:BoundColumn HeaderText="Last Name" DataField="LName"/>	
										<asp:BoundColumn HeaderText="First Name" DataField="FNAME"/>	
										<asp:BoundColumn HeaderText="Effective Date" DataField="EFFECTIVEDATE" DataFormatString="{0:d}"/>	
										<asp:BoundColumn HeaderText="Division" DataField="DIVISION"/>	
										<asp:BoundColumn HeaderText="Job Title" DataField="JOBTITLE"/>	
										<asp:BoundColumn HeaderText="Company" DataField="CompanyName"/>	
										<asp:BoundColumn HeaderText="Location" DataField="LOCATION"/>	
										<asp:BoundColumn HeaderText="Cost Center" DataField="COSTCENTER"/>
										<asp:BoundColumn HeaderText="Supervisor Name" DataField="SUPNAME"/>	
										<asp:BoundColumn HeaderText="Supervisor Email" DataField="SUPEMAIL"/>	
										<asp:BoundColumn HeaderText="Submitted By:" DataField="Rep"/>	
									</columns>
								</asp:DataGrid>
								<br/>Total Number of Records: <asp:label id="lblCount" runat="server"/>
							</td>
						</tr>
						<tr>
							<td colspan="4" ID="tcellOnSiteResults" runat="server">
								<hr/>
								<asp:DataGrid id="dgReportOnSite" OnItemDataBound="dgReportOnSite_Data" Font-Size="10" HeaderStyle-Font-Bold="true" autogeneratecolumns="False" runat="server">							
									<columns>
										<asp:BoundColumn HeaderText="Badge ID " DataField="BadgeID"/>	
										<asp:BoundColumn HeaderText="Last Name" DataField="LName"/>	
										<asp:BoundColumn HeaderText="First Name" DataField="FNAME"/>	
										<asp:BoundColumn HeaderText="Start Date" DataField="STARTDATE" DataFormatString="{0:d}"/>	
										<asp:BoundColumn HeaderText="Division Number" DataField="DIVISION"/>	
										<asp:BoundColumn HeaderText="Job Title" DataField="JOBTITLE"/>	
										<asp:BoundColumn HeaderText="Company" DataField="CompanyName"/>	
										<asp:BoundColumn HeaderText="Location" DataField="LOCATION"/>	
										<asp:BoundColumn HeaderText="Cost Center" DataField="COSTCENTER"/>
										<asp:BoundColumn HeaderText="Supervisor Name" DataField="SUPNAME"/>	
										<asp:BoundColumn HeaderText="Supervisor Email" DataField="SUPEMAIL"/>	
									</columns>
								</asp:DataGrid>
								<br/>Total Number of Records: <asp:label id="lblCountOnSite" runat="server"/>
							</td>
						</tr>
					</table>
				</td>
			<td style="width:1%">&nbsp;</td>
		</tr>	
	</table>


</asp:Content>
