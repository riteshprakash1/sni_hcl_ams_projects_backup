﻿// ------------------------------------------------------------------
// <copyright file="ReportsContract.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace NonEmpNotification
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmpNotification.Classes;

    using System.Data.OleDb;
    using System.Data.Common;
    using System.IO;
    using System.Net.Mail;
    using System.Text;

    /// <summary>This is the ReportsContract page.</summary> 
    public partial class ReportsContract : System.Web.UI.Page
    {
        OleDbConnection oOraConn = new System.Data.OleDb.OleDbConnection(ConfigurationManager.ConnectionStrings["OracleConnString"].ToString());
        OleDbCommand cmd = null;
        OleDbDataReader rdrEmployee = null;
        OleDbDataReader rdrReport = null;
        string strRead;
         string strTempAgencyID = ConfigurationManager.AppSettings["TempAgencyID"];

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            bool blnReportingEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["ReportingEnabled"]);
            if (!blnReportingEnabled)
                Response.Redirect("Default.aspx", true);

	        if (!Page.IsPostBack) {
		        radTerm.Checked = true;
		        tcellResults.Visible = false;
		        tcellOnSiteResults.Visible = false;
		        trowCalendar.Visible = false;
		        btnExport.Visible = false;

		        radStatus.Checked = true;
		        radStatus_Click(sender, e);

		        oOraConn.Open();
                OleDbDataAdapter rdaCompany;
		        DataSet rdsCompany;
		        //Exclude Other and Temp Agency from selection'
                rdaCompany = new OleDbDataAdapter("select * from pwdreset.Company where (CompanyID<>" + ConfigurationManager.AppSettings["OtherCompanyID"] + " and CompanyID <> " + strTempAgencyID + ") order by CompanyName", oOraConn);
		        rdsCompany = new DataSet();
		        rdaCompany.Fill(rdsCompany, "Company");
		        ddlDivision.DataSource = rdsCompany;
		        ddlDivision.DataTextField = rdsCompany.Tables[0].Columns["CompanyName"].ColumnName.ToString();
		        ddlDivision.DataValueField = rdsCompany.Tables[0].Columns["CompanyId"].ColumnName.ToString();
		        ddlDivision.DataBind();
		        ddlDivision.Items.Insert(0, "All Companies");
		        ddlDivision.SelectedIndex = 0;
		        oOraConn.Close();


                Site masterPage = this.Master as Site;
                System.Web.UI.HtmlControls.HtmlGenericControl shell = masterPage.Shell;
                shell.Style["width"] = "1200px";

	        }
        }
        //------------------------btnFindEmpNum_Click (SEARCH FOR EMP.# BY First Name, Last Name)---------'
        protected void btnFindEmpNum_Click(object Sender, EventArgs E)
        {
            object objEmpCount;
            string strBadge;
            int intEmpCount;
	        strBadge = txtBadge.Text;

	        string strLName, strFName;
	        strLName = txtSearchLName.Text.ToUpper();
	        strFName = txtSearchFName.Text.ToUpper();
	        strFName = "%" + strFName + "%";

	        //Check for at least 1 Search Criteria'
            if (((strBadge != string.Empty) | (strLName != string.Empty)))
            {
		        oOraConn.Open();
		        //If Searching by Badge Number'
		        if ((strBadge != "")) {
			        //Make sure there are matching employee records'
			        strRead = "select * from pwdreset.NonEmployee where nonEmployee.BADGEID =" + strBadge;
                    cmd = new OleDbCommand(strRead, oOraConn);
			        objEmpCount = cmd.ExecuteScalar();
                    intEmpCount = Utility.GetInt(objEmpCount);

			        if ((intEmpCount > 0)) {
				        strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (nonEmployee.BADGEID =" + strBadge + " and nonEmpRequest.RequestType='Terminate' and nonEmpRequest.BadgeID=NonEmployee.BadgeID and Company.CompanyID = NonEmployee.CompanyID)";
                        cmd = new OleDbCommand(strRead, oOraConn);
				        rdrEmployee = cmd.ExecuteReader();
				        //If Records Found by Badge'
				        if ((rdrEmployee.HasRows)) {
					        dgSearchResults.DataSource = rdrEmployee;
					        dgSearchResults.DataBind();
					        trowSearchResult.Visible = true;
					        dgSearchResults.Visible = true;
					        lblSearchResult.Visible = false;
					        rdrEmployee.Close();
				        } else {
					        lblSearchResult.Text = "No Termination Records Found for this Temporary Employe/Contractor. Please Try Searching by Last Name, First Name.";
					        lblSearchResult.Visible = true;
					        dgSearchResults.Visible = false;
				        }
			        //No Emp Records'
			        } else {
				        lblSearchResult.Text = "No Temporary Employee/Contractor Information Found. Please Try Searching by Last Name, First Name.";
				        lblSearchResult.Visible = true;
				        dgSearchResults.Visible = false;
			        }
		        //Search by First Name, Last Name'
		        } else {
			        //Make sure there are matching employee records'
			        strRead = "select * from pwdreset.NonEmployee where nonEmployee.LName = :LNAME and nonEmployee.FNAME LIKE :FNAME";
                    cmd = new OleDbCommand(strRead, oOraConn);
			        cmd.Parameters.Add(new OleDbParameter("LNAME", strLName));
			        cmd.Parameters.Add(new OleDbParameter("FNAME", strFName));

                    objEmpCount = cmd.ExecuteScalar();
                    intEmpCount = Utility.GetInt(objEmpCount);

			        if ((intEmpCount > 0)) {
				        //Search for Termination Records'
				        strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (nonEmployee.LName = :LNAME and nonEmployee.FNAME LIKE :FNAME and nonEmpRequest.RequestType='Terminate' and nonEmpRequest.BadgeID=NonEmployee.BadgeID and Company.CompanyID = NonEmployee.CompanyID)";
                        cmd = new OleDbCommand(strRead, oOraConn);
				        cmd.Parameters.Add(new OleDbParameter("LNAME", strLName));
				        cmd.Parameters.Add(new OleDbParameter("FNAME", strFName));
				        rdrEmployee = cmd.ExecuteReader();
				        //Records Found by FirstName, Last Name'
				        if ((rdrEmployee.HasRows)) {
					        dgSearchResults.DataSource = rdrEmployee;
					        dgSearchResults.DataBind();
					        trowSearchResult.Visible = true;
					        dgSearchResults.Visible = true;
					        lblSearchResult.Visible = false;
					        rdrEmployee.Close();
				        //No Records Found'
				        } else {
					        lblSearchResult.Text = "No Termination Records Found. Please Try Again.";
					        lblSearchResult.Visible = true;
					        dgSearchResults.Visible = false;
				        }

			        //No Emp Records Found'
			        } else {
				        lblSearchResult.Text = "No Temporary Employee/Contractor Information Found. Please Try Searching by Last Name, First Name.";
				        lblSearchResult.Visible = true;
				        dgSearchResults.Visible = false;
			        }
		        }

		        oOraConn.Close();
	        } else {
		        //Display Search Criteria'
		        lblSearchResult.Text = "Please enter at least one search criteria and try again.";
		        lblSearchResult.Visible = true;
		        dgSearchResults.Visible = false;
	        }

        //tblDetails.Visible = "false"	'

        //Clear Search Fields'
	        txtSearchFName.Text = "";
	        txtSearchLName.Text = "";
	        //txtSearch.Text = ""'
        }
        //-------------------------dgSearchResults_Data(HIDE NULL VALUES IN dgSearchResults)----------------'
        protected void dgSearchResults_Data(object Sender, DataGridItemEventArgs e)
        {
	        string strLocation, strSupEmail;
	        if ((e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)) {
		        strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
		        strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
		        if ((strLocation == "Null")) {
			        e.Item.Cells[7].Text = "";
		        }
		        if ((strSupEmail == "Null")) {
			        e.Item.Cells[10].Text = "";
		        }
	        }
        }
        //-------------------------dgReport_Data(HIDE NULL VALUES IN dgReport)----------------'
        protected void dgReport_Data(object Sender, DataGridItemEventArgs e)
        {
	        string strLocation, strSupEmail;
	        if ((e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)) {
		        strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
		        strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
		        if ((strLocation == "Null")) {
			        e.Item.Cells[8].Text = "";
		        }
		        if ((strSupEmail == "Null")) {
			        e.Item.Cells[11].Text = "";
		        }
	        }
        }
        //-------------------------dgReportOnSite_Data(HIDE NULL VALUES IN dgReport)----------------'
        protected void dgReportOnSite_Data(object Sender, DataGridItemEventArgs e)
        {
	        string strLocation, strSupEmail;
	        if ((e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)) {
		        strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
		        strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
		        if ((strLocation == "Null")) {
			        e.Item.Cells[7].Text = "";
		        }
		        if ((strSupEmail == "Null")) {
			        e.Item.Cells[10].Text = "";
		        }
	        }
        }
        //--------------------------btnExport_Click()----------------------------------------------'
        protected void btnExport_Click(object Sender, EventArgs E)
        {
	        Response.Clear();
	        Response.Buffer = true;
	        Response.ContentType = "application/vnd.ms-excel";
	        Response.Charset = "";

	        StringBuilder SB = new StringBuilder();
	        StringWriter SW = new StringWriter(SB);
	        HtmlTextWriter htmlTW = new HtmlTextWriter(SW);

	        if ((dgReport.Visible == true)) {
		        dgReport.RenderControl(htmlTW);
            }
            else if ((dgReportOnSite.Visible == true))
            {
		        dgReportOnSite.RenderControl(htmlTW);
	        }
	        Response.Write(SW.ToString());
	        Response.End();
        }
        //--------------------------NewHireReport()-----SEARCH BY REQUEST DATE---------------------'
        protected void NewHireReport(object Sender, EventArgs E)
        {
	        tcellResults.Visible = true;
	        tcellOnSiteResults.Visible = false;

	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select * from pwdreset.nonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.Approve<>'N' and pwdreset.NonEmpRequest.RequestDate BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Add' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID and pwdreset.NonEmployee.CompanyID =" + ddlDivision.SelectedValue + "ORDER BY REQUESTID";
	        } else {
		        strRead = "select * from pwdreset.nonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.Approve<>'N' and pwdreset.NonEmpRequest.RequestDate BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Add' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        }

            cmd = new OleDbCommand(strRead, oOraConn);
	        rdrReport = cmd.ExecuteReader();
	        dgReport.DataSource = rdrReport;
	        dgReport.DataBind();
	        rdrReport.Close();

        //Count the number of displayed records'
	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select count(*) from pwdreset.nonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.RequestDate BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Add' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID and pwdreset.NonEmployee.CompanyID =" + ddlDivision.SelectedValue + "ORDER BY REQUESTID";
	        } else {
		        strRead = "select count(*) from pwdreset.nonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.Approve<>'N' and pwdreset.NonEmpRequest.RequestDate BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Add' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        }
            cmd = new OleDbCommand(strRead, oOraConn);
	        lblCount.Text = Convert.ToString(cmd.ExecuteScalar());
        }
        //--------------------------TransferReport()-------SEARCH BY REQUEST DATE-------------------------------'
        protected void TransferReport(object Sender, EventArgs E)
        {
	        tcellResults.Visible = true;
	        tcellOnSiteResults.Visible = false;

	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select * from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.RequestDate BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Transfer' and NonEmployee.CompanyID = " + ddlDivision.SelectedValue + "and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID ORDER BY REQUESTID";
	        } else if ((ddlDivision.SelectedIndex == 0)) {
		        strRead = "select * from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.RequestDate Between '" + txtFrom.Text + "' and '" + txtTo.Text + "' and REQUESTTYPE = 'Transfer' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        }

            cmd = new OleDbCommand(strRead, oOraConn);
	        rdrReport = cmd.ExecuteReader();
	        dgReport.DataSource = rdrReport;
	        dgReport.DataBind();
	        rdrReport.Close();

        //Count the number of displayed records'
	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select count(*) from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.RequestDate BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Transfer' and NonEmployee.CompanyID = " + ddlDivision.SelectedValue + "and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID ORDER BY REQUESTID";
	        } else {
		        strRead = "select count(*) from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.RequestDate Between '" + txtFrom.Text + "' and '" + txtTo.Text + "' and REQUESTTYPE = 'Transfer' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        }
            cmd = new OleDbCommand(strRead, oOraConn);
	        //lblCount.Text = Convert.ToString(cmd.ExecuteScalar())'
        }
        //--------------------------TermReport()----------------------------------------------'
        protected void TermReport(object Sender, EventArgs E)
        {
	        tcellResults.Visible = true;
	        tcellOnSiteResults.Visible = false;

	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select * from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.EFFECTIVEDATE BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Terminate' and pwdreset.NonEmployee.CompanyID= '" + ddlDivision.SelectedValue + "' and pwdreset.NOnEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID  and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        } else {
		        strRead = "select * from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.EFFECTIVEDATE Between '" + txtFrom.Text + "' and '" + txtTo.Text + "' and REQUESTTYPE = 'Terminate' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID  and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        }

            cmd = new OleDbCommand(strRead, oOraConn);
	        rdrReport = cmd.ExecuteReader();
	        dgReport.DataSource = rdrReport;
	        dgReport.DataBind();
	        rdrReport.Close();

        //Count the number of displayed records'
	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select count(*) from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.EFFECTIVEDATE BETWEEN '" + txtFrom.Text + "' and '" + txtTo.Text + "' and pwdreset.NonEmpRequest.REQUESTTYPE = 'Terminate' and pwdreset.NonEmployee.CompanyID= '" + ddlDivision.SelectedValue + "' and pwdreset.NOnEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID<>" + strTempAgencyID + " ORDER BY REQUESTID";
	        } else {
		        strRead = "select count(*) from pwdreset.NonEmpRequest, pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmpRequest.EFFECTIVEDATE Between '" + txtFrom.Text + "' and '" + txtTo.Text + "' and REQUESTTYPE = 'Terminate' and pwdreset.NonEmpRequest.BadgeID = pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID=pwdreset.Company.CompanyID ORDER BY REQUESTID";
	        }
            cmd = new OleDbCommand(strRead, oOraConn);
	        lblCount.Text = Convert.ToString(cmd.ExecuteScalar());

        }
        //--------------------------OnSiteReport()----------------------------------------------'
        protected void OnSiteReport(object Sender, EventArgs E)
        {
	        tcellOnSiteResults.Visible = true;
	        tcellResults.Visible = false;

	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select * from pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmployee.Active = 'Y' and pwdreset.NonEmployee.CompanyID= '" + ddlDivision.SelectedValue + "' and pwdreset.NonEmployee.CompanyID= pwdreset.Company.CompanyID ORDER BY pwdreset.Company.CompanyName, pwdreset.NonEmployee.BadgeID";
	        } else {
		        strRead = "select * from pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmployee.Active = 'Y' and pwdreset.NonEmployee.CompanyID= pwdreset.Company.CompanyID ORDER BY pwdreset.Company.CompanyName, pwdreset.NonEmployee.BadgeID";
	        }

            cmd = new OleDbCommand(strRead, oOraConn);
	        rdrReport = cmd.ExecuteReader();
	        dgReportOnSite.DataSource = rdrReport;
	        dgReportOnSite.DataBind();
	        rdrReport.Close();

        //Count the number of displayed records'
	        if ((ddlDivision.SelectedIndex != 0)) {
		        strRead = "select count(*) from pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmployee.Active = 'Y' and pwdreset.NonEmployee.CompanyID= '" + ddlDivision.SelectedValue + "' and pwdreset.NonEmployee.CompanyID= pwdreset.Company.CompanyID ORDER BY pwdreset.NonEmployee.BadgeID";
	        } else {
		        strRead = "select count(*) from pwdreset.NonEmployee, pwdreset.Company where pwdreset.NonEmployee.Active = 'Y' and pwdreset.NonEmployee.CompanyID= pwdreset.Company.CompanyID ORDER BY pwdreset.NonEmployee.BadgeID";
	        }
            cmd = new OleDbCommand(strRead, oOraConn);
	        lblCountOnSite.Text = Convert.ToString(cmd.ExecuteScalar());
        }
        //--------------------------btnSubmit_Click()----------------------------------------------'
        protected void btnSubmit_Click(object Sender, EventArgs E)
        {
	        oOraConn.Open();

        //Set Date Format'	
	        string fixDate;
	        fixDate = "ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/YYYY'";
            cmd = new OleDbCommand(fixDate, oOraConn);
	        cmd.ExecuteScalar();

	        if ((radNewHire.Checked)) {
		        NewHireReport(Sender, E);
	        } else if ((radTransfer.Checked)) {
		        TransferReport(Sender, E);
	        } else if ((radTerm.Checked)) {
		        TermReport(Sender, E);
	        } else {
		        OnSiteReport(Sender, E);

	        }

	        oOraConn.Close();
	        btnExport.Visible = true;
        }
        //--------------------------btnSubmit_Click()----------------------------------------------'
        protected void btnReturn_Click(object Sender, EventArgs E)
        {
	        string strUrl = "default.aspx";
	        Response.Redirect(strUrl);
        }
        //--------------------------CALENDAR-RELATED FUNCTIONS-------------------------------------'
        //--------------------------btnFromCalendar_Click()----------------------------------------------'
        protected void btnFromCalendar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
	        trowCalendar.Visible = true;
	        cldFromDate.Visible = true;
	        cldToDate.Visible = false;
        }
        //--------------------------btnToCalendar_Click()----------------------------------------------'
        protected void btnToCalendar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
	        trowCalendar.Visible = true;
	        cldToDate.Visible = true;
	        cldFromDate.Visible = false;
        }
        //--------------------------UpdateMonth()----------------------------------------------'
        protected void UpdateMonth(object Sender, MonthChangedEventArgs E)
        {
	        trowCalendar.Visible = true;
        }
        //--------------------------UpdateFromDate()----------------------------------------------'
        protected void UpdateFromDate(object Sender, EventArgs E)
        {
	        txtFrom.Text = cldFromDate.SelectedDate.ToShortDateString();
	        trowCalendar.Visible = false;

            Site masterPage = this.Master as Site;
            masterPage.Body.Attributes["onload"] = "document.forms[0]['txtTo'].focus();";
            masterPage.Body.Attributes["onload"] = "document.forms[0]['txtTo'].select();";
        }
        //--------------------------UpdateToDate()----------------------------------------------'
        protected void UpdateToDate(object Sender, EventArgs E)
        {
	        txtTo.Text = cldToDate.SelectedDate.ToShortDateString();
	        trowCalendar.Visible = false;

            Site masterPage = this.Master as Site;
            masterPage.Body.Attributes["onload"] = "document.forms[0]['btnSubmit'].focus();";
        }
        //--------------------radStatus_Click()-----------------------------------------------------'
        protected void radStatus_Click(object Sender, EventArgs E)
        {
	        if ((radStatus.Checked)) {
		        radOnSite.Checked = false;
		        radNewHire.Checked = false;
		        radTransfer.Checked = false;
		        radTerm.Checked = false;
		        tblDate.Visible = false;
		        tblFindEmpNum.Visible = true;
		        btnSubmit.Visible = false;
		        tblCompany.Visible = false;
		        tcellOnSiteResults.Visible = false;
		        tcellResults.Visible = false;
		        trowSearchResult.Visible = false;
		        tblPeoplemark.Visible = false;
	        }
        }
        //--------------------radOnSite_Click()-----------------------------------------------------'
        protected void radOnSite_Click(object Sender, EventArgs E)
        {
	        if ((radOnSite.Checked)) {
		        tblDate.Visible = false;
		        radStatus.Checked = false;
		        radNewHire.Checked = false;
		        radTransfer.Checked = false;
		        radTerm.Checked = false;
		        tblFindEmpNum.Visible = false;
		        btnSubmit.Visible = true;
		        tblCompany.Visible = true;
	        }
        }
        //--------------------radNewHire_Click()-----------------------------------------------------'
        protected void radNewHire_Click(object Sender, EventArgs E)
        {
	        if ((radNewHire.Checked)) {
		        tblDate.Visible = true;
		        radOnSite.Checked = false;
		        radStatus.Checked = false;
		        radTransfer.Checked = false;
		        radTerm.Checked = false;
		        tblFindEmpNum.Visible = false;
		        btnSubmit.Visible = true;
		        tblCompany.Visible = true;
	        }
        }
        //--------------------radTransfer_Click()-----------------------------------------------------'
        protected void radTransfer_Click(object Sender, EventArgs E)
        {
	        if ((radTransfer.Checked)) {
		        tblDate.Visible = true;
		        radOnSite.Checked = false;
		        radStatus.Checked = false;
		        radNewHire.Checked = false;
		        radTerm.Checked = false;
		        tblFindEmpNum.Visible = false;
		        btnSubmit.Visible = true;
		        tblCompany.Visible = true;
	        }
        }
        //--------------------radTerm_Click()-----------------------------------------------------'
        protected void radTerm_Click(object Sender, EventArgs E)
        {
	        if ((radTerm.Checked)) {
		        tblDate.Visible = true;
		        radOnSite.Checked = false;
		        radStatus.Checked = false;
		        radNewHire.Checked = false;
		        radTransfer.Checked = false;
		        tblFindEmpNum.Visible = false;
		        btnSubmit.Visible = true;
		        tblCompany.Visible = true;
	        }
        }
        //-------------------------btnReportTemp_Click(REDIRECT USER TO REPORTS PAGE)--------------------------'
        protected void btnReportTemp_Click(object Sender, EventArgs E)
        {
	        string strUrl = "ReportsTemp.aspx";
	        Response.Redirect(strUrl);
        }




    } //// end class
}