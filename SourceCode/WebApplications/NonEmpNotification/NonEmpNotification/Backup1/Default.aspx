﻿<%@ Page Title="Temporary Worker/Contractor Notification Form" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NonEmpNotification.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<table  style="width:100%; background-color:#CCCCCC">
        <tr>
            <td colspan="4" align="center"><hr /></td>
        </tr>
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold; font-size:12pt">TEMPORARY WORKER/CONTRACTOR NOTIFICATION FORM</td>
		</tr>
        <tr>
            <td colspan="4" align="center"><hr /></td>
        </tr>
		<tr>
			<td style="width:13%"><strong>Notification Date:</strong></td>
			<td style="width:30%"><asp:label ID="lblDate" runat="server"/></td>
			<td style="width:20%" colspan="2"><asp:label id="lblUserID" Text="1" Visible="false" runat="server"/></td>
		</tr>
		<tr>
			<td><strong>Notification Type: </strong></td>
			<td colspan="3" align="left">
					<asp:DropDownList id="ddlType" ForeColor="#000000" OnSelectedIndexChanged="ddlType_Change" AutoPostBack="true" runat="server">
						<asp:ListItem Selected="true">Select Notification Type</asp:ListItem>
						<asp:ListItem Text="Add New Temporary Worker/Contractor" Value="Add"/>
						<asp:ListItem Text="Transfer Temporary Worker/Contractor" Value="Transfer"/>
						<asp:ListItem Text="Terminate Temporary Worker/Contractor" Value="Terminate"/>
						<asp:ListItem Text="Revise Request" Value="Revision"/>
					</asp:DropDownList>								
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4" align="center" id="tcellSearchResult">
				<asp:label ForeColor="#E64A00" Font-Bold="true" id="lblSearchResult" runat="server"/>										
			</td>
		</tr>
		<tr style="vertical-align:top">
			<td  style="vertical-align:bottom" colspan="4">
                <div id="trowSearch" runat="server">
				<table width="100%">
					<tr>
						<td style="width:20%">Badge ID:</td>
						<td style="vertical-align:bottom">
							<asp:TextBox id="txtSearch" runat="server"/>
							<asp:Button id="btnSearch" Text="Search" runat="server" 
                                onclick="btnSearch_Click"/>	
							<asp:button id="btnHelpFind" OnClick="btnHelpFind_Click" Text="Help Find Badge ID" runat="server"/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
                            <div id="tblFindEmpNum" runat="server">
							<table width="100%">
								<tr>
									<td style="width:20%">Last Name: </td>
									<td>
										<asp:TextBox id="txtSearchLName" runat="server"/>&nbsp;
										First Name: <asp:TextBox id="txtSearchFName" runat="server"/>		
										<asp:Button id="btnFindEmpNum" OnClick="btnFindEmpNum_Click" Text="Search for Badge ID" runat="server"/>					
									</td>	
							  	</tr>
                    			<tr>
									<td colspan="2">
                                        <div id="trowSearchResult" runat="server">
					                    <table>
                                            <tr>
                                                <td>
										        <asp:DataGrid id="dgSearchResults" Font-Size="10" HeaderStyle-Font-Bold="true" OnItemDataBound="dgSearchResults_Data" autogeneratecolumns="False" runat="server">
											        <columns>
												        <asp:TemplateColumn HeaderText="">
													        <ItemTemplate>
														        <asp:CheckBox ID="chkSelection" Runat="server" />
													        </ItemTemplate>
												        </asp:TemplateColumn>
												        <asp:TemplateColumn HeaderText="Badge ID">
													        <ItemTemplate>
														        <asp:Label ID="BADGEID" Text='<%# DataBinder.Eval(Container.DataItem,"BADGEID") %>' runat="server" />
													        </ItemTemplate>
												        </asp:TemplateColumn>										
													        <asp:BoundColumn HeaderText="Last Name" DataField="LNAME"/>	
													        <asp:BoundColumn HeaderText="First Name" DataField="FNAME"/>													
													        <asp:BoundColumn HeaderText="Start Date" DataField="STARTDATE" DataFormatString="{0:d}"/>	
													        <asp:BoundColumn HeaderText="Job Title" DataField="JOBTITLE"/>	
													        <asp:BoundColumn HeaderText="Company" DataField="CompanyID"/>	
													        <asp:BoundColumn HeaderText="Location" DataField="LOCATION"/>	
													        <asp:BoundColumn HeaderText="Cost Center" DataField="COSTCENTER"/>
													        <asp:BoundColumn HeaderText="Supervisor Name" DataField="SUPNAME"/>	
													        <asp:BoundColumn HeaderText="Supervisor Email" DataField="SUPEMAIL"/>	
												        </columns>
											        </asp:DataGrid>
											        <br/>
											        <asp:DataGrid id="dgSearchRequest" autogeneratecolumns="False" Font-Size="10" HeaderStyle-Font-Bold="true" runat="server">
												        <columns>
													        <asp:TemplateColumn HeaderText="Employee #" Visible="false">
														        <ItemTemplate>
															        <asp:Label ID="REQUESTID" Text='<%# DataBinder.Eval(Container.DataItem,"REQUESTID") %>' runat="server" />
														        </ItemTemplate>
												            </asp:TemplateColumn>					
													        <asp:TemplateColumn HeaderText="">
														        <ItemTemplate>
														           <asp:CheckBox ID="chkSelection" Runat="server" />
														        </ItemTemplate>
														        </asp:TemplateColumn>
														        <asp:BoundColumn HeaderText="Request Date" DataField="RequestDate" DataFormatString="{0:d}"/>	
														        <asp:BoundColumn HeaderText="Request Type" DataField="RequestType"/>	
														        <asp:TemplateColumn HeaderText="Badge ID">
															        <ItemTemplate>
																        <asp:Label ID="BADGEID" Text='<%# DataBinder.Eval(Container.DataItem,"BADGEID") %>' runat="server" />
															        </ItemTemplate>
												    	        </asp:TemplateColumn>										
														        <asp:BoundColumn HeaderText="Last Name" DataField="LNAME"/>	
														        <asp:BoundColumn HeaderText="First Name" DataField="FNAME"/>														
														        <asp:BoundColumn HeaderText="Effective Date" DataField="EFFECTIVEDATE" DataFormatString="{0:d}"/>
														        <asp:BoundColumn HeaderText="Company" DataField="COMPANYNAME"/>														
														        <asp:BoundColumn HeaderText="Supervisor Name" DataField="SUPNAME"/>	
														        <asp:BoundColumn HeaderText="Form Submitted By:" DataField="REP"/>	
													        </columns>
												        </asp:DataGrid>
												        <asp:button id="btnSearchSelected" OnClick="btnSearchSelected_Click" Text="Search by Checked Employee#" runat="server"/>
												        <asp:button id="btnSearchSelectedRequest" OnClick="btnSearchSelectedRequest_Click" Text="Search by Checked Request" runat="server"/>
                                                    </td>
                                                </tr>
                                            </table>
                                            </div>
										</td>
									</tr>
                                   
						  </table>
                          </div>
					  </td>
				  </tr>
				</table>
                </div>			
			</td>
		</tr>
		<tr>
			<td colspan="4">
                <div id="trowRequest" runat="server">
                    <table>
                        <tr>
                            <td style="font-weight:bold">
			                    <asp:label id="lblReqType" runat="server"/> Request Submitted on: <asp:label id="lblReqDate" runat="server"/>
                                <asp:label id="lblReqID" runat="server" Visible="false"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
		</tr>
		<tr>
			<td colspan="4">
				<div  id="tblDetails" runat="server">
                <table id="Table1" width="100%" cellpadding="0" cellspacing="0" runat="server">
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>	
						<td ID="Tablecell1" style="width:20%" runat="server">Last Name:&nbsp;<span class="style1">*</span></td>
						<td ID="Tablecell2" style="width:35%" runat="server">
							<asp:TextBox  id="txtLName" width="250" runat="server"/>
						</td>									
						<td ID="Tablecell3" style="width:20%" runat="server">First Name:&nbsp;<span class="style1">*</span></td>
						<td ID="Tablecell4" style="width:30%" runat="server">
							<asp:TextBox id="txtFName" width="250" runat="server"/>
						</td>
					</tr>
					<tr>
						<td>Badge ID: </td>
						<td>
							<asp:TextBox id="txtEmpNum" width="250" Enabled="false" runat="server"/>

						</td>
						<td>
							<asp:label id="lblDateType" runat="server"/> Date: <font size="2">(mm/dd/yyyy)</font>&nbsp;<span class="style1">*</span>
						</td>
						<td>
							<asp:TextBox id="txtDate" runat="server"/>
							<asp:ImageButton ID="btnCalendar" OnClick="btnCalendar_Click" ImageUrl="~/Images/calendar.gif" ToolTip="View Calendar" runat="server"/>
						</td>
					</tr>												
					<tr id="trowCalendar">
						<td colspan="3"><!-- CALENDAR --></td>
						<td style="width:30%">
							<asp:Calendar id="cldDate" EnableViewState="false" OnVisibleMonthChanged="UpdateMonth" OnSelectionChanged="UpdateDate" DayHeaderStyle-BackColor="#999999" TodayDayStyle-BackColor="#FF9933" ToolTip="Click on the date to close the calendar" runat="server"/>
						</td>
					</tr>
					<tr>	
						<td style="width:20%">Supervisor Name:&nbsp;<span class="style1">*</span></td>
						<td style="width:35%">
							<asp:TextBox id="txtSupName" width="250" runat="server"/>
						</td>
						<td style="width:20%">Supervisor Email: </td>
						<td style="width:30%">
							<asp:TextBox id="txtSupEmail" width="250" runat="server"/>
						</td>
					</tr>								
					<tr>
						<td style="width:20%">Job Title:&nbsp;<span class="style1">*</span></td>
						<td style="width:35%">
							<asp:TextBox id="txtJob" width="250" runat="server"/>
						</td>			
						<td>Location: </td>
						<td>
							<asp:TextBox id="txtLocation" width="250" runat="server"/>
						</td>						
					</tr>
					<tr>
						<td style="vertical-align:top">
							Company:&nbsp;<span class="style1">*</span><br/><font size="2">(Select <strong>Other</strong> to add company)</font> 
						</td>
						<td>
							<asp:DropDownList id="ddlCompany" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_Change" ForeColor="#000000" runat="server"/>
                            <div id="tblOtherCompany" runat="server">
                            <table id="Table2"  Visible="false" width="100%" runat="server">
								<tr>
									<td>
										<span class="style1">*</span>Specify: <asp:TextBox id="txtOtherComp" runat="server"/>
									</td>
								</tr>								
							</table>
                            </div>
						</td>
						<td style="width:20%;vertical-align:top">SAP Cost Center:&nbsp;<span class="style1">*</span><br/><font size="2">(7-Digit Number)</font> </td>
						<td style="width:35%;vertical-align:top">
							<asp:TextBox id="txtCC" Columns="7" Text="0000000" MaxLength="7" runat="server"/>
						</td>									
					</tr>
					<tr>
						<td colspan="2">
                            <div id="tblReturn" runat="server">
							    <table style="width:100%">
								    <tr> 
									    <td style="width:44%">Return Eligibility&nbsp;<span class="style1">*</span></td>
									    <td>
										    <asp:DropDownList id="ddlReturn" runat="server">
											    <asp:ListItem Text="Select Status" Value="0"></asp:ListItem>
											    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
											    <asp:ListItem Text="No" Value="N"></asp:ListItem>
										    </asp:DropDownList>										
									    </td>			
								    </tr>
							    </table>
                            </div>
						</td>
						</tr>											
						<tr>
							<td colspan="4">
								<br/>Temp/Contractor Comments:
								<br/><asp:textbox id="txtNonEmployeeComments" TextMode="MultiLine" Columns="70" runat="server"/>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<br/>Email Comments:
								<br/><asp:textbox id="txtEmailComments" TextMode="MultiLine" Columns="70" runat="server"/>
							</td>
						</tr>
						<tr>
							<td colspan="4" align="center">
								<br/><asp:button id="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" runat="server"/>								
								<asp:button id="btnClear" Text="Clear" OnClick="btnClear_Click" runat="server"/>
							</td>
						</tr>								
				</table>	
                </div>									
			</td>
		</tr>
		<tr>
			<td colspan="4" align="left">
				<asp:button id="btnReportTemp" Text="Run Reports on Temporary Workers (restricted)" OnClick="btnReportTemp_Click" runat="server"/>
				<asp:button id="btnReportContract" Text="Run Reports on Contractors (restricted)" OnClick="btnReportContract_Click" runat="server"/>			
			</td>
		</tr>									
	</table>	

</asp:Content>
