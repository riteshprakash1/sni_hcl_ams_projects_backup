﻿// ------------------------------------------------------------------
// <copyright file="AdminConfig.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2012 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace NonEmpNotification.Admin
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using NonEmpNotification.Classes;

    /// <summary>
    /// This is the admin config class for updating the web.config file
    /// </summary>
    public partial class AdminConfig : System.Web.UI.Page
    {
        /// <summary>
        /// This is an instance of the System.Configuration
        /// </summary>
        private Configuration configuration;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsConfigAdmin())
            {
                Response.Redirect("~/Default.aspx", true);
            }

            this.configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

            string strConfigAdmin = System.Configuration.ConfigurationManager.AppSettings["ConfigAdmin"];

            ////configuration = WebConfigurationManager.OpenWebConfiguration("~");
            if (!Page.IsPostBack)
            {
                this.BindDT();
                this.GetSMTP();
                this.BindConnString();
            }

            this.lblUpdateMsg.Text = string.Empty;
        }

        /// <summary>
        /// Binds the users table to the gvCosts GridView.
        /// </summary>
        protected void BindDT()
        {
            DataTable dt = this.GetSettingsTable();

            this.gvAppSettings.DataSource = dt;
            this.gvAppSettings.DataBind();
        } //// end BindDT

        /// <summary>
        /// Binds the users table to the gvCosts GridView.
        /// </summary>
        protected void BindConnString()
        {
            DataTable dt = this.GetConnectionStringTable();

            this.gvConnString.DataSource = dt;
            this.gvConnString.DataBind();
        } //// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('key');");

                if (e.Row.RowIndex == this.gvAppSettings.EditIndex)
                {
                    TextBox txtSettingValue = (TextBox)e.Row.FindControl("txtSettingValue");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtSettingValue, btnUpdate);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddSettingValue = (TextBox)e.Row.FindControl("txtAddSettingValue");
                TextBox txtAddSettingKey = (TextBox)e.Row.FindControl("txtAddSettingKey");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddSettingValue, btnAdd);
                EnterButton.TieButton(txtAddSettingKey, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the RowDataBound event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBoundConnSTring(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == this.gvAppSettings.EditIndex)
                {
                    TextBox txtConnectionString = (TextBox)e.Row.FindControl("txtConnectionString");
                    Button btnUpdateConn = (Button)e.Row.FindControl("btnUpdateConn");
                    EnterButton.TieButton(txtConnectionString, btnUpdateConn);
                }
            }
        } // end GV_RowDataBoundConnSTring

        /// <summary>
        /// Handles the Delete event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strSettingKey = this.gvAppSettings.DataKeys[e.RowIndex]["SettingKey"].ToString();
            TextBox txtSettingValue = (TextBox)this.gvAppSettings.Rows[e.RowIndex].FindControl("txtSettingValue");

            KeyValueConfigurationCollection settings = this.configuration.AppSettings.Settings;
            settings.Remove(strSettingKey);
            this.configuration.Save();

            this.gvAppSettings.EditIndex = -1;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Edit event of the gvCost GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvAppSettings.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Edit event of the gvCost GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_EditConnString(object sender, GridViewEditEventArgs e)
        {
            this.gvConnString.EditIndex = e.NewEditIndex;
            this.BindConnString();
        }

        /// <summary>
        /// Handles the Update event of the gvConnString GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_UpdateConString(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateConnectionString");

            if (Page.IsValid)
            {
                try
                {
                    string strSettingName = this.gvConnString.DataKeys[e.RowIndex]["SettingName"].ToString();
                    TextBox txtConnectionString = (TextBox)this.gvConnString.Rows[e.RowIndex].FindControl("txtConnectionString");

                    ConnectionStringSettingsCollection settings = this.configuration.ConnectionStrings.ConnectionStrings;
                    settings[strSettingName].ConnectionString = txtConnectionString.Text;

                    this.configuration.Save();

                    this.gvConnString.EditIndex = -1;
                    this.BindConnString();
                    this.lblUpdateMsg.Text = "Connection String Information updated successfully";
                }
                catch (Exception ex)
                {
                    this.lblUpdateMsg.Text = "Connection String Information update failed. <br>" + ex.Message;
                }
            } // end Page.IsValid
        } // end GV_UpdateConString


        /// <summary>
        /// Handles the Update event of the gvCost GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateSetting");

            if (Page.IsValid)
            {
                string strSettingKey = this.gvAppSettings.DataKeys[e.RowIndex]["SettingKey"].ToString();
                TextBox txtSettingValue = (TextBox)this.gvAppSettings.Rows[e.RowIndex].FindControl("txtSettingValue");

                KeyValueConfigurationCollection settings = this.configuration.AppSettings.Settings;
                settings[strSettingKey].Value = txtSettingValue.Text;

                this.configuration.Save();

                this.gvAppSettings.EditIndex = -1;
                this.BindDT();
            } // end Page.IsValid
        } // end GV_Update

        /// <summary>
        /// Handles the Edit event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddSetting");

            TextBox txtAddSettingValue = (TextBox)this.gvAppSettings.FooterRow.FindControl("txtAddSettingValue");
            TextBox txtAddSettingKey = (TextBox)this.gvAppSettings.FooterRow.FindControl("txtAddSettingKey");
            KeyValueConfigurationCollection settings = this.configuration.AppSettings.Settings;

            CustomValidator vldSettingKeyExists = (CustomValidator)this.gvAppSettings.FooterRow.FindControl("vldSettingKeyExists");

            vldSettingKeyExists.IsValid = Convert.ToBoolean(settings[txtAddSettingKey.Text] == null);

            if (Page.IsValid)
            {
                settings.Add(txtAddSettingKey.Text.Trim(), txtAddSettingValue.Text);
                this.configuration.Save();

                this.gvAppSettings.EditIndex = -1;
                this.BindDT();
            } //// Page.IsValid
        }

        /// <summary>Determines whether [is config admin].</summary>
        /// <returns><c>true</c> if [is config admin]; otherwise, <c>false</c>.</returns>
        private bool IsConfigAdmin()
        {
            // Get the UserName
            string[] arrayAuth = Request.ServerVariables["AUTH_USER"].Split('\\');
            string strUser = String.Empty;
            if (arrayAuth.Length > 1)
            {
                strUser = arrayAuth[1].ToLower().Trim();
            }
            else
            {
                return false;
            }

            string strConfigAdmin = string.Empty;
            if (ConfigurationManager.AppSettings["ConfigAdmin"] != null)
            {
                strConfigAdmin = ConfigurationManager.AppSettings["ConfigAdmin"];
            }

            string[] strAdmins = strConfigAdmin.Split(',');

            foreach (string strAdmin in strAdmins)
            {
                if (strAdmin.ToLower().Trim() == strUser)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the settings table.
        /// </summary>
        /// <returns>settings datatable</returns>
        private DataTable GetSettingsTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("SettingKey", typeof(string)));
            dt.Columns.Add(new DataColumn("SettingValue", typeof(string)));

            KeyValueConfigurationCollection settings = this.configuration.AppSettings.Settings;
            string[] keys = settings.AllKeys;
            foreach (string key in keys)
            {
                DataRow r = dt.NewRow();
                r["SettingKey"] = key;
                r["SettingValue"] = settings[key].Value;
                dt.Rows.Add(r);
                Debug.WriteLine("Key: " + key + "; Value: " + settings[key].Value);
            }

            return dt;
        }

        /// <summary>
        /// Gets the Connection String table.
        /// </summary>
        /// <returns>Connection String datatable</returns>
        private DataTable GetConnectionStringTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("SettingName", typeof(string)));
            dt.Columns.Add(new DataColumn("ConnectionString", typeof(string)));

            ConnectionStringSettingsCollection settings = this.configuration.ConnectionStrings.ConnectionStrings;

            foreach (ConnectionStringSettings set in settings)
            {
                DataRow r = dt.NewRow();
                r["SettingName"] = set.Name;
                r["ConnectionString"] = set.ConnectionString;
                dt.Rows.Add(r);
                Debug.WriteLine("Name: " + set.Name + "; Value: " + set.ConnectionString);
            }

            return dt;
        }

        /// <summary>
        /// Loads the SMTP information.
        /// </summary>
        private void GetSMTP()
        {

            System.Net.Configuration.MailSettingsSectionGroup mail = (System.Net.Configuration.MailSettingsSectionGroup)this.configuration.GetSectionGroup("system.net/mailSettings");
            System.Net.Configuration.SmtpSection smtp = (System.Net.Configuration.SmtpSection)this.configuration.GetSection("system.net/mailSettings/smtp");
            System.Net.Configuration.SmtpNetworkElement network = smtp.Network;
            Debug.WriteLine("SMTP Host: " + network.Host);
            this.txtHost.Text = network.Host;
            this.txtPort.Text = network.Port.ToString();

        }

        /// <summary>
        /// Handles the click event of the btnUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            Page.Validate("UpdateSMTP");
            try
            {
                System.Net.Configuration.SmtpSection smtp = (System.Net.Configuration.SmtpSection)this.configuration.GetSection("system.net/mailSettings/smtp");
                System.Net.Configuration.SmtpNetworkElement network = smtp.Network;

                network.Port = Convert.ToInt32(this.txtPort.Text);
                network.Host = this.txtHost.Text;

                this.configuration.Save();
                this.lblUpdateMsg.Text = "SMTP Information updated successfully";
            }
            catch (Exception ex)
            {
                this.lblUpdateMsg.Text = "SMTP Information update failed. <br>" + ex.Message;
            }

        }
    } //// end class
}
