﻿// ------------------------------------------------------------------
// <copyright file="Default.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------


namespace NonEmpNotification
{
    using System;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmpNotification.Classes;

    //using System.Data.OleDb;
    using System.Data.SqlClient;
    using System.Data.Common;
    using System.IO;
    using System.Net.Mail;

    /// <summary>This is the Default page.</summary> 
    public partial class Default : System.Web.UI.Page
    {
        //OleDbDataAdapter rdaCompany;
        //OleDbConnection oOraConn = new System.Data.OleDb.OleDbConnection(ConfigurationManager.ConnectionStrings["OracleConnString"].ToString());
        //OleDbCommand cmd = null;
        //OleDbDataReader rdrEmployee = null;
        string strRead;
        string strEmployee;
        string strCCSupervisor;
        string strRequestType;
        string strEmpNum;
        //string intCompID;
        string strLocation;
        string strReq;
        string strSupEmail;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        protected void Page_Load(object sender, EventArgs e)
        {
            string strNTUserID = ADUtility.GetUserNameOfAppUser(Request).ToUpper();
            lblUserID.Text = strNTUserID;

            //Show Current Date as Request Date'
            lblDate.Text = DateTime.Now.ToShortDateString();

            //If Page is Not PostBack'
            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtSearch, this.btnSearch);
                EnterButton.TieButton(this.txtSearchFName, this.btnFindEmpNum);
                EnterButton.TieButton(this.txtSearchLName, this.btnFindEmpNum);


                this.btnClear_Click(sender, e);
                this.ddlType_Change(sender, e);

                bool blnReportingEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["ReportingEnabled"]);
                this.btnReportTemp.Visible = blnReportingEnabled;
                this.btnReportContract.Visible = blnReportingEnabled;
            }

        }

        protected void ddlType_Change(object sender, EventArgs e)
        {

            this.lblDateType.Text = "Start";

            //tblFindEmpNum.Visible = false
            this.txtEmpNum.Enabled = false;
            txtEmpNum.Text = "Generated when form is submitted";

            this.txtSearch.Text = "";

            this.trowSearchResult.Visible = false;
            //If Request Type <> Select Notification Type'
            if (ddlType.SelectedIndex != 0)
            {
                //If Request Type = New Temp/Contractor'
                if (ddlType.SelectedIndex == 1)
                {
                    ddlCompany.SelectedIndex = 0;
                    txtEmpNum.Text = "Generated when form is submitted";

                    tblDetails.Visible = true;
                    trowSearch.Visible = false;
                    this.trowSearchResult.Visible = false;
                }
                else
                {
                    //IF Request Type <> New Temp/Contractor'	
                    trowSearch.Visible = true;
                    btnHelpFind.Visible = true;
                    tblDetails.Visible = false;
                }

                lblSearchResult.Text = "";
                lblSearchResult.Visible = false;

            }
            else
            {
                //If Select Request Type is Selected'

                tblDetails.Visible = false;
                trowSearch.Visible = false;
            }

            //Hide Request Information used in Revision option'

            trowRequest.Visible = false;
            //lblReqType.Visible = false;

            //Clear Information'
            btnClear_Click(sender, e);

            string sQuery = "select * from Company order by CompanyName";
            DataTable dtCompany = SQLUtility.SqlExecuteDynamicQuery(sQuery);

            //rdaCompany = new OleDbDataAdapter("select * from pwdreset.Company order by CompanyName", oOraConn);
            //DataSet rdsCompany = new DataSet();
            //rdaCompany.Fill(rdsCompany, "Company");
            //ddlCompany.DataSource = rdsCompany;

            ddlCompany.DataSource = dtCompany;
            ddlCompany.DataTextField = dtCompany.Columns["CompanyName"].ColumnName.ToString();
            ddlCompany.DataValueField = dtCompany.Columns["CompanyId"].ColumnName.ToString();
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, "Select Company");
            trowCalendar.Visible = false;
            tblFindEmpNum.Visible = false;
            trowSearchResult.Visible = false;
            //Close DB Connection'
            //oOraConn.Close()			'	

        }

        private bool IsNumeric(string strValue)
        {
            try
            {
                int result = Int32.Parse(strValue);
                return true;
            }
            catch
            {
                Debug.WriteLine("Not a valid integer");
                return false;
            }

        }

        //----------------------btnSubmit_Click (FORM IS SUBMITTED - VERIFY INFORMATION)---------------'
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //If Other Selected - require company name'
            if ((IsOtherCompany(ddlCompany.SelectedValue) & (txtOtherComp.Text == "")))
            {
                lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                lblSearchResult.Visible = true;
                //Check for other stuff'
            }
            else
            {
                //If Request Type <> Terminate'	
                if (((ddlType.SelectedIndex != 3) & (ddlType.SelectedIndex != 4)))
                {
                    //If one of the required fields is not filled out'
                    if (((txtFName.Text == "") | (txtLName.Text == "") | (txtDate.Text == "") | (txtSupName.Text == "") | (txtJob.Text == "") | (txtCC.Text == "") | (!(IsNumeric(txtCC.Text))) | (txtCC.Text.Length != 7) | (ddlCompany.SelectedIndex == 0)))
                    {
                        lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                        lblSearchResult.Visible = true;
                        tblDetails.Visible = true;
                        //If all required fields are filled out'
                    }
                    else
                    {
                        tblDetails.Visible = true;
                        lblSearchResult.Visible = false;
                        trowRequest.Visible = false;
                        //lblReqType.Visible = false;
                        SubmitForm(sender, e);
                        //Clear Form'
                        btnClear_Click(sender, e);
                    }

                    //If Request Type = Terminate'
                }
                else if ((ddlType.SelectedIndex == 3))  //Terminate
                {
                    //If one of the required fields is not filled out'
                    if (((txtLName.Text == "") | (txtFName.Text == "") | (txtDate.Text == "") | (txtSupName.Text == "") | (txtJob.Text == "") | (txtCC.Text == "") | (!(IsNumeric(txtCC.Text))) | (txtCC.Text.Length != 7) | (ddlCompany.SelectedIndex == 0) | (ddlReturn.SelectedIndex == 0)))
                    {
                        lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                        lblSearchResult.Visible = true;
                        tblDetails.Visible = true;
                        //If all required fields are filled out'
                    }
                    else
                    {
                        tblDetails.Visible = true;
                        lblSearchResult.Visible = false;
                        SubmitForm(sender, e);
                        //Clear Form'
                        btnClear_Click(sender, e);
                    }

                    //Request Type = Revision'
                }
                else
                {
                    //If one of the required fields is not filled out'
                    if (((txtLName.Text == "") | (txtFName.Text == "") | (txtDate.Text == "") | (txtSupName.Text == "") | (txtJob.Text == "") | (txtCC.Text == "") | (!(IsNumeric(txtCC.Text))) | (txtCC.Text.Length != 7) | (ddlCompany.SelectedIndex == 0)))
                    {
                        lblSearchResult.Text = "ERROR: Request has not been submitted. Please complete all fields and try again.";
                        lblSearchResult.Visible = true;
                        tblDetails.Visible = true;
                        //If all required fields are filled out'
                    }
                    else
                    {
                        tblDetails.Visible = true;
                        lblSearchResult.Visible = false;
                        //Save Request and Employee Information Revision'
                        SaveRevision(sender, e);

                        //Clear Form'
                        btnClear_Click(sender, e);
                    }
                }
            }
        }

        private int GetRequestID(string strReqID)
        {
            try
            {
                return Convert.ToInt32(strReqID);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetRequestID: " + ex.Message);
                return 0;
            }

        }

        //-----------------------------SaveRevision(SAVE REQUEST AND EMPLOYEE INFO UPDATE)----------'
        protected void SaveRevision(object sender, EventArgs e)
        {
            Debug.WriteLine("SaveRevision");

            int intCompID = 0;
            //oOraConn.Open();
            FixDate(sender, e);
            //Generate Company ID'
            //if (IsOtherCompany(ddlCompany.SelectedValue))
            //{
            //    //Insert Company'
            //    strRead = "Insert Into pwdreset.Company Values (select max(CompanyID) from pwdreset.company + 1), txtOtherComp.Text";
            //    cmd = new OleDbCommand(strRead, oOraConn);
            //    object sqlInsert = cmd.ExecuteScalar();
            //    //Retrive CompanyID'
            //    strRead = "select CompanyID from pwdreset.Company where CompanyName ='" + txtOtherComp.Text + "'";
            //    cmd = new OleDbCommand(strRead, oOraConn);
            //    intCompID = cmd.ExecuteScalar();
            //}
            //else
            //{
            //    intCompID = ddlCompany.SelectedValue;
            //}

            //If company is set to Other - Generate Company ID and insert company into DB'
            if (IsOtherCompany(ddlCompany.SelectedValue))
            {
                Collection<SqlParameter> myCompanyParameters = new Collection<SqlParameter>();

                SqlParameter SQLID = new SqlParameter("@CompanyID", 0);
                SQLID.Direction = ParameterDirection.Output;
                myCompanyParameters.Add(SQLID);

                myCompanyParameters.Add(new SqlParameter("@CompanyName", txtOtherComp.Text));
                SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertCompany", myCompanyParameters);

                if (insertParameteres["@CompanyID"] != null)
                {
                    intCompID = Convert.ToInt32(insertParameteres["@CompanyID"].Value.ToString());
                }
            }
            else
            {
                intCompID = Convert.ToInt32(ddlCompany.SelectedValue);
            }

            //Update Request Info'
            //strRead = "Update pwdreset.NonEmpRequest Set EFFECTIVEDATE=:NewDate, REP=:Rep  WHERE REQUESTID=" + lblReqID.Text;
            //cmd = new OleDbCommand(strRead, oOraConn);
            //cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
            //cmd.Parameters.Add(new OleDbParameter("Rep", lblUserID.Text));
            //object sqlUpdate = cmd.ExecuteScalar();

            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@NewDate", txtDate.Text));
            myParameters.Add(new SqlParameter("@Rep", lblUserID.Text));
            myParameters.Add(new SqlParameter("@RequestID", Int32.Parse(lblReqID.Text)));

            int intRecordsUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmpRequestEffectiveDateRep", myParameters);

            //For Term - update eligible to return'
            if ((lblReqType.Text == "Terminate"))
            {
                //strRead = "Update pwdreset.NonEmpRequest Set ELIGIBLERTN=:RTN, EFFECTIVEDATE=:NewDate WHERE REQUESTID=" + lblReqID.Text;
                //cmd = new OleDbCommand(strRead, oOraConn);
                //cmd.Parameters.Add(new OleDbParameter("RTN", ddlReturn.SelectedValue));
                //cmd.Parameters.Add(new OleDbParameter("NewDate", txtDate.Text));
                //sqlUpdate = cmd.ExecuteScalar();

                myParameters = new Collection<SqlParameter>();
                myParameters.Add(new SqlParameter("@NewDate", txtDate.Text));
                myParameters.Add(new SqlParameter("@RequestID", Int32.Parse(lblReqID.Text)));
                myParameters.Add(new SqlParameter("@EligibleReturn", ddlReturn.SelectedValue));

                int intReturnUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmpRequestEligibleReturn", myParameters);
            }

            Debug.WriteLine("New: " + Convert.ToBoolean(lblReqType.Text == "New").ToString());
            Debug.WriteLine("lblReqType: " + lblReqType.Text);

            //For NEW HIRE'
          //  if ((lblReqType.Text == "New"))

            // Parameters need to be in the same order as they are listed in the Update Query

            if ((lblReqType.Text == "Add"))
            {
                Debug.WriteLine("In lblReqType.Text == 'New'");
              //  strRead = "Update pwdreset.NonEmployee Set FNAME =:FNAME, LNAME = :LNAME, JOBTITLE = :JOBTITLE, COMPANYID = :COMPANY, LOCATION=:LOCATION, SUPNAME = :SUPNAME, SUPEMAIL = :SUPEMAIL, COMMENTS=:COMMENTS, COSTCENTER=:COSTCENTERNUM, STARTDATE=:NEWDATE WHERE BADGEID =" +;
              //  cmd = new OleDbCommand(strRead, oOraConn);
                Collection<SqlParameter> nonEmpParameters = new Collection<SqlParameter>();
                nonEmpParameters.Add(new SqlParameter("@FNAME", txtFName.Text.Trim().ToUpper()));
                nonEmpParameters.Add(new SqlParameter("@LNAME", txtLName.Text.Trim().ToUpper()));
                nonEmpParameters.Add(new SqlParameter("@JOBTITLE", txtJob.Text));
                nonEmpParameters.Add(new SqlParameter("@COMPANYID", intCompID));
                nonEmpParameters.Add(new SqlParameter("@LOCATION", txtLocation.Text));
                nonEmpParameters.Add(new SqlParameter("@SUPNAME", txtSupName.Text));
                nonEmpParameters.Add(new SqlParameter("@SUPEMAIL", txtSupEmail.Text));
                nonEmpParameters.Add(new SqlParameter("@COMMENTS", txtNonEmployeeComments.Text));
                nonEmpParameters.Add(new SqlParameter("@COSTCENTER", txtCC.Text));
                nonEmpParameters.Add(new SqlParameter("@STARTDATE", txtDate.Text));
                nonEmpParameters.Add(new SqlParameter("@BADGEID", Int32.Parse(txtEmpNum.Text)));
            
                int intNonEmployeeUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmployee", nonEmpParameters);

                //Update Request'
                Collection<SqlParameter> effectParameters = new Collection<SqlParameter>();
                effectParameters.Add(new SqlParameter("@NewDate", txtDate.Text));
                effectParameters.Add(new SqlParameter("@RequestID", Int32.Parse(lblReqID.Text)));

                int intEffDateUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmpRequestEffectiveDate", effectParameters);

                SendNotification(this.GetRequestID(lblReqID.Text));

                //Transfer'
            }
            else if ((lblReqType.Text == "Transfer"))
            {
                Debug.WriteLine("In lblReqType.Text == 'Transfer'");
                //strRead = "Update pwdreset.NonEmployee Set FNAME =:FNAME, LNAME = :LNAME, JOBTITLE = :JOBTITLE, COMPANYID = :COMPANY, LOCATION=:LOCATION, SUPNAME = :SUPNAME, SUPEMAIL = :SUPEMAIL, COMMENTS=:COMMENTS, COSTCENTER=:COSTCENTERNUM WHERE BADGEID =" + txtEmpNum.Text;
                //cmd = new OleDbCommand(strRead, oOraConn);
                Collection<SqlParameter> nonEmpParameters = new Collection<SqlParameter>();
                nonEmpParameters.Add(new SqlParameter("@FNAME", txtFName.Text.Trim().ToUpper()));
                nonEmpParameters.Add(new SqlParameter("@LNAME", txtLName.Text.Trim().ToUpper()));
                nonEmpParameters.Add(new SqlParameter("@JOBTITLE", txtJob.Text));
                nonEmpParameters.Add(new SqlParameter("@COMPANYID", intCompID));
                nonEmpParameters.Add(new SqlParameter("@LOCATION", txtLocation.Text));
                nonEmpParameters.Add(new SqlParameter("@SUPNAME", txtSupName.Text));
                nonEmpParameters.Add(new SqlParameter("@SUPEMAIL", txtSupEmail.Text));
                nonEmpParameters.Add(new SqlParameter("@COMMENTS", txtNonEmployeeComments.Text));
                nonEmpParameters.Add(new SqlParameter("@COSTCENTER", txtCC.Text));
                nonEmpParameters.Add(new SqlParameter("@BADGEID", Int32.Parse(txtEmpNum.Text)));

                int intNonEmployeeUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmployeeNoStartDateNoActive", nonEmpParameters);

                //Update Request'
                Collection<SqlParameter> effectParameters = new Collection<SqlParameter>();
                effectParameters.Add(new SqlParameter("@NewDate", txtDate.Text));
                effectParameters.Add(new SqlParameter("@RequestID", Int32.Parse(lblReqID.Text)));

                int intEffDateUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmpRequestEffectiveDate", effectParameters);

                SendNotification(this.GetRequestID(lblReqID.Text));

                //Terminate'
            }
            else
            {
                Debug.WriteLine("In lblReqType.Text == 'Terminate'");
          //      strRead = "Update pwdreset.NonEmployee Set FNAME =:FNAME, LNAME = :LNAME, JOBTITLE = :JOBTITLE, COMPANYID = :COMPANY, LOCATION=:LOCATION, SUPNAME = :SUPNAME, SUPEMAIL = :SUPEMAIL, COMMENTS=:COMMENTS, COSTCENTER=:COSTCENTERNUM WHERE BADGEID =" + txtEmpNum.Text;
                Collection<SqlParameter> nonEmpParameters = new Collection<SqlParameter>();
                nonEmpParameters.Add(new SqlParameter("@FNAME", txtFName.Text.Trim().ToUpper()));
                nonEmpParameters.Add(new SqlParameter("@LNAME", txtLName.Text.Trim().ToUpper()));
                nonEmpParameters.Add(new SqlParameter("@JOBTITLE", txtJob.Text));
                nonEmpParameters.Add(new SqlParameter("@COMPANYID", intCompID));
                nonEmpParameters.Add(new SqlParameter("@LOCATION", txtLocation.Text));
                nonEmpParameters.Add(new SqlParameter("@SUPNAME", txtSupName.Text));
                nonEmpParameters.Add(new SqlParameter("@SUPEMAIL", txtSupEmail.Text));
                nonEmpParameters.Add(new SqlParameter("@COMMENTS", txtNonEmployeeComments.Text));
                nonEmpParameters.Add(new SqlParameter("@COSTCENTER", txtCC.Text));
                nonEmpParameters.Add(new SqlParameter("@BADGEID", Int32.Parse(txtEmpNum.Text)));

                int intNonEmployeeUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmployeeNoStartDateNoActive", nonEmpParameters);

                //Update Request'
                Collection<SqlParameter> effectParameters = new Collection<SqlParameter>();
                effectParameters.Add(new SqlParameter("@NewDate", txtDate.Text));
                effectParameters.Add(new SqlParameter("@RequestID", Int32.Parse(lblReqID.Text)));

                int intEffDateUpdated = SQLUtility.SqlExecuteNonQueryCount("sp_UpdateNonEmpRequestEffectiveDate", effectParameters);

                SendNotification(this.GetRequestID(lblReqID.Text));
            }

            //Set Request Type'
            ddlType.SelectedIndex = 0;
            ddlType_Change(sender, e);
            //oOraConn.Close();
            //END IF'
        }


        //-------------------------SubmitForm (INFORMATION IS SAVED TO THE DB)-----------------------------'
        protected void SubmitForm(object sender, EventArgs e)
        {
            Debug.WriteLine("submitForm");
            //Declare Variables'	
          //  int objReqID, sqlInsert;
            int intReqID, intBadgeID;
            int intCompID = 0;

            //oOraConn.Open();
            ////Generate Request ID'
            //strRead = "select max(RequestID) from pwdreset.NonEmpRequest";
            //cmd = new OleDbCommand(strRead, oOraConn);
            //objReqID = cmd.ExecuteScalar();
            //if ((object.ReferenceEquals(objReqID, DBNull.Value)))
            //{
            //    intReqID = 1;
            //}
            //else
            //{
            //    intReqID = Convert.ToInt32(objReqID) + 1;
            //}

            FixDate(sender, e);

            //If company is set to Other - Generate Company ID and insert company into DB'
            if (IsOtherCompany(ddlCompany.SelectedValue))
            {
                Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

                SqlParameter SQLID = new SqlParameter("@CompanyID", 0);
                SQLID.Direction = ParameterDirection.Output;
                myParameters.Add(SQLID);

                myParameters.Add(new SqlParameter("@CompanyName", txtOtherComp.Text));
                SqlParameterCollection insertParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertCompany", myParameters);

                if (insertParameteres["@CompanyID"] != null)
                {
                    intCompID = Convert.ToInt32(insertParameteres["@CompanyID"].Value.ToString());
                }
            }
            else
            {
                intCompID = Convert.ToInt32(ddlCompany.SelectedValue);
            }

            //If Request Type = NEW Temp/Contractor'
            if ((ddlType.SelectedIndex == 1))
            {
                Collection<SqlParameter> myNonEmployeeParameters = new Collection<SqlParameter>();

                SqlParameter SQLBadgeID = new SqlParameter("@BadgeID", 0);
                SQLBadgeID.Direction = ParameterDirection.Output;
                myNonEmployeeParameters.Add(SQLBadgeID);

                myNonEmployeeParameters.Add(new SqlParameter("@FName", txtFName.Text.Trim().ToUpper()));
                myNonEmployeeParameters.Add(new SqlParameter("@LName", txtLName.Text.Trim().ToUpper()));
                myNonEmployeeParameters.Add(new SqlParameter("@StartDate", txtDate.Text));
                myNonEmployeeParameters.Add(new SqlParameter("@JobTitle", txtJob.Text));
                myNonEmployeeParameters.Add(new SqlParameter("@Location", txtLocation.Text));

                myNonEmployeeParameters.Add(new SqlParameter("@SupName", txtSupName.Text));
                myNonEmployeeParameters.Add(new SqlParameter("@SupEmail", txtSupEmail.Text));
                myNonEmployeeParameters.Add(new SqlParameter("@Comments", txtNonEmployeeComments.Text));
                myNonEmployeeParameters.Add(new SqlParameter("@CostCenter", Int32.Parse(txtCC.Text)));
                myNonEmployeeParameters.Add(new SqlParameter("@CompanyID", intCompID));

                if (IsTempAgency(ddlCompany.SelectedValue))
                {
                    myNonEmployeeParameters.Add(new SqlParameter("@Active", "Y"));
                }
                else
                {
                    myNonEmployeeParameters.Add(new SqlParameter("@Active", "N"));
                }

                SqlParameterCollection insertNonEmployeeParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertNonEmployee", myNonEmployeeParameters);
                intBadgeID = Convert.ToInt32(insertNonEmployeeParameteres["@BadgeID"].Value.ToString());

                //INSERT NEW REQUEST INFO'
                Collection<SqlParameter> myNonEmpRequestParameters = new Collection<SqlParameter>();

                SqlParameter SQLID = new SqlParameter("@RequestID", 0);
                SQLID.Direction = ParameterDirection.Output;
                myNonEmpRequestParameters.Add(SQLID);

                myNonEmpRequestParameters.Add(new SqlParameter("@RequestDate", lblDate.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@RequestType", ddlType.SelectedValue));
                myNonEmpRequestParameters.Add(new SqlParameter("@BadgeID", intBadgeID));
                myNonEmpRequestParameters.Add(new SqlParameter("@Rep", lblUserID.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@EligibleRtn", string.Empty));
                myNonEmpRequestParameters.Add(new SqlParameter("@EffectiveDate", txtDate.Text));

                //If Peoplemark - approve column is empty'
                if (IsTempAgency(ddlCompany.SelectedValue))
                {
                    myNonEmpRequestParameters.Add(new SqlParameter("@Approve", string.Empty));
                }
                else
                {
                    //Not Peoplemark - approval = No'
                    myNonEmpRequestParameters.Add(new SqlParameter("@Approve", "N"));
                }

                SqlParameterCollection insertNonEmpRequestParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertNonEmpRequest", myNonEmpRequestParameters);
                intReqID = Convert.ToInt32(insertNonEmpRequestParameteres["@RequestID"].Value.ToString());

                SendNotification(intReqID);

                //If Request Type = De-activate'
            }
            else if ((ddlType.SelectedIndex == 3))
            {
                //INSERT NEW REQUEST INFO'
                Collection<SqlParameter> myNonEmpRequestParameters = new Collection<SqlParameter>();
                SqlParameter SQLID = new SqlParameter("@RequestID", 0);
                SQLID.Direction = ParameterDirection.Output;
                myNonEmpRequestParameters.Add(SQLID);

                myNonEmpRequestParameters.Add(new SqlParameter("@RequestDate", lblDate.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@RequestType", ddlType.SelectedValue));
                myNonEmpRequestParameters.Add(new SqlParameter("@BadgeID", Int32.Parse(txtEmpNum.Text)));
                myNonEmpRequestParameters.Add(new SqlParameter("@Rep", lblUserID.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@EligibleRtn", ddlReturn.SelectedValue));
                myNonEmpRequestParameters.Add(new SqlParameter("@EffectiveDate", txtDate.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@Approve", string.Empty));
 
                SqlParameterCollection insertNonEmpRequestParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertNonEmpRequest", myNonEmpRequestParameters);
                intReqID = Convert.ToInt32(insertNonEmpRequestParameteres["@RequestID"].Value.ToString());


                Collection<SqlParameter> myNonEmployeeUpdateParameters = new Collection<SqlParameter>();
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@BadgeID", Int32.Parse(txtEmpNum.Text)));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@FName", txtFName.Text.Trim().ToUpper()));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@LName", txtLName.Text.Trim().ToUpper()));
          //      myNonEmployeeUpdateParameters.Add(new SqlParameter("@StartDate", txtDate.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@JobTitle", txtJob.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@Location", txtLocation.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@SupName", txtSupName.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@SupEmail", txtSupEmail.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@Comments", txtNonEmployeeComments.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@CostCenter", Int32.Parse(txtCC.Text)));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@CompanyID", intCompID));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@Active", "N"));

                SqlParameterCollection updateNonEmployeeParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_UpdateNonEmployeeNoStartDate", myNonEmployeeUpdateParameters);

                SendNotification(intReqID);

                //If Request Type = Update/Activate'
            }
            else
            {
                //INSERT NEW REQUEST INFO'
                Collection<SqlParameter> myNonEmpRequestParameters = new Collection<SqlParameter>();
                SqlParameter SQLID = new SqlParameter("@RequestID", 0);
                SQLID.Direction = ParameterDirection.Output;
                myNonEmpRequestParameters.Add(SQLID);

                myNonEmpRequestParameters.Add(new SqlParameter("@RequestDate", lblDate.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@RequestType", ddlType.SelectedValue));
                myNonEmpRequestParameters.Add(new SqlParameter("@BadgeID", Int32.Parse(txtEmpNum.Text)));
                myNonEmpRequestParameters.Add(new SqlParameter("@Rep", lblUserID.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@EligibleRtn", string.Empty));
                myNonEmpRequestParameters.Add(new SqlParameter("@EffectiveDate", txtDate.Text));
                myNonEmpRequestParameters.Add(new SqlParameter("@Approve", string.Empty));

                SqlParameterCollection insertNonEmpRequestParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_InsertNonEmpRequest", myNonEmpRequestParameters);
                intReqID = Convert.ToInt32(insertNonEmpRequestParameteres["@RequestID"].Value.ToString());

                //UPDATE EMPLOYEE INFO'		
                Collection<SqlParameter> myNonEmployeeUpdateParameters = new Collection<SqlParameter>();
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@BadgeID", Int32.Parse(txtEmpNum.Text)));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@FName", txtFName.Text.Trim().ToUpper()));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@LName", txtLName.Text.Trim().ToUpper()));
                //      myNonEmployeeUpdateParameters.Add(new SqlParameter("@StartDate", txtDate.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@JobTitle", txtJob.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@Location", txtLocation.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@SupName", txtSupName.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@SupEmail", txtSupEmail.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@Comments", txtNonEmployeeComments.Text));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@CostCenter", Int32.Parse(txtCC.Text)));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@CompanyID", intCompID));
                myNonEmployeeUpdateParameters.Add(new SqlParameter("@Active", "Y"));

                SqlParameterCollection updateNonEmployeeParameteres = SQLUtility.SqlExecuteNonQueryTransaction("sp_UpdateNonEmployeeNoStartDate", myNonEmployeeUpdateParameters);

                SendNotification(intReqID);
            }

            lblSearchResult.Text = "Thank you. Notification has been distributed/submitted for approval.";
            txtSearch.Text = "";
            lblSearchResult.Visible = true;
            tblDetails.Visible = false;

            //Set Request Type'
            ddlType.SelectedIndex = 0;
            ddlType_Change(sender, e);
            //Close DB Connection'
            //oOraConn.Close();

        } //// end SubmitForm



        //'---------------------btnSearch_Click (SEARCH BY EMPLOYEE NUMBER)-------------------------'
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("In btnSearch_Click");

            if (txtSearch.Text != "")
            {
                lblSearchResult.Text = "";
                lblSearchResult.Visible = false;
                btnHelpFind.Visible = true;
                tblFindEmpNum.Visible = false;
                //Populate Form'	
                //oOraConn.Open();
                if (ddlType.SelectedIndex != 4)
                {
                    Debug.WriteLine("In ddlType.SelectedIndex != 4");
                    //Search and pre-populate Update/De-activate Info'
                    trowSearchResult.Visible = false;
                    if (ddlType.SelectedIndex == 3)
                    {
                        lblDateType.Text = "End";
                    }
                    else
                    {
                        if (ddlType.SelectedIndex == 2)
                            lblDateType.Text = "Transfer";
                    }

                    this.SearchTransTerm(sender, e);
                }
                else
                {
                    ////If Revision'
                    //strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (NonEmployee.BadgeID = " + txtSearch.Text + " and pwdreset.NonEmpRequest.BadgeID=pwdreset.NonEmployee.BadgeID and pwdreset.NonEmployee.CompanyID = pwdreset.Company.CompanyID) Order By pwdreset.NonEmpRequest.RequestDate DESC, pwdreset.NonEmpRequest.RequestType";
                    //cmd = new OleDbCommand(strRead, oOraConn);
                    //rdrEmployee = cmd.ExecuteReader();

                    string sQuery = "SELECT * FROM NonEmployee, NonEmpRequest, Company WHERE NonEmpRequest.BadgeID = NonEmployee.BadgeID AND NonEmployee.CompanyID = Company.CompanyID ";
                    sQuery += "AND NonEmployee.BadgeID = " + txtSearch.Text + " ORDER BY NonEmpRequest.RequestDate DESC, NonEmpRequest.RequestType";
                    DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);
                  //  if (rdrEmployee.HasRows)
                    if (dt.Rows.Count > 0)
                    {
                        dgSearchRequest.DataSource = dt;
                        dgSearchRequest.DataBind();
                        tblFindEmpNum.Visible = true;
                        trowSearchResult.Visible = true;
                        dgSearchRequest.Visible = true;
                        btnSearchSelectedRequest.Visible = true;
                        tblDetails.Visible = false;

                        trowRequest.Visible = false;
                        //lblReqType.Visible = false;
                        dgSearchResults.Visible = false;
                        lblSearchResult.Visible = false;
                        btnSearchSelected.Visible = false;
                    }
                    else
                    {
                        ShowError(sender, e);
                        trowSearchResult.Visible = false;
                        trowRequest.Visible = false;
                        //lblReqType.Visible = false;
                        btnClear_Click(sender, e);
                    }
                    //    rdrEmployee.Close();
                }
                //oOraConn.Close();
            }
            else
            {
                lblSearchResult.Text = "Please enter a Badge ID or click Help Find Badge ID button.";
                lblSearchResult.Visible = true;
            }
        }

        //'----------------------------SearchTransTerm(Search for Transfer/Term Information)-------------------'
        protected void SearchTransTerm(object sender, EventArgs e)
        {
            Debug.WriteLine("In SearchTransTerm");
            //Hide Request Information used in Revision purpose'
            trowRequest.Visible = false;
            //lblReqType.Visible = false;

            //strRead = "select * from pwdreset.NonEmployee where BadgeID=" + txtSearch.Text;
            //cmd = new OleDbCommand(strRead, oOraConn);
            //rdrEmployee = cmd.ExecuteReader();

            string sQuery = "select * from NonEmployee where BadgeID=" + txtSearch.Text;
            DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);

            if(dt.Rows.Count > 0)
            //if (rdrEmployee.HasRows)
            {
                tblDetails.Visible = true;
                lblSearchResult.Visible = false;
                foreach(DataRow r in dt.Rows)
       //         while (rdrEmployee.Read())
                {
                    txtFName.Text = r["FNAME"].ToString();
                    txtLName.Text = r["LNAME"].ToString();
                    txtEmpNum.Text = r["BadgeID"].ToString();
                    txtCC.Text = r["CostCenter"].ToString();
                    txtJob.Text = r["JOBTITLE"].ToString();
                    ddlCompany.SelectedValue = r["COMPANYID"].ToString();
                    txtSupName.Text = r["SUPNAME"].ToString();
                    if (r["SUPEmail"].ToString() == "Null")
                        txtSupEmail.Text = "";
                    else
                        txtSupEmail.Text = r["SUPEmail"].ToString();

                    if (r["Location"].ToString() == "Null")
                        txtLocation.Text = "";
                    else
                        txtLocation.Text = r["LOCATION"].ToString();

                    if (r["COMMENTS"].ToString() == "Null")
                        txtNonEmployeeComments.Text = "";
                    else
                        txtNonEmployeeComments.Text = r["COMMENTS"].ToString();

                }
                //rdrEmployee.Close();

                if (ddlType.SelectedIndex == 3)
                    tblReturn.Visible = true;
                else
                    tblReturn.Visible = false;

                tblDetails.Visible = true;

            }
            else
            {
                //Record with this employee id does not exist'
                ShowError(sender, e);
                btnClear_Click(sender, e);
                txtEmpNum.Text = txtSearch.Text;
            }
        }


        //------------------------btnFindEmpNum_Click (SEARCH FOR EMP.# BY First Name, Last Name)---------'
        protected void btnFindEmpNum_Click(object sender, EventArgs e)
        {
            //oOraConn.Open();
            object strLName, strFName;
            strLName = txtSearchLName.Text.ToUpper().Replace("'", "''");
            strFName = txtSearchFName.Text.ToUpper().Replace("'", "''");
            strFName = "%" + strFName + "%";

            //If NOT a revision'
            if ((ddlType.SelectedValue != "Revision"))
            {
                //strRead = "select * from pwdreset.NonEmployee where (LName = :LNAME and FNAME LIKE :FNAME)";
                //cmd = new OleDbCommand(strRead, oOraConn);
                //cmd.Parameters.Add(new OleDbParameter("LNAME", strLName));
                //cmd.Parameters.Add(new OleDbParameter("FNAME", strFName));
                //rdrEmployee = cmd.ExecuteReader();

                string sQuery = "select * from NonEmployee where LName = '" + strLName + "' AND FNAME LIKE '" + strFName + "' ";
                DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);

                if( dt.Rows.Count > 0 )
              //  if ((rdrEmployee.HasRows))
                {
                    //dgSearchResults.DataSource = rdrEmployee;
                    dgSearchResults.DataSource = dt;
                    dgSearchResults.DataBind();
                    trowSearchResult.Visible = true;
                    dgSearchResults.Visible = true;
                    btnSearchSelected.Visible = true;
                    dgSearchRequest.Visible = false;
                    lblSearchResult.Visible = false;
                    btnSearchSelectedRequest.Visible = false;
                }
                else
                {
                    btnClear_Click(sender, e);
                    txtEmpNum.Text = txtSearch.Text;
                }
            }
            else
            {
                //If Revision'
                //strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (pwdreset.NonEmployee.LNAME=:LName and pwdreset.NonEmployee.FNAME LIKE :FNAME and pwdreset.NonEmployee.BadgeID=pwdreset.NonEmpRequest.BadgeID AND pwdreset.Company.CompanyID=pwdreset.NonEmployee.CompanyID)";
                //cmd = new OleDbCommand(strRead, oOraConn);
                //cmd.Parameters.Add(new OleDbParameter("LNAME", strLName));
                //cmd.Parameters.Add(new OleDbParameter("FNAME", strFName));
                //rdrEmployee = cmd.ExecuteReader();

                string sQuery = "select * from NonEmployee, NonEmpRequest, Company WHERE NonEmployee.BadgeID=NonEmpRequest.BadgeID AND Company.CompanyID=NonEmployee.CompanyID AND NonEmployee.LNAME='" + strLName + "' AND NonEmployee.FNAME LIKE '" + strFName + "' ";
                DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);

                if(dt.Rows.Count > 0)
             //   if ((rdrEmployee.HasRows))
                {
                    dgSearchRequest.DataSource = dt;
                    dgSearchRequest.DataBind();
                    tblDetails.Visible = true;
                    trowSearchResult.Visible = true;
                    dgSearchRequest.Visible = true;
                    btnSearchSelectedRequest.Visible = true;
                    dgSearchResults.Visible = false;
                    lblSearchResult.Visible = false;
                    btnSearchSelected.Visible = false;
                }
                else
                {
                    ShowError(sender, e);
                    trowSearchResult.Visible = false;
                    btnClear_Click(sender, e);
                }
                tblDetails.Visible = false;
            }
            //rdrEmployee.Close();
            //oOraConn.Close();
            //tblDetails.Visible = false	'

            //Clear Search Fields'
            txtSearchFName.Text = "";
            txtSearchLName.Text = "";
            txtSearch.Text = "";
        }

        //------------------------btnSearchSelected_Click (SEARCH BY CHECKED EMPLOYEE NUMBER)---------------'
        protected void btnSearchSelected_Click(object sender, EventArgs e)
        {
            //Make sure that at least one box is checked'	  
            foreach (DataGridItem dgItem in dgSearchResults.Items)
            {
                //Find controls of type Checkbox'
                CheckBox chkSelection = dgItem.FindControl("chkSelection") as CheckBox;
                if (chkSelection.Checked)
                {
                    //Find Employee ID in that row'
                    txtSearch.Text = ((Label)dgItem.FindControl("BADGEID")).Text;
                    tblFindEmpNum.Visible = false;
                    trowSearchResult.Visible = false;
                }
            }
            if ((txtSearch.Text != ""))
            {
                lblSearchResult.Visible = false;
                btnSearch_Click(sender, e);
            }
            else
            {
                lblSearchResult.Text = "Select a record and try again.";
                lblSearchResult.Visible = true;
            }
        }



        //------------------------btnSearchSelectedRequest_Click (SEARCH BY CHECKED R)---------------'
        protected void btnSearchSelectedRequest_Click(object sender, EventArgs e)
        {
            tblOtherCompany.Visible = false;
            //Make sure that at least one box is checked'	  
            object strRequestID;

            tblDetails.Visible = true;
            trowRequest.Visible = true;
            //lblReqType.Visible = false;
            lblSearchResult.Visible = false;

    //        oOraConn.Open();
            foreach (DataGridItem dgItem in dgSearchRequest.Items)
            {
                //Find controls of type Checkbox'
                CheckBox chkSelection = dgItem.FindControl("chkSelection") as CheckBox;

                if (chkSelection.Checked)
                {
                    //Find REQUESTID in that row; Retrieve Info from HRRequest Table'
                    strRequestID = ((Label)dgItem.FindControl("RequestID")).Text;
                    //strRead = "select * from pwdreset.NonEmpRequest where NonEmpRequest.RequestID=" + strRequestID;
                    //cmd = new OleDbCommand(strRead, oOraConn);
                    //rdrEmployee = cmd.ExecuteReader();

                    string sQuery = "select REQUESTID, convert(varchar, REQUESTDATE, 101) as REQUESTDATE, REQUESTTYPE, BADGEID, REP, EFFECTIVEDATE, ELIGIBLERTN, APPROVE from NonEmpRequest ";
                    sQuery += "where NonEmpRequest.RequestID=" + strRequestID;
                    DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);

                    foreach(DataRow r in dt.Rows)
                    //while ((rdrEmployee.Read()))
                    {
                        lblReqDate.Text = String.Format("{0:d}", r["RequestDate"].ToString());
                        lblReqType.Text = r["RequestType"].ToString();
                        lblReqID.Text = r["RequestID"].ToString();
                        if (r["RequestType"].ToString() == "Transfer")
                        {
                            txtDate.Text = String.Format("{0:d}", r["EFFECTIVEDATE"].ToString());
                        }
                        else if ((r["RequestType"].ToString() == "Terminate"))
                        {
                            txtDate.Text = String.Format("{0:d}", r["EFFECTIVEDATE"].ToString());
                            ddlReturn.SelectedValue = r["EligibleRtn"].ToString();
                        }
                    }
                    if ((lblReqType.Text == "Terminate"))
                    {
                        tblReturn.Visible = true;
                        lblDateType.Text = "Termination";
                    }
                    else if ((lblReqType.Text == "Add"))
                    {
                        lblDateType.Text = "Start";
                        tblReturn.Visible = false;
                    }
                    else
                    {
                        lblDateType.Text = "Transfer";
                        tblReturn.Visible = false;
                    }
                    //---FIND EMP NUM in that row; Retrieve Info from Employee Table----'
                    strEmpNum = ((Label)dgItem.FindControl("BADGEID")).Text;
                    //strRead = "select * from pwdreset.NonEmployee where BADGEID='" + strEmpNum + "'";
                    //cmd = new OleDbCommand(strRead, oOraConn);
                    //rdrEmployee = cmd.ExecuteReader();

                    string sQuery2 = "select * from NonEmployee where BADGEID='" + strEmpNum + "'";
                    DataTable dt2 = SQLUtility.SqlExecuteDynamicQuery(sQuery2);

                    foreach (DataRow r in dt2.Rows)
//                   while ((rdrEmployee.Read()))
                    {
                        if ((lblReqType.Text == "New"))
                        {
                            txtDate.Text = r["StartDate"].ToString();
                        }
                        txtFName.Text = r["FNAME"].ToString();
                        txtLName.Text = r["LNAME"].ToString();
                        txtEmpNum.Text = r["BADGEID"].ToString();
                        ddlCompany.SelectedValue = r["CompanyID"].ToString();
                        txtCC.Text = r["CostCenter"].ToString();
                        txtJob.Text = r["JOBTITLE"].ToString();
                        if ((r["Location"].ToString() == "Null"))
                        {
                            txtLocation.Text = "";
                        }
                        else
                        {
                            txtLocation.Text = r["LOCATION"].ToString();
                        }

                        if (r["SUPEmail"].ToString() == "Null")
                        {
                            txtSupEmail.Text = "";
                        }
                        else
                        {
                            txtSupEmail.Text = r["SUPEmail"].ToString();
                        }
                        if (r["COMMENTS"].ToString() == "Null")
                        {
                            txtNonEmployeeComments.Text = "";
                        }
                        else
                        {
                            txtNonEmployeeComments.Text = r["COMMENTS"].ToString();
                        }
                        txtSupName.Text = r["SUPNAME"].ToString();
                    }
                }
                else
                {
                    //No Record Selected'
                }
            }
   //         oOraConn.Close();


            Debug.WriteLine("lblReqType: " + lblReqType.Text);
        }

        //'--------------------------btnHelpFind_Click (LOOK FOR EMP.NUMBER, SHOW SEARCH BAR)--------'
        protected void btnHelpFind_Click(object sender, EventArgs e)
        {
            tblFindEmpNum.Visible = true;
            trowSearchResult.Visible = false;
            btnHelpFind.Visible = false;
            lblSearchResult.Visible = false;
        }

        //'-------------------------btnCalendar_Click (SHOW CALENDAR)---------------------------------'
        protected void btnCalendar_Click(object sender, EventArgs e)
        {
            trowCalendar.Visible = true;
            tblDetails.Visible = true;
        }

        //'-------------------------UpdateMonth (UPDATE MONTH IN CALENDAR)---------------------------------'
        protected void UpdateMonth(object sender, MonthChangedEventArgs e)
        {
            tblDetails.Visible = true;
            trowCalendar.Visible = true;
        }

        //'-------------------------UpdateDate (INSERT SELECTED DATE IN FORM)---------------------------------'
        protected void UpdateDate(object sender, EventArgs e)
        {
            txtDate.Text = cldDate.SelectedDate.ToShortDateString();
            tblDetails.Visible = true;
            trowCalendar.Visible = false;
        }

        //'-------------------------btnReportTemp_Click(REDIRECT USER TO REPORTS PAGE)--------------------------'
        protected void btnReportTemp_Click(object sender, EventArgs e)
        {
            string strUrl = "ReportsTemp.aspx";
            Response.Redirect(strUrl);
        }

        //'-------------------------btnReportContract_Click(REDIRECT USER TO REPORTS PAGE)--------------------------'
        protected void btnReportContract_Click(object sender, EventArgs e)
        {
            string strURL = "ReportsContract.aspx";
            Response.Redirect(strURL);
        }

        //'-------------------------dgSearchResults_Data(HIDE NULL VALUES IN dgSearchResults)----------------'
        protected void dgSearchResults_Data(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
                if (strLocation == "Null")
                    e.Item.Cells[7].Text = "";

                if (strSupEmail == "Null")
                    e.Item.Cells[10].Text = "";
            }
        }


        //'-------------------------dgSearchRequest_Data(HIDE NULL VALUES IN dgSearchRequest)----------------'
        protected void dgSearchRequest_Data(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                strReq = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ReqNum"));
                strSupEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "SupEmail"));
                if (strLocation == "Null")
                    e.Item.Cells[8].Text = "";

                if (strReq == "Null")
                    e.Item.Cells[9].Text = "";

                if (strSupEmail == "Null")
                    e.Item.Cells[12].Text = "";
            }
        }

        //'-------------------------dgNHTerm_Data(HIDE NULL VALUES IN dgNHTerm)-----------------------'
        protected void dgNHTerm_Data(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strRequestType = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RequestType"));
                strReq = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ReqNum"));
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                if (strRequestType == "New Hire")
                    e.Item.Cells[6].Text = "";
                else
                    e.Item.Cells[5].Text = "";

                if (strReq == "Null")
                    e.Item.Cells[9].Text = "";

                if (strLocation == "Null")
                    e.Item.Cells[10].Text = "";
            }
        }




        //'-------------------------dgTrans_Data(HIDE NULL VALUES IN dgTrans)--------------------------'
        protected void dgTrans_Data(object sender, DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                strReq = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ReqNum"));
                strLocation = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Location"));
                if (strReq == null)
                {
                    e.Item.Cells[7].Text = "";
                }
                if (strLocation == "Null")
                {
                    e.Item.Cells[8].Text = "";
                }
            }
        }

        //'---------------------FixDate (Required to performed Oracle date operations)----'
        private void FixDate(object sender, EventArgs e)
        {
            //string fixDate = "ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/YYYY'";
            //cmd = new OleDbCommand(fixDate, oOraConn);
            //cmd.ExecuteScalar();
        }

        //'-----------------------------btnClear_Click (CLEAR FORM CONTENTS)-----------------'
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtDate.Text = "";
            txtCC.Text = "0000000";
            txtJob.Text = "";
            ddlReturn.SelectedIndex = 0;
            txtLocation.Text = "";
            txtSupName.Text = "";
            txtSupEmail.Text = "";
            txtEmailComments.Text = "";
            txtNonEmployeeComments.Text = "";
            tblReturn.Visible = false;
            txtOtherComp.Text = "";
            ddlCompany.SelectedIndex = 0;
        }


        //'-----------------------------SendNotification(Send Notification Upon Form Submission)-----'
        private void SendNotification(int intReqID)
        {
            string strLocation;
            string strReturn;
            string strCompany = string.Empty;
            string strNonEmployeeComments;
            string strComments = "<br><b>Comments: </b>" + txtEmailComments.Text;
            string strCompanyID = "0";
            string dataGridHTML = string.Empty;

            //oOraConn.Open()'
            // if NOT Terminate
            if (ddlType.SelectedIndex != 3)
            {
                //strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (pwdreset.NonEmployee.BadgeID = pwdreset.NonEmpRequest.BadgeID and pwdreset.NonEmpRequest.RequestID=" + intReqID.ToString() + " and pwdreset.Company.CompanyID = pwdreset.NonEmployee.CompanyID)";
                //cmd = new OleDbCommand(strRead, oOraConn);
                //rdrEmployee = cmd.ExecuteReader();


                string sQuery = "select * from NonEmployee, NonEmpRequest, Company where (NonEmployee.BadgeID = NonEmpRequest.BadgeID and NonEmpRequest.RequestID=" + intReqID.ToString() + " and Company.CompanyID = NonEmployee.CompanyID)";
                DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);

                foreach(DataRow r in dt.Rows)
 //               while (rdrEmployee.Read())
                {
                    strCompany = r["CompanyName"].ToString();
                    strCompanyID = r["CompanyID"].ToString();
                    strLocation = r["Location"].ToString();
                    if (strLocation == "Null")
                        strLocation = "";

                    if (Convert.ToString(r["COMMENTS"]) == "Null")
                        strNonEmployeeComments = "";
                    else
                        strNonEmployeeComments = Convert.ToString(r["COMMENTS"]);


                    strReturn = r["EligibleRtn"].ToString();

                    strEmployee = r["LName"].ToString().Trim().ToUpper() + ", " + r["FName"].ToString().Trim().ToUpper();

                    dataGridHTML = "<tr>";
                    dataGridHTML += "<td><b>Request Type: </b></td><td>" + r["RequestType"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Employee Name: </b></td><td>" + strEmployee + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Badge ID: </b></td><td>" + r["BadgeID"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Effective Date: </b></td><td>" + String.Format("{0:d}", r["EFFECTIVEDATE"]) + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Cost Center: </b></td><td>" + r["CostCenter"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Job Title: </b></td><td>" + r["JobTitle"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Company: </b></td><td>" + strCompany + "</td>";
                    dataGridHTML += "<td><b>Location: </b></td><td>" + strLocation + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Supervisor: </b></td><td>" + r["SUPNAME"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Supervisor Email: </b></td><td>" + r["SUPEMAIL"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td valign='top'><b>Temp/Contractor Comments: </b></td><td colspan='3' valign='top'>" + strNonEmployeeComments.Replace("\r", "<br>") + "</td>";
                    dataGridHTML += "</tr>";
                    dataGridHTML += "</table>" + strComments;

                    strEmployee += " (" + String.Format("{0:d}", r["EFFECTIVEDATE"]) + ")";
                    strCCSupervisor = r["SupEmail"].ToString();
                    if (strCCSupervisor == "Null")
                        strCCSupervisor = "";

                }
                //rdrEmployee.Close();
            }
            else
            {
                //        Else 'IF Terminate
                //strRead = "select * from pwdreset.NonEmployee, pwdreset.NonEmpRequest, pwdreset.Company where (pwdreset.NonEmployee.BadgeID = pwdreset.NonEmpRequest.BadgeID and pwdreset.NonEmpRequest.RequestID=" + intReqID.ToString() + " and pwdreset.Company.CompanyID = pwdreset.NonEmployee.CompanyID)";
                //cmd = new OleDbCommand(strRead, oOraConn);
                //rdrEmployee = cmd.ExecuteReader();

                string sQuery = "select * from NonEmployee, NonEmpRequest, Company where (NonEmployee.BadgeID = NonEmpRequest.BadgeID and NonEmpRequest.RequestID=" + intReqID.ToString() + " and Company.CompanyID = NonEmployee.CompanyID)";
                DataTable dt = SQLUtility.SqlExecuteDynamicQuery(sQuery);


                foreach(DataRow r in dt.Rows)
           //     while (rdrEmployee.Read())
                {
                    strCompany = r["CompanyName"].ToString();
                    strCompanyID = r["CompanyID"].ToString();
                    strLocation = r["Location"].ToString();
                    if (strLocation == "Null")
                        strLocation = "";

                    strReturn = r["EligibleRtn"].ToString();
                    if (strReturn == "Y")
                        strReturn = "Yes";
                    else
                        strReturn = "No";

                    if (r["COMMENTS"].ToString() == "Null")
                        strNonEmployeeComments = "";
                    else
                        strNonEmployeeComments = r["COMMENTS"].ToString();

                    strEmployee = r["LName"].ToString().Trim().ToUpper() + ", " + r["FName"].ToString().Trim().ToUpper();

                    dataGridHTML = "<tr>";
                    dataGridHTML += "<td><b>Request Type: </b></td><td>" + r["RequestType"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Employee Name: </b></td><td>" + strEmployee + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Badge ID: </b></td><td>" + r["BadgeID"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Effective Date: </b></td><td>" + String.Format("{0:d}", r["EFFECTIVEDATE"]) + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Cost Center: </b></td><td>" + r["CostCenter"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Job Title: </b></td><td>" + r["JobTitle"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Company: </b></td><td>" + strCompany + "</td>";
                    dataGridHTML += "<td><b>Location: </b></td><td>" + strLocation + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Supervisor: </b></td><td>" + r["SUPNAME"].ToString() + "</td>";
                    dataGridHTML += "<td><b>Supervisor Email: </b></td><td>" + r["SUPEMAIL"].ToString() + "</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td><b>Eligible to Return:</b></td><td>" + strReturn + "</td>";
                    dataGridHTML += "<td>&nbsp;</td><td>&nbsp;</td>";
                    dataGridHTML += "</tr><tr>";
                    dataGridHTML += "<td valign='top'><b>Temp/Contractor Comments: </b></td><td colspan='3' valign='top'>" + strNonEmployeeComments.Replace("\r", "<br>") + "</td>";
                    dataGridHTML += "</tr>";
                    dataGridHTML += "</table>" + strComments;

                    strEmployee += " (" + String.Format("{0:d}", r["EFFECTIVEDATE"]) + ")";
                    strCCSupervisor = r["SupEmail"].ToString();

                } //// end while
                //rdrEmployee.Close();
            }////        End If	
            //rdrEmployee.Close();
            //oOraConn.Close()'

            SendEmail(ddlType.SelectedValue, intReqID.ToString(), strEmployee, strCompany, strCompanyID, strCCSupervisor, dataGridHTML, lblUserID.Text);
        } //// end sub

        //'-----------------SendEmail (Form and Send an Email--------------------------'
        private void SendEmail(string strRequestType, string intReqID, string strEmployee, string strCompany, string strCompanyID, string strCCSupervisor, string strContents, string strHRRep)
        {
            MailMessage myMail = new MailMessage();

            //Sub SendEmail(strRequestType As String, intReqID As String, strEmployee As String, strCompany As String, strCompanyID As String, strCCSupervisor As String, strContents As String, strHRRep As String)	
            //    Dim myMail, cdoMessage
            //    Dim strEmailSubject, 
            string strToEmail;
            string strBCCEmail = string.Empty;
            string strTermNote;
            string strFromEmail;
            string strEmailBody;
            string strEmailMgrSubject;
            string strSiteURL;
            string strPeoplemarkEmail = ConfigurationManager.AppSettings["TempAgencyEmail"] + "; ";

            string strEmailSubject = strCompany + "- Temp/Contractor " + strRequestType + " Notification: " + strEmployee;

            string strHeader = "<font face='Smith&NephewLF, Arial, sans-serif' size='-1'><b>Temporary Worker/Contractor Notification Submitted By:</b> " + strHRRep + "</font>";
            strContents = "<br><table border='1' style='border-color:#999999;empty-cells:show;font-size:14px;font-family:Smith&NephewLF, Arial, sans-serif' cellpadding='5' cellspacing='0'>" + strContents;
            string strPeoplemarkTerm = "";

            //If Company is Temp Agency'
            if (IsTempAgency(strCompanyID))
            {
                strCCSupervisor = strPeoplemarkEmail + strCCSupervisor;
                strPeoplemarkTerm = ConfigurationManager.AppSettings["TempAgencyEmail"];
            }

            // If Request Type = Terminate'
            if (strRequestType == "Terminate")
            {
                //        'Send Email To Dist. List'
                strToEmail = ConfigurationManager.AppSettings["HrEmail"];
                strFromEmail = ConfigurationManager.AppSettings["StaffEmail"];
                strEmailBody = strHeader + strContents;
                //SendMail(strToEmail, strPeoplemarkTerm, strBCCEmail, strFromEmail, strEmailSubject, strEmailBody);
                Utility.SendEmail(strToEmail, strFromEmail, strPeoplemarkTerm, strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));

                //Send Email to Manager'
                strTermNote = "<font face='Smith&NephewLF, Arial, sans-serif' size='-1'>The following termination request has been processed:<br><br>" + strContents + "</font></table>";
                strTermNote += "<br><font color='#DB6415' size='3'><u><b>As a supervisor, you are responsible for returning temporary worker/contractor's equipment and/or tools <b>within 30 days </b> to avoid replacement equipment charges to your cost center.</u></b></font>";
                strTermNote += "<br><br><strong>Please use the list below to complete the termination process:</strong>";
                strTermNote += "<ol><li>Return <strong>Hardware, Office Phone, FOB</strong> to <strong>eIS Desktop Support</strong> (Remote employees must mail their equipment to eIS Desktop Support)</li>";
                strTermNote += "<li>Return <strong>ID Badge and/or Keys</strong> to <strong>Security</strong></li>";
                strTermNote += "<li>Return <strong>Corporate Credit Card</strong> to <strong>Michelle Smith</strong></li>";
                strTermNote += "</ol></font>";

                strEmailBody = strTermNote;
                strEmailMgrSubject = "Termination Procedures for " + strEmployee;
               // SendMail(strCCSupervisor, "", strBCCEmail, strFromEmail, strEmailMgrSubject, strEmailBody);
                Utility.SendEmail(strCCSupervisor, strFromEmail, "", strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));
            }
            else
            {
                //    'All Transfer or New Hire/Revision/Transfer for TempAgency - No Approval'
                //    ELSEIF ((strRequestType = "Transfer") or (IsTempAgency(strCompanyID))) THEN	
                if ((strRequestType == "Transfer") || (IsTempAgency(strCompanyID)))
                {

                    strToEmail = ConfigurationManager.AppSettings["HrEmail"];
                    strFromEmail = ConfigurationManager.AppSettings["StaffEmail"];
                    strEmailBody = strHeader + strContents;
                   // SendMail(strToEmail, strCCSupervisor, strBCCEmail, strFromEmail, strEmailSubject, strEmailBody);
                    Utility.SendEmail(strToEmail, strFromEmail, strCCSupervisor, strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));

                    //Non-Peoplemark New Hire/Revision - Then Request Approval'
                }
                else
                {

                    strToEmail = ConfigurationManager.AppSettings["ContractApprovalEmail"];
                    strFromEmail = ConfigurationManager.AppSettings["NewContractEmail"];
                    strSiteURL = ConfigurationManager.AppSettings["SiteURL"];
                    strEmailBody = strHeader + strContents + "<br><a href='" + strSiteURL + "/ApproveForm.aspx?ID=" + intReqID + "'><b>Approve Request</b></a>";
               //     SendMail(strToEmail, strCCSupervisor, strBCCEmail, strFromEmail, strEmailSubject, strEmailBody);
                    Utility.SendEmail(strToEmail, strFromEmail, strCCSupervisor, strEmailSubject, strEmailBody, true, ADUtility.GetUserNameOfAppUser(Request));

                    //    'If Request Type = Termination'	
                }

                //Show Message on the form'
                lblSearchResult.Text = "Your notification has been sent to the distribution list.";
                lblSearchResult.Visible = true;
                //tcellSearchResult.Visible = true;
                lblSearchResult.Visible = true;
            }
        }

        private void ShowError(object sender, EventArgs e)
        {
            lblSearchResult.Text = "No records found. Please try again.";
            lblSearchResult.Visible = true;
            tblDetails.Visible = false;
        }

        private bool IsTempAgency(string strCompanyID)
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["TempAgencyID"] == strCompanyID);
        }

        private bool IsOtherCompany(string strCompanyID)
        {
            return Convert.ToBoolean(ConfigurationManager.AppSettings["OtherCompanyID"] == strCompanyID);
        }

        protected void ddlCompany_Change(object sender, EventArgs e)
        {
            if (IsOtherCompany(this.ddlCompany.SelectedValue))
                tblOtherCompany.Visible = true;
            else
                tblOtherCompany.Visible = false;

        }

    }//// end class
}