﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" MasterPageFile="MasterNED.Master" Inherits="NonEmployee.Search" Title="Search" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <div id="FilterRow">
            <table>
                <tr>
                    <td style="width:75%">
                        <div id="Search">
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="12pt" ForeColor="#78BDE8"
                                            Text="Search Criteria:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Status:</b></td>
                                    <td>
                                        <asp:DropDownList ID="ddlStatus" runat="server" Width="175" Font-Size="8pt">
                                        </asp:DropDownList>
                                    </td>
                                    <td><b>Last Name:</b></td>
                                    <td>
                                        <asp:TextBox ID="txtLastName" runat="server" Columns="25" Font-Size="8pt"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>ID:</b></td>
                                    <td>
                                        <asp:TextBox ID="txtNonEmpID" Width="175" runat="server"></asp:TextBox>
                                    </td>
                                    <td><b>Start Date:</b></td>
                                    <td>
                                        <asp:TextBox ID="txtHireDateFrom" runat="server" Columns="10" Font-Size="8pt"></asp:TextBox>
                                        &nbsp;&nbsp;To&nbsp;&nbsp;<asp:TextBox ID="txtHireDateTo" runat="server" Columns="10"
                                            Font-Size="8pt" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Country:</b></td>
                                    <td>
                                        <asp:DropDownList ID="ddlCountry" runat="server" Width="175" Font-Size="8pt"></asp:DropDownList>
                                    </td>
                                    <td><b>Term Date:</b></td>
                                    <td>
                                        <asp:TextBox ID="txtTermDateFrom" runat="server" Columns="10" Font-Size="8pt"></asp:TextBox>
                                        &nbsp;&nbsp;To&nbsp;&nbsp;<asp:TextBox ID="txtTermDateTo" runat="server" Columns="10"
                                            Font-Size="8pt" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Supervisor Name:</b></td>
                                    <td>
                                        <asp:TextBox ID="txtSupervisorName" Width="175" runat="server"></asp:TextBox>
                                    </td>
                                    <td><b>Old User ID:</b></td>
                                    <td>
                                        <asp:TextBox ID="txtOldUserId" Width="57px" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div id="SearchButtons">
                            <table align="center" cellpadding="5">
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnNew" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                                            OnClick="BtnNew_Click" Text="Add New" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnReset" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                                            OnClick="BtnReset_Click" Text="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                                            OnClick="BtnSearch_Click" Text="Search" />
                                    </td>
                                </tr>
                                <tr>
                                    <td> <asp:Label ID="lblResults" runat="server" ForeColor="#FF7300"></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="SearchGrid">
            <table>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvNonEmployee" runat="server" DataKeyNames="NonEmployeeID" AutoGenerateColumns="false"
                            OnPageIndexChanging="GV_PageIndexChanging" Width="860" PagerSettings-Mode="NumericFirstLast"
                            OnRowCommand="GV_RowCommand" AllowPaging="true" PageSize="20" CellPadding="3"
                            CellSpacing="0" BorderWidth="1" AllowSorting="true" OnSorting="GV_Sorting" EmptyDataText="No records returned for the current selection.">
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" SortExpression="NonEmployeeID" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnView" CssClass="ID" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "NonEmployeeID") %>'
                                            runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NonEmployeeID") %>'
                                            CommandName="ViewChange" CausesValidation="False" BorderColor="#CCCCCC"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Left" SortExpression="lastname" DataField="lastname"
                                    HeaderStyle-Width="15%" />
                                <asp:BoundField HeaderText="First Name" HeaderStyle-HorizontalAlign="Left" SortExpression="firstname" DataField="firstname"
                                    HeaderStyle-Width="15%" />
                                <asp:BoundField HeaderText="Start Date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" ItemStyle-CssClass="GridCenterAlign"
                                    SortExpression="HireDate" DataField="HireDate" HeaderStyle-Width="10%" />
                                <asp:BoundField HeaderText="Term Date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" ItemStyle-CssClass="GridCenterAlign"
                                    SortExpression="TermDate" DataField="TermDate" HeaderStyle-Width="10%" />
                                <asp:BoundField HeaderText="Country" HeaderStyle-HorizontalAlign="Left" SortExpression="Countries" DataField="Countries"
                                    HeaderStyle-Width="15%" />
                                <asp:BoundField HeaderText="Status" ItemStyle-CssClass="GridCenterAlign" SortExpression="NonEmployeeStatus" DataField="NonEmployeeStatus"
                                    HeaderStyle-Width="5%" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
     </div>
</asp:Content>
