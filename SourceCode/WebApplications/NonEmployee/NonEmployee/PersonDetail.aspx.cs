﻿// -------------------------------------------------------------
// <copyright file="PersonDetail.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace NonEmployee
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    /// <summary>
    /// This is the Person Detail page.
    /// </summary>
    public partial class PersonDetail : System.Web.UI.Page
    {
        /// <summary>
        /// This is our instance of the utility class.
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// This is the old employee object.
        /// </summary>
        private NonEmployeeEntity myNewNonEmployeeObject;

        /// <summary>
        /// Creates the representative ID table.
        /// </summary>
        /// <returns>The data table.</returns>
        public DataTable CreateRepIDTable()
        {
            DataTable dt = new DataTable();
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("RepID", typeof(string)));
            return dt;
        }


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("WindowsIdentity: " + WindowsIdentity.GetCurrent().Name);

            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
            }

            ////CMB - This is for our previous object.
            if (Session["OldEmployee"] == null)
            {
                Debug.WriteLine("OldEmployee is null");
                this.myNewNonEmployeeObject = new NonEmployeeEntity();
                Session["OldEmployee"] = this.myNewNonEmployeeObject;
            }
            else
            {
                Debug.WriteLine("OldEmployee is NOT null");
                this.myNewNonEmployeeObject = (NonEmployeeEntity)Session["OldEmployee"];
            }

            this.lblUpdateStatus.Text = String.Empty;
            this.lblUpdateStatus2.Text = String.Empty;


            if (!Page.IsPostBack)
            {
                //// if a QueryString Exists use that ID, if not use the ViewState ID
                //// if that doesn't exist redirect to default page
                if (Request.QueryString["NonID"] != null)
                {
                    ViewState["NonID"] = Request.QueryString["NonID"];
                }
                else
                {
                    if (ViewState["NonID"] == null)
                    {
                        ViewState["NonID"] = "0";
                    }
                }

                Debug.WriteLine("ViewState NonID: " + ViewState["NonID"].ToString() + "; IsPostBack: " + Page.IsPostBack.ToString());

                EnterButton.TieButton(this.txtAddress1, this.btnSubmit);
                EnterButton.TieButton(this.txtAddress2, this.btnSubmit);
                EnterButton.TieButton(this.txtCity, this.btnSubmit);
                EnterButton.TieButton(this.txtEmailAddress, this.btnSubmit);
                EnterButton.TieButton(this.txtFirstName, this.btnSubmit);
                //EnterButton.TieButton(this.txtAlternatePhone, this.btnSubmit);
                EnterButton.TieButton(this.txtMiddleName, this.btnSubmit);
   //             EnterButton.TieButton(this.txtNetworkID, this.btnSubmit);
                EnterButton.TieButton(this.txtNickName, this.btnSubmit);
                EnterButton.TieButton(this.txtPreferredPhone, this.btnSubmit);
                EnterButton.TieButton(this.txtRepID, this.btnAdd);
                EnterButton.TieButton(this.txtSupervisorEmployeeID, this.btnSubmit);
                EnterButton.TieButton(this.txtSupervisorName, this.btnSubmit);
                EnterButton.TieButton(this.txtZipCode, this.btnSubmit);
                EnterButton.TieButton(this.txtCompanyName, this.btnSubmit);
                EnterButton.TieButton(this.txtKronosNumber, this.btnSubmit);
                EnterButton.TieButton(this.txtSalesDistrict, this.btnSubmit);

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                this.LoadDropDowns();

                if (ViewState["NonID"].ToString() != "0")
                {
                    //this.myUtility.TblNonEmployee = SqlUtility.SetTable("sp_GetNonEmployeeByID", Int32.Parse(ViewState["NonID"].ToString()));
                    this.LoadData();
                }

                this.myUtility.SqlConn.Close();
                this.SetControls();
                this.SetJobControls();

                if (this.ddlFunction.SelectedItem.Text.ToLower() == "sales")
                {
                    this.SetSalesControls(true);
                }
                else
                {
                    this.SetSalesControls(false);
                }

                this.SetDuplicateControl(false);
                ////CMB - End Change
            } //// end !IsPostBack

            Debug.WriteLine("Old Employee Lastname: " + this.myNewNonEmployeeObject.LastName);

            Session["Utility"] = this.myUtility;
        } //// end Page_Load

                /// <summary>

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            Debug.WriteLine("Status selected = " + ddlStatus.SelectedValue.ToLower());
            ValidatorCollection vals = Page.Validators;
            foreach (IValidator val in vals)
            {

                if (val.ErrorMessage.StartsWith("Sales Rep ID"))
                {
                    val.IsValid = true;
                }

                if (val.ErrorMessage.StartsWith("You must enter a Term Date"))
                {

                    if (ddlStatus.SelectedValue.ToLower() == "t" && txtTermDate.Date == String.Empty)
                    {
                        val.IsValid = false;
                    }
                    else
                    {
                        if (ddlStatus.SelectedValue.ToLower() != "t" && txtTermDate.Date != String.Empty)
                        {
                            val.IsValid = false;
                        }
                        else
                        {
                            val.IsValid = true;
                        }
                    }
                }

                if (val.ErrorMessage.StartsWith("The Term Date must not be earlier"))
                {
                    if (txtHireDate.Date != String.Empty && txtTermDate.Date != String.Empty)
                    {
                        val.IsValid = this.IsDatePrior(txtHireDate.Date, txtTermDate.Date);
                    }
                }
                
                if (val.ErrorMessage.StartsWith("The Job Effective Date must not be earlier"))
                {
                    if (txtHireDate.Date != String.Empty && txtLegacyPositionDate.Date != String.Empty)
                    {
                        val.IsValid = this.IsDatePrior(txtHireDate.Date, this.txtLegacyPositionDate.Date);
                    }
                }

            }

            if (Page.IsValid)
            {
                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = this.myUtility.SqlConn;

                cmd.Parameters.Add(new SqlParameter("@NonEmployeeStatus", this.ddlStatus.SelectedValue));

                if (this.txtHireDate.Date == String.Empty)
                {
                    cmd.Parameters.Add(new SqlParameter("@HireDate", Convert.DBNull));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HireDate", this.txtHireDate.Date));
                }

                if (this.txtTermDate.Date == String.Empty)
                {
                    cmd.Parameters.Add(new SqlParameter("@TermDate", Convert.DBNull));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@TermDate", this.txtTermDate.Date));
                }

                cmd.Parameters.Add(new SqlParameter("@EmployeeTypeCode", this.ddlEmployeeType.SelectedValue));

                cmd.Parameters.Add(new SqlParameter("@LastName", this.txtLastName.Text));
                cmd.Parameters.Add(new SqlParameter("@FirstName", this.txtFirstName.Text));
                cmd.Parameters.Add(new SqlParameter("@MidName", this.txtMiddleName.Text));
                cmd.Parameters.Add(new SqlParameter("@NickName", this.txtNickName.Text));
                cmd.Parameters.Add(new SqlParameter("@SupervisorEmployeeID", this.txtSupervisorEmployeeID.Text));
                cmd.Parameters.Add(new SqlParameter("@SupervisorName", this.txtSupervisorName.Text));
                cmd.Parameters.Add(new SqlParameter("@CostCode", this.ddlCostCenter.Text));
                cmd.Parameters.Add(new SqlParameter("@City", this.txtCity.Text));
                cmd.Parameters.Add(new SqlParameter("@State", this.ddlState.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@ShiftCode ", this.ddlShift.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@PaSubCode", this.ddlPersonnelSubArea.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@PositionName", this.ddlPositionTitle.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@EmailAddress", this.txtEmailAddress.Text));
                cmd.Parameters.Add(new SqlParameter("@OrgCode", this.ddlOrgCode.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@LegacyPosition", this.ddlLegacyPosition.SelectedValue));
                
                if (this.txtCertificationReceived.Date == String.Empty)
                {
                    cmd.Parameters.Add(new SqlParameter("@CertificationReceived", Convert.DBNull));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@CertificationReceived", this.txtCertificationReceived.Date));
                }

                if (this.txtCertificationSent.Date == String.Empty)
                {
                    cmd.Parameters.Add(new SqlParameter("@CertificationSent", Convert.DBNull));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@CertificationSent", this.txtCertificationSent.Date));
                }

                if (this.txtLegacyPositionDate.Date == String.Empty)
                {
                    cmd.Parameters.Add(new SqlParameter("@LegacyPositionDate", Convert.DBNull));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@LegacyPositionDate", this.txtLegacyPositionDate.Date));
                }

                ////CMB - These are some of the new fields.
                cmd.Parameters.Add(new SqlParameter("@OldUserID", this.txtOldUserID.Text));
                cmd.Parameters.Add(new SqlParameter("@ContractOwner", this.txtCompanyName.Text));
                cmd.Parameters.Add(new SqlParameter("@Notes", this.txtNotes.Text));
                cmd.Parameters.Add(new SqlParameter("@KronosNumber", this.txtKronosNumber.Text));

                if(this.ddlFunction.SelectedValue == String.Empty)
                    cmd.Parameters.Add(new SqlParameter("@FunctionID", Convert.DBNull));
                else
                    cmd.Parameters.Add(new SqlParameter("@FunctionID", Convert.ToInt32(this.ddlFunction.SelectedValue)));
                ////CMB - End Changes...

                if (this.ddlFunction.SelectedItem.Text.ToLower() == "sales")
                {
                    cmd.Parameters.Add(new SqlParameter("@SalesRepID", this.CommaDelimitRepID()));
                    cmd.Parameters.Add(new SqlParameter("@SalesRepTypeCode", this.ddlSalesRepType.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@SalesDistrict", this.txtSalesDistrict.Text));
                    cmd.Parameters.Add(new SqlParameter("@RegionCode", this.ddlRegion.SelectedValue));

                    if (ckReHire.Visible == false)
                    {
                        cmd.Parameters.Add(new SqlParameter("@Rehire", String.Empty));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@Rehire", this.ckReHire.SelectedValue));
                    }

                    ////CMB - End Changes...
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@SalesRepID", String.Empty));
                    cmd.Parameters.Add(new SqlParameter("@SalesRepTypeCode", String.Empty));
                    cmd.Parameters.Add(new SqlParameter("@SalesDistrict", String.Empty));
                    cmd.Parameters.Add(new SqlParameter("@RegionCode", String.Empty));

                    if (ckReHire.Visible == false)
                    {
                        cmd.Parameters.Add(new SqlParameter("@Rehire", String.Empty));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@Rehire", this.ckReHire.SelectedValue));
                    }

                    ////CMB - End Changes...
                }

                cmd.Parameters.Add(new SqlParameter("@NEtoFT", this.ddlNEtoFT.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@FTtoNE", this.ddlFTtoNE.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@HcpFacing", this.ddlHCP.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@TPS", this.ddlTPS.SelectedValue));
                if(this.ddlLanguage.SelectedValue == String.Empty)
                    cmd.Parameters.Add(new SqlParameter("@LanguageID", Convert.DBNull));
                else
                    cmd.Parameters.Add(new SqlParameter("@LanguageID", Int32.Parse(this.ddlLanguage.SelectedValue)));


                if (this.ddlBusinessArea.SelectedValue == String.Empty)
                    cmd.Parameters.Add(new SqlParameter("@BusinessAreaID", Convert.DBNull));
                else
                    cmd.Parameters.Add(new SqlParameter("@BusinessAreaID", Int32.Parse(this.ddlBusinessArea.SelectedValue)));


                cmd.Parameters.Add(new SqlParameter("@Country_Code", this.ddlCountry.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@PreferredPhone", this.txtPreferredPhone.Text));
                cmd.Parameters.Add(new SqlParameter("@Street1", this.txtAddress1.Text));
                cmd.Parameters.Add(new SqlParameter("@Street2", this.txtAddress2.Text));
                cmd.Parameters.Add(new SqlParameter("@PostalCode", this.txtZipCode.Text));
                cmd.Parameters.Add(new SqlParameter("@LastModifiedBy", this.myUtility.ADAccount));
                SqlParameter paramID = new SqlParameter("@NonEmployeeID", SqlDbType.Int);
                cmd.Parameters.Add(paramID);

                Debug.WriteLine("My Name: " + this.myUtility.ADAccount + "; ViewState: " + ViewState["NonID"].ToString());
                if (ViewState["NonID"].ToString() != "0")
                {
                    paramID.Direction = ParameterDirection.Input;
                    paramID.Value = Convert.ToInt64(ViewState["NonID"].ToString());
                    cmd.CommandText = "sp_UpdateNonEmployee";
                    this.lblUpdateStatus.Text = "The record was successfully updated.";
                }
                else
                {
                    paramID.Direction = ParameterDirection.Output;
                    cmd.CommandText = "sp_InsertNonEmployee";
                    this.lblUpdateStatus.Text = "The record was successfully created.";
                }

                try
                {
                    if (this.myUtility.SqlConn.State != ConnectionState.Open)
                    {
                        this.myUtility.SqlConn.Open();
                    }

                    NonEmployeeEntity oldNE = this.GetNonEmployeeDBRecord(Int32.Parse(ViewState["NonID"].ToString()));

                    // execute insert or update
                    int rowsUpdated = cmd.ExecuteNonQuery();
                    ViewState["NonID"] = paramID.Value.ToString();

                    NonEmployeeEntity newNE = this.GetNonEmployeeDBRecord(Int32.Parse(ViewState["NonID"].ToString()));

                    this.lblEmployeeID.Text = newNE.NonEmployeeID.ToString();

                    this.SendUpdateNotice(oldNE, newNE);
                    this.myUtility.SqlConn.Close();
                    this.SetControls();
                }
                catch (Exception exc)
                {
                    this.lblUpdateStatus.Text = "The record was NOT saved.";
                    
                    foreach (SqlParameter p in cmd.Parameters)
                    {
                        Debug.WriteLine("Name: " + p.ParameterName + "; Value: " + p.Value.ToString() + "; Size: " + p.Value.ToString().Length.ToString());
                    }

                    Debug.WriteLine(cmd.CommandText + " Error - " + exc.Message);
                    this.myUtility.SqlConn.Close();
                    LogException logExc = new LogException();
                    logExc.HandleException(exc, Request);
                }
            }
        }


        private bool IsDatePrior(string strDate1, string strDate2)
        {
            DateTime date1 = this.GetDate(strDate1);
            DateTime date2 = this.GetDate(strDate2);
            int intCompare = date1.CompareTo(date2);
            Debug.WriteLine("intCompare: " + intCompare.ToString());
            return Convert.ToBoolean(intCompare <= 0);
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("void ddlCountry_SelectedIndexChanged");
            this.LoadCountryDropDowns(this.ddlCountry.SelectedValue);
            this.SetJobControls();
            Debug.WriteLine("Countrycode = " + this.ddlCountry.SelectedValue.ToString());
            //// Checking for sales...
            if (this.ddlFunction.SelectedItem.Text.ToLower() == "sales")
            {
                this.SetSalesControls(true);
                this.LoadSRDDL(this.ddlCountry.SelectedValue);
            }
            else
            {
                this.SetSalesControls(false);
            }
        }


        /// <summary>
        /// Handles the Clear event of the grid view control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Clear(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int indexNum = e.RowIndex;
            Debug.WriteLine("Index: " + e.RowIndex.ToString());

            DataTable dt = this.CreateRepIDTable();
            for (int x = 0; x < this.gvSalesRepID.Rows.Count; x++)
            {
                Debug.WriteLine("Index: " + indexNum.ToString() + "; Row: : " + x.ToString());
                if (x != e.RowIndex)
                {
                    DataRow r = dt.NewRow();
                    r["repID"] = this.gvSalesRepID.Rows[x].Cells[0].Text;
                    dt.Rows.Add(r);
                }
            }

            Debug.WriteLine("Rows: " + dt.Rows.Count.ToString());
            this.BindDT(dt);
        }

        /// <summary>
        /// Handles the Add event of the grid view control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            Debug.WriteLine("GV_Add Key 1: " + this.gvSalesRepID.DataKeys[e.NewEditIndex]["repID"].ToString());
            string strRepID = this.gvSalesRepID.DataKeys[e.NewEditIndex]["repID"].ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Page.Validate();

            ValidatorCollection vals = Page.Validators;
            foreach (IValidator val in vals)
            {
                if (!val.ErrorMessage.StartsWith("Sales Rep ID"))
                {
                    val.IsValid = true;
                }
            }

            if (Page.IsValid)
            {
                DataTable datAddRep = this.LoadRepIDTableGridView();
                this.BindDT(datAddRep);
                this.txtRepID.Text = String.Empty;
            }
        }

        /// <summary>
        /// Handles the Select event of the grid view control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Select(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            Debug.WriteLine("GV_Select Key 1: " + this.gvDuplicates.DataKeys[e.NewEditIndex]["NonEmployeeID"].ToString());
            string strDupID = this.gvDuplicates.DataKeys[e.NewEditIndex]["NonEmployeeID"].ToString();
            Response.Redirect("PersonDetail.aspx?NonID=" + strDupID, false);
        }

        /// <summary>
        /// Handles the TextChanged event of the txtLastName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void TxtLastName_TextChanged(object sender, EventArgs e)
        {
            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            DataTable datDups = SqlUtility.SetTable("sp_GetNonEmployeeByLastName", this.txtLastName.Text);
            this.myUtility.SqlConn.Close();

            if (datDups.Rows.Count > 0)
            {
                this.gvDuplicates.DataSource = datDups;
                this.gvDuplicates.DataBind();
                this.SetDuplicateControl(true);
            }
            else
            {
                this.SetDuplicateControl(false);
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetFocus", "document.getElementById('" + this.txtFirstName.ClientID + "').focus();", true);
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnClose_Click(object sender, EventArgs e)
        {
            this.SetDuplicateControl(false);
        }

        /// <summary>
        /// Gets the string after comparing the valuescompare string.
        /// </summary>
        /// <param name="strOld">The string old.</param>
        /// <param name="strNew">The string new.</param>
        /// <param name="strLabel">The string label.</param>
        /// <returns></returns>
        protected string GetCompareString(string strOld, string strNew, string strLabel)
        {
            string strNewLine = String.Empty;
            if (strOld != strNew)
            {
                strNewLine += "<tr>";
                strNewLine += "<td>" + strLabel + ":</td>";
                strNewLine += "<td>" + strOld + "</td>";
                strNewLine += "<td>" + strNew + "</td>";
                strNewLine += "</tr>";
            }

            return strNewLine;
        }

        /// <summary>Sets the email body.</summary>
        /// <param name="oldEmployee">The old non-employee record.</param>
        /// <param name="newEmployee">The new non-employee record.</param>
        /// <returns>the string with the body of the email message.</returns>
        protected string SetEmailBody(NonEmployeeEntity oldEmployee, NonEmployeeEntity newEmployee)
        {
            string strEmailNotifyMsg = ConfigurationManager.AppSettings["emailNotifyMsg"];
            strEmailNotifyMsg = strEmailNotifyMsg.Replace("Smith and Nephew", "Smith & Nephew");

            string strBody = String.Empty;
            strBody += "<table width=\"700\" border=\"0\">";
            strBody += "<tr>";
            strBody += "<td colspan=\"3\" valign=\"bottom\"><span style=\"font-size:12pt;\">";
            strBody += strEmailNotifyMsg;
            strBody += "</span></td>";
            strBody += "</tr>";
            strBody += "<tr>";
            strBody += "<td colspan=\"3\">&nbsp;</td>";
            strBody += "</tr>";

            strBody += "<tr>";
            strBody += "<td width=\"20%\"><b>Non-EmployeeID:</b></td>";
            strBody += "<td>" + newEmployee.NonEmployeeID.ToString() + "</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "</tr>";
            strBody += "<tr>";
            strBody += "<td><b>Last Name:</b></td>";
            strBody += "<td>" + newEmployee.LastName + "</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "</tr>";
            strBody += "<tr>";
            strBody += "<td><b>First Name:</b></td>";
            strBody += "<td>" + newEmployee.FirstName + "</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "</tr>";
            strBody += "<tr>";
            strBody += "<td><b>Position:</b></td>";
            strBody += "<td>" + newEmployee.PositionName + "</td>";
            strBody += "<td>&nbsp;</td>";
            strBody += "</tr>";
            
            strBody += "<tr>";
            string strUrl = Request.Url.ToString().Replace(Request.Url.PathAndQuery, "");
            strUrl += "/persondetail.aspx?NonID="+newEmployee.NonEmployeeID.ToString();
            strBody += "<td><b>Link:</b></td>";
            strBody += "<td colspan=2><A HREF=\"" + strUrl + "\">" + strUrl + "</A></td>";
            strBody += "</tr>";

            strBody += "<tr>";
            strBody += "<td colspan=\"3\"><hr /></td>";
            strBody += "</tr>";
            strBody += "</table>";

            strBody += "<table width=\"700\" border=\"0\">";
            strBody += "<tr valign=\"bottom\" height=\"40\">";
            strBody += "<td width=\"33%\">&nbsp;</td>";
            strBody += "<td width=\"33%\" valign=\"bottom\"><span style=\"font-size:12pt;text-decoration:underline; font-weight: bold\">Old Value</span></td>";
            strBody += "<td width=\"33%\" valign=\"bottom\"><span style=\"font-size:12pt;text-decoration:underline; font-weight: bold\">New Value</span></td>";
            strBody += "</tr>";

            string strContactHead = "<tr>";
            strContactHead += "<td colspan=\"3\" valign=\"bottom\"><span style=\"font-size:12pt;text-decoration:underline; font-weight: bold\">Contact Information</span></td>";
            strContactHead += "</tr>";

            
            string strContactBody = this.GetCompareString(oldEmployee.LastName, newEmployee.LastName, "Last Name");
            strContactBody += this.GetCompareString(oldEmployee.FirstName, newEmployee.FirstName, "First Name");
            strContactBody += this.GetCompareString(oldEmployee.MidName, newEmployee.MidName, "Middle Name");
            strContactBody += this.GetCompareString(oldEmployee.NickName, newEmployee.NickName, "Nick Name");
            strContactBody += this.GetCompareString(oldEmployee.NetworkID, newEmployee.NetworkID, "Network ID");
            strContactBody += this.GetCompareString(oldEmployee.EmailAddress, newEmployee.EmailAddress, "Email Address");
            strContactBody += this.GetCompareString(oldEmployee.PreferredPhone, newEmployee.PreferredPhone, "Preferred Phone");
            strContactBody += this.GetCompareString(oldEmployee.Street1, newEmployee.Street1, "Address 1");
            strContactBody += this.GetCompareString(oldEmployee.Street2, newEmployee.Street2, "Address 2");

            strContactBody += this.GetCompareString(oldEmployee.City, newEmployee.City, "City");
            strContactBody += this.GetCompareString(oldEmployee.State, newEmployee.State, "State");
            strContactBody += this.GetCompareString(oldEmployee.PostalCode, newEmployee.PostalCode, "Postal Code");
            strContactBody += this.GetCompareString(oldEmployee.CountryCode, newEmployee.CountryCode, "Country");
            strContactBody += this.GetCompareString(oldEmployee.OldUserID, newEmployee.OldUserID, "Old User ID");
            strContactBody += this.GetCompareString(oldEmployee.LanguageName, newEmployee.LanguageName, "Preferred Language");

            if (strContactBody != String.Empty)
                strBody += strContactHead + strContactBody;

            string strJobHead = "<tr height=\"40\">";
            strJobHead += "<td colspan=\"3\" valign=\"bottom\"><span style=\"font-size:12pt;text-decoration:underline; font-weight: bold\">Job Information</span></td>";
            strJobHead += "</tr>";

            string strJobBody = this.GetCompareString(oldEmployee.NonEmployeeID.ToString(), newEmployee.NonEmployeeID.ToString(), "Non-Employee ID");
            strJobBody += this.GetCompareString(oldEmployee.NonEmployeeStatus, newEmployee.NonEmployeeStatus, "Status");
            strJobBody += this.GetCompareString(oldEmployee.OrgCode, newEmployee.OrgCode, "OrgCode");

            strJobBody += this.GetCompareString(oldEmployee.HcpFacing, newEmployee.HcpFacing, "HCP Facing");
            strJobBody += this.GetCompareString(oldEmployee.BusinessAreaName, newEmployee.BusinessAreaName, "Business Area");
            strJobBody += this.GetCompareString(oldEmployee.CostCode, newEmployee.CostCode, "Cost Center");
            strJobBody += this.GetCompareString(oldEmployee.PersonnelSubAreaName, newEmployee.PersonnelSubAreaName, "Location");
            strJobBody += this.GetCompareString(oldEmployee.HireDate.ToShortDateString(), newEmployee.HireDate.ToShortDateString(), "Hire Date");
            strJobBody += this.GetCompareString(oldEmployee.TermDate.ToShortDateString(), newEmployee.TermDate.ToShortDateString(), "Term Date");
            strJobBody += this.GetCompareString(oldEmployee.FunctionName, newEmployee.FunctionName, "Function");
            strJobBody += this.GetCompareString(oldEmployee.PositionName, newEmployee.PositionName, "Position");
            strJobBody += this.GetCompareString(oldEmployee.SupervisorEmployeeID, newEmployee.SupervisorEmployeeID, "Supervisor Employee #");
            strJobBody += this.GetCompareString(oldEmployee.SupervisorName, newEmployee.SupervisorName, "Supervisor Name");
            strJobBody += this.GetCompareString(oldEmployee.ContractOwner, newEmployee.ContractOwner, "Contract Owner");
            strJobBody += this.GetCompareString(oldEmployee.KronosNumber, newEmployee.KronosNumber, "Kronos Number");
            strJobBody += this.GetCompareString(oldEmployee.ShiftCode, newEmployee.ShiftCode, "Shift");
            strJobBody += this.GetCompareString(oldEmployee.EmployeeTypeDesc, newEmployee.EmployeeTypeDesc, "RFT/RPT");
            strJobBody += this.GetCompareString(oldEmployee.FTtoNE, newEmployee.FTtoNE, "FT to NE");
            strJobBody += this.GetCompareString(oldEmployee.NEtoFT, newEmployee.NEtoFT, "NE to FT");
            strJobBody += this.GetCompareString(oldEmployee.Rehire, newEmployee.Rehire, "Rehire");
            strJobBody += this.GetCompareString(oldEmployee.LegacyPositionDate.ToShortDateString(), newEmployee.LegacyPositionDate.ToShortDateString(), "Job Effective Date");
            strJobBody += this.GetCompareString(oldEmployee.LegacyPosition, newEmployee.LegacyPosition, "Legacy Job Description");
            strJobBody += this.GetCompareString(oldEmployee.CertificationSent.ToShortDateString(), newEmployee.CertificationSent.ToShortDateString(), "Debarred List Certification Sent");
            strJobBody += this.GetCompareString(oldEmployee.CertificationReceived.ToShortDateString(), newEmployee.CertificationReceived.ToShortDateString(), "Debarred List Certification Received");
            strJobBody += this.GetCompareString(oldEmployee.Notes, newEmployee.Notes, "Notes");

            if (strJobBody != String.Empty)
                strBody += strJobHead + strJobBody;

            string strSalesHead = "<tr height=\"40\">";
            strSalesHead += "<td colspan=\"3\" valign=\"bottom\"><span style=\"font-size:12pt;text-decoration:underline; font-weight: bold\">Sales Information</span></td>";
            strSalesHead += "</tr>";

            string strSalesBody = this.GetCompareString(oldEmployee.SalesRepTypeDesc, newEmployee.SalesRepTypeDesc, "Sales Rep Type");
            strSalesBody += this.GetCompareString(oldEmployee.RegionDesc, newEmployee.RegionDesc, "Region");
            strSalesBody += this.GetCompareString(oldEmployee.SalesDistrict, newEmployee.SalesDistrict, "Sales District");
            strSalesBody += this.GetCompareString(oldEmployee.SalesRepID, newEmployee.SalesRepID, "Sales Rep ID(s)");
            strSalesBody += this.GetCompareString(oldEmployee.TPS, newEmployee.TPS, "TPS");

            if (strSalesBody != String.Empty)
                strBody += strSalesHead + strSalesBody;

            strBody += "</table>";

            return strBody;
        }

        /// <summary>Handles the Click event of the btnNewEntry control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnNewEntry_Click(object sender, EventArgs e)
        {
            Response.Redirect("PersonDetail.aspx?NonID=0");
        }

        /// <summary>Handles the SelectedIndexChanged event of the ddlFunction control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DdlFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            //// We are only going to do this if this is sales.
            if (this.ddlFunction.SelectedItem.Text.ToLower() == "sales")
            {
                this.SetSalesControls(true);
                this.LoadSRDDL(this.ddlCountry.SelectedValue);
            }
            else
            {
                this.SetSalesControls(false);
            }
        }

        /// <summary>Handles the SelectedIndexChanged event of the ddlStatus control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DdlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlStatus.SelectedValue.ToLower().StartsWith("t"))
            {
                ////  this.ckReHire.SelectedValue = "0";
                this.ckReHire.Visible = false;
                this.lblElig.Visible = false;
                this.valReHire.Enabled = false;
                this.valReHire.Visible = false;
            }
            else
            {
                ////   this.ckReHire.SelectedValue = "1";
                this.ckReHire.Visible = true;
                this.lblElig.Visible = true;
                this.valReHire.Enabled = true;
                this.valReHire.Visible = true;
            }
        }

        /// <summary>Sets the controls.</summary>
        private void SetControls()
        {
            bool showID = Convert.ToBoolean(ViewState["NonID"].ToString() != "0");
            this.LabelEmployeeID.Visible = showID;
            this.lblEmployeeID.Visible = showID;

            if (ViewState["NonID"].ToString() != "0")
            {
                this.txtLastName.AutoPostBack = false;
            }
            else
            {
                this.txtLastName.AutoPostBack = true;
            }
        }

        /// <summary>
        /// Sets the Controls in the Job Section; if PA is not 
        /// selected the controls in the section do not display
        /// </summary>
        private void SetJobControls()
        {
            Debug.WriteLine("Set Job Controls");
            bool showJobInfo = !Convert.ToBoolean(this.ddlCountry.SelectedIndex == 0);

            this.lblJobInformation.Visible = showJobInfo;
            this.LabelCostCenter.Visible = showJobInfo;
            this.LabelPosition.Visible = showJobInfo;
            this.LabelShift.Visible = showJobInfo;
            this.LabelStartDate.Visible = showJobInfo;
            this.LabelStatus.Visible = showJobInfo;
            this.LabelSupervisorID.Visible = showJobInfo;
            this.LabelSupervisorName.Visible = showJobInfo;
            this.LabelTermDate.Visible = showJobInfo;
            this.LabelOrgCode.Visible = showJobInfo;
            this.LabelPersonnelSubArea.Visible = showJobInfo;
            this.ddlOrgCode.Visible = showJobInfo;
            this.lnkOrgCode.Visible = showJobInfo;
            this.ddlCostCenter.Visible = showJobInfo;
            this.ddlPositionTitle.Visible = showJobInfo;
            this.ddlShift.Visible = showJobInfo;
            this.ddlStatus.Visible = showJobInfo;
            this.txtHireDate.Visible = showJobInfo;
            this.txtSupervisorEmployeeID.Visible = showJobInfo;
            this.txtSupervisorName.Visible = showJobInfo;
            this.txtTermDate.Visible = showJobInfo;
            this.vldStatus.Visible = showJobInfo;
            this.ddlPersonnelSubArea.Visible = showJobInfo;

            this.LabelHCP.Visible = showJobInfo;
            this.ddlHCP.Visible = showJobInfo;
            this.vldRequiredHCP.Visible = showJobInfo;
            this.lblBusinessArea.Visible = showJobInfo;
            this.ddlBusinessArea.Visible = showJobInfo;
            this.vldRequiredBusinessArea.Visible = showJobInfo;
            this.LabelCompanyName.Visible = showJobInfo;
            this.txtCompanyName.Visible = showJobInfo;
            this.LabelKronos.Visible = showJobInfo;
            this.txtKronosNumber.Visible = showJobInfo;
            this.lblEmployeeType.Visible = showJobInfo;
            this.ddlEmployeeType.Visible = showJobInfo;
            this.LabelFTtoNE.Visible = showJobInfo;
            this.ddlFTtoNE.Visible = showJobInfo;
            this.LabelNEtoFT.Visible = showJobInfo;
            this.ddlNEtoFT.Visible = showJobInfo;
            this.lblLegacyPositionDate.Visible = showJobInfo;
            this.txtLegacyPositionDate.Visible = showJobInfo;
            this.lblLegacyJobDescription.Visible = showJobInfo;
            this.ddlLegacyPosition.Visible = showJobInfo;
            this.lblCertificationSent.Visible = showJobInfo;
            this.txtCertificationSent.Visible = showJobInfo;
            this.lblCertificationReceived.Visible = showJobInfo;
            this.txtCertificationReceived.Visible = showJobInfo;
            this.LabelNotes.Visible = showJobInfo;
            this.txtNotes.Visible = showJobInfo;

            this.vldStatusDate.Visible = showJobInfo;
            this.vldSupervisorEmployeeID.Visible = showJobInfo;
            this.vldRequiredSupervisorName.Visible = showJobInfo;

            this.lnkPosition.Visible = showJobInfo;
            this.lnkCost.Visible = showJobInfo;

            if (ddlStatus.SelectedValue.ToLower().StartsWith("t"))
            {
                this.ckReHire.Visible = showJobInfo;
                this.lblElig.Visible = showJobInfo;
                this.valReHire.Enabled = showJobInfo;
            }
            else
            {
                this.ckReHire.Visible = false;
                this.lblElig.Visible = false;
                this.valReHire.Enabled = false;
            }

            this.vldPositionTitle.Visible = showJobInfo;
            this.ddlFunction.Visible = showJobInfo;
            this.lblFunction.Visible = showJobInfo;
            ////CMB - End Controls

            if (showJobInfo)
            {
                lnkPosition.Attributes.Add("onmouseover", "showhint('If you do not find the Position you are looking for, please select \"Other\" at the end of the list. ', this, event, '200px', 0, 30);");
                lnkCost.Attributes.Add("onmouseover", "showhint('If you do not find the Cost Center you are looking for, please select \"Other\" at the end of the list. ', this, event, '200px', 0, 30);");
                lnkOrgCode.Attributes.Add("onmouseover", "showhint('If you do not find the Organization Code you are looking for, please select \"Other\" at the end of the list. ', this, event, '200px', 0, 30);");
            }
        }

        /// <summary>Sets the duplicate control.</summary>
        /// <param name="isVisible">if set to <c>true</c> [is visible].</param>
        private void SetDuplicateControl(bool isVisible)
        {
            this.LabelDuplicate.Visible = isVisible;
            this.gvDuplicates.Visible = isVisible;
            this.btnClose.Visible = isVisible;
            this.tblDuplicates.Visible = isVisible;
        }

        /// <summary>Sets the sales controls.</summary>
        /// <param name="showSalesInfo">if set to <c>true</c> [show sales info].</param>
        private void SetSalesControls(bool showSalesInfo)
        {
            ////bool showSalesInfo = Convert.ToBoolean(this.ddlEmpType.SelectedValue == "NES");
            LabelSalesHeader.Visible = showSalesInfo;

            this.txtSalesDistrict.Visible = showSalesInfo;
            this.LabelSalesDistrict.Visible = showSalesInfo;
            this.ddlSalesRepType.Visible = showSalesInfo;
            this.LabelSalesRepType.Visible = showSalesInfo;
            this.ddlRegion.Visible = showSalesInfo;
            this.LabelRegion.Visible = showSalesInfo;
            this.gvSalesRepID.Visible = showSalesInfo;

            this.txtRepID.Visible = showSalesInfo;
            this.LabelSalesRepID.Visible = showSalesInfo;
            this.LabelSalesRepIDNote.Visible = showSalesInfo;
            this.btnAdd.Visible = showSalesInfo;
            this.LabelTPS.Visible = showSalesInfo;
            this.ddlTPS.Visible = showSalesInfo;

        }

        /// <summary>Loads the data for a person.</summary>
        private void LoadData()
        {
            Debug.WriteLine("In LoadData");
            NonEmployeeEntity ned =  this.GetNonEmployeeDBRecord(Int32.Parse(ViewState["NonID"].ToString()));
            Debug.WriteLine("Got NED Record");
            this.lblEmployeeID.Text = ViewState["NonID"].ToString();
            this.txtHireDate.Date = ned.HireDate.ToShortDateString();
            if (ned.TermDate.ToString() == "01/01/0001 00:00:00")
            {
                this.txtTermDate.Date = "";
            }
            else
            {
                this.txtTermDate.Date = ned.TermDate.ToShortDateString();
            }
            this.txtLastName.Text = ned.LastName;
            this.txtFirstName.Text = ned.FirstName;
            this.txtMiddleName.Text = ned.MidName;
            this.txtNickName.Text = ned.NickName;
            this.txtSupervisorEmployeeID.Text = ned.SupervisorEmployeeID;
            this.txtSupervisorName.Text = ned.SupervisorName;
            this.txtCity.Text = ned.City;
            this.txtEmailAddress.Text = ned.EmailAddress;
            this.txtPreferredPhone.Text = ned.PreferredPhone;
            this.txtAddress1.Text = ned.Street1;
            this.txtAddress2.Text = ned.Street2;
            this.txtZipCode.Text = ned.PostalCode;
            this.txtSalesDistrict.Text = ned.SalesDistrict;

            this.txtOldUserID.Text = ned.OldUserID;
            this.txtCompanyName.Text = ned.ContractOwner;
            this.txtNotes.Text = ned.Notes;
            this.txtKronosNumber.Text = ned.KronosNumber;

            if (ned.LegacyPositionDate.ToString() == "01/01/0001 00:00:00")
            {
                this.txtLegacyPositionDate.Date = "";
            }
            else
            {
                this.txtLegacyPositionDate.Date = ned.LegacyPositionDate.ToShortDateString();
            }
            if (ned.CertificationReceived.ToString() == "01/01/0001 00:00:00")
            {
                this.txtCertificationReceived.Date = "";
            }
            else
            {
                this.txtCertificationReceived.Date = ned.CertificationReceived.ToShortDateString();
            }
            if (ned.CertificationSent.ToString() == "01/01/0001 00:00:00")
            {
                this.txtCertificationSent.Date = "";
            }
            else
            {
                this.txtCertificationSent.Date = ned.CertificationSent.ToShortDateString();
            }
            
            ////split the comma delimited IDs and load to a table then Bind the table to the GridView
            DataTable datRepIds = this.LoadRepIDTableFromDB(ned.SalesRepID);

            if (datRepIds.Rows.Count > 1)
            {
                this.BindDT(datRepIds);
            }
            else
            {
                if (datRepIds.Rows.Count == 1)
                {
                    this.txtRepID.Text = datRepIds.Rows[0]["RepID"].ToString();
                }
            }

            if (this.ddlCountry.Items.FindByValue(ned.CountryCode) != null)
            {
                 ddlCountry.Items.FindByValue(ned.CountryCode).Selected = true;
            }

            // Load all drop downs related to the Country Code
            if (ddlCountry.Items.Count > 0)
            {
                this.LoadCountryDropDowns(ddlCountry.SelectedValue);
            }

            if (this.ddlPersonnelSubArea.Items.FindByValue(ned.PersonnelSubAreaCode) != null)
            {
                Debug.WriteLine("LoadData PersonnelSubArea" + ned.PersonnelSubAreaCode.ToString());
                ddlPersonnelSubArea.Items.FindByValue(ned.PersonnelSubAreaCode).Selected = true;
            }

            if (this.ddlOrgCode.Items.FindByValue(ned.OrgCode) != null)
            {
                ddlOrgCode.Items.FindByValue(ned.OrgCode).Selected = true;
            }


            if (this.ddlTPS.Items.FindByValue(ned.TPS) != null)
            {
                ddlTPS.Items.FindByValue(ned.TPS).Selected = true;
            }

            if (this.ddlHCP.Items.FindByValue(ned.HcpFacing) != null)
            {
                ddlHCP.Items.FindByValue(ned.HcpFacing).Selected = true;
            }

            if (this.ddlNEtoFT.Items.FindByValue(ned.NEtoFT) != null)
            {
                ddlNEtoFT.Items.FindByValue(ned.NEtoFT).Selected = true;
            }

            if (this.ddlFTtoNE.Items.FindByValue(ned.FTtoNE) != null)
            {
                ddlFTtoNE.Items.FindByValue(ned.FTtoNE).Selected = true;
            }

            if (this.ddlStatus.Items.FindByValue(ned.NonEmployeeStatus) != null)
            {
                ddlStatus.Items.FindByValue(ned.NonEmployeeStatus).Selected = true;
            }

            if (this.ckReHire.Items.FindByValue(ned.Rehire) != null)
            {
                ckReHire.Items.FindByValue(ned.Rehire).Selected = true;
            }

            if (this.ddlEmployeeType.Items.FindByValue(ned.EmployeeTypeCode) != null)
            {
                ddlEmployeeType.Items.FindByValue(ned.EmployeeTypeCode).Selected = true;
            }

            if (this.ddlLegacyPosition.Items.FindByValue(ned.LegacyPosition) != null)
            {
                ddlLegacyPosition.Items.FindByValue(ned.LegacyPosition).Selected = true;
            }

            bool isActive = ddlStatus.SelectedValue.ToLower().StartsWith("a");   
            this.ckReHire.Visible = !isActive;
            this.lblElig.Visible = !isActive;
            this.valReHire.Enabled = !isActive;
//            Debug.WriteLine("LoadData Country");
//            if (this.ddlCountry.Items.FindByValue(ned.CountryCode) != null)
//            {
//                ddlCountry.Items.FindByValue(ned.CountryCode).Selected = true;
//            }
            Debug.WriteLine("LoadData Position");
            if (this.ddlPositionTitle.Items.FindByValue(ned.PositionName) != null)
            {
                ddlPositionTitle.Items.FindByValue(ned.PositionName).Selected = true;
            }

            if (this.ddlState.Items.FindByValue(ned.State) != null)
            {
                ddlState.Items.FindByValue(ned.State).Selected = true;
            }

            if (this.ddlShift.Items.FindByValue(ned.ShiftCode) != null)
            {
                ddlShift.Items.FindByValue(ned.ShiftCode).Selected = true;
            }

            if (this.ddlLanguage.Items.FindByValue(ned.LanguageID.ToString()) != null)
            {
                ddlLanguage.Items.FindByValue(ned.LanguageID.ToString()).Selected = true;
            }

            if (this.ddlBusinessArea.Items.FindByValue(ned.BusinessAreaID.ToString()) != null)
            {
                ddlBusinessArea.Items.FindByValue(ned.BusinessAreaID.ToString()).Selected = true;
            }

            Debug.WriteLine("LoadData CostCenter");
            if (this.ddlCostCenter.Items.FindByValue(ned.CostCode) != null)
            {
                ddlCostCenter.Items.FindByValue(ned.CostCode).Selected = true;
            }
            Debug.WriteLine("LoadData Region");
            if (this.ddlRegion.Items.FindByValue(ned.RegionCode) != null)
            {
                ddlRegion.Items.FindByValue(ned.RegionCode).Selected = true;
            }

            if (this.ddlFunction.Items.FindByValue(ned.FunctionID.ToString()) != null)
            {
                ddlFunction.Items.FindByValue(ned.FunctionID.ToString()).Selected = true;

                if (ddlFunction.Items.FindByValue(ned.FunctionID.ToString()).Text.ToLower() == "sales")
                {
                    this.LoadSRDDL(this.ddlCountry.SelectedValue); ////CMB
                }
            }

            if (this.ddlSalesRepType.Items.FindByValue(ned.SalesRepTypeCode) != null)
            {
                ddlSalesRepType.Items.FindByValue(ned.SalesRepTypeCode).Selected = true;
            }

        } //// end LoadData


        private NonEmployeeEntity GetNonEmployeeDBRecord(int nedID)
        {
            Debug.WriteLine("In NonEmployeeDBRecord");
            NonEmployeeEntity myNonEmployee = new NonEmployeeEntity();
            myNonEmployee.NonEmployeeID = nedID;
            DataTable dt = SqlUtility.SetTable("sp_GetNonEmployeeByID", nedID);
            Debug.WriteLine("Got NonEmployee Record");
            if (dt.Rows.Count > 0)
            {
                DataRow r = dt.Rows[0];
                myNonEmployee.City = r["City"].ToString();
                myNonEmployee.ContractOwner = r["ContractOwner"].ToString();
                myNonEmployee.CostCode = r["CostCode"].ToString();
                myNonEmployee.CountryCode = r["Country_Code"].ToString();
                myNonEmployee.EmailAddress = r["EmailAddress"].ToString();
                myNonEmployee.FirstName = r["FirstName"].ToString();
                myNonEmployee.FunctionID = (r["FunctionID"].ToString().Length <= 0) ? 0 : Convert.ToInt32(r["FunctionID"].ToString());
                myNonEmployee.HireDate = Convert.ToDateTime(this.myUtility.GetShortDate(r["HireDate"].ToString()));
                myNonEmployee.KronosNumber = r["KronosNumber"].ToString();
                myNonEmployee.LanguageID = (r["LanguageID"].ToString().Length <= 0) ? 0 : Convert.ToInt32(r["LanguageID"].ToString());
                myNonEmployee.LastName = r["LastName"].ToString();
                myNonEmployee.MidName = r["MidName"].ToString();
                myNonEmployee.NetworkID = r["NetworkID"].ToString();
                myNonEmployee.NickName = r["NickName"].ToString();
                myNonEmployee.NonEmployeeStatus = r["NonEmployeeStatus"].ToString();
                myNonEmployee.Notes = r["Notes"].ToString();
                myNonEmployee.OldUserID = r["OldUserID"].ToString();
                if (r["BusinessAreaID"].ToString() != "")
                {
                    myNonEmployee.BusinessAreaID = Int32.Parse(r["BusinessAreaID"].ToString());
                }
                myNonEmployee.BusinessAreaName = r["BusinessAreaName"].ToString();
                myNonEmployee.PersonnelSubAreaCode = r["PaSubCode"].ToString();
                myNonEmployee.PersonnelSubAreaName = r["PaSubDesc"].ToString();
                myNonEmployee.PositionName = r["PositionName"].ToString();
                myNonEmployee.PostalCode = r["PostalCode"].ToString();
                myNonEmployee.PreferredPhone = r["PreferredPhone"].ToString();
                myNonEmployee.RegionCode = r["RegionCode"].ToString();
                myNonEmployee.Rehire = r["Rehire"].ToString();
                myNonEmployee.SalesDistrict = r["SalesDistrict"].ToString();
                myNonEmployee.SalesRepID = r["SalesRepID"].ToString();
                myNonEmployee.SalesRepTypeCode = r["SalesRepTypeCode"].ToString();
                myNonEmployee.ShiftCode = r["ShiftCode"].ToString();
                myNonEmployee.State = r["State"].ToString();
                myNonEmployee.Street1 = r["Street1"].ToString();
                myNonEmployee.Street2 = r["Street2"].ToString();
                myNonEmployee.SupervisorEmployeeID = r["SupervisorEmployeeID"].ToString();
                myNonEmployee.SupervisorName = r["SupervisorName"].ToString();
                myNonEmployee.TermDate = (this.myUtility.GetShortDate(r["TermDate"].ToString()) == String.Empty) ? DateTime.MinValue : Convert.ToDateTime(this.myUtility.GetShortDate(r["TermDate"].ToString()));
                myNonEmployee.TPS = r["TPS"].ToString();
                myNonEmployee.HcpFacing = r["HcpFacing"].ToString();
                myNonEmployee.FTtoNE = r["FTtoNE"].ToString();
                myNonEmployee.NEtoFT = r["NEtoFT"].ToString();

                myNonEmployee.LanguageName = r["LanguageName"].ToString();
                myNonEmployee.FunctionName = r["FunctionName"].ToString();
                myNonEmployee.SalesRepTypeDesc = r["SalesRepTypeDesc"].ToString();
                myNonEmployee.RegionDesc = r["RegionDesc"].ToString();

                if (r["LegacyPositionDate"].ToString() != "")
                {
                    myNonEmployee.LegacyPositionDate = Convert.ToDateTime(this.myUtility.GetShortDate(r["LegacyPositionDate"].ToString()));
                }
                if (r["CertificationSent"].ToString() != "")
                {
                    myNonEmployee.CertificationSent = Convert.ToDateTime(this.myUtility.GetShortDate(r["CertificationSent"].ToString()));
                }
                if (r["CertificationReceived"].ToString() != "")
                {
                    myNonEmployee.CertificationReceived = Convert.ToDateTime(this.myUtility.GetShortDate(r["CertificationReceived"].ToString()));
                }
                    
                myNonEmployee.LegacyPosition = r["LegacyPosition"].ToString();
                myNonEmployee.OrgCode = r["OrgCode"].ToString();
            } //// end if

            return myNonEmployee;
        } //// end 

        /// <summary>
        /// Loads the drop downs.
        /// </summary>
        private void LoadDropDowns()
        {
            Debug.WriteLine("In LoadDropDowns");
            ////languages
            this.ddlLanguage.Items.Clear();
            ddlLanguage.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblLanguages.Rows)
            {
                this.ddlLanguage.Items.Add(new ListItem(r["LanguageName"].ToString(), r["LanguageID"].ToString()));
            }

            ////state
            this.ddlState.Items.Clear();
            ddlState.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblStates.Rows)
            {
                this.ddlState.Items.Add(new ListItem(r["states"].ToString(), r["statescode"].ToString()));
            }

            ////Country
            this.ddlCountry.Items.Clear();
            ddlCountry.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblCountries.Rows)
            {
                ////CMB - Took the long county name out of it.  I think this was to make the user happy with the results.
                ////this.ddlCountry.Items.Add(new ListItem(r["Countries"].ToString(), r["country_code"].ToString()));
                this.ddlCountry.Items.Add(new ListItem(r["Countries"].ToString(), r["country_code"].ToString()));
            }

            ////Employee Type
            this.ddlEmployeeType.Items.Clear();
            ddlEmployeeType.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblEmployeeType.Rows)
            {
                this.ddlEmployeeType.Items.Add(new ListItem(r["EmployeeTypeDesc"].ToString(), r["EmployeeTypeCode"].ToString()));
            }

            ////Shift
            this.ddlShift.Items.Clear();
            ddlShift.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblShift.Rows)
            {
                this.ddlShift.Items.Add(new ListItem(r["ShiftDesc"].ToString(), r["ShiftCode"].ToString()));
            }

            ////Status
            this.ddlStatus.Items.Clear();
            ddlStatus.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblStatus.Rows)
            {
                this.ddlStatus.Items.Add(new ListItem(r["StatusDesc"].ToString(), r["StatusCode"].ToString()));
            }

            ////CMB - Functions
            this.ddlFunction.Items.Clear();
            ddlFunction.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblFunctions.Rows)
            {
                this.ddlFunction.Items.Add(new ListItem(r["FunctionName"].ToString(), r["FunctionID"].ToString()));
            }


            ////Org Code
            this.ddlOrgCode.Items.Clear();
            ddlOrgCode.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblOrgCode.Rows)
            {
                this.ddlOrgCode.Items.Add(new ListItem(r["OrgCode"].ToString() + " - " + r["OrgCodeDesc"].ToString(), r["OrgCode"].ToString()));
            }

            ////HCP Facing
            this.ddlHCP.Items.Clear();
            ddlHCP.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in this.myUtility.TblHCPFacing.Rows)
            {
                this.ddlHCP.Items.Add(new ListItem(r["HCPFacingName"].ToString(), r["HCPFacingInitial"].ToString()));
            }

        }

        /// <summary>Loads the Country drop downs.</summary>
        /// <param name="strCountry_Code">The country code.</param>
        private void LoadCountryDropDowns(string strCountry_Code)
        {
            Debug.WriteLine("In LoadCountryDropDowns");
            string strCompanyCode = string.Empty;
 
            foreach (DataRow r in this.myUtility.TblCountries.Rows)
            {
                if (r["Country_Code"].ToString() == strCountry_Code)
                strCompanyCode = r["CompanyCode"].ToString();
            }
            string strWhere = "CompanyCode = '" + strCompanyCode + "'";

            ////Business Areas
            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@CountryCode", strCountry_Code));
            DataTable dtBusinessArea = SqlUtility.SqlExecuteQuery("sp_GetBusinessAreasByCountry", myParamters);
            
            this.ddlBusinessArea.Items.Clear();
            ddlBusinessArea.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in dtBusinessArea.Rows)
            {
                this.ddlBusinessArea.Items.Add(new ListItem(r["BusinessAreaName"].ToString(), r["BusinessAreaID"].ToString()));
            }

            ////Position
            this.ddlPositionTitle.Items.Clear();
            ddlPositionTitle.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlLegacyPosition.Items.Clear();
            ddlLegacyPosition.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] positionRows = this.myUtility.TblPosition.Select(strWhere, "PositionName");
            foreach (DataRow r in positionRows)
            {
                if (r["CompanyCode"].ToString() == strCompanyCode)
                {
                    this.ddlPositionTitle.Items.Add(new ListItem(r["PositionName"].ToString(), r["PositionName"].ToString()));
                    this.ddlLegacyPosition.Items.Add(new ListItem(r["PositionName"].ToString(), r["PositionName"].ToString()));
                }
            }

            // Load Cost Center
            this.ddlCostCenter.Items.Clear();
            this.ddlCostCenter.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] rows = this.myUtility.TblCostCenters.Select(strWhere);
            foreach (DataRow r in rows)
            {
                this.ddlCostCenter.Items.Add(new ListItem(r["CostCenter"].ToString() + " - " + r["CostCenterName"].ToString(), r["CostCenter"].ToString()));
            }

            ////CMB - Locations
            this.ddlPersonnelSubArea.Items.Clear();
            ddlPersonnelSubArea.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] subRows = this.myUtility.TblPersonnelSubArea.Select(strWhere);
            foreach (DataRow r in subRows)
            {
                this.ddlPersonnelSubArea.Items.Add(new ListItem(r["PaSubDesc"].ToString(), r["PaSubCode"].ToString()));
            }

            ////CMB - Region
            this.ddlRegion.Items.Clear();
            ddlRegion.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] regionRows = this.myUtility.TblRegion.Select(strWhere);
            foreach (DataRow r in regionRows)
            {
                this.ddlRegion.Items.Add(new ListItem(r["RegionDesc"].ToString(), r["RegionCode"].ToString()));
            }
        }

        /// <summary>Gets the date.</summary>
        /// <param name="strDate">The date string.</param>
        /// <returns>The date time for the date string.</returns>
        private DateTime GetDate(string strDate)
        {
            try
            {
                if (strDate != String.Empty)
                {
                    string[] arrayDate = strDate.Split('/');
                    int intMo = Int32.Parse(arrayDate[0]);
                    int intDy = Int32.Parse(arrayDate[1]);
                    int intYr = Int32.Parse(arrayDate[2].Substring(0, 4));

                    DateTime dt = new DateTime(intYr, intMo, intDy);
                    return dt;
                }
                else
                {
                    return new DateTime();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Bad Date: " + ex.Message);
                return new DateTime();
            }
        }

        /// <summary>
        /// Comma the delimits representative.
        /// </summary>
        /// <returns>the string of the representative id.</returns>
        private string CommaDelimitRepID()
        {
            string strRepID = String.Empty;
            if (this.txtRepID.Text != String.Empty)
            {
                strRepID = this.txtRepID.Text;
            }

            foreach (GridViewRow r in this.gvSalesRepID.Rows)
            {
                if (strRepID == String.Empty)
                {
                    strRepID += r.Cells[0].Text.Trim();
                }
                else
                {
                    strRepID += "," + r.Cells[0].Text.Trim();
                }
            }

            return strRepID;
        }

        /// <summary>Binds the Data tables.</summary>
        /// <param name="dt">The datatable to bind.</param>
        private void BindDT(DataTable dt)
        {
            this.gvSalesRepID.DataSource = dt;
            this.gvSalesRepID.DataBind();
        }

        /// <summary>
        /// Loads the representative ID table from DB.
        /// </summary>
        /// <param name="strID">The ID string.</param>
        /// <returns>The filled data table.</returns>
        private DataTable LoadRepIDTableFromDB(string strID)
        {

            DataTable datID = this.CreateRepIDTable();
            if (strID != String.Empty)
            {
                string[] strIDs = strID.Split(',');
                for (int x = 0; x < strIDs.Length; x++)
                {
                    DataRow r = datID.NewRow();
                    r["RepID"] = strIDs[x];
                    datID.Rows.Add(r);
                    Debug.WriteLine("r: " + r["RepID"].ToString());
                }
            }

            return datID;
        }

        /// <summary>
        /// Loads the representative ID table grid view.
        /// </summary>
        /// <returns>The filled data table.</returns>
        private DataTable LoadRepIDTableGridView()
        {
            DataTable datID = this.CreateRepIDTable();
            foreach (GridViewRow r in this.gvSalesRepID.Rows)
            {
                DataRow tableRow = datID.NewRow();
                tableRow["RepID"] = r.Cells[0].Text;
                datID.Rows.Add(tableRow);
            }

            if (this.txtRepID.Text != String.Empty)
            {
                DataRow tableRow = datID.NewRow();
                tableRow["RepID"] = this.txtRepID.Text;
                datID.Rows.Add(tableRow);
            }

            return datID;
        }

        /// <summary>Sends the update notice.</summary>
        /// <param name="oldEntity">The old entity.</param>
        /// <param name="newEntity">The new entity.</param>
        private void SendUpdateNotice(NonEmployeeEntity oldEntity, NonEmployeeEntity newEntity)
        {
            Debug.WriteLine("sendNotification: " + Convert.ToBoolean(ConfigurationManager.AppSettings["sendNotification"]).ToString());
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["sendNotification"]))
            {
                Debug.WriteLine("In SendUpdateNotice");

                string strMsg = this.SetEmailBody(oldEntity, newEntity);
                string strFrom = ConfigurationManager.AppSettings["emailNotifyFrom"];
                bool isProduction = Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"]);

                string strSubmissionEmailNotificationTo = ConfigurationManager.AppSettings["SubmissionEmailNotification"].ToString();
                Debug.WriteLine("Intended Email To: " + strSubmissionEmailNotificationTo);

                string strSubject = "Non-Employee Record";
                if (oldEntity.NonEmployeeID != 0)
                {
                    strSubject += " Updated";
                }
                else
                {
                    strSubject += " Added";
                }

                //// if it is not production send the email to the user doing the testing
                //// and add the word 'TEST' to the subject line
                if (!isProduction)
                {
                    ADUserEntity myUser = ADUtility.GetADUserByUserName(myUtility.ADAccount);
                    if (myUser.Email != String.Empty)
                        strSubmissionEmailNotificationTo = myUser.Email;
                    else
                        strSubmissionEmailNotificationTo = ConfigurationManager.AppSettings["ccEmail"];

                    strSubject += " TEST";
                }

                Debug.WriteLine("Current Email To: " + strSubmissionEmailNotificationTo + "; Body: " + strMsg);
                this.myUtility.SendEmail(strSubmissionEmailNotificationTo, strFrom, strSubject, strMsg, true);

            } //// end if sendNotification
        } //// method SendUpdateNotice

        /// <summary>Loads the sales drop down lists.</summary>
        private void LoadSRDDL(string strCountry_Code)
        {
            string strCompanyCode = string.Empty;
            foreach (DataRow r in this.myUtility.TblCountries.Rows)
            {
                if (r["Country_Code"].ToString() == strCountry_Code)
                    strCompanyCode = r["CompanyCode"].ToString();
            }
            ////Sales Rep Type
            this.ddlSalesRepType.Items.Clear();
            ddlSalesRepType.Items.Add(new ListItem("-- Select --", ""));
            foreach (DataRow r in this.myUtility.TblSalesRepType.Rows)
            {
                if (r["CompanyCode"].ToString() == strCompanyCode)
                {
                    this.ddlSalesRepType.Items.Add(new ListItem(r["SalesRepTypeDesc"].ToString(), r["SalesRepTypeCode"].ToString()));
                }
            }

            ddlSalesRepType.Items.Add(new ListItem("Other", "Other"));
        }

    } //// end class
}
