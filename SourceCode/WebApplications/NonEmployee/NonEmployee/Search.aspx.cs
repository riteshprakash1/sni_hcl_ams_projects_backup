﻿// -------------------------------------------------------------
// <copyright file="Search.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace NonEmployee
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;
    
    /// <summary>
/// This is the search page.
/// </summary>
    public partial class Search : System.Web.UI.Page
    {
        /// <summary>This is our instance of the utility class.</summary>
        private Utility myUtility;

        /// <summary>This is our non-employee data table.</summary>
        private DataTable nonEmployeesDataTable;

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
            }

            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtNonEmpID, this.btnSearch);
                EnterButton.TieButton(this.txtHireDateFrom, this.btnSearch);
                EnterButton.TieButton(this.txtHireDateTo, this.btnSearch);
                EnterButton.TieButton(this.txtLastName, this.btnSearch);
                EnterButton.TieButton(this.txtTermDateFrom, this.btnSearch);
                EnterButton.TieButton(this.txtTermDateTo, this.btnSearch);
                EnterButton.TieButton(this.txtSupervisorName, this.btnSearch);
                EnterButton.TieButton(this.txtOldUserId, this.btnSearch);

                this.LoadDropDowns();
            }

            this.lblResults.Text = string.Empty;
            Session["Utility"] = this.myUtility;
        }

        /// <summary>Handles the Click event of the btnNew control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("PersonDetail.aspx?NonID=0");
        }

        /// <summary>Handles the PageIndexChanging event of the grid view control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.LoadDataTable();
            gvNonEmployee.DataSource = this.nonEmployeesDataTable;
            gvNonEmployee.PageIndex = e.NewPageIndex;
            gvNonEmployee.DataBind();
        }

        /// <summary>Handles the Sorting event of the grid view control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void GV_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            string sortBY = e.SortExpression;
            this.LoadDataTable();

            DataRow[] rows = this.nonEmployeesDataTable.Select(null, sortBY);

            this.nonEmployeesDataTable = this.nonEmployeesDataTable.Clone();
            foreach (DataRow row in rows)
            {
                this.nonEmployeesDataTable.ImportRow(row);
            }

            gvNonEmployee.DataSource = this.nonEmployeesDataTable;
            gvNonEmployee.DataBind();
            Session["dtNonEmployees"] = this.nonEmployeesDataTable;
        }

        /// <summary>Handles the RowCommand event of the grid view control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Debug.WriteLine("In GV_RowCommand");
            if (e.CommandName == "ViewChange")
            {
                Response.Redirect("PersonDetail.aspx?NonID=" + e.CommandArgument.ToString(), false);
            }
        }

        /// <summary>Handles the Click event of the btnSearch control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.SetSearchTable();
            gvNonEmployee.DataSource = this.nonEmployeesDataTable;
            gvNonEmployee.DataBind();

            if (this.nonEmployeesDataTable.Rows.Count > 0)
                this.lblResults.Text = this.nonEmployeesDataTable.Rows.Count.ToString() + " records found";
        }

        /// <summary>Handles the Click event of the btnReset control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnReset_Click(object sender, EventArgs e)
        {
            this.txtNonEmpID.Text = String.Empty;
            this.txtLastName.Text = String.Empty;
            this.ddlStatus.SelectedIndex = 0;
            txtHireDateFrom.Text = String.Empty;
            txtHireDateTo.Text = String.Empty;
            txtTermDateFrom.Text = String.Empty;
            txtTermDateTo.Text = String.Empty;

            string strQ = "SELECT firstname, lastname, NonEmployeeID, NonEmployeeStatus ";
            strQ += " FROM NonEmployee WHERE NonEmployeeID=-1";
            Debug.WriteLine("In BtnReset_Click: " + strQ);
            this.nonEmployeesDataTable = SqlUtility.SetQueryTable(strQ);

            gvNonEmployee.DataSource = this.nonEmployeesDataTable;
            gvNonEmployee.DataBind();
            Session["dtNonEmployees"] = this.nonEmployeesDataTable;
        }

        /// <summary>Loads the data table.</summary>
        private void LoadDataTable()
        {
            if (Session["dtNonEmployees"] == null)
            {
                this.SetSearchTable();
            }
            else
            {
                this.nonEmployeesDataTable = Session["dtNonEmployees"] as DataTable;
            }
        }

        /// <summary>Binds the data table.</summary>
        private void BindDT()
        {
            this.gvNonEmployee.DataSource = this.nonEmployeesDataTable;
            this.gvNonEmployee.DataBind();
        }

        /// <summary>Loads the drop downs.</summary>
        private void LoadDropDowns()
        {
            ////Company Code
            this.ddlCountry.Items.Clear();
            ddlCountry.Items.Add(new ListItem("-- Select --", "0"));
            foreach (DataRow r in this.myUtility.TblCountries.Rows)
            {
                this.ddlCountry.Items.Add(new ListItem(r["countries"].ToString(), r["country_code"].ToString()));
            }

            ////Status
            this.ddlStatus.Items.Clear();
            ddlStatus.Items.Add(new ListItem("-- Select --", "0"));
            foreach (DataRow r in this.myUtility.TblStatus.Rows)
            {
                this.ddlStatus.Items.Add(new ListItem(r["statusDesc"].ToString(), r["statuscode"].ToString()));
            }
        }

        /// <summary>Sets the search table.</summary>
        private void SetSearchTable()
        {
            //// build the search query
            string strQ = "SELECT firstname, lastname, NonEmployeeID, NonEmployeeStatus, c.Countries, ";
            strQ += " SupervisorName, hireDate, TermDate ";
            strQ += " FROM NonEmployee a ";
            strQ += " LEFT JOIN Countries c on a.country_code = c.country_code ";

            //// create the where clause
            string strWhere = " WHERE NonEmployeeID<>-1 ";
            if (this.txtNonEmpID.Text != String.Empty)
            {
                strWhere = " WHERE NonEmployeeID = " + this.txtNonEmpID.Text + " ";
            }
            else
            {
                if (this.ddlStatus.SelectedIndex != 0)
                {
                    strWhere += " AND a.NonEmployeeStatus = '" + ddlStatus.SelectedValue + "' ";
                }

                if (this.txtLastName.Text != String.Empty)
                {
                    strWhere += " AND a.lastname LIKE '%" + txtLastName.Text.Replace("'", "''") + "%' ";
                }

                if (this.txtHireDateFrom.Text != String.Empty)
                {
                    strWhere += " AND a.hiredate >= '" + txtHireDateFrom.Text + " 00:00:00' ";
                }

                if (this.txtHireDateTo.Text != String.Empty)
                {
                    strWhere += " AND a.hiredate <= '" + txtHireDateTo.Text + " 23:59:00' ";
                }

                if (this.txtTermDateFrom.Text != String.Empty)
                {
                    strWhere += " AND a.termdate >= '" + txtTermDateFrom.Text + " 00:00:00' ";
                }

                if (this.txtTermDateTo.Text != String.Empty)
                {
                    strWhere += " AND a.termdate <= '" + txtTermDateTo.Text + " 23:59:00' ";
                }

                if (this.txtSupervisorName.Text != String.Empty)
                {
                    strWhere += " AND SupervisorName like '%" + this.txtSupervisorName.Text.Replace("'", "''") + "%' ";
                }

                if (this.txtOldUserId.Text != String.Empty)
                {
                    strWhere += " AND OldUserID like '%" + this.txtOldUserId.Text.Replace("'", "''") + "%' ";
                }

                if (this.ddlCountry.SelectedIndex != 0)
                {
                    strWhere += " AND a.country_code = '" + this.ddlCountry.SelectedValue + "' ";
                }
            }

            strQ += strWhere + " ORDER BY a.NonEmployeeID ";
            ////execute the qurey and display the results
            Debug.WriteLine("In BtnSearch_Click: " + strQ);
            this.nonEmployeesDataTable = SqlUtility.SetQueryTable(strQ);
            Session["dtNonEmployees"] = this.nonEmployeesDataTable;
        }

  
    } // end class
}
