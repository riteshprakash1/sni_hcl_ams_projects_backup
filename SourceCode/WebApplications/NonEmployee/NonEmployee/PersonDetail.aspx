﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonDetail.aspx.cs" MasterPageFile="MasterNED.Master" Inherits="NonEmployee.PersonDetail"  Title="Person Detail"%>
<%@ Register Src="~/WebControls/DateTimeControl.ascx" TagName="DateTimeControl" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/RequiredDateTimeControl.ascx" TagName="RequiredDateTimeControl" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc2" Namespace="NonEmployee.Classes" Assembly="NonEmployee" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td colspan="2" class="titleheader">
                    Contact Information
                </td>
                <td colspan="2">
                    <div style="text-align: center">
                        <uc2:WaitButton ID="btnSubmit" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                            OnClick="BtnSubmit_Click" Text="Submit" WaitText="Processing..." ToolTip="Click here to submit.">
                        </uc2:WaitButton>&nbsp;&nbsp;
                        <asp:Button ID="btnNewEntry" runat="server" CausesValidation="false"
                            CssClass="buttonsSubmit" Text="New Entry" OnClick="BtnNewEntry_Click" ToolTip="Click here to add new entry." /></div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelEmployeeID" runat="server" Font-Size="8pt"><b>Non-Employee ID:</b></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblEmployeeID" runat="server" Font-Size="12pt"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width:18%">
                    <b>Last Name:&nbsp;&nbsp;*</b>
                </td>
                <td style="width:32%">
                    <asp:TextBox ID="txtLastName" Width="200px" runat="server" Font-Size="8pt" MaxLength="50"
                        OnTextChanged="TxtLastName_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldLastName" runat="server" ControlToValidate="txtLastName"
                        ErrorMessage="Last Name is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                </td>
                <td style="width:18%">
                    <b>First Name:&nbsp;&nbsp;*</b>
                </td>
                <td style="width:32%">
                    <asp:TextBox ID="txtFirstName" Width="200px" runat="server" Font-Size="8pt" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldFirstName" runat="server" ControlToValidate="txtFirstName"
                        ErrorMessage="First Name is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Table runat="server" ID="tblDuplicates" HorizontalAlign="center" Width="960"
                        BorderStyle="Dashed" BorderWidth="1px" BorderColor="#79BDE9">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="LabelDuplicate" CssClass="titleDup" runat="server">Possible Duplicate</asp:Label>
                                <asp:Label ID="LabelDupMsg" runat="server"><br/>Please verify that the person you are entering does not already exist in the list below.
                                        If you find the person in the list,<br/>click the 'Select' button next to the name;
                                        otherwise click the 'Close List' button signalling that this is not a duplicate entry.</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="btnClose" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                                    Text="Close List" OnClick="BtnClose_Click" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2" VerticalAlign="top">
                                <asp:GridView ID="gvDuplicates" runat="server" DataKeyNames="NonEmployeeID" AutoGenerateColumns="false"
                                    ShowFooter="false" CellSpacing="0" AllowPaging="false" ShowHeader="true" OnRowEditing="GV_Select"
                                    Width="100%">
                                    <RowStyle BorderStyle="None" BorderColor="white" BackColor="white" HorizontalAlign="left" />
                                    <HeaderStyle BackColor="#dcdcdc" ForeColor="#00008B" HorizontalAlign="left" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <asp:Button ID="btnSelect" runat="server" CssClass="buttonsGrid" CausesValidation="false"
                                                    CommandName="Edit" Text="Select" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Last Name" DataField="LastName" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="First Name" DataField="FirstName" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="Middle" DataField="MidName" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="City" DataField="City" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="State" DataField="State" ItemStyle-Width="15%" />
                                    </Columns>
                                </asp:GridView>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Middle Name:</b>
                </td>
                <td>
                    <asp:TextBox ID="txtMiddleName" Width="200px" runat="server" Font-Size="8pt" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <b>Nick Name:&nbsp;&nbsp;*</b>
                </td>
                <td>
                    <asp:TextBox ID="txtNickName" Width="200px" runat="server" Font-Size="8pt" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldRequiredNickName" runat="server" ErrorMessage="Nick Name is required."
                        ControlToValidate="txtNickName">*</asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr>
                <td>
                    <b>Preferred Phone:</b>
                </td>
                <td>
                    <asp:TextBox ID="txtPreferredPhone" Width="200px" runat="server" Font-Size="8pt"
                        MaxLength="30"></asp:TextBox>
                </td>
                <td>
                    <b>Email Address:&nbsp;&nbsp;*</b>
                </td>
                <td>
                    <asp:TextBox ID="txtEmailAddress" Width="200px" runat="server" Font-Size="8pt" MaxLength="75"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="vldEmail" runat="server" ControlToValidate="txtEmailAddress"
                        ErrorMessage="Email Address is invalid." ForeColor="#FF7300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="VldRequiredEmail" runat="server" ControlToValidate="txtEmailAddress"
                        ErrorMessage="Email Address is required." ForeColor="#FF7300">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Address 1:</b>
                </td>
                <td>
                    <asp:TextBox ID="txtAddress1" Width="200px" runat="server" Font-Size="8pt" MaxLength="100"></asp:TextBox>
                </td>
                <td>
                    <b>Address 2:</b>
                </td>
                <td>
                    <asp:TextBox ID="txtAddress2" Width="200px" runat="server" Font-Size="8pt" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>City:</b>
                </td>
                <td>
                    <asp:TextBox ID="txtCity" Width="200px" runat="server" Font-Size="8pt" MaxLength="30"></asp:TextBox>
                </td>
                <td>
                    <b>State:&nbsp;&nbsp;</b></td>
                <td>
                    <asp:DropDownList ID="ddlState" runat="server" Font-Size="8pt" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><b>Postal Code:</b></td>
                <td>
                    <asp:TextBox ID="txtZipCode" Width="200px" runat="server" Font-Size="8pt" MaxLength="20"></asp:TextBox>
                </td>
                <td>
                    <b>Country:&nbsp;&nbsp;*</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCountry" runat="server" Font-Size="8pt" Width="200px" 
                        AutoPostBack="True" onselectedindexchanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldRequiredCountry" ForeColor="#FF7300" runat="server"
                        ControlToValidate="ddlCountry" ErrorMessage="Country is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Preferred Language:&nbsp;&nbsp;*</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlLanguage" runat="server" Font-Size="8pt" Width="200px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldRequiredLanguage" ForeColor="#FF7300" runat="server" ControlToValidate="ddlLanguage"
                        ErrorMessage="Preferred Language is required.">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <b>Old User ID:</b>
                </td>
                <td>
                    <asp:TextBox ID="txtOldUserID" Width="200px" runat="server" Font-Size="8pt" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="titleheader">
                    <asp:Label ID="lblJobInformation" runat="server" Text="Job Information"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:ValidationSummary ID="vlSummary" runat="server" ForeColor="Red" />
                    <asp:Label ID="lblUpdateStatus" runat="server" ForeColor="#FF7300" Font-Size="10pt"
                        Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
               <td>
                    <asp:Label ID="lblBusinessArea" runat="server" Font-Size="8pt"><b>Business Area:&nbsp;&nbsp;*</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlBusinessArea" runat="server" Font-Size="8pt" Width="275px"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldRequiredBusinessArea" runat="server" ErrorMessage="Business Area is required."
                        ControlToValidate="ddlBusinessArea">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Label ID="LabelStatus" runat="server" Font-Size="8pt"><b>Status:&nbsp;&nbsp;*</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlStatus" runat="server" Font-Size="8pt" Width="250px" 
                        onselectedindexchanged="DdlStatus_SelectedIndexChanged" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldStatus" ForeColor="#FF7300" runat="server" ControlToValidate="ddlStatus"
                        ErrorMessage="Status is required.">*</asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="vldStatusDate" runat="server" ControlToValidate="ddlStatus"
                        ErrorMessage="You must enter a Term Date if the Status is set to Terminated.">*</asp:CustomValidator>
                </td>
            </tr>
             <tr>
                 <td valign="top">
                    <b><asp:Label ID="LabelHCP" runat="server" Text="HCP Facing:  *"></asp:Label></b>
                </td>
                <td valign="top">
                    <asp:DropDownList ID="ddlHCP" Font-Size="8pt" runat="server" Width="275px"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldRequiredHCP" runat="server" 
                        ErrorMessage="HCP Facing is required." ControlToValidate="ddlHCP">*</asp:RequiredFieldValidator>
                </td>
                 <td>
                    <asp:Label ID="LabelOrgCode" runat="server" Font-Size="8pt"><b>Organizantion Code:</b></asp:Label><asp:HyperLink
                        ID="lnkOrgCode" runat="server" CssClass="hintanchor" NavigateUrl="#">[?]</asp:HyperLink>
                </td>
                <td>
                    <asp:DropDownList ID="ddlOrgCode" runat="server" Font-Size="8pt" Width="250px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                    <asp:Label ID="LabelCostCenter" runat="server" Font-Size="8pt"><b>Cost Center:</b></asp:Label>
                    <asp:HyperLink ID="lnkCost" runat="server" CssClass="hintanchor" NavigateUrl="#">[?]</asp:HyperLink>
                </td>
                <td style="height: 10px">
                    <asp:DropDownList ID="ddlCostCenter" Font-Size="8pt" runat="server" Width="275px"></asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="LabelPersonnelSubArea" runat="server" Font-Size="8pt"><b>Location:</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPersonnelSubArea" runat="server" Font-Size="8pt" Width="250px"></asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="LabelStartDate" runat="server" Font-Size="8pt"><b>Start Date:&nbsp;(mm/dd/yyyy)&nbsp;&nbsp;*</b></asp:Label>
                </td>
                <td>
                    <uc1:RequiredDateTimeControl ID="txtHireDate" runat="server" />
                </td>
                <td>
                    <asp:Label ID="LabelTermDate" runat="server" Font-Size="8pt"><b>Term Date:&nbsp;(mm/dd/yyyy)</b></asp:Label>
                    <asp:CustomValidator ID="vldTermHire" runat="server" ControlToValidate="ddlStatus"
                        ErrorMessage="The Term Date must not be earlier than the Hire Date.">*</asp:CustomValidator>
                </td>
                <td>
                    <uc1:DateTimeControl ID="txtTermDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelPosition" runat="server" Font-Size="8pt"><b>Position:&nbsp;&nbsp;*</b></asp:Label><asp:HyperLink
                        ID="lnkPosition" runat="server" CssClass="hintanchor" NavigateUrl="#">[?]</asp:HyperLink>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPositionTitle" runat="server" Font-Size="8pt" Width="275px"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldPositionTitle" runat="server" ErrorMessage="Position is required."
                        ControlToValidate="ddlPositionTitle">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Label ID="lblFunction" runat="server" Font-Size="8pt"><b>Function:&nbsp;&nbsp;*</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlFunction" runat="server" Font-Size="8pt" Width="250px" AutoPostBack="True"
                        OnSelectedIndexChanged="DdlFunction_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="vldRequiredFunction" runat="server" ErrorMessage="Function is required."
                        ControlToValidate="ddlFunction">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:Label ID="LabelSupervisorID" runat="server" Font-Size="8pt"><b>Supervisor Employee #:&nbsp;&nbsp;*</b></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox ID="txtSupervisorEmployeeID" Width="270px" runat="server" Font-Size="8pt"
                        MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldSupervisorEmployeeID" runat="server" ControlToValidate="txtSupervisorEmployeeID"
                        ErrorMessage="Supervisor Employee # required.">*</asp:RequiredFieldValidator>
                </td>
                <td valign="top">
                    <asp:Label ID="LabelSupervisorName" runat="server" Font-Size="8pt"><b>Supervisor Name:&nbsp;&nbsp;*</b></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox ID="txtSupervisorName"  Width="247px" runat="server" Font-Size="8pt"
                        MaxLength="75"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="vldRequiredSupervisorName" runat="server" ControlToValidate="txtSupervisorName"
                        ErrorMessage="Supervisor name is required.">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                 <td></td>
                 <td></td>
                 <td><asp:Label ID="lblElig" Font-Bold="true" runat="server" Text="Eligible for Rehire:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ckReHire" Font-Size="8pt" runat="server" Width="250px">
                        <asp:ListItem Value="">--Select--</asp:ListItem>
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="valReHire" runat="server" 
                        ErrorMessage="You must enter whether or not the person can be rehired." ControlToValidate="ckReHire">*</asp:RequiredFieldValidator>
                </td>
           </tr>
           <tr>
                <td>
                    <b><asp:Label ID="LabelCompanyName" runat="server" Text="Contracting Company Name:"></asp:Label></b>
                </td>
                <td>
                    <asp:TextBox ID="txtCompanyName" Width="270px" runat="server" Font-Size="8pt" 
                        MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <b><asp:Label ID="LabelKronos" runat="server" Text="KRONOS Number:"></asp:Label></b>
                </td>
                <td>
                    <asp:TextBox ID="txtKronosNumber"  Width="247px" runat="server" Font-Size="8pt" MaxLength="50"></asp:TextBox>
                </td>
           </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEmployeeType" runat="server" Font-Size="8pt" ToolTip="If non-employee is regular full-time or regular part-time"><b>RFT/RPT:</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEmployeeType" runat="server" Font-Size="8pt" Width="275px">
                        <asp:ListItem Value="">-- Select --</asp:ListItem>
                        <asp:ListItem Value="Regular Full-time">Regular Full-time</asp:ListItem>
                        <asp:ListItem Value="Regular Part-time">Regular Part-time</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="LabelShift" runat="server" Font-Size="8pt"><b>Shift:</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlShift" runat="server" Font-Size="8pt" Width="250px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <b><asp:Label ID="LabelFTtoNE" runat="server" Text="Full-time to Non-Employee:"  ToolTip="Person converted from a Full-time employee to a Non-Employee."></asp:Label></b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlFTtoNE" Font-Size="8pt" runat="server" Width="275px">
                        <asp:ListItem Value="">-- Select --</asp:ListItem>
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <b><asp:Label ID="LabelNEtoFT" runat="server" Text="Non-Employee to Full-time:" 
                        ToolTip="Person converted from a Non-Employee to a Full-time employee."></asp:Label></b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlNEtoFT" Font-Size="8pt" runat="server" Width="250px">
                        <asp:ListItem Value="">-- Select --</asp:ListItem>
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLegacyPositionDate" runat="server" Font-Size="8pt"><b>Job Effective Date:</b>&nbsp;(mm/dd/yyyy)<br />(for job transfers only)</asp:Label>
                </td>
                <td>
                    <uc1:DateTimeControl ID="txtLegacyPositionDate" runat="server" />
                     <asp:CustomValidator ID="vldLegacyPositionDate" runat="server" ControlToValidate="ddlStatus"
                        ErrorMessage="The Job Effective Date must not be earlier than the Hire Date.">*</asp:CustomValidator>
               </td>
                <td>
                    <asp:Label ID="lblLegacyJobDescription" runat="server" Font-Size="8pt"><b>Legacy Job Description</b><br />(for job transfers only)</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlLegacyPosition" runat="server" Font-Size="8pt" Width="250px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCertificationSent" runat="server" Font-Size="8pt"><b>Debarred List Certification Sent:</b><br />(mm/dd/yyyy)</asp:Label>
                </td>
                <td>
                    <uc1:DateTimeControl ID="txtCertificationSent" runat="server" />
                </td>
                <td>
                    <asp:Label ID="lblCertificationReceived" runat="server" Font-Size="8pt"><b>Debarred List Certification Received:</b>&nbsp;(mm/dd/yyyy)</asp:Label>
                </td>
                <td>
                    <uc1:DateTimeControl ID="txtCertificationReceived" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelNotes" runat="server" Font-Size="8pt"><b>Notes:</b></asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtNotes" Width="700px" runat="server" Font-Size="8pt"
                        Height="50px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table class="PageTable">
            <tr>
                <td colspan="4" class="titleheader" style="height: 18px">
                    <asp:Label ID="LabelSalesHeader" runat="server">Sales Information </asp:Label>
                </td>
            </tr>
            <tr>
                 <td>
                    <asp:Label ID="LabelSalesDistrict" runat="server" Font-Size="8pt"><b>Sales District:</b></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSalesDistrict" runat="server" Font-Size="8pt" MaxLength="50"
                        Width="200px"></asp:TextBox>
                </td>
                 <td>
                    <asp:Label ID="LabelTPS" runat="server" Font-Size="8pt"><b>TPS:&nbsp;&nbsp;</b></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTPS" runat="server" Font-Size="8pt" Width="200px">
                        <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                        <asp:ListItem Text="Level 1" Value="Level 1"></asp:ListItem>
                        <asp:ListItem Text="Level 2" Value="Level 2"></asp:ListItem>
                        <asp:ListItem Text="Level 3" Value="Level 3"></asp:ListItem>
                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                    </asp:DropDownList>
                </td>
           </tr>
            <tr>
                <td style="width:173; vertical-align:top">
                    <asp:Label ID="LabelSalesRepType" runat="server" Font-Size="8pt"><b>Product Line:</b></asp:Label>
                </td>
                <td style="width:269; vertical-align:top"">
                    <asp:DropDownList ID="ddlSalesRepType" runat="server" Font-Size="8pt" Width="200px">
                    </asp:DropDownList>
                </td>
                 <td style="width:160; vertical-align:top">
                    <asp:Label ID="LabelSalesRepID" runat="server" Font-Size="8pt"><b>Sales Rep ID(s):</b></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtRepID"
                        runat="server" ErrorMessage="Sales Rep ID is required.">*</asp:RequiredFieldValidator><br />
                    <asp:Label ID="LabelSalesRepIDNote" runat="server" Font-Size="6pt"><b>Click 'Add' if more than one Sales Rep ID</b></asp:Label>
                </td>
                <td style="vertical-align:top">
                    <asp:TextBox ID="txtRepID" Width="68px" runat="server" Font-Size="8pt" MaxLength="10"></asp:TextBox>
                    <asp:Button ID="btnAdd" CausesValidation="false" runat="server" CssClass="buttonsGrid"
                        Text="Add" OnClick="BtnAdd_Click" />
                    <asp:GridView ID="gvSalesRepID" ShowHeader="false" runat="server" DataKeyNames="repID"
                        AutoGenerateColumns="false" ShowFooter="false" Width="135px" CellSpacing="0"
                        AllowPaging="false" OnRowDeleting="GV_Clear">
                        <RowStyle BorderStyle="None" BorderColor="white" BackColor="white" HorizontalAlign="left" />
                        <HeaderStyle BackColor="#dcdcdc" ForeColor="#00008B" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundField HeaderText="Rep ID" DataField="RepID" ItemStyle-Width="73px" />
                            <asp:TemplateField HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <asp:Button ID="btnClear" runat="server" CssClass="buttonsGrid" CausesValidation="false"
                                        CommandName="Delete" Text="Clear" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btnAdd" CausesValidation="false" runat="server" CssClass="buttonsGrid"
                                        Text="Add" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>   
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelRegion" runat="server" Font-Size="8pt"><b>Region:</b></asp:Label>
                </td>
                <td>
                   <asp:DropDownList ID="ddlRegion" runat="server" Font-Size="8pt" Width="200px"></asp:DropDownList>
                </td>
            
            </tr>
        </table>
        <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td colspan="4">
                    * - indicates the field is required.
                </td>
            </tr>
            <tr>
                <td style="width:18%">
                    &nbsp;
                </td>
                <td colspan="2">
                    <asp:ValidationSummary ID="vldSummary2" runat="server" ForeColor="Red" />
                    <asp:Label ID="lblUpdateStatus2" runat="server" ForeColor="#FF7300" Font-Size="10pt"
                        Font-Bold="True"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width:18%">
                    &nbsp;
                </td>
                <td style="width:28%">
                    &nbsp;
                </td>
                <td style="width:17%">
                    <div style="text-align: center">
                        <uc2:WaitButton ID="btnSubmit2" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                            OnClick="BtnSubmit_Click" Text="Submit" WaitText="Processing..."></uc2:WaitButton></div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>        
    </div>
</asp:Content>
