// --------------------------------------------------------------
// <copyright file="ExcelPackage.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007  Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.IO;
    using System.IO.Packaging;
    using System.Xml;

    /// <summary>
    /// Represents an Excel 2007 XLSX file package.  Opens the file and provides access
    /// to all the components (workbook, worksheets, properties etc.).
    /// </summary>
    public class ExcelPackage : IDisposable
    {
        /// <summary>
        /// Provides access to the main schema used by all Excel components
        /// </summary>
        protected internal const string SchemaMain = @"http://schemas.openxmlformats.org/spreadsheetml/2006/main";

        /// <summary>
        /// Provides access to the relationship schema
        /// </summary>
        protected internal const string SchemaRelationships = @"http://schemas.openxmlformats.org/officeDocument/2006/relationships";

        /// <summary>
        /// The parent package.
        /// </summary>
        private Package myPackage;

        /// <summary>
        /// The output folder path.
        /// </summary>
        private string myOutputFolderPath;

        /// <summary>
        /// The workbook.
        /// </summary>
        private ExcelWorkbook myWorkbook;

        /// <summary>
        /// Setting DebugMode to true will cause the Save method to write the 
        /// raw XML components to the same folder as the output Excel file
        /// </summary>
        private bool debugMode = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelPackage"/> class.
        /// </summary>
        /// <param name="newFile">The new file.</param>
        public ExcelPackage(FileInfo newFile)
        {
            this.myOutputFolderPath = newFile.DirectoryName;
            if (newFile.Exists)
            {
                //// open the existing package
                this.myPackage = Package.Open(newFile.FullName, FileMode.Open, FileAccess.ReadWrite);
            }
            else
            {
                //// create a new package and add the main workbook.xml part
                this.myPackage = Package.Open(newFile.FullName, FileMode.Create, FileAccess.ReadWrite);

                //// save a temporary part to create the default application/xml content type
                Uri uriDefaultContentType = new Uri("/default.xml", UriKind.Relative);
                PackagePart partTemp = this.myPackage.CreatePart(uriDefaultContentType, "application/xml");

                XmlDocument workbook = this.Workbook.WorkbookXml; //// this will create the workbook xml in the package

                //// create the relationship to the main part
                this.myPackage.CreateRelationship(this.Workbook.WorkbookUri, TargetMode.Internal, SchemaRelationships + "/officeDocument");

                //// remove the temporary part that created the default xml content type
                this.myPackage.DeletePart(uriDefaultContentType);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelPackage"/> class.
        /// WARNING: If newFile exists, it is deleted!
        /// </summary>
        /// <param name="newFile">The new file.</param>
        /// <param name="template">The template.</param>
        public ExcelPackage(FileInfo newFile, FileInfo template)
        {
            this.myOutputFolderPath = newFile.DirectoryName;
            if (template.Exists)
            {
                if (newFile.Exists)
                {
                    try
                    {
                        newFile.Delete();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("ExcelPackage Error: Target file already exists and is locked.", ex);
                    }
                }

                newFile = template.CopyTo(newFile.FullName);
                newFile.IsReadOnly = false;

                this.myPackage = Package.Open(newFile.FullName, FileMode.Open, FileAccess.ReadWrite);
            }
            else
            {
                throw new Exception("ExcelPackage Error: Passed invalid TemplatePath to Excel Template");
            }
        }

        /// <summary>
        /// Gets a reference to the file package
        /// </summary>
        public Package Package
        {
            get
            {
                return this.myPackage;
            }
        }

        /// <summary>
        /// Gets a reference to the workbook component within the package.
        /// All worksheets and cells can be accessed through the workbook.
        /// </summary>
        public ExcelWorkbook Workbook
        {
            get
            {
                if (this.myWorkbook == null)
                {
                    this.myWorkbook = new ExcelWorkbook(this);
                }

                return this.myWorkbook;
            }
        }

        /// <summary>
        /// Closes the package.
        /// </summary>
        public void Dispose()
        {
            this.myPackage.Close();
        }

        /// <summary>
        /// Saves all the components back into the package.
        /// This method recursively calls the Save method on all sub-components.
        /// </summary>
        public void Save()
        {
            this.Workbook.Save();
        }

        /// <summary>
        /// Adds additional schema attributes to the root element
        /// </summary>
        /// <param name="root">The root element</param>
        /// <param name="schema">The schema to apply</param>
        /// <param name="nameSpace">The namespace of the schema</param>
        protected internal static void AddSchemaAttribute(XmlElement root, string schema, string nameSpace)
        {
            XmlAttribute nameSpaceAttribute = root.OwnerDocument.CreateAttribute("xmlns", nameSpace, @"http://www.w3.org/2000/xmlns/");
            nameSpaceAttribute.Value = schema;
            root.Attributes.Append(nameSpaceAttribute);
        }

        /// <summary>
        /// Adds additional schema attributes to the root element
        /// </summary>
        /// <param name="root">The root element</param>
        /// <param name="schema">The schema to apply</param>
        protected internal static void AddSchemaAttribute(XmlElement root, string schema)
        {
            XmlAttribute nameSpaceAttribute = root.OwnerDocument.CreateAttribute("xmlns");
            nameSpaceAttribute.Value = schema;
            root.Attributes.Append(nameSpaceAttribute);
        }

        /// <summary>
        /// Writes a debug file to the output folder, but only if DebugMode = true
        /// </summary>
        /// <param name="xmlDoc">The XmlDocument to save to the file system</param>
        /// <param name="subFolder">The subfolder in which the file is to be saved</param>
        /// <param name="fileName">The name of the file to save.</param>
        protected internal void WriteDebugFile(XmlDocument xmlDoc, string subFolder, string fileName)
        {
            if (this.debugMode)
            {
                DirectoryInfo dir = new DirectoryInfo(this.myOutputFolderPath + "/" + subFolder);
                if (!dir.Exists)
                {
                    dir.Create();
                }

                FileInfo file = new FileInfo(this.myOutputFolderPath + "/" + subFolder + "/" + fileName);
                if (file.Exists)
                {
                    file.IsReadOnly = false;
                    file.Delete();
                }

                xmlDoc.Save(file.FullName);
            }
        }

        /// <summary>
        /// Obtains the Uri to a shared part (e.g. sharedstrings.xml)
        /// </summary>
        /// <param name="uriParent">Uri to the parent component</param>
        /// <param name="relationship">The relationship to the parent component</param>
        /// <returns>The Uri to a shared part</returns>
        protected internal Uri GetSharedUri(Uri uriParent, string relationship)
        {
            Uri uriShared = null;
            PackagePart partParent = this.myPackage.GetPart(uriParent);
            ////  Get the Uri to the shared part
            foreach (System.IO.Packaging.PackageRelationship tmprelationship in partParent.GetRelationshipsByType(SchemaRelationships + "/" + relationship))
            {
                uriShared = PackUriHelper.ResolvePartUri(uriParent, tmprelationship.TargetUri);
                break;  ////  There should only be one shared resource
            }

            return uriShared;
        }

        /// <summary>
        /// Saves the XmlDocument into the package at the specified Uri.
        /// </summary>
        /// <param name="uriPart">The Uri of the component</param>
        /// <param name="xmlPart">The XmlDocument to save</param>
        protected internal void SavePart(Uri uriPart, XmlDocument xmlPart)
        {
            PackagePart partPack = this.myPackage.GetPart(uriPart);
            xmlPart.Save(partPack.GetStream(FileMode.Create, FileAccess.Write));
        }

        /// <summary>
        /// Obtains the XmlDocument from the package referenced by the Uri
        /// </summary>
        /// <param name="uriPart">The Uri to the component</param>
        /// <returns>The XmlDocument of the component</returns>
        protected internal XmlDocument GetXmlFromUri(Uri uriPart)
        {
            XmlDocument xlsPart = new XmlDocument();
            PackagePart packPart = this.myPackage.GetPart(uriPart);
            xlsPart.Load(packPart.GetStream());
            return xlsPart;
        }
    }
}