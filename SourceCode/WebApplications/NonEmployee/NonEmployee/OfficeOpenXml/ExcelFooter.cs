﻿// ------------------------------------------------------------------
// <copyright file="ExcelFooter.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007  Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.Xml;

    /// <summary>
    /// Helper class for ExcelHeaderFooter - simply stores the three header or footer
    /// text strings. 
    /// </summary>
    public class ExcelHeaderFooterText
    {
        /// <summary>
        /// Sets the text to appear on the left hand side of the header (or footer) on the worksheet.
        /// </summary>
        private string myLeftAlignedText = null;

        /// <summary>
        /// Sets the text to appear on the right hand side of the header (or footer) on the worksheet.
        /// </summary>
        private string myRightAlignedText = null;

        /// <summary>
        /// Sets the text to appear in the center of the header (or footer) on the worksheet.
        /// </summary>
        private string myCenteredText = null;

        /// <summary>
        /// Gets or sets the left aligned text.
        /// </summary>
        /// <value>The left aligned text.</value>
        public string LeftAlignedText
        {
            get 
            { 
                return this.myLeftAlignedText; 
            }

            set 
            { 
                this.myLeftAlignedText = value; 
            }
        }

        /// <summary>
        /// Gets or sets the centered text.
        /// </summary>
        /// <value>The centered text.</value>
        public string CenteredText
        {
            get 
            { 
                return this.myCenteredText; 
            }

            set 
            { 
                this.myCenteredText = value; 
            }
        }

        /// <summary>
        /// Gets or sets the right aligned text.
        /// </summary>
        /// <value>The right aligned text.</value>
        public string RightAlignedText
        {
            get 
            { 
                return this.myRightAlignedText; 
            }

            set 
            { 
                this.myRightAlignedText = value; 
            }
        }
    }
}