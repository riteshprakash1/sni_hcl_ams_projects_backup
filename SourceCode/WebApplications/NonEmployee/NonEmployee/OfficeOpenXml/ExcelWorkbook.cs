// -----------------------------------------------------------------
// <copyright file="ExcelWorkbook.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -----------------------------------------------------------------
/* 
 * You may amend and distribute as you like, but don't remove this header!
 * 
 * ExcelPackage provides server-side generation of Excel 2007 spreadsheets.
 * See http://www.codeplex.com/ExcelPackage for details.
 * 
 * Copyright 2007 Dr John Tunnicliffe 
 * mailto:dr.john.tunnicliffe@btinternet.com
 * All rights reserved.
 * 
 * ExcelPackage is an Open Source project provided under the 
 * GNU General Public License (GPL) as published by the 
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * The GNU General Public License can be viewed at http://www.opensource.org/licenses/gpl-license.php
 * If you unfamiliar with this license or have questions about it, here is an http://www.gnu.org/licenses/gpl-faq.html
 * 
 * The code for this project may be used and redistributed by any means PROVIDING it is 
 * not sold for profit without the author's written consent, and providing that this notice 
 * and the author's name and all copyright notices remain intact.
 * 
 * All code and executables are provided "as is" with no warranty either express or implied. 
 * The author accepts no liability for any damage or loss of business that this product may cause.
 */

/*
 * Code change notes:
 * 
 * Author                            Change                        Date
 * ******************************************************************************
 * John Tunnicliffe        Initial Release        01-Jan-2007
 * ******************************************************************************
 */
namespace OfficeOpenXml
{
    using System;
    using System.IO;
    using System.IO.Packaging;
    using System.Xml;

    /// <summary>
    /// Represents the possible workbook calculation modes
    /// </summary>
    public enum ExcelCalcMode
    {
        /// <summary>
        /// Set the calculation mode to Automatic
        /// </summary>
        Automatic,

        /// <summary>
        /// Set the calculation mode to AutomaticNoTable
        /// </summary>
        AutomaticNoTable,

        /// <summary>
        /// Set the calculation mode to Manual
        /// </summary>
        Manual
    }

    /// <summary>
    /// Represents the Excel workbook and provides access to all the 
    /// document properties and worksheets within the workbook.
    /// </summary>
    public class ExcelWorkbook
    {
        /// <summary>
        /// My excel package.
        /// </summary>
        private ExcelPackage myXlPackage;

        /// <summary>
        /// My uri workbootk.
        /// </summary>
        private Uri myUriWorkbook = new Uri("/xl/workbook.xml", UriKind.Relative);

        /// <summary>
        /// My uri shared strings.
        /// </summary>
        private Uri myUriSharedStrings = new Uri("/xl/sharedStrings.xml", UriKind.Relative);

        /// <summary>
        /// My uri styles.
        /// </summary>
        private Uri myUriStyles = new Uri("/xl/styles.xml", UriKind.Relative);

        /// <summary>
        /// My uri calc chain.
        /// </summary>
        private Uri myUriCalcChain = new Uri("/xl/calcChain.xml", UriKind.Relative);

        /// <summary>
        /// My XMLworkbook.
        /// </summary>
        private XmlDocument myXmlWorkbook;

        /// <summary>
        /// My xml shared strings.
        /// </summary>
        private XmlDocument myXmlSharedStrings;

        /// <summary>
        /// My XML styles.
        /// </summary>
        private XmlDocument myXmlStyles;

        /// <summary>
        /// My worksheets.
        /// </summary>
        private ExcelWorksheets myWorksheets;

        /// <summary>
        /// My namespace manager.
        /// </summary>
        private XmlNamespaceManager myNsManager;

        /// <summary>
        /// My properties.
        /// </summary>
        private OfficeProperties myProperties;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelWorkbook"/> class.
        /// </summary>
        /// <param name="xlsPackage">The XLS package.</param>
        protected internal ExcelWorkbook(ExcelPackage xlsPackage)
        {
            this.myXlPackage = xlsPackage;
            ////  Create a NamespaceManager to handle the default namespace, 
            ////  and create a prefix for the default namespace:
            NameTable nt = new NameTable();
            this.myNsManager = new XmlNamespaceManager(nt);
            this.myNsManager.AddNamespace("d", ExcelPackage.SchemaMain);
        }

        /// <summary>
        /// Gets access to all the worksheets in the workbook.
        /// </summary>
        public ExcelWorksheets Worksheets
        {
            get
            {
                if (this.myWorksheets == null)
                {
                    this.myWorksheets = new ExcelWorksheets(this.myXlPackage);
                }

                return this.myWorksheets;
            }
        }

        /// <summary>
        /// Gets access to the XML data representing the workbook in the package.
        /// </summary>
        public XmlDocument WorkbookXml
        {
            get
            {
                if (this.myXmlWorkbook == null)
                {
                    if (this.myXlPackage.Package.PartExists(this.WorkbookUri))
                    {
                        this.myXmlWorkbook = this.myXlPackage.GetXmlFromUri(this.WorkbookUri);
                    }
                    else
                    {
                        //// create a new workbook part and add to the package
                        PackagePart partWorkbook = this.myXlPackage.Package.CreatePart(this.WorkbookUri, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml");

                        //// create the workbook
                        this.myXmlWorkbook = new XmlDocument();
                        //// create the workbook tag
                        XmlElement tagWorkbook = this.myXmlWorkbook.CreateElement("workbook", ExcelPackage.SchemaMain);
                        //// Add the relationships namespace
                        ExcelPackage.AddSchemaAttribute(tagWorkbook, ExcelPackage.SchemaRelationships, "r");
                        this.myXmlWorkbook.AppendChild(tagWorkbook);

                        //// create the bookViews tag
                        XmlElement bookViews = this.myXmlWorkbook.CreateElement("bookViews", ExcelPackage.SchemaMain);
                        tagWorkbook.AppendChild(bookViews);
                        XmlElement workbookView = this.myXmlWorkbook.CreateElement("workbookView", ExcelPackage.SchemaMain);
                        bookViews.AppendChild(workbookView);

                        //// create the sheets tag
                        XmlElement tagSheets = this.myXmlWorkbook.CreateElement("sheets", ExcelPackage.SchemaMain);
                        tagWorkbook.AppendChild(tagSheets);

                        //// save it to the package
                        StreamWriter streamWorkbook = new StreamWriter(partWorkbook.GetStream(FileMode.Create, FileAccess.Write));
                        this.myXmlWorkbook.Save(streamWorkbook);
                        streamWorkbook.Close();
                        this.myXlPackage.Package.Flush();
                    }
                }

                return this.myXmlWorkbook;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating access to the XML data representing the styles in the package. 
        /// </summary>
        public XmlDocument StylesXml
        {
            get
            {
                if (this.myXmlStyles == null)
                {
                    if (this.myXlPackage.Package.PartExists(this.StylesUri))
                    {
                        this.myXmlStyles = this.myXlPackage.GetXmlFromUri(this.StylesUri);
                    }
                    else
                    {
                        //// create a new styles part and add to the package
                        PackagePart partSyles = this.myXlPackage.Package.CreatePart(this.StylesUri, @"application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml");

                        //// create the style sheet
                        this.myXmlStyles = new XmlDocument();
                        XmlElement tagStylesheet = this.myXmlStyles.CreateElement("styleSheet", ExcelPackage.SchemaMain);
                        this.myXmlStyles.AppendChild(tagStylesheet);
                        //// create the fonts tag
                        XmlElement tagFonts = this.myXmlStyles.CreateElement("fonts", ExcelPackage.SchemaMain);
                        tagFonts.SetAttribute("count", "1");
                        tagStylesheet.AppendChild(tagFonts);
                        //// create the font tag
                        XmlElement tagFont = this.myXmlStyles.CreateElement("font", ExcelPackage.SchemaMain);
                        tagFonts.AppendChild(tagFont);
                        //// create the sz tag
                        XmlElement tagSz = this.myXmlStyles.CreateElement("sz", ExcelPackage.SchemaMain);
                        tagSz.SetAttribute("val", "11");
                        tagFont.AppendChild(tagSz);
                        //// create the name tag
                        XmlElement tagName = this.myXmlStyles.CreateElement("name", ExcelPackage.SchemaMain);
                        tagName.SetAttribute("val", "Calibri");
                        tagFont.AppendChild(tagName);
                        //// create the cellStyleXfs tag
                        XmlElement tagCellStyleXfs = this.myXmlStyles.CreateElement("cellStyleXfs", ExcelPackage.SchemaMain);
                        tagCellStyleXfs.SetAttribute("count", "1");
                        tagStylesheet.AppendChild(tagCellStyleXfs);
                        //// create the xf tag
                        XmlElement tagXf = this.myXmlStyles.CreateElement("xf", ExcelPackage.SchemaMain);
                        tagXf.SetAttribute("numFmtId", "0");
                        tagXf.SetAttribute("fontId", "0");
                        tagCellStyleXfs.AppendChild(tagXf);
                        //// create the cellXfs tag
                        XmlElement tagCellXfs = this.myXmlStyles.CreateElement("cellXfs", ExcelPackage.SchemaMain);
                        tagCellXfs.SetAttribute("count", "1");
                        tagStylesheet.AppendChild(tagCellXfs);
                        //// create the xf tag
                        XmlElement tagXf2 = this.myXmlStyles.CreateElement("xf", ExcelPackage.SchemaMain);
                        tagXf2.SetAttribute("numFmtId", "0");
                        tagXf2.SetAttribute("fontId", "0");
                        tagXf2.SetAttribute("xfId", "0");
                        tagCellXfs.AppendChild(tagXf2);

                        //// save it to the package
                        StreamWriter streamStyles = new StreamWriter(partSyles.GetStream(FileMode.Create, FileAccess.Write));
                        this.myXmlStyles.Save(streamStyles);
                        streamStyles.Close();
                        this.myXlPackage.Package.Flush();

                        //// create the relationship between the workbook and the new shared strings part
                        this.myXlPackage.Workbook.Part.CreateRelationship(this.StylesUri, TargetMode.Internal, ExcelPackage.SchemaRelationships + "/styles");
                        this.myXlPackage.Package.Flush();
                    }
                }

                return this.myXmlStyles;
            }

            set
            {
                this.myXmlStyles = value;
            }
        }

        /// <summary>
        /// Gets access to the office document properties
        /// </summary>
        public OfficeProperties Properties
        {
            get
            {
                if (this.myProperties == null)
                {
                    this.myProperties = new OfficeProperties(this.myXlPackage);
                }

                return this.myProperties;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating you to set the calculation mode for the workbook.
        /// </summary>
        public ExcelCalcMode CalcMode
        {
            get
            {
                ExcelCalcMode retValue = ExcelCalcMode.Automatic;
                ////  Retrieve the calcMode attribute in the calcPr element.
                XmlNode node = this.WorkbookXml.SelectSingleNode("//d:calcPr", this.myNsManager);
                if (node != null)
                {
                    XmlAttribute attr = node.Attributes["calcMode"];
                    if (attr != null)
                    {
                        switch (attr.Value)
                        {
                            case "auto":
                                retValue = ExcelCalcMode.Automatic;
                                break;
                            case "autoNoTable":
                                retValue = ExcelCalcMode.AutomaticNoTable;
                                break;
                            case "manual":
                                retValue = ExcelCalcMode.Manual;
                                break;
                        }
                    }
                }

                return retValue;
            }

            set
            {
                XmlElement element = (XmlElement)this.WorkbookXml.SelectSingleNode("//d:calcPr", this.myNsManager);
                ////if (element == null)
                ////{
                ////  //// create the element
                ////  element = WorkbookXml.CreateElement(
                ////}
                string actualValue = "auto";  //// default
                ////  Set the value of the attribute:
                switch (value)
                {
                    case ExcelCalcMode.Automatic:
                        actualValue = "auto";
                        break;
                    case ExcelCalcMode.AutomaticNoTable:
                        actualValue = "autoNoTable";
                        break;
                    case ExcelCalcMode.Manual:
                        actualValue = "manual";
                        break;
                }

                element.SetAttribute("calcMode", actualValue);
            }
        }

        /// <summary>
        /// Gets The Uri to the workbook in the package
        /// </summary>
        protected internal Uri WorkbookUri
        {
            get
            {
                return this.myUriWorkbook;
            }
        }

        /// <summary>
        /// Gets The Uri to the styles.xml in the package
        /// </summary>
        protected internal Uri StylesUri
        {
            get
            {
                return this.myUriStyles;
            }
        }

        /// <summary>
        /// Gets The Uri to the shared strings file
        /// </summary>
        protected internal Uri SharedStringsUri
        {
            get
            {
                return this.myUriSharedStrings;
            }
        }

        /// <summary>
        /// Gets a reference to the workbook's part within the package
        /// </summary>
        protected internal PackagePart Part
        {
            get
            {
                return this.myXlPackage.Package.GetPart(this.WorkbookUri);
            }
        }

        /// <summary>
        /// Gets access to the XML data representing the shared strings in the package.
        /// For internal use only!
        /// </summary>
        protected internal XmlDocument SharedStringsXml
        {
            get
            {
                if (this.myXmlSharedStrings == null)
                {
                    if (this.myXlPackage.Package.PartExists(this.SharedStringsUri))
                    {
                        this.myXmlSharedStrings = this.myXlPackage.GetXmlFromUri(this.SharedStringsUri);
                    }
                    else
                    {
                        //// create a new sharedStrings part and add to the package
                        PackagePart partStrings = this.myXlPackage.Package.CreatePart(this.SharedStringsUri, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml");

                        //// create the shared strings xml doc (with no entries in it)
                        this.myXmlSharedStrings = new XmlDocument();
                        XmlElement tagSst = this.myXmlSharedStrings.CreateElement("sst", ExcelPackage.SchemaMain);
                        tagSst.SetAttribute("count", "0");
                        tagSst.SetAttribute("uniqueCount", "0");
                        this.myXmlSharedStrings.AppendChild(tagSst);

                        //// save it to the package
                        StreamWriter streamStrings = new StreamWriter(partStrings.GetStream(FileMode.Create, FileAccess.Write));
                        this.myXmlSharedStrings.Save(streamStrings);
                        streamStrings.Close();
                        this.myXlPackage.Package.Flush();

                        //// create the relationship between the workbook and the new shared strings part
                        this.myXlPackage.Workbook.Part.CreateRelationship(this.SharedStringsUri, TargetMode.Internal, ExcelPackage.SchemaRelationships + "/sharedStrings");
                        this.myXlPackage.Package.Flush();
                    }
                }

                return this.myXmlSharedStrings;
            }
        }

        /// <summary>
        /// Saves the workbook and all its components to the package.
        /// For internal use only!
        /// </summary>
        protected internal void Save()  //// Workbook Save
        {
            //// ensure we have at least one worksheet
            if (this.Worksheets.Count == 0)
            {
                throw new Exception("Workbook Save Error: the workbook must contain at least one worksheet!");
            }

            //// if the calcChain component exists, we should delete it to force Excel to recreate it
            //// when the spreadsheet is next opened
            if (this.myXlPackage.Package.PartExists(this.myUriCalcChain))
            {
                ////  there will be a relationship with the workbook, so first delete the relationship
                Uri calcChain = new Uri("calcChain.xml", UriKind.Relative);
                foreach (PackageRelationship relationship in this.myXlPackage.Workbook.Part.GetRelationships())
                {
                    if (relationship.TargetUri == calcChain)
                    {
                        this.myXlPackage.Workbook.Part.DeleteRelationship(relationship.Id);
                        break;
                    }
                }
                //// delete the calcChain component
                this.myXlPackage.Package.DeletePart(this.myUriCalcChain);
            }

            //// save the workbook
            if (this.myXmlWorkbook != null)
            {
                this.myXlPackage.SavePart(this.WorkbookUri, this.myXmlWorkbook);
                this.myXlPackage.WriteDebugFile(this.myXmlWorkbook, "xl", "workbook.xml");
            }

            //// save the properties of the workbook
            if (this.myProperties != null)
            {
                this.myProperties.Save();
            }

            //// save the style sheet
            if (this.myXmlStyles != null)
            {
                this.myXlPackage.SavePart(this.StylesUri, this.myXmlStyles);
                this.myXlPackage.WriteDebugFile(this.myXmlStyles, "xl", "styles.xml");
            }

            //// save the shared strings
            if (this.myXmlSharedStrings != null)
            {
                this.myXlPackage.SavePart(this.SharedStringsUri, this.myXmlSharedStrings);
                this.myXlPackage.WriteDebugFile(this.myXmlSharedStrings, "xl", "sharedstrings.xml");
            }

            //// save all the open worksheets
            foreach (ExcelWorksheet worksheet in this.Worksheets)
            {
                worksheet.Save();
            }
        }
    } //// end Workbook
}
