﻿// --------------------------------------------------------------
// <copyright file="Utility.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Net.Mail;
    using NonEmployee.ReportingTool;

    /// <summary>
    /// This object is used through out the web app to provide 
    /// basic functions and store common table data.
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// The sql connection in the utility.
        /// </summary>
        private SqlConnection sqlConnection1;

        /// <summary>
        /// The active directory account.
        /// </summary>
        private string strADAccount;

        /// <summary>
        /// The server string.
        /// </summary>
        private string strServer;

        //// private string strPageTitle;

        /// <summary>
        /// The domain.
        /// </summary>
        private string strDomain;

        /// <summary>
        /// This flag of whether this is a production environment.
        /// </summary>
        private bool blnIsProduction;

        /// <summary>
        /// The non-employee data table.
        /// </summary>
        private DataTable tblNonEmployee;

        /// <summary>
        /// The states data table.
        /// </summary>
        private DataTable tblStates;

        /// <summary>
        /// The countries data table.
        /// </summary>
        private DataTable tblCountries;

        /// <summary>
        /// The HCP Facing data table.
        /// </summary>
        private DataTable tblHCPFacing;

        /// <summary>
        /// The position data table.
        /// </summary>
        private DataTable tblPosition;

        /// <summary>
        /// The org code data table.
        /// </summary>
        private DataTable tblOrgCode;

        /// <summary>
        /// The status data table.
        /// </summary>
        private DataTable tblStatus;

        /// <summary>
        /// The time zone data table.
        /// </summary>
        private DataTable tblTimeZone;

        /// <summary>
        /// The region data table.
        /// </summary>
        private DataTable tblRegion;

        /// <summary>
        /// The shift data table.
        /// </summary>
        private DataTable tblShift;

        /// <summary>
        /// The sales representative type data table.
        /// </summary>
        private DataTable tblSalesRepType;

        /// <summary>
        /// The employee type data table.
        /// </summary>
        private DataTable tblEmployeeType;

        /// <summary>
        /// The Personnel Sub Areas data table.
        /// </summary>
        private DataTable tblPersonnelSubArea; ////CMB

        /// <summary>
        /// The job functions data table.
        /// </summary>
        private DataTable tblFunctions; ////CMB

        /// <summary>
        /// These are our cost centers.
        /// </summary>
        private DataTable tblCostCenters; ////CMB

        /// <summary>
        /// These are the App Roles.
        /// </summary>
        private DataTable tblAppRoles; 

        /// <summary>
        /// These are the App Users.
        /// </summary>
        private DataTable tblUsers; ////DEC

        /// <summary>
        /// These are the Languages.
        /// </summary>
        private DataTable tblLanguages; ////DEC

        /// <summary>
        /// These are the business areas.
        /// </summary>
        private DataTable tblBusinessAreas; ////DEC

        /// <summary>These are the Company Code.</summary>
        private DataTable tblCompanyCodes; ////DEC

        /// <summary>
        /// True/False is user an administrator.
        /// </summary>
        private bool blnIsAdmin = false; ////DEC

        /// <summary>
        /// True/False is user a reader.
        /// </summary>
        private bool blnIsReader = false; ////DEC

        /// <summary>
        /// Initializes a new instance of the <see cref="Utility"/> class.
        /// </summary>
        /// <param name="req">The request</param>
        /// <param name="strSession">The session.</param>
        public Utility(System.Web.HttpRequest req, string strSession)
        {
            string strConn = ConfigurationManager.AppSettings["SqlConn"].ToString();
            this.SqlConn = new SqlConnection();
            this.SqlConn.ConnectionString = strConn;

            this.SetServerVariables(req);
            if (this.SqlConn.State != ConnectionState.Open)
            {
                this.SqlConn.Open();
            }
            this.LoadDataTables();
            this.InsertSessionTracker(strSession);
            this.SqlConn.Close();
        } //// end constructor

        /// <summary>
        /// Gets or sets the SQL connection.
        /// </summary>
        /// <value>The connection.</value>
        public SqlConnection SqlConn
        {
            get
            {
                return this.sqlConnection1;
            }

            set
            {
                this.sqlConnection1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the App Users.
        /// </summary>
        /// <value>The App Users.</value>
        public DataTable TblUsers
        {
            get
            {
                return this.tblUsers;
            }

            set
            {
                this.tblUsers = value;
            }
        }

        /// <summary>
        /// Gets or sets the App Roles.
        /// </summary>
        /// <value>The App Roles.</value>
        public DataTable TblAppRoles
        {
            get
            {
                return this.tblAppRoles;
            }

            set
            {
                this.tblAppRoles = value;
            }
        }

        /// <summary>
        /// Gets or sets the functions.
        /// </summary>
        /// <value>The functions.</value>
        public DataTable TblFunctions
        {
            get
            {
                return this.tblFunctions;
            }

            set
            {
                this.tblFunctions = value;
            }
        }

        /// <summary>
        /// Gets or sets the Personnel Sub Areas.
        /// </summary>
        /// <value>The Personnel Sub Areas.</value>
        public DataTable TblPersonnelSubArea
        {
            get
            {
                return this.tblPersonnelSubArea;
            }

            set
            {
                this.tblPersonnelSubArea = value;
            }
        }

        /// <summary>
        /// Gets or sets the time zones.
        /// </summary>
        /// <value>The time zones.</value>
        public DataTable TblTimeZone
        {
            get
            {
                return this.tblTimeZone;
            }

            set
            {
                this.tblTimeZone = value;
            }
        }

        /// <summary>
        /// Gets or sets the regions.
        /// </summary>
        /// <value>The regions.</value>
        public DataTable TblRegion
        {
            get
            {
                return this.tblRegion;
            }

            set
            {
                this.tblRegion = value;
            }
        }

        /// <summary>
        /// Gets or sets the positions.
        /// </summary>
        /// <value>The positions.</value>
        public DataTable TblPosition
        {
            get
            {
                return this.tblPosition;
            }

            set
            {
                this.tblPosition = value;
            }
        }

        /// <summary>
        /// Gets or sets the org codes.
        /// </summary>
        /// <value>The org codes.</value>
        public DataTable TblOrgCode
        {
            get
            {
                return this.tblOrgCode;
            }

            set
            {
                this.tblOrgCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the shifts.
        /// </summary>
        /// <value>The shifts.</value>
        public DataTable TblShift
        {
            get
            {
                return this.tblShift;
            }

            set
            {
                this.tblShift = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the sales reps.
        /// </summary>
        /// <value>The type of the sales reps.</value>
        public DataTable TblSalesRepType
        {
            get
            {
                return this.tblSalesRepType;
            }

            set
            {
                this.tblSalesRepType = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the employees.
        /// </summary>
        /// <value>The type of the employees.</value>
        public DataTable TblEmployeeType
        {
            get
            {
                return this.tblEmployeeType;
            }

            set
            {
                this.tblEmployeeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the non-employees.
        /// </summary>
        /// <value>The non-employees.</value>
        public DataTable TblNonEmployee
        {
            get
            {
                return this.tblNonEmployee;
            }

            set
            {
                this.tblNonEmployee = value;
            }
        }

        /// <summary>
        /// Gets or sets the states.
        /// </summary>
        /// <value>The states.</value>
        public DataTable TblStates
        {
            get
            {
                return this.tblStates;
            }

            set
            {
                this.tblStates = value;
            }
        }

        /// <summary>
        /// Gets or sets the countries.
        /// </summary>
        /// <value>The countries.</value>
        public DataTable TblCountries
        {
            get
            {
                return this.tblCountries;
            }

            set
            {
                this.tblCountries = value;
            }
        }

        /// <summary>
        /// Gets or sets the statuses.
        /// </summary>
        /// <value>The statuses.</value>
        public DataTable TblStatus
        {
            get
            {
                return this.tblStatus;
            }

            set
            {
                this.tblStatus = value;
            }
        }

        /// <summary>
        /// Gets or sets the HCP Facing.
        /// </summary>
        /// <value>The HCP Facing.</value>
        public DataTable TblHCPFacing
        {
            get
            {
                return this.tblHCPFacing;
            }

            set
            {
                this.tblHCPFacing = value;
            }
        }

        /// <summary>
        /// Gets or sets the business areas.
        /// </summary>
        /// <value>The business areas.</value>
        public DataTable TblBusinessArea
        {
            get
            {
                return this.tblBusinessAreas;
            }

            set
            {
                this.tblBusinessAreas = value;
            }
        }

        /// <summary>
        /// Gets or sets the cost centers.
        /// </summary>
        /// <value>The cost centers.</value>
        public DataTable TblCostCenters
        {
            get
            {
                return this.tblCostCenters;
            }

            set
            {
                this.tblCostCenters = value;
            }
        }

        /// <summary>
        /// Gets or sets the languages.
        /// </summary>
        /// <value>The languages.</value>
        public DataTable TblLanguages
        {
            get
            {
                return this.tblLanguages;
            }

            set
            {
                this.tblLanguages = value;
            }
        }

        /// <summary>Gets or sets the CompanyCodes table.</summary>
        /// <value>The CompanyCodes table</value>
        public DataTable TblCompanyCodes
        {
            get
            {
                return this.tblCompanyCodes;
            }

            set
            {
                this.tblCompanyCodes = value;
            }
        }

        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>The server.</value>
        public string Server
        {
            get
            {
                return this.strServer;
            }

            set
            {
                this.strServer = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is production.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is production; otherwise, <c>false</c>.
        /// </value>
        public bool IsProduction
        {
            get
            {
                return this.blnIsProduction;
            }

            set
            {
                this.blnIsProduction = value;
            }
        }

        /// <summary>
        /// Gets or sets the AD account.
        /// </summary>
        /// <value>The AD account.</value>
        public string ADAccount
        {
            get
            {
                return this.strADAccount;
            }

            set
            {
                this.strADAccount = value;
            }
        }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>The domain.</value>
        public string Domain
        {
            get
            {
                return this.strDomain;
            }

            set
            {
                this.strDomain = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this user is an administrator.
        /// </summary>
        /// <value>
        ///     <c>true</c> if user is an administrator; otherwise, <c>false</c>.
        /// </value>
        public bool IsAdmin
        {
            get
            {
                return this.blnIsAdmin;
            }

            set
            {
                this.blnIsAdmin = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this user is a reader.
        /// </summary>
        /// <value>
        ///     <c>true</c> if user is a reader; otherwise, <c>false</c>.
        /// </value>
        public bool IsReader
        {
            get
            {
                return this.blnIsReader;
            }

            set
            {
                this.blnIsReader = value;
            }
        }

        /// <summary>
        /// Inserts the session tracker.
        /// </summary>
        /// <param name="strSessionID">The session ID.</param>
        public void InsertSessionTracker(string strSessionID)
        {
            SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_InsertSessionTracker";
            cmd.Connection = this.SqlConn;

            cmd.Parameters.Add(new SqlParameter("@username", this.ADAccount));
            cmd.Parameters.Add(new SqlParameter("@sessionID", strSessionID));

            int rowsUpdated = cmd.ExecuteNonQuery();
        } //// END InsertUseTrack

        /// <summary>
        /// Gets the short date.
        /// </summary>
        /// <param name="strDate">The date we want to get the short date.</param>
        /// <returns>The short date for the date.</returns>
        public string GetShortDate(string strDate)
        {
            string strShortDate = strDate;
            if (strDate != String.Empty)
            {
                string[] strDates = strDate.Split(' ');
                if (strDates.Length > 0)
                {
                    strShortDate = strDates[0];
                }
            }

            return strShortDate;
        }



        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="strTo">The to address.</param>
        /// <param name="strFrom">The from address.</param>
        /// <param name="strSubj">The subjubject.</param>
        /// <param name="strBody">The body text.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        public void SendEmail(string strTo, string strFrom, string strSubj, string strBody, bool isHtml)
        {
            MailAddress from = new MailAddress(strFrom);
            MailAddress to = new MailAddress(strTo);

            //// Specify the message content.
            MailMessage message = new MailMessage(from, to);
            Console.WriteLine("From: " + message.From.Address);

            if (ConfigurationManager.AppSettings["ccEmail"] != String.Empty)
            {
                message.CC.Add(ConfigurationManager.AppSettings["ccEmail"]);
            }

            message.IsBodyHtml = isHtml;
            message.Subject = strSubj;
            message.Body = strBody;

            SmtpClient client = new SmtpClient();
            Debug.WriteLine("client: " + client.Host);

            try
            {
                client.Send(message);
            }
            catch (Exception excm)
            {
                Debug.WriteLine(excm.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets the report parameters.
        /// </summary>
        /// <param name="reportID">The report ID.</param>
        /// <returns>Returns the list of report parameters.</returns>
        public Collection<ReportParameters> GetReportParameters(int reportID)
        {
            Collection<ReportParameters> parameterList = new Collection<ReportParameters>();
            DataTable myParameters = SqlUtility.SetTable("dbo.sp_GetReportParameters", reportID);
            foreach (DataRow dr in myParameters.Rows)
            {
                ReportParameters myParameter = new ReportParameters();
                myParameter.ParameterID = Convert.ToInt32(dr["ReportParameterID"].ToString());
                myParameter.ParameterField = dr["ParameterField"].ToString();
                myParameter.ParameterValue = dr["ParameterValue"].ToString();
                myParameter.ParameterComparison = dr["ParameterComparison"].ToString();
                parameterList.Add(myParameter);
            }

            Debug.WriteLine("Parameter Count: " + parameterList.Count.ToString() + "; reportID: " + reportID.ToString());
            return parameterList;
        }

        /// <summary>
        /// Gets the report list.
        /// </summary>
        /// <returns>Returns a list of report entities.</returns>
        public List<ReportEntity> GetReportList()
        {
            List<ReportingTool.ReportEntity> myReportList = new List<ReportingTool.ReportEntity>();
            DataTable myReports = SqlUtility.SetTable("dbo.sp_GetAllReports");
            foreach (DataRow dr in myReports.Rows)
            {
                ReportingTool.ReportEntity tmpReport = new ReportingTool.ReportEntity();
                tmpReport.ReportID = Convert.ToInt32(dr["ReportID"].ToString());
                tmpReport.ReportReportHeader = dr["ReportTitle"].ToString();
                tmpReport.ReportFooter = dr["ReportFooter"].ToString();
                tmpReport.ReportCommandText = dr["ReportSelectionQuery"].ToString();
                tmpReport.ReportOutputType = ReportingTool.ReportEntity.OutputType.file;
                tmpReport.ReportOutpath = dr["ReportOutput"].ToString();
                tmpReport.ReportOwner = dr["ReportOwner"].ToString();
                tmpReport.ReportOwnerDisplay = dr["ReportOwnerDisplay"].ToString();
                tmpReport.ReportViewableBy = dr["ReportViewableBy"].ToString();
                tmpReport.ReportConnectionString = ConfigurationManager.AppSettings["SqlConn"].ToString();

                if (tmpReport.ReportCommandText.Trim().ToLower().StartsWith("select") == true)
                {
                    tmpReport.ReportCommandType = CommandType.Text;
                }
                else
                {
                    tmpReport.ReportCommandType = CommandType.StoredProcedure;
                }

                myReportList.Add(tmpReport);
            }

            return myReportList;
        }

        /// <summary>
        /// Gets the reporting entity.
        /// </summary>
        /// <param name="reportID">The report ID.</param>
        /// <returns>The report entity.</returns>
        public ReportEntity GetReportingEntity(int reportID)
        {
            ReportEntity tmpReport = new ReportEntity();

            try
            {
                DataTable myReports = SqlUtility.SetTable("dbo.sp_GetReportByReportID", reportID);
                foreach (DataRow dr in myReports.Rows)
                {
                    tmpReport.ReportID = Convert.ToInt32(dr["ReportID"].ToString());
                    tmpReport.ReportReportHeader = dr["ReportTitle"].ToString();
                    tmpReport.ReportOwner = dr["ReportOwner"].ToString();
                    tmpReport.ReportOwnerDisplay = dr["ReportOwnerDisplay"].ToString();
                    tmpReport.ReportViewableBy = dr["ReportViewableBy"].ToString();
                    tmpReport.ReportFooter = dr["ReportFooter"].ToString();
                    tmpReport.ReportCommandText = dr["ReportSelectionQuery"].ToString();
                    tmpReport.ReportOutputType = ReportEntity.OutputType.file;
                    tmpReport.ReportOutpath = dr["ReportOutput"].ToString();
                    tmpReport.ReportConnectionString = ConfigurationManager.AppSettings["SqlConn"].ToString();

                    if (tmpReport.ReportCommandText.Trim().ToLower().StartsWith("select") == true)
                    {
                        tmpReport.ReportCommandType = CommandType.Text;
                    }
                    else
                    {
                        tmpReport.ReportCommandType = CommandType.StoredProcedure;
                    }
                }
            }
            catch
            {
            }

            return tmpReport;
        }

        /// <summary>
        /// Sets the table.
        /// </summary>
        /// <param name="sp">The stored procedure.</param>
        /// <returns>The filled datatable</returns>
        public DataTable SetTable(string sp)
        {
            SqlCommand cmdSetTable = new SqlCommand();
            cmdSetTable.CommandText = sp;
            cmdSetTable.CommandType = CommandType.StoredProcedure;
            cmdSetTable.Connection = this.SqlConn;

            if (this.SqlConn.State != ConnectionState.Open)
                this.SqlConn.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
            da.Fill(dt);
            da.Dispose();

            return dt;
        } //// END SetTable()

        
        /// <summary>
        /// Loads the data tables.
        /// </summary>
        private void LoadDataTables()
        {
            this.TblAppRoles = this.SetTable("sp_GetAppRoles");
            this.TblOrgCode = this.SetTable("sp_GetOrgCodes");
            this.TblStates = this.SetTable("sp_GetStates");
            this.TblCountries = this.SetTable("sp_GetCountries");
            this.TblStatus = this.SetTable("sp_GetStatuses");
            this.TblBusinessArea = this.SetTable("sp_GetBusinessAreas");
            this.TblTimeZone = this.SetTable("sp_GetTimeZone");
            this.TblRegion = this.SetTable("sp_GetRegions");
            this.TblShift = this.SetTable("sp_GetShifts");
            this.TblSalesRepType = this.SetTable("sp_GetSalesRepTypes");
            this.TblEmployeeType = this.SetTable("sp_GetEmployeeType");
            this.TblPosition = this.SetTable("sp_GetPositions");
            this.TblPersonnelSubArea = this.SetTable("sp_GetPersonnelSubAreas");
            this.TblFunctions = this.SetTable("sp_GetFunctions");
            this.TblCostCenters = this.SetTable("sp_GetCostCenters");
            this.TblLanguages = this.SetTable("sp_GetLanguages");
            this.TblHCPFacing = this.SetTable("sp_GetHCPFacing");
            this.TblCompanyCodes = this.SetTable("sp_GetCompanyCodes");

            this.SetAppRoleVariables();
        } //// end LoadDataTables

        private void SetAppRoleVariables()
        {
            DataTable dt = SqlUtility.SetTable("sp_GetAppUserByUsername", this.ADAccount);
            if (dt.Rows.Count > 0)
            {

                switch (dt.Rows[0]["approleID"].ToString())
                {
                    case "1":
                        this.IsAdmin = true;
                        break;
                    case "2":
                        this.IsReader = true;
                        break;
                } //// end switch
            } //// end if
        }

        /// <summary>
        /// Sets the server variables.
        /// </summary>
        /// <param name="req">The web session request.</param>
        private void SetServerVariables(System.Web.HttpRequest req)
        {
            this.Server = req.ServerVariables["SERVER_NAME"];

            string authUser = req.ServerVariables["AUTH_USER"];
            string[] arrayAuth = authUser.Split('\\');

            if (arrayAuth.Length > 1)
            {
                this.Domain = arrayAuth[0];
                this.ADAccount = arrayAuth[1];
            }
            else
            {
                this.Domain = String.Empty;
                this.ADAccount = String.Empty;
            }

            Debug.WriteLine("authUser: " + authUser + "; ADAccount: " + this.ADAccount);
        }
    } //// end class
}
