﻿// ----------------------------------------------------------------
// <copyright file="TableViewEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ----------------------------------------------------------------

namespace NonEmployee.Classes
{
    /// <summary>
    /// This is the view entry.
    /// </summary>
    public class TableViewEntity
    {
        /// <summary>
        /// This is the name of the catalog.
        /// </summary>
        private string myTableViewCatalog;

        /// <summary>
        /// This is the table or view name.
        /// </summary>
        private string myTableViewName;

        /// <summary>
        /// This is the table or view type.
        /// </summary>
        private string myTableViewType;

        /// <summary>
        /// This is the table or view schema.
        /// </summary>
        private string myTableViewSchema;

        /// <summary>
        /// Initializes a new instance of the <see cref="TableViewEntity"/> class.
        /// </summary>
        public TableViewEntity()
        {
            this.myTableViewCatalog = string.Empty;
            this.myTableViewName = string.Empty;
            this.myTableViewSchema = string.Empty;
            this.myTableViewType = string.Empty;
        }

        /// <summary>
        /// Gets or sets the table view catalog.
        /// </summary>
        /// <value>The table view catalog.</value>
        public string TableViewCatalog
        {
            get { return this.myTableViewCatalog; }
            set { this.myTableViewCatalog = value; }
        }

        /// <summary>
        /// Gets or sets the name of the table view.
        /// </summary>
        /// <value>The name of the table view.</value>
        public string TableViewName
        {
            get { return this.myTableViewName; }
            set { this.myTableViewName = value; }
        }

        /// <summary>
        /// Gets or sets the type of the table view.
        /// </summary>
        /// <value>The type of the table view.</value>
        public string TableViewType
        {
            get { return this.myTableViewType; }
            set { this.myTableViewType = value; }
        }

        /// <summary>
        /// Gets or sets the table view schema.
        /// </summary>
        /// <value>The table view schema.</value>
        public string TableViewSchema
        {
            get { return this.myTableViewSchema; }
            set { this.myTableViewSchema = value; }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.myTableViewName;
        }
    }
}
