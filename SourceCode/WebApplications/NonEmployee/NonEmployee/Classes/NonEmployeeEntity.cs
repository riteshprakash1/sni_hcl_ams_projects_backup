﻿// ------------------------------------------------------------------
// <copyright file="NonEmployeeEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace NonEmployee.Classes
{
    using System;

    /// <summary>
    /// This is the entity object for the non-employee. <br/>
    /// This is so we can compare the non-employee object against the current non-employee object.
    /// </summary>
    public class NonEmployeeEntity
    {
        /// <summary>
        /// This is the non-employee id.
        /// </summary>
        private int myNonEmployeeID;

        /// <summary>
        /// This is the non-employee status.
        /// </summary>
        private string myNonEmployeeStatus;

        /// <summary>
        /// This is the hire date.
        /// </summary>
        private DateTime myHireDate;

        /// <summary>
        /// This is the termination date.
        /// </summary>
        private DateTime myTermDate;

        /// <summary>
        /// This is the employee type code.
        /// </summary>
        private string myEmployeeTypeCode;

        /// <summary>
        /// This is the employee last name.
        /// </summary>
        private string myLastName;

        /// <summary>
        /// This is the employee first name.
        /// </summary>
        private string myFirstName;

        /// <summary>
        /// This is the employee middle name.
        /// </summary>
        private string myMidName;

        /// <summary>
        /// This is the employee nick name.
        /// </summary>
        private string myNickName;

        /// <summary>
        /// This is the employee organization code.
        /// </summary>
        private string myOrgCode;

        /// <summary>
        /// This is the employee supervisor Id.
        /// </summary>
        private string mySupervisorEmployeeID;

        /// <summary>
        /// This is the employee supervisor name.
        /// </summary>
        private string mySupervisorName;

        /// <summary>
        /// This is the employee cost code.
        /// </summary>
        private string myCostCode;

        /// <summary>
        /// This is the employee city.
        /// </summary>
        private string myCity;

        /// <summary>
        /// This is the employee state.
        /// </summary>
        private string myState;

        /// <summary>
        /// This is the employee shift code.
        /// </summary>
        private string myShiftCode;

        /// <summary>
        /// This is the Business Area ID.
        /// </summary>
        private int myBusinessAreaID;

        /// <summary>
        /// This is the Business Area Name.
        /// </summary>
        private string myBusinessAreaName;

        /// <summary>
        /// This is the Personnel Sub Area code.
        /// </summary>
        private string myPersonnelSubAreaCode;

        /// <summary>
        /// This is the Personnel SubArea Name.
        /// </summary>
        private string myPersonnelSubAreaName;

        /// <summary>
        /// This is the employee position name.
        /// </summary>
        private string myPositionName;

        /// <summary>
        /// This is the employee address.
        /// </summary>
        private string myEmailAddress;

        /// <summary>
        /// This is the sales representative id.
        /// </summary>
        private string mySalesRepID;

        /// <summary>
        /// This is the sales representative type code.
        /// </summary>
        private string mySalesRepTypeCode;

        /// <summary>
        /// This is the network id.
        /// </summary>
        private string myNetworkID;

        /// <summary>
        /// This is the region code.
        /// </summary>
        private string myRegionCode;

        /// <summary>
        /// This is the country code.
        /// </summary>
        private string myCountryCode;

        /// <summary>
        /// This is preferred phone.
        /// </summary>
        private string myPreferredPhone;

        /// <summary>
        /// This is the first street address.
        /// </summary>
        private string myStreet1;

        /// <summary>
        /// This is the second street adddress.
        /// </summary>
        private string myStreet2;

        /// <summary>
        /// This is the postal code.
        /// </summary>
        private string myPostalCode;

        ///// <summary>
        ///// This is the time zone.
        ///// </summary>
        //private string myTimeZoneCode;

        /// <summary>
        /// This is the sales district.
        /// </summary>
        private string mySalesDistrict;

        /// <summary>
        /// This was the created by flag.
        /// </summary>
        private string myCreatedBy;

        /// <summary>
        /// This is the creation date.
        /// </summary>
        private DateTime myCreateDate;

        /// <summary>
        /// This was the person who last modified the record.
        /// </summary>
        private string myLastModifiedBy;

        /// <summary>
        /// This is the date the record was last modified.
        /// </summary>
        private DateTime myLastModifiedDate;

        /// <summary>
        /// This is the old iser id?
        /// </summary>
        private string myOldUserID;

        /// <summary>
        /// This is the employee notes.
        /// </summary>
        private string myNotes;

        /// <summary>
        /// This is the employee rehire flag.
        /// </summary>
        private string myRehire;

        /// <summary>
        /// This is the contracting company.
        /// </summary>
        private string myContractOwner;

        /// <summary>
        /// This is the kronos time clock number.
        /// </summary>
        private string myKronosNumber;

        /// <summary>
        /// This is the additional user information.
        /// </summary>
        private string myAdditionalUserInfo;

        /// <summary>
        /// This is the job function id.
        /// </summary>
        private int myFunctionID;

        /// <summary>
        /// This is the other global business unit flag.
        /// </summary>
        private bool myOtherGBU;

        /// <summary>
        /// This is the language id.
        /// </summary>
        private int myLanguageID;

        /// <summary>
        /// This is the language name.
        /// </summary>
        private string myLanguageName;

        /// <summary>
        /// This is the function name.
        /// </summary>
        private string myFunctionName;

        /// <summary>
        /// This is the Sales Rep Type Desc.
        /// </summary>
        private string mySalesRepTypeDesc;

        /// <summary>
        /// This is the Region Desc.
        /// </summary>
        private string myRegionDesc;

        /// <summary>
        /// This is the tps level.
        /// </summary>
        private string myTPS;

        /// <summary>
        /// This is the HCP Facing.
        /// </summary>
        private string myHcpFacing;

        /// <summary>
        /// This is the Full time to NonEmployee.
        /// </summary>
        private string myFTtoNE;

        /// <summary>
        /// This is the NonEmployee to Full time.
        /// </summary>
        private string myNEtoFT;

        /// <summary>
        /// This is the Employee Type Description.
        /// </summary>
        private string myEmployeeTypeDesc;

        /// <summary>
        /// This is the Legacy Position.
        /// </summary>
        private string myLegacyPosition;


        /// <summary>
        /// This is the Legacy Position Date.
        /// </summary>
        private DateTime myLegacyPositionDate;

        /// <summary>
        /// This is the Certification Received Date.
        /// </summary>
        private DateTime myCertificationReceived;

        /// <summary>
        /// This is the Certification Sent Date.
        /// </summary>
        private DateTime myCertificationSent;

        /// <summary>
        /// Initializes a new instance of the <see cref="NonEmployeeEntity"/> class.
        /// </summary>
        public NonEmployeeEntity()
        {
            this.myNonEmployeeID = 0;
            this.myNonEmployeeStatus = String.Empty;
            this.myHireDate = DateTime.MinValue;
            this.myTermDate = DateTime.MinValue;
            this.myEmployeeTypeCode = String.Empty;
            this.myLastName = String.Empty;
            this.myFirstName = String.Empty;
            this.myMidName = String.Empty;
            this.myNickName = String.Empty;
            this.myOrgCode = String.Empty;
            this.mySupervisorEmployeeID = String.Empty;
            this.mySupervisorName = String.Empty;
            this.myCostCode = String.Empty;
            this.myCity = String.Empty;
            this.myState = String.Empty;
            this.myShiftCode = String.Empty;
            this.myBusinessAreaID = 0;
            this.myBusinessAreaName = String.Empty;
            this.myPersonnelSubAreaCode = String.Empty;
            this.myPersonnelSubAreaName = String.Empty;
            this.myPositionName = String.Empty;
            this.myEmailAddress = String.Empty;
            this.mySalesRepID = String.Empty;
            this.mySalesRepTypeCode = String.Empty;
            this.myNetworkID = String.Empty;
            this.myRegionCode = String.Empty;
            this.myCountryCode = String.Empty;
            this.myPreferredPhone = String.Empty;
            //this.myAlternateNumber = String.Empty;
            //this.myDaylightSavings = String.Empty;
            this.myStreet1 = String.Empty;
            this.myStreet2 = String.Empty;
            this.myPostalCode = String.Empty;
            //this.myTimeZoneCode = String.Empty;
            this.mySalesDistrict = String.Empty;
            this.myCreatedBy = String.Empty;
            this.myCreateDate = DateTime.MinValue;
            this.myLastModifiedBy = String.Empty;
            this.myLastModifiedDate = DateTime.MinValue;
            this.myOldUserID = String.Empty;
            this.myNotes = String.Empty;
            this.myRehire = String.Empty;
            this.myContractOwner = String.Empty;
            this.myKronosNumber = String.Empty;
            this.myAdditionalUserInfo = String.Empty;
            this.myFunctionID = 0;
            this.myLanguageID = 0;
            this.myTPS = String.Empty;
            this.myLanguageName = String.Empty;
            this.myFunctionName = String.Empty;
            this.mySalesRepTypeDesc = String.Empty;
            this.myRegionDesc = String.Empty;
            this.myHcpFacing = String.Empty;
            this.myFTtoNE = String.Empty;
            this.myNEtoFT = String.Empty;
            this.myCertificationReceived = DateTime.MinValue;
            this.myCertificationSent = DateTime.MinValue;
            this.myLegacyPositionDate = DateTime.MinValue;
            this.myLegacyPosition = String.Empty;

        }

        /// <summary>
        /// Gets or sets the non-employee ID.
        /// </summary>
        /// <value>The non-employee ID.</value>
        public int NonEmployeeID
        {
            get
            {
                return this.myNonEmployeeID;
            }

            set
            {
                this.myNonEmployeeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the non-employee status.
        /// </summary>
        /// <value>The non-employee status.</value>
        public string NonEmployeeStatus
        {
            get
            {
                return this.myNonEmployeeStatus;
            }

            set
            {
                this.myNonEmployeeStatus = value;
            }
        }

        /// <summary>
        /// Gets or sets the hire date.
        /// </summary>
        /// <value>The hire date.</value>
        public DateTime HireDate
        {
            get
            {
                return this.myHireDate;
            }

            set
            {
                this.myHireDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the term date.
        /// </summary>
        /// <value>The term date.</value>
        public DateTime TermDate
        {
            get
            {
                return this.myTermDate;
            }

            set
            {
                this.myTermDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the non-employee type code.
        /// </summary>
        /// <value>The non-employee type code.</value>
        public string EmployeeTypeCode
        {
            get
            {
                return this.myEmployeeTypeCode;
            }

            set
            {
                this.myEmployeeTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName
        {
            get
            {
                return this.myLastName;
            }

            set
            {
                this.myLastName = value;
            }
        }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName
        {
            get
            {
                return this.myFirstName;
            }

            set
            {
                this.myFirstName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the mid.
        /// </summary>
        /// <value>The name of the mid.</value>
        public string MidName
        {
            get
            {
                return this.myMidName;
            }

            set
            {
                this.myMidName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the nick.
        /// </summary>
        /// <value>The name of the nick.</value>
        public string NickName
        {
            get
            {
                return this.myNickName;
            }

            set
            {
                this.myNickName = value;
            }
        }

        /// <summary>
        /// Gets or sets the org code.
        /// </summary>
        /// <value>The org code.</value>
        public string OrgCode
        {
            get
            {
                return this.myOrgCode;
            }

            set
            {
                this.myOrgCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the supervisor non-employee ID.
        /// </summary>
        /// <value>The supervisor non-employee ID.</value>
        public string SupervisorEmployeeID
        {
            get
            {
                return this.mySupervisorEmployeeID;
            }

            set
            {
                this.mySupervisorEmployeeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the supervisor.
        /// </summary>
        /// <value>The name of the supervisor.</value>
        public string SupervisorName
        {
            get
            {
                return this.mySupervisorName;
            }

            set
            {
                this.mySupervisorName = value;
            }
        }

        /// <summary>
        /// Gets or sets the cost code.
        /// </summary>
        /// <value>The cost code.</value>
        public string CostCode
        {
            get
            {
                return this.myCostCode;
            }

            set
            {
                this.myCostCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city value.</value>
        public string City
        {
            get
            {
                return this.myCity;
            }

            set
            {
                this.myCity = value;
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State
        {
            get
            {
                return this.myState;
            }

            set
            {
                this.myState = value;
            }
        }

        /// <summary>
        /// Gets or sets the shift code.
        /// </summary>
        /// <value>The shift code.</value>
        public string ShiftCode
        {
            get
            {
                return this.myShiftCode;
            }

            set
            {
                this.myShiftCode = value;
            }
        }


        /// <summary>
        /// Gets or sets the personnel sub area code.
        /// </summary>
        /// <value>The personnel sub area code.</value>
        public string PersonnelSubAreaCode
        {
            get
            {
                return this.myPersonnelSubAreaCode;
            }

            set
            {
                this.myPersonnelSubAreaCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the personnel sub area name.
        /// </summary>
        /// <value>The personnel sub area name.</value>
        public string PersonnelSubAreaName
        {
            get
            {
                return this.myPersonnelSubAreaName;
            }

            set
            {
                this.myPersonnelSubAreaName = value;
            }
        }

        /// <summary>
        /// Gets or sets the business area id.
        /// </summary>
        /// <value>The business area id.</value>
        public int BusinessAreaID
        {
            get
            {
                return this.myBusinessAreaID;
            }

            set
            {
                this.myBusinessAreaID = value;
            }
        }

        /// <summary>
        /// Gets or sets the business area name.
        /// </summary>
        /// <value>The business area name.</value>
        public string BusinessAreaName
        {
            get
            {
                return this.myBusinessAreaName;
            }

            set
            {
                this.myBusinessAreaName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the position.
        /// </summary>
        /// <value>The name of the position.</value>
        public string PositionName
        {
            get
            {
                return this.myPositionName;
            }

            set
            {
                this.myPositionName = value;
            }
        }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>The email address.</value>
        public string EmailAddress
        {
            get
            {
                return this.myEmailAddress;
            }

            set
            {
                this.myEmailAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the sales representative ID.
        /// </summary>
        /// <value>The sales representative ID.</value>
        public string SalesRepID
        {
            get
            {
                return this.mySalesRepID;
            }

            set
            {
                this.mySalesRepID = value;
            }
        }

        /// <summary>
        /// Gets or sets the sales representative type code.
        /// </summary>
        /// <value>The sales representative type code.</value>
        public string SalesRepTypeCode
        {
            get
            {
                return this.mySalesRepTypeCode;
            }

            set
            {
                this.mySalesRepTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the network ID.
        /// </summary>
        /// <value>The network ID.</value>
        public string NetworkID
        {
            get
            {
                return this.myNetworkID;
            }

            set
            {
                this.myNetworkID = value;
            }
        }

        /// <summary>
        /// Gets or sets the region code.
        /// </summary>
        /// <value>The region code.</value>
        public string RegionCode
        {
            get
            {
                return this.myRegionCode;
            }

            set
            {
                this.myRegionCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        public string CountryCode
        {
            get
            {
                return this.myCountryCode;
            }

            set
            {
                this.myCountryCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the preferred phone.
        /// </summary>
        /// <value>The preferred phone.</value>
        public string PreferredPhone
        {
            get
            {
                return this.myPreferredPhone;
            }

            set
            {
                this.myPreferredPhone = value;
            }
        }

        ///// <summary>
        ///// Gets or sets the alternate number.
        ///// </summary>
        ///// <value>The alternate number.</value>
        //public string AlternateNumber
        //{
        //    get
        //    {
        //        return this.myAlternateNumber;
        //    }

        //    set
        //    {
        //        this.myAlternateNumber = value;
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the daylight savings.
        ///// </summary>
        ///// <value>The daylight savings.</value>
        //public string DaylightSavings
        //{
        //    get
        //    {
        //        return this.myDaylightSavings;
        //    }

        //    set
        //    {
        //        this.myDaylightSavings = value;
        //    }
        //}

        /// <summary>
        /// Gets or sets the street1.
        /// </summary>
        /// <value>The street1.</value>
        public string Street1
        {
            get
            {
                return this.myStreet1;
            }

            set
            {
                this.myStreet1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the street2.
        /// </summary>
        /// <value>The street2.</value>
        public string Street2
        {
            get
            {
                return this.myStreet2;
            }

            set
            {
                this.myStreet2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>The postal code.</value>
        public string PostalCode
        {
            get
            {
                return this.myPostalCode;
            }

            set
            {
                this.myPostalCode = value;
            }
        }

        ///// <summary>
        ///// Gets or sets the time zone code.
        ///// </summary>
        ///// <value>The time zone code.</value>
        //public string TimeZoneCode
        //{
        //    get
        //    {
        //        return this.myTimeZoneCode;
        //    }

        //    set
        //    {
        //        this.myTimeZoneCode = value;
        //    }
        //}

        /// <summary>
        /// Gets or sets the sales district.
        /// </summary>
        /// <value>The sales district.</value>
        public string SalesDistrict
        {
            get
            {
                return this.mySalesDistrict;
            }

            set
            {
                this.mySalesDistrict = value;
            }
        }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>The created by.</value>
        public string CreatedBy
        {
            get
            {
                return this.myCreatedBy;
            }

            set
            {
                this.myCreatedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        /// <value>The create date.</value>
        public DateTime CreateDate
        {
            get
            {
                return this.myCreateDate;
            }

            set
            {
                this.myCreateDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the last modified by.
        /// </summary>
        /// <value>The last modified by.</value>
        public string LastModifiedBy
        {
            get
            {
                return this.myLastModifiedBy;
            }

            set
            {
                this.myLastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets the last modified date.
        /// </summary>
        /// <value>The last modified date.</value>
        public DateTime LastModifiedDate
        {
            get
            {
                return this.myLastModifiedDate;
            }

            set
            {
                this.myLastModifiedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the old user ID.
        /// </summary>
        /// <value>The old user ID.</value>
        public string OldUserID
        {
            get
            {
                return this.myOldUserID;
            }

            set
            {
                this.myOldUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        /// <value>The notes.</value>
        public string Notes
        {
            get
            {
                return this.myNotes;
            }

            set
            {
                this.myNotes = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="NonEmployeeEntity"/> is rehire.
        /// </summary>
        /// <value><c>true</c> if rehire; otherwise, <c>false</c>.</value>
        public string Rehire
        {
            get
            {
                return this.myRehire;
            }

            set
            {
                this.myRehire = value;
            }
        }

        /// <summary>
        /// Gets or sets the contract owner.
        /// </summary>
        /// <value>The contract owner.</value>
        public string ContractOwner
        {
            get
            {
                return this.myContractOwner;
            }

            set
            {
                this.myContractOwner = value;
            }
        }

        /// <summary>
        /// Gets or sets the kronos number.
        /// </summary>
        /// <value>The kronos number.</value>
        public string KronosNumber
        {
            get
            {
                return this.myKronosNumber;
            }

            set
            {
                this.myKronosNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the additional user info.
        /// </summary>
        /// <value>The additional user info.</value>
        public string AdditionalUserInfo
        {
            get
            {
                return this.myAdditionalUserInfo;
            }

            set
            {
                this.myAdditionalUserInfo = value;
            }
        }

        /// <summary>
        /// Gets or sets the function ID.
        /// </summary>
        /// <value>The function ID.</value>
        public int FunctionID
        {
            get
            {
                return this.myFunctionID;
            }

            set
            {
                this.myFunctionID = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [other GBU].
        /// </summary>
        /// <value><c>true</c> if [other GBU]; otherwise, <c>false</c>.</value>
        public bool OtherGBU
        {
            get
            {
                return this.myOtherGBU;
            }

            set
            {
                this.myOtherGBU = value;
            }
        }

        /// <summary>
        /// Gets or sets the language ID.
        /// </summary>
        /// <value>The language ID.</value>
        public int LanguageID
        {
            get
            {
                return this.myLanguageID;
            }

            set
            {
                this.myLanguageID = value;
            }
        }

        /// <summary>
        /// Gets or sets the TPS level.
        /// </summary>
        /// <value>The tps level.</value>
        public string TPS
        {
            get
            {
                return this.myTPS;
            }

            set
            {
                this.myTPS = value;
            }
        }

        /// <summary>
        /// Gets or sets the language name.
        /// </summary>
        /// <value>The language name.</value>
        public string LanguageName
        {
            get
            {
                return this.myLanguageName;
            }

            set
            {
                this.myLanguageName = value;
            }
        }

        /// <summary>
        /// Gets or sets the function name.
        /// </summary>
        /// <value>The function name.</value>
        public string FunctionName
        {
            get
            {
                return this.myFunctionName;
            }

            set
            {
                this.myFunctionName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Sales Rep Type Desc.
        /// </summary>
        /// <value>The Sales Rep Type Desc.</value>
        public string SalesRepTypeDesc
        {
            get
            {
                return this.mySalesRepTypeDesc;
            }

            set
            {
                this.mySalesRepTypeDesc = value;
            }
        }

        /// <summary>
        /// Gets or sets the Region Desc.
        /// </summary>
        /// <value>The Region Desc.</value>
        public string RegionDesc
        {
            get
            {
                return this.myRegionDesc;
            }

            set
            {
                this.myRegionDesc = value;
            }
        }

        /// <summary>
        /// Gets or sets the HCP Facing.
        /// </summary>
        /// <value>The HCP Facing.</value>
        public string HcpFacing
        {
            get
            {
                return this.myHcpFacing;
            }

            set
            {
                this.myHcpFacing = value;
            }
        }

        /// <summary>
        /// Gets or sets the Full time to NonEmployee.
        /// </summary>
        /// <value>The Full time to NonEmployee.</value>
        public string FTtoNE
        {
            get
            {
                return this.myFTtoNE;
            }

            set
            {
                this.myFTtoNE = value;
            }
        }

        /// <summary>
        /// Gets or sets the NonEmployee to Full time.
        /// </summary>
        /// <value>The NonEmployee to Full time.</value>
        public string NEtoFT
        {
            get
            {
                return this.myNEtoFT;
            }

            set
            {
                this.myNEtoFT = value;
            }
        }

        /// <summary>
        /// Gets or sets the Employee Type Desc.
        /// </summary>
        /// <value>The Employee Type Desc.</value>
        public string EmployeeTypeDesc
        {
            get
            {
                return this.myEmployeeTypeDesc;
            }

            set
            {
                this.myEmployeeTypeDesc = value;
            }
        }

        /// <summary>
        /// Gets or sets the Certification Sent date.
        /// </summary>
        /// <value>The Certification Sent date.</value>
        public DateTime CertificationSent
        {
            get
            {
                return this.myCertificationSent;
            }

            set
            {
                this.myCertificationSent = value;
            }
        }

        /// <summary>
        /// Gets or sets the Certification Received date.
        /// </summary>
        /// <value>The Certification Received date.</value>
        public DateTime CertificationReceived
        {
            get
            {
                return this.myCertificationReceived;
            }

            set
            {
                this.myCertificationReceived = value;
            }
        }

        /// <summary>
        /// Gets or sets the Legacy Position date.
        /// </summary>
        /// <value>The Legacy Position date.</value>
        public DateTime LegacyPositionDate
        {
            get
            {
                return this.myLegacyPositionDate;
            }

            set
            {
                this.myLegacyPositionDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Legacy Position.
        /// </summary>
        /// <value>The Legacy Position.</value>
        public string LegacyPosition
        {
            get
            {
                return this.myLegacyPosition;
            }

            set
            {
                this.myLegacyPosition = value;
            }
        }

    }
}
