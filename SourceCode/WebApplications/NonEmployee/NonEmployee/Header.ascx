﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="NonEmployee.Header" %>

<link href=<%= Request.ApplicationPath.TrimEnd('/') + "/css/StyleNED.css" %> rel="stylesheet" type="text/css" />
<table cellspacing="0" cellpadding="5" width="960" align="center" border="0">
    <tr>
        <td style="width:30%">
            <div align="left">
                <asp:Image ID="Image1" ImageUrl="~/images/logo.gif" runat="server" Width="221" AlternateText="Smith &amp; Nephew" />
            </div>
        </td>
	    <td style="vertical-align:top; text-align:left;color:#FF7300;font-size:9pt">&nbsp;</td>
        <td style="width:30%">
            <div align="right">
                <asp:Button ID="btnAppName" BorderStyle="None" ForeColor="DarkGray" BackColor="White"
                    BorderColor="White" Text="Non-Employees" Font-Size="16pt" Font-Bold="True" runat="server"
                    CausesValidation="False" OnClick="BtnAppName_Click"></asp:Button></div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr />
        </td>
    </tr>
</table>
       <div id="navBar">
        <table>
            <tr>
                <td style="text-align:left">
                    <asp:Menu ID="NavMenu" runat="server" Orientation="Horizontal" CssSelectorClass="SimpleMenu" OnMenuItemClick="OnClick">
                    </asp:Menu>
                </td>
            </tr>
        </table>
      </div>