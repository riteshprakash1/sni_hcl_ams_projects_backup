﻿// --------------------------------------------------------------
// <copyright file="BusinessArea.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class BusinessArea : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {

                this.BindDT();
                this.LoadBusinessAreaDropDowns();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load


        /// <summary>Load the Business Area Drop Downs.</summary>
        protected void LoadBusinessAreaDropDowns()
        {
            this.ddlCurrentBusinessArea.Items.Clear();
            this.ddlCurrentBusinessArea.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewBusinessArea.Items.Clear();
            this.ddlNewBusinessArea.Items.Add(new ListItem("-- Select --", String.Empty));

            foreach (DataRow r in this.myUtility.TblBusinessArea.Rows)
            {
                this.ddlCurrentBusinessArea.Items.Add(new ListItem(r["BusinessAreaName"].ToString(), r["BusinessAreaID"].ToString()));
                this.ddlNewBusinessArea.Items.Add(new ListItem(r["BusinessAreaName"].ToString(), r["BusinessAreaID"].ToString()));
            }
        }


        /// <summary>
        /// Binds the business area table to the gvBusinesss GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblBusinessArea = SqlUtility.SetTable("sp_GetBusinessAreas");
            DataTable dt = this.myUtility.TblBusinessArea;

            if (dt.Rows.Count == 0)
            {
                DataRow newRow = dt.NewRow();
                dt.Rows.Add(newRow);
            }

            this.gvBusiness.DataSource = dt;
            this.gvBusiness.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvBusinesss GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Business Area');");

                //// if the gvBusiness if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvBusiness.DataKeys[e.Row.RowIndex]["BusinessAreaID"].ToString() != "");

                if (e.Row.RowIndex == this.gvBusiness.EditIndex)
                {
                    TextBox txtUpdateBusinessAreaName = (TextBox)e.Row.FindControl("txtUpdateBusinessAreaName");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdateBusinessAreaName, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtBusinessAreaCode = (TextBox)e.Row.FindControl("txtBusinessAreaCode");
                TextBox txtBusinessAreaName = (TextBox)e.Row.FindControl("txtBusinessAreaName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtBusinessAreaName, btnAdd);
                EnterButton.TieButton(txtBusinessAreaCode, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvBusinesss GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intBusinessAreaID = Int32.Parse(this.gvBusiness.DataKeys[e.RowIndex]["BusinessAreaID"].ToString());
            CustomValidator vldBusinessAreaAssigned = (CustomValidator)this.gvBusiness.Rows[e.RowIndex].FindControl("vldBusinessAreaAssigned");

            Page.Validate("Delete");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@BusinessAreaID", intBusinessAreaID));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetBusinessAreaAssignees", myParamters);

            vldBusinessAreaAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete BusinessArea
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@BusinessAreaID", intBusinessAreaID));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeleteBusinessArea", myDeleteParamters);

                this.gvBusiness.EditIndex = -1;
                this.BindDT();
                this.LoadBusinessAreaDropDowns();
            }

        }

        /// <summary>
        /// Handles the Edit event of the gvBusiness GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvBusiness.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Update event of the gvBusiness GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("Update");

            if (Page.IsValid)
            {
                int intBusinessAreaID = Int32.Parse(this.gvBusiness.DataKeys[e.RowIndex]["BusinessAreaID"].ToString());
                TextBox txtUpdateBusinessAreaName = (TextBox)this.gvBusiness.Rows[e.RowIndex].FindControl("txtUpdateBusinessAreaName");

                ////Update BusinessArea
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@BusinessAreaID", intBusinessAreaID));
                myParamters.Add(new SqlParameter("@BusinessAreaName", txtUpdateBusinessAreaName.Text));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateBusinessArea", myParamters);

                this.gvBusiness.EditIndex = -1;
                this.BindDT();
                this.LoadBusinessAreaDropDowns();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvBusinesss GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtBusinessAreaCode = (TextBox)this.gvBusiness.FooterRow.FindControl("txtBusinessAreaCode");
            TextBox txtBusinessAreaName = (TextBox)this.gvBusiness.FooterRow.FindControl("txtBusinessAreaName");
            CustomValidator vldBusinessAreaExists = (CustomValidator)this.gvBusiness.FooterRow.FindControl("vldBusinessAreaExists");

            Collection<SqlParameter> myValidParamters = new Collection<SqlParameter>();
            myValidParamters.Add(new SqlParameter("@BusinessAreaCode", txtBusinessAreaCode.Text));
            myValidParamters.Add(new SqlParameter("@BusinessAreaName", txtBusinessAreaName.Text));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetBusinessAreasByCodeByName", myValidParamters);

            vldBusinessAreaExists.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@BusinessAreaCode", txtBusinessAreaCode.Text));
                myParamters.Add(new SqlParameter("@BusinessAreaName", txtBusinessAreaName.Text));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertBusinessArea", myParamters);

                this.gvBusiness.EditIndex = -1;
                this.BindDT();
                this.LoadBusinessAreaDropDowns();
            } //// Page.IsValid
        }

                /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvBusiness.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Global");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentBusinessAreaID", Int32.Parse(this.ddlCurrentBusinessArea.SelectedValue)));
                myParamters.Add(new SqlParameter("@NewBusinessAreaID", Int32.Parse(this.ddlNewBusinessArea.SelectedValue)));

                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNEBusinessAreaIDGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }   
    } //// end class
}
