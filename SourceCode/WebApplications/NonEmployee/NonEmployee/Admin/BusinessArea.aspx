﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusinessArea.aspx.cs" Inherits="NonEmployee.Admin.BusinessArea" MasterPageFile="~/MasterNED.Master" Title="Business Areas"%>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvBusiness" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no Business Areas." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update"
                             CellPadding="2" GridLines="Both" DataKeyNames="BusinessAreaCode,BusinessAreaID"
                             PageSize="20" AllowPaging="true" OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                            <Columns>
                              <asp:TemplateField HeaderStyle-Width="12%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="Update" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblBusinessAreaCode" Font-Size="8pt" runat="server"  Text='<%# Eval("BusinessAreaCode")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtBusinessAreaCode" MaxLength="50" Width="50px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddBusinessAreaCode" ValidationGroup="Add" ControlToValidate=txtBusinessAreaCode runat="server"  ErrorMessage="Business Area Code is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldBusinessAreaExists" runat="server" ValidationGroup="Add" ControlToValidate="txtBusinessAreaCode" ErrorMessage="The Business Area Code or Name already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="55%" HeaderText="Business Area Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblBusinessAreaName" Font-Size="8pt" runat="server"  Text='<%# Eval("BusinessAreaName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUpdateBusinessAreaName" MaxLength="50" Width="175px" Font-Size="8pt" runat="server" Text='<%# Eval("BusinessAreaName")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdateBusinessAreaName" ValidationGroup="Update" ControlToValidate=txtUpdateBusinessAreaName runat="server"  ErrorMessage="Business Area Name is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtBusinessAreaName" MaxLength="50" Width="175px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddBusinessAreaName" ValidationGroup="Add" ControlToValidate="txtBusinessAreaName" runat="server"  ErrorMessage="Business Area Name is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldBusinessAreaAssigned" runat="server" ValidationGroup="Delete" ErrorMessage="The Organization Code is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%" border="0">
                            <tr>
                                <td>Current Business Area:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentBusinessArea" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentBusinessArea" runat="server" ErrorMessage="Current Business Area is required."
                                        ValidationGroup="Global" ControlToValidate="ddlCurrentBusinessArea">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Business Area:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewBusinessArea" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldNewBusinessArea" runat="server" ErrorMessage="New Business Area is required."
                                        ValidationGroup="Global" ControlToValidate="ddlNewBusinessArea">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" runat="server" Text="Update Globally" 
									    CssClass="buttonsSubmit" CausesValidation="false" 
										ValidationGroup="Global" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="Global" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="vldSummary1" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Update" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="vldSummary3" ValidationGroup="Delete" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
           </table>        
    </div>
</asp:Content>