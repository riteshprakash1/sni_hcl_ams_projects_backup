﻿// --------------------------------------------------------------
// <copyright file="Functions.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class Functions : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");
                this.BindDT(this.myUtility.TblFunctions);
                this.LoadFunctionDropDowns();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvFunctions GridView.
        /// </summary>
        protected void BindDT(DataTable dtFunction)
        {
            DataRow[] rows = dtFunction.Select();
            DataTable dtSelect = dtFunction.Clone();

            for (int x = 0; x < rows.Length; x++)
            {
                dtSelect.ImportRow(rows[x]);
            }

            if (dtSelect.Rows.Count == 0)
            {
                DataRow newRow = dtSelect.NewRow();
                dtSelect.Rows.Add(newRow);
            }

            this.gvFunction.DataSource = dtSelect;
            this.gvFunction.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvFunctions GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Function');");

                //// if the PaSubCode if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvFunction.DataKeys[e.Row.RowIndex]["FunctionID"].ToString() != "");

                if (e.Row.RowIndex == this.gvFunction.EditIndex)
                {
                    TextBox txtUpdateFunction = (TextBox)e.Row.FindControl("txtUpdateFunction");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdateFunction, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In Functions Footer");
                TextBox txtFunction = (TextBox)e.Row.FindControl("txtFunction");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtFunction, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvFunctions GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intFunctionID = Int32.Parse(this.gvFunction.DataKeys[e.RowIndex]["FunctionID"].ToString());

            CustomValidator vldFunctionAssigned = (CustomValidator)this.gvFunction.Rows[e.RowIndex].FindControl("vldFunctionAssigned");

            Page.Validate("Delete");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@FunctionID", intFunctionID));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetFunctionAssignees", myParamters);

            vldFunctionAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete PaSubCode
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@FunctionID", intFunctionID));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeleteFunction", myDeleteParamters);

                this.gvFunction.EditIndex = -1;
                this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");
                this.BindDT(this.myUtility.TblFunctions);
                this.LoadFunctionDropDowns();
            }

        }

        /// <summary>
        /// Handles the Edit event of the gvFunction GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvFunction.EditIndex = e.NewEditIndex;
            this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");
            this.BindDT(this.myUtility.TblFunctions);
        }

        /// <summary>
        /// Handles the Update event of the gvFunction GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("Update");

            if (Page.IsValid)
            {
                int intFunctionID = Int32.Parse(this.gvFunction.DataKeys[e.RowIndex]["FunctionID"].ToString());
                TextBox txtUpdateFunction = (TextBox)this.gvFunction.Rows[e.RowIndex].FindControl("txtUpdateFunction");

                ////Update Function
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@FunctionName", txtUpdateFunction.Text));
                myParamters.Add(new SqlParameter("@FunctionID", intFunctionID));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateFunction", myParamters);

                this.gvFunction.EditIndex = -1;
                this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");
                this.BindDT(this.myUtility.TblFunctions);
                this.LoadFunctionDropDowns();

            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvFunctions GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtFunction = (TextBox)this.gvFunction.FooterRow.FindControl("txtFunction");
            CustomValidator vldFunctionExists = (CustomValidator)this.gvFunction.FooterRow.FindControl("vldFunctionExists");

            Collection<SqlParameter> myVldParamters = new Collection<SqlParameter>();
            myVldParamters.Add(new SqlParameter("@FunctionName", txtFunction.Text));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetFunctionsByFunctionName", myVldParamters);

            vldFunctionExists.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@FunctionName", txtFunction.Text));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertFunction", myParamters);

                this.gvFunction.EditIndex = -1;
                this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");
                this.BindDT(this.myUtility.TblFunctions);
                this.LoadFunctionDropDowns();

            } //// Page.IsValid
        }


        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvFunction.PageIndex = e.NewPageIndex;
            this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");
            this.BindDT(this.myUtility.TblFunctions);
        }

        /// <summary>
        /// Loads the function drop downs.
        /// </summary>
        protected void LoadFunctionDropDowns()
        {
            this.ddlCurrentFunction.Items.Clear();
            this.ddlCurrentFunction.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewFunction.Items.Clear();
            this.ddlNewFunction.Items.Add(new ListItem("-- Select --", String.Empty));
            this.myUtility.TblFunctions = SqlUtility.SetTable("sp_GetFunctions");

            foreach (DataRow r in this.myUtility.TblFunctions.Rows)
            {
                this.ddlCurrentFunction.Items.Add(new ListItem(r["FunctionName"].ToString(), r["FunctionID"].ToString()));
                this.ddlNewFunction.Items.Add(new ListItem(r["FunctionName"].ToString(), r["FunctionID"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Global");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentFunctionID", Int32.Parse(this.ddlCurrentFunction.SelectedValue)));
                myParamters.Add(new SqlParameter("@NewFunctionID", Int32.Parse(this.ddlNewFunction.SelectedValue)));

                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNEFunctionGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }   
    } //// end class
}
