﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Regions.aspx.cs" Inherits="NonEmployee.Admin.Regions" MasterPageFile="~/MasterNED.Master" Title="Regions"%>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
        <table width="100%">
             <tr>
                <td>&nbsp;</td>
            </tr>
                <tr>
                    <td style="width:10%">Company:</td>
                    <td style="width:55%"><asp:DropDownList ID="ddlSelectCompany" width="250px" AutoPostBack="true" 
                            DataTextField="CompanyCodeDesc" DataValueField="CompanyCode" Font-Size="8pt" runat="server" 
                            onselectedindexchanged="ddlSelectCompany_SelectedIndexChanged"></asp:DropDownList></td>
                    <td style="width:35%; border-left:1px solid #cccccc;"><b>Globally Update NED Regions</b></td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvRegion" runat="server" Width="100%" 
                            AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no regions." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update"
                             CellPadding="2" GridLines="Both" DataKeyNames="RegionCode,CompanyCode"
                             PageSize="20" AllowPaging="true" 
                            OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                           <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="UpdateRegion" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="AddRegion" CausesValidation="false" BorderColor="White"></asp:Button>   
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Region Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegionCode" Font-Size="8pt" runat="server"  Text='<%# Eval("RegionCode")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtRegionCode" MaxLength="10" Width="50px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddRegion" ValidationGroup="AddRegion" ControlToValidate="txtRegionCode" runat="server"  ErrorMessage="Region Code is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldRegionExists" runat="server" ValidationGroup="AddRegion" ControlToValidate="txtRegionCOde" ErrorMessage="The Region Code already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Region Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegionDesc" Font-Size="8pt" runat="server"  Text='<%# Eval("RegionDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUpdateRegionDesc" MaxLength="50" Width="150px" Font-Size="8pt" runat="server" Text='<%# Eval("RegionDesc")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdateRegionDesc" ValidationGroup="UpdateRegion" ControlToValidate="txtUpdateRegionDesc" runat="server"  ErrorMessage="Region Name is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtRegionDesc" MaxLength="50" Width="150px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddRegionDesc" ValidationGroup="AddRegion" ControlToValidate="txtRegionDesc" runat="server"  ErrorMessage="Region Name is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="35%" HeaderText="Company">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" Font-Size="8pt" runat="server"  Text='<%# Eval("CompanyCodeDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlCompany" width="175px" DataTextField="CompanyCodeDesc" DataValueField="CompanyCode"  DataSource='<%# GetCompanyList()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldRequiredCompany" ValidationGroup="AddRegion" ControlToValidate="ddlCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                               <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldRegionAssigned" runat="server" ValidationGroup="DeleteRegion" ErrorMessage="The Region is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%">
                            <tr>
                                <td style="width:35%">Company:</td>
                                <td>
                                    <asp:DropDownList ID="ddlGlobalCompany" Width="200px" 
                                        Font-Size="8pt" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="ddlGlobalCompany_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldGlobalRequiredCompany" ValidationGroup="GlobalRegion" ControlToValidate="ddlGlobalCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Current Region:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentRegion" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentRegion" runat="server" ErrorMessage="Current Region is required."
                                        ValidationGroup="GlobalRegion" ControlToValidate="ddlCurrentRegion">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Region:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewRegion" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldNewRegion" runat="server" ErrorMessage="New Region is required."
                                        ValidationGroup="GlobalRegion" ControlToValidate="ddlCurrentRegion">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" Height="18" runat="server" Text="Update Globally" 
                                      CssClass="buttonsSubmit" CausesValidation="false" 
									  ValidationGroup="GlobalRegion" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                             <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddRegion" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateRegion" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="DeleteRegion" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="GlobalRegion" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                               </td>
                            </tr>       
                       </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
           </table>        
    </div>
</asp:Content>
