﻿// --------------------------------------------------------------
// <copyright file="Region.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------


namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class Regions : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.ddlSelectCompany.Items.Clear();
                this.ddlSelectCompany.Items.Add(new ListItem("--Select--", ""));
                this.ddlGlobalCompany.Items.Clear();
                this.ddlGlobalCompany.Items.Add(new ListItem("--Select--", ""));
                foreach (DataRow r in myUtility.TblCompanyCodes.Rows)
                {
                    this.ddlSelectCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                    this.ddlGlobalCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                }

                this.BindDT();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvRegion GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblRegion = SqlUtility.SetTable("sp_GetRegions");
            DataTable dt = this.myUtility.TblRegion;
            if (this.ddlSelectCompany.Items.Count > 0)
            {
                DataRow[] rows = dt.Select("CompanyCodeDesc = '" + this.ddlSelectCompany.SelectedItem + "'");
                DataTable dtSelect = dt.Clone();

                for (int x = 0; x < rows.Length; x++)
                {
                    dtSelect.ImportRow(rows[x]);
                }

                if (dtSelect.Rows.Count == 0)
                {
                    DataRow newRow = dtSelect.NewRow();
                    dtSelect.Rows.Add(newRow);
                }
                dt = dtSelect;

                Debug.WriteLine("Region dt rows: " + dt.Rows.Count.ToString() + "; Region dtSelect rows: " + dtSelect.Rows.Count.ToString());
            }

            this.gvRegion.DataSource = dt;
            this.gvRegion.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvRegions GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Region');");

                //// if the Region if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvRegion.DataKeys[e.Row.RowIndex]["RegionCode"].ToString() != "");

                if (e.Row.RowIndex == this.gvRegion.EditIndex)
                {
                    TextBox txtUpdateRegionDesc = (TextBox)e.Row.FindControl("txtUpdateRegionDesc");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdateRegionDesc, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtRegionCode = (TextBox)e.Row.FindControl("txtRegionCode");
                TextBox txtRegionDesc = (TextBox)e.Row.FindControl("txtRegionDesc");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtRegionDesc, btnAdd);
                EnterButton.TieButton(txtRegionCode, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvRegions GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strRegionCode = this.gvRegion.DataKeys[e.RowIndex]["RegionCode"].ToString();
            string strCompanyCode = this.gvRegion.DataKeys[e.RowIndex]["CompanyCode"].ToString();
            CustomValidator vldRegionAssigned = (CustomValidator)this.gvRegion.Rows[e.RowIndex].FindControl("vldRegionAssigned");

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            Page.Validate("DeleteRegion");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@RegionCode", strRegionCode));
            myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetRegionAssignees", myParamters);

            vldRegionAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete Region
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@RegionCode", strRegionCode));
                myDeleteParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeleteRegion", myDeleteParamters);

                this.gvRegion.EditIndex = -1;
                this.BindDT();
            }

            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the Edit event of the gvRegion GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvRegion.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Update event of the gvRegion GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateRegion");

            if (Page.IsValid)
            {
                string strRegionCode = this.gvRegion.DataKeys[e.RowIndex]["RegionCode"].ToString();
                string strCompanyCode = this.gvRegion.DataKeys[e.RowIndex]["CompanyCode"].ToString();
                TextBox txtUpdateRegionDesc = (TextBox)this.gvRegion.Rows[e.RowIndex].FindControl("txtUpdateRegionDesc");

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                ////Update Region
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@RegionCode", strRegionCode));
                myParamters.Add(new SqlParameter("@RegionDesc", txtUpdateRegionDesc.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateRegion", myParamters);

                this.gvRegion.EditIndex = -1;
                this.BindDT();

                this.myUtility.SqlConn.Close();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvRegions GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddRegion");

            TextBox txtRegionCode = (TextBox)this.gvRegion.FooterRow.FindControl("txtRegionCode");
            TextBox txtRegionDesc = (TextBox)this.gvRegion.FooterRow.FindControl("txtRegionDesc");
            DropDownList ddlCompany = (DropDownList)this.gvRegion.FooterRow.FindControl("ddlCompany");

            foreach (IValidator val in Page.Validators)
            {
                if (val.ErrorMessage == "The Region Code already exists.")
                {
           
                    Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                    myParamters.Add(new SqlParameter("@RegionCode", txtRegionCode.Text));
                    myParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));
                    DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetRegionByRegionCodeByCompanyCode", myParamters);

                    val.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);
                }
            }

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@RegionCode", txtRegionCode.Text));
                myParamters.Add(new SqlParameter("@RegionDesc", txtRegionDesc.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertRegion", myParamters);

                this.gvRegion.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
            this.myUtility.SqlConn.Close();
        }



        /// <summary>
        /// Gets the company code list.
        /// </summary>
        /// <returns></returns>
        public DataTable GetCompanyList()
        {
            DataRow[] rows = myUtility.TblCompanyCodes.Select();
            DataTable dtClone = myUtility.TblCompanyCodes.Clone();
            DataRow newRow = dtClone.NewRow();
            newRow["CompanyCode"] = "";
            newRow["CompanyCodeDesc"] = "--Select--";
            dtClone.Rows.Add(newRow);

            for (int x = 0; x < rows.Length; x++)
            {
                dtClone.ImportRow(rows[x]);
            }

            return dtClone;
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvRegion.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlSelectCompanyCode control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlSelectCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //gvRegion.PageIndex = 1;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlGlobalCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlGlobalCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlCurrentRegion.Items.Clear();
            this.ddlCurrentRegion.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewRegion.Items.Clear();
            this.ddlNewRegion.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] rows = this.myUtility.TblRegion.Select("CompanyCode = '" + this.ddlGlobalCompany.SelectedValue + "'");
            foreach (DataRow r in rows)
            {
                this.ddlCurrentRegion.Items.Add(new ListItem(r["RegionCode"].ToString() + " - " + r["RegionDesc"].ToString(), r["RegionCode"].ToString()));
                this.ddlNewRegion.Items.Add(new ListItem(r["RegionCode"].ToString() + " - " + r["RegionDesc"].ToString(), r["RegionCode"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("GlobalRegion");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentRegionCode", this.ddlCurrentRegion.SelectedValue));
                myParamters.Add(new SqlParameter("@NewRegionCode", this.ddlNewRegion.SelectedValue));
                myParamters.Add(new SqlParameter("@CompanyCode", this.ddlGlobalCompany.SelectedValue));

                foreach (SqlParameter par in myParamters)
                {
                    Debug.WriteLine("Name: " + par.ParameterName + " - " + par.Value.ToString());
                }
                
                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNERegionGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }

    } //// end class
}
