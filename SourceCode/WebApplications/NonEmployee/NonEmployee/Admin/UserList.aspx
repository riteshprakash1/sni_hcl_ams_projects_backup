﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="NonEmployee.Admin.UserList" MasterPageFile="~/MasterNED.Master" Title="User List" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat=server>

</asp:Content>
<asp:Content ContentPlaceHolderID=ContentPlaceHolder1 ID="Content3" runat=server>
    <div id="SearchPage">
        <table>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <asp:GridView ID="gvUser" runat="server" Width="800" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no users." OnRowDeleting="GV_Delete"  ShowFooter=true 
                     CellPadding="2" DataKeyNames="userID" GridLines="Both" OnRowCancelingEdit="GV_Add"
                    >
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                            <ItemTemplate>
                                <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 

                       <asp:TemplateField HeaderStyle-Width="25%" HeaderText="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" Font-Size="8pt" runat="server"  Text='<%# Eval("username")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddUserName" Width="150px" MaxLength="30" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredAddUserName" ValidationGroup="Add" ControlToValidate=txtAddUserName runat="server"  ErrorMessage="User Name is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldUserExists" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name already exists.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldAddUserInAD" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name is not valid.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="20%" HeaderText="User Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserRole" Font-Size="8pt" runat="server"  Text='<%# Eval("appRoleDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlAddUserRole" width="125px" DataTextField="AppRoleDesc" DataValueField="AppRoleID"  DataSource='<%# GetUserRoleList()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                    <asp:CompareValidator ID="vldCompareAppRole"  ValidationGroup="Add" ControlToValidate="ddlAddUserRole" runat="server"  ErrorMessage="User Role is required." Type="Integer" ValueToCompare="0" Operator="NotEqual">*</asp:CompareValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                        <asp:BoundField DataField="firstname" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="First Name" />                       
                        <asp:BoundField DataField="lastname" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="Last Name" />                       
                   </Columns>
                </asp:GridView> 
                
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vldSummary1"  ValidationGroup="FindUser" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />

                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><b>Find User Name</b></td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td width="10%">Last Name:</td>
                            <td width="25%"><asp:TextBox ID="txtLastName" Font-Size="8pt" MaxLength="50" runat="server"></asp:TextBox></td>
                                <asp:RequiredFieldValidator ID="vldFindLastName" ValidationGroup="FindUser" ControlToValidate="txtLastName" runat="server" ErrorMessage="Find User - Last name is required.">*</asp:RequiredFieldValidator>
                            <td>
                                <asp:Button id="btnFindUser" ValidationGroup="FindUser" runat="server" Text="Find User"
				                    CssClass="buttonsSubmit" CausesValidation="false" onclick="btnFindUser_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:GridView ID="gvFindUsers" runat="server" Width="800" AutoGenerateColumns="False"
                        EmptyDataText="There are no users." OnRowEditing="GV_Select"
                         CellPadding="2" DataKeyNames="username" GridLines="Both" 
                        >
                        <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnSelect" Width="50px" Height="18" runat="server" Text="Select" CommandName="Edit"
							            Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								        ></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:BoundField DataField="username" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="UserName" />                       
                            <asp:BoundField DataField="firstname" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="First Name" />                       
                            <asp:BoundField DataField="lastname" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="Last Name" />                       
                            <asp:BoundField DataField="company" HeaderStyle-Width="15%" ReadOnly="true" HeaderText="Company" />                       
                       </Columns>
                    </asp:GridView>                     
                </td>
            </tr>
       </table>    
    </div>
</asp:Content>
