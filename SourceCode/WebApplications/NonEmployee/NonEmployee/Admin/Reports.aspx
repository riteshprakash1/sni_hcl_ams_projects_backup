﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="NonEmployee.Admin.Reports" MasterPageFile="~/MasterNED.Master" Title="Reports" %>

<%@ Register TagPrefix="uc1" Namespace="NonEmployee.Classes" Assembly="NonEmployee" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat=server>

</asp:Content>
<asp:Content ContentPlaceHolderID=ContentPlaceHolder1 ID="Content3" runat=server>
    <div>
        <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="lblReports" runat="server" Text="Reports" CssClass="titleheader"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlReports" runat="server" AutoPostBack="true" 
                        onselectedindexchanged="ddlReports_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" width="100%" colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Parameters" CssClass="titleheader"></asp:Label>
                    <br />
                    <asp:Repeater ID="rptValues" runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lblParamID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ParameterID") %>'></asp:Label>
                            <asp:Label ID="lblParamField" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ParameterField") %>'></asp:Label>
                            <asp:Label ID="lblComparison" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ParameterComparison") %>'></asp:Label>
                            <asp:TextBox ID="txtParamValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ParameterValue") %>'></asp:TextBox>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <br />
                        </SeparatorTemplate>
                    </asp:Repeater>
                    <br />
                    <br />
                    <asp:Button ID="btnGenerate" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                        Text="Generate Report" OnClick="BtnGenerate_Click" />
                    <br />
                    <br />
                    <asp:HyperLink ID="hyperReportDownload" runat="server">HyperLink</asp:HyperLink>
                    <asp:Label ForeColor="#FF7300" ID="lblGenerateMsg" runat="server"></asp:Label>
                </td>
            </tr>
        </table>        
    </div>
</asp:Content>
