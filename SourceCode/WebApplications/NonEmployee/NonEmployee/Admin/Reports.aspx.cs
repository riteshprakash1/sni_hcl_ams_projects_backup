﻿// -------------------------------------------------------------
// <copyright file="Reports.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;
    using ReportingTool;

    /// <summary>
    /// This is the reports page.
    /// </summary>
    public partial class Reports : System.Web.UI.Page
    {
        /// <summary>
        /// This is our instance of the utility class.
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            ////// if not an admin redirect to Unauthorized Page
            //if (!this.myUtility.IsAdmin) 
            //{
            //    Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            //}

            if (this.IsPostBack == false)
            {
                this.RefreshReports();
                this.hyperReportDownload.Visible = false;
                this.btnGenerate.Enabled = false;
            }

            this.lblGenerateMsg.Text = "";
        }

        private void CleanOutputDirectory(string strFolder)
        {
            if (System.IO.Directory.Exists(strFolder))
            {
                try
                {
                    System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(strFolder);
                    FileInfo[] files = dir.GetFiles();
                    foreach (FileInfo f in files)
                    {
                        // File is more thantwo days old delete it
                        Debug.WriteLine(f.CreationTime.ToShortDateString() + "Compare: " + f.CreationTime.AddDays(2).CompareTo(DateTime.Now).ToString());
                        if (f.CreationTime.AddDays(2).CompareTo(DateTime.Now) < 0)
                        {
                            try
                            {
                                f.Delete();
                            }
                            catch (System.Exception exFile)
                            {
                                Debug.WriteLine("Delete File Error: " + exFile.Message);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.WriteLine("Error: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGenerate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnGenerate_Click(object sender, EventArgs e)
        {
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Start BtnGenerate_Click");
            try
            {
                ReportingTool.ReportEntity myReport = this.myUtility.GetReportingEntity(Convert.ToInt32(this.ddlReports.SelectedValue));
                string strOutputFolder = Server.MapPath(".") + "\\ReportOutput\\";
                myReport.ReportOutpath = strOutputFolder + myReport.ReportOutpath + DateTime.Now.ToBinary().ToString() + ".xlsx";
                Debug.WriteLine("ReportOutpath: " + myReport.ReportOutpath);

                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Start CleanOutputDirectory");
                this.CleanOutputDirectory(strOutputFolder);
                myReport.ReportParameters = new Collection<ReportParameters>();
                foreach (System.Web.UI.WebControls.RepeaterItem r in this.rptValues.Items)
                {
                    ReportParameters p = new ReportParameters();
                    
                    foreach (Control c in r.Controls)
                    {
                        if (c is Label)
                        {
                            Label lbl = (Label)c;
                            switch (lbl.ID)       
                             {         
                                case "lblParamField":   
                                    p.ParameterField = lbl.Text;
                                    break;                  
                                 case "lblComparison":            
                                    p.ParameterComparison = lbl.Text;
                                    break;
                                 case "lblParamID":            
                                    p.ParameterID = Int32.Parse(lbl.Text);
                                    break; 
                            }
                        }

                        if (c is TextBox)
                        {
                            p.ParameterValue = ((TextBox)c).Text;
                            p.ParameterComparison = ((TextBox)c).ToolTip;
                            Debug.WriteLine("ParameterValue: " + p.ParameterValue);
                            Debug.WriteLine("ParameterComparison: " + p.ParameterComparison);
                        }
                    }

                    myReport.ReportParameters.Add(p);
                }

                ReportingTool.ReportingToolManager myReportManager = new ReportingToolManager(myReport);
                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Start GenerateReport()");
                myReportManager.GenerateReport();
                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : End GenerateReport()");

                ////This is going to set the report naigation.
                char[] mySeperator = new char[1] { Convert.ToChar("\\") };
                this.hyperReportDownload.Visible = true;
                this.hyperReportDownload.NavigateUrl = "ReportOutput/" + myReport.ReportOutpath.Split(mySeperator)[myReport.ReportOutpath.Split(mySeperator).GetUpperBound(0)].ToString();
                this.hyperReportDownload.Text = "View " + myReport.ReportReportHeader;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
                if (ex.InnerException != null)
                {
                    Debug.WriteLine("Inner Message: " + ex.InnerException.Message);
                }

                this.hyperReportDownload.Visible = false;
                //this.hyperReportDownload.Text = ex.Message;
                this.lblGenerateMsg.Text = "Generate Report Error: " + ex.Message;
            }

            this.btnGenerate.Enabled = false;
            this.RefreshReports();
        }

        /// <summary>
        /// Refreshes the reports.
        /// </summary>
        private void RefreshReports()
        {
            this.ddlReports.Items.Clear();
            this.ddlReports.Items.Add(new ListItem("-- Select Report --", "0"));

            string strUser = ADUtility.GetUserName(Request);
            DataTable dtReports = SqlUtility.SetTable("sp_GetReportsViewableByUser", strUser);

            foreach (DataRow r in dtReports.Rows)
            {
                this.ddlReports.Items.Add(new ListItem(r["ReportTitle"].ToString(), r["ReportID"].ToString()));
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlReports control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportingTool.ReportEntity myEntity = this.myUtility.GetReportingEntity(Convert.ToInt32(this.ddlReports.SelectedValue));

            this.rptValues.DataSource = this.myUtility.GetReportParameters(Convert.ToInt32(this.ddlReports.SelectedValue));
            this.rptValues.DataBind();

            this.btnGenerate.Enabled = true;

            this.hyperReportDownload.Visible = false;
        }
    }
}
