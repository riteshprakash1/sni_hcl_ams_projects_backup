﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CostCenters.aspx.cs" Inherits="NonEmployee.Admin.CostCenters" MasterPageFile="~/MasterNED.Master" Title="Cost Centers" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
        <table width="100%">
             <tr>
                <td>&nbsp;</td>
            </tr>
                <tr>
                    <td style="width:10%">Company:</td>
                    <td style="width:55%"><asp:DropDownList ID="ddlSelectCompany" width="250px" AutoPostBack="true" 
                            DataTextField="CompanyCodeDesc" DataValueField="CompanyCode" Font-Size="8pt" runat="server" 
                            onselectedindexchanged="ddlSelectCompany_SelectedIndexChanged"></asp:DropDownList></td>
                    <td style="width:35%; border-left:1px solid #cccccc;"><b>Globally Update NED Cost Codes</b></td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvCost" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no cost centers." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update"
                             CellPadding="2" GridLines="Both" DataKeyNames="CostCenter,CompanyCode"
                             PageSize="20" AllowPaging="true" OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                           <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="UpdateCostCenter" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="AddCostCenter" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Cost Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCostCenter" Font-Size="8pt" runat="server"  Text='<%# Eval("CostCenter")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtCostCenter" MaxLength="20" Width="50px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddCostCenter" ValidationGroup="AddCostCenter" ControlToValidate="txtCostCenter" runat="server"  ErrorMessage="Cost Center is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldCostCenterExists" runat="server" ValidationGroup="AddCostCenter" ControlToValidate="txtCostCenter" ErrorMessage="The Cost Center already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="35%" HeaderText="Cost Center Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCostCenterName" Font-Size="8pt" runat="server"  Text='<%# Eval("CostCenterName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUpdateCostCenterName" MaxLength="75" Width="175px" Font-Size="8pt" runat="server" Text='<%# Eval("CostCenterName")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdateCostCenterName" ValidationGroup="UpdateCostCenter" ControlToValidate="txtUpdateCostCenterName" runat="server"  ErrorMessage="Cost Center Name is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtCostCenterName" MaxLength="75" Width="175px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddCostCenterName" ValidationGroup="AddCostCenter" ControlToValidate="txtCostCenterName" runat="server"  ErrorMessage="Cost Center Name is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Company">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" Font-Size="8pt" runat="server"  Text='<%# Eval("CompanyCodeDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlCompany" width="175px" DataTextField="CompanyCodeDesc" DataValueField="CompanyCode"  DataSource='<%# GetCompanyList()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldRequiredCompany" ValidationGroup="AddCostCenter" ControlToValidate="ddlCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                               <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldCostCenterAssigned" runat="server" ValidationGroup="DeleteCostCenter" ErrorMessage="The Cost Center is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%">
                            <tr>
                                <td style="width:35%">Company:</td>
                                <td>
                                    <asp:DropDownList ID="ddlGlobalCompany" Width="200px" 
                                        Font-Size="8pt" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="ddlGlobalCompany_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldGlobalRequiredCompany" ValidationGroup="GlobalCostCenter" ControlToValidate="ddlGlobalCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Current Cost Center:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentCostCenter" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentCostCenter" runat="server" ErrorMessage="Current Cost Center is required."
                                        ValidationGroup="GlobalCostCenter" ControlToValidate="ddlCurrentCostCenter">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Cost Center:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewCostCenter" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Cost Center is required."
                                        ValidationGroup="GlobalCostCenter" ControlToValidate="ddlNewCostCenter">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" Height="18" runat="server" Text="Update Globally" 
                                      CssClass="buttonsSubmit" CausesValidation="false" 
									  ValidationGroup="GlobalCostCenter" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                             <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddCostCenter" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateCostCenter" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="DeleteCostCenter" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="GlobalCostCenter" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                               </td>
                            </tr>       
                       </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
           </table>        
    </div>
</asp:Content>
