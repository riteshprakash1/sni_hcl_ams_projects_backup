﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportDesigner.aspx.cs" Inherits="NonEmployee.Admin.ReportDesigner" MasterPageFile="~/MasterNED.Master" Title="Report Designer" %>

<%@ Register TagPrefix="uc1" Namespace="NonEmployee.Classes" Assembly="NonEmployee" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div>
        <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnDesignNewReport" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                        Text="New Report" OnClick="BtnDesignNewReport_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnSaveReport" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                        Text="Save Report" OnClick="BtnSaveReport_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnDeleteReport" runat="server" CausesValidation="false" CssClass="buttonsSubmit"
                        Text="Delete Report" OnClick="BtnDeleteReport_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <b>
                        <asp:Label ID="lblReports" runat="server" Text="Reports" CssClass="titleheader"></asp:Label></b>
                </td>
            </tr>
       </table>
       <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlReports" runat="server" AutoPostBack="true" 
                        onselectedindexchanged="ddlReports_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="titleheader"><b>Main Information</b></td>
            </tr>
            <tr>
                <td width="15%">Report Name:</td>
                <td><asp:TextBox ID="txtReportName" MaxLength="50" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vldRequiredReportName" ControlToValidate="txtReportName" ValidationGroup="SaveReport" runat="server" ErrorMessage="Report Name is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Report Footer:</td>
                <td><asp:TextBox ID="txtReportFooter" MaxLength="50" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Report Viewable By:</td>
                <td><asp:DropDownList ID="ddlReportViewableBy" runat="server">
                        <asp:ListItem>Self</asp:ListItem>
                        <asp:ListItem>Public</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtReportSelection" runat="server" Height="150px" TextMode="MultiLine"
                    Width="400px" Visible="False"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td>
                     <asp:Panel ID="panelBuilder" runat="server">
                        <table>
                            <tr>
                                <td colspan="3" class="titleheader">
                                    <b>Builder Information</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tables/Views:
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddlTables" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlTables_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox ID="lstAvailableFields" SelectionMode="Multiple"  runat="server" Width="150px"></asp:ListBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnAddField" runat="server" Text=">" CssClass="buttonsClose" OnClick="BtnAddField_Click" />
                                    <br />
                                    <asp:Button ID="btnRemoveField" runat="server" Text="<" CssClass="buttonsClose" OnClick="BtnRemoveField_Click" />
                                </td>
                                <td>
                                    <asp:ListBox ID="lstTakenFields" SelectionMode="Multiple" runat="server" Width="150px"></asp:ListBox>
                                    <asp:CustomValidator ID="vldCustomColumns" ValidationGroup="SaveReport" ControlToValidate="lstTakenFields" runat="server" ErrorMessage="You must select data columns for the report."></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel> 
                </td>
            </tr>
        </table>
        <table class="PageTable" cellspacing="2" cellpadding="2">
            <tr>
                <td colspan="4" class="titleheader">
                    <b>Parameter Information</b>
                </td>
            </tr>
            <tr colspan="4">
                <td>
                    <asp:GridView ID="gvParameter" runat="server" Width="800" AutoGenerateColumns="False" 
                        EmptyDataText="No Paramters for this report." 
                        CellPadding="2" DataKeyNames="ParameterID"  GridLines="Both"  
                        OnRowDeleting="GV_Delete">
             
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#F7F6F3" HorizontalAlign=Left Font-Size="8pt" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderStyle-Width="80">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="70px" Height="18" runat="server" CommandName="Delete" Text="Delete"
						                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
							            />
                                </ItemTemplate> 
                            </asp:TemplateField>                   
                            <asp:BoundField HeaderStyle-Width="200" DataField="ParameterField" ReadOnly="true" HeaderText="Parameter Field"  />
                            <asp:BoundField HeaderStyle-Width="150" DataField="ParameterComparison" ReadOnly="true" HeaderText="Comparison" />
                            <asp:BoundField DataField="ParameterValue" ReadOnly="true" HeaderText="Parameter Value" />
                        </Columns>
                    </asp:GridView>               
                </td>
            </tr>
        </table>
        <table class="PageTable" cellspacing="2" cellpadding="2">
           <tr>
                <td width="80">
                    <asp:Button id="btnAddParameter" Width="70px" Height="18" runat="server"  OnClick="BtnAddParameter_Click" Text="Add"
					    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
				    />
                </td>
                <td width="200">
                    <asp:DropDownList ID="ddlParameterFields" runat="server" ></asp:DropDownList>
                </td>
                <td width="150">
                    <asp:DropDownList ID="ddlComparison" runat="server">
                        <asp:ListItem>= (number)</asp:ListItem>
                        <asp:ListItem>= (word)</asp:ListItem>
                        <asp:ListItem>&gt; (number)</asp:ListItem>
                        <asp:ListItem>&gt; (word or date)</asp:ListItem>
                        <asp:ListItem>&lt; (number)</asp:ListItem>
                        <asp:ListItem>&lt; (word or date)</asp:ListItem>
                        <asp:ListItem>like</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td><asp:TextBox ID="txtParameterValue" MaxLength="50" runat="server"></asp:TextBox>
                </td>
           </tr>
        </table>
    </div>
</asp:Content>
