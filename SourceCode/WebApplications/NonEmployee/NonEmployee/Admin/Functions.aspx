﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Functions.aspx.cs" Inherits="NonEmployee.Admin.Functions" MasterPageFile="~/MasterNED.Master" Title="Functions"%>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvFunction" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no functions." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update" 
                             CellPadding="2" GridLines="Both" DataKeyNames="FunctionID"
                             PageSize="20" AllowPaging="true" OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                            <Columns>
                              <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="Update" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="35%" HeaderText="Function Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblFunction" Font-Size="8pt" runat="server"  Text='<%# Eval("FunctionName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUpdateFunction" MaxLength="50" Width="175px" Font-Size="8pt" runat="server" Text='<%# Eval("FunctionName")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdateFunction" ValidationGroup="Update" ControlToValidate=txtUpdateFunction runat="server"  ErrorMessage="Function is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtFunction" MaxLength="50" Width="175px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddFunction" ValidationGroup="Add" ControlToValidate=txtFunction runat="server"  ErrorMessage="Function is required.">*</asp:RequiredFieldValidator>
                                     <asp:CustomValidator ID="vldFunctionExists" runat="server" ValidationGroup="Add" ControlToValidate="txtFunction" ErrorMessage="The Function already exists.">*</asp:CustomValidator>
                               </FooterTemplate>
                            </asp:TemplateField>                            
                               <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldFunctionAssigned" runat="server" ValidationGroup="Delete" ErrorMessage="The Function is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%">
                            <tr>
                                <td>Current Function:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentFunction" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentFunction" runat="server" ErrorMessage="Current Function is required."
                                        ValidationGroup="Global" ControlToValidate="ddlCurrentFunction">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Function:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewFunction" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Function is required."
                                        ValidationGroup="Global" ControlToValidate="ddlNewFunction">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" runat="server" Text="Update Globally" 
									    CssClass="buttonsSubmit" CausesValidation="false" 
										ValidationGroup="Global" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Update" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="Delete" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="Global" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                               </td>
                            </tr>       
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
           </table>        
    </div>
</asp:Content>