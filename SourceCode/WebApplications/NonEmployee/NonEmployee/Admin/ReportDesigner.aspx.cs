﻿// ----------------------------------------------------------------------
// <copyright file="ReportDesigner.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// ----------------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;
    using ReportingTool;

    /// <summary>
    /// This is the page used to design reports.
    /// </summary>
    public partial class ReportDesigner : System.Web.UI.Page
    {
        /// <summary>
        /// This is our instance of the utility class.
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (this.IsPostBack == false)
            {
                this.RefreshReports();

                this.RefreshTables();

                this.SetControls();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDesignNewReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnDesignNewReport_Click(object sender, EventArgs e)
        {
            this.ClearPageControls();
            this.SetControls();
        }

        /// <summary>
        /// Clears the page controls.
        /// </summary>
        private void ClearPageControls()
        {
            this.ddlTables.ClearSelection();
            this.ddlTables.SelectedIndex = 0;
            this.ddlReportViewableBy.ClearSelection();
            this.ddlReportViewableBy.SelectedIndex = 0;
            this.txtParameterValue.Text = String.Empty;
            this.txtReportFooter.Text = String.Empty;
            this.txtReportName.Text = String.Empty;
            this.txtReportSelection.Text = String.Empty;
            this.txtReportName.Text = String.Empty;
            this.ddlReports.ClearSelection();
            this.ddlReports.SelectedIndex = 0;
            this.ddlParameterFields.Items.Clear();
            this.BindParameters(Int32.Parse(this.ddlReports.SelectedValue));
            this.ddlTables.Text = String.Empty;
            this.ddlComparison.Text = "=";
        }

        /// <summary>
        /// Handles the Click event of the btnSaveReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnSaveReport_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveReport");

            this.vldCustomColumns.IsValid = Convert.ToBoolean(lstTakenFields.Items.Count>0);

            if (Page.IsValid)
            {
                if (this.ddlReports.SelectedValue == "0")
                {
                    ////This is going to insert the report.
                    if (this.myUtility.SqlConn.State != ConnectionState.Open)
                    {
                        this.myUtility.SqlConn.Open();
                    }
                    System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = this.myUtility.SqlConn;
                    cmd.CommandText = "dbo.sp_InsertReport";
                    cmd.Parameters.Add(new SqlParameter("@ReportTitle", this.txtReportName.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportFooter", this.txtReportFooter.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportSelectionQuery", this.txtReportSelection.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportOutType", 1));
                    cmd.Parameters.Add(new SqlParameter("@ReportOutput", this.txtReportName.Text));

                    ADUserEntity myUser = ADUtility.GetADUserByUserName(ADUtility.GetUserName(Request));
                    cmd.Parameters.Add(new SqlParameter("@ReportOwner", myUser.UserName));
                    cmd.Parameters.Add(new SqlParameter("@ReportOwnerDisplay", myUser.DisplayName));

                    cmd.Parameters.Add(new SqlParameter("@ReportViewableBy", this.ddlReportViewableBy.SelectedValue));
                    SqlDataReader myReader = cmd.ExecuteReader();

                    string strReportID = String.Empty;
                    while (myReader.Read())
                    {
                        strReportID = myReader.GetValue(0).ToString();
                    }

                    this.RefreshReports();
                    this.myUtility.SqlConn.Close();

                    this.ddlReports.SelectedValue = strReportID;
                }
                else
                {
                    ////This is going to update the report.
                    if (this.myUtility.SqlConn.State != ConnectionState.Open)
                    {
                        this.myUtility.SqlConn.Open();
                    }
                    System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = this.myUtility.SqlConn;
                    cmd.CommandText = "dbo.sp_UpdateReport";
                    cmd.Parameters.Add(new SqlParameter("@ReportID", Convert.ToInt32(this.ddlReports.SelectedValue)));
                    cmd.Parameters.Add(new SqlParameter("@ReportTitle", this.txtReportName.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportFooter", this.txtReportFooter.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportSelectionQuery", this.txtReportSelection.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportOutType", 1));
                    cmd.Parameters.Add(new SqlParameter("@ReportOutput", this.txtReportName.Text));
                    cmd.Parameters.Add(new SqlParameter("@ReportViewableBy", this.ddlReportViewableBy.SelectedValue));
                    cmd.ExecuteNonQuery();
                    this.myUtility.SqlConn.Close();
                }
                this.SetControls();
                this.ResetAddParameterControls();

            } //// End Page.IsValid
        }

        /// <summary>
        /// Handles the Click event of the btnDeleteReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnDeleteReport_Click(object sender, EventArgs e)
        {
            System.Data.SqlClient.SqlCommand cmd;
            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            ////Deleting the report parameters.
            foreach (GridViewRow r in this.gvParameter.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = this.myUtility.SqlConn;
                    cmd.CommandText = "dbo.sp_DeleteParameter";
                    string strID = this.gvParameter.DataKeys[r.RowIndex]["ParameterID"].ToString();
                    cmd.Parameters.Add(new SqlParameter("@ReportParameterID", Convert.ToInt32(strID)));
                    cmd.ExecuteNonQuery();
                }
            }

            ////This is going to delete the report.
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = this.myUtility.SqlConn;
            cmd.CommandText = "dbo.sp_DeleteReport";
            cmd.Parameters.Add(new SqlParameter("@ReportID", Convert.ToInt32(this.ddlReports.SelectedValue)));
            cmd.ExecuteNonQuery();
            this.myUtility.SqlConn.Close();

            this.RefreshReports();
            this.ddlReports.ClearSelection();
            this.SetControls();
            this.ResetAddParameterControls();
        }

        /// <summary>
        /// Handles the Click event of the btnAddParameter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAddParameter_Click(object sender, EventArgs e)
        {
            if (this.ddlReports.SelectedValue != "0")
            {
                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = this.myUtility.SqlConn;
                cmd.CommandText = "dbo.sp_InsertReportParameter";
                cmd.Parameters.Add(new SqlParameter("@ReportID", Convert.ToInt32(this.ddlReports.SelectedValue)));
                cmd.Parameters.Add(new SqlParameter("@ParameterValue", this.txtParameterValue.Text));
                cmd.Parameters.Add(new SqlParameter("@ParameterField", this.ddlParameterFields.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@ParameterComparison", this.ddlComparison.SelectedValue));
                cmd.ExecuteNonQuery();
                this.myUtility.SqlConn.Close();

                this.BindParameters(Convert.ToInt32(this.ddlReports.SelectedValue));
                this.RebuildQuery();
                this.ResetAddParameterControls();
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlTables control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DdlTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstTakenFields.Items.Clear();
            this.lstAvailableFields.Items.Clear();
            this.ddlParameterFields.Items.Clear();

            ////Now we are going to get through the fields for the selelection.
            ////First we need to load the fields for the view or table.
            DataTable myFields = SqlUtility.SetTable("dbo.sp_GetColumnsForView", this.ddlTables.Text);
            string strQueryText = String.Empty;
            foreach (DataRow dr in myFields.Rows)
            {
                strQueryText = dr["VIEW_DEFINITION"].ToString();
            }

            foreach (string s in this.GetColumnsForView(strQueryText))
            {
                if (this.txtReportSelection.Text.Contains(s))
                {
                    this.lstTakenFields.Items.Add(s);
                }
                else
                {
                    this.lstAvailableFields.Items.Add(s);
                }

                this.ddlParameterFields.Items.Add(s);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnAddField control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAddField_Click(object sender, EventArgs e)
        {
            ListItemCollection items = new ListItemCollection();
            foreach (ListItem i in this.lstAvailableFields.Items)
            {
                if (i.Selected)
                {
                    this.lstTakenFields.Items.Add(new ListItem(i.Value, i.Value));
                }
                else
                {
                    items.Add(new ListItem(i.Value, i.Value));
                }
            }
            this.lstAvailableFields.Items.Clear();
            this.lstAvailableFields.DataSource = items;
            this.lstAvailableFields.DataBind();

            this.RebuildQuery();
        }

        /// <summary>
        /// Handles the Click event of the btnRemoveField control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnRemoveField_Click(object sender, EventArgs e)
        {
            ListItemCollection items = new ListItemCollection();
            foreach (ListItem i in this.lstTakenFields.Items)
            {
                if (i.Selected)
                {
                    this.lstAvailableFields.Items.Add(new ListItem(i.Value, i.Value));
                }
                else
                {
                    items.Add(new ListItem(i.Value, i.Value));
                }
            }

            this.lstTakenFields.Items.Clear();
            this.lstTakenFields.DataSource = items;
            this.lstTakenFields.DataBind();

            this.RebuildQuery();
        }

        /// <summary>
        /// Handles the Delete event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intID = Int32.Parse(this.gvParameter.DataKeys[e.RowIndex].Value.ToString());

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = this.myUtility.SqlConn;
            cmd.CommandText = "dbo.sp_DeleteParameter";
            cmd.Parameters.Add(new SqlParameter("@ReportParameterID", intID));
            cmd.ExecuteNonQuery();
            this.myUtility.SqlConn.Close();

            this.BindParameters(Convert.ToInt32(this.ddlReports.SelectedValue));
            this.RebuildQuery();
            this.ResetAddParameterControls();
        }

        /// <summary>
        /// Resets the add parameter controls.
        /// </summary>
        private void ResetAddParameterControls()
        {
            this.txtParameterValue.Text = String.Empty;
            this.ddlComparison.ClearSelection();
            this.ddlParameterFields.ClearSelection();
        }

        /// <summary>
        /// Sets the controls.
        /// </summary>
        private void SetControls()
        {
            this.btnAddParameter.Enabled = Convert.ToBoolean(this.ddlReports.SelectedValue != "0");
        }

        /// <summary>
        /// Binds the parameters.
        /// </summary>
        /// <param name="reportID">The report ID.</param>
        private void BindParameters(int reportID)
        {
            this.gvParameter.DataSource = this.myUtility.GetReportParameters(Int32.Parse(this.ddlReports.SelectedValue));
            this.gvParameter.DataBind();
        }

        /// <summary>
        /// Gets the columns for the view.
        /// </summary>
        /// <param name="longQuery">The long query.</param>
        /// <returns>The columns for the view selected.</returns>
        private List<string> GetColumnsForView(string longQuery)
        {
            List<string> myList = new List<string>();

            try
            {
                string[] myArray = new string[2] { "SELECT", "FROM" };
                string[] myColumnArray = new string[1] { "," };
                foreach (string s in longQuery.Split(myArray, StringSplitOptions.RemoveEmptyEntries)[1].Split(myColumnArray, StringSplitOptions.RemoveEmptyEntries))
                {
                    myList.Add(s.Trim().Replace("DISTINCT ", String.Empty).Replace("Distinct ", String.Empty).Replace("distinct ", String.Empty));
                }
            }
            catch
            {
                DataTable myDataTable = SqlUtility.SetTable("dbo.sp_GetTableColumns", this.ddlTables.SelectedItem.Text);
                foreach (DataRow dr in myDataTable.Rows)
                {
                    myList.Add(dr["COLUMN_NAME"].ToString());
                }
            }

            return myList;
        }

        /// <summary>
        /// Rebuilds the query.
        /// </summary>
        private void RebuildQuery()
        {
            if (this.ddlTables.Text.Length > 0)
            {
                int intCounter = 1;
                string strQuery = String.Empty;

                ////Setting up the selection portion.
                strQuery += "SELECT ";
                foreach (ListItem l in this.lstTakenFields.Items)
                {
                    if (intCounter == 1)
                    {
                        strQuery += l.Value + " ";
                    }
                    else
                    {
                        strQuery += ", " + l.Value + " ";
                    }

                    intCounter++;
                }

                strQuery += "FROM ";
                strQuery += this.ddlTables.SelectedValue;

                ////Now this section is going to setup the from clauses.
                intCounter = 0;
                Collection<ReportParameters> myParList = this.myUtility.GetReportParameters(Convert.ToInt32(this.ddlReports.SelectedValue));
////                Collection<ReportParameters> myParList = this.myUtility.GetReportParameters(Convert.ToInt32(Session["ReportID"]));
                if (myParList.Count > 0)
                {
                    strQuery += " WHERE ";
                }

                string strPlaceHolder = "";

                foreach (ReportParameters r in myParList)
                {
                    strPlaceHolder = r.ParameterField + r.ParameterID.ToString();
                    if (intCounter > 0)
                    {
                        strQuery += " AND ";
                    }

                    if (r.ParameterComparison == "like")
                    {
                        strQuery += r.ParameterField + " like '%" + strPlaceHolder + "%' ";
                    }
                    else if (r.ParameterComparison == "= (number)")
                    {
                        //strQuery += r.ParameterField + " " + r.ParameterComparison + " " + strPlaceHolder + " ";
                        strQuery += r.ParameterField + " = " + strPlaceHolder + " ";
                    }
                    else if (r.ParameterComparison == "= (word)")
                    {
                        strQuery += r.ParameterField + " = '" + strPlaceHolder + "' ";
                    }
                    else if (r.ParameterComparison == "> (number)")
                    {
                        strQuery += r.ParameterField + " > " + strPlaceHolder + " ";
                    }
                    else if (r.ParameterComparison == "> (word or date)")
                    {
                        strQuery += r.ParameterField + " > '" + strPlaceHolder + "' ";
                    }
                    else if (r.ParameterComparison == "< (number)")
                    {
                        strQuery += r.ParameterField + " < " + strPlaceHolder + " ";
                    }
                    else if (r.ParameterComparison == "< (word or date)")
                    {
                        strQuery += r.ParameterField + " < '" + strPlaceHolder + "' ";
                    }
                    else
                    {
                        strQuery += r.ParameterField + " " + r.ParameterComparison + " " + strPlaceHolder + " ";
                    }

                    intCounter++;
                }

                this.txtReportSelection.Text = strQuery;
            }
        }

        /// <summary>
        /// Refreshes the reports drop-down list.
        /// </summary>
        private void RefreshReports()
        {
            this.ddlReports.Items.Clear();
            this.ddlReports.Items.Add(new ListItem("-- Select Report --", "0"));
            string strUser = ADUtility.GetUserName(Request);

            DataTable dtReports = SqlUtility.SetTable("sp_GetReportsByOwner", strUser);

            foreach (DataRow r in dtReports.Rows)
            {
                this.ddlReports.Items.Add(new ListItem(r["ReportTitle"].ToString(), r["ReportID"].ToString()));
            }
        }

        /// <summary>
        /// Refreshes the tables.
        /// </summary>
        private void RefreshTables()
        {
            DataTable myTables = SqlUtility.SetTable("dbo.sp_GetReportingViews");
            this.ddlTables.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow dr in myTables.Rows)
            {
                this.ddlTables.Items.Add(new ListItem(dr["TABLE_NAME"].ToString(), dr["TABLE_NAME"].ToString()));
            }
        }

        /// <summary>
        /// Gets the report parameter.
        /// </summary>
        /// <param name="reportParameterID">The report parameter ID.</param>
        /// <returns>Gets a the report parameter.</returns>
        private ReportParameters GetReportParameter(int reportParameterID)
        {
            ReportParameters myParameter = new ReportParameters();
            DataTable myParameters = SqlUtility.SetTable("dbo.sp_GetReportParameter", reportParameterID);
            foreach (DataRow dr in myParameters.Rows)
            {
                myParameter.ParameterID = Convert.ToInt32(dr["ReportParameterID"].ToString());
                myParameter.ParameterField = dr["ParameterField"].ToString();
                myParameter.ParameterValue = dr["ParameterValue"].ToString();
                myParameter.ParameterComparison = dr["ParameterComparison"].ToString();
            }

            return myParameter;
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlReports control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportingTool.ReportEntity myEntity = this.myUtility.GetReportingEntity(Convert.ToInt32(this.ddlReports.SelectedValue));

            this.txtReportFooter.Text = myEntity.ReportFooter;
            this.txtReportName.Text = myEntity.ReportOutpath;
            this.txtReportSelection.Text = myEntity.ReportCommandText;
            this.txtReportName.Text = myEntity.ReportReportHeader;

            this.ddlReportViewableBy.ClearSelection();
            if (this.ddlReportViewableBy.Items.FindByValue(myEntity.ReportViewableBy) != null)
                this.ddlReportViewableBy.Items.FindByValue(myEntity.ReportViewableBy).Selected = true;

            ////This is going to see if there is an inner join, if there is, then it is completely useless to use the builder.
            if (myEntity.ReportCommandText.ToUpper().Contains(" JOIN "))
            {
                this.panelBuilder.Visible = false;
            }
            else
            {
                this.panelBuilder.Visible = true;
            }

            ////We only really need to worrk about if this is visible.
            if (this.panelBuilder.Visible == true)
            {
                ////Now we are going to figure out what table we are pulling the data.
                foreach (ListItem l in this.ddlTables.Items)
                {
                    if (myEntity.ReportCommandText.Contains(l.Value) && l.Value.Length > 0)
                    {
                        this.ddlTables.Text = l.Value;
                        break;
                    }
                }

                if (this.ddlTables.Text.Length > 0)
                {
                    ////Now we are going to get through the fields for the selelection.
                    ////First we need to load the fields for the view or table.

                    DataTable myFields = NonEmployee.Classes.SqlUtility.SetTable("dbo.sp_GetColumnsForView", this.ddlTables.Text);
                    string strQueryText = String.Empty;
                    foreach (DataRow dr in myFields.Rows)
                    {
                        strQueryText = dr["VIEW_DEFINITION"].ToString();
                    }

                    //// Clear the items and repopulate
                    this.lstAvailableFields.Items.Clear();
                    this.ddlParameterFields.Items.Clear();
                    this.lstTakenFields.Items.Clear();
                    foreach (string s in this.GetColumnsForView(strQueryText))
                    {
                        if (myEntity.ReportCommandText.Contains(s))
                        {
                            this.lstTakenFields.Items.Add(s);
                            this.ddlParameterFields.Items.Add(s);
                        }
                        else
                        {
                            this.lstAvailableFields.Items.Add(s);
                        }
                    }
                }
            }
            ////End the builder stuff.

            ////This is going to get the parameters.
            this.BindParameters(myEntity.ReportID);

            this.RebuildQuery();
            this.SetControls();
            this.ResetAddParameterControls();
        }
     } //// end class
}
