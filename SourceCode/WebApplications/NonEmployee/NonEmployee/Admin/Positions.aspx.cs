﻿// --------------------------------------------------------------
// <copyright file="Position.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------


namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class Positions : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                //EnterButton.TieButton(this.txtLastName, this.btnFindUser);

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                this.ddlSelectCompany.Items.Clear();
                this.ddlSelectCompany.Items.Add(new ListItem("--Select--", ""));
                this.ddlGlobalCompany.Items.Clear();
                this.ddlGlobalCompany.Items.Add(new ListItem("--Select--", ""));
                foreach (DataRow r in myUtility.TblCompanyCodes.Rows)
                {
                    this.ddlSelectCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                    this.ddlGlobalCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                }

                this.BindDT();
                this.myUtility.SqlConn.Close();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvPosition GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblPosition = SqlUtility.SetTable("sp_GetPositions");
            DataTable dt = this.myUtility.TblPosition;
            if (this.ddlSelectCompany.Items.Count > 0)
            {
                DataRow[] rows = dt.Select("CompanyCode = '" + this.ddlSelectCompany.SelectedValue + "'");
                DataTable dtSelect = dt.Clone();

                for (int x = 0; x < rows.Length; x++)
                {
                    dtSelect.ImportRow(rows[x]);
                }

                if (dtSelect.Rows.Count == 0)
                {
                    DataRow newRow = dtSelect.NewRow();
                    dtSelect.Rows.Add(newRow);
                }
                dt = dtSelect;

                Debug.WriteLine("Position dt rows: " + dt.Rows.Count.ToString() + "; PositionName dtSelect rows: " + dtSelect.Rows.Count.ToString());
            }

            this.gvPosition.DataSource = dt;
            this.gvPosition.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvPosition GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Position');");

                //// if the PositionName if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvPosition.DataKeys[e.Row.RowIndex]["PositionName"].ToString() != "");
            }
                
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("On Positions Footer Row");
                TextBox txtPositionName = (TextBox)e.Row.FindControl("txtPositionName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtPositionName, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvPosition GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strPositionName = this.gvPosition.DataKeys[e.RowIndex]["PositionName"].ToString();
            string strCompanyCode = this.gvPosition.DataKeys[e.RowIndex]["CompanyCode"].ToString();
            CustomValidator vldPositionNameAssigned = (CustomValidator)this.gvPosition.Rows[e.RowIndex].FindControl("vldPositionNameAssigned");

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            Page.Validate("Delete");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@PositionName", strPositionName));
            myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetPositionAssignees", myParamters);

            vldPositionNameAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete Position Name
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@PositionName", strPositionName));
                myDeleteParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeletePosition", myDeleteParamters);

                this.gvPosition.EditIndex = -1;
                this.BindDT();
            }

            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the Edit event of the gvPosition GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtPositionName = (TextBox)this.gvPosition.FooterRow.FindControl("txtPositionName");
            DropDownList ddlCompany = (DropDownList)this.gvPosition.FooterRow.FindControl("ddlCompany");

            foreach (IValidator val in Page.Validators)
            {
                if (val.ErrorMessage == "The Position already exists.")
                {
                    Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                    myParamters.Add(new SqlParameter("@PositionName", txtPositionName.Text));
                    myParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));
                    DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetPositionByPositionByCompanyCode", myParamters);

                    val.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);
                }
            }

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@PositionName", txtPositionName.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertPosition", myParamters);

                this.gvPosition.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Gets the company list.
        /// </summary>
        /// <returns></returns>
        public DataTable GetCompanyList()
        {
            DataRow[] rows = myUtility.TblCompanyCodes.Select();
            DataTable dtClone = myUtility.TblCompanyCodes.Clone();
            DataRow newRow = dtClone.NewRow();
            newRow["CompanyCode"] = "";
            newRow["CompanyCodeDesc"] = "--Select--";
            dtClone.Rows.Add(newRow);

            for (int x = 0; x < rows.Length; x++)
            {
                dtClone.ImportRow(rows[x]);
            }

            return dtClone;
        }
        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            this.gvPosition.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlSelectCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlSelectCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlGlobalCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlGlobalCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlCurrentPositionName.Items.Clear();
            this.ddlCurrentPositionName.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewPositionName.Items.Clear();
            this.ddlNewPositionName.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] rows = this.myUtility.TblPosition.Select("CompanyCode = '" + this.ddlGlobalCompany.SelectedValue + "'");
            foreach (DataRow r in rows)
            {
                this.ddlCurrentPositionName.Items.Add(new ListItem(r["PositionName"].ToString(), r["PositionName"].ToString()));
                this.ddlNewPositionName.Items.Add(new ListItem(r["PositionName"].ToString(), r["PositionName"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Global");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentPositionName", this.ddlCurrentPositionName.SelectedValue));
                myParamters.Add(new SqlParameter("@NewPositionName", this.ddlNewPositionName.SelectedValue));
                myParamters.Add(new SqlParameter("@CompanyCode", this.ddlGlobalCompany.SelectedValue));

                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNEPositionGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }   
    } //// end class
}
