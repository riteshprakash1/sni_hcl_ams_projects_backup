﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Positions.aspx.cs" Inherits="NonEmployee.Admin.Positions"  MasterPageFile="~/MasterNED.Master" Title="Positions"%>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:10%">Company:</td>
                    <td style="width:50%">
                        <asp:DropDownList ID="ddlSelectCompany" width="250px" AutoPostBack="true" 
                            DataTextField="CompanyCodeDesc" DataValueField="CompanyCode" Font-Size="8pt" runat="server" 
                            onselectedindexchanged="ddlSelectCompany_SelectedIndexChanged"></asp:DropDownList></td>
                    <td style="width:40%; border-left:1px solid #cccccc;"><b>Globally Update NED Positions</b></td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvPosition" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no positions." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowCancelingEdit="GV_Add"
                             CellPadding="2" GridLines="Both" DataKeyNames="PositionName,CompanyCode"
                             PageSize="20" AllowPaging="true" OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                            <Columns>
                              <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                 <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldPositionNameAssigned" runat="server" ValidationGroup="Delete" ErrorMessage="The Position Name is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                               <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="50%" HeaderText="Position Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblPositionName" Font-Size="8pt" runat="server"  Text='<%# Eval("PositionName")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtPositionName" MaxLength="100" Width="250px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddPositionName" ValidationGroup="Add" ControlToValidate="txtPositionName" runat="server"  ErrorMessage="Position Name is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldPositionNameExists" runat="server" ValidationGroup="Add" ControlToValidate="txtPositionName" ErrorMessage="The Position already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="25%" HeaderText="Company">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" Font-Size="8pt" runat="server"  Text='<%# Eval("CompanyCodeDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlCompany" width="150" DataTextField="CompanyCodeDesc" DataValueField="CompanyCode"  DataSource='<%# GetCompanyList()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldRequiredCompany" ValidationGroup="Add" ControlToValidate="ddlCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%" border="0">
                            <tr>
                                <td>Company:</td>
                                <td>
                                    <asp:DropDownList ID="ddlGlobalCompany" Width="200px" 
                                        Font-Size="8pt" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="ddlGlobalCompany_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldGlobalRequiredCompany" ValidationGroup="Global" ControlToValidate="ddlGlobalCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Current Position Name:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentPositionName" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentPositionName" runat="server" ErrorMessage="Current Position Name is required."
                                        ValidationGroup="Global" ControlToValidate="ddlCurrentPositionName">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Position Name:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewPositionName" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Position Name is required."
                                        ValidationGroup="Global" ControlToValidate="ddlNewPositionName">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" runat="server" Text="Update Globally" 
									    CssClass="buttonsSubmit" CausesValidation="false" 
										ValidationGroup="Global" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="Global" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="vldSummary1" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Update" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="vldSummary3" ValidationGroup="Delete" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
           </table>        
    </div>
</asp:Content>