﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Locations.aspx.cs" Inherits="NonEmployee.Admin.Locations" MasterPageFile="~/MasterNED.Master" Title="Locations" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:10%">Company:</td>
                    <td style="width:55%"><asp:DropDownList ID="ddlSelectCompany" width="250px" AutoPostBack="true" 
                            DataTextField="CompanyCodeDesc" DataValueField="CompanyCode" Font-Size="8pt" runat="server" 
                            onselectedindexchanged="ddlSelectCompany_SelectedIndexChanged"></asp:DropDownList></td>
                    <td style="width:35%; border-left:1px solid #cccccc;"><b>Globally Update NED Locations</b></td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvLocation" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no locations." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update" 
                             CellPadding="2" GridLines="Both" DataKeyNames="PaSubCode,CompanyCode"
                             PageSize="20" AllowPaging="true" OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                            <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="Update" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Location Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaSubCode" Font-Size="8pt" runat="server"  Text='<%# Eval("PaSubCode")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtPaSubCode" MaxLength="20" Width="50px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddPaSubCode" ValidationGroup="Add" ControlToValidate="txtPaSubCode" runat="server"  ErrorMessage="Location Code is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldPaSubCodeExists" runat="server" ValidationGroup="Add" ControlToValidate="txtPaSubCode" ErrorMessage="The Location Code already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="35%" HeaderText="Location Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaSubDesc" Font-Size="8pt" runat="server"  Text='<%# Eval("PaSubDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUpdatePaSubDesc" MaxLength="50" Width="175px" Font-Size="8pt" runat="server" Text='<%# Eval("PaSubDesc")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdatePaSubDesc" ValidationGroup="Update" ControlToValidate="txtUpdatePaSubDesc" runat="server"  ErrorMessage="Location Name is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtPaSubDesc" MaxLength="50" Width="175px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddPaSubDesc" ValidationGroup="Add" ControlToValidate="txtPaSubDesc" runat="server"  ErrorMessage="Location Name is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="30%" HeaderText="Company">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" Font-Size="8pt" runat="server"  Text='<%# Eval("CompanyCodeDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlCompany" width="175px" DataTextField="CompanyCodeDesc" DataValueField="CompanyCode"  DataSource='<%# GetCompanyList()%>' Font-Size="8pt" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldRequiredCompany" ValidationGroup="Add" ControlToValidate="ddlCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                               <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldPaSubAssigned" runat="server" ValidationGroup="Delete" ErrorMessage="The Location Code is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                            </asp:TemplateField> 
                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%">
                            <tr>
                                <td style="width:35%">Company:</td>
                                <td>
                                    <asp:DropDownList ID="ddlGlobalCompany" Width="200px" 
                                        Font-Size="8pt" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="ddlGlobalCompany_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldGlobalRequiredCompany" ValidationGroup="Global" ControlToValidate="ddlGlobalCompany" runat="server" ErrorMessage="You must select a Company.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Current Location Code:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentPaSubCode" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentPaSubCode" runat="server" ErrorMessage="Current Location Code is required."
                                        ValidationGroup="Global" ControlToValidate="ddlCurrentPaSubCode">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Location Code:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewPaSubCode" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Location Code is required."
                                        ValidationGroup="Global" ControlToValidate="ddlNewPaSubCode">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" runat="server" Text="Update Globally" 
									    CssClass="buttonsSubmit" CausesValidation="false" 
										ValidationGroup="Global" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Update" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="Delete" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="Global" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                               </td>
                            </tr>       
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
           </table>        
    </div>
</asp:Content>