﻿// --------------------------------------------------------------
// <copyright file="Locations.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class Locations : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                //EnterButton.TieButton(this.txtLastName, this.btnFindUser);

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                this.ddlSelectCompany.Items.Clear();
                this.ddlSelectCompany.Items.Add(new ListItem("--Select--", ""));
                this.ddlGlobalCompany.Items.Clear();
                this.ddlGlobalCompany.Items.Add(new ListItem("--Select--", ""));
                foreach (DataRow r in myUtility.TblCompanyCodes.Rows)
                {
                    this.ddlSelectCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                    this.ddlGlobalCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));

                }

                this.BindDT();
                this.myUtility.SqlConn.Close();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvLocations GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblPersonnelSubArea = SqlUtility.SetTable("sp_GetPersonnelSubAreas");
            DataTable dt = this.myUtility.TblPersonnelSubArea;
            if (this.ddlSelectCompany.Items.Count > 0)
            {
                DataRow[] rows = dt.Select("CompanyCode = '" + this.ddlSelectCompany.SelectedValue + "'");
                DataTable dtSelect = dt.Clone();

                for (int x = 0; x < rows.Length; x++)
                {
                    dtSelect.ImportRow(rows[x]);
                }

                if (dtSelect.Rows.Count == 0)
                {
                    DataRow newRow = dtSelect.NewRow();
                    dtSelect.Rows.Add(newRow);
                }
                dt = dtSelect;

            }

            this.gvLocation.DataSource = dt;
            this.gvLocation.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvLocations GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Location Code');");

                //// if the PaSubCode if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvLocation.DataKeys[e.Row.RowIndex]["PaSubCode"].ToString() != "");

                if (e.Row.RowIndex == this.gvLocation.EditIndex)
                {
                    TextBox txtUpdatePaSubDesc = (TextBox)e.Row.FindControl("txtUpdatePaSubDesc");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdatePaSubDesc, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In Locations Footer");
                TextBox txtPaSubDesc = (TextBox)e.Row.FindControl("txtPaSubDesc");
                TextBox txtPaSubCode = (TextBox)e.Row.FindControl("txtPaSubCode");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtPaSubDesc, btnAdd);
                EnterButton.TieButton(txtPaSubCode, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvLocations GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strPaSubCode = this.gvLocation.DataKeys[e.RowIndex]["PaSubCode"].ToString();
            string strCompanyCode = this.gvLocation.DataKeys[e.RowIndex]["CompanyCode"].ToString();
            CustomValidator vldPaSubAssigned = (CustomValidator)this.gvLocation.Rows[e.RowIndex].FindControl("vldPaSubAssigned");

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            Page.Validate("Delete");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@PaSubCode", strPaSubCode));
            myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetPaSubCodeAssignees", myParamters);

            vldPaSubAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete PaSubCode
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@PaSubCode", strPaSubCode));
                myDeleteParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeletePersonnelSubArea", myDeleteParamters);

                this.gvLocation.EditIndex = -1;
                this.BindDT();
            }

            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the Edit event of the gvLocation GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvLocation.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Update event of the gvLocation GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("Update");

            if (Page.IsValid)
            {
                string strPaSubCode = this.gvLocation.DataKeys[e.RowIndex]["PaSubCode"].ToString();
                string strCompanyCode = this.gvLocation.DataKeys[e.RowIndex]["CompanyCode"].ToString();
                TextBox txtUpdatePaSubDesc = (TextBox)this.gvLocation.Rows[e.RowIndex].FindControl("txtUpdatePaSubDesc");

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                ////Update PaSubCode
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@PaSubCode", strPaSubCode));
                myParamters.Add(new SqlParameter("@PaSubDesc", txtUpdatePaSubDesc.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdatePersonnelSubArea", myParamters);

                this.gvLocation.EditIndex = -1;
                this.BindDT();

                this.myUtility.SqlConn.Close();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvLocations GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtPaSubCode = (TextBox)this.gvLocation.FooterRow.FindControl("txtPaSubCode");
            TextBox txtPaSubDesc = (TextBox)this.gvLocation.FooterRow.FindControl("txtPaSubDesc");
            DropDownList ddlCompany = (DropDownList)this.gvLocation.FooterRow.FindControl("ddlCompany");
            CustomValidator vldPaSubCodeExists = (CustomValidator)this.gvLocation.FooterRow.FindControl("vldPaSubCodeExists");

            Collection<SqlParameter> myVldParamters = new Collection<SqlParameter>();
            myVldParamters.Add(new SqlParameter("@PaSubCode", txtPaSubCode.Text));
            myVldParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetPaSubCodesByPaSubCodeByCompanyCode", myVldParamters);

            vldPaSubCodeExists.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);


            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@PaSubCode", txtPaSubCode.Text));
                myParamters.Add(new SqlParameter("@PaSubDesc", txtPaSubDesc.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertPersonnelSubArea", myParamters);

                this.gvLocation.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Gets the company code list.
        /// </summary>
        /// <returns></returns>
        public DataTable GetCompanyList()
        {
            DataRow[] rows = myUtility.TblCompanyCodes.Select();
            DataTable dtClone = myUtility.TblCompanyCodes.Clone();
            DataRow newRow = dtClone.NewRow();
            newRow["CompanyCode"] = "";
            newRow["CompanyCodeDesc"] = "--Select--";
            dtClone.Rows.Add(newRow);

            for (int x = 0; x < rows.Length; x++)
            {
                dtClone.ImportRow(rows[x]);
            }

            return dtClone;
        }
        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvLocation.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlSelectCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlSelectCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //gvLocation.PageIndex = 1;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlGlobalCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlGlobalCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlCurrentPaSubCode.Items.Clear();
            this.ddlCurrentPaSubCode.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewPaSubCode.Items.Clear();
            this.ddlNewPaSubCode.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] rows = this.myUtility.TblPersonnelSubArea.Select("CompanyCode = '" + this.ddlGlobalCompany.SelectedValue + "'");
            foreach (DataRow r in rows)
            {
                this.ddlCurrentPaSubCode.Items.Add(new ListItem(r["PaSubCode"].ToString() + " - " + r["PaSubDesc"].ToString(), r["PaSubCode"].ToString()));
                this.ddlNewPaSubCode.Items.Add(new ListItem(r["PaSubCode"].ToString() + " - " + r["PaSubDesc"].ToString(), r["PaSubCode"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Global");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentPaSubCode", this.ddlCurrentPaSubCode.SelectedValue));
                myParamters.Add(new SqlParameter("@NewPaSubCode", this.ddlNewPaSubCode.SelectedValue));
                myParamters.Add(new SqlParameter("@CompanyCode", this.ddlGlobalCompany.SelectedValue));

                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNEPaSubCodeGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }   
    } //// end class
}
