﻿// --------------------------------------------------------------
// <copyright file="UserList.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    /// <summary>
    /// This is the User List page
    /// </summary>
    public partial class UserList : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("Start Page_Load");
            
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin) 
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            
            }

            if (!Page.IsPostBack)
            {
                EnterButton.TieButton(this.txtLastName, this.btnFindUser);

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                this.myUtility.TblAppRoles = SqlUtility.SetTable("sp_GetAppRoles");
                this.myUtility.TblUsers = SqlUtility.SetTable("sp_GetAppUsers");

                this.myUtility.SqlConn.Close();

                this.BindDT();
            }

            Session["Utility"] = this.myUtility;
            Debug.WriteLine("End Page_Load");
        } //// end Page_Load

        /// <summary>
        /// Gets the user role list.
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserRoleList()
        {
            DataRow[] rows = myUtility.TblAppRoles.Select();
            DataTable dtClone = myUtility.TblAppRoles.Clone();
            DataRow newRow = dtClone.NewRow();
            newRow["AppRoleID"] = 0;
            newRow["AppRoleDesc"] = "--Select--";
            dtClone.Rows.Add(newRow);

            for (int x = 0; x < rows.Length; x++)
            {
                dtClone.ImportRow(rows[x]);
            }

            return dtClone;
        }


        /// <summary>
        /// Binds the users table to the gvUsers GridView.
        /// </summary>
        protected void BindDT()
        {
            this.gvUser.DataSource = this.myUtility.TblUsers;
            this.gvUser.DataBind();
        }

        /// <summary>
        /// Handles the Add event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
                Page.Validate("Add");

                TextBox txtUserName = (TextBox)this.gvUser.FooterRow.FindControl("txtAddUserName");
                DropDownList ddlAddUserRole = (DropDownList)this.gvUser.FooterRow.FindControl("ddlAddUserRole");
                CustomValidator vldUserExists = (CustomValidator)this.gvUser.FooterRow.FindControl("vldUserExists");
                CustomValidator vldAddUserInAD = (CustomValidator)this.gvUser.FooterRow.FindControl("vldAddUserInAD");

                DataRow[] rows = this.myUtility.TblUsers.Select("username='" + txtUserName.Text + "'");
                vldUserExists.IsValid = !Convert.ToBoolean(rows.Length > 0);

                ADUserEntity myUser = null;

                if (txtUserName.Text != String.Empty)
                {
                    myUser = ADUtility.GetADUserByUserName(txtUserName.Text);
                    vldAddUserInAD.IsValid = Convert.ToBoolean(myUser != null);
                }

                if (Page.IsValid)
                {
                    int intID = 0;

                    if (this.myUtility.SqlConn.State != ConnectionState.Open)
                    {
                        this.myUtility.SqlConn.Open();
                    }

                    System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_InsertAppUser";
                    cmd.Connection = this.myUtility.SqlConn;

                    cmd.Parameters.Add(new SqlParameter("@username", txtUserName.Text));
                    cmd.Parameters.Add(new SqlParameter("@lastname", myUser.LastName));
                    cmd.Parameters.Add(new SqlParameter("@firstname", myUser.FirstName));
                    cmd.Parameters.Add(new SqlParameter("@appRoleID", Int32.Parse(ddlAddUserRole.SelectedValue)));
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", this.myUtility.ADAccount));
                    SqlParameter paramID = new SqlParameter("@userID", SqlDbType.Int);
                    paramID.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramID);

                    int rowsUpdated = cmd.ExecuteNonQuery();
                    intID = Int32.Parse(paramID.Value.ToString());

                    this.myUtility.TblUsers = SqlUtility.SetTable("sp_GetAppUsers");

                    this.gvUser.EditIndex = -1;
                    this.BindDT();

                    this.myUtility.SqlConn.Close();

                } //// Page.IsValid

        }  ////end GV_Add

        /// <summary>
        /// Handles the RowDataBound event of the gvUsers GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType  == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('user');");
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddUserName = (TextBox)e.Row.FindControl("txtAddUserName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddUserName, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvUsers GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intID = Int32.Parse(this.gvUser.DataKeys[e.RowIndex].Value.ToString()); 
            
            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            ////Delete User and Roles
            System.Data.SqlClient.SqlCommand cmdDelete = new SqlCommand();
            cmdDelete.CommandType = CommandType.StoredProcedure;
            cmdDelete.CommandText = "sp_DeleteAppUser";
            cmdDelete.Connection = this.myUtility.SqlConn;
            cmdDelete.Parameters.Add(new SqlParameter("@userID", intID));

            int rowsUpdated = cmdDelete.ExecuteNonQuery();

            this.myUtility.TblUsers = SqlUtility.SetTable("sp_GetAppUsers");

            this.gvUser.EditIndex = -1;
            this.BindDT();

            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnFindUser control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnFindUser_Click(object sender, EventArgs e)
        {
            Page.Validate("FindUser");

            if (Page.IsValid)
            {
                this.gvFindUsers.DataSource = ADUtility.GetADUsersByLastName(this.txtLastName.Text);
                this.gvFindUsers.DataBind();
            }
        }  //// end GV_Update

        /// <summary>
        /// Handles the Edit event of the gvFindUsers GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Select(object sender, GridViewEditEventArgs e)
        {
            string strUserName = this.gvFindUsers.DataKeys[e.NewEditIndex]["username"].ToString();
            TextBox txtUserName = (TextBox)this.gvUser.FooterRow.FindControl("txtAddUserName");
            txtUserName.Text = strUserName;
        }
    } //// end class
}
