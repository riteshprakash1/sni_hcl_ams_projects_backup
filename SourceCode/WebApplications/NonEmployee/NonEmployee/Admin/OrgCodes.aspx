﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgCodes.aspx.cs" Inherits="NonEmployee.Admin.OrgCodes" MasterPageFile="~/MasterNED.Master" Title="Organization Codes"%>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Content3" runat="server">
    <div id="SearchPage">
            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:GridView ID="gvOrg" runat="server" Width="100%" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                            EmptyDataText="There are no organization codes." OnRowDeleting="GV_Delete" ShowFooter="true"
                            OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update" 
                             CellPadding="2" GridLines="Both" DataKeyNames="OrgCode"
                             PageSize="20" AllowPaging="true" OnPageIndexChanging="GV_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" 
                            >
                            <RowStyle HorizontalAlign="left" />
                            <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                            <Columns>
                              <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										ValidationGroup="Update" ></asp:Button>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" ValidationGroup="Add" CausesValidation="false" BorderColor="White"></asp:Button>                                
							    </FooterTemplate>
                            </asp:TemplateField> 
                           <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Org. Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrgCode" Font-Size="8pt" runat="server"  Text='<%# Eval("OrgCode")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtOrgCode" MaxLength="20" Width="50px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddOrgCode" ValidationGroup="Add" ControlToValidate=txtOrgCode runat="server"  ErrorMessage="Organization Code is required.">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="vldOrgCodeExists" runat="server" ValidationGroup="Add" ControlToValidate="txtOrgCode" ErrorMessage="The Organization Code already exists.">*</asp:CustomValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                           <asp:TemplateField HeaderStyle-Width="35%" HeaderText="Organization Code Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrgCodeDesc" Font-Size="8pt" runat="server"  Text='<%# Eval("OrgCodeDesc")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUpdateOrgCodeDesc" MaxLength="50" Width="175px" Font-Size="8pt" runat="server" Text='<%# Eval("OrgCodeDesc")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredUpdateOrgCodeDesc" ValidationGroup="Update" ControlToValidate=txtUpdateOrgCodeDesc runat="server"  ErrorMessage="Organization Code Name is required.">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtOrgCodeDesc" MaxLength="50" Width="175px" Font-Size="8pt" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldRequiredAddOrgCodeDesc" ValidationGroup="Add" ControlToValidate=txtOrgCodeDesc runat="server"  ErrorMessage="Organization Code Name is required.">*</asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-HorizontalAlign=Center FooterStyle-HorizontalAlign=Center>
                                <ItemTemplate>
                                    <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									    Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										></asp:Button>
                                     <asp:CustomValidator ID="vldOrgCodeAssigned" runat="server" ValidationGroup="Delete" ErrorMessage="The Organization Code is assigned to Non-Employees so it cannot be deleted.">*</asp:CustomValidator>
                               </ItemTemplate>
                            </asp:TemplateField> 

                          </Columns>
                        </asp:GridView> 
                    </td>
                    <td valign="top" style="border-left:1px solid #cccccc;">
                        <table width="100%">
                            <tr>
                                <td>Current Org. Code:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCurrentOrgCode" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vldCurrentOrgCode" runat="server" ErrorMessage="Current Organization Code is required."
                                        ValidationGroup="Global" ControlToValidate="ddlCurrentOrgCode">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>New Org. Code:</td>
                                <td>
                                    <asp:DropDownList ID="ddlNewOrgCode" Font-Size="8pt" runat="server" 
                                        Width="200px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Organization Code is required."
                                        ValidationGroup="Global" ControlToValidate="ddlNewOrgCode">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <asp:Button id="btnGlobalUpdate" runat="server" Text="Update Globally" 
									    CssClass="buttonsSubmit" CausesValidation="false" 
										ValidationGroup="Global" onclick="btnGlobalUpdate_Click" ></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblGlobalUpdateMsg" Font-Size="8pt" ForeColor="#FF7300" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Update" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="Delete" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="Global" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                               </td>
                            </tr>       
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
           </table>        
    </div>
</asp:Content>
