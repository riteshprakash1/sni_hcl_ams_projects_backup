﻿// --------------------------------------------------------------
// <copyright file="ProductLine.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class ProductLine : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.ddlSelectCompany.Items.Clear();
                this.ddlSelectCompany.Items.Add(new ListItem("--Select--", ""));
                this.ddlGlobalCompany.Items.Clear();
                this.ddlGlobalCompany.Items.Add(new ListItem("--Select--", ""));
                foreach (DataRow r in myUtility.TblCompanyCodes.Rows)
                {
                    this.ddlSelectCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                    this.ddlGlobalCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));

                }

                this.BindDT();
                this.myUtility.SqlConn.Close();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvProducts GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblSalesRepType = SqlUtility.SetTable("sp_GetSalesRepTypes");
            DataTable dt = this.myUtility.TblSalesRepType;
            if (this.ddlSelectCompany.Items.Count > 0)
            {
                DataRow[] rows = dt.Select("CompanyCode = '" + this.ddlSelectCompany.SelectedValue + "'");
                DataTable dtSelect = dt.Clone();

                for (int x = 0; x < rows.Length; x++)
                {
                    dtSelect.ImportRow(rows[x]);
                }

                if (dtSelect.Rows.Count == 0)
                {
                    DataRow newRow = dtSelect.NewRow();
                    dtSelect.Rows.Add(newRow);
                }
                dt = dtSelect;

            }

            this.gvProduct.DataSource = dt;
            this.gvProduct.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvProducts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Product Line');");

                //// if the SalesRepTypeCode if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvProduct.DataKeys[e.Row.RowIndex]["SalesRepTypeCode"].ToString() != "");

                if (e.Row.RowIndex == this.gvProduct.EditIndex)
                {
                    TextBox txtUpdateSalesRepTypeDesc = (TextBox)e.Row.FindControl("txtUpdateSalesRepTypeDesc");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdateSalesRepTypeDesc, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In ProductLine Footer");
                TextBox txtSalesRepTypeCode = (TextBox)e.Row.FindControl("txtSalesRepTypeCode");
                TextBox txtSalesRepTypeDesc = (TextBox)e.Row.FindControl("txtSalesRepTypeDesc");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtSalesRepTypeDesc, btnAdd);
                EnterButton.TieButton(txtSalesRepTypeCode, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvProducts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strSalesRepTypeCode = this.gvProduct.DataKeys[e.RowIndex]["SalesRepTypeCode"].ToString();
            string strCompanyCode = this.gvProduct.DataKeys[e.RowIndex]["CompanyCode"].ToString();
            CustomValidator vldSalesRepTypeAssigned = (CustomValidator)this.gvProduct.Rows[e.RowIndex].FindControl("vldSalesRepTypeAssigned");

            Page.Validate("Delete");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@SalesRepTypeCode", strSalesRepTypeCode));
            myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetSalesRepTypeCodeAssignees", myParamters);

            vldSalesRepTypeAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete Sales Rep Type Code
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@SalesRepTypeCode", strSalesRepTypeCode));
                myDeleteParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeleteSalesRepType", myDeleteParamters);

                this.gvProduct.EditIndex = -1;
                this.BindDT();
            }
        }

        /// <summary>
        /// Handles the Edit event of the gvProduct GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvProduct.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Update event of the gvProduct GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("Update");

            if (Page.IsValid)
            {
                string strSalesRepTypeCode = this.gvProduct.DataKeys[e.RowIndex]["SalesRepTypeCode"].ToString();
                string strCompanyCode = this.gvProduct.DataKeys[e.RowIndex]["CompanyCode"].ToString();
                TextBox txtUpdateSalesRepTypeDesc = (TextBox)this.gvProduct.Rows[e.RowIndex].FindControl("txtUpdateSalesRepTypeDesc");

                ////Update Sales Rep Type Code
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@SalesRepTypeCode", strSalesRepTypeCode));
                myParamters.Add(new SqlParameter("@SalesRepTypeDesc", txtUpdateSalesRepTypeDesc.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateSalesRepType", myParamters);

                this.gvProduct.EditIndex = -1;
                this.BindDT();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvProducts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtSalesRepTypeCode = (TextBox)this.gvProduct.FooterRow.FindControl("txtSalesRepTypeCode");
            TextBox txtSalesRepTypeDesc = (TextBox)this.gvProduct.FooterRow.FindControl("txtSalesRepTypeDesc");
            DropDownList ddlCompany = (DropDownList)this.gvProduct.FooterRow.FindControl("ddlCompany");
            CustomValidator vldSalesRepTypeCodeExists = (CustomValidator)this.gvProduct.FooterRow.FindControl("vldSalesRepTypeCodeExists");

            Collection<SqlParameter> myValidParamters = new Collection<SqlParameter>();
            myValidParamters.Add(new SqlParameter("@SalesRepTypeCode", txtSalesRepTypeCode.Text));
            myValidParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetSalesRepTypesBySRTypeCodeByCompanyCode", myValidParamters);

            vldSalesRepTypeCodeExists.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@SalesRepTypeCode", txtSalesRepTypeCode.Text));
                myParamters.Add(new SqlParameter("@SalesRepTypeDesc", txtSalesRepTypeDesc.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", ddlCompany.SelectedValue));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertSalesRepType", myParamters);

                this.gvProduct.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
        }


        /// <summary>
        /// Gets the company list.
        /// </summary>
        /// <returns></returns>
        public DataTable GetCompanyList()
        {
            DataRow[] rows = myUtility.TblCompanyCodes.Select();
            DataTable dtClone = myUtility.TblCompanyCodes.Clone();
            DataRow newRow = dtClone.NewRow();
            newRow["CompanyCode"] = "";
            newRow["CompanyCodeDesc"] = "--Select--";
            dtClone.Rows.Add(newRow);

            for (int x = 0; x < rows.Length; x++)
            {
                dtClone.ImportRow(rows[x]);
            }

            return dtClone;
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvProduct.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlSelectCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlSelectCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //gvProduct.PageIndex = 1;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlGlobalCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlGlobalCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlCurrentSalesRepTypeCode.Items.Clear();
            this.ddlCurrentSalesRepTypeCode.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewSalesRepTypeCode.Items.Clear();
            this.ddlNewSalesRepTypeCode.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] rows = this.myUtility.TblSalesRepType.Select("CompanyCode = '" + this.ddlGlobalCompany.SelectedValue + "'");
            foreach (DataRow r in rows)
            {
                this.ddlCurrentSalesRepTypeCode.Items.Add(new ListItem(r["SalesRepTypeDesc"].ToString(), r["SalesRepTypeCode"].ToString()));
                this.ddlNewSalesRepTypeCode.Items.Add(new ListItem(r["SalesRepTypeDesc"].ToString(), r["SalesRepTypeCode"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Global");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentSalesRepTypeCode", this.ddlCurrentSalesRepTypeCode.SelectedValue));
                myParamters.Add(new SqlParameter("@NewSalesRepTypeCode", this.ddlNewSalesRepTypeCode.SelectedValue));
                myParamters.Add(new SqlParameter("@CompanyCode", this.ddlGlobalCompany.SelectedValue));

                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNESalesRepTypeCodeGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }   
    } //// end class
}
