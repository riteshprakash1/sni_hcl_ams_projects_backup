﻿// --------------------------------------------------------------
// <copyright file="OrgCodes.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class OrgCodes : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                //EnterButton.TieButton(this.txtLastName, this.btnFindUser);

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                this.BindDT();
                this.LoadDropDowns();
                this.myUtility.SqlConn.Close();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvOrgs GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblOrgCode = SqlUtility.SetTable("sp_GetOrgCodes");

            if (this.myUtility.TblOrgCode.Rows.Count == 0)
            {
                DataRow newRow = this.myUtility.TblOrgCode.NewRow();
                this.myUtility.TblOrgCode.Rows.Add(newRow);
            }

            this.gvOrg.DataSource = this.myUtility.TblOrgCode;
            this.gvOrg.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvOrgs GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Organization Code');");

                //// if the OrgCode if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvOrg.DataKeys[e.Row.RowIndex]["OrgCode"].ToString() != "");

                if (e.Row.RowIndex == this.gvOrg.EditIndex)
                {
                    TextBox txtUpdateOrgCodeDesc = (TextBox)e.Row.FindControl("txtUpdateOrgCodeDesc");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdateOrgCodeDesc, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In OrgCodes Footer");
                TextBox txtOrgCode = (TextBox)e.Row.FindControl("txtOrgCode");
                TextBox txtOrgCodeDesc = (TextBox)e.Row.FindControl("txtOrgCodeDesc");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtOrgCodeDesc, btnAdd);
                EnterButton.TieButton(txtOrgCode, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvOrgs GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strOrgCode = this.gvOrg.DataKeys[e.RowIndex]["OrgCode"].ToString();
            CustomValidator vldOrgCodeAssigned = (CustomValidator)this.gvOrg.Rows[e.RowIndex].FindControl("vldOrgCodeAssigned");

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            Page.Validate("Delete");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@OrgCode", strOrgCode));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetOrgCodeAssignees", myParamters);

            vldOrgCodeAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete Org Code
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@OrgCode", strOrgCode));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeleteOrgCode", myDeleteParamters);

                this.gvOrg.EditIndex = -1;
                this.BindDT();
                this.LoadDropDowns();
            }

            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the Edit event of the gvOrg GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvOrg.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Update event of the gvOrg GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("Update");

            if (Page.IsValid)
            {
                string strOrgCode = this.gvOrg.DataKeys[e.RowIndex]["OrgCode"].ToString();
                TextBox txtUpdateOrgCodeDesc = (TextBox)this.gvOrg.Rows[e.RowIndex].FindControl("txtUpdateOrgCodeDesc");

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                ////Update Org Code
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@OrgCode", strOrgCode));
                myParamters.Add(new SqlParameter("@OrgCodeDesc", txtUpdateOrgCodeDesc.Text));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateOrgCode", myParamters);

                this.gvOrg.EditIndex = -1;
                this.BindDT();
                this.LoadDropDowns();

                this.myUtility.SqlConn.Close();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvOrgs GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtOrgCode = (TextBox)this.gvOrg.FooterRow.FindControl("txtOrgCode");
            TextBox txtOrgCodeDesc = (TextBox)this.gvOrg.FooterRow.FindControl("txtOrgCodeDesc");

            foreach (IValidator val in Page.Validators)
            {
                if (val.ErrorMessage == "The Organization Code already exists.")
                {
                    Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                    myParamters.Add(new SqlParameter("@OrgCode", txtOrgCode.Text));
                    DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetOrgCodesByOC", myParamters);

                    val.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);
                }
            }

            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@OrgCode", txtOrgCode.Text));
                myParamters.Add(new SqlParameter("@OrgCodeDesc", txtOrgCodeDesc.Text));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertOrgCode", myParamters);

                this.gvOrg.EditIndex = -1;
                this.BindDT();
                this.LoadDropDowns();

            } //// Page.IsValid
            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvOrg.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        private void LoadDropDowns()
        {
            this.ddlCurrentOrgCode.Items.Clear();
            this.ddlCurrentOrgCode.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewOrgCode.Items.Clear();
            this.ddlNewOrgCode.Items.Add(new ListItem("-- Select --", String.Empty));
            foreach (DataRow r in myUtility.TblOrgCode.Rows)
            {
                this.ddlCurrentOrgCode.Items.Add(new ListItem(r["OrgCode"].ToString() + " - " + r["OrgCodeDesc"].ToString(), r["OrgCode"].ToString()));
                this.ddlNewOrgCode.Items.Add(new ListItem(r["OrgCode"].ToString() + " - " + r["OrgCodeDesc"].ToString(), r["OrgCode"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("Global");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentOrgCode", this.ddlCurrentOrgCode.SelectedValue));
                myParamters.Add(new SqlParameter("@NewOrgCode", this.ddlNewOrgCode.SelectedValue));

                try
                {
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNEOrgCodeGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }   
    } //// end class
}
