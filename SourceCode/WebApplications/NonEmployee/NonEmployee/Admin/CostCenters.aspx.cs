﻿// --------------------------------------------------------------
// <copyright file="CostCenters.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.Admin
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;

    public partial class CostCenters : System.Web.UI.Page
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //// Set The Utility Object
            if (Session["Utility"] != null)
            {
                this.myUtility = (Utility)Session["Utility"];
            }
            else
            {
                this.myUtility = new Utility(Request, Session.SessionID);
                Session["Utility"] = this.myUtility;
            }

            //// if not an admin redirect to Unauthorized Page
            if (!this.myUtility.IsAdmin)
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                //EnterButton.TieButton(this.txtLastName, this.btnFindUser);

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                this.ddlSelectCompany.Items.Clear();
                this.ddlSelectCompany.Items.Add(new ListItem("--Select--", ""));
                this.ddlGlobalCompany.Items.Clear();
                this.ddlGlobalCompany.Items.Add(new ListItem("--Select--", ""));
                foreach (DataRow r in myUtility.TblCompanyCodes.Rows)
                {
                    this.ddlSelectCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                    this.ddlGlobalCompany.Items.Add(new ListItem(r["CompanyCodeDesc"].ToString(), r["CompanyCode"].ToString()));
                }

                this.BindDT();
                this.myUtility.SqlConn.Close();
            }

            this.lblGlobalUpdateMsg.Text = String.Empty;
            Session["Utility"] = this.myUtility;
        } //// end Page_Load

        /// <summary>
        /// Binds the users table to the gvCosts GridView.
        /// </summary>
        protected void BindDT()
        {
            this.myUtility.TblCostCenters = SqlUtility.SetTable("sp_GetCostCenters");
            DataTable dt = this.myUtility.TblCostCenters;
            if (this.ddlSelectCompany.Items.Count > 0)
            {
                DataRow[] rows = dt.Select("CompanyCode = '" + this.ddlSelectCompany.SelectedValue + "'");
                DataTable dtSelect = dt.Clone();

                for (int x = 0; x < rows.Length; x++)
                {
                    dtSelect.ImportRow(rows[x]);
                }

                if (dtSelect.Rows.Count == 0)
                {
                    DataRow newRow = dtSelect.NewRow();
                    dtSelect.Rows.Add(newRow);
                }
                dt = dtSelect;

                Debug.WriteLine("CostCenter dt rows: " + dt.Rows.Count.ToString() + "; CostCenter dtSelect rows: " + dtSelect.Rows.Count.ToString());
            }

            this.gvCost.DataSource = dt;
            this.gvCost.DataBind();

            Session["Utility"] = this.myUtility;
        }//// end BindDT

        /// <summary>
        /// Handles the RowDataBound event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Cost Center');");

                //// if the CostCenter if blank hide the row
                e.Row.Visible = Convert.ToBoolean(this.gvCost.DataKeys[e.Row.RowIndex]["CostCenter"].ToString() != "");

                if (e.Row.RowIndex == this.gvCost.EditIndex)
                {
                    TextBox txtUpdateCostCenterName = (TextBox)e.Row.FindControl("txtUpdateCostCenterName");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtUpdateCostCenterName, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Debug.WriteLine("In CostCenter Footer");
                TextBox txtCostCenter = (TextBox)e.Row.FindControl("txtCostCenter");
                TextBox txtCostCenterName = (TextBox)e.Row.FindControl("txtCostCenterName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtCostCenterName, btnAdd);
                EnterButton.TieButton(txtCostCenter, btnAdd);
            }
        } // end gv_RowDataBound

        /// <summary>
        /// Handles the Delete event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strCostCenter = this.gvCost.DataKeys[e.RowIndex]["CostCenter"].ToString();
            string strCompanyCode = this.gvCost.DataKeys[e.RowIndex]["CompanyCode"].ToString();
            CustomValidator vldCostCenterAssigned = (CustomValidator)this.gvCost.Rows[e.RowIndex].FindControl("vldCostCenterAssigned");
            
            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            Page.Validate("DeleteCostCenter");

            Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
            myParamters.Add(new SqlParameter("@CostCenter", strCostCenter));
            myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
            DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetCostCenterAssignees", myParamters);

            vldCostCenterAssigned.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);

            if (Page.IsValid)
            {
                ////Delete Cost Center
                Collection<SqlParameter> myDeleteParamters = new Collection<SqlParameter>();
                myDeleteParamters.Add(new SqlParameter("@CostCenter", strCostCenter));
                myDeleteParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));
                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_DeleteCostCenter", myDeleteParamters);

                this.gvCost.EditIndex = -1;
                this.BindDT();
            }

            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Handles the Edit event of the gvCost GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            this.gvCost.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the Update event of the gvCost GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateCostCenter");

            if (Page.IsValid)
            {
                string strCostCenter = this.gvCost.DataKeys[e.RowIndex]["CostCenter"].ToString();
                string strCompanyCode = this.gvCost.DataKeys[e.RowIndex]["CompanyCode"].ToString();
                TextBox txtUpdateCostCenterName = (TextBox)this.gvCost.Rows[e.RowIndex].FindControl("txtUpdateCostCenterName");

                if (this.myUtility.SqlConn.State != ConnectionState.Open)
                {
                    this.myUtility.SqlConn.Open();
                }

                ////Update Cost Center
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CostCenter", strCostCenter));
                myParamters.Add(new SqlParameter("@CostCenterName", txtUpdateCostCenterName.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", strCompanyCode));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateCostCenter", myParamters);

                this.gvCost.EditIndex = -1;
                this.BindDT();

                this.myUtility.SqlConn.Close();
            } // end Page.IsValid
        } // end GV_Update


        /// <summary>
        /// Handles the Edit event of the gvCosts GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddCostCenter");

            TextBox txtCostCenter = (TextBox)this.gvCost.FooterRow.FindControl("txtCostCenter");
            TextBox txtCostCenterName = (TextBox)this.gvCost.FooterRow.FindControl("txtCostCenterName");
            DropDownList ddlCompanyCode = (DropDownList)this.gvCost.FooterRow.FindControl("ddlCompany");

            foreach (IValidator val in Page.Validators)
            {
                if (val.ErrorMessage == "The Cost Center already exists.")
                {
                    Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                    myParamters.Add(new SqlParameter("@CostCenter", txtCostCenter.Text));
                    myParamters.Add(new SqlParameter("@CompanyCode", ddlCompanyCode.SelectedValue));
                    DataTable dt = SqlUtility.SqlExecuteQuery("sp_GetCostCenterByCCByCompanyCode", myParamters);

                    val.IsValid = !Convert.ToBoolean(dt.Rows.Count > 0);
                }
            }
       
            if (this.myUtility.SqlConn.State != ConnectionState.Open)
            {
                this.myUtility.SqlConn.Open();
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@costcenter", txtCostCenter.Text));
                myParamters.Add(new SqlParameter("@costcentername", txtCostCenterName.Text));
                myParamters.Add(new SqlParameter("@CompanyCode", ddlCompanyCode.SelectedValue));

                int rowsUpdated = SqlUtility.SqlExecuteNonQueryCount("sp_InsertCostCenter", myParamters);

                this.gvCost.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
            this.myUtility.SqlConn.Close();
        }

        /// <summary>
        /// Gets the company code list.
        /// </summary>
        /// <returns></returns>
        public DataTable GetCompanyList()
        {
            DataRow[] rows = myUtility.TblCompanyCodes.Select();
            DataTable dtClone = myUtility.TblCompanyCodes.Clone();
            DataRow newRow = dtClone.NewRow();
            newRow["CompanyCode"] = "";
            newRow["CompanyCodeDesc"] = "--Select--";
            dtClone.Rows.Add(newRow);

            for (int x = 0; x < rows.Length; x++)
            {
                dtClone.ImportRow(rows[x]);
            }

            return dtClone;
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the GV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void GV_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            gvCost.PageIndex = e.NewPageIndex;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlSelectCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlSelectCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //gvCost.PageIndex = 1;
            this.BindDT();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlGlobalCompany control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlGlobalCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlCurrentCostCenter.Items.Clear();
            this.ddlCurrentCostCenter.Items.Add(new ListItem("-- Select --", String.Empty));
            this.ddlNewCostCenter.Items.Clear();
            this.ddlNewCostCenter.Items.Add(new ListItem("-- Select --", String.Empty));
            DataRow[] rows = this.myUtility.TblCostCenters.Select("CompanyCode = '" + this.ddlGlobalCompany.SelectedValue + "'");
            foreach (DataRow r in rows)
            {
                this.ddlCurrentCostCenter.Items.Add(new ListItem(r["CostCenter"].ToString() + " - " + r["CostCenterName"].ToString(), r["CostCenter"].ToString()));
                this.ddlNewCostCenter.Items.Add(new ListItem(r["CostCenter"].ToString() + " - " + r["CostCenterName"].ToString(), r["CostCenter"].ToString()));
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGlobalUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGlobalUpdate_Click(object sender, EventArgs e)
        {
            Page.Validate("GlobalCostCenter");

            if (Page.IsValid)
            {
                Collection<SqlParameter> myParamters = new Collection<SqlParameter>();
                myParamters.Add(new SqlParameter("@CurrentCostCode", this.ddlCurrentCostCenter.SelectedValue));
                myParamters.Add(new SqlParameter("@NewCostCode", this.ddlNewCostCenter.SelectedValue));
                myParamters.Add(new SqlParameter("@CompanyCode", this.ddlGlobalCompany.SelectedValue));

                try
                {
                    Debug.WriteLine("Global Cost Update parameters " + this.ddlCurrentCostCenter.SelectedValue + " " + this.ddlNewCostCenter.SelectedValue + " " + this.ddlGlobalCompany.SelectedValue);
                    int recordCnt = SqlUtility.SqlExecuteNonQueryCount("sp_UpdateNECostCenterGlobal", myParamters);
                    this.lblGlobalUpdateMsg.Text = "Update Successful.  " + recordCnt.ToString() + " record(s) updated.";
                }
                catch (Exception ex)
                {
                    this.lblGlobalUpdateMsg.Text = "Update Failed.";
                    throw (ex);
                }
            } //// end if
        }

    } //// end class
}
