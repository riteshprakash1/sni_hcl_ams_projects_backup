﻿// --------------------------------------------------------------
// <copyright file="ReportException.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.ReportingTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This is out reporting exception class.
    /// </summary>
    public class ReportException : Exception
    {
        /// <summary>
        /// This is our new error message.
        /// </summary>
        private string myErrorMessage = String.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportException"/> class.
        /// </summary>
        public ReportException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportException"/> class.
        /// </summary>
        /// <param name="value">The error message value.</param>
        public ReportException(string value)
        {
            this.myErrorMessage = value;
        }

        /// <summary>
        /// Gets a message that describes the current exception.
        /// </summary>
        /// <value></value>
        /// <returns>The error message that explains the reason for the exception, or an empty string(String.Empty).</returns>
        public override string Message
        {
            get
            {
                return this.myErrorMessage;
            }
        }
    }
}
