﻿// --------------------------------------------------------------
// <copyright file="ReportingToolManager.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.ReportingTool
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using OfficeOpenXml;

    /// <summary>
    /// This is the reporting tool manager. <br />
    /// <b>USE THIS CLASS TO GENERATE YOUR REPORT.</b>
    /// </summary>
    public class ReportingToolManager
    {
        /// <summary>
        /// This is the internal report entity.
        /// </summary>
        private ReportEntity myReport;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingToolManager"/> class.
        /// </summary>
        /// <param name="value">My report.</param>
        public ReportingToolManager(ReportEntity value)
        {
            this.myReport = value;
        }

        /// <summary>
        /// Generates the report.
        /// </summary>
        /// <returns>Return whether or not the report was generated.</returns>
        public bool GenerateReport()
        {
            ////This is going to check all the various things to make sure they are set correctly.
            if (this.myReport == null)
            {
                throw new ReportException("There is no instance of the report object.");
            }

            if (this.myReport.ReportCommandText.Length <= 0)
            {
                throw new ReportException("The command text is not present.");
            }

            if (this.myReport.ReportConnectionString.Length <= 0)
            {
                throw new ReportException("The connection string is not set.");
            }

            if (this.myReport.ReportOutpath.Length <= 0)
            {
                throw new ReportException("There is no report path set.");
            }

            if (this.myReport.ReportReportHeader.Length <= 0)
            {
                throw new ReportException("There is no report header defined.");
            }
            ////CMB - End validation checking.

            if (this.myReport.ReportOutputType == ReportEntity.OutputType.stream)
            {
                ////This is if we are going to output the download to a stream.
                throw new ReportException("Not implemented yet.");
            }
            else
            {
                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : ReportingTool ReportConnectionString");
                //// Estabilishing our connection.
                SqlConnection myConnection = new SqlConnection(this.myReport.ReportConnectionString);
                ////Opening the connection.
                myConnection.Open();

                try
                {
                    //// Doing the command.
                    SqlCommand myCommand = new SqlCommand(this.myReport.ReportCommandText, myConnection);
                    ////Setting the command type.
                    myCommand.CommandType = this.myReport.ReportCommandType;

                    string strPlaceHolder = "";

                    ////This is if we have parameter to add to the query.
                    if (this.myReport.ReportParameters.Count > 0)
                    {
                        if (this.myReport.ReportCommandType == CommandType.StoredProcedure)
                        {
                            ////This is really easy if it is a stored procedure.
                            foreach (ReportParameters r in this.myReport.ReportParameters)
                            {
                                myCommand.Parameters.Add(new SqlParameter(r.ParameterField, r.ParameterValue));
                            }
                        }
                        else if (this.myReport.ReportCommandType == CommandType.Text)
                        {
                            string s = myCommand.CommandText;
                            Debug.WriteLine("Original CommandText: " + s);
                            ////If this is a text query, then we are going to have to replace the text.
                            foreach (ReportParameters r in this.myReport.ReportParameters)
                            {
                                strPlaceHolder = r.ParameterField + r.ParameterID.ToString();
                                s = s.ToLower().Replace(strPlaceHolder.ToLower(), (string)r.ParameterValue);
                            }

                            myCommand.CommandText = s;
                        }
                    }

                    ////Setting the data adapter.
                    SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                    Debug.WriteLine("Report Query: " + myCommand.CommandText);
                    ////The dataset.
                    DataSet myDataSet = new DataSet();
                    ////Filling the dataset.
                    Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Start ReportingTool myAdapter.Fill");
                    myAdapter.Fill(myDataSet);

                    Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Start ReportingTool Excel File");

                    //// Defining the file.
                    System.IO.FileInfo newFile = new FileInfo(@String.Empty + this.myReport.ReportOutpath);
                    //// Creating the workbook.
                    using (ExcelPackage xlsPackage = new ExcelPackage(newFile))
                    {
                        string strReportHeader = this.myReport.ReportReportHeader.Replace("'", "`"); //// Setting the header...
                        string strReportFooter = this.myReport.ReportFooter.Replace("'", "`");
                        
                        ExcelWorksheet myReportWorkSheet = xlsPackage.Workbook.Worksheets.Add(this.myReport.ReportReportHeader.Replace("'", "`"));
                        myReportWorkSheet.LoadFromDataTable(myDataSet.Tables[0], true, strReportHeader, strReportFooter);
                        xlsPackage.Save();

                        //myReportWorkSheet.Cell(1, 1).Value = this.myReport.ReportReportHeader.Replace("'", "`"); //// Setting the header...
             //           myReportWorkSheet.Cell(myDataSet.Tables[0].Rows.Count+2, 1).Value = this.myReport.ReportFooter.Replace("'", "`");
                        
                        //ExcelWorksheet myReportWorkSheet = xlsPackage.Workbook.Worksheets.Add(this.myReport.ReportReportHeader.Replace("'", "`"));
                        //myReportWorkSheet.Cell(1, 1).Value = this.myReport.ReportReportHeader.Replace("'", "`"); //// Setting the header...

                        ////// Looping through and setting the column names.
                        //int intColumnCounter = 1; ////Setting the column counter.
                        //foreach (DataColumn dc in myDataSet.Tables[0].Columns)
                        //{
                        //    myReportWorkSheet.Cell(2, intColumnCounter).Value = dc.ColumnName.Replace("'", "`");
                        //    intColumnCounter++;
                        //} ////End For

                        //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Finished Column Headers");
                        //int intRowCounter = 3;

                        //////This is going to loo through out rows.
                        //foreach (DataRow dr in myDataSet.Tables[0].Rows)
                        //{
                        //    intColumnCounter = 0;
                        //    Debug.WriteLine("Row " + intRowCounter.ToString() + ": " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.Millisecond.ToString());

                        //    //// This is going to loop through out columns.
                        //    while (intColumnCounter < dr.Table.Columns.Count)
                        //    {
                        //        myReportWorkSheet.Cell(intRowCounter, intColumnCounter + 1).Value = dr[intColumnCounter].ToString().Replace("'", "`");
                        //        intColumnCounter++;
                        //    } //// End while.
                        //    intRowCounter++;
                        //} //// End another for.

                        //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Finished Data Rows");
                        //myReportWorkSheet.Cell(intRowCounter, 1).Value = this.myReport.ReportFooter.Replace("'", "`");

                        //xlsPackage.Save();
                        Debug.WriteLine(DateTime.Now.ToLongTimeString() + " : Finished ReportingTool xlsPackage.Save()");
                    } ////End using.
                }
                catch(Exception ex)
                {

                    Debug.WriteLine("GenerateReport Error: " + ex.Message);
                    throw;
                }
                finally
                {
                    myConnection.Close();
                } ////End finally.
            } ////End else.

            return true;
        } ////End method.
    } //// end class
}
