﻿// -------------------------------------------------------------
// <copyright file="ReportEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace NonEmployee.ReportingTool
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// This is going to be our reporting request entity. <br />
    /// Unfortunately, we cann subclass or get an interface for the command object.
    /// Therefore, we have to do it the old fashioned way.
    /// </summary>
    public class ReportEntity
    {
        /// <summary>
        /// This is the command type for the report generator.
        /// </summary>
        private System.Data.CommandType myCommandType;

        /// <summary>
        /// This is the text for the command.
        /// </summary>
        private string myCommandText;

        /// <summary>
        /// These are the parameters for the command.
        /// </summary>
        private Collection<ReportParameters> myParameters;

        /// <summary>
        /// This is the output type for the report.
        /// </summary>
        private OutputType myOutputType;

        /// <summary>
        /// This is the path we want to output the report.
        /// </summary>
        private string myOutpath;

        /// <summary>
        /// This is the header we would like on the report.
        /// </summary>
        private string myReportHeader;

        /// <summary>
        /// This is the footer we would like to put on the report.
        /// </summary>
        private string myReportFooter;

        /// <summary>
        /// This report owner username.
        /// </summary>
        private string myReportOwner;

        /// <summary>
        /// This report owner displayname.
        /// </summary>
        private string myReportOwnerDisplay;

        /// <summary>
        /// Who the report can be viewed by.
        /// </summary>
        private string myReportViewableBy;

        /// <summary>
        /// The Report DB connection string.
        /// </summary>
        private string myReportConnectionString;

        /// <summary>
        /// This is the report id.  This is so we can interact with the database.
        /// </summary>
        private int myReportID;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportEntity"/> class.
        /// </summary>
        public ReportEntity()
        {
            this.myCommandText = String.Empty;
            this.myCommandType = System.Data.CommandType.Text;
            this.myReportViewableBy = String.Empty;
            this.myReportOwner = String.Empty;
            this.myReportOwnerDisplay = String.Empty;
            this.myOutpath = String.Empty;
            this.myOutputType = OutputType.file;
            this.myParameters = new Collection<ReportParameters>();
            this.myReportFooter = String.Empty;
            this.myReportHeader = String.Empty;
            this.myReportConnectionString = String.Empty;
            this.myReportID = 0;
        }

        /// <summary>
        /// This is output type you want for the report.
        /// </summary>
        public enum OutputType
        {
            /// <summary>
            /// This if you want to output the report directly to a stream.
            /// </summary>
            stream,

            /// <summary>
            /// This is if you want to output to a file.
            /// </summary>
            file
        }

        /// <summary>
        /// Gets or sets the report ID.
        /// </summary>
        /// <value>The report ID.</value>
        public int ReportID
        {
            get { return this.myReportID; }
            set { this.myReportID = value; }
        }

        /// <summary>
        /// Gets or sets my report owner username.
        /// </summary>
        /// <value>Report owner username.</value>
        public string ReportOwner
        {
            get { return this.myReportOwner; }
            set { this.myReportOwner = value; }
        }

        /// <summary>
        /// Gets or sets my report owner displayname.
        /// </summary>
        /// <value>Report owner displayname.</value>
        public string ReportOwnerDisplay
        {
            get { return this.myReportOwnerDisplay; }
            set { this.myReportOwnerDisplay = value; }
        }

        /// <summary>
        /// Gets or sets who the report can be viewed by.
        /// </summary>
        /// <value>Who the report can be viewed by.</value>
        public string ReportViewableBy
        {
            get { return this.myReportViewableBy; }
            set { this.myReportViewableBy = value; }
        }

        /// <summary>
        /// Gets or sets the report connection string.
        /// </summary>
        /// <value>Report Connection String.</value>
        public string ReportConnectionString
        {
            get { return this.myReportConnectionString; }
            set { this.myReportConnectionString = value; }
        }
        
        /// <summary>
        /// Gets or sets my report footer.
        /// </summary>
        /// <value>My report footer.</value>
        public string ReportFooter
        {
            get { return this.myReportFooter; }
            set { this.myReportFooter = value; }
        }

        /// <summary>
        /// Gets or sets my report header.
        /// </summary>
        /// <value>My report header.</value>
        public string ReportReportHeader
        {
            get { return this.myReportHeader; }
            set { this.myReportHeader = value; }
        }

        /// <summary>
        /// Gets or sets my outpath.
        /// </summary>
        /// <value>My outpath.</value>
        public string ReportOutpath
        {
            get { return this.myOutpath; }
            set { this.myOutpath = value; }
        }

        /// <summary>
        /// Gets or sets the type of my output.
        /// </summary>
        /// <value>The type of my output.</value>
        public OutputType ReportOutputType
        {
            get { return this.myOutputType; }
            set { this.myOutputType = value; }
        }

        /// <summary>
        /// Gets or sets my parameters.
        /// </summary>
        /// <value>My parameters.</value>
        public Collection<ReportParameters> ReportParameters
        {
            get { return this.myParameters; }
            set { this.myParameters = value; }
        }

        /// <summary>
        /// Gets or sets my command text.
        /// </summary>
        /// <value>My command text.</value>
        public string ReportCommandText
        {
            get { return this.myCommandText; }
            set { this.myCommandText = value; }
        }

        /// <summary>
        /// Gets or sets the type of my command.
        /// </summary>
        /// <value>The type of my command.</value>
        public System.Data.CommandType ReportCommandType
        {
            get { return this.myCommandType; }
            set { this.myCommandType = value; }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// The report header, which should be the title of the report.
        /// </returns>
        public override string ToString()
        {
            return this.ReportReportHeader;
        }
    } //// end class
}
