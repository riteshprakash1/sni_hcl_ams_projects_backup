﻿// --------------------------------------------------------------
// <copyright file="ReportParameters.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace NonEmployee.ReportingTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This is the reporting parameter object.
    /// </summary>
    public class ReportParameters
    {
        /// <summary>
        /// This is the value of the parameter.
        /// </summary>
        private object myParameterValue;

        /// <summary>
        /// This is the field we wish to do the subsitution.
        /// </summary>
        private string myParameterField;

        /// <summary>
        /// This is the comparison operator for the parameter.
        /// </summary>
        private string myParameterComparison;

        /// <summary>
        /// This is the parameter id for our parameter.
        /// </summary>
        private int myParameterID;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportParameters"/> class.
        /// </summary>
        public ReportParameters()
        {
            this.myParameterValue = String.Empty;
            this.myParameterField = String.Empty;
        }

        /// <summary>
        /// Gets or sets the parameter ID.
        /// </summary>
        /// <value>The parameter ID.</value>
        public int ParameterID
        {
            get { return this.myParameterID; }
            set { this.myParameterID = value; }
        }

        /// <summary>
        /// Gets or sets the parameter comparison.
        /// </summary>
        /// <value>The parameter comparison.</value>
        public string ParameterComparison
        {
            get { return this.myParameterComparison; }
            set { this.myParameterComparison = value; }
        }

        /// <summary>
        /// Gets or sets the parameter field.
        /// </summary>
        /// <value>The parameter field.</value>
        public string ParameterField
        {
            get
            {
                return this.myParameterField;
            }

            set
            {
                this.myParameterField = value;
            }
        }

        /// <summary>
        /// Gets or sets my parameter value.
        /// </summary>
        /// <value>My parameter value.</value>
        public object ParameterValue
        {
            get { return this.myParameterValue; }
            set { this.myParameterValue = value; }
        }
    } //// end claass
}
