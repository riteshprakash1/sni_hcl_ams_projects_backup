﻿// -------------------------------------------------------------
// <copyright file="Header.ascx.cs" company="Smith and Nephew">
//     Copyright (c) 2009 Smith and Nephew All rights reserved.
// </copyright>
// -------------------------------------------------------------

namespace NonEmployee
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using NonEmployee.Classes;
    using CSSFriendly;

    /// <summary>
    /// This is the Header UserControl.  
    /// The Header is added to each of the web pages.
    /// </summary>
    public partial class Header : System.Web.UI.UserControl
    {
        /// <summary>
        /// This is the Utility instance 
        /// </summary>
        private Utility myUtility;
        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //// Set The Utility Object
                if (Session["Utility"] != null)
                {
                    this.myUtility = (Utility)Session["Utility"];
                }
                else
                {
                    this.myUtility = new Utility(Request, Session.SessionID);
                    Session["Utility"] = this.myUtility;
                }

                //// if not an admin or reader redirect to Unauthorized Page
                if (!this.myUtility.IsAdmin && !this.myUtility.IsReader)
                {
                    if (Page.Title.ToLower() != "unauthorized")
                    {
                        Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
                    }
                }
                else ////Set the menu items list
                {
                    this.NavMenu.Items.Add(new MenuItem("Search", "Search.aspx"));
                    this.NavMenu.Items.Add(new MenuItem("New Entry", "PersonDetail.aspx?NonID=0"));
                    this.NavMenu.Items.Add(new MenuItem("Reports", "AdminReports.aspx"));

                    if (myUtility.IsAdmin)
                    {
                        this.NavMenu.Items.Add(new MenuItem("Report Designer", "AdminReportDesigner.aspx"));

                        MenuItem adminMenu = new MenuItem("Administration", "AdminUserList.aspx");
                        adminMenu.ChildItems.Add(new MenuItem("Business Area", "AdminBusinessArea.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("Cost Centers", "AdminCostCenters.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("Functions", "AdminFunctions.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("Locations", "AdminLocations.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("Org Codes", "AdminOrgCodes.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("Positions", "AdminPositions.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("Regions", "AdminRegions.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("User List", "AdminUserList.aspx"));
                        //adminMenu.ChildItems.Add(new MenuItem("Product Line", "AdminProductLine.aspx"));
                        this.NavMenu.Items.Add(adminMenu);
                    }
                }

            } //// end isPostBack
        }

        /// <summary>
        /// Called when MenuItem is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.MenuEventArgs"/> instance containing the event data.</param>
        public void OnClick(Object sender, MenuEventArgs e)
        {
            Debug.WriteLine("In OnClick");
            //MessageLabel.Text = "You selected " + e.Item.Text + ".";
            Debug.WriteLine("Value: " + e.Item.Value + "; Text: " + e.Item.Text);
            e.Item.Selected = true;
            string strPage = "~/" + e.Item.Value.Replace("Admin", "Admin/");
            Response.Redirect(strPage, true);
        }


        /// <summary>
        /// Handles the Click event of the btnAppName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void BtnAppName_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx");
        }


    } // end class
}