﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeControl.ascx.cs" Inherits="NonEmployee.WebControls.DateTimeControl" %>

<script language="javascript" type="text/javascript">
function OpenCalendarWindow(url, controlId)
{
    window.open(url + "?ControlId=" + controlId, null, "height=250,width=250,status=no,toolbar=no,menubar=no,location=no;top:800;left:500");
}
</script>

<asp:Table ID="_webControlAspTable" runat="server" Width="150px">
    <asp:TableRow>
        <asp:TableCell><a href="javascript:OpenCalendarWindow('<%=CalendarPagePath%>', 'ctl00$ContentPlaceHolder1$<%=ControlId%>')"><img src="images/calendar.gif" border="0" alt="Calendar" /></a></asp:TableCell>
        <asp:TableCell>
            <asp:TextBox AutoPostBack="false" ID="_dateTextBox" Font-Size="8pt" runat="server"></asp:TextBox></asp:TableCell>
        <asp:TableCell>
            <asp:RegularExpressionValidator ID="vldDate" runat="server" Width="8px" ErrorMessage="Date must be a valid date (mm/dd/yyyy)."
                ControlToValidate="_dateTextBox" ValidationExpression="(1[0-2]|0?[1-9])[\-\/](0?[1-9]|3[01]|1[0-9]|2[0-9])[\-\/]((19|20)\d{2})">*</asp:RegularExpressionValidator></asp:TableCell>
    </asp:TableRow>
</asp:Table>