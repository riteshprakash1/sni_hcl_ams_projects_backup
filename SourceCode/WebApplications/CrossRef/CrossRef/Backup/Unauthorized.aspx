<%@ Page Title="Unauthorized" Language="C#" MasterPageFile="~/Site1.Master" Codebehind="Unauthorized.aspx.cs" AutoEventWireup="True" Inherits="CrossRef.Unauthorized" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
				<table style="width:500px; text-align:center">
					<tr>
						<td>You do not have Administrative privileges and are unauthorized to view this page.</td>
					</tr>
				</table>
</asp:Content>
