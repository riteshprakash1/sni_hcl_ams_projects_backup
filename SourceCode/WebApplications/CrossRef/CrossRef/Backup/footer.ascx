<%@ Control Language="c#" AutoEventWireup="True" Codebehind="footer.ascx.cs" Inherits="CrossRef.footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table style="width:100%; text-align:center; border-collapse: separate; border-spacing: 5px;">
	<tr>
		<td>
			<hr>
		</td>
	</tr>
	<tr>
		<td class="Email">Please click on the
			<asp:HyperLink id="lnkFeedBack" runat="server">email</asp:HyperLink>
			&nbsp;link to send us comments and feedback.
			<br>
		</td>
	</tr>
	<tr>
		<td style="FONT-SIZE: 9pt">Cross reference information in this document contains 
			facts of normal customer interest and is for reference purposes 
			only.&nbsp;&nbsp;It has been obtained from public sources to foster competition 
			of orthopaedic products.&nbsp;&nbsp;The cross referenced product may or may not 
			perform, function or be suitable for the same or similar 
			indication(s).&nbsp;&nbsp;Nothing contained herein suggests that any Smith 
			&amp; Nephew or competitors products and/or product features are identical or 
			equivalent.&nbsp;&nbsp; Smith &amp; Nephew has made a good faith effort to 
			compile this information and makes no representations or warranties regarding 
			any information obtained from other sources.&nbsp;&nbsp;Any pricing information 
			provided by Smith &amp; Nephew is limited to Smith &amp; Nephew 
			products.&nbsp;&nbsp;The customer must make the final determination whether any 
			product, shown herein, meets its needs.<br>
			<br>
			Copyright 2016 Smith &amp; Nephew, Inc.
		</td>
	</tr>
</table>
