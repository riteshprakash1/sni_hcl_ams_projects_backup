<%@ Page Title="User List" Language="C#" MasterPageFile="~/Site1.Master" Codebehind="UserList.aspx.cs" AutoEventWireup="True" Inherits="CrossRef.admin.UserList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="SearchPage">
        <table>
            <tr>
                <td style="font-size: large;text-decoration: underline;"><strong>Admininstrator User List</strong></td>
            </tr>           
            <tr>
                <td>
                <asp:GridView ID="gvUser" runat="server" Width="950px" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no users." OnRowDeleting="GV_Delete"  ShowFooter="true" 
                    CellPadding="2" DataKeyNames="userID, userName" GridLines="Both" OnRowCancelingEdit="GV_Add">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                          <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
								    ></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 

                       <asp:TemplateField HeaderStyle-Width="15%" HeaderText="UserName" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" Font-Size="8pt" runat="server"  Text='<%# Eval("username")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddUserName" Width="150px" MaxLength="30" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredAddUserName" ValidationGroup="Add" ControlToValidate="txtAddUserName" runat="server"  ErrorMessage="User Name is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldUserExists" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name already exists.">*</asp:CustomValidator>
                                <asp:CustomValidator ID="vldAddUserInAD" runat="server" ValidationGroup="Add" ControlToValidate="txtAddUserName" ErrorMessage="The User Name is not valid.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                        <asp:BoundField DataField="firstname" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="First Name" />                       
                        <asp:BoundField DataField="lastname" HeaderStyle-Width="15%" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" HeaderText="Last Name" />                       
                        <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Role" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRole" Font-Size="8pt" runat="server"  Text='<%# Eval("roleName")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlRole" Font-Size="8pt" runat="server">
                                    <asp:ListItem>Admin</asp:ListItem>
                                    <asp:ListItem>Contributor</asp:ListItem>
                                </asp:DropDownList>
                            </FooterTemplate>
                        </asp:TemplateField> 
                   </Columns>
                </asp:GridView> 
                
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vldSummary1"  ValidationGroup="FindUser" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                    <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
                </td>
            </tr>

       </table>    
    </div></asp:Content>
