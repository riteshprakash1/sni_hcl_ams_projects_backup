﻿// ------------------------------------------------------------------
// <copyright file="CompetitorEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrossRef.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Competitor Entity. </summary>    
    public class CompetitorEntity
    {
        /// <summary>This is the CompetitorID.</summary>
        private int intCompetitorID;

        /// <summary>This is the Competitor Name.</summary>
        private string strCompetitorName;

        /// <summary>This is the InCrossRef.</summary>
        private string strInCrossRef;

        /// <summary>Initializes a new instance of the CompetitorEntity class.</summary>
        public CompetitorEntity()
        {
            this.CompetitorName = String.Empty;
            this.InCrossRef = String.Empty;
        }

        /// <summary>Gets or sets the Competitor ID.</summary>
        /// <value>The Competitor ID.</value>
        public int CompetitorID
        {
            get { return this.intCompetitorID; }
            set { this.intCompetitorID = value; }
        }

        /// <summary>Gets or sets the CompetitorName.</summary>
        /// <value>The CompetitorName.</value>
        public string CompetitorName
        {
            get { return this.strCompetitorName; }
            set { this.strCompetitorName = value; }
        }

        /// <summary>Gets or sets the InCrossRef.</summary>
        /// <value>The InCrossRef.</value>
        public string InCrossRef
        {
            get { return this.strInCrossRef; }
            set { this.strInCrossRef = value; }
        }

        
        /// <summary>Gets all the CompetitorEntity Collection.</summary>
        /// <returns>The newly populated CompetitorEntity List</returns>
        public static List<CompetitorEntity> GetCompetitors()
        {
            List<CompetitorEntity> competitors = new List<CompetitorEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = Utility.SqlExecuteQuery("sp_GetCompetitors", parameters);

            foreach (DataRow r in dt.Rows)
            {
                CompetitorEntity myCompetitor = CompetitorEntity.GetEntityFromDataRow(r);
                competitors.Add(myCompetitor);
            }

            return competitors;
        }

        /// <summary>Gets all the CompetitorEntity Collection.</summary>
        /// <param name="strCompetitorName">The strCompetitor Name.</param>
        /// <returns>The newly populated CompetitorEntity List</returns>
        public static List<CompetitorEntity> GetCompetitorByName(string strCompetitorName)
        {
            List<CompetitorEntity> competitors = new List<CompetitorEntity>();
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@Competitorname", strCompetitorName));

            DataTable dt = Utility.SqlExecuteQuery("sp_GetCompetitorByName", parameters);

            foreach (DataRow r in dt.Rows)
            {
                CompetitorEntity myCompetitor = CompetitorEntity.GetEntityFromDataRow(r);
                competitors.Add(myCompetitor);
            }

            return competitors;
        }


        /// <summary>Receives a Competitor datarow and converts it to a AppUsersEntity.</summary>
        /// <param name="r">The Competitor DataRow.</param>
        /// <returns>CompetitorEntity</returns>
        public static CompetitorEntity GetEntityFromDataRow(DataRow r)
        {
            CompetitorEntity Competitor = new CompetitorEntity();

            if ((r["CompetitorID"] != null) && (r["CompetitorID"] != DBNull.Value))
                Competitor.CompetitorID = Convert.ToInt32(r["CompetitorID"].ToString());

            if (r.Table.Columns.Contains("InCrossRef"))
                Competitor.InCrossRef = r["InCrossRef"].ToString();

            Competitor.CompetitorName = r["CompetitorName"].ToString();

            return Competitor;
        }

        /// <summary>Inserts The Competitor into the database</summary>
        /// <param name="u">CompetitorEntity object</param>
        /// <returns>The Competitor Entity</returns>
        public static CompetitorEntity InsertCompetitor(CompetitorEntity c)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            SqlParameter paramID = new SqlParameter("@competitorID", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            myParameters.Add(paramID);

            myParameters.Add(new SqlParameter("@Competitorname", c.CompetitorName));

            myParameters = Utility.SqlExecuteNonQueryReturnParameters("sp_InsertCompetitor", myParameters);
            c.CompetitorID = Convert.ToInt32(paramID.Value.ToString());

            return c;
        }

        /// <summary>Delete the Competitor record in the database</summary>
        /// <param name="intCompetitorID">the Competitor ID</param>
        /// <returns>records updated count</returns>
        public static int DeleteCompetitor(int intCompetitorID)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@CompetitorID", intCompetitorID));

            int intRecordsUpdated = Utility.SqlExecuteNonQueryCount("sp_DeleteCompetitor", myParameters);

            return intRecordsUpdated;
        }


    } //// end class
}