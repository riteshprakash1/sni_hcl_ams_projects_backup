﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="CrossRef.ErrorPage" MasterPageFile="~/Site1.Master" Title="Error Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table class="center70" style="text-align:center">
            <tr style="height:200px; vertical-align:middle">
                <td style="text-align:center">
                    <asp:Label ID="lblErrorHeading" ForeColor="#FF7300" Font-Size="14pt" runat="server" Text="Application Error"></asp:Label>
                     <br /><br />
                    <asp:Label ID="lblMsg" Font-Size="11pt" runat="server" Text="An application error occurred.  
                    An email has been sent to the application developer.  
                    You may be contacted by the application developer to troubleshoot the issue.  
                    Click the link to be redirected to the " ></asp:Label><asp:HyperLink ID="lnkSearchPage" NavigateUrl="~/Search.aspx" runat="server">Search Page</asp:HyperLink>
                    <br /><br />
                    <asp:Label ID="lblErrorMsg" Font-Size="11pt" runat="server" Text="Application Error"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
