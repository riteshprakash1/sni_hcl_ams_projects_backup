<%@ Page Title="Search" Language="C#" MasterPageFile="~/Site1.Master" Codebehind="Search.aspx.cs" AutoEventWireup="True" Inherits="CrossRef.Search" %>
<%@ Register src="footer.ascx" tagname="footer" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
               <div id="#SearchBox">
					<table style=" width:1190px; text-align:center; BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid">
						<tr>
							<td style="FONT-WEIGHT: bold; FONT-SIZE: 12pt" colspan="2">Search Parameters</td>
							<td style="TEXT-ALIGN: right" colspan="2"><asp:HyperLink id="lnkHelp" runat="server" NavigateUrl="CrossRefTutorial.pdf" Target="_blank">Tutorial</asp:HyperLink></td>
						</tr>
                        <tr>
                            <td style="width:50%; vertical-align:top"">
                                <table style="width:100%; vertical-align:top"">
						            <tr>
							            <td style="vertical-align:top">Competitor:</td>
							            <td style="vertical-align:top"><asp:dropdownlist id="ddlCompetitor" tabIndex="5" runat="server" Width="250px" Font-Size="10pt"></asp:dropdownlist></td>
                                    </tr>
                                    <tr>
							            <td style="vertical-align:top; width:20%">S&amp;N Reference<br/>
								            Number or List: <a class="hintanchor" onmouseover="showhint('Copy Multiple Reference Numbers from a single column of a spread sheet or Type individual reference number entries, hitting the return key after each entry.', this, event, '200px', 0, 30)"
									            href="#">[?]</a></td>
							            <td style="vertical-align:top; width:30%"><asp:textbox id="txtMaterialID" tabIndex="1" runat="server" Width="200px" Font-Size="10pt" Rows="3"
									            TextMode="MultiLine"></asp:textbox></td>
                                    </tr>
                                    <tr>
							            <td style="vertical-align:top">Competitor Reference<br/>
								            Number or List: <a class="hintanchor" onmouseover="showhint('Copy Multiple Reference Numbers from a single column of a spread sheet or Type individual reference number entries, hitting the return key after each entry.', this, event, '200px', 0, 30)"
									            href="#">[?]</a>
							            </td>
							            <td style="vertical-align:top"><asp:textbox id="txtMaterialList" tabIndex="8" runat="server" Width="200px" Font-Size="10pt"
									            Rows="3" TextMode="MultiLine"></asp:textbox></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50%; vertical-align:top">
                                <table style="width:100%; vertical-align:top"">
                                    <tr>
							            <td style="vertical-align:top">Level 2:</td>
							            <td style="vertical-align:top"><asp:dropdownlist id="ddlLevel2" tabIndex="6" runat="server" Width="250px" Font-Size="10pt"
                                                   onselectedindexchanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist></td>						
                                    </tr>
                                    <tr>
							            <td style="vertical-align:top">Level 3:</td>
							            <td style="vertical-align:top">
                                                   <asp:dropdownlist id="ddlLevel3" tabIndex="6" runat="server" Width="250px" Font-Size="10pt"
                                                   onselectedindexchanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist>
                                        </td>
                                    </tr>
                                    <tr>
							            <td style="vertical-align:top; width:17%">Level 4:</td>
							            <td style="vertical-align:top"><asp:dropdownlist id="ddlLevel4" tabIndex="7" runat="server" Width="250px" Font-Size="10pt" 
                                                onselectedindexchanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist>                                               
                                        </td>
                                    </tr>
                                    <tr>
 							            <td style="vertical-align:top">Level 5:</td>
                                        <td style="vertical-align:top"><asp:dropdownlist id="ddlLevel5" tabIndex="9" runat="server" Width="250px" Font-Size="10pt"
                                                   onselectedindexchanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist></td>
                                    </tr>
                                </table>
                            </td>
                         </tr>
						<tr>
							<td style="TEXT-ALIGN: center" colspan="4"><asp:button id="btnSearch" 
                                    runat="server" Width="100px" Font-Size="10pt" BorderColor="#CCCCCC"
									CausesValidation="false" BackColor="White" Text="Search" Height="22" ForeColor="#FF7300" 
                                    onclick="btnSearch_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" Width="100px" Font-Size="10pt" BorderColor="#CCCCCC"
									CausesValidation="False" BackColor="White" Text="Reset" Height="22" ForeColor="#FF7300" 
                                    onclick="btnReset_Click"></asp:button></td>
						</tr>
					</table>
				</div>
				<div id="#SearchGrid">
					<table style="width:1190px; text-align:center">
						<tr>
							<td style="vertical-align:top; width:30%">
                                <asp:imagebutton id="btnFirst" ImageAlign="Bottom" runat="server" ImageUrl="images/first.gif" onclick="btnFirst_Click"></asp:imagebutton>
                                <asp:imagebutton id="btnPrev" ImageAlign="Bottom" runat="server" ImageUrl="images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
								<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist>
                                <asp:label id="lblPageCnt" runat="server">Label</asp:label>&nbsp;&nbsp;
                                <asp:imagebutton id="btnNext" ImageAlign="Bottom"  runat="server" ImageUrl="images/next.gif" onclick="btnNext_Click"></asp:imagebutton>
								<asp:imagebutton id="btnLast" ImageAlign="Bottom" runat="server" ImageUrl="images/last.gif" onclick="btnLast_Click"></asp:imagebutton>
                            </td>
							<td style="width:26%"><asp:label id="lblRecordsPerPage" runat="server">Records per page:</asp:label>&nbsp;<asp:dropdownlist 
                                    id="ddlRecords" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="ddlRecords_SelectedIndexChanged">
									<asp:ListItem Value="10">10</asp:ListItem>
									<asp:ListItem Value="25">25</asp:ListItem>
									<asp:ListItem Value="50">50</asp:ListItem>
									<asp:ListItem Value="100">100</asp:ListItem>
								</asp:dropdownlist>
							</td>
							<td style="width:24%"><asp:button id="btnExport" runat="server" Width="80px" 
                                    Font-Size="10pt" BorderColor="#CCCCCC"
									CausesValidation="False" BackColor="White" Text="Export" Height="22" ForeColor="#FF7300" 
                                    onclick="btnExport_Click"></asp:button>&nbsp;
								<asp:dropdownlist id="ddlExport" runat="server">
									<asp:ListItem Value="pdf">PDF</asp:ListItem>
									<asp:ListItem Value="excel">EXCEL</asp:ListItem>
								</asp:dropdownlist><asp:label id="lblNoRecords" runat="server" Font-Size="10pt" ForeColor="#FF7300">No matching records were found.</asp:label></td>
							<td style="TEXT-ALIGN: right; width:20%"><asp:label id="lblRecordCount" runat="server"></asp:label></td>
						</tr>
						<tr>
							<td colspan="4"><asp:gridview id="gvSearch" runat="server" Width="100%" AutoGenerateColumns="false" PagerSettings-Visible="false"
									AllowPaging="True" OnSorting="sortHandler" AllowSorting="True" CellPadding="5"  OnRowDeleting="GV_Delete" ShowFooter="true"
                                    OnRowDataBound="GV_RowDataBound" OnRowEditing="GV_Edit" OnRowCancelingEdit="GV_Add" OnRowUpdating="GV_Update"
                                    DataKeyNames="CrossMaterialsID, MaterialID, Competitor, CompetitorID, CompetitorDesc" >
									<HeaderStyle CssClass="SearchGrid" Font-Size="9pt" Font-Bold="false" BackColor="#CCCCCC"></HeaderStyle>
                                    <FooterStyle VerticalAlign="Top" />
									<Columns>
                                          <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button id="btnEdit" Width="50px" Height="18" runat="server" Text="Edit" CommandArgument="Edit" CommandName="Edit"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Button id="btnUpdate" Width="50px" Height="18" runat="server"  Text="Update" CommandName="Update"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White" ></asp:Button>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button id="btnAdd" Width="50px" Height="18" runat="server" Text="Add" CommandName="Cancel"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"></asp:Button>                                
							                </FooterTemplate>
                                        </asp:TemplateField> 
                                       <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Font-Size="9pt" HeaderText="S&N Material ID" HeaderStyle-HorizontalAlign="Left" SortExpression="MaterialID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMaterialID" Font-Size="8pt" runat="server" Text='<%# Eval("MaterialID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                 <asp:TextBox ID="txtMaterialID" MaxLength="18" Width="75px" Text='<%# Eval("MaterialID")%>' Font-Size="8pt" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldMaterialID" ValidationGroup="UpdateCR" ControlToValidate="txtMaterialID"  runat="server" ErrorMessage="Material ID is required">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="vldValidMaterialID" ValidationGroup="UpdateCR" runat="server" ErrorMessage="Invalid S&N Material ID.">*</asp:CustomValidator>
                                           </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtAddMaterialID" ValidationGroup="AddCR" Width="75px"  MaxLength="18" Font-Size="8pt" runat="server"></asp:TextBox>
                                                <asp:CustomValidator ID="vldAddValidMaterialID" ValidationGroup="AddCR" runat="server" ErrorMessage="Invalid S&N Material ID.">*</asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="vldAddMaterialID" ValidationGroup="AddCR" ControlToValidate="txtAddMaterialID"  runat="server" ErrorMessage="Material ID is required">*</asp:RequiredFieldValidator>
                                           </FooterTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderStyle-Width="15%" ItemStyle-Font-Size="9pt" HeaderText="S&N Material Desc" HeaderStyle-HorizontalAlign="Left" SortExpression="Material_Desc">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMaterialDesc" Font-Size="8pt" runat="server" Text='<%# Eval("Material_Desc")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Font-Size="9pt" HeaderText="Competitor ID" HeaderStyle-HorizontalAlign="Left" SortExpression="CompetitorID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompetitorID" Font-Size="8pt" runat="server" Text='<%# Eval("CompetitorID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                 <asp:TextBox ID="txtCompetitorID" MaxLength="50" Width="75px" Text='<%# Eval("CompetitorID")%>' Font-Size="8pt" runat="server"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="vldCompetitorID" ValidationGroup="UpdateCR" ControlToValidate="txtCompetitorID"  runat="server" ErrorMessage="Competitor ID is required">*</asp:RequiredFieldValidator>
                                           </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtAddCompetitorID" MaxLength="50" Width="75px" Font-Size="8pt" runat="server"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="vldAddCompetitorID" ValidationGroup="AddCR" ControlToValidate="txtAddCompetitorID"  runat="server" ErrorMessage="Competitor ID is required">*</asp:RequiredFieldValidator>
                                           </FooterTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Font-Size="9pt" HeaderText="Competitor Product Desc" HeaderStyle-HorizontalAlign="Left" SortExpression="CompetitorDesc">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompetitorDesc" Font-Size="8pt" runat="server" Text='<%# Eval("CompetitorDesc")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                 <asp:TextBox ID="txtCompetitorDesc" MaxLength="750" Text='<%# Eval("CompetitorDesc")%>' Font-Size="8pt" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldCompetitorDesc" ValidationGroup="UpdateCR" ControlToValidate="txtCompetitorDesc"  runat="server" ErrorMessage="Competitor Product Desc is required">*</asp:RequiredFieldValidator>
                                           </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtAddCompetitorDesc" MaxLength="750"  Font-Size="8pt" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vldAddCompetitorDesc" ValidationGroup="AddCR" ControlToValidate="txtAddCompetitorDesc"  runat="server" ErrorMessage="Competitor Product Desc is required">*</asp:RequiredFieldValidator>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Font-Size="9pt" HeaderText="Competitor Company" HeaderStyle-HorizontalAlign="Left" SortExpression="Competitor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompetitor" Font-Size="8pt" runat="server" Text='<%# Eval("Competitor")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlCompetitor" DataTextField="Text" Font-Size="7pt" Width="125px" DataValueField="Value" SelectedValue='<%# Bind("Competitor") %>' DataSource='<%# GetCompetitors()%>' runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="vldRequiredCompetitor" ValidationGroup="UpdateCR" ControlToValidate="ddlCompetitor" ForeColor="#FF7300" runat="server" ErrorMessage="Competitor is required.">*</asp:RequiredFieldValidator>
                                           </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlAddCompetitor" DataTextField="Text" Font-Size="7pt" Width="125px" DataValueField="Value" DataSource='<%# GetCompetitors()%>' runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="vldAddRequiredCompetitor" ValidationGroup="AddCR" ControlToValidate="ddlAddCompetitor"  runat="server" ErrorMessage="Competitor is required">*</asp:RequiredFieldValidator>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Font-Size="9pt" HeaderText="Notes" HeaderStyle-HorizontalAlign="Left" SortExpression="Notes">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNotes" Font-Size="8pt" runat="server" Text='<%# Eval("Notes")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtNotes" MaxLength="250" Text='<%# Eval("Notes")%>' Font-Size="8pt" runat="server"></asp:TextBox>
                                           </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtAddNotes" MaxLength="250" Font-Size="8pt" runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
										<asp:BoundField DataField="Level_2_Desc" ItemStyle-Font-Size="9pt" HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Level_2_Desc" HeaderText="Level 2" ReadOnly="true"></asp:BoundField>
										<asp:BoundField DataField="Level_3_Desc" ItemStyle-Font-Size="9pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Level_3_Desc" HeaderText="Level 3" ReadOnly="true"></asp:BoundField>
										<asp:BoundField DataField="Level_4_Desc" ItemStyle-Font-Size="9pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Level_4_Desc" HeaderText="Level 4" ReadOnly="true"></asp:BoundField>
										<asp:BoundField DataField="Level_5_Desc" ItemStyle-Font-Size="9pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Level_5_Desc" HeaderText="Level 5" ReadOnly="true"></asp:BoundField>
                                        <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button id="btnDelete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
									                Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
										            ></asp:Button>
                                           </ItemTemplate>
                                        </asp:TemplateField> 
									</Columns>
								</asp:gridview></td>
							<asp:Literal id="Literal1" runat="server"></asp:Literal>
						</tr>
                        <tr>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="AddCR" runat="server" />
                                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="UpdateCR" runat="server" />
                            </td>
                            
                        </tr>
					    <uc1:footer ID="footer1" runat="server" />
					</table>
				</div>
</asp:Content>
