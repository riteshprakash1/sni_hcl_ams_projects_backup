﻿<%@ Page Title="Competitors" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Competitors.aspx.cs" Inherits="CrossRef.admin.Competitors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="SearchPage">
    <table align="left">
        <tr>
            <td style="font-size: large;text-decoration: underline;"><strong>Competitor List</strong></td>
        </tr>           
        <tr>
            <td style="text-align:left">
                <asp:GridView ID="gvCompetitor" runat="server" Width="400px" AutoGenerateColumns="False"  OnRowDataBound="GV_RowDataBound"
                    EmptyDataText="There are no Competitors." OnRowDeleting="GV_Delete"  ShowFooter="true" 
                    CellPadding="2" DataKeyNames="CompetitorID, InCrossRef" GridLines="Both" OnRowCancelingEdit="GV_Add">
                    <RowStyle HorizontalAlign="left" />
                    <HeaderStyle BackColor="#dcdcdc" HorizontalAlign="left" />
                    <Columns>
                            <asp:TemplateField HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button id="btnDelete" ValidationGroup="Delete" Width="50px" Height="18" runat="server" Text="Delete" CommandName="Delete"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="false" BorderColor="White"
							        ></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnAdd" ValidationGroup="Add" Width="50px" Height="18" runat="server" Text="Add"  CommandName="Cancel"
							        Font-Size="8pt" ForeColor="#FF7300" BackColor="White"  CausesValidation="false" BorderColor="White"></asp:Button>                                
					        </FooterTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Competitor Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCompetitorName" Font-Size="8pt" runat="server"  Text='<%# Eval("competitorname")%>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddCompetitorName" Width="150px" MaxLength="30" Font-Size="8pt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vldRequiredAddCompetitorName" ValidationGroup="Add" ControlToValidate="txtAddCompetitorName" runat="server"  ErrorMessage="Competitor Name is required.">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="vldCompetitorExists" runat="server" ValidationGroup="Add" ControlToValidate="txtAddCompetitorName" ErrorMessage="The Competitor Name already exists.">*</asp:CustomValidator>
                            </FooterTemplate>
                        </asp:TemplateField>                            
                    </Columns>
                </asp:GridView> 
                
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="vldSummary2" ValidationGroup="Add" Font-Size="10pt" ForeColor="#FF7300" runat="server" />
            </td>
        </tr>

    </table>    
</div>
</asp:Content>
