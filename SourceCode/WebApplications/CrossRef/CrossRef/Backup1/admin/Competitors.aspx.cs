﻿// --------------------------------------------------------------
// <copyright file="Competitors.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------
namespace CrossRef.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrossRef.Classes;

    /// <summary>This is the Competitors page</summary>
    public partial class Competitors : System.Web.UI.Page
    {
        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            AppUsersEntity myUser = AppUsersEntity.GetAppUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

            //// if not an admin redirect to Unauthorized Page
            if (!AppUsersEntity.IsAdmin(myUser))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
                this.BindDT();
            }

        } //// end Page_Load

        /// <summary>Binds the Competitor table to the gvCompetitor GridView.</summary>
        protected void BindDT()
        {
            List<CompetitorEntity> Competitors = CompetitorEntity.GetCompetitors();

            if (Competitors.Count == 0)
                Competitors.Add(new CompetitorEntity());

            this.gvCompetitor.DataSource = Competitors;
            this.gvCompetitor.DataBind();
        }

        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtAddCompetitorName = (TextBox)this.gvCompetitor.FooterRow.FindControl("txtAddCompetitorName");
            CustomValidator vldCompetitorExists = (CustomValidator)this.gvCompetitor.FooterRow.FindControl("vldCompetitorExists");

            List<CompetitorEntity> myCompetitors = CompetitorEntity.GetCompetitorByName(txtAddCompetitorName.Text);
            vldCompetitorExists.IsValid = Convert.ToBoolean(myCompetitors.Count == 0);

            if (Page.IsValid)
            {
                CompetitorEntity newCompetitor = new CompetitorEntity();
                newCompetitor.CompetitorName = txtAddCompetitorName.Text;

                CompetitorEntity.InsertCompetitor(newCompetitor);

                this.gvCompetitor.EditIndex = -1;
                this.BindDT();
            } //// Page.IsValid

        }  ////end GV_Add

        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string strInCrossRef = this.gvCompetitor.DataKeys[e.Row.RowIndex]["InCrossRef"].ToString();
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('Competitor');");

                bool showDeleteButton = Convert.ToBoolean(strInCrossRef.ToLower() == "no");
                btn.Visible = showDeleteButton;

                Label lblCompetitorName = (Label)e.Row.FindControl("lblCompetitorName");
                if (lblCompetitorName.Text == string.Empty)
                    e.Row.Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddCompetitorName = (TextBox)e.Row.FindControl("txtAddCompetitorName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddCompetitorName, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            int intCompetitorID = Convert.ToInt32(this.gvCompetitor.DataKeys[e.RowIndex]["CompetitorID"].ToString());
            CompetitorEntity.DeleteCompetitor(intCompetitorID);

            this.gvCompetitor.EditIndex = -1;
            this.BindDT();
        }

    }
}