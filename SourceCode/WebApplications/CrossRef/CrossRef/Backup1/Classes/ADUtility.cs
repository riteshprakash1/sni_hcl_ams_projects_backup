﻿// ------------------------------------------------------------------
// <copyright file="ADUtility.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrossRef.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.DirectoryServices;
    using System.Configuration;
    using System.Web.Security;
    using CrossRef.Classes;

    /// <summary>
    /// This is the Active Directory Utility.
    /// </summary>
    public class ADUtility
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ADUtility"/> class.
        /// </summary>
        private ADUtility()
        {
        }

        /// <summary>
        /// Gets the ADUserEntity By the Username.
        /// </summary>
        /// <param name="strUserName">UserName</param>
        /// <returns></returns>
        public static ADUserEntity GetADUserByUserName(string strUserName)
        {
            DirectorySearcher myDirectorySearch = new DirectorySearcher();
            myDirectorySearch.SearchRoot = ADUtility.GetDirectoryEntry();
            myDirectorySearch.SearchScope = SearchScope.Subtree;
            myDirectorySearch.Filter = ADUtility.GetUserNameFilter(strUserName);

            ADUserEntity myUser = null;
            SearchResult result = myDirectorySearch.FindOne();
            if (result != null)
            {
                DirectoryEntry myDirectoryEntry = result.GetDirectoryEntry();
                System.DirectoryServices.PropertyCollection myCollection = myDirectoryEntry.Properties;
            
                myUser = ADUserEntity.SetUserFromPropertyCollection(myCollection);
            }

            return myUser;
        }


        /// <summary>
        /// Gets list of ADUser with the corresponding the last name.
        /// </summary>
        /// <param name="strLastName">Last name of the STR.</param>
        /// <returns>ADUserEntity DataTable</returns>
        public static DataTable GetADUsersByLastName(string strLastName)
        {
            DirectorySearcher myDirectorySearch = new DirectorySearcher();
            myDirectorySearch.SearchRoot = ADUtility.GetDirectoryEntry();
            myDirectorySearch.SearchScope = SearchScope.Subtree;
            myDirectorySearch.Filter = ADUtility.GetLastNameFilter(strLastName);

            Debug.WriteLine("Filter: " + myDirectorySearch.Filter);
            Debug.WriteLine("Path: " + myDirectorySearch.SearchRoot.Path);
            ADUserEntity myUser = null;
            SearchResultCollection results = myDirectorySearch.FindAll();

            DataTable dtUser = ADUserEntity.GetADUserEntityTable();
            foreach (SearchResult result in results)
            {
                DirectoryEntry myDirectoryEntry = result.GetDirectoryEntry();
                System.DirectoryServices.PropertyCollection myCollection = myDirectoryEntry.Properties;

                myUser = ADUserEntity.SetUserFromPropertyCollection(myCollection);
                Debug.WriteLine("UserName: " + myUser.UserName + "; DistinguishedName: " + myUser.DistinguishedName);
                DataRow rUser = ADUserEntity.SetUserDataRow(myUser, dtUser.NewRow());
                dtUser.Rows.Add(rUser);
            }

            dtUser = ADUserEntity.SortUserTable(dtUser, "LastName asc,FirstName asc");
            return dtUser;
        }

        /// <summary>
        /// Gets the directory entry.
        /// </summary>
        /// <returns></returns>
        private static DirectoryEntry GetDirectoryEntry()
        {
            //// This is the active directory location.
            string strADLocation = ConfigurationManager.AppSettings["AD"];

            //// This is the active directory user name.
            string strADUser = ConfigurationManager.AppSettings["ADUser"];

            //// This is the active directory password.
            string strADPassword = ConfigurationManager.AppSettings["ADPassword"];

            return new DirectoryEntry(strADLocation, strADUser, strADPassword);
        }

        /// <summary>
        /// Gets the username of the logon user.
        /// </summary>
        /// <param name="req">The HttpRequest.</param>
        /// <returns>the username string</returns>
        public static string GetUserNameOfAppUser(System.Web.HttpRequest req)
        {
            string strUser = req.LogonUserIdentity.Name;
            string[] serperator = new string[1] { "\\" };

            if (strUser.Contains("\\"))
                strUser = strUser.Split(serperator, StringSplitOptions.None)[1].ToString();

            return strUser;
        }

        /// <summary>
        /// Gets the user name filter.
        /// </summary>
        /// <param name="strUser">The username.</param>
        /// <returns></returns>
        private static string GetUserNameFilter(string strUser)
        {
            return "(|(&(objectClass=user)(SAMAccountName=" + strUser + ")))";
        }

        /// <summary>
        /// Gets the last name filter.
        /// </summary>
        /// <param name="strLastName">The last name.</param>
        /// <returns></returns>
        private static string GetLastNameFilter(string strLastName)
        {
            return "(|(&(objectClass=user)(sn=" + strLastName + ")))";
        }

    } //// end class
}
