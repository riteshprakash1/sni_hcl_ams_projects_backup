using System;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Collections;

namespace CrossRef.Classes
{
	/// <summary>
	/// Summary description for Utility.
	/// </summary>
	public class Utility
	{
		protected SqlConnection sqlConnection1;
		protected DataTable dtUsers;
		protected DataTable dtRoles;
		protected DataTable dtSearch;
		protected DataTable dtFranchise;
		protected DataTable dtCompetitor;
		protected DataTable dtCategory;
		protected DataTable dtGbu;
        protected DataTable dtProductHierarchy;
        protected string strSiteURL;
		protected string strHost;
		protected int intCrossReferenceID;
		protected string strADAccount;
		protected bool blnIsAdmin;
        protected AppUsersEntity myAppUser;
        protected bool blnIsContributor;

		public Utility(System.Web.HttpRequest req)
		{
            this.SiteURL = ConfigurationManager.AppSettings["siteURL"].ToString();
            string strConn = ConfigurationManager.ConnectionStrings["sqlConn"].ToString();
			this.SqlConn = new SqlConnection();
			SqlConn.ConnectionString = strConn;

			this.CrossReferenceID = 0;
			this.SetServerVariables(req);
			this.SetSiteUrl(req);

            AppUser = AppUsersEntity.GetAppUserByUserName(ADUtility.GetUserNameOfAppUser(req));

            this.SqlConn.Open();
			this.LoadDataTables();
            this.SetUserRole(AppUser);
			this.SqlConn.Close();
		
		}

        public bool IsContributor
        {
            get { return blnIsContributor; }
            set { blnIsContributor = value; }
        }

        public bool IsAdmin
        {
            get { return blnIsAdmin; }
            set { blnIsAdmin = value; }
        }

        public AppUsersEntity AppUser
        {
            get { return myAppUser; }
            set { myAppUser = value; }
        }

        public string SiteURL
		{
			get { return strSiteURL; }
			set { strSiteURL = value; }
		}

		public string Host
		{
			get { return strHost; }
			set { strHost = value; }
		}

		public string ADAccount
		{
			get {return strADAccount;}
			set {strADAccount = value;}
		}

		public int CrossReferenceID
		{
			get {return intCrossReferenceID;}
			set {intCrossReferenceID = value;}
		}


		public DataTable TblSearch
		{
			get {return dtSearch;}
			set {dtSearch = value;}
		}		
		
		public DataTable TblGbu
		{
			get {return dtGbu;}
			set {dtGbu = value;}
		}		

		public DataTable TblCategory
		{
			get {return dtCategory;}
			set {dtCategory = value;}
		}		
		
		public DataTable TblCompetitor
		{
			get {return dtCompetitor;}
			set {dtCompetitor = value;}
		}
		
		public DataTable TblUsers
		{
			get {return dtUsers;}
			set {dtUsers = value;}
		}
		
		public DataTable TblRoles
		{
			get {return dtRoles;}
			set {dtRoles = value;}
		}

		public DataTable TblFranchise
		{
			get {return dtFranchise;}
			set {dtFranchise = value;}
		}

        public DataTable TblProductHierarchy
        {
            get { return dtProductHierarchy; }
            set { dtProductHierarchy = value; }
        }

        public SqlConnection SqlConn
		{
			get {return sqlConnection1;}
			set {sqlConnection1 = value;}
		}

		private void LoadDataTables()
		{
            this.TblCompetitor = this.SetTable("sp_GetCompetitors");
            //this.TblRoles = this.SetTable("sp_cr_GetRoles");
            this.TblUsers = this.SetTable("sp_GetAppUsers");
            this.TblProductHierarchy = this.SetTable("sp_GetProductHierarchy");
        } // end LoadDataTables

        private void SetUserRole(AppUsersEntity user)
        {
            this.IsContributor = AppUsersEntity.IsContributor(user);
            this.IsAdmin = AppUsersEntity.IsAdmin(user);
        }

        private void SetSiteUrl(System.Web.HttpRequest req)
		{
			Debug.WriteLine("Host: " + req.Url.Host + "; IsSecure: " + req.IsSecureConnection.ToString());
			this.Host = req.Url.Host;
			string strPort = "";
			if (req.Url.Port != 80 && !req.IsSecureConnection)
				strPort = ":" + req.Url.Port.ToString();
			else
			{
				if (req.Url.Port != 443 && req.IsSecureConnection)
					strPort = ":" + req.Url.Port.ToString();
			}

			this.SiteURL = "Http://" + req.Url.Host + strPort + "/";
			if (req.IsSecureConnection)
				this.SiteURL = "Https://" + req.Url.Host + strPort + "/";

			Debug.WriteLine("strSiteUrl: " + this.SiteURL);
		}

		private void SetServerVariables(System.Web.HttpRequest req)
		{
			//strServer = req.ServerVariables["SERVER_NAME"];
			string authUser = req.ServerVariables["AUTH_USER"];
			string[] arrayAuth = authUser.Split('\\');

			if(arrayAuth.Length>1)
				this.ADAccount = arrayAuth[1];
			else
				this.ADAccount = String.Empty;

			Debug.WriteLine("ADAccount: " + this.ADAccount);
		}


		public DataTable SetTable(string sp, int ID)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@ID", ID));

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
			da.Fill(dt);
	
			return dt;
		} // END SetTable()


		public DataTable SetTable(string sp, string str1)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@str1", str1));

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
			da.Fill(dt);
	
			return dt;
		} // END SetTable()

		public DataTable SetTable(string sp, string str1, string str2, string str3)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;
			cmdSetTable.Parameters.Add(new SqlParameter("@str1", str1));
			cmdSetTable.Parameters.Add(new SqlParameter("@str2", str2));
			cmdSetTable.Parameters.Add(new SqlParameter("@str3", str3));

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
			da.Fill(dt);
	
			return dt;
		} // END SetTable()

		public DataTable SetTable(string sp)
		{
			SqlCommand cmdSetTable = new SqlCommand();
			cmdSetTable.CommandText = sp;
			cmdSetTable.CommandType = CommandType.StoredProcedure;
			cmdSetTable.Connection = this.SqlConn;

			DataTable dt = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(cmdSetTable);
			da.Fill(dt);
	
			return dt;
		} // END SetTable()

		public string SetWildCardList(ArrayList lst, string strField, string strDelimit)
		{
			string strIn="";
			foreach(object myItem in lst)
			{
				string sItem = myItem.ToString().Trim();
				string target = "*";
				char[] anyOf = target.ToCharArray();
				int intFind = sItem.IndexOfAny(anyOf); // -1 if none found

				if(intFind != -1)
				{
					if(strIn == "")
						strIn += strDelimit + strField + " LIKE '" + sItem.Replace("*", "%") + "' ";
					else
						strIn += " OR " + strField + " LIKE '" + sItem.Replace("*", "%") + "' ";
				}

				Debug.WriteLine("strIn: " + strIn);
			}

			return strIn;

		}

		public string SetMaterialListSearch(ArrayList lst, string strField)
		{
			string strIn="";
			foreach(object myItem in lst)
			{
				string sItem = myItem.ToString().Trim();
				string target = "*";
				char[] anyOf = target.ToCharArray();
				int intFind = sItem.IndexOfAny(anyOf); // -1 if none found
				if(sItem != "" && intFind == -1)
				{
					if(strIn == "")
						strIn += " rtrim(ltrim(" + strField + ")) IN ('" + sItem + "'";
					else
						strIn += ",'" + sItem + "'";
				}

				Debug.WriteLine("strIn: " + strIn);
			}
			if(strIn != "")
				strIn += ")";

			return strIn;
		}
		
		public ArrayList SetMaterialList(string strMaterialItems)
		{
			string strMaterials = strMaterialItems.Replace("\r\n", "^");
			string[] materialList = strMaterials.Split('^');
			ArrayList arrayList = new ArrayList();
			for(int x=0; x<materialList.Length; x++)
			{
				arrayList.Add(materialList[x]);
			}

			return arrayList;
		}


		public DataTable AddItemsNotFound(DataTable dt, string strData, string strTargetCol, string strRefCol)
		{
			// Go thru the results and identify items requested that were not found
			ArrayList lst = this.SetMaterialList(strData);
			
			foreach(object myItem in lst)
			{
				string sItem = myItem.ToString().Trim();
				string target = "*";
				char[] anyOf = target.ToCharArray();
				int intFind = sItem.IndexOfAny(anyOf); // -1 if none found
				if(sItem != "" && intFind == -1)
				{
					DataRow[] rows = dt.Select(strTargetCol+"='"+sItem+"'");
					
					if(rows.Length==0)
					{
						DataRow r = dt.NewRow();
						r[strTargetCol] = sItem;
						r[strRefCol] = "Not Found";
						dt.Rows.Add(r);
					}
				}
			}

			return dt;
		}

		public DataTable SortTable(DataTable dt, string strOrder, string strDirection)
		{
			string strSort = strOrder + " " + strDirection;
			Debug.WriteLine("strSort: " + strSort);
			DataRow[] rows = dt.Select(null, strSort);

			DataTable dtSorted = dt.Clone();
			foreach (DataRow row in rows) 
			{
				dtSorted.ImportRow(row);
			}

			return dtSorted;
		}

        /// <summary>Executes a query.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A datatable of the results.</returns>
        public static DataTable SqlExecuteQuery(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            DataTable myDataTable = new DataTable();
            SqlConnection SqlConn = Utility.OpenSqlConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = SqlConn;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(command);
            myDataAdapter.Fill(myDataTable);
            myDataAdapter.Dispose();

            Utility.CloseSqlConnection(SqlConn);

            return myDataTable;
        }

        /// <summary>Execute non query and return the parameters.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static Collection<SqlParameter> SqlExecuteNonQueryReturnParameters(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection SqlConn = Utility.OpenSqlConnection();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            Utility.CloseSqlConnection(SqlConn);

            return parameters;
        }

        /// <summary>Execute non query and return the number of updated records.</summary>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>record update count</returns>
        public static int SqlExecuteNonQueryCount(string storedProcedureName, Collection<SqlParameter> parameters)
        {
            SqlConnection SqlConn = Utility.OpenSqlConnection();
            SqlTransaction myTransaction = SqlConn.BeginTransaction();
            int intCommandOutcome = 0;

            SqlCommand command = new SqlCommand(storedProcedureName, SqlConn, myTransaction);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count > 0)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }

            intCommandOutcome = command.ExecuteNonQuery();

            myTransaction.Commit();

            Utility.CloseSqlConnection(SqlConn);

            return intCommandOutcome;
        }

        /// <summary>Opens a new SQL connection.</summary>
        /// <returns>Returns a SQL connection</returns>
        public static SqlConnection OpenSqlConnection()
        {
            SqlConnection myConnection = new SqlConnection();
            if (myConnection.State != ConnectionState.Open)
            {
                myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["sqlConn"].ToString();
                myConnection.Open();
            }

            return myConnection;

        }

        /// <summary>Closes the SQL connection.</summary>
        /// <param name="connection">The SQL connection.</param>
        public static void CloseSqlConnection(SqlConnection connection)
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            else if (connection.State == ConnectionState.Broken)
            {
                connection.Close();
            }
        }

	}// end class
}
