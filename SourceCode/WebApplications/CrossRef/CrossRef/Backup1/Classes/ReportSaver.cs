using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
//using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Diagnostics;
using System.Configuration;


namespace CrossRef.Classes
{
	/// <summary>
	/// Summary description for ReportSaver.
	/// </summary>
	public class ReportSaver
	{

		public ReportSaver()
		{
		}


		public bool generateReport(DataSet ds, ReportDocument crDoc, string strFullFileName, string strExportType)
		{
			bool isExported = false;
			crDoc.SetDataSource(ds);

			// Declare variables and get the export options.
			ExportOptions exportOpts = new ExportOptions();
			DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
			exportOpts = crDoc.ExportOptions;

			// Set the File format options.
			switch (strExportType) 
			{
				case "pdf":
					exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
					break;
				case "excel":
					exportOpts.ExportFormatType = ExportFormatType.ExcelRecord;
					break;
				default:
					exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
					break;
			}


			// Set the disk file options and export.
			exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
			diskOpts.DiskFileName = strFullFileName;
			exportOpts.DestinationOptions = diskOpts;

			try
			{
				crDoc.Export();
				isExported = true;
			}
			catch(Exception ex)
			{
				Debug.WriteLine("ERROR: " + ex.Message);
			}
			
			return isExported;
		}
	} // end class
}
