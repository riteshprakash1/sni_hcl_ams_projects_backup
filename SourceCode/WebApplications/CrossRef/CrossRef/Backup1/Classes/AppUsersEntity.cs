﻿// ------------------------------------------------------------------
// <copyright file="AppUsersEntity.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrossRef.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    /// <summary> This is the Application User's Entity. </summary>    
    public class AppUsersEntity
    {
        /// <summary>This is the UserID.</summary>
        private int intUserID;

        /// <summary>This is the Username.</summary>
        private string strUserName;

        /// <summary>This is the Lastname.</summary>
        private string strLastName;

        /// <summary>This is the Firstname.</summary>
        private string strFirstName;

        /// <summary>This is the Role Name.</summary>
        private string strRoleName;

        /// <summary>This is the Created By.</summary>
        private string strCreatedBy;

        /// <summary>This is the CreateDate.</summary>
        private string strCreateDate;

        /// <summary>This is the Modified By.</summary>
        private string strModifiedBy;

        /// <summary>This is the ModifiedDate.</summary>
        private string strModifiedDate;
        
        /// <summary>Initializes a new instance of the AppUsersEntity class.</summary>
        public AppUsersEntity()
        {
            this.UserName = String.Empty;
            this.LastName = String.Empty;
            this.FirstName = String.Empty;
            this.RoleName = String.Empty;

            this.CreatedBy = String.Empty;
            this.CreateDate = String.Empty;
            this.ModifiedBy = String.Empty;
            this.ModifiedDate = String.Empty;
        }

        /// <summary>Gets or sets the User ID.</summary>
        /// <value>The User ID.</value>
        public int UserID
        {
            get{ return this.intUserID;}
            set{this.intUserID = value;}
        }

        /// <summary>Gets or sets the UserName.</summary>
        /// <value>The UserName.</value>
        public string UserName
        {
            get{ return this.strUserName;}
            set{this.strUserName = value;}
        }

        /// <summary>Gets or sets the Lastname.</summary>
        /// <value>The Lastname.</value>
        public string LastName
        {
            get{ return this.strLastName;}
            set{this.strLastName = value;}
        }

        /// <summary>Gets or sets the Firstname.</summary>
        /// <value>The First name.</value>
        public string FirstName
        {
            get{ return this.strFirstName;}
            set{this.strFirstName = value;}
        }

        /// <summary>Gets or sets the Role Name.</summary>
        /// <value>The Role Name.</value>
        public string RoleName
        {
            get { return this.strRoleName; }
            set { this.strRoleName = value; }
        }

        /// <summary>Gets or sets the CreatedBy.</summary>
        /// <value>The CreatedBy.</value>
        public string CreatedBy
        {
            get { return this.strCreatedBy; }
            set { this.strCreatedBy = value; }
        }

        /// <summary>Gets or sets the CreateDate.</summary>
        /// <value>The CreateDate.</value>
        public string CreateDate
        {
            get { return this.strCreateDate; }
            set { this.strCreateDate = value; }
        }

        /// <summary>Gets or sets the ModifiedBy.</summary>
        /// <value>The ModifiedBy.</value>
        public string ModifiedBy
        {
            get { return this.strModifiedBy; }
            set { this.strModifiedBy = value; }
        }

        /// <summary>Gets or sets the ModifiedDate.</summary>
        /// <value>The ModifiedDate.</value>
        public string ModifiedDate
        {
            get { return this.strModifiedDate; }
            set { this.strModifiedDate = value; }
        }

        ///// <summary>Determines whether the user is a Super Admin</summary>
        ///// <param name="user">The  AppUsersEntity</param>
        ///// <returns>true/ false</returns>
        //public static bool IsSuperAdmin(AppUsersEntity user)
        //{
        //    if (user == null)
        //        return false;
        //    else
        //        return Convert.ToBoolean(user.RoleName.ToLower() == "superadmin");
        //}


        /// <summary>Determines whether the user is a program Admin</summary>
        /// <param name="user">The  AppUsersEntity</param>
        /// <returns>true/ false</returns>
        public static bool IsAdmin(AppUsersEntity user)
        {
                return Convert.ToBoolean(user.RoleName.ToLower() == "admin");
        }

        /// <summary>Determines whether the user is a Contributor</summary>
        /// <param name="user">The  AppUsersEntity</param>
        /// <returns>true/ false</returns>
        public static bool IsContributor(AppUsersEntity user)
        {
                return Convert.ToBoolean(user.RoleName.ToLower() == "contributor");
        }

        /// <summary>Gets the AppUsersEntity by the UserName.</summary>
        /// <param name="strUserName">The Username</param>
        /// <returns>The newly populated AppUsersEntity</returns>
        public static AppUsersEntity GetAppUserByUserName(string strUserName)
        {
            AppUsersEntity appUser = new AppUsersEntity();
            
            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@username", strUserName));

            DataTable dt = Utility.SqlExecuteQuery("sp_GetAppUserByUserName", parameters);

            if(dt.Rows.Count > 0)
            {
                appUser = AppUsersEntity.GetEntityFromDataRow(dt.Rows[0]);
            }

            return appUser;
        }


        /// <summary>Gets all the AppUsersEntity Collection.</summary>
        /// <returns>The newly populated AppUsersEntity List</returns>
        public static List<AppUsersEntity> GetAppUsers()
        {
            List<AppUsersEntity> users = new List<AppUsersEntity>();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();

            DataTable dt = Utility.SqlExecuteQuery("sp_GetAppUsers", parameters);

            foreach (DataRow r in dt.Rows)
            {
                AppUsersEntity myUser = AppUsersEntity.GetEntityFromDataRow(r);
                users.Add(myUser);
            }

            return users;
        }

        /// <summary>Receives a AppUsers datarow and converts it to a AppUsersEntity.</summary>
        /// <param name="r">The App User DataRow.</param>
        /// <returns>AppUsersEntity</returns>
        public static AppUsersEntity GetEntityFromDataRow(DataRow r)
        {
            AppUsersEntity appUser = new AppUsersEntity();

            if ((r["UserID"] != null) && (r["UserID"] != DBNull.Value))
                appUser.UserID = Convert.ToInt32(r["UserID"].ToString());
 
            appUser.CreateDate = r["CreateDate"].ToString();
            appUser.CreatedBy = r["CreatedBy"].ToString();
            appUser.FirstName = r["FirstName"].ToString();
            appUser.LastName = r["LastName"].ToString();
            appUser.ModifiedBy = r["ModifiedBy"].ToString();
            appUser.ModifiedDate = r["ModifiedDate"].ToString();

            appUser.RoleName = r["RoleName"].ToString();
            appUser.UserName = r["UserName"].ToString();

            return appUser;
        }

        /// <summary>Inserts The App User into the database</summary>
        /// <param name="u">AppUsersEntity object</param>
        /// <returns>The AppUsers Entity</returns>
        public static AppUsersEntity InsertAppUser(AppUsersEntity u)
        {
            Collection<SqlParameter> myParameters = new Collection<SqlParameter>();

            SqlParameter paramID = new SqlParameter("@userID", SqlDbType.Int);
            paramID.Direction = ParameterDirection.Output;
            myParameters.Add(paramID);


            myParameters.Add(new SqlParameter("@rolename", u.RoleName));
            myParameters.Add(new SqlParameter("@ModifiedBy", u.ModifiedBy));
            myParameters.Add(new SqlParameter("@firstName", u.FirstName));
            myParameters.Add(new SqlParameter("@lastName", u.LastName));
            myParameters.Add(new SqlParameter("@username", u.UserName));

            myParameters = Utility.SqlExecuteNonQueryReturnParameters("sp_InsertAppUser", myParameters);
            u.UserID = Convert.ToInt32(paramID.Value.ToString());

            return u;
        }

        /// <summary>Delete the App User record in the database</summary>
        /// <param name="strUserName">the UserName</param>
        /// <returns>records updated count</returns>
        public static int DeleteAppUser(string strUserName)
        {
            Debug.WriteLine("DeleteAppUser: ");
             Collection<SqlParameter> myParameters = new Collection<SqlParameter>();
            myParameters.Add(new SqlParameter("@userName", strUserName));

            int intRecordsUpdated = Utility.SqlExecuteNonQueryCount("sp_DeleteAppUser", myParameters);

            return intRecordsUpdated;
        }

    }//// end class
}