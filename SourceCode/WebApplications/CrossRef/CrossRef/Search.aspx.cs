// ------------------------------------------------------------------
// <copyright file="Search.cs.aspx" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// ------------------------------------------------------------------

namespace CrossRef
{
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Diagnostics;
    using System.IO;
    using System.Data.SqlClient;
    using CrystalDecisions.CrystalReports.Engine;
    using CrystalDecisions.Shared;
    using System.Configuration;
    using CrossRef.Classes;
    
    /// <summary> Summary description for Search.</summary>    
    public partial class Search : System.Web.UI.Page
    {
        protected Utility myUtility;
        protected CrystalDecisions.Shared.ParameterDiscreteValue myID;
        private CrystalDecisions.CrystalReports.Engine.ReportDocument crDoc;
        protected CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions pars;
        protected AppUsersEntity myUser;

        private void Page_Load(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Search Page: Page_Load");

            myUser = AppUsersEntity.GetAppUserByUserName(ADUtility.GetUserNameOfAppUser(Request));

            if (Session["Utility"] != null)
                myUtility = (Utility)Session["Utility"];
            else
            {
                myUtility = new Utility(Request);
                Session["Utility"] = myUtility;
            }

            if (!Page.IsPostBack)
            {
                this.CleanReportFolder();

                this.ddlCompetitor.Items.Clear();
                ddlCompetitor.Items.Add(new ListItem("-- Select --", "*"));
                List<CompetitorEntity> competitors = CompetitorEntity.GetCompetitors();
                foreach (CompetitorEntity c in competitors)
                {
                    this.ddlCompetitor.Items.Add(new ListItem(c.CompetitorName, c.CompetitorName));
                }

                this.LoadLevelDropDowns(myUtility.TblProductHierarchy);

            }

            this.SetControls();
            Literal1.Text = "";

        }

        public void BindDT()
        {
            Debug.WriteLine("In BindDT");

            if (myUtility.TblSearch == null)
            {
                Debug.WriteLine("myUtility.TblSearch == null");
                myUtility.TblSearch = this.DoSearch();
            }

            Debug.WriteLine("BindDT Records: " + myUtility.TblSearch.Rows.Count.ToString());
            
            gvSearch.PageSize = Int32.Parse(this.ddlRecords.SelectedValue);
            gvSearch.DataSource = myUtility.TblSearch;
            gvSearch.DataBind();

            this.SetControls();

            if (gvSearch.PageIndex != 0)
            {
                btnFirst.ImageUrl = "images/first.gif";
                btnPrev.ImageUrl = "images/prev.gif";
            }
            else
            {
                btnFirst.ImageUrl = "images/firstd.gif";
                btnPrev.ImageUrl = "images/prevd.gif";
            }

            if (gvSearch.PageIndex != (gvSearch.PageCount - 1))
            {
                btnLast.ImageUrl = "images/last.gif";
                btnNext.ImageUrl = "images/next.gif";
            }
            else
            {
                btnLast.ImageUrl = "images/lastd.gif";
                btnNext.ImageUrl = "images/nextd.gif";
            }
            lblRecordCount.Text = "<b><font color=#FF7300>" + Convert.ToString(myUtility.TblSearch.Rows.Count) + "</font> records found";

            lblPageCnt.Text = " of " + gvSearch.PageCount.ToString();
            ddlPage.Items.Clear();

            Debug.WriteLine("PageCount = " + gvSearch.PageCount.ToString());
            for (int x = 1; x < gvSearch.PageCount + 1; x++)
            {
                ddlPage.Items.Add(new ListItem(x.ToString()));
            }
            int myPage = gvSearch.PageIndex + 1;
            
            if(ddlPage.Items.FindByValue(myPage.ToString()) != null)
                ddlPage.Items.FindByValue(myPage.ToString()).Selected = true;

            Session["Utility"] = myUtility;
        } // END BindDT()


        /// <summary>Handles the RowDataBound event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                string strCrossMaterialsID = this.gvSearch.DataKeys[e.Row.RowIndex]["CrossMaterialsID"].ToString();
                //Debug.WriteLine("strCrossMaterialsID: " + strCrossMaterialsID);
                Button btnDelete = (Button)e.Row.FindControl("btnDelete");
                btnDelete.Attributes.Add("onclick", "return confirm_deleteObjectName('Cross Reference');");

                if (strCrossMaterialsID == "0")
                {
                    btnDelete.Visible = false;

                    Button btnEdit = (Button)e.Row.FindControl("btnEdit");
                    btnEdit.Visible = false;
                }

                if (e.Row.RowIndex == this.gvSearch.EditIndex)
                {
                    Debug.WriteLine("Edit Index: " + this.gvSearch.EditIndex.ToString());
                    Debug.WriteLine("Is Null: " + Convert.ToString(e.Row.FindControl("txtCompetitorID") == null));
                    TextBox txtMaterialID = (TextBox)e.Row.FindControl("txtMaterialID");
                    TextBox txtCompetitorID = (TextBox)e.Row.FindControl("txtCompetitorID");
                    TextBox txtCompetitorDesc = (TextBox)e.Row.FindControl("txtCompetitorDesc");
                    Button btnUpdate = (Button)e.Row.FindControl("btnUpdate");
                    EnterButton.TieButton(txtMaterialID, btnUpdate);
                    EnterButton.TieButton(txtCompetitorID, btnUpdate);
                    EnterButton.TieButton(txtCompetitorDesc, btnUpdate);
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddMaterialID = (TextBox)e.Row.FindControl("txtAddMaterialID");
                TextBox txtAddCompetitorID = (TextBox)e.Row.FindControl("txtAddCompetitorID");
                TextBox txtAddCompetitorDesc = (TextBox)e.Row.FindControl("txtAddCompetitorDesc");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddMaterialID, btnAdd);
                EnterButton.TieButton(txtAddCompetitorID, btnAdd);
                EnterButton.TieButton(txtAddCompetitorDesc, btnAdd);
            }

        } // end GV_RowDataBoundConnSTring

        /// <summary>Handles the Delete event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strCrossMaterialsID = this.gvSearch.DataKeys[e.RowIndex]["CrossMaterialsID"].ToString();

            Collection<SqlParameter> parameters = new Collection<SqlParameter>();
            parameters.Add(new SqlParameter("@CrossMaterialsID", Convert.ToInt32(strCrossMaterialsID)));
            int recordsUpdated = Utility.SqlExecuteNonQueryCount("sp_DeleteCrossMaterial", parameters);

            Debug.WriteLine("CrossRef ID=" + strCrossMaterialsID + "; records deleted: " + recordsUpdated.ToString());

            this.gvSearch.EditIndex = -1;
            myUtility.TblSearch = null;
            this.BindDT();
        }

        /// <summary>Handles the Edit event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Edit(object sender, GridViewEditEventArgs e)
        {
            Debug.WriteLine("In GV_Edit");
            this.gvSearch.EditIndex = e.NewEditIndex;
            this.BindDT();
        }

        /// <summary>Handles the Update event of the GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewUpdateEventArgs"/> instance containing the event data.</param>
        protected void GV_Update(object sender, GridViewUpdateEventArgs e)
        {
            Page.Validate("UpdateCR");

            string strCrossMaterialsID = this.gvSearch.DataKeys[e.RowIndex]["CrossMaterialsID"].ToString();

            TextBox txtMaterialID = (TextBox)this.gvSearch.Rows[e.RowIndex].FindControl("txtMaterialID");
            TextBox txtCompetitorID = (TextBox)this.gvSearch.Rows[e.RowIndex].FindControl("txtCompetitorID");
            TextBox txtCompetitorDesc = (TextBox)this.gvSearch.Rows[e.RowIndex].FindControl("txtCompetitorDesc");
            DropDownList ddlCompetitor = (DropDownList)this.gvSearch.Rows[e.RowIndex].FindControl("ddlCompetitor");
            TextBox txtNotes = (TextBox)this.gvSearch.Rows[e.RowIndex].FindControl("txtNotes");

   //         CustomValidator vldCrossRef = (CustomValidator)this.gvSearch.Rows[e.RowIndex].FindControl("vldCrossRef");
            CustomValidator vldValidMaterialID = (CustomValidator)this.gvSearch.Rows[e.RowIndex].FindControl("vldValidMaterialID");

  //          vldCrossRef.IsValid = Convert.ToBoolean((txtMaterialID.Text != string.Empty) || (txtCompetitorID.Text != string.Empty));

            if (txtMaterialID.Text != string.Empty)
            {
                Collection<SqlParameter> matParameters = new Collection<SqlParameter>();
                matParameters.Add(new SqlParameter("@MaterialID", txtMaterialID.Text));
                DataTable dtMaterial = Utility.SqlExecuteQuery("sp_GetGdwMaterialByMaterialID", matParameters);

                vldValidMaterialID.IsValid = Convert.ToBoolean(dtMaterial.Rows.Count > 0);
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> parameters = new Collection<SqlParameter>();
                parameters.Add(new SqlParameter("@CrossMaterialsID", Convert.ToInt32(strCrossMaterialsID)));
                parameters.Add(new SqlParameter("@MaterialID", txtMaterialID.Text.ToUpper()));
                parameters.Add(new SqlParameter("@Competitor", ddlCompetitor.SelectedValue));
                parameters.Add(new SqlParameter("@CompetitorDesc", txtCompetitorDesc.Text));
                parameters.Add(new SqlParameter("@CompetitorID", txtCompetitorID.Text.ToUpper()));
                parameters.Add(new SqlParameter("@Notes", txtNotes.Text));
                parameters.Add(new SqlParameter("@modifiedBy", ADUtility.GetUserNameOfAppUser(Request)));

                int recordsUpdated = Utility.SqlExecuteNonQueryCount("sp_UpdateCrossMaterial", parameters);

                Debug.WriteLine("CrossRef ID=" + strCrossMaterialsID + "; records updated: " + recordsUpdated.ToString());

                this.gvSearch.EditIndex = -1;
                this.BindDT();

            } //// Page.IsValid
        } // end GV_Update

        /// <summary>Handles the Edit event of the GridView control. </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("AddCR");

            TextBox txtAddMaterialID = (TextBox)this.gvSearch.FooterRow.FindControl("txtAddMaterialID");
            TextBox txtAddCompetitorID = (TextBox)this.gvSearch.FooterRow.FindControl("txtAddCompetitorID");
            TextBox txtAddCompetitorDesc = (TextBox)this.gvSearch.FooterRow.FindControl("txtAddCompetitorDesc");
            DropDownList ddlAddCompetitor = (DropDownList)this.gvSearch.FooterRow.FindControl("ddlAddCompetitor");
            TextBox txtAddNotes = (TextBox)this.gvSearch.FooterRow.FindControl("txtAddNotes");

    //        CustomValidator vldAddCrossRef = (CustomValidator)this.gvSearch.FooterRow.FindControl("vldAddCrossRef");
            CustomValidator vldAddValidMaterialID = (CustomValidator)this.gvSearch.FooterRow.FindControl("vldAddValidMaterialID");

   //         vldAddCrossRef.IsValid = Convert.ToBoolean((txtAddMaterialID.Text != string.Empty) || (txtAddCompetitorID.Text != string.Empty));

            if (txtAddMaterialID.Text != string.Empty)
            {
                Collection<SqlParameter> matParameters = new Collection<SqlParameter>();
                matParameters.Add(new SqlParameter("@MaterialID", txtAddMaterialID.Text));
                DataTable dtMaterial = Utility.SqlExecuteQuery("sp_GetGdwMaterialByMaterialID", matParameters);

                vldAddValidMaterialID.IsValid = Convert.ToBoolean(dtMaterial.Rows.Count > 0);
            }

            if (Page.IsValid)
            {
                Collection<SqlParameter> parameters = new Collection<SqlParameter>();
                SqlParameter paramID = new SqlParameter("@CrossMaterialsID", SqlDbType.Int);
                paramID.Direction = ParameterDirection.Output;
                parameters.Add(paramID);

                parameters.Add(new SqlParameter("@MaterialID", txtAddMaterialID.Text.ToUpper()));
                parameters.Add(new SqlParameter("@Competitor", ddlAddCompetitor.SelectedValue));
                parameters.Add(new SqlParameter("@CompetitorDesc", txtAddCompetitorDesc.Text));
                parameters.Add(new SqlParameter("@CompetitorID", txtAddCompetitorID.Text.ToUpper()));
                parameters.Add(new SqlParameter("@Notes", txtAddNotes.Text));
                parameters.Add(new SqlParameter("@modifiedBy", ADUtility.GetUserNameOfAppUser(Request)));

                parameters = Utility.SqlExecuteNonQueryReturnParameters("sp_InsertCrossMaterial", parameters);
                int intCrossMaterialsID = Convert.ToInt32(paramID.Value.ToString());

                Debug.WriteLine("CrossRef Add; ID=" + intCrossMaterialsID.ToString());
                
                this.gvSearch.EditIndex = -1;
                myUtility.TblSearch = null;
                this.BindDT();

            } //// Page.IsValid
        }

        /// <summary>Gets the Competitors collection.</summary>
        /// <returns>the Competitors collection</returns>
        public Collection<ListItem> GetCompetitors()
        {
                DataTable dt = myUtility.SetTable("sp_GetCompetitors");

                Collection<ListItem> items = new Collection<ListItem>();
                items.Add(new ListItem("-- Select --", ""));

                foreach (DataRow r in dt.Rows)
                {
                    items.Add(new ListItem(r["CompetitorName"].ToString(), r["CompetitorName"].ToString()));
                }

                return items;
        }

        ///// <summary>Handles the Sorting event of the grid view control.</summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void sortHandler(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            if (ViewState["OrderBy"].ToString() == e.SortExpression)
            {
                if (ViewState["SortDirection"].ToString() == "asc")
                    ViewState["SortDirection"] = "desc";
                else
                    ViewState["SortDirection"] = "asc";
            }
            else
                ViewState["SortDirection"] = "asc";

            ViewState["OrderBy"] = e.SortExpression;
            //	dt = (DataTable)Session["dt"];

            if (myUtility.TblSearch == null)
            {
                myUtility.TblSearch = this.DoSearch();
            }
            else
            {
                string strSort = ViewState["OrderBy"].ToString() + " " + ViewState["SortDirection"].ToString();
                Debug.WriteLine("strSort: " + strSort);
                DataRow[] rows = myUtility.TblSearch.Select(null, strSort);

                myUtility.TblSearch = myUtility.TblSearch.Clone();
                foreach (DataRow row in rows)
                {
                    myUtility.TblSearch.ImportRow(row);
                }
            }
            this.BindDT();
            Session["Utility"] = myUtility;
        } // END sortHandler

        protected void btnFirst_Click(object sender, ImageClickEventArgs e)
        {
            gvSearch.PageIndex = 0;
            this.BindDT();
        } // END btnFirst_Click

        protected void btnPrev_Click(object sender, ImageClickEventArgs e)
        {
            if (gvSearch.PageIndex >= 1)
                gvSearch.PageIndex = gvSearch.PageIndex - 1;
            else
                gvSearch.PageIndex = 0;

            this.BindDT();
        }// END btnPrev_Click

        protected void btnNext_Click(object sender, ImageClickEventArgs e)
        {
            if (gvSearch.PageIndex != (gvSearch.PageCount - 1))
                gvSearch.PageIndex = gvSearch.PageIndex + 1;
            else
                gvSearch.PageIndex = (gvSearch.PageCount - 1);

            this.BindDT();
        }// END btnNext_Click


        protected void btnLast_Click(object sender, ImageClickEventArgs e)
        {
            gvSearch.PageIndex = (gvSearch.PageCount - 1);
            this.BindDT();
        }// END btnLast_Click

        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvSearch.PageIndex = ddlPage.SelectedIndex;
            this.BindDT();
        }

        public void SetControls()
        {
            if (myUtility.TblSearch == null || myUtility.TblSearch.Rows.Count == 0)
            {
                gvSearch.Visible = false;
                btnFirst.Visible = false;
                btnPrev.Visible = false;
                ddlPage.Visible = false;
                lblPageCnt.Visible = false; ;
                btnNext.Visible = false;
                btnLast.Visible = false;
                lblRecordsPerPage.Visible = false;
                this.ddlRecords.Visible = false;
                this.btnExport.Visible = false;
                this.ddlExport.Visible = false;

                lblRecordCount.Visible = false;
                if (!Page.IsPostBack)
                    lblNoRecords.Visible = false;
                else
                    lblNoRecords.Visible = true;

            }
            else
            {
                gvSearch.Visible = true;
                btnFirst.Visible = true;
                btnPrev.Visible = true;
                ddlPage.Visible = true;
                lblPageCnt.Visible = true; ;
                btnNext.Visible = true;
                btnLast.Visible = true;
                lblRecordCount.Visible = true;
                lblNoRecords.Visible = false;
                this.ddlRecords.Visible = true;
                this.btnExport.Visible = true;
                this.ddlExport.Visible = true;
                lblRecordCount.Visible = true;
                lblRecordsPerPage.Visible = true;
            } // END if

            bool showAdminColumns = Convert.ToBoolean(myUtility.IsAdmin || myUtility.IsContributor);
            gvSearch.Columns[0].Visible = showAdminColumns;
            gvSearch.Columns[11].Visible = showAdminColumns;
            gvSearch.ShowFooter = showAdminColumns;

        } // END SetControls()

        private DataTable DoSearch()
        {
            string strSearchQuery = "  SELECT CrossMaterialsID, MaterialID, Competitor, CompetitorID, CompetitorDesc, Material_Desc, Notes, Material_Type, Level_1_Id, ";
            strSearchQuery += "Level_1_Desc, Level_2_Id, Level_2_Desc, Level_3_Id, Level_3_Desc, Level_4_Id, ";
            strSearchQuery += "Level_4_Desc, Level_5_Id, Level_5_Desc, Level_6_Id, Level_6_Desc ";
            strSearchQuery += "FROM CrossReferenceSearch_v  ";

            string strWhere = "WHERE MaterialID<>'zzzzzzzzzzz' ";

            if (ViewState["Competitor"].ToString() != "*")
                strWhere += " AND Competitor =  '" + ViewState["Competitor"].ToString() + "' ";

            if (ViewState["Level2"].ToString() != "*")
                strWhere += " AND Level_2_Id =  '" + ViewState["Level2"].ToString() + "' ";

            if (ViewState["Level3"].ToString() != "*")
                strWhere += " AND Level_3_Id =  '" + ViewState["Level3"].ToString() + "' ";

            if (ViewState["Level4"].ToString() != "*")
                strWhere += " AND Level_4_Id =  '" + ViewState["Level4"].ToString() + "' ";

            if (ViewState["Level5"].ToString() != "*")
                strWhere += " AND Level_5_Id =  '" + ViewState["Level5"].ToString() + "' ";

            if (ViewState["MaterialID"].ToString() != "")
            {
                ArrayList snList = myUtility.SetMaterialList(ViewState["MaterialID"].ToString());
                string strInClause = myUtility.SetMaterialListSearch(snList, "MaterialID");
                strWhere += " AND (" + myUtility.SetMaterialListSearch(snList, "MaterialID");
                if (strInClause == "")
                    strWhere += myUtility.SetWildCardList(snList, "MaterialID", "") + " ";
                else
                    strWhere += myUtility.SetWildCardList(snList, "MaterialID", " OR ") + " ";
                strWhere += ") ";
            }

            if (ViewState["CompetitorID"].ToString() != "")
            {
                ArrayList CompList = myUtility.SetMaterialList(ViewState["CompetitorID"].ToString());
                string strInClause = myUtility.SetMaterialListSearch(CompList, "CompetitorID");
                strWhere += " AND (" + myUtility.SetMaterialListSearch(CompList, "CompetitorID");
                if (strInClause == "")
                    strWhere += myUtility.SetWildCardList(CompList, "CompetitorID", "") + " ";
                else
                    strWhere += myUtility.SetWildCardList(CompList, "CompetitorID", " OR ") + " ";
                strWhere += ") ";
            }

            string strOrderBy = "MaterialID ";
            if (ViewState["OrderBy"].ToString() != "")
                strOrderBy = ViewState["OrderBy"].ToString();

            string strSortDirection = "asc ";
            if (ViewState["SortDirection"].ToString() != "")
                strSortDirection = ViewState["SortDirection"].ToString();



            strSearchQuery += strWhere + " ORDER BY " + strOrderBy + " " + strSortDirection;

            Debug.WriteLine("Query = " + strSearchQuery);
            DataTable dt = new DataTable();

            if (myUtility.SqlConn.State != ConnectionState.Open)
                myUtility.SqlConn.Open();

            SqlCommand cmdSearch = new System.Data.SqlClient.SqlCommand();
            cmdSearch.CommandText = strSearchQuery;
            cmdSearch.Connection = myUtility.SqlConn;

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmdSearch);
                da.Fill(dt);
                //dt = myUtility.AddItemsNotFound(dt, ViewState["MaterialID"].ToString(), "MaterialID", "CompetitorID");
                //dt = myUtility.AddItemsNotFound(dt, ViewState["CompetitorID"].ToString(), "CompetitorID", "MaterialID");

                //dt = myUtility.SortTable(dt, ViewState["OrderBy"].ToString(), ViewState["SortDirection"].ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
                LogException.HandleException(ex, Request);
            }

            myUtility.SqlConn.Close();

            return dt;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("btnSearch_Click");
            //Page.Validate();

            //if (Page.IsValid)
            //{
                this.SetViewState();
                gvSearch.PageIndex = 0;
                this.gvSearch.EditIndex = -1;
                myUtility.TblSearch = this.DoSearch();
                this.BindDT();
                this.SetControls();

                Debug.WriteLine("Rows = " + myUtility.TblSearch.Rows.Count.ToString());
                Session["Utility"] = myUtility;
            //}
        }

        private void SetViewState()
        {
            ViewState["Level2"] = this.ddlLevel2.SelectedValue;
            ViewState["Level3"] = this.ddlLevel3.SelectedValue;
            ViewState["Level4"] = this.ddlLevel4.SelectedValue;
            ViewState["Level5"] = this.ddlLevel5.SelectedValue;
            ViewState["Competitor"] = this.ddlCompetitor.SelectedValue;
            ViewState["MaterialID"] = this.txtMaterialID.Text;
            ViewState["CompetitorID"] = this.txtMaterialList.Text;
            ViewState["OrderBy"] = "MaterialID ";
            ViewState["SortDirection"] = "asc";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            myUtility.TblSearch = null;
            Session["Utility"] = myUtility;
            Response.Redirect("Search.aspx");
        }


        protected void ddlRecords_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvSearch.PageIndex = 0;
            this.BindDT();
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("btnExport_Click");
            //			string strFullFile = Server.MapPath("report\\CrossRefRpt.rpt");
            string strReportFolder = ConfigurationManager.AppSettings["ReportFolder"];
            string strFullFile = strReportFolder + "CrossReferenceSearch.rpt";
            if (this.ddlExport.SelectedValue.ToLower() == "excel")
                strFullFile = strReportFolder + "CrossReferenceSearchExcel.rpt";
            crDoc = new ReportDocument();
            crDoc.Load(strFullFile);

            Debug.WriteLine("Location: " + crDoc.FilePath + "; strFullFile: " + strFullFile);

            string strRptID = "";
            DateTime t = DateTime.Now;
            strRptID += t.Year.ToString() + t.Month.ToString() + t.Day.ToString();
            strRptID += t.Hour.ToString() + t.Minute.ToString() + t.Second.ToString() + t.Millisecond.ToString();

            string sExt = ".pdf";
            Debug.WriteLine("1-sExt: " + sExt + "; " + this.ddlExport.SelectedValue.ToLower());
            if (this.ddlExport.SelectedValue.ToLower() == "excel")
            {
                sExt = ".xls";
                Debug.WriteLine("2-sExt: " + sExt + "; " + this.ddlExport.SelectedValue.ToLower());
            }

            string rptName = "Reports\\CrossRef_" + strRptID + sExt;
            string strRptFullFile = Server.MapPath(rptName);
            if (File.Exists(strRptFullFile))
                File.Delete(strRptFullFile);

            DataTable dt = this.DoSearch();
            dt.TableName = "CrossReferenceSearch_v"; //"cr_CrossReference";
            //			myUtility.TblSearch.TableName =  "cr_CrossReference"; //"cr_CrossReference";
            DataSet ds = new DataSet("DataSet2");
            ds.Tables.Add(dt);

            ReportSaver saver = new ReportSaver();
            bool exported = saver.generateReport(ds, crDoc, strRptFullFile, this.ddlExport.SelectedValue.ToLower());
            Debug.WriteLine("Report Exported: " + exported + "; File: " + strRptFullFile);

            Literal1.Text = "<script type='text/javascript'>Start('" + myUtility.SiteURL + "crossref/Reports/CrossRef_" + strRptID + sExt + "',document.forms[0]);</script>";

        }

        private void CleanReportFolder()
        {
            try
            {
                string strRptFolder = Server.MapPath("Reports\\");
                Debug.WriteLine("strRptFolder: " + strRptFolder);
                if (Directory.Exists(strRptFolder))
                {
                    DirectoryInfo d = new DirectoryInfo(strRptFolder);
                    FileInfo[] files = d.GetFiles();
                    for (int x = 0; x < files.Length; x++)
                    {
                        FileInfo f = files[x];
                        TimeSpan span = DateTime.Now.Subtract(f.CreationTime);
                        if (span.Days > 5)
                            f.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("CleanReportFolder Error: " + ex.Message);
            }

        }// end CleanReportFolder

        protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string strLevelID = this.ddlLevel3.SelectedValue;
            DataTable dt = this.GetProductHierarchy();
            this.LoadLevelDropDowns(dt);
        }

        protected DataTable GetProductHierarchy()
        {
            string strWhere = string.Empty;
            string strLevel2 = string.Empty;
            string strLevel3 = string.Empty;
            string strLevel4 = string.Empty;
            string strLevel5 = string.Empty;
            string strLevel6 = string.Empty;

            if (ddlLevel2.SelectedIndex != 0)
            {
                strLevel2 = this.ddlLevel2.SelectedValue;
                if(strWhere == string.Empty)
                    strWhere += "Level_2_Id = '" + strLevel2 + "' ";
                else
                    strWhere += "AND Level_2_Id = '" + strLevel2 + "' ";

            }

            if (ddlLevel3.SelectedIndex != 0)
            {
                strLevel3 = this.ddlLevel3.SelectedValue;
                if (strWhere == string.Empty)
                    strWhere += "Level_3_Id = '" + strLevel3 + "' ";
                else
                    strWhere += "AND Level_3_Id = '" + strLevel3 + "' ";

            }

            if (ddlLevel4.SelectedIndex != 0)
            {
                strLevel4 = this.ddlLevel4.SelectedValue;
                if (strWhere == string.Empty)
                    strWhere += "Level_4_Id = '" + strLevel4 + "' ";
                else
                    strWhere += "AND Level_4_Id = '" + strLevel4 + "' ";
            }

            if (ddlLevel5.SelectedIndex != 0)
            {
                strLevel5 = this.ddlLevel5.SelectedValue;
                if (strWhere == string.Empty)
                    strWhere += "Level_5_Id = '" + strLevel5 + "' ";
                else
                    strWhere += "AND Level_5_Id = '" + strLevel5 + "' ";
            }

            Debug.WriteLine("strWhere: " + strWhere + "; TblProductHierarchy Records: " + myUtility.TblProductHierarchy.Rows.Count.ToString());

            DataTable dtProductHierarchy = myUtility.TblProductHierarchy.Select(strWhere, "Level_2_Id, Level_3_Id, Level_4_Id, Level_5_Id, Level_6_Id").CopyToDataTable();

            return dtProductHierarchy;
        }

        protected void LoadLevelDropDowns(DataTable dtProductHier)
        {
            string strWhere = string.Empty;
            string strLevel2 = string.Empty;
            string strLevel3 = string.Empty;
            string strLevel4 = string.Empty;
            string strLevel5 = string.Empty;
            string strLevel6 = string.Empty;

            if (ddlLevel2.SelectedIndex != 0)
                strLevel2 = this.ddlLevel2.SelectedValue;

            if (ddlLevel3.SelectedIndex != 0)
                strLevel3 = this.ddlLevel3.SelectedValue;

            if (ddlLevel4.SelectedIndex != 0)
                strLevel4 = this.ddlLevel4.SelectedValue;

            if (ddlLevel5.SelectedIndex != 0)
                strLevel5 = this.ddlLevel5.SelectedValue;

            //if (ddlLevel6.SelectedIndex != 0)
            //    strLevel6 = this.ddlLevel6.SelectedValue;


            string strCurrentID = string.Empty;
            this.ddlLevel2.Items.Clear();
            ddlLevel2.Items.Add(new ListItem("-- Select --", "*"));
            foreach (DataRow r in dtProductHier.Rows)
            {
                if (strCurrentID != r["Level_2_Id"].ToString())
                {
                    this.ddlLevel2.Items.Add(new ListItem(r["Level_2_Id"].ToString() + ": " + r["Level_2_Desc"].ToString(), r["Level_2_Id"].ToString()));
                    strCurrentID = r["Level_2_Id"].ToString();
                }
            }

            strCurrentID = string.Empty;
            this.ddlLevel3.Items.Clear();
            ddlLevel3.Items.Add(new ListItem("-- Select --", "*"));
            foreach (DataRow r in dtProductHier.Rows)
            {
                if (strCurrentID != r["Level_3_Id"].ToString())
                {
                    this.ddlLevel3.Items.Add(new ListItem(r["Level_3_Id"].ToString() + ": " + r["Level_3_Desc"].ToString(), r["Level_3_Id"].ToString()));
                    strCurrentID = r["Level_3_Id"].ToString();
                }
            }

            strCurrentID = string.Empty;
            this.ddlLevel4.Items.Clear();
            ddlLevel4.Items.Add(new ListItem("-- Select --", "*"));
            foreach (DataRow r in dtProductHier.Rows)
            {
                if (strCurrentID != r["Level_4_Id"].ToString())
                {
                    this.ddlLevel4.Items.Add(new ListItem(r["Level_4_Id"].ToString() + ": " + r["Level_4_Desc"].ToString(), r["Level_4_Id"].ToString()));
                    strCurrentID = r["Level_4_Id"].ToString();
                }
            }

            strCurrentID = string.Empty;
            this.ddlLevel5.Items.Clear();
            ddlLevel5.Items.Add(new ListItem("-- Select --", "*"));
            foreach (DataRow r in dtProductHier.Rows)
            {
                if (strCurrentID != r["Level_5_Id"].ToString())
                {
                    this.ddlLevel5.Items.Add(new ListItem(r["Level_5_Id"].ToString() + ": " + r["Level_5_Desc"].ToString(), r["Level_5_Id"].ToString()));
                    strCurrentID = r["Level_5_Id"].ToString();
                }
            }

            if (strLevel2 != string.Empty && this.ddlLevel2.Items.FindByValue(strLevel2) != null)
                ddlLevel2.Items.FindByValue(strLevel2).Selected = true;

            if (strLevel3 != string.Empty && this.ddlLevel3.Items.FindByValue(strLevel3) != null)
                ddlLevel3.Items.FindByValue(strLevel3).Selected = true;

            if (strLevel4 != string.Empty && this.ddlLevel4.Items.FindByValue(strLevel4) != null)
                ddlLevel4.Items.FindByValue(strLevel4).Selected = true;

            if (strLevel5 != string.Empty && this.ddlLevel5.Items.FindByValue(strLevel5) != null)
                ddlLevel5.Items.FindByValue(strLevel5).Selected = true;

        }

    } //// end class
}