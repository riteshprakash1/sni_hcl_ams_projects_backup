﻿// --------------------------------------------------------------
// <copyright file="Site.cs" company="Smith and Nephew">
//     Copyright (c) 2015 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------

namespace CrossRef
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrossRef.Classes;

    /// <summary>This is the Master page for the Web Application.</summary>    
    public partial class Site1 : System.Web.UI.MasterPage
    {

        protected Utility myUtility;

        /// <summary>Handles the OnInit event of the Page control.  This fixes Menu rendering in Chrome and Safari.</summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {    // For Chrome and Safari    
            //if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            //{
            //    if (Request.Browser.Adapters.Count > 0)
            //    {
            //        Request.Browser.Adapters.Clear();
            //        Response.Redirect(Page.Request.Url.AbsoluteUri);
            //    }
            Response.Write("<br>OnInit");
            //}
        }

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/ShowHint.js"));
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/jquery-1.4.1.min.js"));
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/jquery-1.4.1.js"));
            this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global", this.ResolveClientUrl("~/Scripts/jquery-1.4.1-vsdoc.js"));

            string strReferringPage = Request.Url.ToString().ToLower();
            Debug.WriteLine("strReferringPage: " + strReferringPage);

            if (!strReferringPage.Contains("errorpage"))
            {

                if (Session["Utility"] != null)
                    myUtility = (Utility)Session["Utility"];
                else
                {
                    myUtility = new Utility(Request);
                    Session["Utility"] = myUtility;
                }
            }

            if (!Page.IsPostBack)
            {
                bool blnShowEnvironment = !Convert.ToBoolean(ConfigurationManager.AppSettings["isProduction"].ToString());

                if (blnShowEnvironment)
                    this.lblEnvironment.Text = ConfigurationManager.AppSettings["Environment"].ToString();

                this.ltlEnvBreak.Visible = blnShowEnvironment;
                this.lblEnvironment.Visible = blnShowEnvironment;

                Debug.WriteLine("Site Master !Page.IsPostBack");
                this.lblAppName.Text = ConfigurationManager.AppSettings["AppName"];

                //Debug.WriteLine("UserName: " + myUtility.AppUser.UserName + "; RoleName: " + myUtility.AppUser.RoleName + "; IsAdmin: " + myUtility.IsAdmin.ToString());

                try
                {
                    if (myUtility.IsAdmin)
                    {
                        this.NavMenu.Items.Add(new MenuItem("Search", "", "", "~/Search.aspx"));

                        MenuItem adminMenu = new MenuItem("Administration", "", "", "");
                        adminMenu.ChildItems.Add(new MenuItem("Competitors", "", "", "~/Admin/Competitors.aspx"));
                        adminMenu.ChildItems.Add(new MenuItem("User List", "", "", "~/Admin/UserList.aspx"));

                        this.NavMenu.Items.Add(adminMenu);
                    }
                }
                catch (Exception ex)
                {
                    LogException.HandleException(ex, Request);
                }

            }
        }

        /// <summary>Called when MenuItem is clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.MenuEventArgs"/> instance containing the event data.</param>
        public void OnClick(Object sender, MenuEventArgs e)
        {
            Debug.WriteLine("In OnClick");
            //MessageLabel.Text = "You selected " + e.Item.Text + ".";
            Debug.WriteLine("Value: " + e.Item.Value + "; Text: " + e.Item.Text);
            e.Item.Selected = true;
            string strPage = "~/" + e.Item.Value.Replace("Admin", "Admin/");
            Response.Redirect(strPage, true);
        }

    } //// end class
}