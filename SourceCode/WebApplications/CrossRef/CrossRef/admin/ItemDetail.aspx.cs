using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Diagnostics;
using System.Data.SqlClient;
using CrossRef.Classes;

namespace CrossRef.admin
{
	/// <summary>
	/// Summary description for ItemDetail.
	/// </summary>
	public partial class ItemDetail : System.Web.UI.Page
	{
		protected Utility myUtility;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			if(Session["adminUtility"]!=null)
				myUtility = (Utility)Session["adminUtility"];
			else
			{
				myUtility = new Utility(Request);
				Session["adminUtility"] = myUtility;
			}

			this.lblMsg.Text = "";

			if(!Page.IsPostBack)
			{
				this.ddlCategory.Items.Clear();
				ddlCategory.Items.Add(new ListItem("-- Select --",""));
				foreach(DataRow r in myUtility.TblCategory.Rows)
				{
					this.ddlCategory.Items.Add(new ListItem(r["Category"].ToString(),r["Category"].ToString()));
				}

				this.ddlCompetitor.Items.Clear();
				ddlCompetitor.Items.Add(new ListItem("-- Select --",""));
				foreach(DataRow r in myUtility.TblCompetitor.Rows)
				{
					this.ddlCompetitor.Items.Add(new ListItem(r["Competitor"].ToString(),r["Competitor"].ToString()));
				}

				this.ddlFranchise.Items.Clear();
				ddlFranchise.Items.Add(new ListItem("-- Select --",""));
				foreach(DataRow r in myUtility.TblFranchise.Rows)
				{
					this.ddlFranchise.Items.Add(new ListItem(r["Franchise"].ToString(),r["Franchise"].ToString()));
				}

				this.ddlGBU.Items.Clear();
				ddlGBU.Items.Add(new ListItem("-- Select --",""));
				foreach(DataRow r in myUtility.TblGbu.Rows)
				{
					this.ddlGBU.Items.Add(new ListItem(r["gbuDesc"].ToString(),r["gbuDesc"].ToString()));
				}

				this.btnDelete.Attributes.Add("onclick", "return confirm_delete();");

				Debug.WriteLine("Item Detail ID: " + myUtility.CrossReferenceID.ToString());

				if(myUtility.CrossReferenceID != 0)
				{
					if(myUtility.SqlConn.State != ConnectionState.Open)
						myUtility.SqlConn.Open();

					ViewState["CrossReferenceID"] = myUtility.CrossReferenceID;
					this.LoadData();
					myUtility.SqlConn.Close();
				}
				else
				{
					ViewState["CrossReferenceID"] = myUtility.CrossReferenceID;

                    if (this.ddlGBU.Items.FindByValue(ConfigurationManager.AppSettings["defaultGBU"].ToString().ToLower()) != null)
                        ddlGBU.Items.FindByValue(ConfigurationManager.AppSettings["defaultGBU"].ToString().ToLower()).Selected = true;

				}

			}

			Debug.WriteLine("CrossReferenceID: " + myUtility.CrossReferenceID.ToString());
			Session["adminUtility"] = myUtility;

		} // end Page_Load

		private void LoadData()
		{
			DataTable dt = myUtility.SetTable("sp_cr_GetCrossReferenceByID", Int32.Parse(ViewState["CrossReferenceID"].ToString()));
			if(dt.Rows.Count>0)
			{
				DataRow r = dt.Rows[0];
				this.txtCompetitorID.Text = r["CompetitorID"].ToString();
				this.txtMaterialID.Text = r["MaterialID"].ToString();
				this.txtMaterialDesc.Text = r["MaterialDesc"].ToString();
				this.txtCompetitorDesc.Text = r["CompetitorDesc"].ToString();

				if(this.ddlCategory.Items.FindByValue(r["category"].ToString()) != null)
					ddlCategory.Items.FindByValue(r["category"].ToString()).Selected=true;

				if(this.ddlFranchise.Items.FindByValue(r["franchise"].ToString()) != null)
					ddlFranchise.Items.FindByValue(r["franchise"].ToString()).Selected=true;

				if(this.ddlCompetitor.Items.FindByValue(r["Competitor"].ToString()) != null)
					ddlCompetitor.Items.FindByValue(r["Competitor"].ToString()).Selected=true;

				if(this.ddlGBU.Items.FindByValue(r["gbu"].ToString()) != null)
					ddlGBU.Items.FindByValue(r["gbu"].ToString()).Selected=true;

			}

		}




		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			myUtility.TblSearch = new DataTable();
			Response.Redirect("AdminSearch.aspx");
			Session["adminUtility"] = myUtility;

		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			int myID = Int32.Parse(ViewState["CrossReferenceID"].ToString());

			SqlCommand cmdDelete = new System.Data.SqlClient.SqlCommand();
			cmdDelete.CommandText = "sp_cr_DeleteCrossReferenceByID";
			cmdDelete.CommandType = CommandType.StoredProcedure;
			cmdDelete.Connection = myUtility.SqlConn;
			cmdDelete.Parameters.Add(new SqlParameter("@CrossReferenceID", Int32.Parse(ViewState["CrossReferenceID"].ToString())));

			try
			{
				myUtility.SqlConn.Open();
				int rowsUpdated = cmdDelete.ExecuteNonQuery();
				myUtility.SqlConn.Close();
			
				this.lblMsg.Text = "The record was successfully deleted.";
				this.btnDelete.Visible = false;
				this.btnUpdate.Visible = false;
			}
			catch(Exception ex)
			{
				this.lblMsg.Text = "An error occurred, the record was NOT successfully deleted.";
				LogException.HandleException(ex, Request);
				Debug.WriteLine(cmdDelete.CommandText + " Error: " + ex.Message);
				myUtility.SqlConn.Close();
			}
			Session["adminUtility"] = myUtility;
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			myUtility.SqlConn.Open();
			Debug.WriteLine("In btnUpdate_Click");
			Page.Validate();
			System.Web.UI.ValidatorCollection vals = Page.Validators;

			foreach(IValidator val in vals)
			{
				if(val.ErrorMessage.StartsWith("Duplicate Cross Reference"))
				{
					val.IsValid = true;
					DataTable dt = myUtility.SetTable("sp_cr_GetCrossReferenceByMaterialIDByCompetitorID", this.txtMaterialID.Text, this.txtCompetitorID.Text, this.ddlCompetitor.SelectedValue);
					Debug.WriteLine("Crit: txtMaterialID='" + this.txtMaterialID.Text + "; txtCompetitorID: " + this.txtCompetitorID.Text + "; ddlCompetitor: " + this.ddlCompetitor.SelectedValue);
					
					if(dt.Rows.Count>0)
					{
						if(Int32.Parse(ViewState["CrossReferenceID"].ToString()) == 0)
						{
							Debug.WriteLine("Insert Crit: Gbu='" + this.ddlGBU.SelectedValue + "'");
							DataRow[] rows = dt.Select("Gbu='" + this.ddlGBU.SelectedValue + "'");
							val.IsValid = Convert.ToBoolean(rows.Length == 0);
						}
						else
						{
							Debug.WriteLine("Update Crit: Gbu='" + this.ddlGBU.SelectedValue + "' AND CrossReferenceID<>"+ViewState["CrossReferenceID"].ToString());
							DataRow[] rows = dt.Select("Gbu='" + this.ddlGBU.SelectedValue + "' AND CrossReferenceID<>"+ViewState["CrossReferenceID"].ToString());
							val.IsValid = Convert.ToBoolean(rows.Length == 0);
						}		
					}
				}


			} // END foreach



			if(Page.IsValid)
			{
				SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = myUtility.SqlConn;
			
				Debug.WriteLine("Setting Parameters");
				cmd.Parameters.Add(new SqlParameter("@MaterialID", this.txtMaterialID.Text));
				cmd.Parameters.Add(new SqlParameter("@MaterialDesc", this.txtMaterialDesc.Text));
				cmd.Parameters.Add(new SqlParameter("@Franchise", this.ddlFranchise.SelectedValue));
				cmd.Parameters.Add(new SqlParameter("@Category", this.ddlCategory.SelectedValue));
				cmd.Parameters.Add(new SqlParameter("@Competitor", this.ddlCompetitor.SelectedValue));
				cmd.Parameters.Add(new SqlParameter("@CompetitorID", this.txtCompetitorID.Text));
				cmd.Parameters.Add(new SqlParameter("@CompetitorDesc", this.txtCompetitorDesc.Text));
				cmd.Parameters.Add(new SqlParameter("@Gbu", this.ddlGBU.SelectedValue));

				SqlParameter paramID = new SqlParameter("@CrossReferenceID", SqlDbType.Int);
				cmd.Parameters.Add(paramID);

				if(Int32.Parse(ViewState["CrossReferenceID"].ToString()) == 0)
				{
					cmd.CommandText = "sp_cr_InsertCrossReferenceByID";
					paramID.Direction = ParameterDirection.Output;
				}
				else
				{
					cmd.CommandText = "sp_cr_UpdateCrossReferenceByID";
					paramID.Value = Int32.Parse(ViewState["CrossReferenceID"].ToString());
				}		

				try
				{   
					int rowsInserted = cmd.ExecuteNonQuery();
					this.lblMsg.Text = "The record was successfully updated.";

					if(Int32.Parse(ViewState["CrossReferenceID"].ToString()) == 0)
						ViewState["CrossReferenceID"] = Int32.Parse(paramID.Value.ToString());

					this.LoadData();
				}
				catch(Exception ex)
				{
					this.lblMsg.Text = "An error occurred, the record was NOT successfully updated.";
                    LogException.HandleException(ex, Request);
                    Debug.WriteLine(cmd.CommandText + " Error: " + ex.Message);
					myUtility.SqlConn.Close();
				}
			

			} // end IsValid
			myUtility.SqlConn.Close();

			Session["adminUtility"] = myUtility;
		}// btnUpdate_Click
	}
}
