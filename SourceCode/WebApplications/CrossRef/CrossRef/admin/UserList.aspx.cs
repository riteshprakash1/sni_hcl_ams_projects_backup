// --------------------------------------------------------------
// <copyright file="UserList.aspx.cs" company="Smith and Nephew">
//     Copyright (c) 2016 Smith and Nephew All rights reserved.
// </copyright>
// --------------------------------------------------------------
namespace CrossRef.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CrossRef.Classes;

    /// <summary>This is the User List page</summary>
    public partial class UserList : System.Web.UI.Page
    {

        /// <summary>Handles the Load event of the Page control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("Start Page_Load");

            Debug.WriteLine("Username:" + ADUtility.GetUserNameOfAppUser(Request));

            AppUsersEntity myUser = AppUsersEntity.GetAppUserByUserName(ADUtility.GetUserNameOfAppUser(Request));
            Debug.WriteLine("Display Name:" + myUser.FirstName + " " + myUser.LastName + "; Role: " + myUser.RoleName);
            
            //// if not an admin redirect to Unauthorized Page
            if (!AppUsersEntity.IsAdmin(myUser))
            {
                Response.Redirect("~/Unauthorized.aspx?PageTitle=" + Page.Title);
            }

            if (!Page.IsPostBack)
            {
               this.BindDT();
            }
            Debug.WriteLine("End Page_Load");
        } //// end Page_Load

        /// <summary>Binds the users table to the gvUsers GridView.</summary>
        protected void BindDT()
        {
            //   DataTable dtUsers = SQLUtility.SqlExecuteQuery("sp_GetAdminUsers");
            List<AppUsersEntity> adminUsers = AppUsersEntity.GetAppUsers();

            if (adminUsers.Count == 0)
                adminUsers.Add(new AppUsersEntity());

            this.gvUser.DataSource = adminUsers;
            this.gvUser.DataBind();
        }


        /// <summary>Handles the Add event of the GV control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCancelEditEventArgs"/> instance containing the event data.</param>
        protected void GV_Add(object sender, GridViewCancelEditEventArgs e)
        {
            Page.Validate("Add");

            TextBox txtUserName = (TextBox)this.gvUser.FooterRow.FindControl("txtAddUserName");
            DropDownList ddlRole = (DropDownList)this.gvUser.FooterRow.FindControl("ddlRole");
            CustomValidator vldUserExists = (CustomValidator)this.gvUser.FooterRow.FindControl("vldUserExists");
            CustomValidator vldAddUserInAD = (CustomValidator)this.gvUser.FooterRow.FindControl("vldAddUserInAD");

            AppUsersEntity testUser = AppUsersEntity.GetAppUserByUserName(txtUserName.Text);
            //List<AppUsersEntity> myUsers = AppUsersEntity.GetAppUserByUserName(Utility.CurrentUser(Request));
            vldUserExists.IsValid = Convert.ToBoolean(testUser.UserName == string.Empty);

            ADUserEntity myUser = null;

            if (txtUserName.Text != String.Empty)
            {
                myUser = ADUtility.GetADUserByUserName(txtUserName.Text);
                vldAddUserInAD.IsValid = Convert.ToBoolean(myUser != null);
            }

            if (Page.IsValid)
            {
                AppUsersEntity newUser = new AppUsersEntity();
                newUser.RoleName = ddlRole.SelectedValue;
                newUser.UserName = txtUserName.Text;
                newUser.LastName = myUser.LastName;
                newUser.FirstName = myUser.FirstName;
                newUser.ModifiedBy = ADUtility.GetUserNameOfAppUser(Request);

                AppUsersEntity.InsertAppUser(newUser);

                this.gvUser.EditIndex = -1;
                this.BindDT();
            } //// Page.IsValid

        }  ////end GV_Add

        /// <summary>Handles the RowDataBound event of the gvUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Button btn;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btn = (Button)e.Row.FindControl("btnDelete");
                btn.Attributes.Add("onclick", "return confirm_delete('user');");

                Label lblUserName = (Label)e.Row.FindControl("lblUserName");
                if (lblUserName.Text == string.Empty)
                    e.Row.Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                TextBox txtAddUserName = (TextBox)e.Row.FindControl("txtAddUserName");
                Button btnAdd = (Button)e.Row.FindControl("btnAdd");
                EnterButton.TieButton(txtAddUserName, btnAdd);
            }

        } // end gv_RowDataBound

        /// <summary>Handles the Delete event of the gvUsers GridView control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewDeleteEventArgs"/> instance containing the event data.</param>
        protected void GV_Delete(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            string strUserName = this.gvUser.DataKeys[e.RowIndex]["userName"].ToString();
            AppUsersEntity.DeleteAppUser(strUserName);

            this.gvUser.EditIndex = -1;
            this.BindDT();
        }

    } //// end class
} // END namespace
