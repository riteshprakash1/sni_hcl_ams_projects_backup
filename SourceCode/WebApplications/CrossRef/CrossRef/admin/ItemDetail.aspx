<%@ Page Title="Item Detail" Language="C#" MasterPageFile="~/Site1.Master" Codebehind="ItemDetail.aspx.cs" AutoEventWireup="True" Inherits="CrossRef.admin.ItemDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
				<TABLE style="width:960px; text-align:center">
					<TR>
						<td style="width:20%">S&amp;N Reference Number:</td>
						<td><asp:textbox id="txtMaterialID" runat="server" Width="500px" Font-Size="10pt" MaxLength="20"></asp:textbox>
							<asp:RequiredFieldValidator id="vldRequiredMaterialID" runat="server" ErrorMessage="S&amp;N Reference Number is required."
								ControlToValidate="txtMaterialID">*</asp:RequiredFieldValidator></td>
					</TR>
					<TR>
						<td>S&amp;N Product Description:</td>
						<td><asp:textbox id="txtMaterialDesc" runat="server" Width="500px" Font-Size="10pt" MaxLength="150"></asp:textbox>
							<asp:RequiredFieldValidator id="vldRequiredMaterialDesc" runat="server" ErrorMessage="S&amp;N Product Description is required."
								ControlToValidate="txtMaterialDesc">*</asp:RequiredFieldValidator></td>
					</TR>
					<TR>
						<td>Competitor Reference Number:</td>
						<td><asp:textbox id="txtCompetitorID" runat="server" Width="500px" Font-Size="10pt" MaxLength="40"></asp:textbox>
							<asp:RequiredFieldValidator id="vldRequiredCompetitorReferenceNumber" runat="server" ErrorMessage="Competitor Reference Number is required."
								ControlToValidate="txtCompetitorID">*</asp:RequiredFieldValidator></td>
					</TR>
					<TR>
						<td>Competitor Product Description:</td>
						<td><asp:textbox id="txtCompetitorDesc" runat="server" Width="500px" Font-Size="10pt" MaxLength="150"></asp:textbox>
							<asp:RequiredFieldValidator id="vldRequiredCompetitorProductDescription" runat="server" ErrorMessage="Competitor Product Description is required."
								ControlToValidate="txtCompetitorDesc">*</asp:RequiredFieldValidator></td>
					</TR>
					<TR>
						<td>Competitor Company:</td>
						<td><asp:dropdownlist id="ddlCompetitor" runat="server" Width="200px" Font-Size="10pt"></asp:dropdownlist>
							<asp:RequiredFieldValidator id="vldRequiredCompetitor" runat="server" ErrorMessage="Competitor Company is required."
								ControlToValidate="ddlCompetitor">*</asp:RequiredFieldValidator></td>
					</TR>
					<TR>
						<td>Franchise:</td>
						<td><asp:dropdownlist id="ddlFranchise" runat="server" Width="200px" Font-Size="10pt"></asp:dropdownlist>
							<asp:RequiredFieldValidator id="vldRequiredFranchise" runat="server" ErrorMessage="Franchise is required." ControlToValidate="ddlFranchise">*</asp:RequiredFieldValidator></td>
					</TR>
					<TR>
						<td style="vertical-align:top">Business Unit:</td>
						<td><asp:dropdownlist id="ddlGBU" runat="server" Width="200px" Font-Size="10pt"></asp:dropdownlist>
							<asp:RequiredFieldValidator id="vldRequiredGbu" runat="server" ErrorMessage="Business Unit is required." ControlToValidate="ddlGBU">*</asp:RequiredFieldValidator>
							<asp:CustomValidator id="vldDuplicate" runat="server" ControlToValidate="ddlGBU" ErrorMessage="Duplicate Cross Reference - an item already exists with the same S&N reference number and Competitor reference number.">*</asp:CustomValidator>
						</td>
					</TR>
					<TR>
						<td>Product Category:</td>
						<td><asp:dropdownlist id="ddlCategory" runat="server" Width="200px" Font-Size="10pt"></asp:dropdownlist>
							<asp:RequiredFieldValidator id="vldRequiredCategory" runat="server" ErrorMessage="Product Category is required."
								ControlToValidate="ddlCategory">*</asp:RequiredFieldValidator></td>
					</TR>
					<tr>
						<td colspan="2">
							<asp:ValidationSummary id="ValidationSummary1" ForeColor="#FF7300" Font-Size="10pt" runat="server"></asp:ValidationSummary>
							<asp:Label id="lblMsg" runat="server" ForeColor="#FF7300" Font-Size="10pt"></asp:Label>
						</td>
					</tr>
				</TABLE>
				<TABLE style="width:960px; text-align:center">
					<TR>
						<td style="width:25%; TEXT-ALIGN: center"><asp:button id="btnUpdate" runat="server" Width="100px" Font-Size="10pt" BorderColor="#CCCCCC"
								CausesValidation="False" BackColor="White" Text="Update" Height="22" ForeColor="#FF7300" onclick="btnUpdate_Click"></asp:button></td>
						<td style="width:25%; TEXT-ALIGN: center"><asp:button id="btnDelete" runat="server" Width="100px" Font-Size="10pt" BorderColor="#CCCCCC"
								CausesValidation="False" BackColor="White" Text="Delete" Height="22" ForeColor="#FF7300" onclick="btnDelete_Click"></asp:button>
						</td>
						<td style="width:25%; TEXT-ALIGN: center"><asp:button id="btnSearch" runat="server" Width="100px" Font-Size="10pt" BorderColor="#CCCCCC"
								CausesValidation="False" BackColor="White" Text="Search Page" Height="22" ForeColor="#FF7300" onclick="btnSearch_Click"></asp:button></td>
						<td style="width:25%">&nbsp;</td>
					</TR>
				</TABLE>
</asp:Content>