<%@ Page Title="Admin Search" Language="C#" MasterPageFile="~/Site1.Master" Codebehind="AdminSearch.aspx.cs" AutoEventWireup="True" Inherits="CrossRef.admin.AdminSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
				<DIV id="#SearchBox">
					<TABLE style="width:960px; text-align:center; BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid">
						<TR>
							<td style="FONT-WEIGHT: bold; FONT-SIZE: 12pt" colSpan="2">Search Parameters</td>
							<td style="TEXT-ALIGN: right" colSpan="2"><asp:HyperLink id="lnkHelp" runat="server" NavigateUrl="AdminTutorial.pdf" Target="_blank">Admin. Tutorial</asp:HyperLink></td>
						</TR>
						<TR>
							<td style="vertical-align:top">Business Unit:</td>
							<td><asp:dropdownlist id="ddlGBU" tabIndex="7" runat="server" Font-Size="10pt" Width="200px">
									<asp:ListItem Value="*">All</asp:ListItem>
									<asp:ListItem Value="endoscopy">Endoscopy</asp:ListItem>
									<asp:ListItem Value="orthopaedic">Orthopaedic</asp:ListItem>
								</asp:dropdownlist></td>
							<td style="vertical-align:top">Competitor Company:</td>
							<td style="vertical-align:top"><asp:dropdownlist id="ddlCompetitor" tabIndex="5" runat="server" Font-Size="10pt" Width="200px"></asp:dropdownlist></td>
						</TR>
						<TR>
							<td style="vertical-align:top; width:20%">S&amp;N Reference<BR>
								Number or List: <A class="hintanchor" onmouseover="showhint('Copy Multiple Reference Numbers from a single column of a spread sheet or Type individual reference number entries, hitting the return key after each entry.', this, event, '200px', 0, 30)"
									href="#">[?]</A></td>
							<td  style="vertical-align:top; width:30%"><asp:textbox id="txtMaterialID" tabIndex="1" runat="server" Font-Size="10pt" Width="200px" TextMode="MultiLine"
									Rows="3"></asp:textbox></td>
							<td style="vertical-align:top">Franchise:</td>
							<td style="vertical-align:top"><asp:dropdownlist id="ddlFranchise" tabIndex="6" runat="server" Font-Size="10pt" Width="200px"></asp:dropdownlist></td>
						</TR>
						<TR>
							<td style="vertical-align:top">Competitor Reference<BR>
								Number or List: <A class="hintanchor" onmouseover="showhint('Copy Multiple Reference Numbers from a single column of a spread sheet or Type individual reference number entries, hitting the return key after each entry.', this, event, '200px', 0, 30)"
									href="#">[?]</A>
							</td>
							<td style="vertical-align:top"><asp:textbox id="txtMaterialList" tabIndex="8" runat="server" Font-Size="10pt" Width="200px"
									TextMode="MultiLine" Rows="3"></asp:textbox></td>
							<td style="vertical-align:top; width:17%">Product Category:</td>
							<td style="vertical-align:top"><asp:dropdownlist id="ddlCategory" tabIndex="7" runat="server" Font-Size="10pt" Width="200px"></asp:dropdownlist></td>
						</TR>
						<TR>
							<td style="TEXT-ALIGN: center" colSpan="4">
                                <asp:button id="btnSearch" runat="server" Font-Size="10pt" Width="100px" ForeColor="#FF7300"
									Height="22" Text="Search" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnSearch_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnReset" runat="server" Font-Size="10pt" Width="100px" ForeColor="#FF7300"
									Height="22" Text="Reset" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC" onclick="btnReset_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
						</TR>
					</TABLE>
					<table style="width:960px; text-align:center">
						<tr>
							<td>
								<asp:CustomValidator id="vldCrossRefID" ControlToValidate="txtMaterialList" runat="server" ErrorMessage="This cross reference does not exist in the database.  Click the new record button if you would like to add it."></asp:CustomValidator></td>
						</tr>
					</table>
				</DIV>
				<DIV id="#SearchGrid">
					<TABLE style="width:960px; text-align:center">
						<TR>
							<td style="vertical-align:top; width:37%"><asp:imagebutton id="btnFirst" ImageUrl="../images/First.gif" runat="server" onclick="btnFirst_Click"></asp:imagebutton><asp:imagebutton id="btnPrev" runat="server" ImageUrl="../images/prev.gif" onclick="btnPrev_Click"></asp:imagebutton>&nbsp;&nbsp;
								<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True" onselectedindexchanged="ddlPage_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblPageCnt" runat="server">Label</asp:label>&nbsp;&nbsp;<asp:imagebutton id="btnNext" runat="server" ImageUrl="../images/Next.gif" onclick="btnNext_Click"></asp:imagebutton>
								<asp:imagebutton id="btnLast" runat="server" ImageUrl="../images/Last.gif" ImageAlign="Bottom" onclick="btnLast_Click"></asp:imagebutton></td>
							<td><asp:label id="lblRecordsPerPage" runat="server">Records per page:</asp:label>&nbsp;<asp:dropdownlist id="ddlRecords" runat="server" AutoPostBack="True" onselectedindexchanged="ddlRecords_SelectedIndexChanged">
									<asp:ListItem Value="10">10</asp:ListItem>
									<asp:ListItem Value="25">25</asp:ListItem>
									<asp:ListItem Value="50">50</asp:ListItem>
									<asp:ListItem Value="100">100</asp:ListItem>
								</asp:dropdownlist>
							</td>
							<td>&nbsp;<asp:label id="lblNoRecords" runat="server" Font-Size="10pt" ForeColor="#FF7300">No matching records were found.</asp:label></td>
							<td style="text-align:right; width:25%"><asp:label id="lblRecordCount" runat="server"></asp:label></td>
						</TR>
						<TR>
							<td colSpan="4"><asp:datagrid id="dgSearch" runat="server" Width="100%" CellPadding="5" AllowSorting="True" OnSortCommand="sortHandler"
									AllowPaging="True" PagerStyle-Visible="False" DataKeyField="CrossReferenceID" AutoGenerateColumns="False"
									OnEditCommand="DG_Edit">
									<HeaderStyle CssClass="SearchGrid" Font-Bold="True" BackColor="#CCCCCC"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn ItemStyle-Width="5%" ItemStyle-Wrap="False" ItemStyle-VerticalAlign="Top">
											<ItemTemplate>
												<asp:Button id="btnEdit" Width="50" Height="18" runat="server" Text="Edit" CommandName="Edit"
													Font-Size="8pt" ForeColor="#FF7300" BackColor="White" CausesValidation="False" BorderColor="#CCCCCC"></asp:Button>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="MaterialID" ItemStyle-Font-Size="10pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
											SortExpression="MaterialID" HeaderText="S&amp;N&lt;br&gt;Reference#"></asp:BoundColumn>
										<asp:BoundColumn DataField="MaterialDesc" ItemStyle-Font-Size="10pt" HeaderStyle-Width="22%" HeaderStyle-HorizontalAlign="left"
											SortExpression="MaterialDesc" HeaderText="S&amp;N<br>Product Description"></asp:BoundColumn>
										<asp:BoundColumn DataField="CompetitorID" ItemStyle-Font-Size="10pt" SortExpression="CompetitorID"
											HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" HeaderText="Competitor&lt;br&gt;Reference#"></asp:BoundColumn>
										<asp:BoundColumn DataField="CompetitorDesc" ItemStyle-Font-Size="10pt" HeaderStyle-Width="23%" HeaderStyle-HorizontalAlign="left"
											SortExpression="CompetitorDesc" HeaderText="Competitor<br>Product Description"></asp:BoundColumn>
										<asp:BoundColumn DataField="Competitor" ItemStyle-Font-Size="10pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Competitor" HeaderText="Competitor<br>Company"></asp:BoundColumn>
										<asp:BoundColumn DataField="Franchise" ItemStyle-Font-Size="10pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Franchise" HeaderText="Franchise"></asp:BoundColumn>
										<asp:BoundColumn DataField="Category" ItemStyle-Font-Size="10pt" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Category" HeaderText="Product&lt;br&gt;Category"></asp:BoundColumn>
										<asp:BoundColumn DataField="Gbu" ItemStyle-Font-Size="10pt" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="left"
											SortExpression="Gbu" HeaderText="Business&lt;br&gt;Unit"></asp:BoundColumn>
									</Columns>
									<PagerStyle Visible="False"></PagerStyle>
								</asp:datagrid>
							</td>
							<asp:Literal id="Literal1" runat="server"></asp:Literal>
                        </TR>
					</TABLE>
			</div>
</asp:Content>