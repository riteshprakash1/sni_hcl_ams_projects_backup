using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Diagnostics;
using System.Data.SqlClient;
using CrossRef.Classes;

namespace CrossRef.admin
{
	/// <summary>
	/// Summary description for AdminSearch.
	/// </summary>
	public partial class AdminSearch : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtCompetitorID;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.Label lblMissingMaterial;
		protected System.Web.UI.WebControls.Button btnAdminSearch;
		protected System.Web.UI.WebControls.Button btnUserList;
		protected Utility myUtility;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["adminUtility"]!=null)
				myUtility = (Utility)Session["adminUtility"];
			else
			{
				myUtility = new Utility(Request);
				Session["adminUtility"] = myUtility;
			}

			if(!Page.IsPostBack)
			{
				this.ddlCategory.Items.Clear();
				ddlCategory.Items.Add(new ListItem("-- Select --","*"));
				foreach(DataRow r in myUtility.TblCategory.Rows)
				{
					this.ddlCategory.Items.Add(new ListItem(r["Category"].ToString(),r["Category"].ToString() ));
				}

				this.ddlCompetitor.Items.Clear();
				ddlCompetitor.Items.Add(new ListItem("-- Select --","*"));
				foreach(DataRow r in myUtility.TblCompetitor.Rows)
				{
					this.ddlCompetitor.Items.Add(new ListItem(r["Competitor"].ToString(),r["Competitor"].ToString() ));
				}

				this.ddlFranchise.Items.Clear();
				ddlFranchise.Items.Add(new ListItem("-- Select --","*"));
				foreach(DataRow r in myUtility.TblFranchise.Rows)
				{
					this.ddlFranchise.Items.Add(new ListItem(r["Franchise"].ToString(),r["Franchise"].ToString() ));
				}

                if (this.ddlGBU.Items.FindByValue(ConfigurationManager.AppSettings["defaultGBU"].ToString().ToLower()) != null)
                    ddlGBU.Items.FindByValue(ConfigurationManager.AppSettings["defaultGBU"].ToString().ToLower()).Selected = true;

				this.SetControls();
				
			}
			Literal1.Text = "";

		}

		public void DG_Edit(Object Sender, DataGridCommandEventArgs e)
		{
			Page.Validate();
			System.Web.UI.ValidatorCollection vals = Page.Validators;

			foreach(IValidator val in vals)
			{
				if(val.ErrorMessage.StartsWith("This cross reference does not exist"))
				{
					Debug.WriteLine("CrossReferenceID: " + dgSearch.DataKeys[e.Item.ItemIndex].ToString());
					val.IsValid = Convert.ToBoolean(dgSearch.DataKeys[e.Item.ItemIndex].ToString()!="");
				}


			} // END foreach

			if(Page.IsValid)
			{
				myUtility.CrossReferenceID = Int32.Parse(dgSearch.DataKeys[e.Item.ItemIndex].ToString());
				Debug.WriteLine("ID: " + myUtility.CrossReferenceID.ToString());
				Session["adminUtility"] = myUtility;
				Response.Redirect("ItemDetail.aspx");
			}
		}


		public void BindDT()
		{
			if(myUtility.TblSearch == null)
				myUtility.TblSearch = this.DoSearch();
			
			try
			{
				dgSearch.PageSize = Int32.Parse(this.ddlRecords.SelectedValue);
				dgSearch.DataSource = myUtility.TblSearch;
				dgSearch.DataBind();
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Bind Error: " + ex.Message);
				dgSearch.CurrentPageIndex = 0;
			}
			
			this.SetControls();

			if(dgSearch.CurrentPageIndex != 0)
			{
				btnFirst.ImageUrl = "images/first.gif";
				btnPrev.ImageUrl = "images/prev.gif";
			}
			else
			{
				btnFirst.ImageUrl = "images/firstd.gif";
				btnPrev.ImageUrl = "images/prevd.gif";
			}

			if(dgSearch.CurrentPageIndex != (dgSearch.PageCount-1))
			{
				btnLast.ImageUrl = "images/last.gif";
				btnNext.ImageUrl = "images/next.gif";
			}
			else
			{
				btnLast.ImageUrl = "images/lastd.gif";
				btnNext.ImageUrl = "images/nextd.gif";
			}
			lblRecordCount.Text = "<b><font color=#FF7300>" + Convert.ToString(myUtility.TblSearch.Rows.Count) + "</font> records found";

			lblPageCnt.Text = " of " + dgSearch.PageCount.ToString();
			ddlPage.Items.Clear();
			
			Debug.WriteLine("PageCount = " + dgSearch.PageCount.ToString());
			for(int x=1;x<dgSearch.PageCount+1;x++)
			{
				ddlPage.Items.Add(new ListItem(x.ToString()));
			}
			int myPage=dgSearch.CurrentPageIndex+1;
			
			ddlPage.Items.FindByValue(myPage.ToString()).Selected=true;

			Session["adminUtility"] = myUtility;	
		} // END BindDT()

		public void sortHandler(object sender, DataGridSortCommandEventArgs e)
		{
			if(ViewState["OrderBy"].ToString() == e.SortExpression)
			{
				if(ViewState["SortDirection"].ToString()  == "asc")
					ViewState["SortDirection"] = "desc";
				else
					ViewState["SortDirection"] = "asc";
			}
			else
				ViewState["SortDirection"] = "asc";

			ViewState["OrderBy"] = e.SortExpression;
			//	dt = (DataTable)Session["dt"];

			if(myUtility.TblSearch==null)
				myUtility.TblSearch = this.DoSearch();
			else
			{
				string strSort = ViewState["OrderBy"].ToString() + " " + ViewState["SortDirection"].ToString();
				Debug.WriteLine("strSort: " + strSort);
				DataRow[] rows = myUtility.TblSearch.Select(null, strSort);

				myUtility.TblSearch = myUtility.TblSearch.Clone();
				foreach (DataRow row in rows) 
				{
					myUtility.TblSearch.ImportRow(row);
				}
			}
			this.BindDT();
			Session["adminUtility"] = myUtility;	
		} // END sortHandler

		protected void btnFirst_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgSearch.CurrentPageIndex = 0;
			this.BindDT();
		} // END btnFirst_Click

		protected void btnPrev_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgSearch.CurrentPageIndex >= 1)
				dgSearch.CurrentPageIndex = dgSearch.CurrentPageIndex - 1;
			else
				dgSearch.CurrentPageIndex = 0;

			this.BindDT();
		}// END btnPrev_Click

		protected void btnNext_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(dgSearch.CurrentPageIndex != (dgSearch.PageCount-1))
				dgSearch.CurrentPageIndex = dgSearch.CurrentPageIndex + 1;
			else
				dgSearch.CurrentPageIndex = (dgSearch.PageCount-1);

			this.BindDT();
		}// END btnNext_Click

		protected void btnLast_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			dgSearch.CurrentPageIndex = (dgSearch.PageCount-1);
			this.BindDT();
		}// END btnLast_Click

		protected void ddlPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgSearch.CurrentPageIndex = ddlPage.SelectedIndex;
			this.BindDT();
		}

		public void SetControls()
		{
			if(myUtility.TblSearch==null || myUtility.TblSearch.Rows.Count==0)
			{
				dgSearch.Visible=false;
				btnFirst.Visible=false;
				btnPrev.Visible=false;
				ddlPage.Visible=false;
				lblPageCnt.Visible=false;;
				btnNext.Visible=false;
				btnLast.Visible=false;
				lblRecordsPerPage.Visible=false;
				this.ddlRecords.Visible=false;
				lblRecordCount.Visible=false;
				if(!Page.IsPostBack)
					lblNoRecords.Visible=false;
				else
					lblNoRecords.Visible=true;

			}
			else
			{
				dgSearch.Visible=true;
				btnFirst.Visible=true;
				btnPrev.Visible=true;
				ddlPage.Visible=true;
				lblPageCnt.Visible=true;;
				btnNext.Visible=true;
				btnLast.Visible=true;
				lblRecordCount.Visible=true;
				lblNoRecords.Visible=false;
				lblRecordsPerPage.Visible=true;
				this.ddlRecords.Visible=true;
				lblRecordCount.Visible=true;
			} // END if
		} // END SetControls()

		private DataTable DoSearch()
		{
			string strSearchQuery = "SELECT CrossReferenceID, MaterialID, MaterialDesc, Franchise, Category, Competitor, Gbu, ";
			strSearchQuery  += "CompetitorID, CompetitorDesc FROM cr_CrossReference ";

			string strWhere = "WHERE MaterialID<>'zzzzzzzzzzz' ";

			if(ViewState["Category"].ToString() != "*")
				strWhere += " AND Category =  '" + ViewState["Category"].ToString() + "' ";

			if(ViewState["Competitor"].ToString() != "*")
				strWhere += " AND Competitor =  '" + ViewState["Competitor"].ToString() + "' ";

			if(ViewState["GBU"].ToString() != "*")
				strWhere += " AND Gbu = '" + ViewState["GBU"].ToString() + "' ";

			if(ViewState["MaterialID"].ToString() != "")
			{
				ArrayList snList = myUtility.SetMaterialList(ViewState["MaterialID"].ToString());
				string strInClause = myUtility.SetMaterialListSearch(snList, "MaterialID");
				strWhere += " AND (" + myUtility.SetMaterialListSearch(snList, "MaterialID");
				if(strInClause == "")
					strWhere +=  myUtility.SetWildCardList(snList, "MaterialID", "") + " ";
				else
					strWhere +=  myUtility.SetWildCardList(snList, "MaterialID", " OR ") + " ";
				strWhere += ") ";
			}

			if(ViewState["CompetitorID"].ToString() != "")
			{
				ArrayList CompList = myUtility.SetMaterialList(ViewState["CompetitorID"].ToString());
				string strInClause = myUtility.SetMaterialListSearch(CompList, "CompetitorID");
				strWhere += " AND (" + myUtility.SetMaterialListSearch(CompList, "CompetitorID");
				if(strInClause == "")
					strWhere +=  myUtility.SetWildCardList(CompList, "CompetitorID", "") + " ";
				else
					strWhere +=  myUtility.SetWildCardList(CompList, "CompetitorID", " OR ") + " ";
				strWhere += ") ";
			}

			if(ViewState["Franchise"].ToString() != "*")
				strWhere += " AND Franchise =  '" + ViewState["Franchise"].ToString() + "' ";
			
			string strSort = " ORDER BY " + ViewState["OrderBy"].ToString();
			strSearchQuery = strSearchQuery + strWhere + strSort;

			Debug.WriteLine("Query = " + strSearchQuery);
			DataTable dt = new DataTable();

			if(myUtility.SqlConn.State != ConnectionState.Open)
				myUtility.SqlConn.Open();

			SqlCommand cmdSearch = new System.Data.SqlClient.SqlCommand();
			cmdSearch.CommandText = strSearchQuery;
			cmdSearch.Connection = myUtility.SqlConn;

			try
			{
				SqlDataAdapter da = new SqlDataAdapter(cmdSearch);
				da.Fill(dt);
				dt = myUtility.AddItemsNotFound(dt, ViewState["MaterialID"].ToString(), "MaterialID", "CompetitorID");
				dt = myUtility.AddItemsNotFound(dt, ViewState["CompetitorID"].ToString(), "CompetitorID", "MaterialID");
			
				dt = myUtility.SortTable(dt, ViewState["OrderBy"].ToString(), ViewState["SortDirection"].ToString());
			}
			catch(Exception ex)
			{
                LogException.HandleException(ex, Request);
            }

			myUtility.SqlConn.Close();

			return dt;
			//			myUtility.TblSearch = dt;
		}


		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			Debug.WriteLine("btnSearch_Click");
			Page.Validate();

			if(Page.IsValid)
			{
				this.SetViewState();
				dgSearch.CurrentPageIndex = 0;
				myUtility.TblSearch = this.DoSearch();
				this.BindDT();
				this.SetControls();

				Debug.WriteLine("Rows = " + myUtility.TblSearch.Rows.Count.ToString());
				Session["adminUtility"] = myUtility;	
			}
		}

		private void SetViewState()
		{
			ViewState["Franchise"] = this.ddlFranchise.SelectedValue;
			ViewState["Competitor"] = this.ddlCompetitor.SelectedValue;
			ViewState["Category"] = this.ddlCategory.SelectedValue;
			ViewState["MaterialID"] = this.txtMaterialID.Text;
			ViewState["CompetitorID"] = this.txtMaterialList.Text;
			ViewState["GBU"] = this.ddlGBU.SelectedValue;
			ViewState["OrderBy"] = "MaterialID ";
			ViewState["SortDirection"] = "desc";
		}

		protected void btnReset_Click(object sender, System.EventArgs e)
		{
			myUtility.TblSearch = null;
			Session["adminUtility"] = myUtility;	
			Response.Redirect("AdminSearch.aspx");
		}

		protected void ddlRecords_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgSearch.CurrentPageIndex = 0;
			this.BindDT();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion






	}// end class
}
