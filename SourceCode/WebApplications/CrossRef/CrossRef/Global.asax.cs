using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using CrossRef.Classes;

namespace CrossRef 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{

		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

        /// <summary>Handles the Error event of the Application control.</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = null;
            string authUser = Request.LogonUserIdentity.Name.ToString();
            string strErrorMsg = string.Empty;
            string strHtmlError = string.Empty;

            try
            {
                ex = Server.GetLastError().GetBaseException();
                //System.Diagnostics.Debug.WriteLine("Application Error: " + ex.Message + "; Inner: " + ex.ToString());
                Debug.WriteLine("Application Error: " + ex.Message);
                strErrorMsg = ex.Message;


                string strPotentiallyDangerous = "A potentially dangerous Request.Form value";
                if (strErrorMsg.ToLower().StartsWith(strPotentiallyDangerous.ToLower()))
                {
                    strHtmlError = strPotentiallyDangerous.ToLower();
                }
                else

                    strHtmlError = ex.Message.Replace("<", "&lt>;").Replace(">", "&gt;");

                LogException.HandleException(ex, Request);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine("ExceptionHandler Error: " + exc.Message);
            }

            Server.ClearError();

            string strReferringPage = Request.Url.ToString().ToLower();
            Debug.WriteLine("Global.ascx strErrorReferringPage: " + strReferringPage);
            string strErrorPage = string.Empty;

            if (!strReferringPage.Contains("errorpage"))
            {
                try
                {
                    strErrorPage = ConfigurationManager.AppSettings["ErrorPage"];
                    Response.Redirect(strErrorPage + "?ErrorMsg=" + strHtmlError, false);
                }
                catch (Exception exc)
                {
                    Debug.WriteLine("Redirect Error: " + exc.Message);
                    Response.Redirect(strErrorPage, false);
                }
            }
            //string strErrorPage = ConfigurationManager.AppSettings["ErrorPage"];
            //Response.Redirect(strErrorPage + "?ErrorMsg=" + strHtmlError, false);
        }

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

