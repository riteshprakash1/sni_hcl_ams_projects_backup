USE [VEI_LV]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Truncate_Table_Stage_Work_Order_Data]
/******************************************************************************/
/* Parameters: @Year_month  INT                                               */
/* Description: This Stored Procedure truncate the Work_Order_Staging Table   */
/*                                                                            */
/* Programmer:        [ Andy Eggers]                                          */
/* Date Written:      [ 8/22/2017 ]                                           */
/* Change History:    [ Enter Date and why the change was necessary Here]     */
/*                                                                            */
/* Usage: EXEC  USP_Truncate_Work_Order_Staging_Table                         */
/* ****************************************************************************/

AS
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON

BEGIN TRY 
	BEGIN TRANSACTION
	/**    ADD Your Code Here   **/
		TRUNCATE TABLE [dbo].[Stage_Work_Order_Data] ;
		
		IF @@TRANCOUNT > 0
			BEGIN
				COMMIT TRANSACTION;
			END	
    
END TRY 

/* Catching any Errors and Rolling back the Transaction */
BEGIN CATCH
	DECLARE @ErrorMessage   NVARCHAR(4000);
	DECLARE @ErrorSeverity  INT           ;
	DECLARE @ErrorState     INT           ;

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE();

	SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState
	  , ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;

	RAISERROR (@ErrorMessage  ,    /* Error text Message.     */
			   @ErrorSeverity ,    /* Error Severity number . */
			   @ErrorState   );    /* Error State.            */

	IF @@TRANCOUNT > 0
	   BEGIN
		   ROLLBACK TRANSACTION ;
	   END
	    
END CATCH ;

GO


