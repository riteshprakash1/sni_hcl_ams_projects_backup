USE [VEI_LV]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_INS_Catalog_Item_Master]
/******************************************************************************/
/* Parameters: None                                                           */
/* Description: This Stored Procedure Checks to make sure the are item to     */
/*              load for the Catalog Item Master Table                        */
/*                                                                            */
/* Programmer:        Andy Eggers                                             */
/* Date Written:      9/6/2017                                                */
/* Change History:    [ Enter Date and why the change was necessary Here]     */
/*                                                                            */
/* Usage: EXEC  USP_INS_Catalog_Item_Master                                   */
/* ****************************************************************************/
/* 2. Process the incoming parameters                                         */

AS
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON

/* Checking to See if we retreived records from Prisym */
DECLARE @Record_Counter INT = 0
SET @Record_Counter = (SELECT COUNT(*) FROM dbo.Stage_Catalog_Item_Master)

BEGIN TRY 
	BEGIN TRANSACTION
	    IF @Record_Counter > 0  /* We have records to insert  */
			TRUNCATE TABLE [VEI_LV].[dbo].[Catalog_Item_Master] 
			/* Load the Data from the Staging Table  */
			INSERT INTO [VEI_LV].[dbo].[Catalog_Item_Master](Catalog_Item, OT1_Required, OT1_Ratio
			, OT2_Required, OT2_Ratio, IN1_Required, IN1_Ratio, IN2_Required, IN2_Ratio, CS1_Required
			, CS1_Ratio, CS2_Required, CS2_Ratio, MSC1_Required, MSC1_Ratio, MSC2_Required, MSC2_Ratio)
				SELECT RTRIM(LTRIM(Catalog_Item)) AS Catalog_Item
				, ISNULL(OT1_Required,0)   AS  OT1_Required
				, RTRIM(LTRIM(OT1_Ratio))  AS OT1_Ratio
				, ISNULL(OT2_Required,0)   AS OT2_Required
				, RTRIM(LTRIM(OT2_Ratio))  AS OT2_Ratio
				, ISNULL(IN1_Required,0)   AS IN1_Required
				, RTRIM(LTRIM(IN1_Ratio))  AS IN1_Ratio
				, ISNULL(IN2_Required,0)   AS IN2_Required
				, RTRIM(LTRIM(IN2_Ratio))  AS IN2_Ratio
				, ISNULL(CS1_Required,0)   AS CS1_Required
				, RTRIM(LTRIM(CS1_Ratio))  AS CS1_Ratio
				, ISNULL(CS2_Required,0)   AS CS2_Required
				, RTRIM(LTRIM(CS2_Ratio))  AS CS2_Ratio
				, ISNULL(MSC1_Required,0)  AS MSC1_Required
				, RTRIM(LTRIM(MSC1_Ratio)) AS MSC1_Ratio
				, ISNULL(MSC2_Required,0)  AS MSC2_Required
				, RTRIM(LTRIM(MSC2_Ratio)) AS MSC2_Ratio
				FROM [dbo].[Stage_Catalog_Item_Master]
				ORDER BY RTRIM(LTRIM(Catalog_Item))
		
		-- Else 
			  /*  We did not return any records from Production Prisym, so do not empty the Catalog_Item_Master table */
	
	
		IF @@TRANCOUNT > 0
			BEGIN
				COMMIT TRANSACTION;
			END	

END TRY 

/* Catching any Errors and Rolling back the Transaction */

BEGIN CATCH
	DECLARE @ErrorMessage   NVARCHAR(4000);
	DECLARE @ErrorSeverity  INT           ;
	DECLARE @ErrorState     INT           ;

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE();

	SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState
	  , ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;

	RAISERROR (@ErrorMessage  ,    /* Error text Message.     */
			   @ErrorSeverity ,    /* Error Severity number . */
			   @ErrorState   );    /* Error State.            */

	IF @@TRANCOUNT > 0
	   BEGIN
		   ROLLBACK TRANSACTION ;
	   END
	    
END CATCH ;


GO