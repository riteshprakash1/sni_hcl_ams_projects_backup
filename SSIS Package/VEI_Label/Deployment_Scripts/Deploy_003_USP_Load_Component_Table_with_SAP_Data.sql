USE [VEI_LV]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Load_Component_Table_with_SAP_Data] 
/*******************************************************************************/
/* Parameters: None                                                            */
/* Description: This Stored Procedure takes the imported SAP Work Orders from  */
/*             The Stage_Work_Order_Data table and converts the text data to   */
/*             fill the fields in the Component table.                         */
/*   Note: this is step one of the Import process (SAP Work Order Files) and   */
/*         and occurs every hour of the day. 24 hours a day x 7 days a week x  */
/*         365 days a year.                                                    */
/*                                                                             */
/* The following assumptions apply                                             */
/* 1. There are zero duplicate records received from SAP, However we are going */
/*    to check. the Default Value = 0 for the [Validation] Field               */
/*                                                                             */
/* 2. The value of 1 in the [VEI_Order_Initiated] field means the VEI has      */
/*    started their process so we do not touch this record. As a Matter fact   */
/*    we will delete these records from the staging table to make sure we do   */
/*    not touch them before any updates or adds occur.                         */
/*                                                                             */
/* 3. Updating Existing Records with the new values  [Validation] = 2          */
/*    This means the Work Order, Batch Number and Catalog Number already Exist */
/*    in the VEI_LV Component Table so I need to determine is one of the IFU's */
/*    has changed or has been Updated.                                         */                  
/*                                                                             */
/* 4. Finding exact matched and mark for deletion  [Validation] = 3            */
/*                                                                             */
/* Programmer:        Andy Eggers                                              */
/* Date Written:      8/22/2017                                                */
/* Change History:    [ Enter Date and why the change was necessary Here]      */
/*                                                                             */
/* Usage: EXEC USP_Load_Component_Table_with_SAP_Data                          */
/* *****************************************************************************/
/* 2. Process the incoming parameters                                          */
 (@FileName varchar(200) = NULL )     

AS
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON

BEGIN TRY 
	BEGIN TRANSACTION
		/**    ADD Your Code Here   **/
		/* Check for Existing Records in the Component Table */
		CREATE TABLE #Temp_Existing(
		[TE_ID]               [int] IDENTITY(1,1) NOT NULL,
		[Work_Order]          [varchar](32) NULL,
		[Batch_Number]        [varchar](32) NULL,
		[Catalog_Item]        [varchar](32) NULL,
		[IFU_1]               [varchar](10) NULL,
		[IFU_2]               [varchar](10) NULL,
		[IFU_3]               [varchar](10) NULL,
		[IFU_4]               [varchar](10) NULL,
		[IFU_5]               [varchar](10) NULL,
		[VEI_Order_Initiated] [int]         NULL, 
		[Validation]          [int] DEFAULT (0)  NULL)

		/* Creating Non-Cluster index  */
		CREATE NONCLUSTERED INDEX NCI_TMP_Existing ON  #Temp_Existing([Work_Order],[Batch_Number] ,[Catalog_Item]) 
		INCLUDE([IFU_1], [IFU_2], [IFU_3], [IFU_4], [IFU_5], [VEI_Order_Initiated], [Validation])

		/* Filling the table with Matching records information */
		INSERT INTO #Temp_Existing([Work_Order], [Batch_Number], [Catalog_Item],[IFU_1],[IFU_2], [IFU_3], [IFU_4], [IFU_5],	[VEI_Order_Initiated], [Validation])
		SELECT SUBSTRING(RTRIM(LTRIM(S.[ORDER_NUMBER])),1,32) AS Work_Order
		 , SUBSTRING(RTRIM(LTRIM(S.[BATCH])),1,32)            AS Batch_Number
		 , SUBSTRING(RTRIM(LTRIM(S.[ITEM_NUMBER])),1,32)      AS [Catalog_Item]
		 , SUBSTRING(RTRIM(LTRIM(S.[IFU_1])),1,10)             AS [IFU_1]
		 , SUBSTRING(RTRIM(LTRIM(S.[IFU_2])),1,10)             AS [IFU_2]
		 , SUBSTRING(RTRIM(LTRIM(S.[IFU_3])),1,10)             AS [IFU_3]
		 , SUBSTRING(RTRIM(LTRIM(S.[IFU_4])),1,10)             AS [IFU_4]
		 , SUBSTRING(RTRIM(LTRIM(S.[IFU_5])),1,10)             AS [IFU_5]
		 , C.[VEI_Order_Initiated] 
		 , S.[Validation]
		  FROM [dbo].[Stage_Work_Order_Data] AS S INNER JOIN [dbo].[Component] AS C
		   ON SUBSTRING(RTRIM(LTRIM(S.[ORDER_NUMBER])),1,32) = C.Work_Order
		  AND SUBSTRING(RTRIM(LTRIM(S.[ITEM_NUMBER])),1,32)  = C.[Catalog_Item]
		  -- AND SUBSTRING(RTRIM(LTRIM(S.[BATCH])),1,32)        = C.Batch_Number  /* This line has been commented out due to Batch numbers can cheange */
		  ORDER BY SUBSTRING(RTRIM(LTRIM(S.[ORDER_NUMBER])),1,32), SUBSTRING(RTRIM(LTRIM(S.[BATCH])),1,32)
		  , SUBSTRING(RTRIM(LTRIM(S.[ITEM_NUMBER])),1,32)

		 /*  Work Order which are already in the VEO_Order_Initiated = 1  State - meaning VEI has already started or has comleted Verification on these work orders. So as agreed we will not u[pdate this Work Orders.  */
	 	 UPDATE #Temp_Existing
		 SET [Validation] = 1 
		 WHERE [VEI_Order_Initiated] = 1
		 
		 /* Updating Existing Records with the new values  [Validation] = 2  */
		 UPDATE #Temp_Existing
		 SET [Validation] = 2
		 FROM #Temp_Existing AS T INNER JOIN [dbo].[Component] AS C
		   ON T.[Work_Order]   = C.Work_Order
		  AND T.[Catalog_Item] = C.[Catalog_Item]
		  --AND T.[Batch_Number] = C.Batch_Number
		  WHERE (T.[VEI_Order_Initiated] = 0 AND T.[Batch_Number] <> C.Batch_Number)
		     OR (T.[VEI_Order_Initiated] = 0 AND T.[IFU_1] <> C.[IFU_1])
		     OR (T.[VEI_Order_Initiated] = 0 AND T.[IFU_2] <> C.[IFU_2])
			 OR (T.[VEI_Order_Initiated] = 0 AND T.[IFU_3] <> C.[IFU_3])
			 OR (T.[VEI_Order_Initiated] = 0 AND T.[IFU_4] <> C.[IFU_4])
			 OR (T.[VEI_Order_Initiated] = 0 AND T.[IFU_5] <> C.[IFU_5])
         
		 /* Finding exact matched and mark for do nothing  [Validation] = 3 */
		 UPDATE #Temp_Existing
	 	 SET   [Validation] = 3
		 WHERE [Validation] = 0

		 /* Updating the [Stage_Work_Order_Data] table with the information we Found */ 
		 UPDATE [dbo].[Stage_Work_Order_Data]
		 SET [Validation] = E.[Validation]
		 FROM [dbo].[Stage_Work_Order_Data] AS S INNER JOIN #Temp_Existing AS E
		   ON SUBSTRING(RTRIM(LTRIM(S.[ORDER_NUMBER])),1,32) = E.Work_Order
		  AND SUBSTRING(RTRIM(LTRIM(S.[BATCH])),1,32)        = E.Batch_Number
		  AND SUBSTRING(RTRIM(LTRIM(S.[ITEM_NUMBER])),1,32)  = E.[Catalog_Item]

		  /* Updating the Required and Ratio Fields from the Catalog_Item_Master table */
		  UPDATE [dbo].[Stage_Work_Order_Data]
		  SET OT1_Required  = CIM.OT1_Required     ,
		      OT1_Ratio     = CIM.OT1_Ratio        , 
		      OT2_Required  = CIM.OT2_Required     , 
		      OT2_Ratio     = CIM.OT2_Ratio        , 
		      IN1_Required  = CIM.IN1_Required     ,
		      IN1_Ratio     = CIM.IN1_Ratio        , 
			  IN2_Required  = CIM.IN2_Required     , 
			  IN2_Ratio     = CIM.IN2_Ratio        , 
			  CS1_Required  = CIM.CS1_Required     , 
		      CS1_Ratio     = CIM.CS1_Ratio        , 
			  CS2_Required  = CIM.CS2_Required     , 
			  CS2_Ratio     = CIM.CS2_Ratio        , 
			  MSC1_Required = CIM.MSC1_Required    , 
			  MSC1_Ratio    = CIM.MSC1_Ratio       ,
			  MSC2_Required = CIM.MSC2_Required    , 
			  MSC2_Ratio    = CIM.MSC2_Ratio      
			 FROM dbo.Stage_Work_Order_Data AS SWO INNER JOIN dbo.Catalog_Item_Master AS CIM
			    ON RTRIM(LTRIM(SWO.ITEM_NUMBER)) = RTRIM(LTRIM(CIM.Catalog_Item))
			 
		  /* Collecting Tracking Data */
		  DECLARE @Total_Number_of_Records_in_File  INT = 0
		        , @Work_Order_Insert_Count          INT = 0
		        , @VEI_Verification_Count           INT = 0
				, @Work_Order_Updated_Count         INT = 0
				, @Duplicate_Record_Count           INT = 0
	   
	      SET @Total_Number_of_Records_in_File = (SELECT COUNT(*) FROM [dbo].[Stage_Work_Order_Data])                    /* Total Number of Records in the File for Processing  */
		  SET @Work_Order_Insert_Count  = (SELECT COUNT(*) FROM [dbo].[Stage_Work_Order_Data] WHERE [Validation] = 0 )  /* Default Value */
		  SET @VEI_Verification_Count   = (SELECT COUNT(*) FROM [dbo].[Stage_Work_Order_Data] WHERE [Validation] = 1 )  /* VEI has already started or has completed Verification */
		  SET @Work_Order_Updated_Count = (SELECT COUNT(*) FROM [dbo].[Stage_Work_Order_Data] WHERE [Validation] = 2 )  /* Updates for the IFU Fields */ 
		  SET @Duplicate_Record_Count   = (SELECT COUNT(*) FROM [dbo].[Stage_Work_Order_Data] WHERE [Validation] = 3 )  /* True Duplicate Records */

		  /* Load The Data Tracking Table with the Processing information */
		  EXEC USP_Load_Data_Load_Tracking_Table 'SAP_Work_Order', @FileName, @Total_Number_of_Records_in_File, @Work_Order_Insert_Count, @VEI_Verification_Count , @Work_Order_Updated_Count , @Duplicate_Record_Count
		  		 		  
		  /* Delete Records where VEI has already started or has completed the Verification on */ 
		  DELETE FROM [dbo].[Stage_Work_Order_Data]
		  WHERE [Validation] = 1      /* Verification Has Started or Completed. We Do not Touch these records. */

		  /* Inserting New SAP Work Order Records into the Component Table  */
		 INSERT INTO dbo.Component (Work_Order, Batch_Number, Catalog_Item, OT1_Required, OT1_Ratio, OT2_Required, OT2_Ratio, IN1_Required, IN1_Ratio, IN2_Required, IN2_Ratio, CS1_Required, CS1_Ratio, CS2_Required, CS2_Ratio, MSC1_Required,
		   MSC1_Ratio, MSC2_Required, MSC2_Ratio, IFU_1, IFU_2, IFU_3, IFU_4, IFU_5 ) 
		 SELECT DISTINCT SUBSTRING(RTRIM(LTRIM([ORDER_NUMBER])),1,32) AS Work_Order
		 , SUBSTRING(RTRIM(LTRIM([BATCH])),1,32)             AS Batch_Number
		 , SUBSTRING(RTRIM(LTRIM([ITEM_NUMBER])),1,32)       AS [Catalog_Item]
         , [OT1_Required] , [OT1_Ratio]
		 , [OT2_Required] , [OT2_Ratio]
		 , [IN1_Required] , [IN1_Ratio]
		 , [IN2_Required] , [IN2_Ratio]
		 , [CS1_Required] , [CS1_Ratio]
		 , [CS2_Required] , [CS2_Ratio]
		 , [MSC1_Required], [MSC1_Ratio]
		 , [MSC2_Required], [MSC2_Ratio]
		 , SUBSTRING(RTRIM(LTRIM([IFU_1])),1,10)             AS [IFU_1]
		 , SUBSTRING(RTRIM(LTRIM([IFU_2])),1,10)              AS [IFU_2]
		 , SUBSTRING(RTRIM(LTRIM([IFU_3])),1,10)              AS [IFU_3]
		 , SUBSTRING(RTRIM(LTRIM([IFU_4])),1,10)              AS [IFU_4]
		 , SUBSTRING(RTRIM(LTRIM([IFU_5])),1,10)              AS [IFU_5]
		 FROM [dbo].[Stage_Work_Order_Data] 
		 WHERE [Validation] = 0
		
		 /* Updating Existing Records in the Component Table with the new IFU's values from SAP */
		 UPDATE [dbo].[Component]
		 SET [Batch_Number] = SUBSTRING(RTRIM(LTRIM(S.[BATCH])),1,32)    ,
		     [IFU_1]        = SUBSTRING(RTRIM(LTRIM(S.[IFU_1])),1,10)     ,
		     [IFU_2]        = SUBSTRING(RTRIM(LTRIM(S.[IFU_2])),1,10)     ,
			 [IFU_3]        = SUBSTRING(RTRIM(LTRIM(S.[IFU_3])),1,10)     ,
			 [IFU_4]        = SUBSTRING(RTRIM(LTRIM(S.[IFU_4])),1,10)     ,
			 [IFU_5]        = SUBSTRING(RTRIM(LTRIM(S.[IFU_5])),1,10)
		FROM [dbo].[Component] AS C INNER JOIN [dbo].[Stage_Work_Order_Data] AS S
		    ON C.Work_Order     = SUBSTRING(RTRIM(LTRIM(S.[ORDER_NUMBER])),1,32)
		   AND C.[Catalog_Item] = SUBSTRING(RTRIM(LTRIM(S.[ITEM_NUMBER])),1,32)
		   AND S.[Validation]   = 2 
		
		/* What to Do with Exact Matching ("True Duplicate") Records */
		/* Do Nothing with Stage_Work_Order_Data.dbo.Validation  = 3 records  */

		IF @@TRANCOUNT > 0
			BEGIN
				COMMIT TRANSACTION;
			END	
    
END TRY 

/* Catching any Errors and Rolling back the Transaction */

BEGIN CATCH
	DECLARE @ErrorMessage   NVARCHAR(4000);
	DECLARE @ErrorSeverity  INT           ;
	DECLARE @ErrorState     INT           ;

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE();

	SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState
	  , ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;

	RAISERROR (@ErrorMessage  ,    /* Error text Message.     */
			   @ErrorSeverity ,    /* Error Severity number . */
			   @ErrorState   );    /* Error State.            */

	IF @@TRANCOUNT > 0
	   BEGIN
		   ROLLBACK TRANSACTION ;
	   END
	    
END CATCH ;


GO


