USE [VEI_LV]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Load_Data_Load_Tracking_Table]
/*********************************************************************************/
/* Parameters: (@Process_Name Varchar(100) = NULL, @FileName Varchar(100) = NULL */
/* , @Total_Number_of_Records_in_File INT = 0, @Work_Order_Insert_Count INT = 0, */
/* @VEI_Verification_Count INT = 0, @Work_Order_Updated_Count INT = 0,           */
/* @Duplicate_Record_Count INT = 0)                                              */
/* Description: This Stored Procedure inserts records into the Data Load         */
/*              Tracking Table.                                                  */
/*                                                                               */
/* Programmer:       Andy Eggers                                                 */
/* Date Written:     9/4/2017                                                    */
/* Change History:    [ Enter Date and why the change was necessary Here]        */
/*                                                                               */
/* *******************************************************************************/
/* 2. Process the incoming parameters                                            */
/* (@parameter1 varchar(max) = NULL, @parameter2 int = 0       )                 */
/* Note: I am assigning a default value of Null's or Zero's  in case no value    */
/* is passed into the Stored Procedure                                           */  
/* *******************************************************************************/
(@Process_Name Varchar(100) = NULL, @FileName Varchar(100) = NULL, @Total_Number_of_Records_in_File INT = 0,
  @Work_Order_Insert_Count INT = 0, @VEI_Verification_Count INT = 0, @Work_Order_Updated_Count INT = 0,
  @Duplicate_Record_Count INT = 0)
 
AS
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON

BEGIN TRY 
	BEGIN TRANSACTION
			INSERT INTO [dbo].[Data_Load_Tracking] ([Process_Name], [File_Name], [Total_Number_of_Records], [New_Record_Count], [VEI_Verification_Count], [Work_Orders_Updated_Count], [Duplicate_Record_Count])
			VALUES( @Process_Name, @FileName, @Total_Number_of_Records_in_File, @Work_Order_Insert_Count, @VEI_Verification_Count, @Work_Order_Updated_Count, @Duplicate_Record_Count)

		IF @@TRANCOUNT > 0
			BEGIN
				COMMIT TRANSACTION;
			END	
    
END TRY 

/* Catching any Errors and Rolling back the Transaction */

BEGIN CATCH
	DECLARE @ErrorMessage   NVARCHAR(4000);
	DECLARE @ErrorSeverity  INT           ;
	DECLARE @ErrorState     INT           ;

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE();

	SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState
	  , ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;

	RAISERROR (@ErrorMessage  ,    /* Error text Message.     */
			   @ErrorSeverity ,    /* Error Severity number . */
			   @ErrorState   );    /* Error State.            */

	IF @@TRANCOUNT > 0
	   BEGIN
		   ROLLBACK TRANSACTION ;
	   END
	    
END CATCH ;


GO


