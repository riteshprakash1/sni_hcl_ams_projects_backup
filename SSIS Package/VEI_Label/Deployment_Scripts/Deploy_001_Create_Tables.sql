/****************************************************************/
/* Create Tables Script                                         */
/****************************************************************/

USE [VEI_LV]
GO

/****** Object:  Table [dbo].[Catalog_Item_Master]    Script Date: 04/30/2018 13:17:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Catalog_Item_Master](
	[Catalog_Item_Master_ID] [int] IDENTITY(1,1) NOT NULL,
	[Catalog_Item] [varchar](32) NULL,
	[OT1_Required] [int] NULL,
	[OT1_Ratio] [varchar](8) NULL,
	[OT2_Required] [int] NULL,
	[OT2_Ratio] [varchar](8) NULL,
	[IN1_Required] [int] NULL,
	[IN1_Ratio] [varchar](8) NULL,
	[IN2_Required] [int] NULL,
	[IN2_Ratio] [varchar](8) NULL,
	[CS1_Required] [int] NULL,
	[CS1_Ratio] [varchar](8) NULL,
	[CS2_Required] [int] NULL,
	[CS2_Ratio] [varchar](8) NULL,
	[MSC1_Required] [int] NULL,
	[MSC1_Ratio] [varchar](8) NULL,
	[MSC2_Required] [int] NULL,
	[MSC2_Ratio] [varchar](8) NULL,
 CONSTRAINT [PK_Catalog_Item_Master] PRIMARY KEY CLUSTERED 
(
	[Catalog_Item_Master_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Catalog_Item_Master Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'Catalog_Item_Master_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Part Number (Item Number, Catalog Item, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'Catalog_Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 Required - Default Value = 0,  The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'OT1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'OT1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'OT2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'OT2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'IN1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'IN1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'IN2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'IN2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'CS1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'CS1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'CS2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'CS2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'MSC1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'MSC1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'MSC2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Catalog_Item_Master', @level2type=N'COLUMN',@level2name=N'MSC2_Ratio'
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_OT1_Required]  DEFAULT ((0)) FOR [OT1_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_OT2_Required]  DEFAULT ((0)) FOR [OT2_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_IN1_Required]  DEFAULT ((0)) FOR [IN1_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_IN2_Required]  DEFAULT ((0)) FOR [IN2_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_CS1_Required]  DEFAULT ((0)) FOR [CS1_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_CS2_Required]  DEFAULT ((0)) FOR [CS2_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_MSC1_Required]  DEFAULT ((0)) FOR [MSC1_Required]
GO

ALTER TABLE [dbo].[Catalog_Item_Master] ADD  CONSTRAINT [DF_Catalog_Item_Master_MSC2_Required]  DEFAULT ((0)) FOR [MSC2_Required]
GO



CREATE TABLE [dbo].[Component](
	[Component_ID] [int] IDENTITY(1,1) NOT NULL,
	[Work_Order] [varchar](32) NULL,
	[Batch_Number] [varchar](32) NULL,
	[Catalog_Item] [varchar](32) NULL,
	[OT1_Required] [int] NULL,
	[OT1_Ratio] [varchar](8) NULL,
	[OT2_Required] [int] NULL,
	[OT2_Ratio] [varchar](8) NULL,
	[IN1_Required] [int] NULL,
	[IN1_Ratio] [varchar](8) NULL,
	[IN2_Required] [int] NULL,
	[IN2_Ratio] [varchar](8) NULL,
	[CS1_Required] [int] NULL,
	[CS1_Ratio] [varchar](8) NULL,
	[CS2_Required] [int] NULL,
	[CS2_Ratio] [varchar](8) NULL,
	[MSC1_Required] [int] NULL,
	[MSC1_Ratio] [varchar](8) NULL,
	[MSC2_Required] [int] NULL,
	[MSC2_Ratio] [varchar](8) NULL,
	[IFU_1] [varchar](10) NULL,
	[IFU_2] [varchar](10) NULL,
	[IFU_3] [varchar](10) NULL,
	[IFU_4] [varchar](10) NULL,
	[IFU_5] [varchar](10) NULL,
	[VEI_Order_Initiated] [int] NULL,
	[Date_VEI_Order_Initiated] [datetime] NULL,
	[Date_Entered] [datetime] NULL,
 CONSTRAINT [PK_Component] PRIMARY KEY CLUSTERED 
(
	[Component_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'Component_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SAP Work Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'Work_Order'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Lot Number (Batch, Work Order, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'Batch_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Part Number (Item Number, Catalog Item, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'Catalog_Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 Required - Default Value = 0,  The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'OT1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'OT1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'OT2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'OT2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IN1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IN1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IN2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IN2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'CS1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'CS1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'CS2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'CS2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'MSC1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 1 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'MSC1_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'MSC2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 2 Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'MSC2_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 1, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IFU_1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 2, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IFU_2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 3, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IFU_3'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 4, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IFU_4'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 5, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'IFU_5'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An Indicator to show if VEI has started their Verification Process. Default Value = Zero,  1 = The VEI process has started and Do Not change anything within this record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'VEI_Order_Initiated'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time the VEI Verification Process started.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'Date_VEI_Order_Initiated'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was first entered into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_OT1_Required]  DEFAULT ((0)) FOR [OT1_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_OT2_Required]  DEFAULT ((0)) FOR [OT2_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_IN1_Required]  DEFAULT ((0)) FOR [IN1_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_IN2_Required]  DEFAULT ((0)) FOR [IN2_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_CS1_Required]  DEFAULT ((0)) FOR [CS1_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_CS2_Required]  DEFAULT ((0)) FOR [CS2_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_MSC1_Required]  DEFAULT ((0)) FOR [MSC1_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_MSC2_Required]  DEFAULT ((0)) FOR [MSC2_Required]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_Order_Started]  DEFAULT ((0)) FOR [VEI_Order_Initiated]
GO

ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO


CREATE TABLE [dbo].[Component_Clean](
	[Component_Clean_ID] [int] IDENTITY(1,1) NOT NULL,
	[Catalog_Item] [varchar](32) NULL,
	[OT1_InClean] [int] NULL,
	[OT2_InClean] [int] NULL,
	[IN1_InClean] [int] NULL,
	[IN2_InClean] [int] NULL,
	[CS1_InClean] [int] NULL,
	[CS2_InClean] [int] NULL,
	[MSC1_InClean] [int] NULL,
	[MSC2_InClean] [int] NULL,
	[IFU_1_Clean] [int] NULL,
	[IFU_2_Clean] [int] NULL,
	[IFU_3_Clean] [int] NULL,
	[IFU_4_Clean] [int] NULL,
	[IFU_5_Clean] [int] NULL,
	[Date_Entered] [datetime] NULL,
 CONSTRAINT [PK_Component_Clean] PRIMARY KEY CLUSTERED 
(
	[Component_Clean_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Clean Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'Component_Clean_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Part Number (Item Number, Catalog Item, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'Catalog_Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 In Cleanroom - Default Value = 0, The Value of Zero = Not in Clean Room, 1 = In Cleanroom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'OT1_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 In Cleanroom - Default Value = 0, The Value of Zero = Not in Clean Room, 1 = In Cleanroom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'OT2_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 In Cleanroom - Default Value = 0, The Value of Zero = Not in Clean Room, 1 = In Cleanroom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IN1_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 In Cleanroom - Default Value = 0, The Value of Zero = Not in Clean Room, 1 = In Cleanroom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IN2_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 1 In Cleanroom - Default Value = 0, The Value of Zero = Not in Cleanroom , 1 = In Cleanroom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'CS1_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 2 In Cleanroom - Default Value = 0, The Value of Zero = Not in Cleanroom , 1 = In Cleanroom
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'CS2_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 2 In Cleanroom - Default Value = 0, The Value of Zero = Not in Cleanroom , 1 = In Cleanroom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'MSC2_InClean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 1 Clean Room, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IFU_1_Clean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 2 Clean Room, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IFU_2_Clean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 3 Clean Room, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IFU_3_Clean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 4 Clean Room, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IFU_4_Clean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 5 Clean Room, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'IFU_5_Clean'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was first entered into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Component_Clean', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_OT1_InClean]  DEFAULT ((0)) FOR [OT1_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_OT2_InClean]  DEFAULT ((0)) FOR [OT2_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_IN1_InClean]  DEFAULT ((0)) FOR [IN1_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_IN2_InClean]  DEFAULT ((0)) FOR [IN2_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_CS1_InClean]  DEFAULT ((0)) FOR [CS1_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_CS2_InClean]  DEFAULT ((0)) FOR [CS2_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_MSC2_InClean]  DEFAULT ((0)) FOR [MSC2_InClean]
GO

ALTER TABLE [dbo].[Component_Clean] ADD  CONSTRAINT [DF_Component_Clean_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO


CREATE TABLE [dbo].[Data_Load_Tracking](
	[Data_Load_Tracking_ID] [int] IDENTITY(1,1) NOT NULL,
	[Process_Name] [varchar](100) NULL,
	[File_Name] [varchar](100) NULL,
	[Total_Number_of_Records] [int] NULL,
	[New_Record_Count] [int] NULL,
	[VEI_Verification_Count] [int] NULL,
	[Work_Orders_Updated_Count] [int] NULL,
	[Duplicate_Record_Count] [int] NULL,
	[Date_Entered] [datetime] NULL,
 CONSTRAINT [PK_Data_Load_Tracking] PRIMARY KEY CLUSTERED 
(
	[Data_Load_Tracking_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data_Loading_Tracking Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'Data_Load_Tracking_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the import Process which inserted this information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'Process_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The File Name which was processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'File_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Number of Records Loaded into the Tables' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'Total_Number_of_Records'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Number of New Records from SAP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'New_Record_Count'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Number of VEI Verification records in the File Processed. Note these Records were not update.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'VEI_Verification_Count'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Number of Existing Work Order Updated with New information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'Work_Orders_Updated_Count'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Number of Duplicate Records in this File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'Duplicate_Record_Count'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was inserted into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Data_Load_Tracking', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

ALTER TABLE [dbo].[Data_Load_Tracking] ADD  CONSTRAINT [DF_Data_Load_Tracking_Total_Number_of_Records]  DEFAULT ((0)) FOR [Total_Number_of_Records]
GO

ALTER TABLE [dbo].[Data_Load_Tracking] ADD  CONSTRAINT [DF_Table1_New_Records_Count]  DEFAULT ((0)) FOR [New_Record_Count]
GO

ALTER TABLE [dbo].[Data_Load_Tracking] ADD  CONSTRAINT [DF_Data_Load_Tracking_VEI_Verification_Count]  DEFAULT ((0)) FOR [VEI_Verification_Count]
GO

ALTER TABLE [dbo].[Data_Load_Tracking] ADD  CONSTRAINT [DF_Data_Load_Tracking_Work_Orders_Updated_Count]  DEFAULT ((0)) FOR [Work_Orders_Updated_Count]
GO

ALTER TABLE [dbo].[Data_Load_Tracking] ADD  CONSTRAINT [DF_Data_Load_Tracking_Duplicate_Record_Count]  DEFAULT ((0)) FOR [Duplicate_Record_Count]
GO

ALTER TABLE [dbo].[Data_Load_Tracking] ADD  CONSTRAINT [DF_Data_Load_Tracking_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO



CREATE TABLE [dbo].[History_Detail](
	[History_Detail_ID] [int] IDENTITY(1,1) NOT NULL,
	[History_Master_ID] [int] NULL,
	[Work_Order] [varchar](32) NULL,
	[Detail_Type] [varchar](32) NULL,
	[Date_Time] [datetime] NULL,
	[User_Name] [varchar](32) NULL,
	[Production_Line] [varchar](32) NULL,
	[Data] [varchar](128) NULL,
	[Description] [varchar](128) NULL,
	[Date_Entered] [datetime] NULL,
 CONSTRAINT [PK_History_Detail] PRIMARY KEY CLUSTERED 
(
	[History_Detail_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'History Detail Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'History_Detail_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'History Master Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'History_Master_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Lot Number (Batch, Work Order, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Work_Order'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System identifier for detail type: Scan, error, component, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Detail_Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Date_Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User who performed the action (can be different than users in master table)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'User_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For instances where an order can be run on multiple lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Production_Line'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scanned, Typed or Retrieved data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Data'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System notes (Override messages, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was entered into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Detail', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

ALTER TABLE [dbo].[History_Detail] ADD  CONSTRAINT [DF_History_Detail_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO


CREATE TABLE [dbo].[History_Master](
	[History_Master_ID] [int] IDENTITY(1,1) NOT NULL,
	[Work_Order] [varchar](32) NULL,
	[Batch_Number] [varchar](32) NULL,
	[Catalog_Item] [varchar](32) NULL,
	[Quantity] [int] NULL,
	[Adjusted_Quantity] [int] NULL,
	[Verification_Status] [int] NULL,
	[Production_Line_Started] [varchar](32) NULL,
	[Username_Started] [varchar](32) NULL,
	[Username_Completed] [varchar](32) NULL,
	[Date_Time_Started] [datetime] NULL,
	[Date_Time_Completed] [datetime] NULL,
	[OT1_Required] [int] NULL,
	[OT1_Quantity_Complete] [int] NULL,
	[OT2_Required] [int] NULL,
	[OT2_Quantity_Complete] [int] NULL,
	[IN1_Required] [int] NULL,
	[IN1_Quantity_Complete] [int] NULL,
	[IN2_Required] [int] NULL,
	[IN2_Quantity_Complete] [int] NULL,
	[CS1_Required] [int] NULL,
	[CS1_Quantity_Complete] [int] NULL,
	[CS2_Required] [int] NULL,
	[CS2_Quantity_Complete] [int] NULL,
	[MSC1_Required] [int] NULL,
	[MSC1_Quantity_Complete] [int] NULL,
	[MSC2_Required] [int] NULL,
	[MSC2_Quantity_Complete] [int] NULL,
	[IFU1_Quantity_Complete] [int] NULL,
	[IFU2_Quantity_Complete] [int] NULL,
	[IFU3_Quantity_Complete] [int] NULL,
	[IFU4_Quantity_Complete] [int] NULL,
	[IFU5_Quantity_Complete] [int] NULL,
	[Date_Entered] [datetime] NULL,
 CONSTRAINT [PK_History_Master] PRIMARY KEY CLUSTERED 
(
	[History_Master_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'History_Master Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'History_Master_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SAP Work Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Work_Order'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Lot Number (Batch, Work Order, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Batch_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Part Number (Item Number, Catalog Item, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Catalog_Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total parts/kits for the order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Quantity'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If supervisor changes order qty, new value is stored here' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Adjusted_Quantity'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status (In use, Complete, incomplete, etc.)  0 = ? , 1 = ? , XXXX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Verification_Status'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Production line where order was initiated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Production_Line_Started'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User who initiated the order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Username_Started'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User who completed the order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Username_Completed'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time order was started' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Date_Time_Started'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time order was ended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Date_Time_Completed'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 Required - Default Value = 0,  The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'OT1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 1 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'OT1_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'OT2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outer Label Field Number 2 for Quantity, Default Value = 0 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'OT2_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IN1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 1 for Quantity, Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IN1_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IN2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inner Label Field Number 2 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IN2_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'CS1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Quantity Field Number 1 for Quantity, Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'CS1_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'CS2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chart Stick Quantity Field Number 2 for Quantity, Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'CS2_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 1 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'MSC1_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous  Label Field Number 1 for Quantity, Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'MSC1_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Label Field Number 2 Required - Default Value = 0, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'MSC2_Required'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous  Label Field Number 2 for Quantity, Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'MSC2_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 1 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IFU1_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 2 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IFU2_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 3 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IFU3_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 4 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IFU4_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 5 Quantity Complete - Default Value = 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'IFU5_Quantity_Complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this Record was first entered into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'History_Master', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_Quantity]  DEFAULT ((0)) FOR [Quantity]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_Adjusted_Quantity]  DEFAULT ((0)) FOR [Adjusted_Quantity]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_Verification_Status]  DEFAULT ((0)) FOR [Verification_Status]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_OT1_Required]  DEFAULT ((0)) FOR [OT1_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_OT1_Quantity]  DEFAULT ((0)) FOR [OT1_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_OT2_Required]  DEFAULT ((0)) FOR [OT2_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_OT2_Quantity]  DEFAULT ((0)) FOR [OT2_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IN1_Required]  DEFAULT ((0)) FOR [IN1_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IN1_Quantity]  DEFAULT ((0)) FOR [IN1_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IN2_Required]  DEFAULT ((0)) FOR [IN2_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IN2_Quantity]  DEFAULT ((0)) FOR [IN2_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_CS1_Required]  DEFAULT ((0)) FOR [CS1_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_CS1_Quantity]  DEFAULT ((0)) FOR [CS1_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_CS2_Required]  DEFAULT ((0)) FOR [CS2_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_CS2_Quantity]  DEFAULT ((0)) FOR [CS2_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_MSC1_Required]  DEFAULT ((0)) FOR [MSC1_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_MSC1_Quantity]  DEFAULT ((0)) FOR [MSC1_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_MSC2_Required]  DEFAULT ((0)) FOR [MSC2_Required]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_MSC2_Quantity]  DEFAULT ((0)) FOR [MSC2_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IFU1_Quantity_Complete
IFU1_Quantity_Complete]  DEFAULT ((0)) FOR [IFU1_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IFU2_Quantity_Complete]  DEFAULT ((0)) FOR [IFU2_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IFU3_Quantity_Complete]  DEFAULT ((0)) FOR [IFU3_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IFU4_Quantity_Complete]  DEFAULT ((0)) FOR [IFU4_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_IFU5_Quantity_Complete]  DEFAULT ((0)) FOR [IFU5_Quantity_Complete]
GO

ALTER TABLE [dbo].[History_Master] ADD  CONSTRAINT [DF_History_Master_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO



CREATE TABLE [dbo].[IFU_Data](
	[IFU_Data_ID] [int] IDENTITY(1,1) NOT NULL,
	[Catalog_Item] [varchar](32) NULL,
	[IFU_1] [varchar](10) NULL,
	[IFU_1_Enabled] [int] NULL,
	[IFU_2] [varchar](10) NULL,
	[IFU_2_Enabled] [int] NULL,
	[IFU_3] [varchar](10) NULL,
	[IFU_3_Enabled] [int] NULL,
	[IFU_4] [varchar](10) NULL,
	[IFU_4_Enabled] [int] NULL,
	[IFU_5] [varchar](10) NULL,
	[IFU_5_Enabled] [int] NULL,
	[IFU_Ratio] [varchar](8) NULL,
	[Date_Entered] [datetime] NULL,
	[Date_Record_Updated] [datetime] NULL,
	[Operator_Name] [varchar](30) NULL,
 CONSTRAINT [PK_IFU_Data] PRIMARY KEY CLUSTERED 
(
	[IFU_Data_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IFU_Data Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_Data_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Part Number (Item Number, Catalog Item, etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'Catalog_Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 1, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use Enabled Field Number 1 to Identify if this IFU is Used- Default Value = 1, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_1_Enabled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 2, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use Enabled Field Number 2 to Identify if this IFU is Used- Default Value = 1, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_2_Enabled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 3, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_3'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use Enabled Field Number 3 to Identify if this IFU is Used- Default Value = 1, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_3_Enabled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 4, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_4'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use Enabled Field Number 4 to Identify if this IFU is Used- Default Value = 1, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_4_Enabled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use - Field Number 5, if the Field is Empty or Full spaces, then this is not required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_5'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instructions For Use Enabled Field Number 5 to Identify if this IFU is Used- Default Value = 1, The Value of Zero = Not Required , 1 = Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_5_Enabled'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Label Field Ratio (Parts:Labels --   Order_Quantity/Parts or Order_Quantity*Labels)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'IFU_Ratio'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was first entered into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was Updated in this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'Date_Record_Updated'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Windows Login Name of Who Updated / Changed the Record in any way' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IFU_Data', @level2type=N'COLUMN',@level2name=N'Operator_Name'
GO

ALTER TABLE [dbo].[IFU_Data] ADD  CONSTRAINT [DF_IFU_Data_IFU_1_Enabled]  DEFAULT ((1)) FOR [IFU_1_Enabled]
GO

ALTER TABLE [dbo].[IFU_Data] ADD  CONSTRAINT [DF_IFU_Data_IFU_2_Enabled]  DEFAULT ((1)) FOR [IFU_2_Enabled]
GO

ALTER TABLE [dbo].[IFU_Data] ADD  CONSTRAINT [DF_IFU_Data_IFU_3_Enabled]  DEFAULT ((1)) FOR [IFU_3_Enabled]
GO

ALTER TABLE [dbo].[IFU_Data] ADD  CONSTRAINT [DF_IFU_Data_IFU_4_Enabled]  DEFAULT ((1)) FOR [IFU_4_Enabled]
GO

ALTER TABLE [dbo].[IFU_Data] ADD  CONSTRAINT [DF_IFU_Data_IFU_5_Enabled]  DEFAULT ((1)) FOR [IFU_5_Enabled]
GO

ALTER TABLE [dbo].[IFU_Data] ADD  CONSTRAINT [DF_IFU_Data_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO


CREATE TABLE [dbo].[Setting_Translations](
	[Setting_Translations_ID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NULL,
	[OriginalText] [varchar](128) NULL,
	[Translation] [varchar](128) NULL,
	[Date_Entered] [datetime] NULL,
 CONSTRAINT [PK_Setting_Translations] PRIMARY KEY CLUSTERED 
(
	[Setting_Translations_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Setting Translations Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Setting_Translations', @level2type=N'COLUMN',@level2name=N'Setting_Translations_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Language Unique Record Identification Number. ie English = 1, French = 2 ...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Setting_Translations', @level2type=N'COLUMN',@level2name=N'LanguageID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Original Text displayed ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Setting_Translations', @level2type=N'COLUMN',@level2name=N'OriginalText'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Original Text displayed in the New Language' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Setting_Translations', @level2type=N'COLUMN',@level2name=N'Translation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time this record was first entered into this table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Setting_Translations', @level2type=N'COLUMN',@level2name=N'Date_Entered'
GO

ALTER TABLE [dbo].[Setting_Translations] ADD  CONSTRAINT [DF_Setting_Translations_Date_Entered]  DEFAULT (getdate()) FOR [Date_Entered]
GO



CREATE TABLE [dbo].[Stage_Catalog_Item_Master](
	[Catalog_Item] [varchar](32) NULL,
	[OT1_Required] [int] NULL,
	[OT1_Ratio] [varchar](8) NULL,
	[OT2_Required] [int] NULL,
	[OT2_Ratio] [varchar](8) NULL,
	[IN1_Required] [int] NULL,
	[IN1_Ratio] [varchar](8) NULL,
	[IN2_Required] [int] NULL,
	[IN2_Ratio] [varchar](8) NULL,
	[CS1_Required] [int] NULL,
	[CS1_Ratio] [varchar](8) NULL,
	[CS2_Required] [int] NULL,
	[CS2_Ratio] [varchar](8) NULL,
	[MSC1_Required] [int] NULL,
	[MSC1_Ratio] [varchar](8) NULL,
	[MSC2_Required] [int] NULL,
	[MSC2_Ratio] [varchar](8) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



CREATE TABLE [dbo].[Stage_Work_Order_Data](
	[Stage_Work_Order_Data_ID] [int] IDENTITY(1,1) NOT NULL,
	[BATCH] [varchar](50) NULL,
	[COO] [varchar](50) NULL,
	[DOM] [varchar](50) NULL,
	[EXPIRY_DATE] [varchar](50) NULL,
	[IFU_1] [varchar](50) NULL,
	[IFU_2] [varchar](50) NULL,
	[IFU_3] [varchar](50) NULL,
	[IFU_4] [varchar](50) NULL,
	[IFU_5] [varchar](50) NULL,
	[IMPORT_DATE] [varchar](50) NULL,
	[ITEM_NUMBER] [varchar](50) NULL,
	[ORDER_NUMBER] [varchar](50) NULL,
	[ORDER_QUANTITY] [varchar](50) NULL,
	[SOURCE] [varchar](50) NULL,
	[Validation] [int] NULL,
	[OT1_Required] [int] NULL,
	[OT1_Ratio] [varchar](8) NULL,
	[OT2_Required] [int] NULL,
	[OT2_Ratio] [varchar](8) NULL,
	[IN1_Required] [int] NULL,
	[IN1_Ratio] [varchar](8) NULL,
	[IN2_Required] [int] NULL,
	[IN2_Ratio] [varchar](8) NULL,
	[CS1_Required] [int] NULL,
	[CS1_Ratio] [varchar](8) NULL,
	[CS2_Required] [int] NULL,
	[CS2_Ratio] [varchar](8) NULL,
	[MSC1_Required] [int] NULL,
	[MSC1_Ratio] [varchar](8) NULL,
	[MSC2_Required] [int] NULL,
	[MSC2_Ratio] [varchar](8) NULL,
 CONSTRAINT [PK_Stage_Work_Order_Data] PRIMARY KEY CLUSTERED 
(
	[Stage_Work_Order_Data_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A Field used for Data Validation for Existing Records, 1= VEI started Working on, 2 = Update Existing records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Stage_Work_Order_Data', @level2type=N'COLUMN',@level2name=N'Validation'
GO

ALTER TABLE [dbo].[Stage_Work_Order_Data] ADD  CONSTRAINT [DF_Stage_Work_Order_Data_Validation]  DEFAULT ((0)) FOR [Validation]
GO



CREATE TABLE [dbo].[System_Log](
	[System_Log_ID] [int] IDENTITY(1,1) NOT NULL,
	[DateTimeStamp] [datetime] NULL,
	[SystemName] [varchar](32) NULL,
	[UserName] [varchar](32) NULL,
	[OtherID] [varchar](32) NULL,
	[EventType] [varchar](32) NULL,
	[EventText] [varchar](128) NULL,
	[EventDetail] [varchar](max) NULL,
 CONSTRAINT [PK_System_Log] PRIMARY KEY CLUSTERED 
(
	[System_Log_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System_Log Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'System_Log_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time of Event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'DateTimeStamp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Generated from PC name.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'SystemName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Logged In User or user attempted login.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'UserName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch/WO/Part Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'OtherID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category: PROGRAM_START, LOGIN_FAILURE, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'EventType'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Brief overview of the event.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'EventText'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Detail of the Event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Log', @level2type=N'COLUMN',@level2name=N'EventDetail'
GO


CREATE TABLE [dbo].[System_Setting](
	[System_Settings_ID] [int] IDENTITY(1,1) NOT NULL,
	[SystemID] [varchar](64) NULL,
	[GroupID] [varchar](64) NULL,
	[SettingID] [varchar](64) NULL,
	[Setting] [varchar](192) NULL,
 CONSTRAINT [PK_Sytem_Setting] PRIMARY KEY CLUSTERED 
(
	[System_Settings_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System_Setting Table Unique Record Identification Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Setting', @level2type=N'COLUMN',@level2name=N'System_Settings_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Generated From PC Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Setting', @level2type=N'COLUMN',@level2name=N'SystemID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category of the SettingID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Setting', @level2type=N'COLUMN',@level2name=N'GroupID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the Setting
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Setting', @level2type=N'COLUMN',@level2name=N'SettingID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System_Setting', @level2type=N'COLUMN',@level2name=N'Setting'
GO




