USE [Staging_Table]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CXP_CUSTO__ORG_C_CUS_Tracking]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF__CXP_CUSTO__ORG_C_CUS_Tracking]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CXP_CUSTO__SYNCH_CUS_Tracking]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF__CXP_CUSTO__SYNCH_CUS_Tracking]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TRANS_CREATED_DATE_STG_CUS_Tracking]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF_TRANS_CREATED_DATE_STG_CUS_Tracking]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Record_Counter]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Record_Counter]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Resubmitted_Counter]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Resubmitted_Counter]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Record_Fixed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Record_Fixed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Date_Inserted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] DROP CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Date_Inserted]
END

GO

USE [Staging_Table]
GO

/****** Object:  Table [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking]    Script Date: 05/27/2016 13:22:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking]') AND type in (N'U'))
DROP TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking]
GO


USE [Staging_Table]
GO

/****** Object:  Table [dbo].[SSIS Configurations]    Script Date: 05/27/2016 13:22:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SSIS_Configurations]') AND type in (N'U'))
DROP TABLE [dbo].[SSIS_Configurations]
GO



