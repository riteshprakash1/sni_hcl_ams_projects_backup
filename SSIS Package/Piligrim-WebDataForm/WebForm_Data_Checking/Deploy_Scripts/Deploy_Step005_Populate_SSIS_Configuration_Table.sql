USE Staging_Table


/* Insert statments for the SSIS configuration table */

/* Connection string to the SMTP Server */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','SmtpServer=us-relay.SNCORP.Smith-nephew.com;UseWindowsAuthentication=False;EnableSsl=False;','\Package.Connections[SMTP Connection Manager].Properties[ConnectionString]','String')

/* Connection string to the database */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','Data Source=LocalHost;Initial Catalog=Staging_Table;Provider=SQLNCLI10.1;Integrated Security=SSPI;Packet Size=32767','\Package.Connections[LocalHost.Staging_Table].Properties[ConnectionString]','String')

/* Send the Email To  */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','Lisa.Chambliss@smith-nephew.com','\Package.Variables[User::Email_To].Properties[Value]','String')

/* Email Subject Line  */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','Pilgrim Web Intake Error Reprocessing','\Package.Variables[User::Email_Subject_Line].Properties[Value]','String')

/* Email From  */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','andy.eggers@smith-nephew.com','\Package.Variables[User::Email_From].Properties[Value]','String')

/* Email CC: Line */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','andy.eggers@smith-nephew.com','\Package.Variables[User::Email_CC].Properties[Value]','String')

/* Email Message line(s) */
INSERT INTO [SSIS_Configurations](ConfigurationFilter, ConfiguredValue, PackagePath, ConfiguredValueType)
VALUES('WebForm_Data_Checking','"Hello, the following Synch_ID s: " + @[User::Synch_id] +" These Synch_id(s) are in the CXP_CUSTOMER_PXP_STG_CUS_Tracking table where the Record_Counter is greater than 1 and the Record_Fixed is NO. Once you have fixed the record(s) please change the Record_Fixed value to Yes."', '\Package.Variables[User::Email_Message].Properties[Value]','String')

