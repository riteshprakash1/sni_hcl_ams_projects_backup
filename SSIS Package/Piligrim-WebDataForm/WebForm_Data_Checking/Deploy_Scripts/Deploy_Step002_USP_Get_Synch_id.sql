USE [Staging_Table]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_Get_Synch_id]
/******************************************************************************/
/* Parameters: none                                                           */
/* Description: Resubmitting the User entered data for processing and         */
/*              notification of user for correction after being resubmitted   */
/*              2 or more times                                               */
/*                                                                            */
/*                                                                            */
/* Programmer:        [ Andy Eggers ]                                         */
/* Date Written:      [ 5/25/2016   ]                                         */
/* Change History:    [ Enter Date and why the change was necessary Here]     */
/*                                                                            */
/* Usage: EXEC USP_Send_Email_Notification_Get_Synch_IDs                      */
/* ****************************************************************************/
/* 2. Process the incoming parameters                                         */
 @Synch_ID_String nvarchar(4000) = '' OUTPUT 
 
AS
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON

--DECLARE @Synch_ID_String nvarchar(4000) = '' -- OUTPUT 
DECLARE @loop_id         int  = 1           ,
		@max_loop_id     int   =1         

/* Create Temp Table of Synch_ID's */
Select CAST(SYNCH_ID AS Varchar(200))AS Synch_ID INTO #Temp_Synch_ID
FROM Staging_Table.dbo.CXP_CUSTOMER_PXP_STG_CUS_Tracking WHERE Record_Counter > 1 AND Record_Fixed = 'No '

/* Adding Row Identification Number to Temp Table */
ALTER TABLE #Temp_Synch_ID
ADD Row_id INT IDENTITY(1,1) NOT NULL

/* Getting the Max Number of Synch ID's   */
SET @max_loop_id = (Select MAX(Row_id) FROM #Temp_Synch_ID)

/* Looping through the result set to place all the Synch_ID's into one Variable */
WHILE @loop_id <= @max_loop_id
	 Begin
		SET @Synch_ID_String = @Synch_ID_String + (Select RTRIM(LTRIM(CAST(SYNCH_ID AS Varchar(200)))) +', ' AS Synch_ID FROM #Temp_Synch_ID where Row_ID = @loop_id )
		SET @loop_id = @loop_id + 1 
	 END
	
	
/* Returning the Synch_id string */
-- RETURN 
SELECT RTRIM(LTRIM(@Synch_ID_String)) AS Synch_ID_String


	

GO


