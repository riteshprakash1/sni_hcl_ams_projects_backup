USE [Staging_Table]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking](
	[CXP_Customer_PXP_STG_CUS_Tracking_ID] [int] IDENTITY(1,1) NOT NULL,
	[QXP_EXCEPTION_NO]                     [varchar](40)  NULL,
	[QXP_EXCEPTION_TYPE]                   [varchar](40)  NULL,
	[CXP_CLIENT_CODE]                      [varchar](40)  NULL,
	[CMC_CODE]                             [varchar](40)  NULL,
	[CMC_TYPE]                             [varchar](50)  NULL,
	[CMC_TITLE]                            [varchar](120) NULL,
	[CMC_COMPANY]                          [varchar](60)  NULL,
	[CMC_PHONE]                            [varchar](25)  NULL,
	[CMC_FAX]                              [varchar](25)  NULL,
	[CMC_E_MAIL]                           [varchar](256) NULL,
	[CMC_NAME]                             [varchar](40)  NULL,
	[CMC_STREET]                           [varchar](120) NULL,
	[CMC_CITY]                             [varchar](60)  NULL,
	[CMC_STATE]                            [varchar](60)  NULL,
	[CMC_ZIPCODE]                          [varchar](10)  NULL,
	[CMC_COUNTRY]                          [varchar](30)  NULL,
	[CMC_UDF_STRING_1]                     [varchar](255) NULL,
	[CMC_UDF_STRING_2]                     [varchar](255) NULL,
	[QXP_UDF_STRING_1]                     [varchar](255) NULL,
	[QXP_UDF_STRING_2]                     [varchar](255) NULL,
	[QXP_UDF_STRING_3]                     [varchar](255) NULL,
	[QXP_UDF_STRING_4]                     [varchar](255) NULL,
	[QXP_UDF_STRING_5]                     [varchar](255) NULL,
	[QXP_UDF_STRING_6]                     [varchar](255) NULL,
	[QXP_UDF_STRING_7]                     [varchar](255) NULL,
	[QXP_UDF_STRING_8]                     [varchar](255) NULL,
	[CXP_AWARE_DATE]                       [datetime]     NULL,
	[CXP_BRAND_NAME]                       [varchar](60)  NULL,
	[CXP_CATALOG_NUMBER]                   [varchar](60)  NULL,
	[CXP_MFG_DATE]                         [datetime]     NULL,
	[CXP_EXP_DATE]                         [datetime]     NULL,
	[CXP_INJURY]                           [int]          NULL,
	[CXP_DEATH]                            [int]          NULL,
	[QXP_UDF_DATE_1]                       [datetime]     NULL,
	[QXP_UDF_DATE_2]                       [datetime]     NULL,
	[QXP_UDF_DATE_3]                       [datetime]     NULL,
	[PXP_PRODUCT_CODE]                     [varchar](40)  NULL,
	[PXP_LOT_NUMBER]                       [varchar](60)  NULL,
	[PXP_QTY_AFFECTED]                     [int]          NULL,
	[PXP_QTY_UNIT]                         [varchar](40)  NULL,
	[PXP_UNIT_COST]                        [numeric](12, 4) NULL,
	[QXP_REPORT_DATE]                      [datetime]     NULL,
	[QXP_REPORTER_CODE]                    [varchar](40)  NULL,
	[QXP_OCCURENCE_DATE]                   [datetime]     NULL,
	[CXP_CNTRY_OF_ORIGIN]                  [varchar](30)  NULL,
	[QXP_ORU_CODE]                         [varchar](40)  NULL,
	[QXP_SHORT_DESC]                       [varchar](100) NULL,
	[QXP_DESCRIPTION]                      [varchar](2000) NULL,
	[QXP_CATEGORY]                         [varchar](30)  NULL,
	[QXP_COST]                             [int]          NULL,
	[QXP_PRIORITY]                         [int]          NULL,
	[QXP_SEVERITY]                         [int]          NULL,
	[QXP_REFERENCE]                        [varchar](60)  NULL,
	[QXP_ROOT_CAUSE]                       [varchar](100) NULL,
	[ORG_CODE]                             [varchar](40)  NULL,
	[TRANS_DATE]                           [datetime]     NULL,
	[ERROR_CODE]                           [varchar](50)  NULL,
	[ERROR_INFO]                           [varchar](2000) NULL,
	[SYNCH_STATUS]                         [varchar](20)  NULL,
	[SYNCH_ID]                             [char](36)     NOT NULL,
	[QXP_STATUS]                           [varchar](30)  NULL,
	[PXP_QTY_DEFECT]                       [numeric](12, 4) NULL,
	[QXP_ORU_NAME]                         [varchar](40)  NULL,
	[CXP_CLIENT_NAME]                      [varchar](40)  NULL,
	[EXCEP_SYNCH_ID]                       [varchar](36)  NULL,
	[PXP_FCC_CODE]                         [varchar](40)  NULL,
	[PXP_REPORTING_SITE_CODE]              [varchar](40)  NULL,
	[TRANS_CREATED_DATE]                   [datetime]     NULL,
	[QXP_COORD_CODE]                       [varchar](40)  NULL,
	[QXP_COORD_ROLE_CODE]                  [varchar](40)  NULL,
	[QXP_FM_UDF1]                          [varchar](60)  NULL,
	[QXP_FM_UDF2]                          [varchar](60)  NULL,
	[QXP_FM_UDF3]                          [varchar](60)  NULL,
	[QXP_FM_UDF4]                          [varchar](60)  NULL,
	[QXP_FM_UDF5]                          [varchar](60)  NULL,
	[QXP_ID]                               [varchar](25)  NULL,
	[TOKEN_ID]                             [char](60)     NULL,
	[CXP_SALES_REP_CONTACTED]              [varchar](10)  NULL,
	[CXP_SALES_REP_PRESENT]                [varchar](10)  NULL,
	[CXP_CUST_FOLLOWUP]                    [varchar](10)  NULL,
	[CXP_COMP_DEVICE]                      [varchar](10)  NULL,
	[CXP_REV_SURGERY]                      [varchar](10)  NULL,
	[CXP_PATIENT_INVOLVED]                 [varchar](10)  NULL,
	[CXP_FINAL_PROD_DISP]                  [varchar](20)  NULL,
	[CXP_OOB_FAILURE]                      [varchar](10)  NULL,
	[CXP_DEVICE_USED]                      [varchar](60)  NULL,
	[CXP_X_RAYS]                           [varchar](10)  NULL,
	[CXP_PIECE_BREAK_OFF]                  [varchar](10)  NULL,
	[CXP_PIECE_REMOVED]                    [varchar](10)  NULL,
	[CXP_DEVICE_WARRANTY]                  [varchar](20)  NULL,
	[CXP_PROD_SAMP_REQ]                    [varchar](255) NULL,
	[CXP_RETURN_EVAL]                      [varchar](20)  NULL,
	[PXP_UDF_STRING_9]                     [varchar](120) NULL,
	[PXP_UDF_STRING_10]                    [varchar](120) NULL,
	[PXP_UDF_STRING_11]                    [varchar](120) NULL,
	[PXP_UDF_STRING_12]                    [varchar](120) NULL,
	[PXP_UDF_STRING_13]                    [varchar](120) NULL,
	[PXP_UDF_STRING_14]                    [varchar](120) NULL,
	[PXP_UDF_STRING_15]                    [varchar](120) NULL,
	[PXP_UDF_STRING_16]                    [varchar](120) NULL,
	[CXP_PART_NO_ENTERED]                  [varchar](40)  NULL,
	[CXP_FORM_ENTERED_BY]                  [varchar](60)  NULL,
	[CMC_NAME1]                            [varchar](40)  NULL,
	[CMC_TYPE1]                            [varchar](50)  NULL,
	[CMC_PHONE1]                           [varchar](25)  NULL,
	[CMC_NAME2]                            [varchar](40)  NULL,
	[CMC_TYPE2]                            [varchar](50)  NULL,
	[CMC_PHONE2]                           [varchar](25)  NULL,
	[CMC_E_MAIL2]                          [varchar](256) NULL,
	[CXP_COMP_DIVISION]                    [varchar](40)  NULL,
	[CXP_SNCORP_ID]                        [varchar](40)  NULL,
	[CXP_ENTRY_DATETIME]                   [datetime]     NULL,
	[CXP_ADD_PART_INFO]                    [varchar](60)  NULL,
	[CMC_TYPE3]                            [varchar](50)  NULL,
	[CXP_FORM_ENT_BY_TITLE]                [varchar](40)  NULL,
	[CXP_ANESTHETIZED]                     [varchar](40)  NULL,
	[CXP_MED_INTERVENTION]                 [varchar](40)  NULL,
	[CMC_FAX2]                             [varchar](25)  NULL,
	[QXP_UDF_MEMO_1]                       [varchar](2000) NULL,
	[CXP_FORM_ENT_BY_PHONE]                [varchar](40)  NULL,
	[CXP_PART_UDIDI]                       [nvarchar](25) NULL,
	[CXP_SN_CONTACT_EMAIL]                 [varchar](60)  NULL,
	[Record_Counter]                       [int]          NULL,
	[Resubmitted_Counter]                  [int]          NULL,
	[Record_Fixed]                         [char](3)      NULL,
	[Date_Inserted]                        [datetime]     NULL,
 CONSTRAINT [PK_CXP_CUSTOMER_PXP_STG_CUS_Tracking] PRIMARY KEY CLUSTERED 
([SYNCH_ID] ASC) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__CXP_CUSTOMER_PXP_CUS_Tracking] UNIQUE NONCLUSTERED 
(
	[QXP_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] ADD  CONSTRAINT [DF__CXP_CUSTO__SYNCH_CUS_Tracking]  DEFAULT ('NEW') FOR [SYNCH_STATUS]
GO

ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] ADD  CONSTRAINT [DF_TRANS_CREATED_DATE_STG_CUS_Tracking]  DEFAULT (GetDate()) FOR [TRANS_CREATED_DATE]
GO

ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] ADD  CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Record_Counter]  DEFAULT ((1)) FOR [Record_Counter]
GO

ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] ADD  CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Resubmitted_Counter]  DEFAULT ((0)) FOR [Resubmitted_Counter]
GO

ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] ADD  CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Record_Fixed]  DEFAULT ('No') FOR [Record_Fixed]
GO

ALTER TABLE [dbo].[CXP_CUSTOMER_PXP_STG_CUS_Tracking] ADD  CONSTRAINT [DF_CXP_CUSTOMER_PXP_STG_CUS_Tracking_Date_Inserted]  DEFAULT (GetDate()) FOR [Date_Inserted]
GO

USE [Staging_Table]
/****************************************************************************/
CREATE TABLE [dbo].[SSIS_Configurations](
	[SSIS_Configurations_ID] [int] IDENTITY(1,1) NOT NULL,
	[ConfigurationFilter] [nvarchar](255) NULL,
	[VariableName] [nvarchar](100) NULL,
	[ConfiguredValue] [nvarchar](4000) NULL,
	[PackagePath] [nvarchar](255) NULL,
	[ConfiguredValueType] [nvarchar](50) NULL,
 CONSTRAINT [PK_SSIS_Configurations_ID] PRIMARY KEY CLUSTERED 
(
	[SSIS_Configurations_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
